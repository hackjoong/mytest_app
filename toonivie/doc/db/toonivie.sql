-- MySQL dump 10.13  Distrib 5.7.19, for Win32 (AMD64)
--
-- Host: localhost    Database: toonivie
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aaaaa`
--

DROP TABLE IF EXISTS `aaaaa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aaaaa` (
  `a` int(50) DEFAULT NULL,
  `b` int(50) DEFAULT NULL,
  `c` int(50) DEFAULT NULL,
  `d` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aaaaa`
--

LOCK TABLES `aaaaa` WRITE;
/*!40000 ALTER TABLE `aaaaa` DISABLE KEYS */;
INSERT INTO `aaaaa` VALUES (11,1,1,2),(22,3,4,5),(33,0,0,0);
/*!40000 ALTER TABLE `aaaaa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `b_file` varchar(200) NOT NULL,
  `ono` varchar(45) DEFAULT NULL,
  `juso` varchar(100) DEFAULT NULL,
  `kinds` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `cno` int(11) NOT NULL AUTO_INCREMENT,
  `tno` int(11) DEFAULT NULL,
  `comment` varchar(45) NOT NULL,
  `pw` text NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dapCount` int(11) DEFAULT '0',
  PRIMARY KEY (`cno`),
  KEY `fk_comment` (`tno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dap`
--

DROP TABLE IF EXISTS `dap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dap` (
  `dno` int(11) NOT NULL AUTO_INCREMENT,
  `cno` int(11) DEFAULT NULL,
  `content` varchar(500) NOT NULL,
  `ddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dwriter` varchar(45) NOT NULL,
  `pwd` text NOT NULL,
  PRIMARY KEY (`dno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dap`
--

LOCK TABLES `dap` WRITE;
/*!40000 ALTER TABLE `dap` DISABLE KEYS */;
/*!40000 ALTER TABLE `dap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `genreSeq` int(11) NOT NULL AUTO_INCREMENT,
  `genreName` varchar(45) NOT NULL,
  PRIMARY KEY (`genreSeq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series` (
  `wmno` int(11) NOT NULL AUTO_INCREMENT,
  `wmfile` varchar(50) NOT NULL,
  `wmgenre` varchar(45) NOT NULL,
  `wmtitle` varchar(45) NOT NULL,
  `id` varchar(100) DEFAULT NULL,
  `wmthumbnail` varchar(50) DEFAULT NULL,
  `wmwriter` varchar(45) NOT NULL,
  `wmcontent` varchar(500) NOT NULL,
  `wridate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` timestamp NULL DEFAULT NULL,
  `viewCount` int(11) DEFAULT '0',
  `commentCount` int(11) DEFAULT '0',
  `kinds` varchar(45) DEFAULT NULL,
  `free_open` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wmno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series`
--

LOCK TABLES `series` WRITE;
/*!40000 ALTER TABLE `series` DISABLE KEYS */;
/*!40000 ALTER TABLE `series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titlejoingenre`
--

DROP TABLE IF EXISTS `titlejoingenre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titlejoingenre` (
  `toonivieTitleSeq` int(11) NOT NULL,
  `genreSeq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titlejoingenre`
--

LOCK TABLES `titlejoingenre` WRITE;
/*!40000 ALTER TABLE `titlejoingenre` DISABLE KEYS */;
/*!40000 ALTER TABLE `titlejoingenre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tooninfo`
--

DROP TABLE IF EXISTS `tooninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tooninfo` (
  `tno` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(50) NOT NULL,
  `genre` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `id` varchar(100) NOT NULL,
  `thumbnail` varchar(50) NOT NULL,
  `writer` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `wridate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` timestamp NULL DEFAULT NULL,
  `viewCount` int(11) DEFAULT '0',
  `commentCount` int(11) DEFAULT '0',
  `wmno` int(11) NOT NULL,
  `free_open` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`tno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tooninfo`
--

LOCK TABLES `tooninfo` WRITE;
/*!40000 ALTER TABLE `tooninfo` DISABLE KEYS */;
INSERT INTO `tooninfo` VALUES (4,'/resources/uploadfiles/20181206102512206.mp4','1','1','','/resources/thumbnail/20181206102512206.jpg','1','&lt;script&gt;alert(&#39;test&#39;)&lt;/script&gt;','2018-12-06 01:25:12',NULL,4,0,0,0),(5,'/resources/uploadfiles/20190213110629718.mp4','?? - ??/???','?? - ????? 1?','admin','/resources/thumbnail/20190213110629718.jpg','?? - ???','?? - ???? ?? ??','2019-02-13 02:06:29',NULL,0,0,0,0);
/*!40000 ALTER TABLE `tooninfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tooniviecontents`
--

DROP TABLE IF EXISTS `tooniviecontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tooniviecontents` (
  `toonivieContentsSeq` int(11) NOT NULL AUTO_INCREMENT,
  `toonivieTitleSeq` int(11) NOT NULL,
  `thumbnailUrl` text NOT NULL,
  `writerWord` text,
  PRIMARY KEY (`toonivieContentsSeq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tooniviecontents`
--

LOCK TABLES `tooniviecontents` WRITE;
/*!40000 ALTER TABLE `tooniviecontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tooniviecontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toonivietitle`
--

DROP TABLE IF EXISTS `toonivietitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toonivietitle` (
  `toonivieTitleSeq` int(11) NOT NULL AUTO_INCREMENT,
  `titleName` varchar(45) NOT NULL,
  `userSeq` int(11) NOT NULL,
  `registrationDate` varchar(45) NOT NULL,
  `imgUrl` text NOT NULL,
  `introText` text,
  PRIMARY KEY (`toonivieTitleSeq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toonivietitle`
--

LOCK TABLES `toonivietitle` WRITE;
/*!40000 ALTER TABLE `toonivietitle` DISABLE KEYS */;
/*!40000 ALTER TABLE `toonivietitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userSeq` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(45) NOT NULL,
  `pw` text NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `work` varchar(45) DEFAULT '없음',
  `tel` varchar(100) DEFAULT '없음',
  `matching` varchar(45) DEFAULT NULL,
  `icon` varchar(60) DEFAULT NULL,
  `joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `conndate` timestamp NULL DEFAULT NULL,
  `logerror` int(11) DEFAULT '0',
  `is_account_lock` varchar(45) DEFAULT 'N',
  PRIMARY KEY (`userSeq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','1000:082bafb4f1089b27a4d7f618b4bf70522ad9a7e524c79c1e:edc3d44a3f5db93deaf4a4e82e3d3947d1d84c39a8417e25','admin','admin@ideaconcert.com','ROLE_ADMIN',NULL,NULL,'null-null-null',NULL,'/resources/profile/default/profile.jpg','2018-10-01 07:45:09','2019-08-05 08:40:37',0,'N');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vieworder`
--

DROP TABLE IF EXISTS `vieworder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vieworder` (
  `vno` int(11) NOT NULL AUTO_INCREMENT,
  `viewId` varchar(45) NOT NULL,
  `viewContentNo` varchar(45) NOT NULL,
  PRIMARY KEY (`vno`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vieworder`
--

LOCK TABLES `vieworder` WRITE;
/*!40000 ALTER TABLE `vieworder` DISABLE KEYS */;
INSERT INTO `vieworder` VALUES (1,'admin','4'),(2,'admin','4');
/*!40000 ALTER TABLE `vieworder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visit`
--

DROP TABLE IF EXISTS `visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visit` (
  `vno` int(11) NOT NULL AUTO_INCREMENT,
  `vip` varchar(45) DEFAULT NULL,
  `vtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`vno`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visit`
--

LOCK TABLES `visit` WRITE;
/*!40000 ALTER TABLE `visit` DISABLE KEYS */;
INSERT INTO `visit` VALUES (106,'0:0:0:0:0:0:0:1','2019-04-01 08:09:38'),(107,'0:0:0:0:0:0:0:1','2019-04-02 00:13:35'),(108,'0:0:0:0:0:0:0:1','2019-04-03 00:25:50'),(109,'127.0.0.1','2019-04-03 07:01:19'),(110,'0:0:0:0:0:0:0:1','2019-04-05 01:11:03'),(111,'0:0:0:0:0:0:0:1','2019-05-29 00:59:27'),(112,'0:0:0:0:0:0:0:1','2019-08-05 07:45:49'),(113,'127.0.0.1','2019-08-05 08:32:24');
/*!40000 ALTER TABLE `visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webtooncomment`
--

DROP TABLE IF EXISTS `webtooncomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webtooncomment` (
  `cno` int(11) NOT NULL AUTO_INCREMENT,
  `wtno` int(11) DEFAULT NULL,
  `comment` varchar(45) NOT NULL,
  `pw` text NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wtDapCount` int(11) DEFAULT '0',
  PRIMARY KEY (`cno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webtooncomment`
--

LOCK TABLES `webtooncomment` WRITE;
/*!40000 ALTER TABLE `webtooncomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `webtooncomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webtoondap`
--

DROP TABLE IF EXISTS `webtoondap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webtoondap` (
  `wdno` int(11) NOT NULL AUTO_INCREMENT,
  `cno` int(11) DEFAULT NULL,
  `wtcontent` varchar(500) NOT NULL,
  `wtddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wtdwriter` varchar(45) NOT NULL,
  `wtpwd` text NOT NULL,
  PRIMARY KEY (`wdno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webtoondap`
--

LOCK TABLES `webtoondap` WRITE;
/*!40000 ALTER TABLE `webtoondap` DISABLE KEYS */;
/*!40000 ALTER TABLE `webtoondap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webtooninfo`
--

DROP TABLE IF EXISTS `webtooninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webtooninfo` (
  `wtno` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(500) NOT NULL,
  `wtgenre` varchar(45) DEFAULT NULL,
  `wtname` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `thumbnail` varchar(300) DEFAULT NULL,
  `wtwriter` varchar(45) DEFAULT NULL,
  `wtcontent` varchar(500) DEFAULT NULL,
  `wtwridate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` timestamp NULL DEFAULT NULL,
  `wtviewCount` int(11) DEFAULT '0',
  `wtcommCount` int(11) DEFAULT '0',
  `wmno` int(11) DEFAULT '0',
  `free_open` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wtno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webtooninfo`
--

LOCK TABLES `webtooninfo` WRITE;
/*!40000 ALTER TABLE `webtooninfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `webtooninfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-06 13:36:40
