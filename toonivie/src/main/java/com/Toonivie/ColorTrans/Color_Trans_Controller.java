package com.Toonivie.ColorTrans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Color_Trans_Controller {
	
	@RequestMapping(value ="/color_trans/get_color_palette", method=RequestMethod.POST)
	@ResponseBody
	public ArrayList<String> get_color_palette(HttpServletRequest request) throws IOException {
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		String filename = request.getParameter("filename");
		int color_count = Integer.parseInt(request.getParameter("color_count"));
		return color_Trans.init_color_getpalette(filename, color_count, request);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/color_trans/color_change", method=RequestMethod.POST)
	@ResponseBody
	public String color_change(HttpServletRequest request/*, @RequestParam("color_list[]") ArrayList<String> color_list, @RequestParam("color_list_af[]") ArrayList<String> color_list_af
			, @RequestParam("color_list_bf[]") ArrayList<String> color_list_bf*/) throws IOException {
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		String info = request.getParameter("info");
		/*String filepath = request.getParameter("filepath");*/
		//System.out.println(color_list.toString());
		//System.out.println(color_list_af.toString());
		//System.out.println(color_list_bf.toString());
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(info);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
		//System.out.println(jsonObj.toString());
		color_Trans.init_color_change((String)jsonObj.get("filepath"), (ArrayList<String>)jsonObj.get("color_list"), (ArrayList<String>)jsonObj.get("color_list_bf"), (ArrayList<String>)jsonObj.get("color_list_af"), request.getServletContext().getRealPath(""));
		return "success";
		
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/color_trans/color_change_frame", method=RequestMethod.POST)
	@ResponseBody
	public String color_change_frame(HttpServletRequest request) throws IOException {
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String info = request.getParameter("info");
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(info);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
		
		return color_Trans.frame_color_change(rootPath,(String) jsonObj.get("video_url"), (ArrayList<String>)jsonObj.get("color_list"), (ArrayList<String>)jsonObj.get("color_list_bf"), (ArrayList<String>)jsonObj.get("color_list_af"), request);
		
	}
	@RequestMapping(value ="/color_trans/get_color_palette_roi", method=RequestMethod.POST)
	@ResponseBody
	public ArrayList<String> get_color_palette_roi(HttpServletRequest request) throws IOException {
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		String filename = request.getParameter("filename");
		int GbeX = Integer.parseInt(request.getParameter("GbeX"));
		int GbeY = Integer.parseInt(request.getParameter("GbeY"));
		int GafX = Integer.parseInt(request.getParameter("GafX"));
		int GafY = Integer.parseInt(request.getParameter("GafY"));
		String inoutFlag = request.getParameter("inoutFlag");
		inoutFlag = (inoutFlag.equals("true"))? "0" : "1";
		if(GbeX>GafX){
			int temp = GafX;
			GafX=GbeX;
			GbeX=temp;
		}
		if(GbeY>GafY){
			int tempY = GafY;
			GafY=GbeY;
			GbeY=tempY;
		}
		int color_count = Integer.parseInt(request.getParameter("color_count"));
		return color_Trans.init_color_getpalette_roi(filename, color_count, GbeX, GbeY, GafX, GafY, inoutFlag, request);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/color_trans/color_change_roi", method=RequestMethod.POST)
	@ResponseBody
	public String color_change_roi(HttpServletRequest request) throws IOException {
		Color_Trans_Module color_Trans_roi = new Color_Trans_Module();
		String info = request.getParameter("info");
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(info);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
		//System.out.println(jsonObj.toString());
		String inoutFlag = ((boolean)jsonObj.get("inoutFlag"))? "0" : "1";
		color_Trans_roi.init_color_change_roi((String)jsonObj.get("filepath"), (ArrayList<String>)jsonObj.get("color_list"), (ArrayList<String>)jsonObj.get("color_list_bf"), (ArrayList<String>)jsonObj.get("color_list_af"),
								(int)(long)jsonObj.get("GbeX"), (int)(long)jsonObj.get("GbeY"), (int)(long)jsonObj.get("GafX"), (int)(long)jsonObj.get("GafY"), inoutFlag, request);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/color_trans/color_merge", method=RequestMethod.POST)
	@ResponseBody
	public List<String> color_merge(HttpServletRequest request) throws IOException {
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		String info = request.getParameter("info");
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(info);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
		
		return color_Trans.color_merge((String)jsonObj.get("filepath"), (ArrayList<String>)jsonObj.get("color_list"), (ArrayList<String>)jsonObj.get("color_list_bf"), (ArrayList<String>)jsonObj.get("color_list_af"),
				 request);
	}
	
	
	@RequestMapping("/test/test")
	public String testtest(HttpServletRequest request, HttpSession session) throws IOException {
		return "home/test";
	}

}
