package com.Toonivie.ColorTrans;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import com.Toonivie.Util.CreateFileUtils;


public class Color_Trans_Module {
	
	private StringBuffer buffer;
	
	private BufferedReader bufferedReader;
	private StringBuffer readBuffer;
	
	public ArrayList<String> Color_pallet(List<String> cmd) {
		Process process = null;
		ArrayList<String> result = new ArrayList<String>();

		try {
			process = new ProcessBuilder(cmd).start();
			int processOutput = process.waitFor();
	        if(processOutput == 0){
				bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				
				String line = bufferedReader.readLine();

				do {
					line = bufferedReader.readLine();
					if(line.contains("#")) {
						result.add(line);
					}
				}while(!line.equals("success"));
				return result;
			}
		}catch (Exception e) {
			e.printStackTrace();
			process.destroy();
			return result;
		}
		return result;
	}
	
	public void Color_change(List<String> cmd) {
		Process process = null;

		try {
			process = new ProcessBuilder(cmd).start();
			process.waitFor();
		}catch (Exception e) {
			e.printStackTrace();
			process.destroy();
		}
	}
	
	public ArrayList<String> init_color_getpalette(String filepath, int palette_count, HttpServletRequest request) {
		String rootpath = request.getServletContext().getRealPath("resources/cvimgs/");
		List<String> command_list = new ArrayList<String>();
		filepath = rootpath + filepath;
		//String command = inputCommand(" " + filepath +  " " + palette_count, request.getServletContext().getRealPath(""), "Full");
		command_list.add(request.getServletContext().getRealPath("") + "resources\\lib\\colorTrans\\"+"WebtoonFull"+".exe");
		command_list.add(filepath);
		command_list.add(String.valueOf(palette_count));
		return Color_pallet(command_list);
	}
	
	public void init_color_change(String filepath, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, String rootpath) {
		List<String> command_list = new ArrayList<String>();
		filepath = rootpath+"resources/cvimgs/" + filepath;
		//기본명령어
		command_list.add(rootpath+"resources\\lib\\colorTrans\\"+"WebtoonFull"+".exe");
//		String command = " " + filepath/*이전이미지*/ + " " + filepath/*이후이미지*/ +  " " + color_list.size();
		command_list.add("0");
		command_list.add(filepath);/*이전이미지*/
		command_list.add(filepath);/*이후이미지*/
		command_list.add(String.valueOf(color_list.size()));
		//팔레트 이미지
		for(int i=0;i<color_list.size();i++) {
			command_list.add(color_list.get(i));
//			command = command + " " + color_list.get(i);
		}
		//변경될 색상 수
		command_list.add(String.valueOf(color_list_bf.size()));
//		command = command + " " + color_list_bf.size();
		//변경될 이전 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_bf.get(j));
//			command = command + " " + color_list_bf.get(j);
		}
		//변경될 이후 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_af.get(j));
//			command = command + " " + color_list_af.get(j);
		}
		Color_change(command_list);
	}
	
	public ArrayList<String> init_color_getpalette_roi(String filepath, int palette_count, int GbeX, int GbeY, int GafX, int GafY, String inoutFlag, HttpServletRequest request) {
		List<String> command_list = new ArrayList<String>();
		String rootpath = request.getServletContext().getRealPath("resources/cvimgs/");
		filepath = rootpath + filepath;
		command_list.add(request.getServletContext().getRealPath("") + "resources\\lib\\colorTrans\\"+"WebtoonROI2.exe");
		command_list.add(inoutFlag);
		command_list.add(filepath);
		command_list.add(String.valueOf(palette_count));
		command_list.add(String.valueOf(GbeX));
		command_list.add(String.valueOf(GbeY));
		command_list.add(String.valueOf(GafX));
		command_list.add(String.valueOf(GafY));
		//String command = inputCommand(" " + filepath +  " " + palette_count + " " + GbeX + " " + GbeY +" " + GafX + " " + GafY, request.getServletContext().getRealPath(""), "ROI");

		return Color_pallet(command_list);
	}
	
	public void init_color_change_roi(String filepath, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, int GbeX, int GbeY, int GafX, int GafY, String inoutFlag, HttpServletRequest request) {
		List<String> command_list = new ArrayList<String>();
		if(GbeX>GafX){
			int temp = GafX;
			GafX=GbeX;
			GbeX=temp;
		}
		if(GbeY>GafY){
			int tempY = GafY;
			GafY=GbeY;
			GbeY=tempY;
		}
		String rootpath = request.getServletContext().getRealPath("resources/cvimgs/");
		filepath = rootpath + filepath;
		command_list.add(request.getServletContext().getRealPath("") + "resources\\lib\\colorTrans\\"+"WebtoonROI2.exe");
		command_list.add("0");
		command_list.add(inoutFlag);
		command_list.add(filepath);/*이전이미지*/
		command_list.add(filepath);/*이후이미지*/
		command_list.add(String.valueOf(GbeX));/*이전좌표*/
		command_list.add(String.valueOf(GbeY));/*이전좌표*/
		command_list.add(String.valueOf(GafX));/*이후좌효*/
		command_list.add(String.valueOf(GafY));/*이후좌효*/
		command_list.add(String.valueOf(color_list.size()));/*색상수*/
		/*//기본명령어
		String command = " " + filepath이전이미지 + " " + filepath이후이미지;
		//이전좌표
		command = command + " " + GbeX + " " + GbeY;
		//이후좌효
		command = command + " " + GafX + " " + GafY;
		//색상수
		command = command +  " " + color_list.size();*/
		//팔레트 이미지
		for(int i=0;i<color_list.size();i++) {
			command_list.add(color_list.get(i));
			//command = command + " " + color_list.get(i);
		}
		//변경될 색상 수
		command_list.add(String.valueOf(color_list_bf.size()));
//		command = command + " " + color_list_bf.size();
		//변경될 이전 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_bf.get(j));
//			command = command + " " + color_list_bf.get(j);
		}
		//변경될 이후 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_af.get(j));
//			command = command + " " + color_list_af.get(j);
		}
		Color_change(command_list);
	}
/*
	public String frame_color_change(String rootPath, String video_name, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, HttpServletRequest request) {
		long startclock = System.currentTimeMillis();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		VideoCapture input_video = new VideoCapture(rootPath + "resources/cvMov/" + video_name);
		VideoWriter videoWriter  = new VideoWriter();
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		CreateFileUtils cfu = new CreateFileUtils();
		Mat frame = new Mat();
		input_video.read(frame);
		
		String today = cfu.getToday(0);

		
        File file = new File(rootPath+"resources/cvMov/colorchange_"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
		videoWriter.open(rootPath+"resources/cvMov/colorchange_"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),20,frame.size());
		for(int i=0;i<input_video.get(7);i++) {
			if(input_video.isOpened()) {
				input_video.read(frame);
				Imgcodecs.imwrite(rootPath + "resources/cvimgs/color_changing.png", frame);
				color_Trans.init_color_change("color_changing.png", color_list, color_list_bf, color_list_af, rootPath);
				frame = Imgcodecs.imread(rootPath + "resources/cvimgs/color_changing.png");
				videoWriter.write(frame);
			}
			System.out.println(i);
		}
		videoWriter.release();
		input_video.release();
		
		long endclock = System.currentTimeMillis();
		long execlock = endclock - startclock;

		System.out.println("걸린 시간: " + execlock + " 밀리초");
		return "/resources/cvMov/colorchange_"+today+".mp4";
	}
*/

	public String frame_color_change(String rootPath, String video_name, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, HttpServletRequest request) {
		long startclock = System.currentTimeMillis();
		System.out.println("------------- 프로그램 시작 시간: " + startclock + " 밀리초");
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		VideoCapture input_video = new VideoCapture(rootPath + "resources/cvMov/" + video_name);
		
		Mat frame = new Mat();
		int framecount = (int) input_video.get(Videoio.CAP_PROP_FRAME_COUNT);
		//int framePS = (int) input_video.get(Videoio.CAP_PROP_FPS);
		int framePS = 8;
		int frameheight = (int) input_video.get(Videoio.CAP_PROP_FRAME_HEIGHT);
		int framewidth = (int) input_video.get(Videoio.CAP_PROP_FRAME_WIDTH);
		
		int threadSize = 0;
		if(framecount % (int) input_video.get(Videoio.CAP_PROP_FPS) == 0) {
			threadSize = framecount / framePS;
		}else {
			threadSize = (framecount / framePS) + 1;
		}	 
		
		CreateFileUtils cfu = new CreateFileUtils();
		String today = cfu.getToday(0);
		
		File fileDir = new File(rootPath+"resources/cvimgs/"+today);
		if(fileDir.exists()){
			fileDir.delete();
        }else {
        	fileDir.mkdirs();
        	System.out.println("---------------- 동영상 -> 프레임 분리 시작: ");
        	for(int i=0;i<framecount;i++) {
    			if(input_video.isOpened()) {
    				input_video.read(frame);
    				Imgcodecs.imwrite(rootPath + "resources/cvimgs/"+today + "/"+i+".png", frame);
    			}
    			//System.out.println(i);
    		}
    		input_video.release();
    		long endclock = System.currentTimeMillis();
    		long execlock = endclock - startclock;
    		System.out.println("---------------- 동영상 -> 프레임 분리 종료: " + execlock + " 밀리초");
        }
		
		Runnable [] Obj1 = new Color_Trans_Thread[threadSize]; 
		Thread [] th1 = new Thread[threadSize];
		// Runnable 인터페이스 객체생성
		for(int i=0; i<threadSize; i++) {
			Obj1[i] = new Color_Trans_Thread(rootPath,color_list,color_list_bf,color_list_af,today,framecount,framePS,i);
			th1[i] = new Thread(Obj1[i]);
			th1[i].start();
		}
		for(int i=0; i<threadSize; i++) {
			try {
				th1[i].join();
			} catch (InterruptedException e) {
				fileDir.delete();
				e.printStackTrace();
			}
		}
		
        File file = new File(rootPath+"resources/cvMov/colorchange_"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        
        VideoWriter videoWriter  = new VideoWriter();
//        frame.create(framewidth, frameheight, -1);
		videoWriter.open(rootPath+"resources/cvMov/colorchange_"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),20, new Size(framewidth,frameheight));
		for(int i=0;i<framecount;i++) {
			frame = Imgcodecs.imread(rootPath + "resources/cvimgs/"+today+"/"+i+".png");
			videoWriter.write(frame);		
		}
		fileDir.delete();
		videoWriter.release();


		long endclock = System.currentTimeMillis();
		long execlock = endclock - startclock;

		System.out.println("------------- 프로그램 종료 시간: " + execlock + " 밀리초");
		return "/resources/cvMov/colorchange_"+today+".mp4";
	}

	public List<String> color_merge(String filepath, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, HttpServletRequest request) {
		String rootpath = request.getServletContext().getRealPath("resources/cvimgs/");
		List<String> command_list = new ArrayList<String>();
		filepath = rootpath + filepath;
		//기본명령어
		command_list.add(request.getServletContext().getRealPath("")+"resources\\lib\\colorTrans\\"+"WebtoonROI2.exe");
		command_list.add("1");// merge param
		command_list.add(String.valueOf(color_list.size()));
		//팔레트 이미지
		for(int i=0;i<color_list.size();i++) {
			command_list.add(color_list.get(i));
		}
		//변경될 색상 수
		command_list.add(String.valueOf(color_list_bf.size())); //병합 횟수 / 색상 갯수
		command_list.add("2"); //병합 횟수 / 색상 갯수
		//변경될 이전 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_bf.get(j));
//			command = command + " " + color_list_bf.get(j);
		}
		//변경될 이후 색상
		for(int j=0;j<color_list_bf.size();j++) {
			command_list.add(color_list_af.get(j));
//			command = command + " " + color_list_af.get(j);
		}
		System.out.println("@"+command_list);
		List<String> result = new ArrayList<String>();
		result = Color_pallet(command_list);
		System.out.println("!"+result);
		return result;
	}
}