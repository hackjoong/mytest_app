package com.Toonivie.ColorTrans;

import java.util.ArrayList;

class Color_Trans_Thread implements Runnable {

	int framecount = 0;
	int startNum = 0;
	int endNum = 0;
	String rootPath = null;
	String today = null;
	ArrayList<String> color_list;
	ArrayList<String> color_list_bf;
	ArrayList<String> color_list_af;
	public Color_Trans_Thread(String rootPath, ArrayList<String> color_list, ArrayList<String> color_list_bf, ArrayList<String> color_list_af, String today, int framecount,int framePS, int exNum) {
		// TODO Auto-generated constructor stub
		this.framecount = framecount;
		if(exNum == 0) {
			this.startNum = exNum;
			this.endNum = framePS;
		}else if(framePS * exNum < framecount) {
				this.startNum = (framePS * exNum);
				this.endNum = (framePS * exNum) + framePS;
		}else {
			this.startNum = (framePS * (exNum-1));
			this.endNum = framecount;
		}
		this.color_list = color_list;
		this.color_list_bf = color_list_bf;
		this.color_list_af = color_list_af;
		this.today = today;
		this.rootPath = rootPath;
	}

	public synchronized void run() {
		exeColorTrans();
	}

	private synchronized void exeColorTrans() {
		// TODO Auto-generated method stub
		Color_Trans_Module color_Trans = new Color_Trans_Module();
		for(int i=startNum;i<endNum;i++) {
			color_Trans.init_color_change(today+"/"+i+".png", color_list, color_list_bf, color_list_af, rootPath);	
		}
	}
}
