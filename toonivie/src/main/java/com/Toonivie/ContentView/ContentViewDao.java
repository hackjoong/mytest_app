package com.Toonivie.ContentView;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContentViewDao {
	
	public void increaseViewCount(int tno);
	
	public void increaseViewCountSeries(int wmno);

}
