package com.Toonivie.ContentView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContentViewService {
	
	@Autowired
	ContentViewDao dao;
	public void increaseViewCount(int tno){
		dao.increaseViewCount(tno);
	}
}
