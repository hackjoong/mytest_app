package com.Toonivie.Member;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/member")
public class MemberController {
	
	@Autowired
	MemberService service;
	
	@RequestMapping("/list")
	public String MemberList(Model model) throws Exception {
		model.addAttribute("MemberList",service.MemberList());
		model.addAttribute("countMember",service.MemberCount());
		return "adminPage/memberList";
	}
	
	@RequestMapping("/list_en")
	public String MemberList_en(Model model) throws Exception {
		model.addAttribute("MemberList_en",service.MemberList());
		model.addAttribute("countMember_en",service.MemberCount());
		return "adminPage/en/memberList_en";
	}
	
	@RequestMapping("/delete/{userSeq}")
	@ResponseBody
	public int MemberDelete(@PathVariable int userSeq, HttpServletRequest request) throws Exception {
		String id = request.getParameter("id");
		service.toonDelete(id);
		return service.MemberDelete(userSeq);
	}
	

	
}
