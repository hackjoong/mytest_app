package com.Toonivie.Member;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.UserVo;
import com.Toonivie.Vo.VisitVo;

@Mapper
public interface MemberDao {
	
	//멤버 목록
	public List<UserVo> MemberList();
	
	//멤버 삭제
	public int MemberDelete(int userSeq) throws Exception;
	
	//탈퇴작가 처리
	public int toonIdDelete(String id);
	
	public int MemberCount() throws Exception;
	
	public int MemberUpdate(UserVo vo);
	
	public int MemberUpdate_2(UserVo vo);
	
	public List<UserVo> listPage(int page) throws Exception;
	
	public int getTodayIp(String ip);
	
	public void insertIp(VisitVo vo);

}
