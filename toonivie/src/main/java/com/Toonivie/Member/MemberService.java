package com.Toonivie.Member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Signup.SignupValidate;
import com.Toonivie.Vo.UserVo;

@Service
public class MemberService {
	@Autowired
	MemberDao dao;
	@Autowired
	SignupValidate signupValidate;
	
	//멤버 목록
	public List<UserVo> MemberList(){
		return dao.MemberList();
	}
	
	public int MemberDelete(int userSeq) throws Exception {
		return dao.MemberDelete(userSeq);
	}
	public int toonDelete(String id)  {
		return dao.toonIdDelete(id);
	}
	
	public int MemberCount() throws Exception{
		return dao.MemberCount();
		
	}
	
	public int MemberUpdate(int userseq, String id, String email ,String name, String pw,String role){
		pw=signupValidate.hashPw(pw);
		UserVo vo = new UserVo();
		vo.setUserSeq(userseq);
		vo.setId(id);
		vo.setEmail(email);
		vo.setName(name);
		vo.setPw(pw);
		vo.setRole(role);
		
		return dao.MemberUpdate(vo);
	}
	
	public int MemberUpdate_2(int userseq, String id, String email ,String name, String role){

		UserVo vo = new UserVo();
		vo.setUserSeq(userseq);
		vo.setId(id);
		vo.setEmail(email);
		vo.setName(name);
		vo.setRole(role);
		
		return dao.MemberUpdate_2(vo);
	}
	

}
