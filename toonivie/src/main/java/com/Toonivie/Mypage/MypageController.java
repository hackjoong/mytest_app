package com.Toonivie.Mypage;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Signup.SignupDao;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.UserVo;
import com.Toonivie.ffmpeg.ffmpegConverter_thumbnail;
import com.Toonivie.uploadWebtoon.UploadWebtoonService;

@Controller
@RequestMapping("/members")
public class MypageController {

	@Autowired
	MypageService mypageservice;
	@Autowired
	UploadWebtoonService upservice;
	@Autowired
	HttpServletRequest request;
	@Autowired
	SignupDao userDao;
	@Autowired
	HttpSession session;
	@Autowired
	MultipartHttpServletRequest req;

	@RequestMapping("/myPage")
	public String MyPage(Model model) {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String id = currentUser.getId();
			model.addAttribute("myInfo", mypageservice.myInfo(id));
			model.addAttribute("myToonInfo", mypageservice.myToonInfo(id));
		} else {
			System.out.println("currentUser is null");
		}
		return "mypage/mypage";
	}

	@RequestMapping("/toon/list")
	public String MyToonAll(Model model) {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		model.addAttribute("myToonInfoAll", mypageservice.myTooninfoAll(id));
		return "mypage/toon/toonList";
	}

	@RequestMapping("/dropOut")
	public String DropOut(Model model) {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String id = currentUser.getId();
			model.addAttribute("myInfo2", mypageservice.myInfo(id));
		}
		return "mypage/out/dropOut";
	}

	@RequestMapping("/delete/{id}")
	public String MembersDelete() {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		String iconValue = currentUser.getIcon();
		mypageservice.MembersDelete(id);
		mypageservice.toonIdDelete(id);

		String rootPath = System.getProperty("user.dir")+"/src/main/webapp";		
		if(!iconValue.equals("/resources/profile/default/profile.jpg")) {
			File fileinfo = new File(rootPath+ iconValue);
			System.out.println(fileinfo);

			if (fileinfo.exists()) {
				fileinfo.delete();
			}
		}else if(iconValue.equals("/resources/profile/default/profile.jpg")) {
		}

		session.invalidate();
		return "redirect:/";
	}

	@RequestMapping("/modifyInfo")
	@ResponseBody
	public int mydifyInfo(HttpServletRequest request) {
		String id = request.getParameter("id");
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String nickname = request.getParameter("nickname");
		String work = request.getParameter("work");
		String tel = request.getParameter("tel");
		String matching = request.getParameter("matching");
		String writer = request.getParameter("nickname");
		
		mypageservice.toonNickUpdate(writer,id);
		int returnValue = mypageservice.MembersUpdate_nopw(id, email, name, nickname, tel, work, matching);
		if (returnValue == 1) {
			return returnValue;
		} else {
			return returnValue;
		}
	}
	
	@RequestMapping("/toonChange")
	@ResponseBody
	public int modifyToon(HttpServletRequest request, MultipartHttpServletRequest req) {
		String changed = request.getParameter("movieChanged");
		System.out.println("changed: " + changed); // on-null
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		
		int tno = Integer.parseInt(request.getParameter("tno"));
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		String id = currentUser.getId();
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		

		if (changed == null) { // change!
			MultipartFile upfile = req.getFile("upfile");
			// file convert
			CreateFileUtils cfu = new CreateFileUtils();
			String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); // file naming
			cfu.CreateFile(upfile, request, "/resources/uploadfiles/", filename);
			System.out.println(filename);
			
			//ffmpeg add by yoo_dh0416, 2017-11-28
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String cutSecond = "00:00:10";
			int idx = filename.indexOf(".");
			String outputFileName = filename.substring(0, idx);
			ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
			String outputName = ff.convert();
			
			System.out.println(upfile.getOriginalFilename());

			String thumbnail = "/resources/thumbnail/"+outputFileName+".jpg";
			//ffmpeg end
			
			String file = "/resources/uploadfiles/" + filename;
			int returnValue = mypageservice.UpdateTooninfo(tno, file, thumbnail, genre, title, id, writer, content);
			System.out.println(returnValue);
			if (returnValue == 1) {
				return returnValue;
			} else {
				return returnValue;
			}
		} else { // no changed
			int returnValue2 = mypageservice.UpdateTooninfo_noFile(tno, genre, title, id, writer, content);
			if (returnValue2 == 1) {
				return returnValue2;
			} else {
				return returnValue2;
			}

		}
	}

	@RequestMapping(value="/toonUpload", method = RequestMethod.POST)
	@ResponseBody
	public int upload_tooniviefile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		MultipartFile upfile = req.getFile("upfile");
		// �떒�씪 �뙆�씪�씪 寃쎌슦 html�쓽 name�뿉 �꽕�젙�맂 �씠由꾩쑝濡� �뙆�씪�쓣 媛��졇�삱 �닔 �엳�떎.
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); // �뙆�씪紐� :
																												// �뾽濡쒕뱶
																												// �떆媛꾩쑝濡�
		cfu.CreateFile(upfile, request, "/resources/uploadfiles/", filename);
		// resultMap.put("resultUrl",filename);
		
		//�뜽�꽕�씪 異붿텧 湲곕뒫 異붽� add by yoo_dh0416, 2017-11-28
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String cutSecond = "00:00:10";
		int idx = filename.indexOf(".");
		String outputFileName = filename.substring(0, idx);
		ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
		String outputName = ff.convert();
		
		System.out.println(upfile.getOriginalFilename());

		String thumbnail = "/resources/thumbnail/"+outputFileName+".jpg";
		//�뜽�꽕�씪 異붿텧湲곕뒫 END
		
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String file = "/resources/uploadfiles/" + filename; // request.getParameter("upfile");
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		String id = currentUser.getId();
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		int result = mypageservice.InputTooninfo(file,thumbnail, genre, title, id, writer, content);
		return result;
	}

	@RequestMapping("/remove/{tno}")
	@ResponseBody
	public int ToonDelete(@PathVariable int tno) throws Exception {
		int returnValue = mypageservice.ToonDelete(tno);
		if(returnValue==1) {
			String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
			
			String filename = request.getParameter("file_name");
			String thumbnail = request.getParameter("thumbnail");
			
			File fileinfo = new File(rootPath+ filename);
			File thumbinfo = new File(rootPath+thumbnail);
			if (fileinfo.exists()) {
				fileinfo.delete();
			}
			if (thumbinfo.exists()) {
				thumbinfo.delete();
			}
			return returnValue;
		}else {
			return returnValue;
		}
		
	}

	@RequestMapping("/modifyPw")
	@ResponseBody
	public int modifyPw(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		String pw = request.getParameter("pwd");
		int returnValue = mypageservice.UpdatePw(id, pw);

		if (returnValue == 1) {
			session.invalidate();
			return 1;
		} else {
			return 0;
		}
	}
	
	
	@RequestMapping(value="/iconUpload", method=RequestMethod.POST)
	@ResponseBody
	public int modifyIcon(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)throws Exception{
		MultipartFile file = req.getFile("upfile2");
		// �떒�씪 �뙆�씪�씪 寃쎌슦 html�쓽 name�뿉 �꽕�젙�맂 �씠由꾩쑝濡� �뙆�씪�쓣 媛��졇�삱 �닔 �엳�떎.
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(file.getOriginalFilename()); // �뙆�씪紐�
																												// :
		cfu.CreateFile(file, request, "/resources/profile/", filename);

		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		
		
		String icon = "/resources/profile/" + filename;
		
		
		
		int returnValue = mypageservice.modifyIcon(id, icon);

		if (returnValue == 1) {
			String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
			
			String fileNew = request.getParameter("iconValue");
			
			if(!fileNew.equals("/resources/profile/default/profile.jpg")) {
				File fileinfo = new File(rootPath+ fileNew);
				System.out.println(fileinfo);

				if (fileinfo.exists()) {
					fileinfo.delete();
				}
			}else if(fileNew.equals("/resources/profile/default/profile.jpg")) {
				System.out.println("noChange!!");
			}
			
			
			
			return 1;
		} else {
			return 0;
		}
	}	
}