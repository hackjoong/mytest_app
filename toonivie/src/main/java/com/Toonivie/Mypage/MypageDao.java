package com.Toonivie.Mypage;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;
import com.Toonivie.Vo.SeriesVo;
import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.UserVo;

@Mapper
public interface MypageDao {
	
	public List<UserVo> myInfo(String id);
	
	public List<ToonVo> myToonInfo(String id);
	
	public List<ToonVo> myToonInfoAll(String id);
	
	public List<SeriesVo> mySeriesInfoAll(String id);
	
	public void membersDelete(String id);
	
	public void toonIdDelete(String id);
	
	
	public int MembersUpdate_nopw(UserVo vo);
	
	public int toonNickUpdate(@Param("writer") String writer, @Param("id") String id);
	
	public int UpdateTooninfo(ToonVo toonVo);
	
	public int UpdateTooninfo_noFile(ToonVo toonVo);
	
	public int InsertTooninfo(ToonVo toonVo);
	
	public int ToonDelete(int tno) throws Exception;
	
	public int UpdatePw(UserVo vo);
	
	public int modifyIcon(UserVo vo);
	
	public int modifyCommPw(CommentVo cvo);
	
	public int modifyDapPw(DapVo dvo);
}