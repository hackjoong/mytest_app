package com.Toonivie.Mypage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Signup.SignupValidate;
import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;
import com.Toonivie.Vo.SeriesVo;
import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.UserVo;

@Service
public class MypageService {

	@Autowired
	MypageDao mypagedao;
	@Autowired
	SignupValidate signupValidate;

	public List<UserVo> myInfo(String id) {
		return mypagedao.myInfo(id);
	}

	public List<ToonVo> myToonInfo(String id) {
		return mypagedao.myToonInfo(id);
	}

	public List<ToonVo> myTooninfoAll(String id) {
		return mypagedao.myToonInfoAll(id);
	}
	public List<SeriesVo> mySeriesinfoAll(String id) {
		return mypagedao.mySeriesInfoAll(id);
	}

	public void MembersDelete(String id) {
		mypagedao.membersDelete(id);
	}
	
	public void toonIdDelete(String id) {
		mypagedao.toonIdDelete(id);
	}

	public int MembersUpdate_nopw(String id, String email, String name, String nickname, String tel, String work,
			String matching) {
		UserVo vo = new UserVo();

		vo.setId(id);
		vo.setEmail(email);
		vo.setName(name);
		vo.setNickname(nickname);
		vo.setWork(work);
		vo.setTel(tel);
		vo.setMatching(matching);
		return mypagedao.MembersUpdate_nopw(vo);

	}
	
	public int toonNickUpdate(String writer, String id) {
		ToonVo vo = new ToonVo();
		
		vo.setWriter(writer);
		vo.setId(id);
		return mypagedao.toonNickUpdate(writer,id);
	}

	public int UpdateTooninfo(int tno, String file, String thumbnail, String genre, String title, String id,
			String writer, String content) { // modify
		ToonVo vo = new ToonVo();
		vo.setTno(tno);
		vo.setFile(file);
		vo.setThumbnail(thumbnail);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setId(id);
		vo.setWriter(writer);
		vo.setContent(content);

		return mypagedao.UpdateTooninfo(vo);
	}

	public int UpdateTooninfo_noFile(int tno, String genre, String title, String id, String writer, String content) { // modify
		ToonVo vo = new ToonVo();
		vo.setTno(tno);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setId(id);
		vo.setWriter(writer);
		vo.setContent(content);

		return mypagedao.UpdateTooninfo_noFile(vo);
	}

	public int InputTooninfo(String file, String thumbnail, String genre, String title, String id, String writer,
			String content) { // insert
		ToonVo vo = new ToonVo();
		vo.setFile(file);
		vo.setThumbnail(thumbnail);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setId(id);
		vo.setWriter(writer);
		vo.setContent(content);

		return mypagedao.InsertTooninfo(vo);
	}

	public int ToonDelete(int tno) throws Exception {
		return mypagedao.ToonDelete(tno);
	}

	public int UpdatePw(String id, String pw) {
		String nohashpw= pw;
		//댓글 비밀번호 변경
		CommentVo cvo = new CommentVo();
		cvo.setNickname(id);
		cvo.setPw(nohashpw);
		mypagedao.modifyCommPw(cvo);
		
		//답글 비밀번호 변경
		DapVo dvo = new DapVo();
		dvo.setDwriter(id);
		dvo.setPwd(nohashpw);
		mypagedao.modifyDapPw(dvo);
		
		//비밀번호 변경
		UserVo vo = new UserVo();
		pw = signupValidate.hashPw(pw);
		vo.setId(id);
		vo.setPw(pw);
		return mypagedao.UpdatePw(vo);
	}
	
	public int modifyIcon(String id, String icon) {
		UserVo vo = new UserVo();
		vo.setId(id);
		vo.setIcon(icon);
		
		return mypagedao.modifyIcon(vo);
	}

}