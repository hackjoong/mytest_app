package com.Toonivie.OpenCv.AddText;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Util.CreateFileUtils;

@Controller
@RequestMapping("/addText")
public class AddTextController {
	@Autowired
	AddTextService addTextService;

	@RequestMapping(value="/Apply" ,method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> addTextApply(HttpServletRequest request) throws Exception{
		String str = request.getParameter("data");
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONArray others = (JSONArray)jsonObject.get("otherObj");
		String effectKind = jsonObject.get("effectKind").toString();
		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);

		JSONObject background = (JSONObject)jsonObject.get("background");
		JSONObject obj = (JSONObject)jsonObject.get("obj");

		String rootPath = request.getServletContext().getRealPath("/");
		return addTextService.applyObject(effectKind,effectTime,background,obj,rootPath,others, request);
	}

	//save
	@RequestMapping(value="/Save" ,method=RequestMethod.POST)
	@ResponseBody
	public String makePngFile(HttpServletRequest request) throws Exception {
		/**
		 * imgbase64 (imgbase64data:image/png;base64,iVBORw0KGgoAA head info)
		 * saveFilePath (save path)
		 * savename (save file name)
		 */
		CreateFileUtils createFileUtils = new CreateFileUtils();

    	File filepath = new File(request.getServletContext().getRealPath("resources/cvimgs/") + "subtitle/");
        if(!filepath.exists()){
        	filepath.mkdirs(); //디렉토리가 존재하지 않는다면 생성
        }

		String imgbase64 =  request.getParameter("imgUrl");
		/*String saveFilePath = "src/main/webapp/resources/cvimgs/subtitle";*/
		String rootPath = request.getServletContext().getRealPath("/");
		String saveFilePath = rootPath+"resources/cvimgs/subtitle/";
		String savename = createFileUtils.getToday(1);
		try {
			// create a buffered image
			BufferedImage image = null;

			String[] base64Arr = imgbase64.split(","); // image/png;base64, for remove this
			byte[] imageByte = Base64.decodeBase64(base64Arr[1]); // base64 to byte array

			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			// write the image to a file
			//savename = savename;
			File outputfile = new File(saveFilePath + savename + ".png");
			ImageIO.write(image, "png", outputfile); // create a file

		} catch (IOException e) {
			throw e;
		}
		return savename + ".png";
	}
}

