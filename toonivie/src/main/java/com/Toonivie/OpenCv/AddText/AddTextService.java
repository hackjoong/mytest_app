package com.Toonivie.OpenCv.AddText;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

import com.Toonivie.OpenCv.ApplyObject.ApplyObjectService;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.JNI;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObjectVo;

@Service
public class AddTextService { //add by shkwak, 2017-11-27
	static HttpServletRequest request_url;
	static final int frame = 20;
	static final String extType = ".mp4"; //add by shkwak, 2017-06-15
	Mat matrix = null;
	
	public Mat apply() throws Exception {
	  // Loading the OpenCV core library
	  System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	
	  // Reading the Image from the file and storing it in to a Matrix object
	  String file ="E:/OpenCV/chap8/input.jpg";
	  Mat matrix = Imgcodecs.imread(file);
	
	  // Adding Text
	  Imgproc.putText (
	     matrix,                          // Matrix obj of the image
	     "Ravivarma's Painting",          // Text to be added
	     new Point(10, 50),               // point
	     Core.FONT_HERSHEY_SIMPLEX ,      // front face
	     1,                               // front scale
	     new Scalar(0, 0, 0),             // Scalar object for color
	     4                                // Thickness
	  );
	  
	  // Encoding the image
	  MatOfByte matOfByte = new MatOfByte();
	  Imgcodecs.imencode(".jpg", matrix, matOfByte);
	
	  // Storing the encoded Mat in a byte array
	  byte[] byteArray = matOfByte.toArray();
	
	  // Displaying the image
	  InputStream in = new ByteArrayInputStream(byteArray);
	  BufferedImage bufImage = ImageIO.read(in);
	  this.matrix = matrix;
	
	  //Creating the Writable Image
	  //WritableImage writableImage = SwingFXUtils.toFXImage(bufImage, null);
	  return matrix; //writableImage;
   }
	
	public HashMap<String, Object> applyObject(String effectKind, int effectTime, JSONObject background, JSONObject obj, String rootPath, JSONArray others, HttpServletRequest request) {
		request_url = request;
		ApplyObjectService applyObjectService = new ApplyObjectService();
		applyObjectService.request_url = request;
		String rootPath_txt = request.getSession().getServletContext().getRealPath("resources/cvimgs/subtitle/");
		//define default value
		HashMap<String, Object> resultMap = new HashMap<>();
		System.out.println("ApplyText.java:31 - ApplyText() : " + effectKind);
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		MoveObjectVo objVo = applyObjectService.changeMoveObjectVo(obj, background);
		MoveObjectVo backgroundVo = applyObjectService.changeMoveBackgroundVo(background);
		List<MoveObjectVo> otherList = applyObjectService.makeOtherList(others, objVo,backgroundVo); 
		//String text_url = rootPath_txt + obj.get("text_url").toString();
		String font_size = obj.get("font_size").toString().replace("px", "");
		String font_txt = obj.get("font_txt").toString();
		String font_type = obj.get("font_type").toString();
		String font_color_r = obj.get("font_color_r").toString();
		String font_color_g = obj.get("font_color_g").toString();
		String font_color_b = obj.get("font_color_b").toString();
		String font_bold= obj.get("font_bold").toString();
		resultMap = applyText(effectTime, objVo, backgroundVo, rootPath, otherList, font_type, font_size, font_txt, font_color_r, font_color_g, font_color_b, font_bold);
		return resultMap; 
	}
	
	public HashMap<String, Object> applyText(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, List<MoveObjectVo> others, String font_type, String font_size,String font_txt,String font_color_r,String font_color_g,String font_color_b,String font_bold){
		JNI jni = new JNI(rootPath);
		ApplyObjectService applyObjectService = new ApplyObjectService();
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im2 = applyObjectService.setRotateBackground(background, others, rootPath);
		Mat im3 = im2.clone();
		String img_path = rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		Imgcodecs.imwrite(img_path, im2);
		//Mat im3 = new Mat(jni.init_puttext(img_path, im2.getNativeObjAddr(),  (rootPath+"resources\\lib\\PutText\\fonts\\"+font_type).replaceAll("\\\\","\\\\\\\\"), Integer.parseInt(font_size),  font_txt, (int)obj.getLeft(), (int)obj.getTop(), Integer.parseInt(font_color_r),  Integer.parseInt(font_color_g),  Integer.parseInt(font_color_b),  Integer.parseInt(font_bold)));
		jni.init_puttext(img_path, im2.getNativeObjAddr(),  (rootPath+"resources\\lib\\PutText\\fonts\\"+font_type).replaceAll("\\\\","\\\\\\\\"), (int) Math.round(Integer.parseInt(font_size)*1.3),  font_txt, (int)obj.getLeft(), (int)obj.getTop(), Integer.parseInt(font_color_r),  Integer.parseInt(font_color_g),  Integer.parseInt(font_color_b),  Integer.parseInt(font_bold));
		//Puttext puttext = new Puttext();
		//puttext.puttext_init(rootPath, img_path, font_type, font_size, font_txt, Float.toString(obj.getLeft()), Float.toString(obj.getTop()), font_color_r, font_color_g, font_color_b, font_bold);
		
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();
		//smi load
//		Mat object2 = Imgcodecs.imread(rootPath + "resources/cvimgs/subtitle" + today + ".png", -1);

		File file = new File(rootPath + "resources/cvMov/subtitle" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		/*videoWriter.open("src/main/webapp/resources/cvMov/subtitle" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());*/
		//유덕희 주석 처리_180402
		videoWriter.open(rootPath+"resources/cvMov/subtitle" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();

		Mat thumbNail = null;
		for (int i = 0; i < frame * effectTime; i++) {
			Mat im = Imgcodecs.imread(img_path);
			//im3.release();
			//im = putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im, object);
			
			  // Adding Text
/*			  Imgproc.putText (
					  im,                          // Matrix obj of the image
					  "hello우",          // Text to be added
			     new Point(obj.getLeft(), obj.getTop()),               // point
			     Core.FONT_HERSHEY_SIMPLEX ,      // front face
			     3,                               // front scale
			     new Scalar(0, 0, 0),             // Scalar object for color
			     10                                // Thickness
			  );*/
			  
			
			//List<Mat> objectMatList = new ArrayList<>();
			//Core.split(object, objectMatList);
			//Imgproc.cvtColor(object, object, Imgproc.COLOR_BGRA2BGR);
			
			
/*			Rect rect = new Rect((int) obj.getLeft(), (int) obj.getTop(), object.cols(), object.rows());
			Mat imRoi = object;
			Mat crop = im.submat(rect);
			imRoi.copyTo(crop);
			Imgcodecs.imwrite("imRoi.png", imRoi);
			Imgcodecs.imwrite("crop.png", crop);
			Imgcodecs.imwrite("object.png", object);
			Imgcodecs.imwrite("im.png", im);*/
			
			
//			imRoi.copyTo(crop, im);
			
			//Imgcodecs.imwrite("src/main/webapp/resources/cvimgs/TETETETETETSET1.png", crop);
			//Imgcodecs.imwrite("src/main/webapp/resources/cvimgs/TETETETETETSET2.png", imRoi);
			videoWriter.write(im);
			if(i==(frame * effectTime)/2){
				thumbNail=im.clone();
			}
		}
		Imgcodecs.imwrite(img_path, im3);
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail"+today+".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl","/resources/cvMov/subtitle" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		//resultMap.put("videoSize", im2.size()); //add by shkwak, 2017-06-19
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im2.size().width);
		return resultMap;

	}

}
