package com.Toonivie.OpenCv.ApplyObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Util.CreateFileUtils;

@Controller
@RequestMapping("/applyObject")
public class ApplyObjectController {
	@Autowired
	ApplyObjectService applyObjectService;
	
	static final int frame = 20;
	static final String extType = ".mp4";
	
	@RequestMapping("/getApply")
	@ResponseBody
	public HashMap<String,Object> getApply(HttpServletRequest request){
		String str = request.getParameter("data");
		System.out.println(str);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONArray others = (JSONArray)jsonObject.get("otherObj");
		String effectKind = jsonObject.get("effectKind").toString();
		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);
		
		JSONObject background = (JSONObject)jsonObject.get("background");
		JSONObject obj = (JSONObject)jsonObject.get("obj");
		
		String rootPath = request.getServletContext().getRealPath("/");
		return applyObjectService.applyObject(effectKind,effectTime,background,obj,rootPath,others, request);
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public HashMap<String,Object> getApplyTest(HttpServletRequest request){
		String str = "{ \"effectKind\":\"1\",\"effectTime\":2,\"background\":{\"imgUrl\":\"http://localhost:8088/resources/cvimgs/backGround20170607022339.png\",\"top\":298.5,\"left\":475.75},\"obj\":{\"imgUrl\":\"/resources/cvimgs/result20170607022333.png\",\"top\":518.5,\"left\":720.75},\"otherObj\":[{\"top\":504.5,\"left\":898.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607022318.png\"},{\"top\":430.5,\"left\":593.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607022327.png\"},{\"top\":518.5,\"left\":720.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607022333.png\"},{\"top\":310.5,\"left\":686.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607022339.png\"}]}";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONArray others = (JSONArray)jsonObject.get("otherObj");
		String effectKind = jsonObject.get("effectKind").toString();
		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);
		
		JSONObject background = (JSONObject) jsonObject.get("background");
		JSONObject obj = (JSONObject) jsonObject.get("obj");
		
		String rootPath = request.getServletContext().getRealPath("/");
		return applyObjectService.applyObject(effectKind,effectTime,background,obj,rootPath,others, request);
	}
	
	@RequestMapping(value="/rainyDay",method=RequestMethod.POST) //no use
	@ResponseBody
	public HashMap<String,Object> rainDay(HttpServletRequest request) throws IOException{
		HashMap<String,Object> resultMap=new HashMap<String,Object>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String imgUrl=request.getParameter("imgUrl");
		System.out.println(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+imgUrl);
		Mat im = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+imgUrl);
		CreateFileUtils cfu = new CreateFileUtils();
        String today=cfu.getToday(0);
        /*ApplyObjectService.java 거쳐서 처리되게 수정 필요 : 나중에, shkwak, 2017-06-29*/
		rainDrop(im,request,today); 
		String fileName ="/resources/cvMov/rain" + today + extType;
		resultMap.put("fileName", fileName);
		return resultMap;
	}
	
	private void rainDrop(Mat im,HttpServletRequest request, String today){
		//roi
        Mat im1 = im.clone();
        Mat im2 = im.clone();
        
        Mat rainPng = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvStaticImg/")+"raindrop_1.jpg",1);
        Imgproc.threshold(rainPng, rainPng, 10, 255, Imgproc.THRESH_TOZERO);
        Mat rainPng2 = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvStaticImg/")+"raindrop_2.jpg",1);
        Imgproc.threshold(rainPng2, rainPng2, 10, 255, Imgproc.THRESH_TOZERO);
        
        System.out.println("Size im : "+im.rows()+","+im.cols());
        System.out.println("Size rainPng : "+rainPng.rows()+","+rainPng.cols());
        System.out.println("Size rainPng2 : "+rainPng2.rows()+","+rainPng2.cols());
        
        Imgproc.resize(rainPng, rainPng, new Size(im.cols(),im1.rows()));
        Imgproc.resize(rainPng2, rainPng2, new Size(im.cols(),im2.rows()));
 
        Mat imageRoi = new Mat(im1,new Rect(0, 0, rainPng.cols(), rainPng.rows()));
        Mat imageRoi2 = new Mat(im2,new Rect(0, 0, rainPng2.cols(), rainPng2.rows()));
        
        Core.addWeighted(imageRoi, 1.0, rainPng, 1, 0.0, imageRoi);
        Core.addWeighted(imageRoi2, 1.0, rainPng2, 1, 0.0, imageRoi2);
        
        VideoWriter videoWriter = new VideoWriter();
        
        File file = new File(request.getSession().getServletContext().getRealPath("resources/cvMov/") + "rain" + today + extType);
        if(file.exists()){
        	file.delete();
        }
        String rootPath = request.getServletContext().getRealPath("/");
        /*videoWriter.open("src/main/webapp/resources/cvMov/rain" + today + extType, VideoWriter.fourcc('H','2','6','4'), 8, imageRoi.size());*/ //frame
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/rain" + today + extType, VideoWriter.fourcc('H','2','6','4'), 8, imageRoi.size()); //frame
                
        for (int i = 0; i < 75; i++) { //75?
        	if(i%6>=3){
        		videoWriter.write(imageRoi);
                im.release();
        	}else{
        		videoWriter.write(imageRoi2);
                im.release();
        	}
        }
        videoWriter.release();		
        System.out.println("rain effect");        
	}
}
