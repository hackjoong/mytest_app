package com.Toonivie.OpenCv.ApplyObject;



import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.ImageTypeConvert;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObjectVo;

@Service
public class ApplyObjectService { //�궗臾쇱뿰異� �꽌鍮꾩뒪
	static final int frame = 20;
	static final String extType = ".mp4"; //add by shkwak, 2017-06-15
	public static HttpServletRequest request_url;
	
	public HashMap<String, Object> applyObject(String effectKind, int effectTime, JSONObject background, JSONObject obj, String rootPath, JSONArray others, HttpServletRequest request) {
		long ssssstart = System.currentTimeMillis();

		request_url = request;
		HashMap<String, Object> resultMap = new HashMap<>();
		System.out.println("ApplyObjectService.java:31 - applyObject() : " + effectKind);
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		MoveObjectVo objVo = changeMoveObjectVo(obj, background);
		MoveObjectVo backgroundVo = changeMoveBackgroundVo(background);
		List<MoveObjectVo> otherList = makeOtherList(others, objVo,backgroundVo); 
		int rotateVector;
		int biggerVector;
		if (effectKind.equals("1")){ //move
			
		}else if(effectKind.equals("2")||effectKind.equals("3")){ //smaller, bigger 1, 4 -> 2, 3
			if(effectKind.equals("2")){
				biggerVector=1;
			}else{
				biggerVector=-1;
			}
			resultMap = smaller(effectTime,objVo,backgroundVo,rootPath,otherList,biggerVector);
		}else if (effectKind.equals("4")){ //rotation-move
			
		}else if(effectKind.equals("5")||effectKind.equals("6")) { //rotate-smaller, bigger 8, 9 -> 5, 6
			if(effectKind.equals("5")){
				biggerVector=1; //left
			}else{
				biggerVector=-1; //right
			}
			resultMap = rotateSmaller(effectTime,objVo,backgroundVo,rootPath,otherList,biggerVector);
		}else if(effectKind.equals("7")) { //bigger and smaller 10 -> 7
			resultMap = biggerAndSmaller(effectTime,objVo,backgroundVo,rootPath,otherList);
		}else if (effectKind.equals("8")||effectKind.equals("9")){ //rotate 5, 6 -> 8, 9
			if(effectKind.equals("8")){
				rotateVector=1; //left
			}else{
				rotateVector=-1; //right
			}
			resultMap = rotate(effectTime, objVo, backgroundVo, rootPath,rotateVector,otherList);
		}else if(effectKind.equals("10")){ //vibrate 7 -> 10
			resultMap = vibrate(effectTime, objVo, backgroundVo, rootPath, otherList);
		}else if(effectKind.equals("11")) { //effectKind 11 異붽� (�늿源쒕컯�엫)
			rotateVector=0;
			resultMap = eyeBlink(effectTime, objVo, backgroundVo, rootPath,rotateVector,otherList);
		}
		long eeeend = System.currentTimeMillis();

		long tttttttime = eeeend - ssssstart;
		System.out.println("걸린 시간: " + tttttttime + " 밀리초");
		return resultMap;
	}
	
	/* �궗臾쇱뿰異� - Bigger and Smaller Effect - add by jinuk*/
	private HashMap<String,Object> biggerAndSmaller(int effectTime,MoveObjectVo obj, MoveObjectVo background,String rootPath,List<MoveObjectVo> others){
		HashMap<String, Object> resultMap = new HashMap<>();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat im2 = setRotateBackground(background, others, rootPath);
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		File file = new File(rootPath + "resources/cvMov/bToS" + today + ".mp4");
		if (file.exists()) { 
			file.delete();
		}
		
		videoWriter.open(rootPath+"resources/cvMov/bToS" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = null;
		float rate = (float)fore.rows()/(float)fore.cols();
		List<Mat> videoWriterMatList = new ArrayList<>();
		Mat afterObject = new Mat();
		for (int i = 0; i < frame * effectTime/2; i++) {
			Mat fm = fore.clone();
			int w = (int)((float)fore.cols()+(float)(((float)i/((float)frame * (float)effectTime/2)))*(float)20);
			Size size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			int left = (int) (obj.getLeft()-(float)(((float)i/((float)frame * (float)effectTime/2)))*(float)10);
			int top = (int)(obj.getTop()-(float)(((float)i/((float)frame * (float)effectTime/2)))*(float)10*rate);
			afterObject = putObjOnBackground.setBackMat(left, top, im2.clone(), fm);
			videoWriterMatList.add(afterObject);
			videoWriter.write(afterObject);
			if(i==(frame*effectTime)/4){
				thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
			}
		}
		for(int i=videoWriterMatList.size()-1;i>=0;i--) {
			videoWriter.write(videoWriterMatList.get(i));
		}
		videoWriter.release();

		String thumbNailImgName = rootPath+"resources/cvimgs/bToS"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl","/resources/cvMov/bToS" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/bToS"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}
	
	/* �궗臾쇱뿰異� - Rotation-Smaller, Bigger Effect - add by jinuk*/
	private HashMap<String,Object> rotateSmaller(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, List<MoveObjectVo> others, int biggerVector){
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat im2 = setRotateBackground(background, others, rootPath);
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		File file = new File(rootPath + "resources/cvMov/bigger" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		videoWriter.open(rootPath+"resources/cvMov/bigger" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = null;
		
		float rate = (float)fore.rows()/(float)fore.cols();
		List<Mat> videoWriterMatList = new ArrayList<>();
		Mat afterObject = new Mat();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat fm = fore.clone();
			float a =((float)(fore.width()-4))/((float)(frame*effectTime));
			int w = (int)((float)fore.cols()-i*a);
			Size size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			Mat m = Imgproc.getRotationMatrix2D(new Point(fm.width()/2,fm.height()/2), i*2, 1.0);
			Imgproc.warpAffine(fm, afterObject, m, new Size(fm.cols(),fm.rows()));
			videoWriterMatList.add(putObjOnBackground.setBackMat((int)obj.getLeft()+((fore.cols()-fm.cols()))/2, (int)obj.getTop()+((fore.rows()-fm.rows()))/2, im2.clone(), afterObject));
			if(i==(frame*effectTime)/2){
				thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
			}
		}
		videoWriterMatList.add(im2);
		if(biggerVector==1){
			for(Mat image : videoWriterMatList){
				videoWriter.write(image);
			}
		}else{
			for(int i=videoWriterMatList.size()-1;i>=0;i--){
				videoWriter.write(videoWriterMatList.get(i));
			}
		}
		videoWriter.release();
		
		HashMap<String, Object> resultMap = new HashMap<>();
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl","/resources/cvMov/bigger" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}
	/* �궗臾쇱뿰異� - Bigger, Smaller Effect */
	private HashMap<String,Object> smaller(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, List<MoveObjectVo> others, int biggerVector){
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat im2 = setRotateBackground(background, others, rootPath);
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
		
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		File file = new File(rootPath + "resources/cvMov/bigger" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		
		VideoWriter videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/bigger" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = null;

		float rate = (float)fore.rows()/(float)fore.cols();
		List<Mat> videoWriterMatList = new ArrayList<>();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat fm = fore.clone();
			float a =((float)(fore.width()-4))/((float)(frame*effectTime));
			int w = (int)((float)fore.cols()-i*a);
			Size size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			videoWriterMatList.add(putObjOnBackground.setBackMat((int)obj.getLeft()+((fore.cols()-fm.cols()))/2, (int)obj.getTop()+((fore.rows()-fm.rows()))/2, im2.clone(), fm));
			if(i==(frame*effectTime)/2){
				thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
			}
		}
		videoWriterMatList.add(im2);
		if(biggerVector==1){
			for(Mat image : videoWriterMatList){
				videoWriter.write(image);
			}
		}else{
			for(int i=videoWriterMatList.size()-1;i>=0;i--){
				videoWriter.write(videoWriterMatList.get(i));
			}
		}
		videoWriter.release();

		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		HashMap<String, Object> resultMap = new HashMap<>();
		resultMap.put("videoUrl","/resources/cvMov/bigger" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}
	
	/* �궗臾쇱뿰異� - Vibration Effect */
	private HashMap<String,Object> vibrate(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, List<MoveObjectVo> others){
		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME); //add by shkwak, 2017-06-15		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat im2 = setRotateBackground(background, others, rootPath);
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
		
		Mat f0 = putObjOnBackground.setBackMat((int)obj.getLeft()-10, (int)obj.getTop()-10, im2.clone(), fore.clone());
		Mat f1 = putObjOnBackground.setBackMat((int)obj.getLeft()+10, (int)obj.getTop()+10, im2.clone(), fore.clone());
		Mat f2 = putObjOnBackground.setBackMat((int)obj.getLeft()+10, (int)obj.getTop()-10, im2.clone(), fore.clone());
		Mat f3 = putObjOnBackground.setBackMat((int)obj.getLeft()-10, (int)obj.getTop()+10, im2.clone(), fore.clone());
		Mat f_origin = putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fore.clone());
		
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/vibrate" + today + ".mp4"); //avi
		if (file.exists()) {
			file.delete();
		} 
	
		videoWriter.open(rootPath+"resources/cvMov/vibrate" + today + ".mp4", VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = f1.clone();
		videoWriter.write(f_origin);
		for (int i = 0; i < frame * effectTime; i++) {	
			int index = i+1;
			index=index%4;
			if(index==0){
				videoWriter.write(f0);
			}else if(index==1){
				videoWriter.write(f1);
			}else if(index==2){
				videoWriter.write(f2);
			}else if(index==3){
				videoWriter.write(f3);
			}
		}
		videoWriter.write(f_origin);
		videoWriter.release();

		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl","/resources/cvMov/vibrate" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}
	
	public List<MoveObjectVo> makeOtherList(JSONArray others, MoveObjectVo obj, MoveObjectVo backgroundVo){
		List<MoveObjectVo> moveObjectVos = new ArrayList<>();
		for(int i=0;i<others.size();i++){
			try {
				JSONObject object = (JSONObject)others.get(i);
				String imgUrl = object.get("imgUrl").toString();
				Double d3 = new Double(object.get("top").toString());
				Double d4 = new Double(object.get("left").toString());
				Double d5 = new Double(object.get("degree").toString()); //2017-12-15 add by. KH
				float top = d3.floatValue();
				float left = d4.floatValue();
				float degree = d5.floatValue(); //2017-12-15 add by. KH
				System.out.println(degree!=obj.getDegree());
				if(!imgUrl.replace("http://localhost:8088","").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "").equals(obj.getImgUrl())&&top!=obj.getTop()&&left!=obj.getLeft()){ //2017-12-15 add by. KH
					MoveObjectVo vo = new  MoveObjectVo();
					vo.setImgUrl(imgUrl);
					vo.setTop(top-backgroundVo.getTop());
					vo.setLeft(left-backgroundVo.getLeft());
					vo.setDegree(degree); //2017-12-15 add by. KH
					vo.setHeight((new Double(object.get("height").toString())).floatValue());
					vo.setWidth((new Double(object.get("width").toString())).floatValue());
					moveObjectVos.add(vo);
				}
				
			}catch(Exception e) {
				continue;
			}
		}
		return moveObjectVos;
	}
	
	public Mat setRotateBackground(MoveObjectVo background, List<MoveObjectVo> others, String rootPath){
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat im2 = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im2.width(), im2.height());
		im2 = imagetypeconvert.bgraTobgr(im2);
		Mat resultMat=im2;
		String path= "";
		for(MoveObjectVo vo : others){
			path = rootPath+vo.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, vo.getHeight(), vo.getWidth());
			Mat fore = Imgcodecs.imread(rootPath+vo.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			resultMat=putObjOnBackground.setBackMat((int)vo.getLeft(), (int)vo.getTop(), im2, fore);
		}
		return resultMat;
	}
	
	/* 占쎄텢�눧�눘肉곁빊占� - Rotation Effect */
	private HashMap<String, Object> rotate(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, int rotateVector, List<MoveObjectVo> others) {
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im2 = setRotateBackground(background, others, rootPath);
		
		Mat object2 = Imgcodecs.imread(rootPath + obj.getImgUrl(), Imgcodecs.IMREAD_UNCHANGED);

		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);

		File file = new File(rootPath + "resources/cvMov/rotate" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		VideoWriter videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/rotate" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = null;
		//
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat im = im2.clone();
			Mat object = object2.clone();
			Point pt = new Point(object.cols() / 2, object.rows() / 2);
			Mat m = Imgproc.getRotationMatrix2D(pt, ((double)(360)/(double)(frame*effectTime-1))*i*rotateVector, 1.0);
			Imgproc.warpAffine(object, object, m, new Size(object.cols(), object.rows()));
			Mat f_origin = putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im, object.clone());

			videoWriter.write(f_origin);
			if(i==(frame * effectTime)/2){
				thumbNail=im.clone();
			}
		}

		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl","/resources/cvMov/rotate" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im2.size().width);
		return resultMap;
	}
	
	//effectKind 11 �늿源쒕컯�엫 異붽�
	private HashMap<String, Object> eyeBlink(int effectTime, MoveObjectVo obj, MoveObjectVo background, String rootPath, int rotateVector, List<MoveObjectVo> others) {
		HashMap<String, Object> resultMap = new HashMap<>();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat im2 = setRotateBackground(background, others, rootPath);
		others.add(obj); 
		
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		File file = new File(rootPath + "resources/cvMov/eyeBlink" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		
		videoWriter.open(rootPath + "resources/cvMov/eyeBlink" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
		Mat thumbNail = null;
		
		List<Mat> videoWriterMatList = new ArrayList<>();
		Mat afterObject = new Mat();
		for(int i = 0; i < effectTime ; i++) {
			for(int j = 0; j < frame / 3; j++) {
				for(int obj_count = 0; obj_count < others.size(); obj_count++) {
					Mat fore = Imgcodecs.imread(rootPath+others.get(obj_count).getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
					MoveObjectVo obj_eyes = others.get(obj_count);

					Mat fm = fore.clone();
					Size size = new Size(fore.cols() ,fore.rows());

					Imgproc.resize(fm, fm, size);
					int left = (int) obj_eyes.getLeft();
					int top = (int) obj_eyes.getTop();
					Point pt = new Point(size.width / 2, size.height / 2);
					if(obj_count == 0) {
						afterObject = putObjOnBackground.setBackMat(left, top, im2.clone(), fm);
					}else {
						afterObject = putObjOnBackground.setBackMat(left, top, afterObject.clone(), fm);
					}
					videoWriterMatList.add(afterObject);
				}
				videoWriter.write(afterObject);
			}
			for (int j = 0; j< frame / 3; j++) {
				for(int obj_count = 0; obj_count < others.size(); obj_count++) {
					Mat fore = Imgcodecs.imread(rootPath+others.get(obj_count).getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
					MoveObjectVo obj_eyes = others.get(obj_count);
					float rate = (float)fore.rows()/(float)fore.cols();

					Mat fm = fore.clone();
					int h = (int)((fore.rows()/2)-((fore.rows()/2)-5));
					Size size = new Size(fore.cols() ,h);
					Imgproc.resize(fm, fm, size);
					int left = (int) obj_eyes.getLeft();
					int top = (int) ((int) obj_eyes.getTop()+(fore.rows()/2)*rate);
					Point pt = new Point(size.width / 2, size.height / 2);
					Mat m = Imgproc.getRotationMatrix2D(pt, others.get(obj_count).getDegree()*-1, 1.0);
					Imgproc.warpAffine(fm, fm, m, new Size(fore.cols(), fore.rows()));
					if(obj_count == 0) {
						afterObject = putObjOnBackground.setBackMat(left, top, im2.clone(), fm);
					}else {
						afterObject = putObjOnBackground.setBackMat(left, top, afterObject.clone(), fm);
					}
					videoWriterMatList.add(afterObject);
					thumbNail=putObjOnBackground.setBackMat((int)obj_eyes.getLeft(), (int)obj_eyes.getTop(), im2.clone(), fm);
				}
				videoWriter.write(afterObject);
			}
			for(int j = 0; j < (frame / 3)+2; j++) {
				for(int obj_count = 0; obj_count < others.size(); obj_count++) {
					Mat fore = Imgcodecs.imread(rootPath+others.get(obj_count).getImgUrl().replace("webapp//", "webapp/"), Imgcodecs.IMREAD_UNCHANGED);
					MoveObjectVo obj_eyes = others.get(obj_count);
					Mat fm = fore.clone();
					Size size = new Size(fore.cols() ,fore.rows());

					Imgproc.resize(fm, fm, size);
					int left = (int) obj_eyes.getLeft();
					int top = (int) obj_eyes.getTop();
					Point pt = new Point(size.width / 2, size.height / 2);
					if(obj_count == 0) {
						afterObject = putObjOnBackground.setBackMat(left, top, im2.clone(), fm);
					}else {
						afterObject = putObjOnBackground.setBackMat(left, top, afterObject.clone(), fm);
					}
					videoWriterMatList.add(afterObject);
				}
				videoWriter.write(afterObject);
			}
		}

		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl","/resources/cvMov/eyeBlink" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}

	//private -> public by KH 2017-12-28
	public MoveObjectVo changeMoveObjectVo(JSONObject obj, JSONObject background) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		String imgUrl = obj.get("imgUrl").toString();
		Double d1 = new Double(background.get("top").toString());
		Double d2 = new Double(background.get("left").toString());
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		Double d5 = new Double(obj.get("degree").toString());
		float top = d3.floatValue() - d1.floatValue();
		float left = d4.floatValue() - d2.floatValue();
		float degree = d5.floatValue();
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		moveObjectVo.setHeight((new Double(obj.get("height").toString())).floatValue());
		moveObjectVo.setWidth((new Double(obj.get("width").toString())).floatValue());
		moveObjectVo.setDegree(degree);
		return moveObjectVo;
	}
	
	//private -> public by KH 2017-12-28
	public MoveObjectVo changeMoveBackgroundVo(JSONObject obj) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		String imgUrl = obj.get("imgUrl").toString().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		float top = d3.floatValue();
		float left = d4.floatValue();
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		return moveObjectVo;
	}	
}
