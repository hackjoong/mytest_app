package com.Toonivie.OpenCv.Attach;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.ffmpeg.ffmpegConverter;

@Controller
@RequestMapping("/attach")
public class AttachController {
	@Autowired
	AttachService attachService;
	
	@RequestMapping(value="/attachVideo")
	@ResponseBody
	public HashMap<String,Object> attachVideo(HttpServletRequest request){
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		String str = request.getParameter("data"); //date : jsonString
		String target_resolution = request.getParameter("target_resolution"); //date : jsonString
		System.out.println("/attachVideo:26 - data : " + str);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject)jsonParser.parse(str);
			System.out.println("/attachVideo:31 - jsonObject : " + jsonObject);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String rootPath = request.getServletContext().getRealPath("/"); //C:\toonivie\src\main\webapp\
		JSONArray videoList = (JSONArray)jsonObject.get("videoList");
		JSONArray resolution_List = (JSONArray)jsonObject.get("target_resolution");
		System.out.println("/attachVideo:38 - rootPath : " + rootPath + ", videoList : " + videoList);
		resultMap.put("videoUrl", attachService.attachVideo(videoList,rootPath)); //결과 : resultMap.put("videoUrl", "/resources/cvMov/final" + today + ".mp4")); 
		List<String> resolution_list = new ArrayList<String>();
		for(int i=0;i<resolution_List.size();i++) {
			resolution_list.add(i, (String) resolution_List.get(i));
		}
		for(String resolution : resolution_list) {
			int resolution_H = Integer.parseInt(resolution);
			int resolution_W = (int) Math.ceil((double)resolution_H * (double) 1.77777777777777);
			ffmpegConverter ff = new ffmpegConverter(((String)(((Map)resultMap.get("videoUrl")).get("videoUrl"))).replaceAll(".mp4", "").replace("resources/cvMov/", ""), rootPath, "44100", "192", "mp4",((String)(((Map)resultMap.get("videoUrl")).get("videoUrl"))).replace("resources/cvMov/", "")+"_"+resolution+"p", resolution_H, resolution_W);
			resultMap.put("output_"+resolution+"p", "/resources/cvMov/"+ff.convert());
			
		}
		
		return resultMap;
	}
}
