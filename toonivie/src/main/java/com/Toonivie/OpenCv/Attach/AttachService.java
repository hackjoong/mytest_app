package com.Toonivie.OpenCv.Attach;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.AttachVideoVo;

@Service
public class AttachService {
	static final int frame = 20;
	//static final int sizeWidth = 960;
	//static final int sizeHeight = 540;
	static final String extType = ".mp4"; //.mp4, add by shkwak, 2017-06-16
	public HashMap<String,Object> attachVideo(JSONArray videoList,String rootPath){
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		HashMap<String,Object> resultMap = new HashMap<>();
		List<AttachVideoVo> voList = makeAttachVideoVoList(videoList);
		System.out.println(voList.size());
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		File file = new File(rootPath + "resources/cvMov/final" + today + extType);
		if (file.exists()) {
			file.delete();
		}
		int j;
		try {
			j = get_smallest(voList);//get smallest resolution by KH 180410
		}catch(Exception e) {
			 j = 0;
		}
		int videoWidth = Integer.parseInt(voList.get(j).getVideoWidth()); //add by shkwak, 2017-06-19
		int videoHeight = Integer.parseInt(voList.get(j).getVideoHeight());
		
		
		System.out.println("videoSize : " + videoWidth + "x" + videoHeight);
		videoWriter.open(rootPath + "resources/cvMov/final" + today + extType,	
				VideoWriter.fourcc('H','2','6','4'), frame, new Size(videoWidth,videoHeight)); //new Size(960,540) //'D','I','V','3'
		
		if (!videoWriter.isOpened()){
			System.out.println("Can not open videoWriter !!!");		  
		} else {
			int i=0;
			for(AttachVideoVo vo : voList){
				Mat image=new Mat();
				System.out.println("attachVideo():41 - " + vo.getVideoUrl());
				VideoCapture vc = new VideoCapture(rootPath+vo.getVideoUrl());
				System.out.println(vc.isOpened());			
				while(vc.read(image)){
					Imgproc.resize(image, image, new Size(videoWidth, videoHeight)); //resize image
					videoWriter.write(image);
					i++;
				}
			}
			videoWriter.release();		
		}
		resultMap.put("videoUrl", "/resources/cvMov/final" + today + extType);
		resultMap.put("videoHeight", videoHeight); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", videoWidth); //援ъ� return �븷 �븘�슂�뾾�뒗 �빆紐�
		return resultMap;
	}
	
	private List<AttachVideoVo> makeAttachVideoVoList(JSONArray videoList){ //modify by shkwak, 2017-06-19
		List<AttachVideoVo> attachVideoVos = new ArrayList<>();
		for(int i=0;i<videoList.size();i++){ //videoUse 議곌굔�떇 異붽� by shkwak, 2017-06-26
			JSONObject jsonObject = (JSONObject)videoList.get(i);
			String thisUse = new String(jsonObject.get("videoUse").toString());
			if(thisUse.equals("yes")){
				AttachVideoVo attachVideoVo = new AttachVideoVo();			
				attachVideoVo.setVideoUrl(jsonObject.get("videoUrl").toString());
				attachVideoVo.setVideoWidth(jsonObject.get("videoWidth").toString()); //add by shkwak
				attachVideoVo.setVideoHeight(jsonObject.get("videoHeight").toString()); //add
				attachVideoVo.setTime(Integer.parseInt(jsonObject.get("time").toString()));
				attachVideoVos.add(attachVideoVo);
			}else{
				System.out.println("pass!");
			}
			System.out.println(i + " - thisUse : " + thisUse);
		}
		System.out.println(attachVideoVos); //[com.Toonivie.Vo.AttachVideoVo@48653264,
		return attachVideoVos;
	}
	
	public int get_smallest(List<AttachVideoVo> vo) {
		int result_w = 0;
		int result_h = 0;
		int result_i = 0;
		int nxt_width = 0;
		//get smallest width by KH 2018-01-24
		for(int i=0;i<vo.size();i++) {
			result_w = Integer.parseInt(vo.get(i).getVideoWidth());			
			try {
				nxt_width = Integer.parseInt(vo.get(i+1).getVideoWidth());
			}catch(Exception e) {
			}
			
			if(result_w>nxt_width) {
				result_w = Integer.parseInt(vo.get(i+1).getVideoWidth());
				result_h = Integer.parseInt(vo.get(i+1).getVideoHeight());
				result_i = i+1;
			}
		}
		return result_i;
	}
}
