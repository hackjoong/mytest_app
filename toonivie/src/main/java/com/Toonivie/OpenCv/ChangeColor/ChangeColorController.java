package com.Toonivie.OpenCv.ChangeColor;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
@RequestMapping("/changeColor")
public class ChangeColorController {
/*	@Autowired
	ChangeColorService changeColorService;*/
	
	@RequestMapping(value="/getColorAvg",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> getColorAvg(HttpServletRequest request){
		ChangeColorService changeColorService = new ChangeColorService();
		String imgUrl="";
		System.out.println(request.getParameter("imgUrl"));
		if(request.getParameter("psd_img_mode").equals("true")) {
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
		}else if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
			imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        }else{
            imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        }
        return changeColorService.getColorAvg(imgUrl);
	}
	@RequestMapping(value="/changeExe",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> changeExe(HttpServletRequest request){
		ChangeColorService changeColorService = new ChangeColorService();
		int red = Integer.parseInt(request.getParameter("red"));
		int green = Integer.parseInt(request.getParameter("green"));
		int blue = Integer.parseInt(request.getParameter("blue"));
		String save_yn = request.getParameter("save_yn");
		String imgUrl="";
		String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
		if(request.getParameter("save_yn").equals("psd")) {
        	imgUrl=request.getParameter("imgUrl");
        	rootPath = request.getSession().getServletContext().getRealPath("");
		}else if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        	rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
        }else {
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        	rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
        }
        return changeColorService.changeExe(red, green, blue, imgUrl, rootPath, save_yn);
	}
}
