package com.Toonivie.OpenCv.ChangeColor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.stereotype.Service;

@Service
public class ChangeColorService {
	
	public HashMap<String, Object> getColorAvg(String imgUrl) {
		HashMap<String, Object> resultMap = new HashMap<>();
		//System.load("./lib/opencv_java320.jar"); add by KH, 2017-12-22
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		System.out.println("### imgUrl : "+imgUrl);
		Mat img = Imgcodecs.imread(imgUrl, 1);
		List<Mat> rgb = new ArrayList<>();
		Core.split(img, rgb);
		int blue = 0;
		int green = 0;
		int red = 0;
		int divideVal = img.rows() * img.cols();
		for (int i = 0; i < img.rows(); i++) {
			for (int j = 0; j < img.cols(); j++) {
				double[] mask1Pixel = rgb.get(0).get(i, j);
				blue += mask1Pixel[0];
			}
		}
		for (int i = 0; i < img.rows(); i++) {
			for (int j = 0; j < img.cols(); j++) {
				double[] mask1Pixel = rgb.get(1).get(i, j);
				green += mask1Pixel[0];
			}
		}
		for (int i = 0; i < img.rows(); i++) {
			for (int j = 0; j < img.cols(); j++) {
				double[] mask1Pixel = rgb.get(2).get(i, j);
				red += mask1Pixel[0];
			}
		}
		resultMap.put("red", red / divideVal);
		resultMap.put("green", green / divideVal);
		resultMap.put("blue", blue / divideVal);	
		return resultMap;
	}
	public int byteToint(byte[] arr){
	    return (arr[0] & 0xff)<<24 | (arr[1] & 0xff)<<16 |
	              (arr[2] & 0xff)<<8 | (arr[3] & 0xff);
	   }
	public HashMap<String, Object> changeExe(int redHope, int greenHope, int blueHope, String imgUrl, String rootPath, String save_yn) {
		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat oldImg2;
		if(save_yn.equals("psd")){
			oldImg2 = Imgcodecs.imread(rootPath+imgUrl, 1);
		}else {
			oldImg2 = Imgcodecs.imread(imgUrl, 1);
		}
		Mat oldImg = oldImg2.clone();
		List<Mat> rgb = new ArrayList<>();
		Core.split(oldImg, rgb);
		int blue = 0;
		int green = 0;
		int red = 0;
		int divideVal = oldImg.rows() * oldImg.cols();
		for (int i = 0; i < oldImg.rows(); i++) {
			for (int j = 0; j < oldImg.cols(); j++) {
				double[] mask1Pixel = rgb.get(0).get(i, j);
				blue += mask1Pixel[0];
			}
		}
		for (int i = 0; i < oldImg.rows(); i++) {
			for (int j = 0; j < oldImg.cols(); j++) {
				double[] mask1Pixel = rgb.get(1).get(i, j);
				green += mask1Pixel[0];
			}
		}
		for (int i = 0; i < oldImg.rows(); i++) {
			for (int j = 0; j < oldImg.cols(); j++) {
				double[] mask1Pixel = rgb.get(2).get(i, j);
				red += mask1Pixel[0];
			}
		}

		int blueAvg = blue / divideVal;
		int redAvg = red / divideVal;
		int greenAvg = green / divideVal;
		
		int redDis = redHope-redAvg;
		int greenDis = greenHope-greenAvg;
		int blueDis = blueHope-blueAvg;
		
//		System.out.println("AVG : " + blueAvg + "," + greenAvg + "," + redAvg);
//		System.out.println("Dis : " + blueDis + "," + greenDis + "," + redDis);
		System.out.println("AVG : " + redAvg + "," + greenAvg + "," + blueAvg);
		System.out.println("Dis : " + redDis + "," + greenDis + "," + blueDis);
		
		for (int i = 0; i < oldImg.rows(); i++) {
			for (int j = 0; j < oldImg.cols(); j++) {
				
				
				double[] buffer = oldImg.get(i, j);
				double blueVal = buffer[0]+blueDis;
				if(blueVal>255){
					blueVal=255;
				}
				if(blueVal<0){
					blueVal=0;
				}
				double greenVal = buffer[1]+greenDis;
				if(greenVal>255){
					greenVal=255;
				}
				if(greenVal<0){
					greenVal=0;
				}
				double redVal = buffer[2]+redDis;
				if(redVal>255){
					redVal=255;
				}
				if(redVal<0){
					redVal=0;
				}
				buffer[0]=blueVal;
				buffer[1]=greenVal;
				buffer[2]=redVal; 
				oldImg.put(i, j, buffer);
			}
		}
		
		if(save_yn.equals("true")) {
			//적용하기
			Imgcodecs.imwrite(imgUrl, oldImg); //add by KH , 2017-12-18
			if(imgUrl.contains("backGround")) {//add by hoo , 2018-10-05
				resultMap.put("resultImg", "/resources/cvimgs/" + imgUrl.substring(imgUrl.length()-31, imgUrl.length()));//add by hoo , 2018-10-05
			}else {
				resultMap.put("resultImg", "/resources/cvimgs/" + imgUrl.substring(imgUrl.length()-21, imgUrl.length()));//add by KH , 2017-12-18
			}
			 
		}else if(save_yn.equals("psd")) {
			Imgcodecs.imwrite(rootPath + "/resources/psdimgs/new_" +imgUrl.substring(22, imgUrl.length()), oldImg); //add by KH , 2018-02-09
			resultMap.put("resultImg", "/resources/psdimgs/new_" +imgUrl.substring(22, imgUrl.length())); //add by KH , 2018-02-09
			resultMap.put("resultImg_Name", imgUrl.substring(22, imgUrl.length())); //add by KH , 2018-02-09
			
		}else {
			//미리보기
			Imgcodecs.imwrite(rootPath + "new.png", oldImg); //add by KH , 2017-12-18
			resultMap.put("resultImg", "/resources/cvimgs/new.png"); //add by KH , 2017-12-18
		}
		return resultMap;
	}
}
