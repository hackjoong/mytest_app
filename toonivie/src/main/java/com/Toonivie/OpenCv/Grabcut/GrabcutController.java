package com.Toonivie.OpenCv.Grabcut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Rect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.OpenCv.Grabcut.Vo.GrabcutRectangleVo;
import com.Toonivie.Util.DataURL_tofile;
import com.Toonivie.Util.Ext_API;

@Controller
@RequestMapping("/grabcut")
public class GrabcutController {
	@Autowired
	GrabcutService grabcutService;
	@Autowired
	GrabcutServiceAdvanced grabcutServiceAdvanced;
	
	@RequestMapping(value="/grabcutExe",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> grabcutExe(HttpServletRequest request,@RequestParam("bgListX[]") ArrayList<Integer> bgListX,@RequestParam("bgListY[]") ArrayList<Integer> bgListY
    		,@RequestParam("fgListY[]") ArrayList<Integer> fgListY,@RequestParam("fgListX[]") ArrayList<Integer> fgListX, @RequestParam("clicked_color_rgb[]") ArrayList<Integer> clicked_color_rgb){
		HashMap<String,Object> resultMap = new HashMap<>();
        GrabcutRectangleVo grabcutRectangleVo = new GrabcutRectangleVo(Integer.parseInt(request.getParameter("GbeX")), Integer.parseInt(request.getParameter("GbeY")), 
        		Integer.parseInt(request.getParameter("GafX")), Integer.parseInt(request.getParameter("GafY")));
        int mode = Integer.parseInt(request.getParameter("Gmode"));
        String imgUrl="";
        if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        }else{
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        }
        
        String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
		//return grabcutService.grabcutExe(bgListX, bgListY, fgListX, fgListY, grabcutRectangleVo, mode, imgUrl,rootPath);
        
        
        int beX = Integer.parseInt(request.getParameter("GbeX"));
        int beY = Integer.parseInt(request.getParameter("GbeY"));
        int afX = Integer.parseInt(request.getParameter("GafX"));
        int afY = Integer.parseInt(request.getParameter("GafY"));

        String[] gcBgdX = request.getParameterValues("gcBgdX[]");
        String[] gcBgdY = request.getParameterValues("gcBgdY[]");
        
        String[] gcFgdX = request.getParameterValues("gcFgdX[]");
        String[] gcFgdY = request.getParameterValues("gcFgdY[]");
        
        String[] gcprBgdX = request.getParameterValues("gcprBgdX[]");
        String[] gcprBgdY = request.getParameterValues("gcprBgdY[]");
        
        String[] gcprFgdX = request.getParameterValues("gcprFgdX[]");
        String[] gcprFgdY = request.getParameterValues("gcprFgdY[]");
        
        int filling_type = Integer.parseInt(request.getParameter("filling_type")); //add by KH, 2017-12-21
        Boolean blur_status = Boolean.parseBoolean(request.getParameter("blur_status")); //add by KH, 2017-12-21
        int blurSigmaXV = Integer.parseInt(request.getParameter("blurSigmaXV")); //add hoo 2018-10-29
        return grabcutServiceAdvanced.grabcut(bgListX, bgListY, fgListX, fgListY, mode, imgUrl, rootPath, beX, beY, afX, afY, filling_type, blur_status, blurSigmaXV, clicked_color_rgb,
        		gcBgdX, gcBgdY, gcFgdX, gcFgdY, gcprBgdX, gcprBgdY, gcprFgdX, gcprFgdY); //modified by KH, 2017-12-21 hoo 2018-10-30
	}
	
	@RequestMapping("/init")
	@ResponseBody
	public HashMap<String,String> init(HttpServletRequest request){
		HashMap<String,String> resultMap = new HashMap<>();
		grabcutService.init();
		return resultMap;
	}
	
	@RequestMapping(value="/saveBGasOBJ",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> saveBGasOBJ(HttpServletRequest request, @RequestParam("clicked_color_rgb[]") ArrayList<Integer> clicked_color_rgb ){
		HashMap<String,Object> resultMap = new HashMap<>();
        int filling_type = Integer.parseInt(request.getParameter("filling_type"));
        String imgUrl="";
        if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        }else{
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        }
        String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
		//return grabcutService.grabcutExe(bgListX, bgListY, fgListX, fgListY, grabcutRectangleVo, mode, imgUrl,rootPath);
        return grabcutServiceAdvanced.make_background(imgUrl, rootPath, filling_type, clicked_color_rgb); //modified by KH, 2017-12-21
	}
	@RequestMapping(value="/grabcutMask",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> grabcutExe(HttpServletRequest request, @RequestParam("clicked_color_rgb[]") ArrayList<Integer> clicked_color_rgb){
		HashMap<String,Object> resultMap = new HashMap<>();
        String imgUrl_BG="";
        String resultImg="";
        if(request.getParameter("ori_background").contains("resources/cvimgs/")){
        	imgUrl_BG=request.getSession().getServletContext().getRealPath("/")+request.getParameter("ori_background");
        }else{
        	imgUrl_BG=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("ori_background");
        }
        if(request.getParameter("resultImg").contains("resources/cvimgs/grabcut/")){
        	resultImg=request.getSession().getServletContext().getRealPath("/")+request.getParameter("resultImg");
        }else{
        	resultImg=request.getSession().getServletContext().getRealPath("resources/cvimgs/grabcut/")+request.getParameter("resultImg");
        }
        String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
        int filling_type = Integer.parseInt(request.getParameter("filling_type")); 
        Boolean blur_status = Boolean.parseBoolean(request.getParameter("blur_status")); 
        String grabcutBG = request.getParameter("grabcutBG");
        int beX = Integer.parseInt(request.getParameter("beX"));
        int beY = Integer.parseInt(request.getParameter("beY"));
        int resultWidth = Integer.parseInt(request.getParameter("resultWidth")); 
        int resultHeight = Integer.parseInt(request.getParameter("resultHeight")); 
        Rect rect = new Rect(beX, beY, resultWidth, resultHeight);
        
		DataURL_tofile dataURL_tofile = new DataURL_tofile();
		dataURL_tofile.dataURL_toPNG(request.getParameter("grabcut_mask"), "/resources/cvimgs/", "grabcut_mask", request);
        
        return grabcutServiceAdvanced.grabcut(imgUrl_BG, rect, grabcutBG, resultImg, rootPath, "/resources/cvimgs/grabcut_mask.png", filling_type, blur_status, clicked_color_rgb); 
	}
	@RequestMapping(value="/coloring",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> coloring(HttpServletRequest request){
        String imgUrl="";
        Ext_API coloring_api = new Ext_API();
        
        if(request.getParameter("ori_background").contains("resources/cvimgs/")){
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("ori_background");
        }else{
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("ori_background");
        }
        String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
        
        
        return coloring_api.paint_chainer(imgUrl, imgUrl, rootPath);
	}
}
