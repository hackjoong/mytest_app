package com.Toonivie.OpenCv.Grabcut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Service;

import com.Toonivie.OpenCv.Grabcut.Vo.GrabcutRectangleVo;
import com.Toonivie.Util.CreateFileUtils;

@Service
public class GrabcutService {
	static Mat mask1;
	static ArrayList<Integer> staticBgListX=new ArrayList<Integer>();
	static ArrayList<Integer> staticBgListY=new ArrayList<Integer>();
	static ArrayList<Integer> staticFgListX=new ArrayList<Integer>();
	static ArrayList<Integer> staticFgListY=new ArrayList<Integer>();
	int beX;
	int beY;
	int afX;
	int afY;
	
	public HashMap<String,Object> grabcutExe(ArrayList<Integer> bgListX,ArrayList<Integer> bgListY,ArrayList<Integer> fgListX,ArrayList<Integer> fgListY,GrabcutRectangleVo grabcutRectangleVo,int mode,String imgUrl,String rootPath){
		HashMap<String,Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		Mat im = Imgcodecs.imread(imgUrl);
		
		beX=grabcutRectangleVo.getBeX();
		beY=grabcutRectangleVo.getBeY();
		afX=grabcutRectangleVo.getAfX();
		afY=grabcutRectangleVo.getAfY();
		mask1 = new Mat(im.size(),CvType.CV_8UC1,new Scalar(Imgproc.GC_BGD));
        grabcutMaskSetting(grabcutRectangleVo.getGrabcutRect(),mode,bgListX,bgListY,im,fgListX,fgListY);
        
        Mat bgModel = new Mat();
        Mat fgModel = new Mat();
        Mat source = new Mat(1, 1, CvType.CV_8U, new Scalar(3));
        
        Imgproc.grabCut(im, mask1, grabcutRectangleVo.getGrabcutRect(), bgModel, fgModel, 1,Imgproc.GC_INIT_WITH_MASK);
        
        Core.compare(mask1, source, mask1, Core.CMP_EQ);
        Mat fg = new Mat(im.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
        Mat imClone=im.clone();
        imClone.copyTo(fg, mask1);
        
        Mat fg2=new Mat(im.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
        Core.subtract(im, fg, fg2);
        
        int rectWidth = afX-beX;
        int rectHeight = afY-beY;
        for(int i=beY;i<=afY;i++){
            for(int j=beX;j<=beX+rectWidth/2;j++){
                double[] mask1Pixel = mask1.get(i, j);
                if(mask1Pixel[0]>150){
                    double[] putPixel = {255,255,255};
                    putPixel[0]=fg2.get(i, j-10)[0];
                    putPixel[1]=fg2.get(i, j-10)[1];
                    putPixel[2]=fg2.get(i, j-10)[2];

                    fg2.put(i, j, putPixel);
                }
            }
        }
        for(int i=beY;i<=afY;i++){
            for(int j=afX;j>beX+rectWidth/2;j--){
                double[] mask1Pixel = mask1.get(i, j);
                if(mask1Pixel[0]>150){
                    double[] putPixel = {255,255,255};
                    int jp10=j+10;
                    if(jp10>im.width()){
                        jp10=im.width();
                    }
                    putPixel[0]=fg2.get(i, jp10-1)[0];
                    putPixel[1]=fg2.get(i, jp10-1)[1];
                    putPixel[2]=fg2.get(i, jp10-1)[2];

                    fg2.put(i, j, putPixel);
                }
            }
        }
        Mat blurMask = fg2.submat(grabcutRectangleVo.getGrabcutRect());
        Imgproc.GaussianBlur(blurMask, blurMask, new Size(11,11), 11); //blur 주석처리 by 

        CreateFileUtils createFileUtils = new CreateFileUtils();
        String backGroundFileName = "backGround"+createFileUtils.getToday(1)+".png";
        
        Imgcodecs.imwrite(rootPath+backGroundFileName, fg2);

        Mat bgMask = new Mat(im.size(), CvType.CV_8UC1,new Scalar(0,0,0));
        Mat src=fg.clone();
        Mat dst=new Mat();
        Mat tmp= new Mat();
        Mat alpha=mask1;

        Imgproc.cvtColor(src,tmp,Imgproc.COLOR_BGR2GRAY);

        List<Mat> rgb = new ArrayList<>();
        Core.split(src, rgb);
        List<Mat> rgba = new ArrayList<>();
        rgba.add(rgb.get(0));
        rgba.add(rgb.get(1));
        rgba.add(rgb.get(2));
        rgba.add(alpha);

        Core.merge(rgba, dst);
        
        String fullImageName = "full"+createFileUtils.getToday(1)+".png";
        String cropImageName = "result"+createFileUtils.getToday(1)+".png";
        
        Imgcodecs.imwrite(rootPath+fullImageName, dst);
        
        Mat cropImage = dst.submat(grabcutRectangleVo.getGrabcutRect());
        
        Imgcodecs.imwrite(rootPath+cropImageName, cropImage);
        resultMap.put("resultImg", "/resources/cvimgs/"+cropImageName);
        resultMap.put("backGroundFile", "/resources/cvimgs/"+backGroundFileName);
        resultMap.put("backGroundWidth", fg2.cols());
        resultMap.put("backGroundHeight", fg2.rows());
        resultMap.put("width", grabcutRectangleVo.getGrabcutRect().width);
        resultMap.put("height", grabcutRectangleVo.getGrabcutRect().height);
        return resultMap;
	}
	
	private void grabcutMaskSetting(Rect rect,int mode,ArrayList<Integer> bgListX,ArrayList<Integer> bgListY,Mat im
    		,ArrayList<Integer> fgListX,ArrayList<Integer> fgListY){
        byte[] buffer = new byte[3];
        staticBgListX.addAll(bgListX);
        staticBgListY.addAll(bgListY);
        staticFgListX.addAll(fgListX);
        staticFgListY.addAll(fgListY);
        	for (int x = rect.x; x <= rect.x+rect.width; x++) {
                for (int y = rect.y; y <= rect.y+rect.height; y++) {
                    mask1.get(y, x, buffer);
                    int value = buffer[0];
                    buffer[0] = Imgproc.GC_PR_FGD;
                    mask1.put(y, x, buffer);
                }
            }
        
        if(bgListX.size()>1){
        	for (int x = 1; x <= staticBgListX.size()-1; x++) {
        		mask1.get(staticBgListY.get(x), staticBgListX.get(x), buffer);
        		int value = buffer[0];
        		buffer[0] = Imgproc.GC_BGD;
        		mask1.put(staticBgListY.get(x), staticBgListX.get(x), buffer);
        	}
        	int count = 0;
        	for (int x = beX; x <= afX; x++) {
        		for (int y = beY; y <= afY; y++) {
        			mask1.get(y, x, buffer);
        			int value = buffer[0];
        			if(value==Imgproc.GC_BGD){
        				count++;
        			}
        		}
        	}
        	System.out.println("bgcount : "+count);
    	}
        if(fgListX.size()>1){
        	for (int x = 1; x <= staticFgListX.size()-1; x++) {
        		mask1.get(staticFgListY.get(x), staticFgListX.get(x), buffer);
        		int value = buffer[0];
        		buffer[0] = Imgproc.GC_FGD;
        		mask1.put(staticFgListY.get(x), staticFgListX.get(x), buffer);
        	}
        
        	int count = 0;
        	for (int x = beX; x <= afX; x++) {
        		for (int y = beY; y <= afY; y++) {
        			mask1.get(y, x, buffer);
        			int value = buffer[0];
        			if(value==Imgproc.GC_FGD){
        				count++;
        			}
        		}
        	}
        	System.out.println("fgcount : "+count);
        }
    }
	
	public void init(){
		staticBgListX=new ArrayList<Integer>();
		staticBgListY=new ArrayList<Integer>();
		staticFgListX=new ArrayList<Integer>();
		staticFgListY=new ArrayList<Integer>();
		if(staticBgListX.size()==0&&staticBgListY.size()==0){
		}else{
		}
	}
}
