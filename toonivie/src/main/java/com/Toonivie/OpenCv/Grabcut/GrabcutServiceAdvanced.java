package com.Toonivie.OpenCv.Grabcut;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.Ext_API;
import com.Toonivie.Util.ImageTypeConvert;

@Service
public class GrabcutServiceAdvanced { //�떎�젣 �씠誘몄� 遺꾨━ 肄붾뱶
	static Mat mask1;
	Mat bgModel;
	Mat fgModel;

	public HashMap<String, Object> grabcut(ArrayList<Integer> bgListX, ArrayList<Integer> bgListY,
			ArrayList<Integer> fgListX, ArrayList<Integer> fgListY, int mode,
			String imgUrl, String rootPath, int beX, int beY, int afX, int afY, int filling_type, Boolean blur_status, int blurSigmaXV, ArrayList<Integer> clicked_color_rgb,
			String[] gcBgdX, String[] gcBgdY, String[] gcFgdX, String[] gcFgdY, String[] gcprBgdX, String[] gcprBgdY, String[] gcprFgdX, String[] gcprFgdY) {// List

		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		CreateFileUtils cfu = new CreateFileUtils();
		
		
		
		// Rect 
		int grabcutInteger = Imgproc.GC_INIT_WITH_MASK;
		if (mode == 0) {
			grabcutInteger = Imgproc.GC_INIT_WITH_RECT; //吏곸궗媛곹삎 �궗�슜
		}
		if( gcBgdX != null || gcFgdX != null || gcprBgdX != null || gcprFgdX != null ) {
			grabcutInteger = Imgproc.GC_INIT_WITH_MASK;
		}
		System.out.println("###mode : " + mode);
		/*		//! Imread flags
		enum ImreadModes {
		       IMREAD_UNCHANGED  = -1, //!< If set, return the loaded image as is (with alpha channel, otherwise it gets cropped).
		       IMREAD_GRAYSCALE  = 0,  //!< If set, always convert image to the single channel grayscale image.
		       IMREAD_COLOR      = 1,  //!< If set, always convert image to the 3 channel BGR color image.
		       IMREAD_ANYDEPTH   = 2,  //!< If set, return 16-bit/32-bit image when the input has the corresponding depth, otherwise convert it to 8-bit.
		       IMREAD_ANYCOLOR   = 4,  //!< If set, the image is read in any possible color format.
		       IMREAD_LOAD_GDAL  = 8   //!< If set, use the gdal driver for loading the image.
		       http://m.blog.daum.net/kurtjim/13688750
		     };
		 */

		Mat im = Imgcodecs.imread(imgUrl, Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);

		// for sure background
		/*
			- 입력/결과 분할 영상은 4가지 값 중 하나가 될 수 있음.
			- cv::GC_BGD 확실히 배경에 속하는 화소 (예제의 직사각형 밖에 있는 화소)
			- cv::GC_FGC 확실히 전경에 속하는 화소
			- cv::GC_PR_BGD 배경에 속할 수도 있는 화소
			- cv::GC_PR_FGD 전경에 속할 수도 있는 화소 (예제에서 직사각형 내부에 화소 초기값) 
		 */
		//if (mask1 == null) {
			mask1 = new Mat(im.size(), CvType.CV_8UC1, new Scalar(Imgproc.GC_BGD));
		//}

		// �씠誘몄�遺꾨━�쁺�뿭 �꽕�젙
		Rect rect = new Rect();
		if (mode == 1) {
			rect = setRect(bgListX, bgListY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.x+rect.width;
			afY = rect.y+rect.height;
		} else {
			if(beX>afX){
				int temp = afX;
				afX=beX;
				beX=temp;
			}
			if(beY>afY){
				int tempY = afY;
				afY=beY;
				beY=tempY;
			}
			//rect = new Rect(beX, beY, afX - beX, afY - beY);
			rect = setRect_cali(beX, beY, afX, afY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.x + rect.width;
			afY = rect.y + rect.height;
		}
		System.out.println("### rect : " + rect.toString());
		grabcutMaskSetting(beX, beY, afX, afY, mode, bgListX, bgListY, im, fgListX, fgListY, rect, gcBgdX, gcBgdY, gcFgdX, gcFgdY, gcprBgdX, gcprBgdY, gcprFgdX, gcprFgdY); //grabcutMaskSetting
		System.out.println(beX + " beX " + beY + " beY " + afX + " afX " + afY + " afY ");
		
		
		
		// bgModel, fgModel �꽑�뼵
/*		Mat bgModel = new Mat();
		Mat fgModel = new Mat();*/
		bgModel = new Mat();
		fgModel = new Mat();

		Mat source = new Mat(1, 1, CvType.CV_8U, new Scalar(3));

		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im = imagetypeconvert.bgraTobgr(im.clone());
		Imgproc.grabCut(im, mask1, rect, bgModel, fgModel, 1, grabcutInteger); //grabcut
		
//		Imgproc.grabCut(im, mask1, rect, bgModel, fgModel, 5); //grabcut
		System.out.println("mask log: col"+ mask1.cols() + " row " + mask1.rows());
		// �쟾寃쎌씪 媛��뒫�꽦�씠 �엳�뒗 �솕�냼瑜� 留덊겕�븳 寃껋쓣 媛��졇�삤湲�
		Core.compare(mask1, source, mask1, Core.CMP_EQ);
		Mat fg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		// 諛곌꼍 �솕�냼�뒗 蹂듭궗�릺吏� �븡�
		im.copyTo(fg, mask1);


		Mat fg2 = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		Core.subtract(im, fg, fg2); //subtract
		
		
		// �쁾遺꾨━�맂 �쁺�뿭�쓣 梨꾩슦�뒗 肄붾뱶, checked shkwak, 2017-11-16
		//Imgcodecs.imwrite("mask1.png", mask1); //二쇱꽍泥섎━ by shkwak, 2018-02-08
		int rectWidth = afX - beX;
		int rectHeight = afY - beY;
		//遺꾨━�맂 �쁺�뿭 �깋�긽 蹂�寃�  add by KH 2017-12-21
		//Imgcodecs.imwrite("testmask.png", mask1);
		if(filling_type == 0) {
			System.out.println("afY: "+afY);
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					//System.out.println("i: "+ i + " j: " + j);
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 0, 0, 0 };
						/*putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type == 1) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 255, 255, 255 };
						/*putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type == 2) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth / 2; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					//System.out.println(mask1Pixel[0]);
					if (mask1Pixel[0] > 150) {
						//System.out.println("in");
						double[] putPixel = { 255, 255, 255 };
						putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];

						fg2.put(i, j, putPixel);
					}
				}
			}
			for (int i = beY; i <= afY; i++) {
				for (int j = afX; j > beX + rectWidth / 2; j--) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 255, 255, 255 };
						int jp10 = j + 10;
						if (jp10 > im.width()) {
							jp10 = im.width();
						}
						putPixel[0] = fg2.get(i, jp10 - 1)[0];
						putPixel[1] = fg2.get(i, jp10 - 1)[1];
						putPixel[2] = fg2.get(i, jp10 - 1)[2];

						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type ==3) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { clicked_color_rgb.get(2), clicked_color_rgb.get(1), clicked_color_rgb.get(0) };
/*						putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
			
		}else if(filling_type==4) {
		}


		Mat blurMask = fg2.submat(rect);
		if(blur_status) {
			Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), blurSigmaXV);
		}

		//Imgcodecs.imwrite("fgGrabcut2.png", fg2); //二쇱꽍泥섎━ by shkwak, 2018-02-08
		//Imgcodecs.imwrite("fgGrabcut3.png", blurMask);

		Mat bgMask = new Mat(im.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
		Mat src = fg.clone();
		Mat dst = new Mat();
		Mat tmp = new Mat();
		Mat alpha = mask1.clone();

		Imgproc.cvtColor(src, tmp, Imgproc.COLOR_BGR2GRAY);

		List<Mat> rgb = new ArrayList<>();
		Core.split(src, rgb);
		List<Mat> rgba = new ArrayList<>();
		rgba.add(rgb.get(0));
		rgba.add(rgb.get(1));
		rgba.add(rgb.get(2));
		rgba.add(alpha);
		Core.merge(rgba, dst);

		// 
		Imgcodecs.imwrite(rootPath+"/resources/cvimgs/grabcut/" + "full.png", dst);

		Mat cropImage = dst.submat(rect);

		// 
		File grabcutResultDir = new File(rootPath+"/grabcut/");
		if(grabcutResultDir.exists()==false) {
			grabcutResultDir.mkdirs();
		}
		String backGroundFileName = "backGround"+cfu.getToday(1)+".png";
        
        Imgcodecs.imwrite(rootPath+backGroundFileName, fg2);
        
        //adding alpha channel to BG
		rgb = new ArrayList<>();
		dst = new Mat();
		rgba = new ArrayList<>();
		Core.bitwise_not(alpha, alpha);
		Core.split(fg2, rgb);
		rgba.add(rgb.get(0));
		rgba.add(rgb.get(1));
		rgba.add(rgb.get(2));
		rgba.add(alpha);
		Core.merge(rgba, dst);
		//debuging by KH 180426
	/*	Imgcodecs.imwrite("TESET_targetBG.png", dst);
		System.out.println("dst check alpha channel ::::::::::::::::"+dst.channels());*/
		/*		System.out.println("fg2 check alpha channel ::::::::::::::::"+fg2.channels());
        Imgcodecs.imwrite("TESET.png", mask1);
        Imgcodecs.imwrite("TESET_BG.png", fg2);
        Imgcodecs.imwrite("TESET_targetBG.png", dst);*/
		//System.out.println("###grabcut Path : "+rootPath+"/grabcut/" + "grabcut.png");
		
        String resultImageName = cfu.getToday(0)+".png";
        //auto filling api set by KH 180503
        if(filling_type==4) {
        	Imgcodecs.imwrite(rootPath+backGroundFileName, dst);
        	Ext_API inpainting = new Ext_API();
        	String inpainting_file = inpainting.input_BG_img(rootPath+backGroundFileName, backGroundFileName, rootPath);
        	Mat inpainted_bg = after_inpainting(im, rect, inpainting_file, rootPath);
    		if(blur_status) {
    			blurMask = inpainted_bg.submat(rect);
    			Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), blurSigmaXV);
    		}
        	Imgcodecs.imwrite(rootPath+backGroundFileName, inpainted_bg);
        }
        
        Imgcodecs.imwrite(rootPath+"/grabcut/" + resultImageName, cropImage);
		resultMap.put("resultImg", "/resources/cvimgs/grabcut/"+resultImageName);
		resultMap.put("backGroundFile", "/resources/cvimgs/"+backGroundFileName);
        resultMap.put("backGroundWidth", fg2.cols());
        resultMap.put("backGroundHeight", fg2.rows());
		resultMap.put("width", rect.width);
		resultMap.put("height", rect.height);
		resultMap.put("beX", beX);
		resultMap.put("beY", beY);


		return resultMap;
	}
	
	//for Object Detection by KH 180330
	public HashMap<String, Object> grabcut(int mode,
			String imgUrl, String rootPath, String Object_type, int beX, int beY, int afX, int afY, int filling_type, Boolean blur_status, ArrayList<Integer> clicked_color_rgb, String autofill_bg) {// List

		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		CreateFileUtils cfu = new CreateFileUtils();
		
		// Rect 
		int grabcutInteger = Imgproc.GC_INIT_WITH_MASK;
		if (mode == 0) {
			grabcutInteger = Imgproc.GC_INIT_WITH_RECT; //吏곸궗媛곹삎 �궗�슜
		}

		System.out.println("###mode : " + mode);
		/*		//! Imread flags
		enum ImreadModes {
		       IMREAD_UNCHANGED  = -1, //!< If set, return the loaded image as is (with alpha channel, otherwise it gets cropped).
		       IMREAD_GRAYSCALE  = 0,  //!< If set, always convert image to the single channel grayscale image.
		       IMREAD_COLOR      = 1,  //!< If set, always convert image to the 3 channel BGR color image.
		       IMREAD_ANYDEPTH   = 2,  //!< If set, return 16-bit/32-bit image when the input has the corresponding depth, otherwise convert it to 8-bit.
		       IMREAD_ANYCOLOR   = 4,  //!< If set, the image is read in any possible color format.
		       IMREAD_LOAD_GDAL  = 8   //!< If set, use the gdal driver for loading the image.
		       http://m.blog.daum.net/kurtjim/13688750
		     };*/

		Mat im = Imgcodecs.imread(imgUrl);
		//Mat im = Imgcodecs.imread(imgUrl, -1);
		//Imgproc.cvtColor(im, im, Imgproc.COLOR_RGBA2RGB);
		//Imgcodecs.imwrite(rootPath + "ttest"+cfu.getToday(1)+".png", im);
		
		// for sure background
		/*- �엯�젰/寃곌낵 遺꾪븷 �쁺�긽�� 4媛�吏� 媛� 以� �븯�굹媛� �맆 �닔 �엳�쓬.
		- cv::GC_BGD �솗�떎�엳 諛곌꼍�뿉 �냽�븯�뒗 �솕�냼 (�삁�젣�쓽 吏곸궗媛곹삎 諛뽰뿉 �엳�뒗 �솕�냼)
		- cv::GC_FGC �솗�떎�엳 �쟾寃쎌뿉 �냽�븯�뒗 �솕�냼
		- cv::GC_PR_BGD 諛곌꼍�뿉 �냽�븷 �닔�룄 �엳�뒗 �솕�냼
		- cv::GC_PR_FGD �쟾寃쎌뿉 �냽�븷 �닔�룄 �엳�뒗 �솕�냼 (�삁�젣�뿉�꽌 吏곸궗媛곹삎 �궡遺��뿉 �솕�냼 珥덇린媛�)*/
		//if (mask1 == null) {
			mask1 = new Mat(im.size(), CvType.CV_8UC1, new Scalar(Imgproc.GC_BGD));
		//}

		// �씠誘몄�遺꾨━�쁺�뿭 �꽕�젙
		Rect rect = new Rect();
		if (mode == 1) {
/*			rect = setRect(bgListX, bgListY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.x+rect.width;
			afY = rect.y+rect.height;*/
		} else {
			if(beX>afX){
				int temp = afX;
				afX=beX;
				beX=temp;
			}
			if(beY>afY){
				int tempY = afY;
				afY=beY;
				beY=tempY;
			}
//			rect = new Rect(beX, beY, afX - beX, afY - beY);
			rect = setRect_cali(beX, beY, afX, afY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.width + rect.x;
			afY = rect.height + rect.y;
		}
		System.out.println("### rect : " + rect.toString());
		grabcutMaskSetting(beX, beY, afX, afY, mode, im, rect); //grabcutMaskSetting
		
		// bgModel, fgModel �꽑�뼵
		Mat bgModel = new Mat();
		Mat fgModel = new Mat();

		Mat source = new Mat(1, 1, CvType.CV_8U, new Scalar(3));

		// 
		Imgproc.grabCut(im, mask1, rect, bgModel, fgModel, 1, grabcutInteger); //grabcut

		// �쟾寃쎌씪 媛��뒫�꽦�씠 �엳�뒗 �솕�냼瑜� 留덊겕�븳 寃껋쓣 媛��졇�삤湲�
		Core.compare(mask1, source, mask1, Core.CMP_EQ);
		
		Mat fg = new Mat(im.size(), CvType.CV_8UC4, new Scalar(0, 0, 0, 0));
		// 諛곌꼍 �솕�냼�뒗 蹂듭궗�릺吏� �븡�쓬
		im.copyTo(fg, mask1);



		Mat fg2 = new Mat(im.size(), CvType.CV_8UC4, new Scalar(0, 0, 0, 0));
		Core.subtract(im, fg, fg2); //subtract
		
		
		// �쁾遺꾨━�맂 �쁺�뿭�쓣 梨꾩슦�뒗 肄붾뱶, checked shkwak, 2017-11-16
		//Imgcodecs.imwrite("mask1.png", mask1); //二쇱꽍泥섎━ by shkwak, 2018-02-08
		int rectWidth = afX - beX;
		int rectHeight = afY - beY;

		//遺꾨━�맂 �쁺�뿭 �깋�긽 蹂�寃�  add by KH 2017-12-21
		if(filling_type == 0) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 0, 0, 0 };
						/*putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type == 1) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 255, 255, 255 };
						/*putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type == 2) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth / 2; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					//System.out.println(mask1Pixel[0]);
					if (mask1Pixel[0] > 150) {
						//System.out.println("in");
						double[] putPixel = { 255, 255, 255 };
						putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];

						fg2.put(i, j, putPixel);
					}
				}
			}
			for (int i = beY; i <= afY; i++) {
				for (int j = afX; j > beX + rectWidth / 2; j--) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { 255, 255, 255 };
						int jp10 = j + 10;
						if (jp10 > im.width()) {
							jp10 = im.width();
						}
						putPixel[0] = fg2.get(i, jp10 - 1)[0];
						putPixel[1] = fg2.get(i, jp10 - 1)[1];
						putPixel[2] = fg2.get(i, jp10 - 1)[2];

						fg2.put(i, j, putPixel);
					}
				}
			}
		}else if(filling_type ==3) {
			for (int i = beY; i <= afY; i++) {
				for (int j = beX; j <= beX + rectWidth; j++) {
					double[] mask1Pixel = mask1.get(i, j);
					if (mask1Pixel[0] > 150) {
						double[] putPixel = { clicked_color_rgb.get(2), clicked_color_rgb.get(1), clicked_color_rgb.get(0) };
						/*putPixel[0] = fg2.get(i, j - 10)[0];
						putPixel[1] = fg2.get(i, j - 10)[1];
						putPixel[2] = fg2.get(i, j - 10)[2];*/
						fg2.put(i, j, putPixel);
					}
				}
			}
		}



		Mat blurMask = fg2.submat(rect);
		if(blur_status) {
			Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), 11);
		}

		//Imgcodecs.imwrite("fgGrabcut2.png", fg2); //二쇱꽍泥섎━ by shkwak, 2018-02-08
		//Imgcodecs.imwrite("fgGrabcut3.png", blurMask);

		Mat bgMask = new Mat(im.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
		Mat src = fg.clone();
		Mat dst = new Mat();
		Mat tmp = new Mat();
		Mat alpha = mask1.clone();

		Imgproc.cvtColor(src, tmp, Imgproc.COLOR_BGR2GRAY);

		List<Mat> rgb = new ArrayList<>();
		Core.split(src, rgb);
		List<Mat> rgba = new ArrayList<>();
		rgba.add(rgb.get(0));
		rgba.add(rgb.get(1));
		rgba.add(rgb.get(2));
		rgba.add(alpha);

		Core.merge(rgba, dst);

		// 
		Imgcodecs.imwrite(rootPath+"/resources/cvimgs/grabcut/" + "full.png", dst);

		Mat cropImage = dst.submat(rect);

		// 
		File grabcutResultDir = new File(rootPath+"/grabcut/");
		if(grabcutResultDir.exists()==false) {
			grabcutResultDir.mkdirs();
		}
		String backGroundFileName = "backGround"+cfu.getToday(1)+".png";
        
        Imgcodecs.imwrite(rootPath+backGroundFileName, fg2);

        
		//System.out.println("###grabcut Path : "+rootPath+"/grabcut/" + "grabcut.png");
		
        String resultImageName = cfu.getToday(0)+".png";
        
        
        //auto filling api set by KH 180503
        if(filling_type==4) {
        	Imgcodecs.imwrite(rootPath+backGroundFileName, dst);
        	Ext_API inpainting = new Ext_API();
        	if(autofill_bg == null) {
        		String inpainting_file = inpainting.input_BG_img(rootPath+backGroundFileName, backGroundFileName, rootPath);
        		autofill_bg = inpainting_file;
        		resultMap.put("autofill_bg", autofill_bg);
        	}
        	Mat inpainted_bg = after_inpainting(im, rect, autofill_bg, rootPath);
    		if(blur_status) {
    			blurMask = inpainted_bg.submat(rect);
    			Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), 11);
    		}
        	Imgcodecs.imwrite(rootPath+backGroundFileName, inpainted_bg);
        }
        Imgcodecs.imwrite(rootPath+"/grabcut/" + resultImageName, cropImage);
		resultMap.put("resultImg", "/resources/cvimgs/grabcut/"+resultImageName);
		resultMap.put("backGroundFile", "/resources/cvimgs/"+backGroundFileName);
		resultMap.put("backGroundFileName", backGroundFileName);
        resultMap.put("backGroundWidth", fg2.cols());
        resultMap.put("backGroundHeight", fg2.rows());
		resultMap.put("width", rect.width);
		resultMap.put("height", rect.height);
		resultMap.put("Object_type",  Object_type);
		


		return resultMap;
	}

	private void grabcutMaskSetting(int beX, int beY, int afX, int afY, int mode, ArrayList<Integer> bgListX,
			ArrayList<Integer> bgListY, Mat im, ArrayList<Integer> fgListX,
			ArrayList<Integer> fgListY, Rect rect, String[] gcBgdX, String[] gcBgdY, String[] gcFgdX, String[] gcFgdY, String[] gcprBgdX, String[] gcprBgdY, String[] gcprFgdX, String[] gcprFgdY) {
		
		mask1.setTo(new Scalar(Imgproc.GC_BGD));
		Imgproc.rectangle(mask1, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height ), new Scalar(Imgproc.GC_PR_FGD),-1);
		
		if(gcBgdX != null) {
			for (int i = 0; i < gcBgdX.length; i++) {
				Imgproc.circle( mask1, new Point( Integer.parseInt(gcBgdX[i]), Integer.parseInt(gcBgdY[i]) ), 2, new Scalar(Imgproc.GC_BGD), -1 );
			}
		}
		if(gcFgdX != null) {
			for (int i = 0; i < gcFgdX.length; i++) {
				Imgproc.circle( mask1, new Point( Integer.parseInt(gcFgdX[i]), Integer.parseInt(gcFgdY[i]) ), 2, new Scalar(Imgproc.GC_FGD), -1 );
			}
		}
		if(gcprBgdX != null) {
			for (int i = 0; i < gcprBgdX.length; i++) {
				Imgproc.circle( mask1, new Point( Integer.parseInt(gcprBgdX[i]), Integer.parseInt(gcprBgdY[i]) ), 2, new Scalar(Imgproc.GC_PR_BGD), -1 );
			}
		}
		if(gcprFgdX != null) {
			for (int i = 0; i < gcprFgdX.length; i++) {
				Imgproc.circle( mask1, new Point( Integer.parseInt(gcprFgdX[i]), Integer.parseInt(gcprFgdY[i]) ), 2, new Scalar(Imgproc.GC_PR_FGD), -1 );
			}
		}
		
		if (bgListX.size() > 1) {
			for (int x = 1; x < bgListX.size() - 2; x++) {
				Imgproc.line(mask1, new Point(bgListX.get(x), bgListY.get(x)),
						new Point(bgListX.get(x + 1), bgListY.get(x + 1)), new Scalar(Imgproc.GC_BGD), 10);
			}
		}
		if (bgListX.size() > 1) {
			Imgproc.line(mask1, new Point(bgListX.get(1), bgListY.get(1)),
					new Point(bgListX.get(bgListX.size() - 2), bgListY.get(bgListY.size() - 2)),
					new Scalar(Imgproc.GC_BGD), 10);
		}
	}

	//for Object Detection by KH 180330
	private void grabcutMaskSetting(int beX, int beY, int afX, int afY, int mode, Mat im, Rect rect) {
		byte[] buffer = new byte[3];
//		System.out.println("### rect " + rect.x + "," + rect.width + "    " + rect.y + "," + rect.height);
		System.out.println("### rect  beX,beY" + beX + "," + beY + "   afx,afy " + afX + "," + afY);
		for (int x = rect.x; x < rect.x + rect.width; x++) {
			for (int y = rect.y; y < rect.y + rect.height; y++) {
				mask1.get(y, x, buffer);
				int value = buffer[0];
				buffer[0] = Imgproc.GC_PR_FGD; // for sure background
				mask1.put(y, x, buffer);
			}
		}
/*		if (bgListX.size() > 1) {
			for (int x = 1; x < bgListX.size() - 2; x++) {
				Imgproc.line(mask1, new Point(bgListX.get(x), bgListY.get(x)),
						new Point(bgListX.get(x + 1), bgListY.get(x + 1)), new Scalar(Imgproc.GC_BGD), 10);
			}
		}
		if (bgListX.size() > 1) {
			Imgproc.line(mask1, new Point(bgListX.get(1), bgListY.get(1)),
					new Point(bgListX.get(bgListX.size() - 2), bgListY.get(bgListY.size() - 2)),
					new Scalar(Imgproc.GC_BGD), 10);
		}*/

		bfsMat(rect);

		int count = 0;
		for (int x = beX; x <= afX; x++) {
			for (int y = beY; y <= afY; y++) {
				mask1.get(y, x, buffer);
				int value = buffer[0];
				if (value == Imgproc.GC_BGD) {
					count++;
				}
			}
		}
		System.out.println("bgcount : " + count);

	}

	private void bfsMat(Rect rect) {
		Queue<Point> q = new LinkedList<Point>();
		q.add(new Point(rect.x, rect.y));
		bfs(q, rect);
	}

	private void bfs(Queue<Point> q, Rect r) {

		int count = 0;

		while (!q.isEmpty()) {
			// System.out.println(q.size());
			Point p = q.poll();
			if (p.x + 1 <= r.x + r.width && p.y <= r.y + r.height) {
				byte[] buffer = new byte[3];
				mask1.get((int) p.y, (int) p.x + 1, buffer);
				int value = buffer[0];
				if (value == Imgproc.GC_PR_FGD) {
					mask1.put((int) p.y, (int) p.x + 1, Imgproc.GC_BGD);
					q.add(new Point(p.x + 1, p.y));
				}
			}
			if (p.x <= r.x + r.width && p.y + 1 <= r.y + r.height) {
				byte[] buffer = new byte[3];
				mask1.get((int) p.y + 1, (int) p.x, buffer);
				int value = buffer[0];
				if (value == Imgproc.GC_PR_FGD) {
					mask1.put((int) p.y + 1, (int) p.x, Imgproc.GC_BGD);
					q.add(new Point(p.x, p.y + 1));
				}
			}
			if (p.x - 1 >= r.x && p.y <= r.y + r.height) {
				byte[] buffer = new byte[3];
				mask1.get((int) p.y, (int) p.x - 1, buffer);
				int value = buffer[0];
				if (value == Imgproc.GC_PR_FGD) {
					mask1.put((int) p.y, (int) p.x - 1, Imgproc.GC_BGD);
					q.add(new Point(p.x - 1, p.y));
				}
			}
			if (p.x <= r.x + r.width && p.y - 1 >= r.y) {
				byte[] buffer = new byte[3];
				mask1.get((int) p.y - 1, (int) p.x, buffer);
				int value = buffer[0];
				if (value == Imgproc.GC_PR_FGD) {
					mask1.put((int) p.y - 1, (int) p.x, Imgproc.GC_BGD);
					q.add(new Point(p.x, p.y - 1));
				}
			}
			count++;
		}
	}

	private Rect setRect(ArrayList<Integer> bgListX, ArrayList<Integer> bgListY, Mat im) {
		int minX = bgListX.get(1);
		int minY = bgListY.get(1);
		int maxX = bgListX.get(1);
		int maxY = bgListY.get(1);
		System.out.println("###bgListX.size() : " + bgListX.size());
		for (int x = 0; x < bgListX.size() - 1; x++) {
			if (bgListX.get(x) > maxX) {
				maxX = bgListX.get(x);
			}
			if (bgListX.get(x) < minX) {
				minX = bgListX.get(x);
			}
			if (bgListY.get(x) > maxY) {
				maxY = bgListY.get(x);
			}
			if (bgListY.get(x) < minY) {
				minY = bgListY.get(x);
			}
		}
		System.out.println("### free " + minX + "," + minY + "    " + maxX + "," + maxY);
		if(minX < 1) {
			minX = 1;
		}
		if(minY < 1) {
			minY = 1;
		}
		if(maxX >= im.width()) {
			maxX = im.width()-1;
		}
		if(maxY >= im.height()) {
			maxY = im.height()-1;
		}
/*		if (minX - 10 < 0) {
			minX = 0;
		} else {
			minX = minX - 10;
		}
		if (maxX + 10 > im.cols()) {
			maxX = im.cols();
		} else {
			maxX = maxX + 10;
		}
		if (minY - 10 < 0) {
			minY = 0;
		} else {
			minY = minY - 10;
		}
		if (maxY + 10 > im.rows()) {
			maxY = im.rows();
		} else {
			maxY = maxY + 10;
		}*/
		
		System.out.println("### free " + maxX + "," + im.width() + "    " + maxY + "," + im.height());
		System.out.println("### free " + minX + "," + minY + "    " + maxX + "," + maxY);
		return new Rect(minX, minY, maxX - minX, maxY - minY);
	}
	//calibrate bex bey afx afy by KH 180403
	private Rect setRect_cali(int beX, int beY, int afX, int afY, Mat im) {
		System.out.println("before -- bex: " + beX +" bey: " + beY + " afx: " + afX + " afy: " + afY);
		if(beX <= 1) {
			beX = 1;
		}
		if(beY <= 1) {
			beY = 1;
		}
		if(afX >= im.width()) {
			afX = im.width()-1;
		}
		if(afY >= im.height()) {
			afY = im.height()-1;
		}
		System.out.println("im.width(): " + im.width() +" im.height(): " + im.height());
		System.out.println("after -- bex: " + beX +" bey: " + beY + " afx: " + afX + " afy: " + afY);
		return new Rect(beX, beY, afX - beX, afY - beY);
	}
	
	public HashMap<String, Object> make_background(String imgUrl, String rootPath, int filling_type, ArrayList<Integer> clicked_color_rgb) {// List
		HashMap<String, Object> resultMap = new HashMap<>();
		double r = 0;
		double g = 0;
		double b = 0;
		if(filling_type == 0) {
			r = 0;
			g = 0;
			b = 0;
		}else if(filling_type == 1) {
			r = 255;
			g = 255;
			b = 255;
		}else if(filling_type == 3) {
			r = (double)(clicked_color_rgb.get(0));
			g = (double)(clicked_color_rgb.get(1));
			b = (double)(clicked_color_rgb.get(2));
		}
		System.out.println("BGcolor set "+r +","+ g +","+ b);
		System.out.println(imgUrl);
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		CreateFileUtils cfu = new CreateFileUtils();
		String backGroundFileName = "backGroundcolor"+cfu.getToday(1)+".png";
		String FileName = cfu.getToday(1)+".png";
		Mat im = Imgcodecs.imread(imgUrl, -1);
		//make bg
		Mat bgMask = new Mat(im.size(), CvType.CV_8UC3, new Scalar(b, g, r));
		Imgcodecs.imwrite(rootPath + backGroundFileName, bgMask);
		Imgcodecs.imwrite(rootPath + FileName, im);
		
		resultMap.put("resultImg", "/resources/cvimgs/"+FileName);
		resultMap.put("backGroundFile", "/resources/cvimgs/"+backGroundFileName);
        resultMap.put("backGroundWidth", im.cols());
        resultMap.put("backGroundHeight", im.rows());
		resultMap.put("width", im.cols());
		resultMap.put("height", im.rows());
		return resultMap;
		
	}
	//for mask grabcut by KH 180426
	public HashMap<String, Object> grabcut(String imgUrl_BG, Rect rect, String grabcutBG, String grabcutFG, String rootPath, String mask_path, int filling_type, Boolean blur_status, ArrayList<Integer> clicked_color_rgb) {// List
		
		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		CreateFileUtils cfu = new CreateFileUtils();
		
		//original image mat
		Mat im = Imgcodecs.imread(imgUrl_BG);
		Mat FG = Imgcodecs.imread(grabcutFG, 0);
		
		//reset model after RECT grabcut
		bgModel = new Mat();
		fgModel = new Mat();
		
		//mask (grayscale)
		Mat grabcut_mask = Imgcodecs.imread(rootPath+"grabcut_mask.png", 0);
		
		//make BG mask
		//mask1 = last mask image(from RECT grabcut)
		Mat fg_mask = mask1.clone();
		Mat bg_roi = fg_mask.submat(rect);
		Core.add(grabcut_mask, bg_roi, bg_roi);
		mask1 = fg_mask.clone();
		
		//grabcut with mask option
		convertToOpencvValues(fg_mask);
		Imgproc.grabCut(im, fg_mask, rect, bgModel, fgModel, 1, Imgproc.GC_INIT_WITH_MASK); //grabcut
		convertToHumanValues(fg_mask);
		
		//extract bg and fg 
		Mat fg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		im.copyTo(fg, fg_mask);
		Mat fg2 = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		Core.subtract(im, fg, fg2); //subtract
		fg = fg.submat(rect);
		
		//add alpha channel to fg img 
		Mat src = fg.clone();
		Mat dst = new Mat();
		Mat alpha = fg_mask.clone();
		alpha = alpha.submat(rect);
		List<Mat> rgb = new ArrayList<>();
		Core.split(src, rgb);
		List<Mat> rgba = new ArrayList<>();
		rgba.add(rgb.get(0));
		rgba.add(rgb.get(1));
		rgba.add(rgb.get(2));
		rgba.add(alpha);
		Core.merge(rgba, dst);
		
/*		Imgcodecs.imwrite("grabcut_mask.png", grabcut_mask);
		Imgcodecs.imwrite("testMASKIMG.png", mask1);*/
        String resultImageName = cfu.getToday(0)+".png";
        Imgcodecs.imwrite(rootPath+"/grabcut/" + resultImageName, dst);
		resultMap.put("resultImg", "/resources/cvimgs/grabcut/"+resultImageName);
		resultMap.put("width", rect.width);
		resultMap.put("height", rect.height);



		return resultMap;
	}
	
    private static void convertToHumanValues(Mat mask) {
        byte[] buffer = new byte[3];
        for (int x = 0; x < mask.rows(); x++) {
            for (int y = 0; y < mask.cols(); y++) {
                mask.get(x, y, buffer);
                int value = buffer[0];
                if (value == Imgproc.GC_BGD) {
                    buffer[0] = 0; // for sure background
                } else if (value == Imgproc.GC_PR_BGD) {
                    buffer[0] = 85; // probably background
                } else if (value == Imgproc.GC_PR_FGD) {
                    buffer[0] = (byte) 170; // probably foreground
                } else {
                    buffer[0] = (byte) 255; // for sure foreground

                }
                mask.put(x, y, buffer);
            }
        }
    }
    //픽셀 단위로 비교해서 원본 mask1과 비교하여, 스트로크 값을 mask1에 삽입 하는 함수 제작. 
    
    private static void convertToOpencvValues(Mat mask) {
        byte[] buffer = new byte[3];
        for (int x = 0; x < mask.rows(); x++) {
            for (int y = 0; y < mask.cols(); y++) {
                mask.get(x, y, buffer);
                int value = buffer[0];
                if (value >= 0 && value < 64) {
                    buffer[0] = Imgproc.GC_BGD; // for sure background
                } else if (value >= 64 && value < 128) {
                    buffer[0] = Imgproc.GC_PR_BGD; // probably background
                } else if (value >= 128 && value < 192) {
                    buffer[0] = Imgproc.GC_PR_FGD; // probably foreground
                } else {
                    buffer[0] = Imgproc.GC_FGD; // for sure foreground

                }
                mask.put(x, y, buffer);
            }
        }
    }
    
    public Mat after_inpainting(Mat original_img, Rect rect, String img_name, String rootpath) {
    	//resize img 
    	Mat im = original_img.clone();
    	Mat inpainted = Imgcodecs.imread(rootpath+"/inpainting/"+img_name);
    	Imgproc.resize(inpainted, inpainted, im.size());
    	Imgcodecs.imwrite(rootpath+"/inpainting/"+img_name, inpainted);
    	
    	Mat fg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
    	im.copyTo(fg, mask1);
		Mat bg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		Core.subtract(im, fg, bg); //subtract
		inpainted.copyTo(bg, mask1);
		
    	fg = fg.submat(rect);
		Mat src = fg.clone();
		Mat dst = new Mat();
		Mat alpha = mask1.clone();
		alpha = alpha.submat(rect);
		
		List<Mat> rgb = new ArrayList<>();
		Core.split(src, rgb);
		List<Mat> rgba = new ArrayList<>();
		rgba.add(rgb.get(0));
		rgba.add(rgb.get(1));
		rgba.add(rgb.get(2));
		rgba.add(alpha);
		Core.merge(rgba, dst);
		
		Mat result_roi = bg.submat(rect);
		dst.copyTo(result_roi);
		
		return bg;
    }
}
