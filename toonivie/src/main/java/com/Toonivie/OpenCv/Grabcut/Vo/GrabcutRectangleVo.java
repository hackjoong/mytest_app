package com.Toonivie.OpenCv.Grabcut.Vo;
import org.opencv.core.Rect;
public class GrabcutRectangleVo {
	int beX;
    int beY;
    int afX;
    int afY;
    public GrabcutRectangleVo(int beX,int beY,int afX,int afY){
    	if(beX>afX){
    		this.beX=afX;
    		this.afX=beX;
    	}else{
    		this.beX=beX;
    		this.afX=afX;
    	}
    	if(beY>afY){
    		this.beY=afY;
    		this.afY=beY;
    	}else{
    		this.beY=beY;
    		this.afY=afY;
    	}
    	
    }
    private GrabcutRectangleVo(){
    	
    }
    public Rect getGrabcutRect(){
    	return new Rect(beX,beY,afX-beX,afY-beY);
    }
    
	public int getBeX() {
		return beX;
	}
	public void setBeX(int beX) {
		this.beX = beX;
	}
	public int getBeY() {
		return beY;
	}
	public void setBeY(int beY) {
		this.beY = beY;
	}
	public int getAfX() {
		return afX;
	}
	public void setAfX(int afX) {
		this.afX = afX;
	}
	public int getAfY() {
		return afY;
	}
	public void setAfY(int afY) {
		this.afY = afY;
	}
    
}
