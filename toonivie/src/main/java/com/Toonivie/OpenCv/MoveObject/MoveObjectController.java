package com.Toonivie.OpenCv.MoveObject;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Util.CreateFileUtils;

@Controller
@RequestMapping("/moveObject")
public class MoveObjectController {
	@Autowired
	MoveObjectService moveObjectService;

	@RequestMapping(value = "/uploadImg", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> testFile(MultipartHttpServletRequest req, HttpSession session,
			HttpServletRequest request) throws Exception {
		HashMap<String, String> resultMap = new HashMap<>();
		MultipartFile fileFore = req.getFile("fore");
		MultipartFile fileBack = req.getFile("back");
		CreateFileUtils cfu = new CreateFileUtils();
		String filenameFore = "fore" + cfu.getToday(1) + "." + cfu.getFileType(fileFore.getOriginalFilename());
		String filenameBack = "back" + cfu.getToday(1) + "." + cfu.getFileType(fileBack.getOriginalFilename());
		cfu.CreateFile(fileFore, request, "/resources/cvimgs/", filenameFore);
		cfu.CreateFile(fileBack, request, "/resources/cvimgs/", filenameBack);
		File imgFileFore = new File(
				request.getSession().getServletContext().getRealPath("resources/cvimgs/") + filenameFore);
		File imgFileBack = new File(
				request.getSession().getServletContext().getRealPath("resources/cvimgs/") + filenameBack);
		BufferedImage biFore = ImageIO.read(imgFileFore);
		BufferedImage biBack = ImageIO.read(imgFileBack);
		resultMap.put("foreUrl", filenameFore);
		resultMap.put("backUrl", filenameBack);
		return resultMap;
	}

	@RequestMapping(value = "/applyMoveObject", method = RequestMethod.POST) //no use?
	@ResponseBody
	public HashMap<String, Object> applyMoveObject(HttpServletRequest request) {
		String foreUrl = request.getParameter("foreUrl");
		String backUrl = request.getParameter("backUrl");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		int mode = Integer.parseInt(request.getParameter("mode"));
		return moveObjectService.applyMoveObject(foreUrl, backUrl, mode, rootPath, request);
	}

	@RequestMapping(value = "/moveExe", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> moveExe(HttpServletRequest request) {
		String data = request.getParameter("str");
		System.out.println(data);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String effectKind = jsonObject.get("effectKind").toString();
		System.out.println(effectKind);

		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);
		System.out.println(effectTimeString);
		
		JSONArray firstInfo = (JSONArray) jsonObject.get("firstInfo");
		System.out.println(firstInfo.toString());

		JSONArray lastInfo = (JSONArray) jsonObject.get("lastInfo");
		System.out.println(lastInfo.toString());
		
		JSONObject background = (JSONObject)jsonObject.get("background"); 
		
		String rootPath = request.getServletContext().getRealPath("/");
		System.out.println("rootPath : "+rootPath);
		return moveObjectService.exe(background,firstInfo,lastInfo,effectTime,effectKind,rootPath,request);
	}
	
	@RequestMapping("/moveTest")
	public void moveTest(HttpServletRequest request){
		String data="{\"firstInfo\" : [{\"top\":486.5,\"left\":414.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170606175435.png\"}],\"lastInfo\" : [{\"top\":401.5,\"left\":588.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170606175435.png\"}],\"effectKind\":\"move\",\"effectTime\":\"3\",\"background\" : {\"imgUrl\":\"http://localhost:8088/resources/cvimgs/backGround20170606175435.png\",\"top\":298.5,\"left\":475.75}}";
		System.out.println(data);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String effectKind = jsonObject.get("effectKind").toString();
		System.out.println(effectKind);

		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);
		System.out.println(effectTimeString);
		
		JSONArray firstInfo = (JSONArray) jsonObject.get("firstInfo");
		System.out.println(firstInfo.toString());

		JSONArray lastInfo = (JSONArray) jsonObject.get("lastInfo");
		System.out.println(lastInfo.toString());
		
		JSONObject background = (JSONObject)jsonObject.get("background"); 
		
		String rootPath = request.getServletContext().getRealPath("/");
		moveObjectService.exe(background,firstInfo,lastInfo,effectTime,effectKind,rootPath,request);
	}
}
