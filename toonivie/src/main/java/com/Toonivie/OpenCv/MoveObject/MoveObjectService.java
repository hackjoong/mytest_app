package com.Toonivie.OpenCv.MoveObject;

//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObjectVo;

@Service
public class MoveObjectService {

	static HttpServletRequest request_url;
	
	public HashMap<String, Object> exe(JSONObject background, JSONArray firstInfo, JSONArray lastInfo, int effectTime,
			String effectKind, String rootPath, HttpServletRequest request) {
		request_url = request;
		HashMap<String, Object> resultMap = new HashMap<>();
		if (effectKind.equals("move")) {
			return moveExe(background, firstInfo, lastInfo, effectTime, rootPath);
		} else if (effectKind.equals("fastMove")) {
			return movefastExe(background, firstInfo, lastInfo, effectTime, rootPath);
		} else if (effectKind.equals("walk")) {
			return walk(background, firstInfo, lastInfo, effectTime, rootPath);
		} else if (effectKind.equals("rotateMove")) {
			return rotateMove(background, firstInfo, lastInfo, effectTime, rootPath);
		}
		return resultMap;
	}
	
	/* 사물 연출 - 회전-이동 효과 */
	private HashMap<String, Object> rotateMove(JSONObject background, JSONArray firstInfo, JSONArray lastInfo,
			int effectTime, String rootPath) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		System.out.println("rotateMove - bg, fi, li");
		System.out.println(background);
		System.out.println(firstInfo);
		System.out.println(lastInfo);
		/* rotateMove - bg, fi, li
		{"imgUrl":"\/resources\/cvimgs\/backGround20171031110815.png","top":298.5,"left":475.75}
		[{"imgUrl":"\/resources\/cvimgs\/grabcut\/2017-10-31_11-08-04.png","top":489.5,"left":265.75},{"imgUrl":"\/resources\/cvimgs\/grabcut\/2017-10-31_11-08-15.png","top":361.5,"left":1001.75}]
		[{"imgUrl":"\/resources\/cvimgs\/grabcut\/2017-10-31_11-08-04.png","top":524.5,"left":768.75},{"imgUrl":"\/resources\/cvimgs\/grabcut\/2017-10-31_11-08-15.png","top":361.5,"left":1001.75}]
		*/
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		int frame = 20;
		HashMap<String, Object> resultMap = new HashMap<>();
		List<MoveObjectVo> firstList = changeMoveObjectVo(firstInfo, background);
		List<MoveObjectVo> lastList = changeMoveObjectVo(lastInfo, background);
		System.out.println("firstList, lastList : ");
		System.out.println(firstList);
		System.out.println(lastList);
		/* firstList, lastList : 
		[com.Toonivie.Vo.MoveObjectVo@30eed012, com.Toonivie.Vo.MoveObjectVo@10c87820]
		[com.Toonivie.Vo.MoveObjectVo@27787bcd, com.Toonivie.Vo.MoveObjectVo@68f38504]
		*/		
		
		MoveObjectVo backgroundVo = changeMoveObjectVo(background);
		int loopSize = firstList.size(); //
		System.out.println("firstList.size : " + loopSize); //2
		String backUrl = (rootPath + backgroundVo.getImgUrl()).replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		System.out.println(backUrl); //C:\toonivie\src\main\webapp\/resources/cvimgs/backGround20171031112734.png
		Mat imBack = Imgcodecs.imread(backUrl);
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		Mat thumbNail = null;
		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());*/
		//유덕희 경로주석 _180402
		videoWriter.open(rootPath+"resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());
		Mat result = new Mat();
		
		//분리된 객체가 2개 이상일 경우 : 실제 효과가 적용되는 객체를 식별하고, 나머지 객체는 백그라운드와 merge - add by shkwak, 2017-11-09
		int selectedObjectNum = -1;
		Mat imBackNew = null;
		Mat imBackOld = imBack.clone();
		for (int i = 0; i < loopSize; i++) {
			MoveObjectVo fInfo = firstList.get(i);
			MoveObjectVo lInfo = lastList.get(i);
			//1.실제 효과가 적용되는 객체 식별 : left 값의 변화로 판단!
			if ((int) fInfo.getLeft() == (int) lInfo.getLeft() && (int) fInfo.getTop() == (int) lInfo.getTop()) {				
				String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""); //.replace("http://192.168.0.125:8088", "");
				Mat object = Imgcodecs.imread(fUrl, -1);				
				//2.나머지 객체 백그라운드와 merge
				imBackNew = putObjOnBackground.setBackMat((int) fInfo.getLeft(), (int) fInfo.getTop(), imBackOld, object);
				/*String ObjOnBack = "src/main/webapp/resources/cvimgs/backimg" + today + "_" + i + ".png";*/
				//유덕희 주석 처리_180402
				String ObjOnBack = rootPath+"resources/cvimgs/backimg" + today + "_" + i + ".png";
				Imgcodecs.imwrite(ObjOnBack, imBackNew);
				imBackOld = imBackNew;
				continue;
			}else {
				if(loopSize == 1) {
					imBackNew = imBack;
				}
			}
			selectedObjectNum = i;
			System.out.println("MoveObjectService.java:112 - selectedObjectNum : " + selectedObjectNum);							
		}		
		
		for (int i = 0; i < frame * effectTime; i++) {			
			Mat im = imBackNew.clone(); //imBack
			//for (int j = 0; j < loopSize; j++) {
			if (selectedObjectNum != -1){
				MoveObjectVo fInfo = firstList.get(selectedObjectNum);
				MoveObjectVo lInfo = lastList.get(selectedObjectNum);
				String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""); //.replace("http://192.168.0.125:8088", "");
				putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
				putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
				Mat person = Imgcodecs.imread(fUrl, -1);
				result = rotateMoveExe((int) fInfo.getLeft(), (int) fInfo.getTop(), (int) lInfo.getLeft(), (int) lInfo.getTop(), 
						im, person, i, frame * effectTime-1, i); //★★rotateMoveExe : 실제 분리된 객체를 회전이동 시키는 메소드
			}
			videoWriter.write(result);
//			if (i == (frame * effectTime) / 2) {
			if (i == (frame * effectTime) / 4) {
				thumbNail = result.clone();
			}
		}

		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 경로 주석_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl", "/resources/cvMov/move" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", imBack.size().height); // add by shkwak, 2017-06-19
		resultMap.put("videoWidth", imBack.size().width);
		return resultMap;
	}

	private List<MoveObjectVo> changeMoveObjectVo(JSONArray array, JSONObject background) { //changeMoveObjectVo
		List<MoveObjectVo> moveObjectVoList = new ArrayList<MoveObjectVo>();
		System.out.println("background.get(top) : " + background.get("top")); //298.5
		Double d1 = new Double(Double.parseDouble(background.get("top").toString())); //Double.parseDouble()
		Double d2 = new Double(Double.parseDouble(background.get("left").toString()));
		float backTop = d1.floatValue();
		float backLeft = d2.floatValue();
		
		for (int i = 0; i < array.size(); i++) {
			MoveObjectVo moveObjectVo = new MoveObjectVo();
			JSONObject obj = (JSONObject) array.get(i);
			Double f1 = new Double(Double.parseDouble(obj.get("top").toString()));
			Double f2 = new Double(Double.parseDouble(obj.get("left").toString()));
			moveObjectVo.setHeight((new Double(obj.get("height").toString())).floatValue());
			moveObjectVo.setWidth((new Double(obj.get("width").toString())).floatValue());
			moveObjectVo.setImgUrl(obj.get("imgUrl").toString());
			moveObjectVo.setLeft(f2.floatValue() - backLeft);
			moveObjectVo.setTop(f1.floatValue() - backTop);
			moveObjectVoList.add(moveObjectVo);
		}
		return moveObjectVoList;
	}
	
	/* 사뭏 연출 - 회전이동효과 */ 
	private Mat rotateMoveExe(int initX, int initY, int lastX, int lastY, Mat im, Mat parameterPerson, int frameNum, int totalFrame, int i) {
		System.out.println("initX, initY, lastX, lastY, im, parameterPerson, frameNum, totalFrame");
		System.out.println(initX +", " + initY +", " + lastX +", " + lastY +", " + im +", " + parameterPerson +", " + frameNum +", " + totalFrame);		
		if (lastX == initX) {

		} else {
			int distance = Math.abs(lastX - initX);
			float pPf = (float) distance / (float) totalFrame;
			if (lastX < initX) {
				frameNum = -frameNum;
			}
			System.out.println("distance : " + distance +", pPf : " + pPf +", frameNum : " + frameNum);
			int startX = initX + (int) ((float) frameNum * pPf);
			System.out.println("initX : " + initX +", initY : " + initY +", lastX : " + lastX +", lastY : " + lastY +", startX : " + startX);
			initY = (int) getY(initX, initY, lastX, lastY, startX);
			initX = startX;
			System.out.println("initX : " + initX +", initY : " + initY);
		}
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat afterObject = new Mat();		
		//GetRotationMatrix2D 함수로 원하는 각만큼 회전 시켜줄 틀을 만들어 준다.
		//WarpAffine 함수로 원하는 틀에 맞춰 변경비켜준다.
//		Mat m = Imgproc.getRotationMatrix2D(new Point(parameterPerson.width() / 2, parameterPerson.height() / 2), frameNum * 10, 1.0);
		Mat m = Imgproc.getRotationMatrix2D(new Point(parameterPerson.width() / 2, parameterPerson.height() / 2), ((double)(360)/(double)(totalFrame))*i, 1.0);
		Imgproc.warpAffine(parameterPerson, afterObject, m, new Size(parameterPerson.cols(), parameterPerson.rows()));
		return putObjOnBackground.setBackMat(initX, initY, im, afterObject);
	}
	
	private HashMap<String, Object> walk(JSONObject background, JSONArray firstInfo, JSONArray lastInfo, int effectTime,
			String rootPath) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		int frame = 20;
		HashMap<String, Object> resultMap = new HashMap<>();
		List<MoveObjectVo> firstList = changeMoveObjectVo(firstInfo, background);
		List<MoveObjectVo> lastList = changeMoveObjectVo(lastInfo, background);
		MoveObjectVo backgroundVo = changeMoveObjectVo(background);
		int loopSize = firstList.size();
		String backUrl = (rootPath + backgroundVo.getImgUrl()).replace("http://localhost:8088/", "")
				.replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		System.out.println(backUrl);
		Mat imBack = Imgcodecs.imread(backUrl);
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		Mat thumbNail = null;
		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath + "resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());
		Mat result = new Mat();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat im = imBack.clone();
			for (int j = 0; j < loopSize; j++) {
				MoveObjectVo fInfo = firstList.get(j);
				MoveObjectVo lInfo = lastList.get(j);
				String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
						.replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
				putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
				putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
				Mat person = Imgcodecs.imread(fUrl, -1);
				result = walkImageExe((int) Math.ceil(fInfo.getLeft()), (int) Math.ceil(fInfo.getTop()), (int) Math.ceil(lInfo.getLeft()),
						(int) Math.ceil(lInfo.getTop()), im, person, i, frame * effectTime);
			}
			videoWriter.write(result);
//			if (i == (frame * effectTime) / 2) {
			if (i == (frame * effectTime) / 4) {
				thumbNail = result.clone();
			}
		}

		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 경로 주석_180402
		String thumbNailImgName = rootPath + "resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl", "/resources/cvMov/move" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", imBack.size().height); // add by shkwak, 2017-06-19
		resultMap.put("videoWidth", imBack.size().width);
		return resultMap;
	}

	private int getCount(double gradient, int kernelSize, int yAtXZero) {
		if (gradient <= 1) {
			return kernelSize;
		}
		int left = (int) (-yAtXZero / gradient);
		int right = (int) (((kernelSize + 1) - yAtXZero) / gradient);
		return Math.abs(Math.abs(left) - Math.abs(right));
	}

	private Mat setFastMoveBackground(Mat im, double gradient) {
		System.out.println("gradient : " + gradient);
		int kernelSize = 51;
		int kernelCenterX = (kernelSize + 1) / 2;
		int kernelCenterY = (kernelSize + 1) / 2;
		int yAtXZero = (int) (kernelCenterY - gradient * kernelCenterX);
		int divideCount = getCount(gradient, kernelSize, yAtXZero);
		System.out.println("divideCount : " + divideCount);
		Mat kernel = new Mat(kernelSize, kernelSize, CvType.CV_32F) {
			{
				for (int i = 0; i < kernelSize; i++) {
					for (int j = 0; j < kernelSize; j++) {
						put(i, j, 0);
						int y = (int) (gradient * j + yAtXZero);
						if (y >= 0 || y <= kernelSize) {
							put(y, j, 1 / (double) divideCount);
						}
					}
				}
			}
		};

		Imgproc.filter2D(im, im, -1, kernel);
		return im;
	}

	private double getGradient(int fLeft, int fTop, int lLeft, int lTop) {
		if (((double) lLeft - (double) fLeft) == 0.0) {
			return 0.0;
		}
		return ((double) lTop - (double) fTop) / ((double) lLeft - (double) fLeft);
	}

	private HashMap<String, Object> movefastExe(JSONObject background, JSONArray firstInfo, JSONArray lastInfo,
			int effectTime, String rootPath) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		int frame = 20;
		HashMap<String, Object> resultMap = new HashMap<>();
		List<MoveObjectVo> firstList = changeMoveObjectVo(firstInfo, background);
		List<MoveObjectVo> lastList = changeMoveObjectVo(lastInfo, background);
		MoveObjectVo backgroundVo = changeMoveObjectVo(background);
		int loopSize = firstList.size();
		String backUrl = (rootPath + backgroundVo.getImgUrl()).replace("http://localhost:8088/", "")
				.replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		System.out.println("backUrl :::::"+backUrl);
/*		int fLeft = (int) firstList.get(0).getLeft();
		int fTop = (int) firstList.get(0).getTop();
		int lLeft = (int) lastList.get(0).getLeft();
		int lTop = (int) lastList.get(0).getTop();*/
		int fLeft = 0;
		int fTop = 0;
		int lLeft = 0;
		int lTop = 0;
		Mat imBack = Imgcodecs.imread(backUrl);
		//put object to background by KH 18-03-08
		for(int i=0;i<firstList.size();i++) {
			Mat fore = Imgcodecs.imread(rootPath+firstList.get(i).getImgUrl().replace("webapp//", "webapp/"),-1);
			if((firstList.get(i).getLeft() == lastList.get(i).getLeft()) && (firstList.get(i).getTop() == lastList.get(i).getTop())) {
				putObjOnBackground.resize_mat(rootPath+firstList.get(i).getImgUrl().replace("webapp//", "webapp/"), firstList.get(i).getHeight(), firstList.get(i).getWidth());
				fore = Imgcodecs.imread(rootPath+firstList.get(i).getImgUrl().replace("webapp//", "webapp/"),-1);
				putObjOnBackground.setBackMat((int)firstList.get(i).getLeft(), (int)firstList.get(i).getTop(), imBack, fore);
			}else {
				fLeft = (int) firstList.get(i).getLeft();
				fTop = (int) firstList.get(i).getTop();
				lLeft = (int) lastList.get(i).getLeft();
				lTop = (int) lastList.get(i).getTop();
			}
		}
		// Imgproc.GaussianBlur(imBack, imBack, new Size(11,11), 11);
		Mat filteredBack = setFastMoveBackground(imBack, getGradient(fLeft, fTop, lLeft, lTop));
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		Mat thumbNail = null;
		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath + "resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());
		Mat result = new Mat();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat im = filteredBack.clone();
			for (int j = 0; j < loopSize; j++) {
				MoveObjectVo fInfo = firstList.get(j);
				MoveObjectVo lInfo = lastList.get(j);
				String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
						.replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
				putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
				putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
				Mat person = Imgcodecs.imread(fUrl, -1);
				if ((int) fInfo.getLeft() != (int) lInfo.getLeft() || (int) fInfo.getLeft() != (int) lInfo.getLeft())
					result = moveImageExe((int) fInfo.getLeft(), (int) fInfo.getTop(), (int) lInfo.getLeft(), (int) lInfo.getTop(), im, person, i, frame * effectTime);
			}
			videoWriter.write(result);
//			if (i == (frame * effectTime) / 2) {
			if (i == (frame * effectTime) / 4) {
				thumbNail = result.clone();
			}
		}
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 경로 주석_180402
		String thumbNailImgName = rootPath + "resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl", "/resources/cvMov/move" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", imBack.size().height); // add by shkwak, 2017-06-19
		resultMap.put("videoWidth", imBack.size().width);
		return resultMap;
	}

	private HashMap<String, Object> moveExe(JSONObject background, JSONArray firstInfo, JSONArray lastInfo,
			int effectTime, String rootPath) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		int frame = 20;
		HashMap<String, Object> resultMap = new HashMap<>();
		List<MoveObjectVo> firstList = changeMoveObjectVo(firstInfo, background);
		List<MoveObjectVo> lastList = changeMoveObjectVo(lastInfo, background);
		MoveObjectVo backgroundVo = changeMoveObjectVo(background);
		int loopSize = firstList.size();
		String backUrl = (rootPath + backgroundVo.getImgUrl()).replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
		System.out.println(backUrl);
		Mat imBack = Imgcodecs.imread(backUrl);
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}
		Mat thumbNail = null;
		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath + "resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), frame, imBack.size());
		Mat result = new Mat();
		for (int i = 0; i < frame * effectTime; i++) {
			Mat im = imBack.clone();
			for (int j = 0; j < loopSize; j++) {
				MoveObjectVo fInfo = firstList.get(j);
				MoveObjectVo lInfo = lastList.get(j);
				String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
						.replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
				putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
				putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
				Mat person = Imgcodecs.imread(fUrl, -1);
				result = moveImageExe((int) Math.ceil(fInfo.getLeft()), (int) Math.ceil(fInfo.getTop()), (int) Math.ceil(lInfo.getLeft()),
						(int) Math.ceil(lInfo.getTop()), im, person, i, frame * effectTime-1);
			}
			videoWriter.write(result);
//			if (i == (frame * effectTime) / 2) {
			if (i == (frame * effectTime) / 4) {
				thumbNail = result.clone();
			}
		}

		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 경로 주석_180402
		String thumbNailImgName = rootPath + "resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl", "/resources/cvMov/move" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", imBack.size().height); // add by shkwak, 2017-06-19
		resultMap.put("videoWidth", imBack.size().width);
		return resultMap;
	}

	private MoveObjectVo changeMoveObjectVo(JSONObject object) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		Object o1 = object.get("top");
		Double d1 = new Double(Double.parseDouble(object.get("top").toString()));
		Double d2 = new Double(Double.parseDouble(object.get("left").toString()));
		moveObjectVo.setImgUrl(object.get("imgUrl").toString());
		moveObjectVo.setLeft(d1.floatValue());
		moveObjectVo.setTop(d2.floatValue());
		return moveObjectVo;
	}

	private float getY(int fX, int fY, int lX, int lY, int frameNum) {//frameNum은 x에 대한 y의 함수에서 변수 x에 들어갈 값
		return (float) Math.ceil(((((float) lY - (float) fY) / ((float) lX - (float) fX))) * frameNum
				+ Math.ceil(((float) fY - (float) fX * (((float) lY - (float) fY) / ((float) lX - (float) fX)))));
	}

	private float getX(float fX, float fY, float lX, float lY, float frameNum) {//frameNum은 y에 대한 x의 함수에서 변수 y에 들어갈 값
		return (float) Math.ceil((frameNum - (fY - ((fY - lY) / (fX - lX)) * fX)) / ((fY - lY) / (fX - lX)));
	}

	private Mat walkImageExe(int initX, int initY, int lastX, int lastY, Mat im, Mat parameterPerson, int frameNum,
			int totalFrame) {
		int distanceX = Math.abs(lastX - initX);
		int distanceY = Math.abs(lastY - initY);
		int distance;
		int walking_object;
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		if (distanceX < distanceY) {
			if (lastY == initY) {
				walking_object = 0;
			} else {
				distance = Math.abs(lastY - initY);
				float pPf = (float) distance / (float) totalFrame;
				if (lastY < initY) {
					frameNum = -frameNum;
				}
				int startY = initY + (int) ((float) frameNum * pPf);
				initX = (int) getX(initX, initY, lastX, lastY, startY);
				initY = startY;
				walking_object = (int) (Math.sin(initX / 12) * ((double) im.height() / 10));
			}
		} else {
			if (lastX == initX) {
				walking_object = 0;
			} else {
				distance = Math.abs(lastX - initX);
				float pPf = (float) distance / (float) totalFrame;
				if (lastX < initX) {
					frameNum = -frameNum;
				}
				int startX = initX + (int) ((float) frameNum * pPf);
				initY = (int) getY(initX, initY, lastX, lastY, startX);
				initX = startX;
				walking_object = (int) (Math.sin(initX / 12) * ((double) im.height() / 10));
			}
			
		}
		
//		return putObjOnBackground.setBackMat(initX, (int) (Math.sin(initX / 12) * ((double) im.height() / 10)) + initY,	im, parameterPerson);
		return putObjOnBackground.setBackMat(initX, (int) walking_object + initY, im, parameterPerson);

	}	

	private Mat moveImageExe(int initX, int initY, int lastX, int lastY, Mat im, Mat parameterPerson, int frameNum,
			int totalFrame) {
		int distanceX = Math.abs(lastX - initX);
		int distanceY = Math.abs(lastY - initY);
		int distance;
		if (distanceX < distanceY) {
			if (lastY == initY) {

			} else {
				distance = Math.abs(lastY - initY);
				float pPf = (float) distance / (float) totalFrame;
				if (lastY < initY) {
					frameNum = -frameNum;
				}
				int startY = initY + (int) Math.ceil((float) frameNum * (float) pPf);
				initX = (int) getX(initX, initY, lastX, lastY, startY);
				initY = startY;
			}
		} else {
			if (lastX == initX) {

			} else {
				distance = Math.abs(lastX - initX);
				float pPf = (float) distance / (float) totalFrame;
				if (lastX < initX) {
					frameNum = -frameNum;
				}
				int startX = initX + (int) Math.ceil((float) frameNum * (float) pPf);
				initY = (int) getY(initX, initY, lastX, lastY, startX);
				initX = startX;
			}
		}
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		return putObjOnBackground.setBackMat(initX, initY, im, parameterPerson);

	}

	public HashMap<String, Object> applyMoveObject(String foreUrl, String backUrl, int mode, String rootPath, HttpServletRequest request) {
		request_url = request;
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		HashMap<String, Object> resultMap = new HashMap<>();
		CreateFileUtils cfu = new CreateFileUtils();
		String today = cfu.getToday(1);
		if (CreateFileUtils.getFileType(backUrl).equals("mov") || CreateFileUtils.getFileType(backUrl).equals("mp4")) {
			VideoCapture vc = new VideoCapture(rootPath + "resources/cvimgs/" + backUrl);
			Mat foreUrlMat = Imgcodecs.imread(rootPath + "resources/cvimgs/" + foreUrl, -1);
			moveObject(vc, foreUrlMat, today, rootPath);
		} else {
			Mat backUrlMat = Imgcodecs.imread(rootPath + "resources/cvimgs/" + backUrl);
			Mat foreUrlMat = Imgcodecs.imread(rootPath + "resources/cvimgs/" + foreUrl, -1);
			moveObject(backUrlMat, foreUrlMat, today, rootPath);
		}
		String videoUrl = "/resources/cvMov/move" + today + ".mp4";
		resultMap.put("videoUrl", videoUrl);
		return resultMap;
	}

	private void moveObject(VideoCapture vc, Mat personOrigin, String today, String rootPath) {
		Mat person = personOrigin;
		Mat parameterPerson = person.clone();
		Mat mask = null;
		Imgproc.cvtColor(parameterPerson, parameterPerson, Imgproc.COLOR_BGRA2BGR);
		List<Mat> personMatList = new ArrayList<>();
		Core.split(person, personMatList);

		mask = personMatList.get(3);

		Mat im;
		Mat imageRoi;

		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), 20,
				new Size((int) vc.get(Videoio.CAP_PROP_FRAME_WIDTH), (int) vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath+"resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), 20,
				new Size((int) vc.get(Videoio.CAP_PROP_FRAME_WIDTH), (int) vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
		int initX;
		int initY;
		int rectWidth;
		int rectHeight;
		Mat image = new Mat();
		int count = 0;
		while (vc.read(image)) {
			im = image.clone();
			rectWidth = parameterPerson.cols();
			rectHeight = parameterPerson.rows();
			initX = count;
			// initY=((lastY-firstY)/(lastX-firstX))*(initX-firstX)+firstY;//
			initY = 50;
			if (initX + rectWidth <= 0) {
				videoWriter.write(im);
			} else if (initX < 0 && initX + rectWidth > 0) {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, 0, rectWidth - Math.abs(initX), rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX),
							im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX),
							im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im,
							new Rect(0, initY, rectWidth - Math.abs(initX), im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, initY, rectWidth - Math.abs(initX), rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				}
			} else if (initX > 0 && initX < im.cols() && initX + rectWidth > im.cols()) {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, im.cols() - initX, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));

					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Rect rectCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				}
			} else if (initX >= im.cols()) {
				videoWriter.write(im);
			} else {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, rectWidth, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, rectHeight));
					parameterPerson.copyTo(imageRoi, mask);
					videoWriter.write(im);
				}
			}
			count++;
		}
		videoWriter.release();
	}

	private void moveObject(Mat imOrigin, Mat personOrigin, String today, String rootPath) {
		Mat person = personOrigin;
		Mat parameterPerson = person.clone();
		Mat mask = null;
		Imgproc.cvtColor(parameterPerson, parameterPerson, Imgproc.COLOR_BGRA2BGR);
		List<Mat> personMatList = new ArrayList<>();
		Core.split(person, personMatList);

		mask = personMatList.get(3);

		Mat im;
		Mat imageRoi;

		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/move" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), 20, imOrigin.size());*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath+"resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('H', '2', '6', '4'), 20, imOrigin.size());
		int initX;
		int initY;
		int rectWidth;
		int rectHeight;
		for (int j = -person.cols(); j < imOrigin.cols() + person.cols(); j = j + 5) {
			im = imOrigin.clone();
			rectWidth = parameterPerson.cols();
			rectHeight = parameterPerson.rows();
			initX = j;
			// initY=((lastY-firstY)/(lastX-firstX))*(initX-firstX)+firstY;/
			initY = 50;
			if (initX + rectWidth <= 0) {
				videoWriter.write(im);
			} else if (initX < 0 && initX + rectWidth > 0) {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, 0, rectWidth - Math.abs(initX), rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX),
							im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX),
							im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im,
							new Rect(0, initY, rectWidth - Math.abs(initX), im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, initY, rectWidth - Math.abs(initX), rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				}
			} else if (initX > 0 && initX < im.cols() && initX + rectWidth > im.cols()) {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, im.cols() - initX, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));

					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Rect rectCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				}
			} else if (initX >= im.cols()) {
				videoWriter.write(im);
			} else {
				if (initY < 0 && initY + rectHeight < 0) {
					videoWriter.write(im);
				} else if (initY >= im.rows()) {
					videoWriter.write(im);
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, rectWidth, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					videoWriter.write(im);
				} else {
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, rectHeight));
					parameterPerson.copyTo(imageRoi, mask);
					videoWriter.write(im);
				}
			}
		}
		videoWriter.release();
	}
}