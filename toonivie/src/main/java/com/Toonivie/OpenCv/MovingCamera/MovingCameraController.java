package com.Toonivie.OpenCv.MovingCamera;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/movingCamera")
public class MovingCameraController {
	@Autowired
	MovingCameraService movingCameraService;
	
	@RequestMapping(value="/moveToDown")
	public String moveToDown(HttpServletRequest request){
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		return movingCameraService.moveToDown(rootPath);
	}
}
