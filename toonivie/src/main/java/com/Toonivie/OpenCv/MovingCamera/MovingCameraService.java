package com.Toonivie.OpenCv.MovingCamera;

import java.io.File;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

@Service
public class MovingCameraService {

	public String moveToDown(String rootPath) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat backImg = Imgcodecs.imread(rootPath + "resources/cvimgs/20170601220136.png");
		int imgRow = backImg.rows();
		int imgCol = backImg.cols();

		VideoWriter videoWriter = new VideoWriter();
		VideoCapture vc = new VideoCapture(rootPath + "resources/cvMov/cameraMoving.mp4");

		System.out.println(vc.isOpened());
		File file = new File(rootPath + "resources/cvMov/cameraMoving.mp4");
		if (file.exists()) {
			file.delete();
		}

		/*videoWriter.open("src/main/webapp/resources/cvMov/cameraMoving.mp4", VideoWriter.fourcc('H', '2', '6', '4'), 20,
				new Size(backImg.rows(), backImg.rows()));*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath+"resources/cvMov/cameraMoving.mp4", VideoWriter.fourcc('H', '2', '6', '4'), 20,
				new Size(backImg.rows(), backImg.rows()));
		Mat backMat = new Mat(new Size(imgRow, imgRow), CvType.CV_8UC3, new Scalar(0, 0, 0));

		// Mat mainImg = backMat.clone();
		// Rect rect = new Rect(0,0,imgRow-40,imgRow);
		// Mat imgRoi = new Mat(backImg,rect);
		// Imgcodecs.imwrite("roi.png", imgRoi);
		// Mat crop = mainImg.submat(new Rect(40,0,imgRow-40,imgRow));
		// Imgcodecs.imwrite("crop1.png", crop);
		// Core.addWeighted(crop, 1.0, imgRoi, 0.5, 0., crop);
		// Imgcodecs.imwrite("crop2.png", crop);
		// Imgcodecs.imwrite("crop3.png", mainImg);
		// videoWriter.write(mainImg);

		for (int i = -40; i < 80; i++) {
			int y = 0;
			Mat mainImg = backMat.clone();
			if (i < 0) {
				Rect rect = new Rect(0, y, imgRow - Math.abs(i), imgRow);
				Rect mainRect = new Rect(Math.abs(i), y, imgRow - Math.abs(i), imgRow);
				Mat imRoi = new Mat(backImg, rect);
				Mat crop = mainImg.submat(mainRect);
				Core.addWeighted(crop, 1.0, imRoi, 1.0, 0., crop);
				videoWriter.write(mainImg);
			} else {
				System.out.println(i);
				Rect rect = new Rect(i, y, imgRow, imgRow);
				Mat main = backImg.submat(rect);
				videoWriter.write(main);
			}

		}
		videoWriter.release();
		return "aa";
	}
}
