package com.Toonivie.OpenCv.MultiApplyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Vo.MoveObjectVo;
import com.Toonivie.Vo.MultiApplyVo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Controller
@RequestMapping("/multiApplyObject")
public class MultiApplyObjectController {
	
	static final int frame = 20;
	static final String extType = ".mp4";
	
	@RequestMapping(value="/getApply",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> getApply(HttpServletRequest request){
		String str = request.getParameter("data");
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String rootPath = request.getServletContext().getRealPath("/");
		
		//objs 리스트 생성
		List<String> json_objs = new ArrayList<String>();
		List<MultiApplyVo> objs = new ArrayList<MultiApplyVo>();
		List<MoveObjectVo> others = new ArrayList<MoveObjectVo>();
		json_objs = (JSONArray)jsonObject.get("objs");
		Object item = null;
		Object item_move = jsonObject.get("move");
		try {
			item = json_objs.get(0);
		}catch(Exception e) {
		}
		try {
			JSONObject temp = (JSONObject) item;
			JSONArray oth_objs = (JSONArray) temp.get("otherObj");
			JSONObject background = (JSONObject) temp.get("background");
			for(Object item_inner : oth_objs){
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setTop( ((Number)oth_inner.get("top")).floatValue());
				vo.setLeft( ((Number) oth_inner.get("left")).floatValue());
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setDegree(((Number) oth_inner.get("degree")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(vo.getTop()-((Number)background.get("top")).floatValue());
				vo.setLeft(vo.getLeft()-((Number)background.get("left")).floatValue());
				vo.setInfo("null");
				others.add(vo);
			}
		}catch(Exception e) {
			System.out.println(e);

		}try {
			JSONObject other_objs = (JSONObject) item_move;
			JSONObject background_move = (JSONObject) other_objs.get("background");
			for(Object item_inner : (JSONArray) other_objs.get("firstInfo")){
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setBackground((JSONObject)other_objs.get("background"));
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(((Number) oth_inner.get("top")).floatValue()-((Number)background_move.get("top")).floatValue());
				vo.setLeft(((Number) oth_inner.get("left")).floatValue()-((Number)background_move.get("left")).floatValue());
				vo.setEffectKind((String) oth_inner.get("effectKind"));
				vo.setEffectTime(Integer.parseInt((String) oth_inner.get("effectTime")));
				vo.setInfo("firstinfo");
				others.add(vo);
			}
		}catch(Exception e) {
			System.out.println(e);

		}try {
			JSONObject other_objs = (JSONObject) item_move;
			JSONObject background_move = (JSONObject) other_objs.get("background");
			for(Object item_inner : (JSONArray) other_objs.get("lastInfo")){
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(((Number) oth_inner.get("top")).floatValue()-((Number)background_move.get("top")).floatValue());
				vo.setLeft(((Number) oth_inner.get("left")).floatValue()-((Number)background_move.get("left")).floatValue());
				vo.setEffectKind((String) oth_inner.get("effectKind"));
				vo.setEffectTime(Integer.parseInt((String) oth_inner.get("effectTime")));
				vo.setInfo("lastinfo");
				others.add(vo);

			}
		}catch(Exception e) {
			System.out.println(e);
		}
		try {
			JSONObject other_objs = (JSONObject) item_move;
			JSONObject background_move = (JSONObject) other_objs.get("background");
			for(Object item_inner : (JSONArray) other_objs.get("others")){
				System.out.println(item_inner);
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(((Number) oth_inner.get("top")).floatValue()-((Number)background_move.get("top")).floatValue());
				vo.setLeft(((Number) oth_inner.get("left")).floatValue()-((Number)background_move.get("left")).floatValue());
				vo.setInfo("null");
				others.add(vo);
			}
		}catch(Exception e) {
			System.out.println(e);
		}
		Gson gson = new Gson();
		try {
			objs.addAll(gson.fromJson(json_objs.toString(),  new TypeToken<List<MultiApplyVo>>() {}.getType()));
		}catch(Exception e){
			
		}
			 
		MultiApplyObjectService multiApplyObjectService = new MultiApplyObjectService(objs, others, request, rootPath);
		multiApplyObjectService.start();
		HashMap<String,Object> result = new HashMap<String,Object>();
		result = multiApplyObjectService.finish();
		return result;
		//return MultiApplyObjectService.applyObject(effectKind,effectTime,background,obj,rootPath,others, request);
	}

	@RequestMapping("/tests")
	@ResponseBody
	public HashMap<String,Object> getApplyMovetest(HttpServletRequest request){
		String str = "{"+
				"	\"background\": {"+
				"		\"background\": {"+
				"			\"imgUrl\": \"/resources/cvimgs/backGround20180802162825281.png\","+
				"			\"top\": 353.6875,"+
				"			\"left\": 475.65625"+
				"		}"+
				"	},"+
				"	\"firstInfo\": [{"+
				"		\"top\": 587.5,"+
				"		\"left\": 782.75,"+
				"		\"height\": 200,"+
				"		\"width\": 199,"+
				"		\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-02_16-28-25289.png\","+
				"		\"effectKind\": \"move\","+
				"		\"effectTime\": \"1\""+
				"	}, {"+
				"		\"top\": 382.5,"+
				"		\"left\": 586.75,"+
				"		\"height\": 197,"+
				"		\"width\": 197,"+
				"		\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-02_16-28-24773.png\","+
				"		\"effectKind\": \"rotateMove\","+
				"		\"effectTime\": \"1\""+
				"	}],"+
				"	\"lastInfo\": [{"+
				"		\"top\": 587.5,"+
				"		\"left\": 782.75,"+
				"		\"height\": 200,"+
				"		\"width\": 199,"+
				"		\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-02_16-28-25289.png\","+
				"		\"effectKind\": \"move\","+
				"		\"effectTime\": \"1\""+
				"	}, {"+
				"		\"top\": 382.5,"+
				"		\"left\": 586.75,"+
				"		\"height\": 197,"+
				"		\"width\": 197,"+
				"		\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-02_16-28-24773.png\","+
				"		\"effectKind\": \"rotateMove\","+
				"		\"effectTime\": \"1\""+
				"	}]"+
				"}";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		/*		JSONArray others = (JSONArray)jsonObject.get("otherObj");
		String effectKind = jsonObject.get("effectKind").toString();
		String effectTimeString = jsonObject.get("effectTime").toString();
		int effectTime = Integer.parseInt(effectTimeString);
		
		JSONObject background = (JSONObject)jsonObject.get("background");
		JSONObject obj = (JSONObject)jsonObject.get("obj");*/
		String rootPath = request.getServletContext().getRealPath("/");
		
		//objs 리스트 생성
		List<String> json_objs = new ArrayList<String>();
		List<MultiApplyVo> objs = new ArrayList<MultiApplyVo>();
		List<MoveObjectVo> others = new ArrayList<MoveObjectVo>();
		json_objs = (JSONArray)jsonObject.get("objs");
		Object item = json_objs.get(0);
		JSONObject temp = (JSONObject) item;
		JSONArray oth_objs = (JSONArray) temp.get("otherObj");
		JSONObject background = (JSONObject) temp.get("background");
		for(Object item_inner : oth_objs){
			JSONObject oth_inner = (JSONObject) item_inner;
			MoveObjectVo vo = new MoveObjectVo();
			vo.setTop( ((Number)oth_inner.get("top")).floatValue());
			vo.setLeft( ((Number) oth_inner.get("left")).floatValue());
			vo.setHeight(((Number) oth_inner.get("height")).floatValue());
			vo.setWidth(((Number) oth_inner.get("width")).floatValue());
			vo.setDegree(((Number) oth_inner.get("degree")).floatValue());
			vo.setImgUrl((String) oth_inner.get("imgUrl"));
			
			vo.setTop(vo.getTop()-((Number)background.get("top")).floatValue());
			vo.setLeft(vo.getLeft()-((Number)background.get("left")).floatValue());
			others.add(vo);
		}
		Gson gson = new Gson();
		/*		for(Object item : json_objs) {
			objs.add(gson.fromJson(item.toString(),  new TypeToken<List<MultiApplyVo>>() {}.getType()));
		}*/
		objs.addAll(gson.fromJson(json_objs.toString(),  new TypeToken<List<MultiApplyVo>>() {}.getType()));
		MultiApplyObjectService multiApplyObjectService = new MultiApplyObjectService(objs, others, request, rootPath);
		multiApplyObjectService.start();
		HashMap<String,Object> result = new HashMap<String,Object>();
		result = multiApplyObjectService.finish();
		return result;
		//return MultiApplyObjectService.applyObject(effectKind,effectTime,background,obj,rootPath,others, request);
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public void getApplyTest(HttpServletRequest request){
		String str = "{"+
				"	\"move\": {"+
				"		\"background\": {"+
				"			\"imgUrl\": \"/resources/cvimgs/backGround20180817105535876.png\","+
				"			\"top\": 353.6875,"+
				"			\"left\": 475.65625"+
				"		},"+
				"		\"firstInfo\": [{"+
				"			\"top\": 584.5,"+
				"			\"left\": 765.75,"+
				"			\"height\": 200,"+
				"			\"width\": 199,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35883.png\","+
				"			\"effectKind\": \"move\","+
				"			\"effectTime\": \"2\""+
				"		}, {"+
				"			\"top\": 449.5,"+
				"			\"left\": 1100.75,"+
				"			\"height\": 197,"+
				"			\"width\": 197,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35364.png\","+
				"			\"effectKind\": \"move\","+
				"			\"effectTime\": \"1\""+
				"		}, {"+
				"			\"top\": 344.5,"+
				"			\"left\": 477.75,"+
				"			\"height\": 200,"+
				"			\"width\": 199,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35883.png\","+
				"			\"effectKind\": \"rotateMove\","+
				"			\"effectTime\": \"1\""+
				"		}, {"+
				"			\"top\": 449.5,"+
				"			\"left\": 1100.75,"+
				"			\"height\": 197,"+
				"			\"width\": 197,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35364.png\","+
				"			\"effectKind\": \"rotateMove\","+
				"			\"effectTime\": \"1\""+
				"		}],"+
				"		\"lastInfo\": [{"+
				"			\"top\": 344.5,"+
				"			\"left\": 477.75,"+
				"			\"height\": 200,"+
				"			\"width\": 199,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35883.png\","+
				"			\"effectKind\": \"move\","+
				"			\"effectTime\": \"2\""+
				"		}, {"+
				"			\"top\": 449.5,"+
				"			\"left\": 1100.75,"+
				"			\"height\": 197,"+
				"			\"width\": 197,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35364.png\","+
				"			\"effectKind\": \"move\","+
				"			\"effectTime\": \"1\""+
				"		}, {"+
				"			\"top\": 344.5,"+
				"			\"left\": 477.75,"+
				"			\"height\": 200,"+
				"			\"width\": 199,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35883.png\","+
				"			\"effectKind\": \"rotateMove\","+
				"			\"effectTime\": \"1\""+
				"		}, {"+
				"			\"top\": 590.5,"+
				"			\"left\": 775.75,"+
				"			\"height\": 197,"+
				"			\"width\": 197,"+
				"			\"imgUrl\": \"/resources/cvimgs/grabcut/2018-08-17_10-55-35364.png\","+
				"			\"effectKind\": \"rotateMove\","+
				"			\"effectTime\": \"1\""+
				"		}],"+
				"		\"others\": []"+
				"	}"+
				"}";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String rootPath = request.getServletContext().getRealPath("/");
		
		//objs 리스트 생성
		List<String> json_objs = new ArrayList<String>();
		List<MultiApplyVo> objs = new ArrayList<MultiApplyVo>();
		List<MoveObjectVo> others = new ArrayList<MoveObjectVo>();
		json_objs = (JSONArray)jsonObject.get("objs");
		Object item = null;
		Object item_move = jsonObject.get("move");
		try {
			item = json_objs.get(0);
		}catch(Exception e) {
		}
		try {
			JSONObject temp = (JSONObject) item;
			JSONArray oth_objs = (JSONArray) temp.get("otherObj");
			JSONObject background = (JSONObject) temp.get("background");
			for(Object item_inner : oth_objs){
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setTop( ((Number)oth_inner.get("top")).floatValue());
				vo.setLeft( ((Number) oth_inner.get("left")).floatValue());
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setDegree(((Number) oth_inner.get("degree")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(vo.getTop());
				vo.setLeft(vo.getLeft());
				vo.setInfo("null");
				others.add(vo);
			}
		}catch(Exception e) {
			System.out.println(e);

		}try {
			JSONObject other_objs = (JSONObject) item_move;
			JSONObject background_move = (JSONObject) other_objs.get("background");
			for(Object item_inner : (JSONArray) other_objs.get("firstInfo")){
				JSONObject oth_inner = (JSONObject) item_inner;
				MoveObjectVo vo = new MoveObjectVo();
				vo.setBackground((JSONObject)other_objs.get("background"));
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(((Number) oth_inner.get("top")).floatValue()-((Number)background_move.get("top")).floatValue());
				vo.setLeft(((Number) oth_inner.get("left")).floatValue()-((Number)background_move.get("left")).floatValue());
				vo.setEffectKind((String) oth_inner.get("effectKind"));
				vo.setEffectTime(Integer.parseInt((String) oth_inner.get("effectTime")));
				vo.setInfo("firstinfo");
				others.add(vo);
			}
		}catch(Exception e) {
			System.out.println(e);

		}try {
			JSONObject other_objs = (JSONObject) item_move;
			JSONObject background_move = (JSONObject) other_objs.get("background");
			for(Object item_inner : (JSONArray) other_objs.get("lastInfo")){
				JSONObject oth_inner = (JSONObject) item_inner;
				System.out.println("HELLO");
				System.out.println(oth_inner.toJSONString());
				MoveObjectVo vo = new MoveObjectVo();
				vo.setHeight(((Number) oth_inner.get("height")).floatValue());
				vo.setWidth(((Number) oth_inner.get("width")).floatValue());
				vo.setImgUrl((String) oth_inner.get("imgUrl"));
				vo.setTop(((Number) oth_inner.get("top")).floatValue()-((Number)background_move.get("top")).floatValue());
				vo.setLeft(((Number) oth_inner.get("left")).floatValue()-((Number)background_move.get("left")).floatValue());
				vo.setEffectKind((String) oth_inner.get("effectKind"));
				vo.setEffectTime(Integer.parseInt((String) oth_inner.get("effectTime")));
				vo.setInfo("lastinfo");
				others.add(vo);

			}
		}catch(Exception e) {
			System.out.println(e);

		}
		Gson gson = new Gson();
		try {
			objs.addAll(gson.fromJson(json_objs.toString(),  new TypeToken<List<MultiApplyVo>>() {}.getType()));
		}catch(Exception e){
			
		}
		System.out.println("objs " + objs.toString());
		MultiApplyObjectService multiApplyObjectService = new MultiApplyObjectService(objs, others, request, rootPath);
		multiApplyObjectService.start();
		HashMap<String,Object> result = new HashMap<String,Object>();
		result = multiApplyObjectService.finish();
		
	}
	/*
	@RequestMapping(value="/rainyDay",method=RequestMethod.POST) //no use
	@ResponseBody
	public HashMap<String,Object> rainDay(HttpServletRequest request) throws IOException{
		HashMap<String,Object> resultMap=new HashMap<String,Object>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String imgUrl=request.getParameter("imgUrl");
		System.out.println(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+imgUrl);
		Mat im = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+imgUrl);
		CreateFileUtils cfu = new CreateFileUtils();
        String today=cfu.getToday(0);
        ApplyObjectService.java 거쳐서 처리되게 수정 필요 : 나중에, shkwak, 2017-06-29
		rainDrop(im,request,today); 
		String fileName ="/resources/cvMov/rain" + today + extType;
		resultMap.put("fileName", fileName);
		return resultMap;
	}
	
	private void rainDrop(Mat im,HttpServletRequest request, String today){
		//roi
        Mat im1 = im.clone();
        Mat im2 = im.clone();
        
        Mat rainPng = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvStaticImg/")+"raindrop_1.jpg",1);
        Imgproc.threshold(rainPng, rainPng, 10, 255, Imgproc.THRESH_TOZERO);
        Mat rainPng2 = Imgcodecs.imread(request.getSession().getServletContext().getRealPath("resources/cvStaticImg/")+"raindrop_2.jpg",1);
        Imgproc.threshold(rainPng2, rainPng2, 10, 255, Imgproc.THRESH_TOZERO);
        
        System.out.println("Size im : "+im.rows()+","+im.cols());
        System.out.println("Size rainPng : "+rainPng.rows()+","+rainPng.cols());
        System.out.println("Size rainPng2 : "+rainPng2.rows()+","+rainPng2.cols());
        
        Imgproc.resize(rainPng, rainPng, new Size(im.cols(),im1.rows()));
        Imgproc.resize(rainPng2, rainPng2, new Size(im.cols(),im2.rows()));
 
        Mat imageRoi = new Mat(im1,new Rect(0, 0, rainPng.cols(), rainPng.rows()));
        Mat imageRoi2 = new Mat(im2,new Rect(0, 0, rainPng2.cols(), rainPng2.rows()));
        
        Core.addWeighted(imageRoi, 1.0, rainPng, 1, 0.0, imageRoi);
        Core.addWeighted(imageRoi2, 1.0, rainPng2, 1, 0.0, imageRoi2);
        
        VideoWriter videoWriter = new VideoWriter();
        
        File file = new File(request.getSession().getServletContext().getRealPath("resources/cvMov/") + "rain" + today + extType);
        if(file.exists()){
        	file.delete();
        }
        String rootPath = request.getServletContext().getRealPath("/");
        videoWriter.open("src/main/webapp/resources/cvMov/rain" + today + extType, VideoWriter.fourcc('H','2','6','4'), 8, imageRoi.size()); //frame
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/rain" + today + extType, VideoWriter.fourcc('H','2','6','4'), 8, imageRoi.size()); //frame
                
        for (int i = 0; i < 75; i++) { //75?
        	if(i%6>=3){
        		videoWriter.write(imageRoi);
                im.release();
        	}else{
        		videoWriter.write(imageRoi2);
                im.release();
        	}
        }
        videoWriter.release();		
        System.out.println("rain effect");        
	}*/
}
