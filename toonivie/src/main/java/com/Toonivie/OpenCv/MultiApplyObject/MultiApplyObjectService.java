package com.Toonivie.OpenCv.MultiApplyObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springsource.loaded.SystemClassReflectionRewriter;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.ImageTypeConvert;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObjectVo;
import com.Toonivie.Vo.MultiApplyVo;


class MultiApplyObjectService{
	
	@Autowired 
	MoveObjectVo moveObjectVo;
	
	List<MultiApplyVo> objs = null; //전체 효과들 
	MoveObjectVo background = null; 
	List<Map<String,Object>> after_apply = null;
	String rootPath = null;
	HttpServletRequest request = null;
	HashMap<String, Object> resultMap = null;
	//하나의 writer 사용
	VideoWriter videoWriter = null;
	String video_name = null;
	static final int frame = 20;
	Mat im2 = null;
	Mat thumbNail = null;
	String today = null;
	float max = 0;
	List<MoveObjectVo> otherlist = null;
	List<MoveObjectVo> Movelist_f = null;
	List<MoveObjectVo> Movelist_l = null;
	List<MoveObjectVo> Movelist_f_go = null;
	List<MoveObjectVo> Movelist_l_go = null;
	List<Map<String,Object>> afterImgHistoy = null;
	
	 
	//생성자. 
	public MultiApplyObjectService(List<MultiApplyVo> objs, List<MoveObjectVo> otherList, HttpServletRequest request, String rootPath) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		after_apply = new ArrayList<Map<String, Object>>();
		afterImgHistoy = new ArrayList<Map<String, Object>>();
		this.request = request;
		this.rootPath = rootPath;
		List<Float> effect_time_total = new ArrayList<Float>();
		
		otherlist = new ArrayList<MoveObjectVo>();
		Movelist_f = new ArrayList<MoveObjectVo>();
		Movelist_l = new ArrayList<MoveObjectVo>();
		Movelist_f_go = new ArrayList<MoveObjectVo>();
		Movelist_l_go = new ArrayList<MoveObjectVo>();
		for(MoveObjectVo item : otherList) {
			if(item.getInfo().equals("null")) {
				otherlist.add(item);
			} else {
				if(item.getInfo().equals("firstinfo")) {
					item.setBackgroundVo(changeMoveBackgroundVo(item.getBackground()));
					effect_time_total.add(item.getEffectTime());
					Movelist_f.add(item);
				}else {
					Movelist_l.add(item);
				}
			}
			
		}
		float first_xposition, last_xposition = 0;
		String first_element, last_element, first_effect, last_effect = null;
		for(int k=0;k<Movelist_f.size();k++) {
			for(int l=0;l<Movelist_l.size();l++) {
				first_xposition = Movelist_f.get(k).getLeft();
				last_xposition = Movelist_l.get(l).getLeft();
				first_element = Movelist_f.get(k).getImgUrl();
				last_element = Movelist_l.get(l).getImgUrl();
				first_effect = Movelist_f.get(k).getEffectKind();
				last_effect = Movelist_l.get(l).getEffectKind();
				if(first_xposition != last_xposition && first_element.equals(last_element) && first_effect.equals(last_effect)) {
					Movelist_f_go.add(Movelist_f.get(k));
					Movelist_l_go.add(Movelist_l.get(l));
				}
			}
		}
		for(int z=0;z<otherlist.size();z++) {
			for(MoveObjectVo item_inner : Movelist_f_go) {
				if(item_inner.getImgUrl().equals(otherlist.get(z).getImgUrl())) {
					otherlist.remove(z);
				}
			}
		}
		
		Iterator<MoveObjectVo> it1 = otherlist.iterator();
		for(int i=0;i<Movelist_f_go.size();i++) { 
			while(it1.hasNext()) {
				MoveObjectVo item = it1.next();
				if(Movelist_f_go.get(i).getImgUrl().equals(item.getImgUrl())) {
					it1.remove();
				}
			}
		}
		try {
			im2 = Imgcodecs.imread(rootPath + Movelist_f.get(0).getBackgroundVo().getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), ""),-1);
		}catch(Exception e ){
		}

		//값 셋팅. JSON -> VO 변환
		for(MultiApplyVo item : objs) {
			//값 셋팅
			try {
				item.setObjVo(changeMoveObjectVo(item.getObj(), item.getBackground()));
				item.setBackgroundVo(changeMoveBackgroundVo(item.getBackground()));
				item.setOtherList(makeOtherList(item.getOtherObj(), item.getObjVo(), item.getBackgroundVo()));
				im2 = Imgcodecs.imread(rootPath + objs.get(0).getBackgroundVo().getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), ""), -1);
			}catch(Exception e) {
			}
			
			//putObjOnBackground.resize_mat(rootPath+ item.getObjVo().getImgUrl().replace("webapp//", "webapp/"), item.getObjVo().getHeight(), item.getObjVo().getWidth());
			
			item.setIm2(im2.clone());
			effect_time_total.add(item.getEffectTime());
			Iterator<MoveObjectVo> it2 = otherlist.iterator();
			while(it2.hasNext()) {
				MoveObjectVo otherlist_item = it2.next();
				if(item.getObjVo().getImgUrl().equals(otherlist_item.getImgUrl())) {
					System.out.println(otherlist_item.toString());
					it2.remove();
				}
			}
		}
		try {
			im2 = setRotateBackground(otherlist, rootPath);
		}catch(Exception e) {
		}
		this.objs = objs;

		max = Collections.max(effect_time_total);
	}
	
	public void start() {
		Mat im2_ori = im2.clone();
		resultMap = new HashMap<>();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		today = createFileUtils.getToday(1);
		videoWriter = new VideoWriter();
		video_name = rootPath + "resources/cvMov/Multi" + today + ".mp4";
		
		File file = new File(video_name); //avi
		if (file.exists()) {
			file.delete();
		}
		int effectKind, biggerVector, rotateVector = 0;
		String effect_Kind = null;
			videoWriter.open(video_name, VideoWriter.fourcc('H', '2', '6', '4'), frame, im2.size());
			for(int i=0;i<max*frame;i++) {
				//reset
				after_apply = new ArrayList<Map<String, Object>>();
				for(int j=0;j<objs.size();j++) {
					
					if(Character.isDigit(objs.get(j).getEffectKind().charAt(0))) {
						effectKind = Integer.parseInt(objs.get(j).getEffectKind());
						
						biggerVector = objs.get(j).getBiggerVector();
						rotateVector = objs.get(j).getRotateVector();
						if(objs.get(j).getEffectTime()*frame>i) {
							if (effectKind == 1){ //move
								
							}
							if(effectKind ==2||effectKind ==3){ //smaller, bigger 1, 4 -> 2, 3
								if(effectKind ==2){
									biggerVector=1;
								}else{
									biggerVector=-1;
								}
								after_apply.add(smaller(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i, biggerVector));
								
							}
							if (effectKind ==4){ //rotation-move
								
							}
							if(effectKind ==5||effectKind == 6) { //rotate-smaller, bigger 8, 9 -> 5, 6
								if(effectKind ==5){
									biggerVector=1; //left
								}else{
									biggerVector=-1; //right
								}
								after_apply.add(rotateSmaller(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i, biggerVector));
							}
							if(effectKind ==7) { //bigger and smaller 10 -> 7
								after_apply.add(biggerAndSmaller(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i ));
							}
							if (effectKind ==8||effectKind ==9){ //rotate 5, 6 -> 8, 9
								if(effectKind ==8){
									rotateVector=1; //left
								}else{
									rotateVector=-1; //right
								}
								after_apply.add(rotate(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i, rotateVector ));
							}
							if(effectKind ==10){ //vibrate 7 -> 10
								after_apply.add(vibrate(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i ));
							}
							if(effectKind ==11) { //effectKind 11 異붽� (�늿源쒕컯�엫)
								rotateVector=0;
								after_apply.add(eyeBlink(objs.get(j).getEffectTime(), objs.get(j).getObjVo(), rootPath, i)); //add hoo 2018-10-31 
							}
						}else {
							if(effectKind == 2 || effectKind == 5) {
							}else {
								Mat fore = Imgcodecs.imread(rootPath+objs.get(j).getObjVo().getImgUrl().replace("webapp//", "webapp/"),-1);
								Map<String,Object> result_map = new HashMap<String,Object>();
								result_map.put("mat",fore.getNativeObjAddr());
								result_map.put("top",objs.get(j).getObjVo().getTop());
								result_map.put("left",objs.get(j).getObjVo().getLeft());
								after_apply.add(result_map);
							}

						}					
					}	
				}
				
				for(int h=0;h<Movelist_f_go.size();h++) {
					effect_Kind = Movelist_f_go.get(h).getEffectKind();
					if(Movelist_f_go.get(h).getEffectTime()*frame>i) {
						if(effect_Kind.equals("move")) {
							after_apply.add(moveExe(Movelist_f_go.get(h).getEffectTime(),Movelist_f_go.get(h), Movelist_l_go.get(h), rootPath, i)); 
						}
						if(effect_Kind.equals("fastMove")) {
							after_apply.add(movefastExe(Movelist_f_go.get(h).getEffectTime(),Movelist_f_go.get(h), Movelist_l_go.get(h), rootPath, i));
						}
						if(effect_Kind.equals("walk")) {
							after_apply.add(walk(Movelist_f_go.get(h).getEffectTime(),Movelist_f_go.get(h), Movelist_l_go.get(h), rootPath, i));
						}
						if(effect_Kind.equals("rotateMove")) {
							after_apply.add(rotateMove(Movelist_f_go.get(h).getEffectTime(),Movelist_f_go.get(h), Movelist_l_go.get(h), rootPath, i));
						}
						if(effect_Kind.equals("afterImage-Move")) {									
							after_apply.add(afterImageMove(Movelist_f_go.get(h).getEffectTime(),Movelist_f_go.get(h), Movelist_l_go.get(h), rootPath, i));
							/*
							 *	다른 효과 추가시 afterImage-Move if 문을 가장 아래로 두워야 함.
							 */ 
						}							
					}else {
						Mat fore = Imgcodecs.imread(rootPath+Movelist_l_go.get(h).getImgUrl().replace("webapp//", "webapp/"),-1); 
						Map<String,Object> result_map = new HashMap<String,Object>();
						result_map.put("mat",fore.getNativeObjAddr());
						result_map.put("top",Movelist_l_go.get(h).getTop());
						result_map.put("left",Movelist_l_go.get(h).getLeft());
						after_apply.add(result_map);
					}
				}
				
				try {
					videoWriter.write(setRotateBackground_map(objs.get(0).getBackgroundVo(), after_apply, rootPath));
					System.out.println(i + "---Start Used Memory : " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
				}catch(Exception e) {
					videoWriter.write(setRotateBackground_map(Movelist_f.get(0).getBackgroundVo(), after_apply, rootPath));
				}
				im2 = im2_ori.clone();
			}
				
	}
	
	
	//종료시
	public HashMap<String,Object> finish() {
		videoWriter.release();
		try {
			CreateFileUtils createFileUtils = new CreateFileUtils();
			String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail"+today+".png";
			Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		}catch(Exception e) {
			
		}
		resultMap.put("videoUrl","/resources/cvMov/Multi" + today + ".mp4");
		resultMap.put("effectTime", max);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail"+today+".png");
		resultMap.put("videoHeight", im2.size().height); //add by shkwak, 2017-06-19
		resultMap.put("videoWidth", im2.size().width); 
		return resultMap;
	}
	
	public Map<String,Object> vibrate(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb) {
		Map<String,Object> result_map = new HashMap<String,Object>();
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"),-1);

		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();

		if(frame_numb == 0) {
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()));
			result_map.put("left",obj.getLeft());
			return result_map;
		}else if(frame_numb == (effectTime * frame)-1) {
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()));
			result_map.put("left",obj.getLeft());
			return result_map;
		}
		int index = frame_numb+1;
		index=index%4;
		if(index==0){
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()+10));
			result_map.put("left",obj.getLeft()-10);
		}else if(index==1){
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()+10));
			result_map.put("left",obj.getLeft()+10);
		}else if(index==2){
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()-10));
			result_map.put("left",obj.getLeft()+10);
		}else if(index==3){
			result_map.put("mat",fore.getNativeObjAddr());
			result_map.put("top",(obj.getTop()-10));
			result_map.put("left",obj.getLeft()-10);
		}
		
		if(frame_numb==(frame*effectTime)/2){
			thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fore);
			putObjOnBackground = null;
		}
		
		return result_map;
	}
	
	public Map<String,Object> smaller(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb, int biggerVector) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Map<String,Object> result_map = new HashMap<String,Object>();
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"),-1);
		float rate = (float)fore.rows()/(float)fore.cols();
			Mat fm = fore.clone();
			if(biggerVector == 1){
				float a =((float)(fore.width()-4))/((float)(frame*effectTime));
				int w = (int)((float)fore.cols()-frame_numb*a);
				Size size = new Size(w,(int)((float)w*rate));
				Imgproc.resize(fm, fm, size);
			}else {
				float a =((float)(fore.width()-4))/((float)(frame*effectTime));
				int w = (int)((float)fore.cols()-(frame*effectTime-frame_numb)*a);
				Size size = new Size(w,(int)((float)w*rate));
				Imgproc.resize(fm, fm, size);
			}
			
			if(frame_numb==(frame*effectTime)/2){
				thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
			}
			result_map.put("mat", fm.getNativeObjAddr());
			result_map.put("top", obj.getTop()+((fore.rows()-fm.rows()))/2);
			result_map.put("left", obj.getLeft()+((fore.cols()-fm.cols()))/2);
			
			return result_map;
	}
	
	public Map<String,Object> rotateSmaller(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb, int biggerVector){
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Map<String,Object> result_map = new HashMap<String,Object>();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"),-1);

		float rate = (float)fore.rows()/(float)fore.cols();
		
		Mat afterObject = new Mat();
		
		Mat fm = fore.clone();
		if(biggerVector == 1){
			float a =((float)(fore.width()-4))/((float)(frame*effectTime));
			int w = (int)((float)fore.cols()-frame_numb*a);
			Size size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			Mat m = Imgproc.getRotationMatrix2D(new Point(fm.width()/2,fm.height()/2), frame_numb*2, 1.0);
			Imgproc.warpAffine(fm, afterObject, m, new Size(fm.cols(),fm.rows()));
		}else {
			float a =((float)(fore.width()-4))/((float)(frame*effectTime));
			int w = (int)((float)fore.cols()-(frame*effectTime-frame_numb)*a);
			Size size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			Mat m = Imgproc.getRotationMatrix2D(new Point(fm.width()/2,fm.height()/2), (frame*effectTime-frame_numb)*2, 1.0);
			Imgproc.warpAffine(fm, afterObject, m, new Size(fm.cols(),fm.rows()));
			
		}
			if(frame_numb==(frame*effectTime)/2){
				thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
			}
		result_map.put("mat", afterObject.getNativeObjAddr());
		result_map.put("top", obj.getTop()+((fore.rows()-fm.rows()))/2);
		result_map.put("left", obj.getLeft()+((fore.cols()-fm.cols()))/2);
		return result_map;
	}
	
	public Map<String,Object> biggerAndSmaller(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb){
		Map<String,Object> result_map = new HashMap<>();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());
		Mat fore = Imgcodecs.imread(rootPath+obj.getImgUrl().replace("webapp//", "webapp/"),-1);
		
		float rate = (float)fore.rows()/(float)fore.cols();
		
		Mat afterObject = new Mat();
		Mat fm = fore.clone(); 
		int w;
		int left;
		int top;
		Size size;
		if(frame * effectTime / 2 > frame_numb) {
			w = (int)((float)fore.cols()+(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)20);
			size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			left = (int) (obj.getLeft()-(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)10);
			top = (int)(obj.getTop()-(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)10*rate);
		}else {		
			int max_w = (int)((float)fore.cols()+40);
			w = Math.abs((int)((float)max_w-(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)20));
			size = new Size(w,(int)((float)w*rate));
			Imgproc.resize(fm, fm, size);
			left = (int) (obj.getLeft()-(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)10);
			top = (int)(obj.getTop()-(float)(((float)frame_numb/((float)frame * (float)effectTime/2)))*(float)10*rate);
		}
		afterObject = putObjOnBackground.setBackMat(left, top, im2.clone(), fm);
		if(frame_numb==(frame*effectTime)/4){
			thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
		}
		result_map.put("mat", fm.getNativeObjAddr());
		result_map.put("top", obj.getTop()+((fore.rows()-fm.rows()))/2);
		result_map.put("left", obj.getLeft()+((fore.cols()-fm.cols()))/2);
		
		if(frame_numb==(frame*effectTime)/2){
			thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
		}
		
		return result_map;
	}
	
	private Map<String,Object> rotate(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb, int rotateVector) {
		Map<String,Object> result_map = new HashMap<String,Object>();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat object2 = Imgcodecs.imread(rootPath + obj.getImgUrl(), -1);

		Mat im = im2.clone();
		Mat object = object2.clone();
		Point pt = new Point(object.cols() / 2, object.rows() / 2);
		Mat crop;

		if(rotateVector == 1){
			Mat m = Imgproc.getRotationMatrix2D(pt, ((double)(360)/(double)(frame*effectTime-1))*frame_numb*rotateVector, 1.0);
			//System.out.println(((double)(360)/(double)(frame*effectTime-1))*frame_numb*rotateVector);
			Imgproc.warpAffine(object, object, m, new Size(object.cols(), object.rows()));
			/* hoo - 2019-04-03  불필요한 부분 주석 
			List<Mat> objectMatList = new ArrayList<>();
			Core.split(object, objectMatList);
			Imgproc.cvtColor(object, object, Imgproc.COLOR_BGRA2BGR);
			Rect rect = new Rect((int) obj.getLeft(), (int) obj.getTop(), object.cols(), object.rows());
			Mat imRoi = object;
			crop = im.submat(rect);
			imRoi.copyTo(crop, objectMatList.get(3));
			*/
		}else {
			Mat m = Imgproc.getRotationMatrix2D(pt, ((double)(360)/(double)(frame*effectTime-1))*frame_numb*rotateVector, 1.0);
			//System.out.println(((double)(360)/(double)(frame*effectTime-1))*frame_numb*rotateVector);
			Imgproc.warpAffine(object, object, m, new Size(object.cols(), object.rows()));
			/*
			List<Mat> objectMatList = new ArrayList<>();
			Core.split(object, objectMatList);
			Imgproc.cvtColor(object, object, Imgproc.COLOR_BGRA2BGR);
			Rect rect = new Rect((int) obj.getLeft(), (int) obj.getTop(), object.cols(), object.rows());
			Mat imRoi = object;
			crop = im.submat(rect);
			imRoi.copyTo(crop, objectMatList.get(3));
			*/
			
		}
		
		result_map.put("mat", object.getNativeObjAddr());   
		result_map.put("top", obj.getTop()+((object2.rows()-object.rows()))/2);
		result_map.put("left", obj.getLeft()+((object2.cols()-object.cols()))/2);
		//System.out.println("rotateSmaller - done");
		
		
		if(frame_numb==(frame*effectTime)/2){
			thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), object2);
		}
		return result_map;
	}
	public Mat setRotateBackground_map(MoveObjectVo background, List<Map<String,Object>> others, String rootPath){
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat returMat = im2.clone();

		for(int i = 0; i<after_apply.size(); i++) {	
			if(after_apply.get(i).containsKey("backImgTemp")) {
				returMat = (Mat) after_apply.get(i).get("backImgTemp");
			}
		}
		/*		*/
		int initX, initY = 0;
		for(Map<String, Object> vo : others){
			initX = (int)Math.round((float) vo.get("left"));
			initY = (int)Math.round((float)vo.get("top"));
			Mat fore = new Mat((long) vo.get("mat"));			
			returMat = putObjOnBackground.setBackMat(initX, initY, returMat, fore);
		}
		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(returMat.width(), returMat.height());
		imagetypeconvert.bgraTobgr(returMat);
		
		return returMat;
	}

	public MoveObjectVo changeMoveObjectVo(JSONObject obj, JSONObject background) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		String imgUrl = obj.get("imgUrl").toString();
		Double d1 = new Double(background.get("top").toString());
		Double d2 = new Double(background.get("left").toString());
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		Double d5 = new Double(obj.get("degree").toString());
		float top = d3.floatValue() - d1.floatValue();
		float left = d4.floatValue() - d2.floatValue();
		float degree = d5.floatValue();
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		moveObjectVo.setHeight((new Double(obj.get("height").toString())).floatValue());
		moveObjectVo.setWidth((new Double(obj.get("width").toString())).floatValue());
		moveObjectVo.setDegree(degree);
		return moveObjectVo;
	}
	
	public MoveObjectVo changeMoveBackgroundVo(JSONObject obj) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		String imgUrl = obj.get("imgUrl").toString().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "");
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		float top = d3.floatValue();
		float left = d4.floatValue();
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		return moveObjectVo;
	}	
	
	public List<MoveObjectVo> makeOtherList(JSONArray others, MoveObjectVo obj, MoveObjectVo backgroundVo){
		List<MoveObjectVo> moveObjectVos = new ArrayList<>();
		List<MoveObjectVo> result = new ArrayList<>();
		JSONParser parser = new JSONParser();
		for(int i=0;i<others.size();i++){	
			try {
				JSONObject object = (JSONObject) others.get(i);
				String imgUrl = object.get("imgUrl").toString();
				Double d3 = new Double(object.get("top").toString());
				Double d4 = new Double(object.get("left").toString());
				Double d5 = new Double(object.get("degree").toString()); //2017-12-15 add by. KH
				float top = d3.floatValue();
				float left = d4.floatValue();
				float degree = d5.floatValue(); //2017-12-15 add by. KH
				//System.out.println(degree!=obj.getDegree());
				if(!imgUrl.replace("http://localhost:8088","").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "").equals(obj.getImgUrl())&&top!=obj.getTop()&&left!=obj.getLeft()){ //2017-12-15 add by. KH
					MoveObjectVo vo = new  MoveObjectVo();
					vo.setImgUrl(imgUrl);
					vo.setTop(top-backgroundVo.getTop());
					vo.setLeft(left-backgroundVo.getLeft());
					vo.setDegree(degree); //2017-12-15 add by. KH
					vo.setHeight((new Double(object.get("height").toString())).floatValue());
					vo.setWidth((new Double(object.get("width").toString())).floatValue());
					moveObjectVos.add(vo);
				}
				
			}catch(Exception e) {
				//System.out.println(e);
				continue;
			}
		}
		return moveObjectVos;
	}
	
	public Mat setRotateBackground(List<MoveObjectVo> others, String rootPath){
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat resultMat=this.im2.clone();
		
		String path= "";
		for(MoveObjectVo vo : others){
			path = rootPath+vo.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "");
			Mat fore = Imgcodecs.imread(rootPath+vo.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), ""),-1);
			
			resultMat=putObjOnBackground.setBackMat((int)vo.getLeft(), (int)vo.getTop(), resultMat, fore);
		}
		return resultMat;
	}
	
	public Map<String, Object> moveExe(float effectTime, MoveObjectVo firstInfo, MoveObjectVo lastInfo, String rootPath, int frame_numb) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Map<String,Object> result_map = new HashMap<String,Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();
		MoveObjectVo fInfo = firstInfo;
		MoveObjectVo lInfo = lastInfo;
		String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
				.replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "");
		putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
		//putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
		Mat person = Imgcodecs.imread(fUrl, -1);
		result = moveImageExe((int) Math.ceil(fInfo.getLeft()), (int) Math.ceil(fInfo.getTop()), (int) Math.ceil(lInfo.getLeft()),
				(int) Math.ceil(lInfo.getTop()), person, frame_numb, (int)(frame * effectTime-1));
		result_map.put("mat",result.get("mat"));
		result_map.put("left",result.get("left"));
		result_map.put("top",result.get("top"));
		return result_map;
	}
	
	private HashMap<String, Object> movefastExe(float effectTime, MoveObjectVo firstInfo, MoveObjectVo lastInfo, String rootPath, int frame_numb) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		HashMap<String,Object> result_map = new HashMap<String,Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();

		int fLeft = 0;
		int fTop = 0;
		int lLeft = 0;
		int lTop = 0;
		Imgproc.GaussianBlur(im2, im2, new Size(11,11), 11);
		Mat filteredBack = setFastMoveBackground(im2, getGradient(fLeft, fTop, lLeft, lTop));
		im2 = filteredBack.clone();
		MoveObjectVo fInfo = firstInfo;
		MoveObjectVo lInfo = lastInfo;
		String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
				.replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "");
		putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
		//putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
		Mat person = Imgcodecs.imread(fUrl, -1);
		if ((int) fInfo.getLeft() != (int) lInfo.getLeft() || (int) fInfo.getLeft() != (int) lInfo.getLeft())
			result = moveImageExe((int) fInfo.getLeft(), (int) fInfo.getTop(), (int) lInfo.getLeft(), (int) lInfo.getTop(), person, frame_numb, (int)(frame * effectTime));
		result_map.put("mat",result.get("mat"));
		result_map.put("left",result.get("left"));
		result_map.put("top",result.get("top"));
		return result_map;
	}
	
	private HashMap<String, Object> walk(float effectTime, MoveObjectVo firstInfo, MoveObjectVo lastInfo, String rootPath, int frame_numb) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		HashMap<String,Object> result_map = new HashMap<String,Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();
		MoveObjectVo fInfo = firstInfo;
		MoveObjectVo lInfo = lastInfo;
		
		String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "")
				.replace("http://192.168.0.88:8088", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), "");
		putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
		//putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
		Mat person = Imgcodecs.imread(fUrl, -1);
		result = walkImageExe((int) Math.ceil(fInfo.getLeft()), (int) Math.ceil(fInfo.getTop()), (int) Math.ceil(lInfo.getLeft()),
				(int) Math.ceil(lInfo.getTop()), im2.clone(), person, frame_numb, (int)(frame * effectTime));
		result_map.put("mat",result.get("mat"));
		result_map.put("left",result.get("left"));
		result_map.put("top",result.get("top"));
		return result;
	}
	
	private HashMap<String, Object> rotateMove(float effectTime, MoveObjectVo firstInfo, MoveObjectVo lastInfo, String rootPath, int frame_numb) {
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		HashMap<String,Object> result_map = new HashMap<String,Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();
		MoveObjectVo fInfo = firstInfo;
		MoveObjectVo lInfo = lastInfo;
		
		String fUrl = (rootPath + fInfo.getImgUrl()).replace("http://localhost:8088/", "").replace("www."+request.getServerName()+".com","").replace(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(), ""); //.replace("http://192.168.0.125:8088", "");
		putObjOnBackground.resize_mat(fUrl, fInfo.getHeight(), fInfo.getWidth());
		//putObjOnBackground.resize_mat(fUrl, lInfo.getHeight(), lInfo.getWidth());
		Mat person = Imgcodecs.imread(fUrl, -1);
		result = rotateMoveExe((int) fInfo.getLeft(), (int) fInfo.getTop(), (int) lInfo.getLeft(), (int) lInfo.getTop(), 
				im2.clone(), person, frame_numb, (int)(frame * effectTime-1), frame_numb); //★★rotateMoveExe : 실제 분리된 객체를 회전이동 시키는 메소드
		result_map.put("mat",result.get("mat"));
		result_map.put("left",result.get("left"));
		result_map.put("top",result.get("top"));
		return result_map;
	}
	
	//add hoo 2018-11-08
	public Map<String, Object> afterImageMove(float effectTime, MoveObjectVo firstInfo, MoveObjectVo lastInfo, String rootPath, int frame_numb) {
		Mat object = Imgcodecs.imread(rootPath + firstInfo.getImgUrl().replace("webapp//", "webapp/"),-1);
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath + firstInfo.getImgUrl().replace("webapp//", "webapp/"), firstInfo.getHeight(), firstInfo.getWidth());
		Mat objectWeihgt = object.clone();
		
		
		int afterimgCnt = 10;
		Mat background = im2.clone();
		
		if(0 < afterImgHistoy.size() && afterImgHistoy.size() < max*frame -1) {
			int j = 1;
			int startno;
			if(afterimgCnt <= afterImgHistoy.size()) {
				startno = afterImgHistoy.size()-afterimgCnt;
				if( max*frame - afterImgHistoy.size() < afterimgCnt ){
					startno = afterImgHistoy.size()- (((int)max*frame) - afterImgHistoy.size());
				}
			}else {
				startno = 0;
			}
			for(int i = startno; i<afterImgHistoy.size(); i++) {
				afterImgHistoy.get(i).get("top");
				int initX = (int)Math.round((float) afterImgHistoy.get(i).get("left"));
				int initY = (int)Math.round((float)afterImgHistoy.get(i).get("top"));
				
				Core.addWeighted(object, 1-(0.1*j), object, 3 - (j*0.15), 0.0, objectWeihgt);	//가중치 적용
				background = putObjOnBackground.setBackMat(initX, initY, background, objectWeihgt);
			j++;	
			}
		}
		
		HashMap<String, Object> result = new HashMap<String, Object>();		
		result = moveImageExe((int) Math.ceil(firstInfo.getLeft()), (int) Math.ceil(firstInfo.getTop()), (int) Math.ceil(lastInfo.getLeft()),(int) Math.ceil(lastInfo.getTop()), object, frame_numb, (int)(frame * effectTime-1));
		
		Map<String,Object> result_map = new HashMap<String,Object>();		
		result_map.put("mat",result.get("mat"));
		result_map.put("left",result.get("left"));
		result_map.put("top",result.get("top"));
		afterImgHistoy.add(result_map);
		
		result_map.put("backImgTemp",background);
		return result_map;
	}
	
	
	public MoveObjectVo changeMoveObjectVo(JSONObject object) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		Object o1 = object.get("top");
		Double d1 = new Double(Double.parseDouble(object.get("top").toString()));
		Double d2 = new Double(Double.parseDouble(object.get("left").toString()));
		moveObjectVo.setImgUrl(object.get("imgUrl").toString());
		moveObjectVo.setLeft(d1.floatValue());
		moveObjectVo.setTop(d2.floatValue());
		return moveObjectVo;
	}
	
	public List<MoveObjectVo> changeMoveObjectVo(JSONArray array, JSONObject background) { //changeMoveObjectVo
		List<MoveObjectVo> moveObjectVoList = new ArrayList<MoveObjectVo>();
		System.out.println("background.get(top) : " + background.get("top")); //298.5
		Double d1 = new Double(Double.parseDouble(background.get("top").toString())); //Double.parseDouble()
		Double d2 = new Double(Double.parseDouble(background.get("left").toString()));
		float backTop = d1.floatValue();
		float backLeft = d2.floatValue();
		
		for (int i = 0; i < array.size(); i++) {
			MoveObjectVo moveObjectVo = new MoveObjectVo();
			JSONObject obj = (JSONObject) array.get(i);
			Double f1 = new Double(Double.parseDouble(obj.get("top").toString()));
			Double f2 = new Double(Double.parseDouble(obj.get("left").toString()));
			moveObjectVo.setHeight((new Double(obj.get("height").toString())).floatValue());
			moveObjectVo.setWidth((new Double(obj.get("width").toString())).floatValue());
			moveObjectVo.setImgUrl(obj.get("imgUrl").toString());
			moveObjectVo.setLeft(f2.floatValue() - backLeft);
			moveObjectVo.setTop(f1.floatValue() - backTop);
			moveObjectVoList.add(moveObjectVo);
		}
		return moveObjectVoList;
	}
	
	public HashMap<String, Object> moveImageExe(int initX, int initY, int lastX, int lastY, Mat parameterPerson, int frameNum,
			int totalFrame) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		int distanceX = Math.abs(lastX - initX);
		int distanceY = Math.abs(lastY - initY);
		int distance;
		if (distanceX < distanceY) {
			if (lastY == initY) {

			} else {
				distance = Math.abs(lastY - initY);
				float pPf = (float) distance / (float) totalFrame;
				if (lastY < initY) {
					frameNum = -frameNum;
				}
				int startY = initY + (int) Math.ceil((float) frameNum * (float) pPf);
				initX = (int) getX(initX, initY, lastX, lastY, startY);
				initY = startY;
			}
		} else {
			if (lastX == initX) {

			} else {
				distance = Math.abs(lastX - initX);
				float pPf = (float) distance / (float) totalFrame;
				if (lastX < initX) {
					frameNum = -frameNum;
				}
				int startX = initX + (int) Math.ceil((float) frameNum * (float) pPf);
				initY = (int) getY(initX, initY, lastX, lastY, startX);
				initX = startX;
			}
		}
		
			thumbNail = im2.clone();
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		//return putObjOnBackground.setBackMat(initX, initY, im, parameterPerson);
		result.put("mat", parameterPerson.getNativeObjAddr());
		result.put("top", (float)initY);
		result.put("left", (float)initX);
		return result;
	}
	
	public float getY(int fX, int fY, int lX, int lY, int frameNum) {//frameNum은 x에 대한 y의 함수에서 변수 x에 들어갈 값
		return (float) Math.ceil(((((float) lY - (float) fY) / ((float) lX - (float) fX))) * frameNum
				+ Math.ceil(((float) fY - (float) fX * (((float) lY - (float) fY) / ((float) lX - (float) fX)))));
	}

	public float getX(float fX, float fY, float lX, float lY, float frameNum) {//frameNum은 y에 대한 x의 함수에서 변수 y에 들어갈 값
		return (float) Math.ceil((frameNum - (fY - ((fY - lY) / (fX - lX)) * fX)) / ((fY - lY) / (fX - lX)));
	}
	
	private double getGradient(int fLeft, int fTop, int lLeft, int lTop) {
		if (((double) lLeft - (double) fLeft) == 0.0) {
			return 0.0;
		}
		return ((double) lTop - (double) fTop) / ((double) lLeft - (double) fLeft);
	}
	
	private Mat setFastMoveBackground(Mat im, double gradient) {
		System.out.println("gradient : " + gradient);
		int kernelSize = 51;
		int kernelCenterX = (kernelSize + 1) / 2;
		int kernelCenterY = (kernelSize + 1) / 2;
		int yAtXZero = (int) (kernelCenterY - gradient * kernelCenterX);
		int divideCount = getCount(gradient, kernelSize, yAtXZero);
		System.out.println("divideCount : " + divideCount);
		Mat kernel = new Mat(kernelSize, kernelSize, CvType.CV_32F) {
			{
				for (int i = 0; i < kernelSize; i++) {
					for (int j = 0; j < kernelSize; j++) {
						put(i, j, 0);
						int y = (int) (gradient * j + yAtXZero);
						if (y >= 0 || y <= kernelSize) {
							put(y, j, 1 / (double) divideCount);
						}
					}
				}
			}
		};

		Imgproc.filter2D(im, im, -1, kernel);
		return im;
	}
	
	private int getCount(double gradient, int kernelSize, int yAtXZero) {
		if (gradient <= 1) {
			return kernelSize;
		}
		int left = (int) (-yAtXZero / gradient);
		int right = (int) (((kernelSize + 1) - yAtXZero) / gradient);
		return Math.abs(Math.abs(left) - Math.abs(right));
	}
	
	private HashMap<String, Object> walkImageExe(int initX, int initY, int lastX, int lastY, Mat im, Mat parameterPerson, int frameNum,
			int totalFrame) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		int distanceX = Math.abs(lastX - initX);
		int distanceY = Math.abs(lastY - initY);
		int distance;
		int walking_object;
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		if (distanceX < distanceY) {
			if (lastY == initY) {
				walking_object = 0;
			} else {
				distance = Math.abs(lastY - initY);
				float pPf = (float) distance / (float) totalFrame;
				if (lastY < initY) {
					frameNum = -frameNum;
				}
				int startY = initY + (int) ((float) frameNum * pPf);
				initX = (int) getX(initX, initY, lastX, lastY, startY);
				initY = startY;
				walking_object = (int) (Math.sin(initX / 12) * ((double) im.height() / 10));
			}
		} else {
			if (lastX == initX) {
				walking_object = 0;
			} else {
				distance = Math.abs(lastX - initX);
				float pPf = (float) distance / (float) totalFrame;
				if (lastX < initX) {
					frameNum = -frameNum;
				}
				int startX = initX + (int) ((float) frameNum * pPf);
				initY = (int) getY(initX, initY, lastX, lastY, startX);
				initX = startX;
				walking_object = (int) (Math.sin(initX / 12) * ((double) im.height() / 10));
			}
			
		}
		if (frameNum == (frame * max) / 4) {
			thumbNail = im2.clone();
		}
		result.put("mat", parameterPerson.getNativeObjAddr());
		result.put("top", (float)walking_object + initY);
		result.put("left", (float)initX);
		return result;

	}	
	
	private HashMap<String, Object> rotateMoveExe(int initX, int initY, int lastX, int lastY, Mat im, Mat parameterPerson, int frameNum, int totalFrame, int i) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		System.out.println("initX, initY, lastX, lastY, im, parameterPerson, frameNum, totalFrame");
		System.out.println(initX +", " + initY +", " + lastX +", " + lastY +", " + im +", " + parameterPerson +", " + frameNum +", " + totalFrame);		
		if (lastX == initX) {

		} else {
			int distance = Math.abs(lastX - initX);
			float pPf = (float) distance / (float) totalFrame;
			if (lastX < initX) {
				frameNum = -frameNum;
			}
			System.out.println("distance : " + distance +", pPf : " + pPf +", frameNum : " + frameNum);
			int startX = initX + (int) ((float) frameNum * pPf);
			System.out.println("initX : " + initX +", initY : " + initY +", lastX : " + lastX +", lastY : " + lastY +", startX : " + startX);
			initY = (int) getY(initX, initY, lastX, lastY, startX);
			initX = startX;
			System.out.println("initX : " + initX +", initY : " + initY);
		}
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		Mat afterObject = new Mat();		
		//GetRotationMatrix2D 함수로 원하는 각만큼 회전 시켜줄 틀을 만들어 준다.
		//WarpAffine 함수로 원하는 틀에 맞춰 변경비켜준다.
//		Mat m = Imgproc.getRotationMatrix2D(new Point(parameterPerson.width() / 2, parameterPerson.height() / 2), frameNum * 10, 1.0);
		Mat m = Imgproc.getRotationMatrix2D(new Point(parameterPerson.width() / 2, parameterPerson.height() / 2), ((double)(360)/(double)(totalFrame))*i, 1.0);
		Imgproc.warpAffine(parameterPerson, afterObject, m, new Size(parameterPerson.cols(), parameterPerson.rows()));
		if (frameNum == (frame * max) / 4) {
			thumbNail = im2.clone();
		}
		//return putObjOnBackground.setBackMat(initX, initY, im, afterObject);
		result.put("mat", afterObject.getNativeObjAddr());
		result.put("top", (float)initY);
		result.put("left", (float)initX);
		return result;
	}
	
	//add hoo 2018-10-31 
	private Map<String, Object> eyeBlink(float effectTime, MoveObjectVo obj, String rootPath, int frame_numb) {		
		Map<String, Object> resultMap = new HashMap<>();
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		putObjOnBackground.resize_mat(rootPath + obj.getImgUrl().replace("webapp//", "webapp/"), obj.getHeight(), obj.getWidth());		
		Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("webapp//", "webapp/"),-1);
		Mat fm = fore.clone();
		Size size = new Size();
		Point pt = new Point();
		int left;
		int top;
		int operNo = frame_numb % frame;
		if( 6 < operNo && operNo <= 12 ){
			float rate = (float)fore.rows()/(float)fore.cols();			
			int h = (int)((fore.rows()/2)-((fore.rows()/2)-5));
			size = new Size(fore.cols() ,h);
			pt = new Point(size.width / 2, size.height / 2);			
			left = (int) obj.getLeft();
			top = (int) ((int) obj.getTop()+(fore.rows()/2)*rate);			
			Mat m = Imgproc.getRotationMatrix2D(pt, obj.getDegree()*-1, 1.0);
			Imgproc.warpAffine(fm, fm, m, new Size(fore.cols(), fore.rows()));
		}else{			
			size = new Size(fore.cols() ,fore.rows());		
			pt = new Point(size.width / 2, size.height / 2);		
			left = (int) obj.getLeft();
			top = (int) obj.getTop();		
		}
		Imgproc.resize(fm, fm, size);		
		if(frame_numb == 1) { // 한번만 실행 
			thumbNail=putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im2.clone(), fm);
		}
		resultMap.put("mat", fm.getNativeObjAddr());
		resultMap.put("top", (float) top);
		resultMap.put("left", (float) left);		
		return resultMap;
	}
	
	
}
