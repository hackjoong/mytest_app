package com.Toonivie.OpenCv.Object_Detection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.OpenCv.Grabcut.GrabcutServiceAdvanced;
import com.Toonivie.OpenCv.Grabcut.Vo.GrabcutRectangleVo;

@Controller
public class ObjectDetectionController {
	@Autowired
	GrabcutServiceAdvanced grabcutServiceAdvanced;
	@Autowired
	ObjectDetectionService objectDetectionService;
	
	@RequestMapping(value="/ObjectDetection",method=RequestMethod.POST)
	@ResponseBody
	public List<Map> ObjectDetection(HttpServletRequest request, @RequestParam("clicked_color_rgb[]") ArrayList<Integer> clicked_color_rgb ){
		long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("ObjectDetection used memory is " + used + " bytes");

		List<Map> result = new ArrayList<Map>();
		List<String[]> resultList = new ArrayList<String[]>();
		int filling_type = Integer.parseInt(request.getParameter("filling_type")); 
		Boolean blur_status = Boolean.parseBoolean(request.getParameter("blur_status")); 
		String rootPath = request.getSession().getServletContext().getRealPath("resources/cvimgs/");
		//System.out.println("obj rootpath: "+rootPath);
//		int mode = Integer.parseInt(request.getParameter("Gmode"));
		int mode = 0;
		int beX;
		int beY;
		int afX;
		int afY;
		String autofill_bg = null;
		
        String imgUrl="";
        if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
        	imgUrl=request.getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        }else{
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        }
        resultList = objectDetectionService.init_object_dectection(imgUrl, request);
        //System.out.println(resultList.toString());
        //System.out.println(imgUrl);
        for(int i=0;i<resultList.size();i++) {
    		if(i==0) {
    			result.add(grabcutServiceAdvanced.grabcut(mode, imgUrl, rootPath, resultList.get(i)[0], Integer.parseInt(resultList.get(i)[1]), Integer.parseInt(resultList.get(i)[2]), Integer.parseInt(resultList.get(i)[3]), Integer.parseInt(resultList.get(i)[4]), filling_type, blur_status, clicked_color_rgb, autofill_bg)); 
    			//System.out.println(i+": "+result.get(i).get("backGroundFileName"));
    			//System.out.println("size : "+resultList.size());
    		}else {
    			//remove rootpath 
    			imgUrl = request.getSession().getServletContext().getRealPath("resources/cvimgs/")+(String) result.get(i-1).get("backGroundFileName");
    			result.add(grabcutServiceAdvanced.grabcut(mode, imgUrl, rootPath, resultList.get(i)[0], Integer.parseInt(resultList.get(i)[1]), Integer.parseInt(resultList.get(i)[2]), Integer.parseInt(resultList.get(i)[3]), Integer.parseInt(resultList.get(i)[4]), filling_type, blur_status, clicked_color_rgb, autofill_bg));
    			autofill_bg = (String) result.get(i).get("autofill_bg");
    			//System.out.println(i+": "+result.get(i).get("backGroundFileName"));
    			//System.out.println("size : "+resultList.size());
    		}
        }
        
        
        return result;
	}
}
