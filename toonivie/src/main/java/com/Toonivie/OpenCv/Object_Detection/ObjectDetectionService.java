package com.Toonivie.OpenCv.Object_Detection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

@Service
public class ObjectDetectionService {
	private StringBuffer buffer;
	
/*	public String inputCommand(String cmd, String rootpath) {		
		buffer = new StringBuffer();
		buffer.append(rootpath+"\\resources\\lib\\Object_Detection\\Object_Detection.exe"); //revised_exe -> colorTrans 변경, by shkwak 2018-03-16
		buffer.append(cmd);
		//System.out.println(buffer);
		return buffer.toString();
	}*/
	
	public ArrayList<String[]> Object_Detection_Service(List<String> cmd) {
		Process process = null;
		BufferedReader bufferedReader;
		StringBuffer readBuffer = null;
		ArrayList<String[]> result_list = new ArrayList<String[]>();
		String[] coord = new String[5];
		System.out.println("cmd: "+cmd.toString());
		try {
			process = new ProcessBuilder(cmd).start();
			int processOutput = process.waitFor();
			if(processOutput == 0){
				bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = bufferedReader.readLine();
				while((line = bufferedReader.readLine()) != null) {
					//System.out.println(line);
					if(line.contains(",")) {
						for(int i=0;i<5;i++) {
								coord[i] = line.split(",")[i];
						}
						result_list.add(coord);
					}
					coord = new String[5];
				}

	/*			do {
					line = bufferedReader.readLine();
					if(line.contains("#")) {
						result.add(line);
					}
				}while(!line.equals("success"));*/
				
				return result_list;
			}
		}catch (Exception e) {
			e.printStackTrace();
			process.destroy();
			//System.exit(1);
		}
		return result_list;
	}
	
	public ArrayList<String[]> init_object_dectection(String filepath, HttpServletRequest request) {
		String rootpath = request.getServletContext().getRealPath("resources/cvimgs/");
		List<String> command_list = new ArrayList<String>();
		//filepath = rootpath + filepath;
//		String command = inputCommand(" " + filepath +  " ", request.getServletContext().getRealPath("").replace("\\src\\main\\webapp\\", ""));
//		String command = inputCommand(" " + filepath +  " ", request.getServletContext().getRealPath(""));
		command_list.add(request.getServletContext().getRealPath("") + "\\resources\\lib\\Object_Detection\\Object_Detection.exe");
		command_list.add(filepath);
		
		return Object_Detection_Service(command_list);
	}
}
