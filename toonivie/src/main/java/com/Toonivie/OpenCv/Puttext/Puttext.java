package com.Toonivie.OpenCv.Puttext;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

public class Puttext {
	
	public void puttext_init(String rootpath, String img_path, String font_type, String font_size, String font_txt, String font_position_x, String font_position_y, String font_color_r, String font_color_g, String font_color_b, String font_thickness) {
		Process process = null;
		BufferedReader bufferedReader;
		List<String> cmd = new ArrayList<String>();
		cmd.add(rootpath + "resources\\lib\\PutText\\PutText.exe");
		cmd.add(img_path);
		//cmd.add(font_type);
		cmd.add("c:\\windows\\fonts\\malgunsl.ttf");
		cmd.add(font_size);
		cmd.add(font_txt);
		cmd.add(font_position_x);
		cmd.add(font_position_y);
		cmd.add(font_color_r);
		cmd.add(font_color_g);
		cmd.add(font_color_b);
		cmd.add(font_thickness);
		
		System.out.println("cmd: "+cmd.toString());
		try {
			process = new ProcessBuilder(cmd).start();
			int processOutput = process.waitFor();
			if(processOutput == 0){
				bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = bufferedReader.readLine();
				while((line = bufferedReader.readLine()) != null) {
					System.out.println(line);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			process.destroy();
			//System.exit(1);
		}
	}
}
