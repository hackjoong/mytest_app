package com.Toonivie.OpenCv.Rotate;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rotate")
public class RotateController {
	@Autowired
	RotateService rotateService;
	@RequestMapping("/rotateExe")
	@ResponseBody
	public HashMap<String,Object> rotateExe(HttpServletRequest request){
		return rotateService.rotateExe(request);
	}
}
