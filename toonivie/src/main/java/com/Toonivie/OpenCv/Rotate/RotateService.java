package com.Toonivie.OpenCv.Rotate;

import java.io.File;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

@Service
public class RotateService {

	public HashMap<String,Object> rotateExe(HttpServletRequest request){
		HashMap<String,Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		
		Mat src1 = Imgcodecs.imread(request.getServletContext().getRealPath("/")+"resources/cvimgs/result20170606175435.png");
		String today = "0101010";
		VideoWriter videoWriter  = new VideoWriter();
        
        File file = new File(request.getServletContext().getRealPath("/")+"resources/cvMov/rotate"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        String rootPath = request.getServletContext().getRealPath("/");
       /* videoWriter.open("src/main/webapp/resources/cvMov/rotate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),20,src1.size());*/
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/rotate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),20,src1.size());
		for(int i=0;i<80;i++){
			Mat src = Imgcodecs.imread(request.getServletContext().getRealPath("/")+"resources/cvimgs/result20170606175435.png");
			Point pt = new Point(src.cols()/2,src.rows()/2);
			Mat m = Imgproc.getRotationMatrix2D(pt, i, 1.0);
			Imgproc.warpAffine(src, src, m, new Size(src.cols(),src.rows()));
			videoWriter.write(src);
		}
		videoWriter.release();
		
		return resultMap;
	}
}
