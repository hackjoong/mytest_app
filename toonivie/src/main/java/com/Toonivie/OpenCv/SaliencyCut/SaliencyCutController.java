package com.Toonivie.OpenCv.SaliencyCut;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Util.DataURL_tofile;


@Controller
@RequestMapping("/sailencyCut")
public class SaliencyCutController {
	@Autowired
	SaliencyCutService sailencyCutService;
	
	@RequestMapping(value="/sailencyCutExe",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> sailencyCutExe(HttpServletRequest request,@RequestParam("bgListX[]") ArrayList<Integer> bgListX,@RequestParam("bgListY[]") ArrayList<Integer> bgListY
    		,@RequestParam("fgListY[]") ArrayList<Integer> fgListY,@RequestParam("fgListX[]") ArrayList<Integer> fgListX,@RequestParam("clicked_color_rgb[]") ArrayList<Integer> clicked_color_rgb){
		
/*		DataURL_tofile dataURL_tofile = new DataURL_tofile();
		dataURL_tofile.dataURL_toPNG(request.getParameter("fg_mask"), "/resources/cvimgs/", "fg_maskimg", request);
		dataURL_tofile.dataURL_toPNG(request.getParameter("bg_mask"), "/resources/cvimgs/", "bg_maskimg", request);
*/		
		
//        int mode = Integer.parseInt(request.getParameter("Gmode"));
        int iterate = Integer.parseInt(request.getParameter("iterate"));
        String imgUrl="";
        if(request.getParameter("imgUrl").contains("resources/cvimgs/")){
        	imgUrl=request .getSession().getServletContext().getRealPath("/")+request.getParameter("imgUrl");
        }else{
        	imgUrl=request.getSession().getServletContext().getRealPath("resources/cvimgs/")+request.getParameter("imgUrl");
        }
        
        String rootPath = request.getSession().getServletContext().getRealPath("");
		//return grabcutService.grabcutExe(bgListX, bgListY, fgListX, fgListY, grabcutRectangleVo, mode, imgUrl,rootPath);
		System.out.println("imgUrl: "+imgUrl);
		System.out.println("rootPath: "+rootPath);
		
        int beX = Integer.parseInt(request.getParameter("GbeX"));
        int beY = Integer.parseInt(request.getParameter("GbeY"));
        int afX = Integer.parseInt(request.getParameter("GafX"));
        int afY = Integer.parseInt(request.getParameter("GafY"));
        Boolean blur_status = Boolean.parseBoolean(request.getParameter("blur_status"));
        int filling_type = Integer.parseInt(request.getParameter( "filling_type")); //add by KH, 2017-12-21
        int mode = Integer.parseInt(request.getParameter( "Gmode")); //add by KH, 2017-12-21
		Mat im = Imgcodecs.imread(imgUrl);
		Rect rect = new Rect();
		if (mode == 1) {
			rect = setRect(bgListX, bgListY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.x+rect.width;
			afY = rect.y+rect.height;
		} else {
			if(beX>afX){
				int temp = afX;
				afX=beX;
				beX=temp;
			}
			if(beY>afY){
				int tempY = afY;      
				afY=beY;
				beY=tempY;
			}
		  	rect = setRect_cali(beX, beY, afX, afY, im);
			beX = rect.x;
			beY = rect.y;
			afX = rect.x + rect.width;
			afY = rect.y + rect.height;
		}
		
        try {
			sailencyCutService.SailencyCut_getinfo(imgUrl, rootPath);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        //sailencyCutService.SailencyCut_getresult(imgUrl, rootPath, beX, beY, afX, afY);
        
        try {
			return sailencyCutService.SailencyCut_getresult(rootPath, imgUrl, beX, beY, afX, afY, iterate, mode, rect, bgListX, bgListY, fgListX, fgListY, clicked_color_rgb, filling_type, blur_status);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
        
       // return sailencyCutService.SailencyCut_getresult(imgUrl, rootPath, beX, beY, afX, afY); //modified by KH, 2017-12-21
	}
	
	private Rect setRect(ArrayList<Integer> bgListX, ArrayList<Integer> bgListY, Mat im) {
		int minX = bgListX.get(1);
		int minY = bgListY.get(1);
		int maxX = bgListX.get(1);
		int maxY = bgListY.get(1);
		System.out.println("###bgListX.size() : " + bgListX.size());
		for (int x = 0; x < bgListX.size() - 1; x++) {
			if (bgListX.get(x) > maxX) {
				maxX = bgListX.get(x);
			}
			if (bgListX.get(x) < minX) {
				minX = bgListX.get(x);
			}
			if (bgListY.get(x) > maxY) {
				maxY = bgListY.get(x);
			}
			if (bgListY.get(x) < minY) {
				minY = bgListY.get(x);
			}
		}
		System.out.println("### free " + minX + "," + minY + "    " + maxX + "," + maxY);
		if(minX < 1) {
			minX = 1;
		}
		if(minY < 1) {
			minY = 1;
		}
		if(maxX >= im.width()) {
			maxX = im.width()-1;
		}
		if(maxY >= im.height()) {
			maxY = im.height()-1;
		}
/*		if (minX - 10 < 0) {
			minX = 0;
		} else {
			minX = minX - 10;
		}
		if (maxX + 10 > im.cols()) {
			maxX = im.cols();
		} else {
			maxX = maxX + 10;
		}
		if (minY - 10 < 0) {
			minY = 0;
		} else {
			minY = minY - 10;
		}
		if (maxY + 10 > im.rows()) {
			maxY = im.rows();
		} else {
			maxY = maxY + 10;
		}*/
		
		System.out.println("### free " + maxX + "," + im.width() + "    " + maxY + "," + im.height());
		System.out.println("### free " + minX + "," + minY + "    " + maxX + "," + maxY);
		return new Rect(minX, minY, maxX - minX, maxY - minY);
	}

	 private Rect setRect_cali(int beX, int beY, int afX, int afY, Mat im) {
			if(beX <= 1) {
				beX = 1;
			}
			if(beY <= 1) {
				beY = 1;
			}
			if(afX >= im.width()) {
				afX = im.width()-1;
			}
			if(afY >= im.height()) {
				afY = im.height()-1;
			}
			System.out.println("im.width(): " + im.width() +" im.height(): " + im.height());
			System.out.println("bex: " + beX +" bey: " + beY + " afx: " + afX + " afy: " + afY);
			return new Rect(beX, beY, afX - beX, afY - beY);
		}
}
