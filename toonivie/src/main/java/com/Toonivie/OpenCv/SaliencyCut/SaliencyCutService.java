package com.Toonivie.OpenCv.SaliencyCut;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.Ext_API;

@Service
public class SaliencyCutService {
	float first_threshold = 0;
	float second_threshold = 0;
	static Mat mask1;
	 public HashMap<String, Object> SailencyCut_getresult(String rootpath, String target_imgPath, int beX, int beY, int afX, int afY, int iterate, int mode, Rect rect, ArrayList<Integer> bgListX, ArrayList<Integer> bgListY,
				ArrayList<Integer> fgListX, ArrayList<Integer> fgListY, ArrayList<Integer> clicked_color_rgb, int filling_type, Boolean blur_status) throws IOException, InterruptedException{
		  Process p = null;	
		  CreateFileUtils cfu = new CreateFileUtils();
		  HashMap<String, Object> resultMap = new HashMap<>();
		  String get_date = cfu.getToday(1);
		  
		  //rect = new Rect(beX, beY, afX - beX, afY - beY);
		  Mat im = Imgcodecs.imread(target_imgPath);
		  mask1 = new Mat(im.size(), CvType.CV_8UC1, new Scalar(Imgproc.GC_BGD));
		  grabcutMaskSetting(beX, beY, afX, afY, mode, bgListX, bgListY, im, fgListX, fgListY, rect); //grabcutMaskSetting
		  System.out.println(beX + " beX " + beY + " beY " + afX + " afX " + afY + " afY ");
		  
		  String[] DataInfo = {
					/*0. Program URL*/ "\""+rootpath+"resources\\lib\\SaliencyCut\\TestSaliency.exe\"",
					/*1. Target Image URL*/ ""+target_imgPath+"",
					/*2. saveUrl_segImg*/  ""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png",
					/*3. saveUrl_hgImg */ ""+rootpath+"resources\\cvimgs\\hgsaliency"+get_date+".png",
					/*4. saveUrl_bgImg*/ ""+rootpath+"resources\\cvimgs\\bgsaliency"+get_date+".png",
					/*5. 시작 X 좌표*/ Integer.toString(beX),
					/*6. 시작 y 좌표*/ Integer.toString(beY),
					/*7. 종료 x 좌표*/ Integer.toString(afX),
					/*8. 종료 y 좌표*/ Integer.toString(afY),
					/*9. Base Threshold*/ Float.toString(first_threshold),                        
					/*10. middle Threshold*/ Float.toString(second_threshold),
					/*11. 반복 횟수*/ Integer.toString(iterate)
					};
		  String[] DataInfo3 = {
					/*0. Program URL*/ "\""+rootpath+"resources\\lib\\SaliencyCut\\TestSaliency.exe\"",
					/*1. Target Image URL*/ ""+target_imgPath+"",
					/*2. saveUrl_segImg   */ ""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png",
					/*3. saveUrl_hgImg  */ ""+rootpath+"resources\\cvimgs\\hgsaliency"+get_date+".png",
					/*4. saveUrl_bgImg */""+rootpath+"resources\\cvimgs\\bgsaliency"+get_date+".png",
					/*5. initUrl_mask */ ""+rootpath+"resources\\cvimgs\\mask"+get_date+".png",
					/*6. Base Threshold*/ Float.toString(first_threshold),
					/*7. middle Threshold*/ Float.toString(second_threshold),
					/*8. 반복 횟수*/ Integer.toString(iterate)
					
					};	
/*		  for(String result : DataInfo) {
			  System.out.println(result);
		  }*/
/*		  for(String me : DataInfo) {
			  System.out.println("input: "+ me);
		  }
		  System.out.println(DataInfo);*/
		  	try {
		  		if(mode == 0) {
		  			p = new ProcessBuilder(DataInfo).start();
		  		}else {
		  			Imgcodecs.imwrite(""+rootpath+"resources\\cvimgs\\mask"+get_date+".png", mask1);
		  			p = new ProcessBuilder(DataInfo3).start();
		  		}
		  		
			}  catch (IOException ex) {
				ex.printStackTrace();
			}
			finally {
			  BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
			  String s;
			  while((s = stdOut.readLine()) != null) System.out.println(s);
				p.waitFor();
				System.out.println("Process Done");
				p.destroy();
				System.out.println("Process destroy");
			}


		  	//add BG color
		  	Mat fg3 = Imgcodecs.imread(""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png", -1);
		  	if(mode != 0 ) {
		  		fg3 = fg3.submat(rect);
		  		Imgcodecs.imwrite(""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png", fg3);
		  	}
		  	Mat crop_fg = fg3.submat(new Rect(0, 10, fg3.cols(), fg3.rows()-20));
		  	Imgcodecs.imwrite(""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png", crop_fg);
		  	fg3 = Imgcodecs.imread(""+rootpath+"resources\\cvimgs\\grabcut\\saliency"+get_date+".png", -1);
		  	//split alpha use as mask
		  	List<Mat> rgb1 = new ArrayList<>();
			Core.split(fg3, rgb1);
			List<Mat> rgba1 = new ArrayList<>();
			rgba1.add(rgb1.get(0));
			rgba1.add(rgb1.get(1));
			rgba1.add(rgb1.get(2));
			rgba1.add(rgb1.get(3));
			
			rect = new Rect(beX, beY, fg3.cols(), fg3.rows());
			Mat bg = Imgcodecs.imread(""+rootpath+"resources\\cvimgs\\bgsaliency"+get_date+".png");
		  	mask1 = new Mat(bg.size(), CvType.CV_8UC1, new Scalar(Imgproc.GC_BGD));
		  	Mat mask_roi = mask1.submat(rect);
		  	Core.add(mask_roi, rgb1.get(3), mask_roi);
		  	//rgb.get(3).copyTo(mask_roi);
		  	
		  	Mat fg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		  	im.copyTo(fg, mask1);
		  	Mat fg2 = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		  	Core.subtract(im, fg, fg2); //subtract
		  	
		 // �쁾遺꾨━�맂 �쁺�뿭�쓣 梨꾩슦�뒗 肄붾뱶, checked shkwak, 2017-11-16
			//Imgcodecs.imwrite("mask1.png", mask1); //二쇱꽍泥섎━ by shkwak, 2018-02-08
			int rectWidth = afX - beX;
			int rectHeight = afY - beY;
			//遺꾨━�맂 �쁺�뿭 �깋�긽 蹂�寃�  add by KH 2017-12-21
			//Imgcodecs.imwrite("testmask.png", mask1);
			if(filling_type == 0) {
				System.out.println("afY: "+afY);
				for (int i = beY; i <= afY; i++) {
					for (int j = beX; j <= beX + rectWidth; j++) {
						//System.out.println("i: "+ i + " j: " + j);
						double[] mask1Pixel = mask1.get(i, j);
						if (mask1Pixel[0] > 150) {
							double[] putPixel = { 0, 0, 0 };
							/*putPixel[0] = fg2.get(i, j - 10)[0];
							putPixel[1] = fg2.get(i, j - 10)[1];
							putPixel[2] = fg2.get(i, j - 10)[2];*/
							fg2.put(i, j, putPixel);
						}
					}
				}
			}else if(filling_type == 1) {
				for (int i = beY; i <= afY; i++) {
					for (int j = beX; j <= beX + rectWidth; j++) {
						double[] mask1Pixel = mask1.get(i, j);
						if (mask1Pixel[0] > 150) {
							double[] putPixel = { 255, 255, 255 };
							/*putPixel[0] = fg2.get(i, j - 10)[0];
							putPixel[1] = fg2.get(i, j - 10)[1];
							putPixel[2] = fg2.get(i, j - 10)[2];*/
							fg2.put(i, j, putPixel);
						}
					}
				}
			}else if(filling_type == 2) {
				for (int i = beY; i <= afY; i++) {
					for (int j = beX; j <= beX + rectWidth / 2; j++) {
						double[] mask1Pixel = mask1.get(i, j);
						//System.out.println(mask1Pixel[0]);
						if (mask1Pixel[0] > 150) {
							//System.out.println("in");
							double[] putPixel = { 255, 255, 255 };
							putPixel[0] = fg2.get(i, j - 10)[0];
							putPixel[1] = fg2.get(i, j - 10)[1];
							putPixel[2] = fg2.get(i, j - 10)[2];

							fg2.put(i, j, putPixel);
						}
					}
				}
				for (int i = beY; i <= afY; i++) {
					for (int j = afX; j > beX + rectWidth / 2; j--) {
						double[] mask1Pixel = mask1.get(i, j);
						if (mask1Pixel[0] > 150) {
							double[] putPixel = { 255, 255, 255 };
							int jp10 = j + 10;
							if (jp10 > im.width()) {
								jp10 = im.width();
							}
							putPixel[0] = fg2.get(i, jp10 - 1)[0];
							putPixel[1] = fg2.get(i, jp10 - 1)[1];
							putPixel[2] = fg2.get(i, jp10 - 1)[2];

							fg2.put(i, j, putPixel);
						}
					}
				}
			}else if(filling_type ==3) {
				for (int i = beY; i <= afY; i++) {
					for (int j = beX; j <= beX + rectWidth; j++) {
						double[] mask1Pixel = mask1.get(i, j);
						if (mask1Pixel[0] > 150) {
							double[] putPixel = { clicked_color_rgb.get(2), clicked_color_rgb.get(1), clicked_color_rgb.get(0) };
	/*						putPixel[0] = fg2.get(i, j - 10)[0];
							putPixel[1] = fg2.get(i, j - 10)[1];
							putPixel[2] = fg2.get(i, j - 10)[2];*/
							fg2.put(i, j, putPixel);
						}
					}
				}
				
			}else if(filling_type==4) {
			}
			Mat blurMask = fg2.submat(rect);
			if(blur_status) {
				Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), 11);
			}

			//Imgcodecs.imwrite("fgGrabcut2.png", fg2); //二쇱꽍泥섎━ by shkwak, 2018-02-08
			//Imgcodecs.imwrite("fgGrabcut3.png", blurMask);

			Mat bgMask = new Mat(im.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
			Mat src = fg.clone();
			Mat dst = new Mat();
			Mat tmp = new Mat();
			Mat alpha = mask1.clone();

			Imgproc.cvtColor(src, tmp, Imgproc.COLOR_BGR2GRAY);

			List<Mat> rgb = new ArrayList<>();
			Core.split(src, rgb);
			List<Mat> rgba = new ArrayList<>();
			rgba.add(rgb.get(0));
			rgba.add(rgb.get(1));
			rgba.add(rgb.get(2));
			rgba.add(alpha);
			Core.merge(rgba, dst);

			// 
			Imgcodecs.imwrite(rootpath+"/resources/cvimgs/grabcut/" + "full.png", dst);

			Mat cropImage = dst.submat(rect);

			// 

			String backGroundFileName = "/resources/cvimgs/bgsaliency"+get_date+".png";
	        
	        Imgcodecs.imwrite(rootpath+backGroundFileName, fg2);
	        
	        //adding alpha channel to BG
			rgb = new ArrayList<>();
			dst = new Mat();
			rgba = new ArrayList<>();
			Core.bitwise_not(alpha, alpha);
			Core.split(fg2, rgb);
			rgba.add(rgb.get(0));
			rgba.add(rgb.get(1));
			rgba.add(rgb.get(2));
			rgba.add(alpha);
			Core.merge(rgba, dst);
			//debuging by KH 180426
		/*	Imgcodecs.imwrite("TESET_targetBG.png", dst);
			System.out.println("dst check alpha channel ::::::::::::::::"+dst.channels());*/
			/*		System.out.println("fg2 check alpha channel ::::::::::::::::"+fg2.channels());
	        Imgcodecs.imwrite("TESET.png", mask1);
	        Imgcodecs.imwrite("TESET_BG.png", fg2);
	        Imgcodecs.imwrite("TESET_targetBG.png", dst);*/
			//System.out.println("###grabcut Path : "+rootPath+"/grabcut/" + "grabcut.png");
			
	        String resultImageName = cfu.getToday(0)+".png";
	        //auto filling api set by KH 180503
	        if(filling_type==4) {
	        	Imgcodecs.imwrite(rootpath+backGroundFileName, dst);
	        	Ext_API inpainting = new Ext_API();
	        	String inpainting_file = inpainting.input_BG_img(rootpath+backGroundFileName, backGroundFileName, rootpath);
	        	Mat inpainted_bg = after_inpainting(im, rect, inpainting_file, rootpath);
	    		if(blur_status) {
	    			blurMask = inpainted_bg.submat(rect);
	    			Imgproc.GaussianBlur(blurMask, blurMask, new Size(11, 11), 11);
	    		}
	        	Imgcodecs.imwrite(rootpath+backGroundFileName, inpainted_bg);
	        }
		  	
			resultMap.put("resultImg", "/resources/cvimgs/grabcut/saliency"+get_date+".png");
			resultMap.put("backGroundFile", backGroundFileName);
	        resultMap.put("backGroundWidth", bg.cols());
	        resultMap.put("backGroundHeight", bg.rows());
			resultMap.put("width", rect.width);
			resultMap.put("height", rect.height);
			resultMap.put("beX", beX);
			resultMap.put("beY", beY);
			
			return resultMap;
			
     }
	 public void SailencyCut_getinfo(String target_imgPath, String rootpath) throws IOException, InterruptedException{
		  Process p = null;	
		  CreateFileUtils cfu = new CreateFileUtils();
		  String[] DataInfo = {
					/*0. Program URL*/ "\""+rootpath+"resources\\lib\\SaliencyCut\\TestSaliency.exe\"",
					/*1. Target Image URL*/ target_imgPath,
					/*2. saveUrl_hgImg */ ""+rootpath+"resources\\cvimgs\\hg"+cfu.getToday(1)+"",
					};
		  for(String me : DataInfo) {
			  System.out.println("get info "+ me);
		  }
		  	try {
		  		p = new ProcessBuilder(DataInfo).start();
			}  catch (IOException ex) {
				ex.printStackTrace();
			}
			finally {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
			  BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
			  String s;
			  int i = 0;
			  String[] result = new String[] {};
			  while((s = stdOut.readLine()) != null) {
				  System.out.println("readline: " + s);
				  if(i==0) {
					  result = s.split(" ");
				  }
				  i++;
			  }

			  first_threshold = Float.parseFloat(result[3]);
			  second_threshold = Float.parseFloat(result[7]);
				p.waitFor();
				System.out.println("Process Done"); 
				p.destroy();
				System.out.println("Process destroy");
			}
			
      }
	 
	 private void grabcutMaskSetting(int beX, int beY, int afX, int afY, int mode, ArrayList<Integer> bgListX,
				ArrayList<Integer> bgListY, Mat im, ArrayList<Integer> fgListX,
				ArrayList<Integer> fgListY, Rect rect) {
			byte[] buffer = new byte[3];
			System.out.println("### rect " + rect.x + "," + rect.width + "    " + rect.y + "," + rect.height);
			for (int x = rect.x; x < rect.x + rect.width; x++) {
				for (int y = rect.y; y < rect.y + rect.height; y++) {
					mask1.get(y, x, buffer);
					int value = buffer[0];
					buffer[0] = Imgproc.GC_PR_FGD; // for sure background
					mask1.put(y, x, buffer);
				}
			}
			if (bgListX.size() > 1) {
				for (int x = 1; x < bgListX.size() - 2; x++) {
					Imgproc.line(mask1, new Point(bgListX.get(x), bgListY.get(x)),
							new Point(bgListX.get(x + 1), bgListY.get(x + 1)), new Scalar(Imgproc.GC_BGD), 10);
				}
			}
			if (bgListX.size() > 1) {
				Imgproc.line(mask1, new Point(bgListX.get(1), bgListY.get(1)),
						new Point(bgListX.get(bgListX.size() - 2), bgListY.get(bgListY.size() - 2)),
						new Scalar(Imgproc.GC_BGD), 10);
			}

			bfsMat(rect);

			int count = 0;
			for (int x = beX; x <= afX; x++) {
				for (int y = beY; y <= afY; y++) {
					mask1.get(y, x, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_BGD) {
						count++;
					}
				}
			}
			System.out.println("bgcount : " + count);

			Mat mask2 = mask1.clone();
			for (int i = 0; i < mask2.rows(); i++) {
				for (int j = 0; j < mask2.cols(); j++) {
					mask2.get(i, j, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_BGD) {
						// buffer[0]=125;
						mask2.put(i, j, 125);
					}
				}
			}
			//Imgcodecs.imwrite("maskShow.png", mask2);
		}
	 
		private void bfsMat(Rect rect) {
			Queue<Point> q = new LinkedList<Point>();
			q.add(new Point(rect.x, rect.y));
			bfs(q, rect);
		}
		private void bfs(Queue<Point> q, Rect r) {

			int count = 0;

			while (!q.isEmpty()) {
				// System.out.println(q.size());
				Point p = q.poll();
				if (p.x + 1 <= r.x + r.width && p.y <= r.y + r.height) {
					byte[] buffer = new byte[3];
					mask1.get((int) p.y, (int) p.x + 1, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_PR_FGD) {
						mask1.put((int) p.y, (int) p.x + 1, Imgproc.GC_BGD);
						q.add(new Point(p.x + 1, p.y));
					}
				}
				if (p.x <= r.x + r.width && p.y + 1 <= r.y + r.height) {
					byte[] buffer = new byte[3];
					mask1.get((int) p.y + 1, (int) p.x, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_PR_FGD) {
						mask1.put((int) p.y + 1, (int) p.x, Imgproc.GC_BGD);
						q.add(new Point(p.x, p.y + 1));
					}
				}
				if (p.x - 1 >= r.x && p.y <= r.y + r.height) {
					byte[] buffer = new byte[3];
					mask1.get((int) p.y, (int) p.x - 1, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_PR_FGD) {
						mask1.put((int) p.y, (int) p.x - 1, Imgproc.GC_BGD);
						q.add(new Point(p.x - 1, p.y));
					}
				}
				if (p.x <= r.x + r.width && p.y - 1 >= r.y) {
					byte[] buffer = new byte[3];
					mask1.get((int) p.y - 1, (int) p.x, buffer);
					int value = buffer[0];
					if (value == Imgproc.GC_PR_FGD) {
						mask1.put((int) p.y - 1, (int) p.x, Imgproc.GC_BGD);
						q.add(new Point(p.x, p.y - 1));
					}
				}
				count++;
			}
		}
		
		public Mat after_inpainting(Mat original_img, Rect rect, String img_name, String rootpath) {
	    	//resize img 
	    	Mat im = original_img.clone();
	    	Mat inpainted = Imgcodecs.imread(rootpath+"/inpainting/"+img_name);
	    	Imgproc.resize(inpainted, inpainted, im.size());
	    	Imgcodecs.imwrite(rootpath+"/inpainting/"+img_name, inpainted);
	    	
	    	Mat fg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
	    	im.copyTo(fg, mask1);
			Mat bg = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
			Core.subtract(im, fg, bg); //subtract
			inpainted.copyTo(bg, mask1);
			
	    	fg = fg.submat(rect);
			Mat src = fg.clone();
			Mat dst = new Mat();
			Mat alpha = mask1.clone();
			alpha = alpha.submat(rect);
			
			List<Mat> rgb = new ArrayList<>();
			Core.split(src, rgb);
			List<Mat> rgba = new ArrayList<>();
			rgba.add(rgb.get(0));
			rgba.add(rgb.get(1));
			rgba.add(rgb.get(2));
			rgba.add(alpha);
			Core.merge(rgba, dst);
			
			Mat result_roi = bg.submat(rect);
			dst.copyTo(result_roi);
			
			return bg;
	    }
	

}
