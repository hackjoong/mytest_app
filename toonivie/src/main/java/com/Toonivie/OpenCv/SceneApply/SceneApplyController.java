package com.Toonivie.OpenCv.SceneApply;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sceneApply")
public class SceneApplyController {
	@Autowired
	SceneApplyService sceneApplyService;

	@RequestMapping("/apply")
	@ResponseBody
	public HashMap<String, Object> apply(HttpServletRequest request) {
		String str = request.getParameter("data");
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONObject background = (JSONObject) jsonObject.get("background");
		JSONArray obj = (JSONArray) jsonObject.get("obj");
		String effectKind = jsonObject.get("effectKind").toString();
		if (effectKind.equals("t2")) {
			effectKind = "fadeOut";
		} else if (effectKind.equals("t1")) {
			effectKind = "fadeIn";
		} else if (effectKind.equals("6")) { // 5->6
			effectKind = "nakTemplate";
		} else if (effectKind.equals("10")) { // add by shkwak, 2017-07-05
			effectKind = "rainTemplate";
		} else if (effectKind.equals("5")) { // add by shkwak, 2017-07-13
			effectKind = "flowerTemplate";
		} else if (effectKind.equals("7")) {
			effectKind = "twinkleTemplate";
		} else if (effectKind.equals("8")) {
			effectKind = "speedTemplate";
		} else if (effectKind.equals("9")) {
			effectKind = "heartTemplate";
		} else if (effectKind.equals("3-1") || effectKind.equals("3-2") || effectKind.equals("3-3")
				|| effectKind.equals("3-4") || effectKind.equals("3-5") || effectKind.equals("3-6")|| effectKind.equals("3-7") || effectKind.equals("3-8")) { // add by shkwak, 2017-07-14, 08-20
					effectKind = effectKind;// "stateTemplate";
		} else if (effectKind.equals("t5")) {
			effectKind = "ink1Appear";
		} else if (effectKind.equals("t7")) {
			effectKind = "ink2Appear";
		} else if (effectKind.equals("t6")) {
			effectKind = "ink3Appear";
		} else if (effectKind.equals("t4")) {
			effectKind = "ink1Disappear";
		} else if (effectKind.equals("t8")) {
			effectKind = "leftToRight";
		} else if (effectKind.equals("t9")) {
			effectKind = "upToDown";
		} else if (effectKind.equals("t10")) {
			effectKind = "randomStick";
		} else if (effectKind.equals("t3")) {
			effectKind = "zizic";
		}
		int effectTime = Integer.parseInt(jsonObject.get("effectTime").toString());
		String rootPath = request.getServletContext().getRealPath("/");
		return sceneApplyService.apply(background, obj, effectKind, rootPath, effectTime, request);
	}

	@RequestMapping("/test")
	@ResponseBody
	public HashMap<String, Object> test(HttpServletRequest request) {
		String str = "{ \"effectKind\":\"1\",\"effectTime\":2,\"background\":{\"imgUrl\":\"http://localhost:8088/resources/cvimgs/backGround20170607114752.png\",\"top\":298.5,\"left\":475.75},\"obj\":[{\"top\":372.5,\"left\":1138.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607114745.png\"},{\"top\":419.5,\"left\":586.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607114752.png\"}]}";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONObject background = (JSONObject) jsonObject.get("background");
		JSONArray obj = (JSONArray) jsonObject.get("obj");
		String effectKind = jsonObject.get("effectKind").toString();
		if (effectKind.equals("1")) {
			effectKind = "fadeOut";
		}
		int effectTime = Integer.parseInt(jsonObject.get("effectTime").toString());
		String rootPath = request.getServletContext().getRealPath("/");
		return sceneApplyService.apply(background, obj, effectKind, rootPath, effectTime,request);
	}
}
