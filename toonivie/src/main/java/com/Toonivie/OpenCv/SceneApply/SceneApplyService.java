package com.Toonivie.OpenCv.SceneApply;

//import static org.mockito.Matchers.contains;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.ImageTypeConvert;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObjectVo;

@Service
public class SceneApplyService {
	@Autowired
	inkSceneUtil inkSceneUtil;
	final static int frame = 20; //FPS : 1�룯占� 占쎈짗占쎈툧 占쎌삺占쎄문占쎈┷占쎈뮉 占쎌뵠沃섎챷占쏙옙�벥 占쎈땾
	//static final int sizeWidth = 960;
	//static final int sizeHeight = 540;
	static HttpServletRequest request_url;

	public HashMap<String, Object> apply(JSONObject background, JSONArray obj, String effectKind, String rootPath, int effectTime, HttpServletRequest request) {
		request_url = request;
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		HashMap<String, Object> resultMap = new HashMap<>();
		MoveObjectVo backgroundVo = changeMoveBackgroundVo(background);
		List<MoveObjectVo> otherList = makeOtherList(obj, backgroundVo);
		
		if(effectKind.equals("fadeOut")||effectKind.equals("fadeIn")) {
			resultMap = fadeOut(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("nakTemplate")||effectKind.equals("flowerTemplate")||effectKind.equals("twinkleTemplate")||
				effectKind.equals("speedTemplate")||effectKind.equals("heartTemplate")){ //effectKind : 6,5,7,8,9
			resultMap = totalTemplate(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("rainTemplate")){ //effectKind : 10
			resultMap = rainTemplate(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("3-1")||effectKind.equals("3-2")||effectKind.equals("3-3")||effectKind.equals("3-4")||
				effectKind.equals("3-5")||effectKind.equals("3-7")){ //�뤃�됱カ,�눧�눊猿�,占쎈염疫뀐옙,占쎄묶占쎈��,占쎈：獄쏉옙,占쎈뻤�뜮占�
			resultMap = totalTemplate(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("3-6")){ //�룺諛� imsi
			resultMap = totalTemplate1(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("3-8")){ //遺� imsi
			resultMap = totalTemplate2(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("ink1Appear")||effectKind.equals("ink2Appear")||effectKind.equals("ink3Appear")||effectKind.equals("ink1Disappear")) {
			String staticMoveName = null;
			int isAppear=1;
			if(effectKind.equals("ink1Appear")) {
				staticMoveName="ink1.mp4";
			}else if(effectKind.equals("ink2Appear")) {
				staticMoveName="ink2.mp4";
			}else if(effectKind.equals("ink3Appear")) {
				staticMoveName="ink3.mp4";
			}else if(effectKind.equals("ink1Disappear")) {
				staticMoveName="ink1.mp4";
				isAppear=0;
			}
			resultMap = ink1Appear(backgroundVo,otherList,effectKind,rootPath,effectTime,staticMoveName,isAppear);
		}else if(effectKind.equals("leftToRight")) {
			resultMap=leftToRight(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("upToDown")) {
			resultMap=upToDown(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("randomStick")) {
			resultMap=randomStick(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}else if(effectKind.equals("zizic")) {
			resultMap=zizic(backgroundVo,otherList,effectKind,rootPath,effectTime);
		}
		else{ //imsi
		
			resultMap.put("videoUrl", "/resources/cvMov/_dumy.mp4");
			resultMap.put("effectTime", effectTime);
			resultMap.put("thumbNailUrl", "/resources/cvimgs/2imageRoi1.png"); //dumy
		}
		return resultMap;
	}
	
	private HashMap<String,Object> zizic(MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String,Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im, fore.clone());
		}
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		String resultFileName = "zizic"+today+".mp4";
				
		videoWriter.open(rootPath+"resources/cvMov/" + resultFileName, 
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		Mat thumbnail = new Mat();

		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im = imagetypeconvert.bgraTobgr(im);
		
		Mat black = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0,0,0));
		int kernelSize = 11;
		for(int i=0;i<frame*effectTime;i++) {
			Mat result = new Mat();
			int kernelSizeInLoop = kernelSize+i*2;
			Mat kernel = new Mat(kernelSizeInLoop, kernelSizeInLoop, CvType.CV_32F) {
				{
					for (int j = 0; j < kernelSizeInLoop; j++) {
						for (int k = 0; k < kernelSizeInLoop; k++) {
							if (j == kernelSizeInLoop / 2) {
								put(j, k, 1.0 / ((float) kernelSizeInLoop));
							} else {
								put(j, k, 0);
							}
						}
					}

				}
			};
			Imgproc.filter2D(im, result, -1, kernel);
			Core.addWeighted(result, (float)1 - (float) i / (float)(frame*effectTime), black, (float) i / (float)(frame*effectTime), 0., result);
			videoWriter.write(result);
			if(i==(frame*effectTime)/4) {
				thumbnail=result.clone();
			}
			
		}
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, thumbnail);
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + resultFileName);
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height);
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	private HashMap<Integer,String> makeZeroToTwentyHashMap(){
		HashMap<Integer,String> resultMap = new HashMap<>();
		for(int i=0;i<20;i++) {
			resultMap.put(i, "1");
		}
		return resultMap;
	}
	private HashMap<String,Object> randomStick (MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		System.out.println("leftToRight");
		HashMap<String,Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.113:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		Mat im1 = im.clone();		
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		String resultFileName = "randomStick"+today+".mp4";

		videoWriter.open(rootPath+"resources/cvMov/" + resultFileName, 
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		Mat thumbnail = new Mat();
		int colDivide20 = im.cols()/20;
		Mat smallStick = new Mat(im.rows(), colDivide20, CvType.CV_8UC4, new Scalar(0, 0, 0, 255));
		HashMap<Integer,String> integerList = makeZeroToTwentyHashMap();
		Random randomGenerator = new Random();
		Mat result = new Mat();
		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im1 = imagetypeconvert.bgraTobgr((im1));
		
		
		for(int i=1;i<=frame*effectTime;i++) {
			if(i==frame*effectTime) {
				result = new Mat(im.size(),CvType.CV_8UC3,new Scalar(0, 0, 0));
				videoWriter.write(result);
				break;
			}else if(i == 1){
				System.out.println(i);
				videoWriter.write(im);
			}else {
				if(i%effectTime==0) {
					
					for(int j=0;j<20;j++) {
						int a = randomGenerator.nextInt(20);
						if(integerList.get(a).toString()=="1") {
							integerList.put(a, "0");
							im1 = putObjOnBackground.setBackMat(a*colDivide20, 0, im1, smallStick);
							im1 = imagetypeconvert.bgraTobgr((im1));
							videoWriter.write(im1);
							break;
						}
					}
				}else {
					videoWriter.write(result);
				}
			}
			if(i==(frame*effectTime)/4) {
				thumbnail=result.clone();
			}
			
		}
		String thumbNailImgName = rootPath+"esources/cvimgs/thumbNail" + today + ".png";
		//유덕희 주석처리_180402
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
        Imgcodecs.imwrite(thumbNailImgName, thumbnail);
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + resultFileName);
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height);
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	public HashMap<String,Object> upToDown (MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		System.out.println("leftToRight");
		HashMap<String,Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.113:8088", "").replace("www."+request_url.getServerName()+".com","").replace("http://192.168.0.44:8088", "").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		Mat im1 = im.clone();
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		String resultFileName = "upToDown"+today+".mp4";

		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im1 = imagetypeconvert.bgraTobgr((im1));
		
		videoWriter.open(rootPath+"resources/cvMov/" + resultFileName, 
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		float widthPerLoop = ((float)im.height()/((float)frame*(float)effectTime));
		Mat thumbnail = new Mat();
		for(int i=1;i<=frame*effectTime;i++) {
			Mat result = new Mat();
			if(i == 1) {
				videoWriter.write(im);
				continue;
			}else if(i == (frame*effectTime)) {
				videoWriter.write(new Mat(im.size(),CvType.CV_8UC3,new Scalar(0, 0, 0)));
				break;
			}
			if((int)(widthPerLoop*(float)i)<1) {
				videoWriter.write(im1);
			}
			else {
				if(i==frame*effectTime) {
					System.out.println("lastFrame");
					Mat fade = new Mat(im.rows(),im.cols(),CvType.CV_8UC4,new Scalar(0,0,0,255));
					videoWriter.write(fade);
				}else {
					Mat fade = new Mat((int)(widthPerLoop*i),im.cols(),CvType.CV_8UC4,new Scalar(0,0,0,255));
					result = putObjOnBackground.setBackMat(0, 0, im.clone(), fade);
					result = imagetypeconvert.bgraTobgr((result));
					videoWriter.write(result);
				}
			}
			if(i==(frame*effectTime)/4) {
				thumbnail=result.clone();
			}
			
		}
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, thumbnail);
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + resultFileName);
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height);
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	public HashMap<String,Object> leftToRight (MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		System.out.println("leftToRight");
		HashMap<String,Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.113:8088", "").replace("www."+request_url.getServerName()+".com","").replace("http://192.168.0.44:8088", "").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		Mat im1 = im.clone();
		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		String resultFileName = "leftToRight"+today+".mp4";

		videoWriter.open(rootPath+"resources/cvMov/" + resultFileName, 
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		float widthPerLoop = ((float)im.width()/((float)frame*(float)effectTime));
		Mat thumbnail = new Mat();

		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im1 = imagetypeconvert.bgraTobgr((im1));
		
		for(int i=1;i<=frame*effectTime;i++) {
			Mat result = new Mat();
			
			if(i == 1) {
				videoWriter.write(im1);
				continue;
			}else if(i == (frame*effectTime)) {
				videoWriter.write(new Mat(im.size(),CvType.CV_8UC3,new Scalar(0, 0, 0)));
				break;
			}
			
			if((int)(widthPerLoop*(float)i)<1) {
				videoWriter.write(im1);
			}else {
				if(i==frame*effectTime) {
					System.out.println("lastFrame");
					Mat fade = new Mat(im.rows(),im.cols(),CvType.CV_8UC4,new Scalar(0,0,0,255));
					videoWriter.write(fade);
				}else {
					Mat fade = new Mat(im.rows(),(int)(widthPerLoop*i),CvType.CV_8UC4,new Scalar(0,0,0,255));
					result = putObjOnBackground.setBackMat(0, 0, im.clone(), fade);
					result = imagetypeconvert.bgraTobgr((result));
					videoWriter.write(result);
				}
			}
			if(i==(frame*effectTime)/4) {
				thumbnail=result.clone();
			}
			
		}
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, thumbnail);
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + resultFileName);
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height);
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	public HashMap<String, Object> ink1Appear(MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime,String staticMoveName,int isAppear){

		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}

		VideoWriter videoWriter = new VideoWriter();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		String resultFileName = "ink"+today+".mp4";

		videoWriter.open(rootPath+"resources/cvMov/" + resultFileName, 
				VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		inkSceneUtil inkSceneUtil = new inkSceneUtil();
		HashMap<String,Object> resultMap = inkSceneUtil.setEffectTime(effectTime).setFps(frame).setIm(im).setStaticMov(staticMoveName).setRootPath(rootPath).setVideoWriter(videoWriter).applyInk(resultFileName,isAppear);
		
	
		return resultMap;
	}
	private MoveObjectVo changeMoveBackgroundVo(JSONObject obj) {
		MoveObjectVo moveObjectVo = new MoveObjectVo();
		String imgUrl = obj.get("imgUrl").toString();
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		float top = d3.floatValue();
		float left = d4.floatValue();
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		return moveObjectVo;
	}

	private List<MoveObjectVo> makeOtherList(JSONArray others, MoveObjectVo backgroundVo) {
		List<MoveObjectVo> moveObjectVos = new ArrayList<>();
		for (int i = 0; i < others.size(); i++) {
			JSONObject object = (JSONObject) others.get(i);
			String imgUrl = object.get("imgUrl").toString();
			Double d3 = new Double(object.get("top").toString());
			Double d4 = new Double(object.get("left").toString());
			float top = d3.floatValue();
			float left = d4.floatValue();

			MoveObjectVo vo = new MoveObjectVo();
			vo.setImgUrl(imgUrl);
			vo.setTop(top - backgroundVo.getTop());
			vo.setLeft(left - backgroundVo.getLeft());
			vo.setHeight((new Double(object.get("height").toString())).floatValue());
			vo.setWidth((new Double(object.get("width").toString())).floatValue());
			moveObjectVos.add(vo);
		}
		return moveObjectVos;
	}

	public HashMap<String, Object> totalTemplate(MoveObjectVo background, List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED); 
		
		//object insert to background
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			im = putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		
		Mat im1 = im.clone(); //back image clone
        Mat im2 = im.clone(); 
        
        String tempImgName = "";
        switch(effectKind){ //add by shkwak, 2017-07-13
        	case "flowerTemplate" : tempImgName = "flower"; break;
        	case "nakTemplate" : tempImgName = "nak"; break; //占쎈꽰占쎈�
        	case "twinkleTemplate" : tempImgName = "twinkle"; break;
        	case "speedTemplate" : tempImgName = "speed"; break;
        	case "heartTemplate" : tempImgName = "heart"; break;
        	case "3-1" : tempImgName = "cloud"; break; //add, 2017-07-14
        	case "3-2" : tempImgName = "wave"; break;
        	case "3-3" : tempImgName = "smoke"; break;
        	case "3-4" : tempImgName = "typhoon"; break;
        	case "3-5" : tempImgName = "explosion"; break;
        	case "3-7" : tempImgName = "sunlight"; break;
        	default : tempImgName = "flower"; break;
        }        
        
        Mat tempPng1 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "1.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "2.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1, tempPng1, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2, tempPng2, new Size(im.cols(),im.rows()));
        
        Mat imageRoi1 = new Mat(im1, new Rect(0, 0, tempPng1.cols(), tempPng1.rows())); //ROI 占쎌겫占쎈열 占쎌젟占쎌벥
        Mat imageRoi2 = new Mat(im2, new Rect(0, 0, tempPng2.cols(), tempPng2.rows()));

        Mat tempPng1mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "1.png", Imgcodecs.IMREAD_UNCHANGED); //IMREAD_UNCHANGED(-1)
        Mat tempPng2mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "2.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1mask, tempPng1mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2mask, tempPng2mask, new Size(im.cols(),im.rows()));
        
        List<Mat> mats1 = new ArrayList<Mat>();
        Mat nak1 = tempPng1mask.clone(); //tempPng1
		Core.split(nak1, mats1); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask1 = mats1.get(mats1.size()-1); //筌띾뜆�뮞占쎄쾿 占쎄퐬占쎌젟
		List<Mat> mats2 = new ArrayList<Mat>();
        Mat nak2 = tempPng2mask.clone(); //tempPng2
		Core.split(nak2, mats2);
		Mat mask2 = mats2.get(mats2.size()-1); //0(R), 1(G), 2(B), 3(A)

		tempPng1.copyTo(imageRoi1, mask1); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng2.copyTo(imageRoi2, mask2);

        ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
        im1 = imagetypeconvert.bgraTobgr((im1));
        im2 = imagetypeconvert.bgraTobgr((im2));
        
        CreateFileUtils createFileUtils  = new CreateFileUtils();
        String today = createFileUtils.getToday(1);
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath + "/resources/cvMov/" + tempImgName + today + ".mp4");
        if(file.exists()){
        	file.delete();
        }

        String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, im1); //imageRoi1        

        videoWriter.open(rootPath+"resources/cvMov/" + tempImgName + today + ".mp4", 
        		VideoWriter.fourcc('H', '2', '6', '4'), frame, im1.size());
        for (int i = 0; i < frame*effectTime; i++) {
        	if(i%6>=3){
        		videoWriter.write(im1);        		
        	}else{
        		videoWriter.write(im2);        		
        	}       	
        }
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + tempImgName + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im1.size().width);
		return resultMap;
	}
	
	//effectKind : 10, 占쎄틕占쎈탣�뵳占� - �뜮占�
	public HashMap<String, Object> rainTemplate(MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED); 
		
		//object insert to background
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}

		
		Mat im1 = im.clone(); 
        Mat im2 = im.clone(); 
        
        Mat nakPng1 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_1.jpg", Imgcodecs.IMREAD_UNCHANGED);
        Mat nakPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_2.jpg", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(nakPng1, nakPng1, new Size(im.cols(),im.rows()));
        Imgproc.resize(nakPng2, nakPng2, new Size(im.cols(),im.rows()));
        
        Mat imageRoi1 = new Mat(im1,new Rect(0, 0, nakPng1.cols(), nakPng1.rows()));
        Mat imageRoi2 = new Mat(im2,new Rect(0, 0, nakPng2.cols(), nakPng2.rows()));

        ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
        imageRoi1 = imagetypeconvert.bgraTobgr((imageRoi1));
        imageRoi2 = imagetypeconvert.bgraTobgr((imageRoi2));
				
        Core.addWeighted(imageRoi1, 1.0, nakPng1, 1, 0.0, imageRoi1);
        Core.addWeighted(imageRoi2, 1.0, nakPng2, 1, 0.0, imageRoi2);
        
        
        CreateFileUtils createFileUtils  = new CreateFileUtils();
        String today = createFileUtils.getToday(1);
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath + "/resources/cvMov/rain" + today + ".mp4");
        if(file.exists()){
        	file.delete();
        }
        String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, imageRoi1);

        videoWriter.open(rootPath+"resources/cvMov/rain" + today + ".mp4", 
        		VideoWriter.fourcc('H', '2', '6', '4'), frame, imageRoi1.size());
        for (int i = 0; i < frame*effectTime; i++) { 
        	if(i%6>=3){ 
        		videoWriter.write(imageRoi1);        		
        	}else{
        		videoWriter.write(imageRoi2);        		
        	}       	
        }
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/rain" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im1.size().width);
		return resultMap;
	}
	
	public HashMap<String, Object> fadeOut(MoveObjectVo background,List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		
		Mat black = new Mat(im.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/fadeOut" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im = imagetypeconvert.bgraTobgr(im);
		
		videoWriter.open(rootPath+"resources/cvMov/fadeOut" + today + ".mp4", VideoWriter.fourcc('H', '2', '6', '4'), frame, im.size());
		Mat thumbNail = null;
		int totalFrame = frame * effectTime;
		for (int i = 0; i < totalFrame; i++) {
			Mat result = new Mat();
			if(effectKind.equals("fadeOut")){
				Core.addWeighted(im, 1 - (float) i / totalFrame, black, (float) i / totalFrame, 0., result);
			}else{
				Core.addWeighted(im, (float) i / totalFrame, black, 1-(float) i / totalFrame, 0., result);
			}
			if (totalFrame / 2 == i) {
				thumbNail = result.clone();
			}

			videoWriter.write(result);
		}

		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		resultMap.put("videoUrl", "/resources/cvMov/fadeOut" + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	public HashMap<String, Object> totalTemplate1(MoveObjectVo background, List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		
		Mat im1 = im.clone(); //back image clone
        Mat im2 = im.clone(); 
        Mat im3 = im.clone(); //back image clone
        Mat im4 = im.clone();        
        
        String tempImgName = "";        
        switch(effectKind){ //add by shkwak, 2017-07-13        	
        	case "3-6" : tempImgName = "explosion"; break;
        	default : tempImgName = "explosion"; break;
        }        
        Mat tempPng1 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "01.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "02.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1, tempPng1, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2, tempPng2, new Size(im.cols(),im.rows()));
        Mat tempPng3 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "03.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng4 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "04.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng3, tempPng3, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng4, tempPng4, new Size(im.cols(),im.rows()));
           
        Mat imageRoi1 = new Mat(im1, new Rect(0, 0, tempPng1.cols(), tempPng1.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi2 = new Mat(im2, new Rect(0, 0, tempPng2.cols(), tempPng2.rows()));
        Mat imageRoi3 = new Mat(im3, new Rect(0, 0, tempPng3.cols(), tempPng3.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi4 = new Mat(im4, new Rect(0, 0, tempPng4.cols(), tempPng4.rows()));
   
        //�씠誘몄� 留덉뒪�겕 �뵲濡� read 
        Mat tempPng1mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "01.png", Imgcodecs.IMREAD_UNCHANGED); //IMREAD_UNCHANGED(-1)
        Mat tempPng2mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "02.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1mask, tempPng1mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2mask, tempPng2mask, new Size(im.cols(),im.rows()));
        Mat tempPng3mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "03.png", Imgcodecs.IMREAD_UNCHANGED); //IMREAD_UNCHANGED(-1)
        Mat tempPng4mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "04.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng3mask, tempPng3mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng4mask, tempPng4mask, new Size(im.cols(),im.rows()));
        
        List<Mat> mats1 = new ArrayList<Mat>();
        Mat nak1 = tempPng1mask.clone(); //tempPng1
		Core.split(nak1, mats1); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask1 = mats1.get(3); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats2 = new ArrayList<Mat>();
        Mat nak2 = tempPng2mask.clone(); //tempPng2
		Core.split(nak2, mats2);
		Mat mask2 = mats2.get(3); //0(R), 1(G), 2(B), 3(A)
		
		List<Mat> mats3 = new ArrayList<Mat>();
        Mat nak3 = tempPng3mask.clone(); //tempPng1
		Core.split(nak3, mats3); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask3 = mats3.get(3); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats4 = new ArrayList<Mat>();
        Mat nak4 = tempPng4mask.clone(); //tempPng2
		Core.split(nak4, mats4);
		Mat mask4 = mats4.get(3);		

		tempPng1.copyTo(imageRoi1, mask1); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng2.copyTo(imageRoi2, mask2);
        tempPng3.copyTo(imageRoi3, mask3); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng4.copyTo(imageRoi4, mask4);

        ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im1 = imagetypeconvert.bgraTobgr((im1));
		im2 = imagetypeconvert.bgraTobgr((im2));
		im3 = imagetypeconvert.bgraTobgr((im3));
		im4 = imagetypeconvert.bgraTobgr((im4));
		
        CreateFileUtils createFileUtils  = new CreateFileUtils();
        String today = createFileUtils.getToday(1);
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath + "/resources/cvMov/" + tempImgName + today + ".mp4");
        if(file.exists()){
        	file.delete();
        }

        String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, im1);

        videoWriter.open(rootPath+"resources/cvMov/" + tempImgName + today + ".mp4", 
        		VideoWriter.fourcc('H', '2', '6', '4'), frame, im1.size());
        for (int i = 0; i < frame*effectTime; i++) { //frame*effectTime, ex) 20*2 = 40
        	if(i%16<=3){ //% : �굹癒몄�, 湲곗〈 if(i%20>=10)
        		videoWriter.write(im1);        		
        	}else if(i%16>3 && i%16<=7){
        		videoWriter.write(im2);
        	}else if(i%16>7 && i%16<=11){
        		videoWriter.write(im3);
        	}else{        		
        		videoWriter.write(im4);
        	}
        }
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + tempImgName + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im1.size().width);
		return resultMap;
	}
	public HashMap<String, Object> totalTemplate2(MoveObjectVo background, List<MoveObjectVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
		
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im,fore.clone());
		}
		Mat im1 = im.clone(); //back image clone
        Mat im2 = im.clone(); 
        Mat im3 = im.clone(); //back image clone
        Mat im4 = im.clone();
        Mat im5 = im.clone(); //back image clone
        Mat im6 = im.clone(); 
        Mat im7 = im.clone(); //back image clone
        Mat im8 = im.clone();
        
        String tempImgName = "";        
        switch(effectKind){ //add by shkwak, 2017-07-13        	
        	case "3-8" : tempImgName = "fire"; break;
        	default : tempImgName = "fire"; break;
        }        
        
        Mat tempPng1 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "01.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "02.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1, tempPng1, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2, tempPng2, new Size(im.cols(),im.rows()));
        Mat tempPng3 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "03.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng4 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "04.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng3, tempPng3, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng4, tempPng4, new Size(im.cols(),im.rows()));
        Mat tempPng5 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "05.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng6 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "06.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng5, tempPng5, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng6, tempPng6, new Size(im.cols(),im.rows()));
        Mat tempPng7 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "07.png", Imgcodecs.IMREAD_UNCHANGED); 
        Mat tempPng8 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "08.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng7, tempPng7, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng8, tempPng8, new Size(im.cols(),im.rows()));
        
        Mat imageRoi1 = new Mat(im1, new Rect(0, 0, tempPng1.cols(), tempPng1.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi2 = new Mat(im2, new Rect(0, 0, tempPng2.cols(), tempPng2.rows()));
        Mat imageRoi3 = new Mat(im3, new Rect(0, 0, tempPng3.cols(), tempPng3.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi4 = new Mat(im4, new Rect(0, 0, tempPng4.cols(), tempPng4.rows()));
        Mat imageRoi5 = new Mat(im5, new Rect(0, 0, tempPng5.cols(), tempPng5.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi6 = new Mat(im6, new Rect(0, 0, tempPng6.cols(), tempPng6.rows()));
        Mat imageRoi7 = new Mat(im7, new Rect(0, 0, tempPng7.cols(), tempPng7.rows())); //ROI �쁺�뿭 �젙�쓽
        Mat imageRoi8 = new Mat(im8, new Rect(0, 0, tempPng8.cols(), tempPng8.rows()));
        
        
        //�씠誘몄� 留덉뒪�겕 �뵲濡� read 
        Mat tempPng1mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "01.png", Imgcodecs.IMREAD_UNCHANGED);
        Mat tempPng2mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "02.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng1mask, tempPng1mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng2mask, tempPng2mask, new Size(im.cols(),im.rows()));
        Mat tempPng3mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "03.png", Imgcodecs.IMREAD_UNCHANGED);
        Mat tempPng4mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "04.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng3mask, tempPng3mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng4mask, tempPng4mask, new Size(im.cols(),im.rows()));
        Mat tempPng5mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "05.png", Imgcodecs.IMREAD_UNCHANGED);
        Mat tempPng6mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "06.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng5mask, tempPng5mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng6mask, tempPng6mask, new Size(im.cols(),im.rows()));
        Mat tempPng7mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "07.png", Imgcodecs.IMREAD_UNCHANGED);
        Mat tempPng8mask = Imgcodecs.imread(rootPath+"resources/cvStaticImg/" + tempImgName + "08.png", Imgcodecs.IMREAD_UNCHANGED);
        Imgproc.resize(tempPng7mask, tempPng7mask, new Size(im.cols(),im.rows()));
        Imgproc.resize(tempPng8mask, tempPng8mask, new Size(im.cols(),im.rows()));
        
        List<Mat> mats1 = new ArrayList<Mat>();
        Mat nak1 = tempPng1mask.clone(); //tempPng1
		Core.split(nak1, mats1); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask1 = mats1.get(mats1.size()-1); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats2 = new ArrayList<Mat>();
        Mat nak2 = tempPng2mask.clone(); //tempPng2
		Core.split(nak2, mats2);
		Mat mask2 = mats2.get(mats2.size()-1); //0(R), 1(G), 2(B), 3(A)
		
		List<Mat> mats3 = new ArrayList<Mat>();
        Mat nak3 = tempPng3mask.clone(); //tempPng1
		Core.split(nak3, mats3); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask3 = mats3.get(mats3.size()-1); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats4 = new ArrayList<Mat>();
        Mat nak4 = tempPng4mask.clone(); //tempPng2
		Core.split(nak4, mats4);
		Mat mask4 = mats4.get(mats4.size()-1);		
		
		List<Mat> mats5 = new ArrayList<Mat>();
        Mat nak5 = tempPng5mask.clone(); //tempPng1
		Core.split(nak5, mats5); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask5 = mats5.get(mats5.size()-1); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats6 = new ArrayList<Mat>();
        Mat nak6 = tempPng6mask.clone(); //tempPng2
		Core.split(nak6, mats6);
		Mat mask6 = mats6.get(mats6.size()-1); //0(R), 1(G), 2(B), 3(A)
		
		List<Mat> mats7 = new ArrayList<Mat>();
        Mat nak7 = tempPng7mask.clone(); //tempPng1
		Core.split(nak7, mats7); //split(InputArray m, OutputArrayOfArrays mv)
		Mat mask7 = mats7.get(mats7.size()-1); //留덉뒪�겕 �꽕�젙
		
		List<Mat> mats8 = new ArrayList<Mat>();
        Mat nak8 = tempPng8mask.clone(); //tempPng2
		Core.split(nak8, mats8);
		Mat mask8 = mats8.get(mats8.size()-1);
			
		tempPng1.copyTo(imageRoi1, mask1); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng2.copyTo(imageRoi2, mask2);
        tempPng3.copyTo(imageRoi3, mask3); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng4.copyTo(imageRoi4, mask4);
        tempPng5.copyTo(imageRoi5, mask5); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng6.copyTo(imageRoi6, mask6);
        tempPng7.copyTo(imageRoi7, mask7); //Mat::copyTo(OutputArray m, InputArray mask)
        tempPng8.copyTo(imageRoi8, mask8);

        ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
        im1 = imagetypeconvert.bgraTobgr((im1));
        im2 = imagetypeconvert.bgraTobgr((im2));
        im3 = imagetypeconvert.bgraTobgr((im3));
        im4 = imagetypeconvert.bgraTobgr((im4));
        im5 = imagetypeconvert.bgraTobgr((im5));
        im6 = imagetypeconvert.bgraTobgr((im6));
        im7 = imagetypeconvert.bgraTobgr((im7));
        im8 = imagetypeconvert.bgraTobgr((im8));
        
        CreateFileUtils createFileUtils  = new CreateFileUtils();
        String today = createFileUtils.getToday(1);
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath + "/resources/cvMov/" + tempImgName + today + ".mp4");
        if(file.exists()){
        	file.delete();
        }
        String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
        Imgcodecs.imwrite(thumbNailImgName, im1);
        videoWriter.open(rootPath+"resources/cvMov/" + tempImgName + today + ".mp4", 
        		VideoWriter.fourcc('H', '2', '6', '4'), frame, im1.size());
        for (int i = 0; i < frame*effectTime; i++) { //frame*effectTime, ex) 20*2 = 40
        	if(i%16<=1){
        		videoWriter.write(im1);        		
        	}else if(i%16>1 && i%16<=3){
        		videoWriter.write(im2);
        	}else if(i%16>3 && i%16<=5){
        		videoWriter.write(im3);
        	}else if(i%16>5 && i%16<=7){
        		videoWriter.write(im4);
        	}else if(i%16>7 && i%16<=9){
        		videoWriter.write(im5);
        	}else if(i%16>9 && i%16<=11){
        		videoWriter.write(im6);
        	}else if(i%16>11 && i%16<=13){
        		videoWriter.write(im7);
        	}else{        		
        		videoWriter.write(im8);
        	}
        }
        videoWriter.release();
        resultMap.put("videoUrl", "/resources/cvMov/" + tempImgName + today + ".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im1.size().width);
		return resultMap;
	}
}