package com.Toonivie.OpenCv.SceneApply;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.ImageTypeConvert;


@Service
public class inkSceneUtil { 
	VideoWriter videoWriter;
	String staticMov;
	int fps;
	Mat im;
	String rootPath;
	int effectTime;
	public inkSceneUtil setEffectTime(int effectTime) {
		this.effectTime=effectTime;
		return this;
	}
	public inkSceneUtil setRootPath(String rootPath) {
		this.rootPath=rootPath;
		return this;
	}
	public inkSceneUtil setIm(Mat im) {
		this.im = im;
		return this;
	}
	public inkSceneUtil setVideoWriter(VideoWriter videoWriter) {
		this.videoWriter = videoWriter;
		return this;
	}
	public inkSceneUtil setStaticMov(String staticMoveName) {
		this.staticMov=staticMoveName;
		return this;
	}
	public inkSceneUtil setFps(int fps) {
		this.fps=fps;
		return this;
	}

	
	private double validateMinus(double a,double b) {
		if(a-b<0) {
			return 0;
		}
		return a-b;
	}
	private static double validatePlus(double a,double b) {
		if(a+b>255) {
			return 255;
		}
		return a+b;
	}
	private void setImage(Mat image,Mat im,int rows,int cols,double b,double g, double r,int isAppear) {
		if(isAppear==1) {
			b=255-b;
			g=255-g;
			r=255-r;	
			double[] pixel=im.get(rows, cols);
			pixel[0]=validateMinus(pixel[0],b);
			pixel[1]=validateMinus(pixel[1],g);
			pixel[2]=validateMinus(pixel[2],r);
		
		image.put(rows, cols, pixel);
		}else if(isAppear==0) {
			b=255+b;
			g=255+g;
			r=255+r;
			double[] pixel=im.get(rows, cols);
			pixel[0]=validatePlus(pixel[0],b);
			pixel[1]=validatePlus(pixel[1],g);
			pixel[2]=validatePlus(pixel[2],r);
		
		image.put(rows, cols, pixel);
		}
		

	}
	public HashMap<String,Object> applyInk(String resultFileName,int isAppear) {
		CreateFileUtils createFileUtils = new CreateFileUtils();
		long startTime = System.currentTimeMillis();
		VideoCapture vc = new VideoCapture(rootPath+"/resources/cvStaticMov/"+staticMov);
		VideoCapture vc2 = new VideoCapture(rootPath+"/resources/cvStaticMov/"+staticMov);
		File file = new File(rootPath + "/resources/cvMov/" + resultFileName);
		if(file.exists()) {
			file.delete();
		}
		Mat thumbNail = new Mat();
		Mat image = new Mat();
		Mat beforeImage = new Mat();
		int sum=0;
		int k=0;
		double totalFrame = vc.get(Videoio.CAP_PROP_FRAME_COUNT);
		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im = imagetypeconvert.bgraTobgr(im);
		runInk2 r = new runInk2(im.clone(), 0, (int)(totalFrame/2.0-1.0), vc, isAppear);
		runInk2 r2 = new runInk2(im.clone(), (int)(totalFrame/2.0), (int)(totalFrame), vc2, isAppear);
		r.start();
		r2.start();
		try {
			r.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			r2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Mat> rList = r.getResult();
		List<Mat> r2List = r2.getResult();
		for(int i=0;i<rList.size();i++) {
			videoWriter.write(rList.get(i));
			if(rList.size()/2 == i) {
				thumbNail=rList.get(i).clone();
			}
		}
		for(int i=0;i<r2List.size();i++) {
			videoWriter.write(r2List.get(i));
		}
		videoWriter.release();
		
		CreateFileUtils createFileUtils2 = new CreateFileUtils();
		String today = createFileUtils2.getToday(1);
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		videoWriter.release();
		
		HashMap<String,Object> resultMap = new HashMap<>();
		resultMap.put("videoUrl", "/resources/cvMov/" + resultFileName);
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im.size().width);
		long endTime = System.currentTimeMillis();
		//System.out.println("##  소요시간(초.0f) : " + ( endTime - startTime )/1000.0f +"초"); 
		return resultMap;
	}
}
class runInk2 extends Thread{
	Mat im;
	int stF;
	int eF;
	List<Mat> matList;
	VideoCapture vc;
	int isAppear;
	private static double validateMinus(double a,double b) {
		if(a-b<0) {
			return 0;
		}
		return a-b;
	}
	public List<Mat> getResult(){
		return matList;
	}
	private static double validatePlus(double a,double b) {
		if(a+b>255) {
			return 255;
		}
		return a+b;
	}
	private static void setImage(Mat image,Mat im,int rows,int cols,double b,double g, double r, int isAppear) {

		if(isAppear==1) {
			b=255-b;
			g=255-g;
			r=255-r;	
			double[] pixel=im.get(rows, cols);
			pixel[0]=validateMinus(pixel[0],b);
			pixel[1]=validateMinus(pixel[1],g);
			pixel[2]=validateMinus(pixel[2],r);
		
		image.put(rows, cols, pixel);
		}else if(isAppear==0) {

			double[] pixel=im.get(rows, cols);
			pixel[0]=validatePlus(pixel[0],b);
			pixel[1]=validatePlus(pixel[1],g);
			pixel[2]=validatePlus(pixel[2],r);
		
		image.put(rows, cols, pixel);
		}
	}
	public runInk2(Mat im,int stF,int eF,VideoCapture vc, int isAppear) {
		this.im=im;
		this.vc = vc;
		this.stF=stF;
		this.eF=eF;
		this.isAppear = isAppear;
		this.vc.set(Videoio.CAP_PROP_POS_FRAMES, stF);
		matList = new ArrayList<>();
		
	}
	public void run() {
		Mat image = new Mat();
		int count=0;
		while(vc.read(image)){
			Imgproc.resize(image, image, im.size());
			for(int i=0;i<im.cols();i++) {
				for(int j=0;j<im.rows();j++) {
					double[] putPixel = {255,255,255};
					putPixel[0]=image.get(j, i)[0];
					putPixel[1]=image.get(j, i)[1];
					putPixel[2]=image.get(j, i)[2];
					setImage(image,im,j,i,putPixel[0],putPixel[1],putPixel[2], isAppear);
				}
			}
			matList.add(image.clone());
			if(count==(int)eF) {
				break;
			}
			count++;
		}
	}
}