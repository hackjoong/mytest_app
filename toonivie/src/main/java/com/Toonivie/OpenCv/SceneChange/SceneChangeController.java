package com.Toonivie.OpenCv.SceneChange;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/sceneChange")
public class SceneChangeController {
	@Autowired
	SceneChangeService sceneChangeService;

	@RequestMapping(value = "/desolveTest")
	public String desolveTest(HttpServletRequest request) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		String rootPath = request.getSession().getServletContext().getRealPath("/");

		String video1 = rootPath + "resources/cvMov/video1.mov";
		String video2 = rootPath + "resources/cvMov/video2.mov";

		VideoCapture vc1 = new VideoCapture(video1);
		VideoCapture vc2 = new VideoCapture(video2);

		sceneChangeService.desolve(vc1, vc2, rootPath);
		return "aa";
	}

	@RequestMapping("/makeDump")
	public String makeDump(HttpServletRequest request) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		Mat im = Imgcodecs.imread(rootPath + "resources/cvimgs/20170524205349.png");
		Imgproc.resize(im, im, new Size(690,856));
		
		File file = new File(rootPath + "resources/cvMov/dump.avi");
		if (file.exists()) {
			file.delete();
		}
		VideoWriter videoWriter = new VideoWriter();
		videoWriter.open("src/main/webapp/resources/cvMov/dump.avi",
				VideoWriter.fourcc('D', 'I', 'V', '3'), 8,
				new Size(690,856));
		for(int i=0;i<80;i++){
			videoWriter.write(im);
		}
		videoWriter.release();
		return "aa";
	}
	@RequestMapping("/makeDesolveImage")
	public String makeDesolveImage(HttpServletRequest request){
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		Mat im1 = Imgcodecs.imread(rootPath + "resources/cvimgs/20170524205349.png");
		Mat im2 = Imgcodecs.imread(rootPath + "resources/cvimgs/20170524225310.png");
		Mat im3 = new Mat(new Size(690,856),CvType.CV_8UC3);
		Imgproc.resize(im1, im1, new Size(690,856));
		Imgproc.resize(im2, im2, new Size(690,856));
		File file = new File(rootPath + "resources/cvMov/dump.avi");
		if (file.exists()) {
			file.delete();
		}
		VideoWriter videoWriter = new VideoWriter();
		videoWriter.open("src/main/webapp/resources/cvMov/dump.avi",VideoWriter.fourcc('D', 'I', 'V', '3'), 8,new Size(690,856));
		for(int k=0;k<8;k++){
			for (int i = 0; i < im1.rows(); i++) {
				for (int j = 0; j < im1.cols(); j++) {
					double[] image1Pixel = im1.get(i, j);
					double[] image2Pixel = im2.get(i, j);
				
					double[] putPixel = { 255, 255, 255 };
					putPixel[0] = (image1Pixel[0]*(8-k)/8 + image2Pixel[0]*k/8);
					putPixel[1] = (image1Pixel[1]*(8-k)/8 + image2Pixel[1]*k/8);
					putPixel[2] = (image1Pixel[2]*(8-k)/8 + image2Pixel[2]*k/8);
				
					im3.put(i, j, putPixel);
				}
			}
			videoWriter.write(im3);
		}
		videoWriter.release();
		return "aa";
	}
}
