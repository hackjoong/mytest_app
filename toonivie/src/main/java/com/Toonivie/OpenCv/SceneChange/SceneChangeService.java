package com.Toonivie.OpenCv.SceneChange;

import java.io.File;
import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;

@Service
public class SceneChangeService {

	public HashMap<String, Object> desolve(VideoCapture vc1, VideoCapture vc2, String rootPath) {
		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		int vc1Length = (int) vc1.get(Videoio.CAP_PROP_FRAME_COUNT);
		int vc2Length = (int) vc2.get(Videoio.CAP_PROP_FRAME_COUNT);

		int vc1Fps = (int) vc1.get(Videoio.CAP_PROP_FPS);
		int vc2Fps = (int) vc2.get(Videoio.CAP_PROP_FPS);

		// vc1.set(Videoio.CAP_PROP_POS_FRAMES, vc1Length - 1 - vc1Fps);

		Mat image1 = new Mat();
		Mat image2 = new Mat();

		CreateFileUtils cfu = new CreateFileUtils();
		String today = cfu.getToday(1);

		VideoWriter videoWriter = new VideoWriter();

		File file = new File(rootPath + "resources/cvMov/desolve" + today + ".mp4");
		if (file.exists()) {
			file.delete();
		}

		/*videoWriter.open("src/main/webapp/resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('D', 'I', 'V', '3'), vc1Fps,
				new Size((int) vc1.get(Videoio.CAP_PROP_FRAME_WIDTH), (int) vc1.get(Videoio.CAP_PROP_FRAME_HEIGHT)));*/
		//유덕희 경로 주석_180402
		videoWriter.open(rootPath+"resources/cvMov/move" + today + ".mp4",
				VideoWriter.fourcc('D', 'I', 'V', '3'), vc1Fps,
				new Size((int) vc1.get(Videoio.CAP_PROP_FRAME_WIDTH), (int) vc1.get(Videoio.CAP_PROP_FRAME_HEIGHT)));

		System.out.println(
				(int) vc1.get(Videoio.CAP_PROP_FRAME_WIDTH) + "," + (int) vc1.get(Videoio.CAP_PROP_FRAME_HEIGHT));
		System.out.println(
				(int) vc2.get(Videoio.CAP_PROP_FRAME_WIDTH) + "," + (int) vc2.get(Videoio.CAP_PROP_FRAME_HEIGHT));

		int count = vc1Fps * 2;
		int countAll = 0;
		while (vc1.read(image1)) {
			if (countAll == vc1Length - vc1Fps * 2) {
				vc2.read(image2);
				for (int i = 0; i < image1.rows(); i++) {
					for (int j = 0; j < image1.cols(); j++) {
						double[] image1Pixel = image1.get(i, j);
						double[] image2Pixel = image2.get(i, j);

						double[] putPixel = { 255, 255, 255 };
						putPixel[0] = (image1Pixel[0] * count / vc1Fps * 2
								+ image2Pixel[0] * (vc1Fps * 2 - count) / vc1Fps * 2);
						putPixel[1] = (image1Pixel[1] * count / vc1Fps * 2
								+ image2Pixel[1] * (vc1Fps * 2 - count) / vc1Fps * 2);
						putPixel[2] = (image1Pixel[2] * count / vc1Fps * 2
								+ image2Pixel[2] * (vc1Fps * 2 - count) / vc1Fps * 2);

						image1.put(i, j, putPixel);
					}
				}
				if (count >= 0)
					count--;
			}
			videoWriter.write(image1);
			countAll++;
		}
		while (vc2.read(image2)) {
			videoWriter.write(image2);
		}
		videoWriter.release();
		return resultMap;
	}
}
