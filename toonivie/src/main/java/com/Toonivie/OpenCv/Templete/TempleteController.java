package com.Toonivie.OpenCv.Templete;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Util.CreateFileUtils;

@Controller
@RequestMapping("/templete")
public class TempleteController {
	@Autowired
	TempleteService templeteService;
	
	@RequestMapping(value="/applyTemplete",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> applyTemplete(HttpServletRequest request){
		String imgUrl = request.getParameter("imgUrl");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		return templeteService.applyTemplete(imgUrl,rootPath);
	}
	
	@RequestMapping(value="/applyVibrate",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> applyVibrate(HttpServletRequest request){
		String imgUrl = request.getParameter("imgUrl");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		return templeteService.applyVibrate(imgUrl,rootPath);
	}
	
	@RequestMapping(value = "/uploadCustomTemplete", method = RequestMethod.POST)
	@ResponseBody
    public HashMap<String,String> testFile(MultipartHttpServletRequest req,HttpSession session, HttpServletRequest request) throws Exception {
    	HashMap<String,String> resultMap = new HashMap<>();
    	MultipartFile file1 = req.getFile("templete1");
    	MultipartFile file2 = req.getFile("templete2");
    	CreateFileUtils cfu = new CreateFileUtils();
    	String filename1="templete1"+cfu.getToday(1)+"."+cfu.getFileType(file1.getOriginalFilename());
    	String filename2="templete2"+cfu.getToday(1)+"."+cfu.getFileType(file2.getOriginalFilename());
    	cfu.CreateFile(file1, request, "/resources/cvimgs/", filename1);
    	cfu.CreateFile(file2, request, "/resources/cvimgs/", filename2);
    	resultMap.put("templete1",filename1);
    	resultMap.put("templete2",filename2);
    	return resultMap;
    }
	@RequestMapping(value="/applyCustomTemplete",method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> applyCustomTemplete(HttpServletRequest request){
		String imgUrl = request.getParameter("imgUrl");
		String templete1 = request.getParameter("templete1");
		String templete2 = request.getParameter("templete2");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		if(CreateFileUtils.getFileType(imgUrl).equals("avi")||CreateFileUtils.getFileType(imgUrl).equals("mov")){
			VideoCapture vc = new VideoCapture(rootPath+"resources/cvimgs/"+imgUrl);
			return templeteService.applyCustomTemplete(vc, templete1, templete2, rootPath); 
		}else{
			return templeteService.applyCustomTemplete(imgUrl,templete1,templete2,rootPath);
		}
	}
}
