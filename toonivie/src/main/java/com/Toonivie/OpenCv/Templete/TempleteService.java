package com.Toonivie.OpenCv.Templete;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;


@Service
public class TempleteService {

	public HashMap<String,Object> applyTemplete(String imgUrl,String rootPath){
		CreateFileUtils cfu = new CreateFileUtils();
		HashMap<String,Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		String today=cfu.getToday(0);
		if(CreateFileUtils.getFileType(imgUrl).equals("mov")){
			VideoCapture vc = new VideoCapture(rootPath+"resources/cvimgs/"+imgUrl);
			rainDrop(vc,rootPath,today);
		}else{
			Mat im = Imgcodecs.imread(rootPath+"resources/cvimgs/"+imgUrl);
			rainDrop(im,rootPath,today);
		}
		String fileName ="/resources/cvMov/templete"+today+".mp4";
		resultMap.put("videoUrl", fileName);
		return resultMap;
	}
	private void rainDrop(VideoCapture vc,String rootPath,String today){
		Mat rainPng = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_1.jpg",-1);
		Mat rainPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_2.jpg",-1);
		
		
		Imgproc.resize(rainPng, rainPng, new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        Imgproc.resize(rainPng2, rainPng2, new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        
		VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath+"/resources/cvMov/rain"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        /*videoWriter.open("src/main/webapp/resources/cvMov/rain"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));*/
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/rain"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        int count=0;
        Mat image = new Mat();
        while(vc.read(image)){
        	Mat im1 = image.clone();
            Mat im2 = image.clone();
        	Mat imageRoi = new Mat(im1,new Rect(0, 0, rainPng.cols(), rainPng.rows()));
            Mat imageRoi2 = new Mat(im2,new Rect(0, 0, rainPng2.cols(), rainPng2.rows()));
            Core.addWeighted(imageRoi, 1.0, rainPng, 1, 0.0, imageRoi);
            Core.addWeighted(imageRoi2, 1.0, rainPng2, 1, 0.0, imageRoi2);
            if(count%6>=3){
        		videoWriter.write(imageRoi);
                image.release();
        	}else{
        		videoWriter.write(imageRoi2);
                image.release();
        	}	
        	count++;
        }
        videoWriter.release();
	}
	private void rainDrop(Mat im,String rootPath, String today){
        Mat im1 = im.clone();
        Mat im2 = im.clone();
        
        Mat rainPng = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_1.jpg",-1);
        Mat rainPng2 = Imgcodecs.imread(rootPath+"resources/cvStaticImg/"+"raindrop_2.jpg",-1);
        Imgproc.resize(rainPng, rainPng, new Size(im.cols(),im1.rows()));
        Imgproc.resize(rainPng2, rainPng2, new Size(im.cols(),im2.rows()));
 
        Mat imageRoi = new Mat(im1,new Rect(0, 0, rainPng.cols(), rainPng.rows()));
        Mat imageRoi2 = new Mat(im2,new Rect(0, 0, rainPng2.cols(), rainPng2.rows()));
        
        Core.addWeighted(imageRoi, 1.0, rainPng, 1, 0.0, imageRoi);
        Core.addWeighted(imageRoi2, 1.0, rainPng2, 1, 0.0, imageRoi2);
        
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath+"/resources/cvMov/rain"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        /*videoWriter.open("src/main/webapp/resources/cvMov/rain"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,imageRoi.size());*/
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/rain"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,imageRoi.size());
        for (int i = 0; i < 75; i++) {
        	if(i%6>=3){
        		videoWriter.write(imageRoi);
                im.release();
        	}else{
        		videoWriter.write(imageRoi2);
                im.release();
        	}	
        }
        videoWriter.release();		
	}
	public HashMap<String,Object> applyVibrate(VideoCapture vc,String rootPath){
		HashMap<String,Object> resultMap = new HashMap<>();
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat img = new Mat();
		vc.read(img);
		
		int vibratePixelX = img.cols()/20;
		int vibratePixelY = img.rows()/20;
		CreateFileUtils cfu = new CreateFileUtils();
		String today=cfu.getToday(0);
		
		File file = new File(rootPath+"/resources/cvMov/vibrate"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        VideoWriter videoWriter  = new VideoWriter();
        /*videoWriter.open("src/main/webapp/resources/cvMov/vibrate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),16,new Size(img.cols()-vibratePixelX,img.rows()-vibratePixelY));*/
        //유덕희 경로 주석_180402
        videoWriter.open(rootPath+"resources/cvMov/vibrate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),16,new Size(img.cols()-vibratePixelX,img.rows()-vibratePixelY));
		
		Rect leftRect = new Rect(0,vibratePixelY/2,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect bottomRect = new Rect(vibratePixelX/2,vibratePixelY,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect rightRect = new Rect(vibratePixelX,vibratePixelY/2,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect topRect = new Rect(vibratePixelX/2,0,img.width()-vibratePixelX,img.height()-vibratePixelY);
		
		int i=0;
		while(vc.read(img)){
        	int order = i%4;
        	if(order==0){
        		Mat imageRoiLeft = new Mat(img,leftRect);
        		videoWriter.write(imageRoiLeft);
        	}else if(order==1){
        		Mat imageRoiRight = new Mat(img,rightRect);
        		videoWriter.write(imageRoiRight);
        	}else if(order==2){
        		Mat imageRoiBottom = new Mat(img,bottomRect);
        		videoWriter.write(imageRoiBottom);
        	}else{
        		Mat imageRoiTop = new Mat(img,topRect);
        		videoWriter.write(imageRoiTop);
        	}
        	i++;
		}
        videoWriter.release();
        
        resultMap.put("videoUrl", "/resources/cvMov/vibrate"+today+".mp4");
        resultMap.put("videoHeight", img.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", img.size().width);
		return resultMap;
	}
	public HashMap<String,Object> applyVibrate(String imgUrl,String rootPath){
		HashMap<String,Object> resultMap = new HashMap<>();
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		if(CreateFileUtils.getFileType(imgUrl).equals("mov")||CreateFileUtils.getFileType(imgUrl).equals("mp4")){
			VideoCapture vc = new VideoCapture(rootPath+"resources/cvimgs/"+imgUrl);
			return applyVibrate(vc,rootPath);
		}
		
		Mat img = Imgcodecs.imread(rootPath+"resources/cvimgs/"+imgUrl);
		System.out.println(img.channels());
		System.out.println(img.size().toString());
		
		int vibratePixelX = img.cols()/20;
		int vibratePixelY = img.rows()/20;
		
		Rect leftRect = new Rect(0,vibratePixelY/2,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect bottomRect = new Rect(vibratePixelX/2,vibratePixelY,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect rightRect = new Rect(vibratePixelX,vibratePixelY/2,img.width()-vibratePixelX,img.height()-vibratePixelY);
		Rect topRect = new Rect(vibratePixelX/2,0,img.width()-vibratePixelX,img.height()-vibratePixelY);
		
		Mat imageRoiLeft = new Mat(img,leftRect);
		Mat imageRoiBottom = new Mat(img,bottomRect);
		Mat imageRoiTop = new Mat(img,topRect);
		Mat imageRoiRight = new Mat(img,rightRect);
		
		CreateFileUtils cfu = new CreateFileUtils();
		String today=cfu.getToday(0);
		
		File file = new File(rootPath+"/resources/cvMov/vibrate"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        VideoWriter videoWriter  = new VideoWriter();
        /*videoWriter.open("src/main/webapp/resources/cvMov/vibrate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),16,imageRoiLeft.size());*/
        videoWriter.open(rootPath+"resources/cvMov/vibrate"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),16,imageRoiLeft.size());
        for(int i=0;i<400;i++){
        	int order = i%4;
        	if(order==0){
        		videoWriter.write(imageRoiLeft);
        	}else if(order==1){
        		videoWriter.write(imageRoiRight);
        	}else if(order==2){
        		videoWriter.write(imageRoiBottom);
        	}else{
        		videoWriter.write(imageRoiTop);
        	}
        }
        videoWriter.release();
        
        resultMap.put("videoUrl", "/resources/cvMov/vibrate"+today+".mp4");
        resultMap.put("videoHeight", img.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", img.size().width);
		return resultMap;
	}
	public HashMap<String,Object> applyCustomTemplete(String imgUrl,String templete1,String templete2,String rootPath){
		HashMap<String,Object> resultMap = new HashMap<>();
		
		CreateFileUtils cfu = new CreateFileUtils();
		String today = cfu.getToday(1);
		
		Mat back1 = Imgcodecs.imread(rootPath+"resources/cvimgs/"+imgUrl);
		Mat back2 = back1.clone();
		
		Mat tem1 = Imgcodecs.imread(rootPath+"resources/cvimgs/"+templete1);
		Mat tem2 = Imgcodecs.imread(rootPath+"resources/cvimgs/"+templete2);
		
		Imgproc.resize(tem1, tem1, new Size(back1.cols(),back1.rows()));
        Imgproc.resize(tem2, tem2, new Size(back2.cols(),back2.rows()));
        
        Mat imageRoi = new Mat(back1,new Rect(0, 0, tem1.cols(), tem1.rows()));
        Mat imageRoi2 = new Mat(back2,new Rect(0, 0, tem2.cols(), tem2.rows()));
        
        Core.addWeighted(imageRoi, 1.0, tem1, 1, 0.0, imageRoi);
        Core.addWeighted(imageRoi2, 1.0, tem2, 1, 0.0, imageRoi2);
        
        VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath+"/resources/cvMov/custom"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        /*videoWriter.open("src/main/webapp/resources/cvMov/custom"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,imageRoi.size());*/
        videoWriter.open(rootPath+"resources/cvMov/custom"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,imageRoi.size());
        
        for (int i = 0; i < 75; i++) {
        	if(i%6>=3){
        		videoWriter.write(imageRoi);
        	}else{
        		videoWriter.write(imageRoi2);
        	}	
        }
        videoWriter.release();
        
        resultMap.put("videoUrl", "/resources/cvMov/custom"+today+".mp4");
        resultMap.put("videoHeight", back1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", back1.size().width);
		return resultMap;
	}
	public HashMap<String,Object> applyCustomTemplete(VideoCapture vc,String templete1,String templete2,String rootPath){
		HashMap<String,Object> resultMap = new HashMap<>();
		
		Mat tem1 = Imgcodecs.imread(rootPath+"resources/cvimgs/"+templete1,-1);
		Mat tem2 = Imgcodecs.imread(rootPath+"resources/cvimgs/"+templete2,-1);
		
		Imgproc.resize(tem1, tem1, new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        Imgproc.resize(tem2, tem2, new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        
        CreateFileUtils cfu = new CreateFileUtils();
		String today = cfu.getToday(1);
        
		VideoWriter videoWriter  = new VideoWriter();
        File file = new File(rootPath+"/resources/cvMov/custom"+today+".mp4");
        if(file.exists()){
        	file.delete();
        }
        /*videoWriter.open("src/main/webapp/resources/cvMov/custom"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));*/
        videoWriter.open(rootPath+"resources/cvMov/custom"+today+".mp4",VideoWriter.fourcc('H', '2', '6', '4'),8,new Size((int)vc.get(Videoio.CAP_PROP_FRAME_WIDTH),(int)vc.get(Videoio.CAP_PROP_FRAME_HEIGHT)));
        int count=0;
        Mat image = new Mat();
        while(vc.read(image)){
        	Mat im1 = image.clone();
            Mat im2 = image.clone();
        	Mat imageRoi = new Mat(im1,new Rect(0, 0, tem1.cols(), tem1.rows()));
            Mat imageRoi2 = new Mat(im2,new Rect(0, 0, tem2.cols(), tem2.rows()));
            Core.addWeighted(imageRoi, 1.0, tem1, 1, 0.0, imageRoi);
            Core.addWeighted(imageRoi2, 1.0, tem2, 1, 0.0, imageRoi2);
            if(count%6>=3){
        		videoWriter.write(imageRoi);
        	}else{
        		videoWriter.write(imageRoi2);
        	}	
        	count++;
        }
        videoWriter.release();
        
        resultMap.put("videoUrl", "/resources/cvMov/custom"+today+".mp4");
        resultMap.put("videoHeight", tem1.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", tem1.size().width);
		return resultMap;
	}
}
