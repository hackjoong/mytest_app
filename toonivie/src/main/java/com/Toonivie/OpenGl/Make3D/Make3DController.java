package com.Toonivie.OpenGl.Make3D;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Util.DataURL_tofile;

@Controller
@RequestMapping("/make3D")
public class Make3DController {
	@Autowired
	Make3DService make3DService;
	
	@RequestMapping(value="/apply")
	@ResponseBody
	public HashMap<String,Object> apply(HttpServletRequest request){		
		String str = request.getParameter("data");
		System.out.println("Make3DController.java - apply() data : ");
		System.out.println(str);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject)jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONObject background = (JSONObject)jsonObject.get("background");
		JSONArray obj = (JSONArray)jsonObject.get("obj");
		String effectKind = jsonObject.get("effectKind").toString();
		System.out.println("Make3DController.java - effectKind : " + effectKind);
		if(effectKind.equals("8")){
			effectKind="left";
		}else if(effectKind.equals("9")){
			effectKind="right";
		}else if(effectKind.equals("10")){
			effectKind="top";
		}else if(effectKind.equals("11")){
			effectKind="bottom";
		}else if(effectKind.equals("5")){
			effectKind="zoomOut";
		}else if(effectKind.equals("6")){
			effectKind="zoomIn";
		}else if(effectKind.equals("12")){
			effectKind="ElementLeft";
		}else if(effectKind.equals("13")){
			effectKind="ElementRight";
		}else if(effectKind.equals("14")){
			effectKind="ElementTop";
		}else if(effectKind.equals("15")){
			effectKind="ElementDown";
		}else if(effectKind.equals("16")){
			effectKind="ZoomInLeftTop";
		}else if(effectKind.equals("17")){
			effectKind="ZoomInLeftBottom";
		}else if(effectKind.equals("18")){
			effectKind="ZoomInRightTop";
		}else if(effectKind.equals("19")){
			effectKind="ZoomInRightBottom";
		}else if(effectKind.equals("20")){
			effectKind="ZoomOutLeftTop";
		}else if(effectKind.equals("21")){
			effectKind="ZoomOutLeftBottom";
		}else if(effectKind.equals("22")){
			effectKind="ZoomOutRightTop";
		}else if(effectKind.equals("23")){
			effectKind="ZoomOutRightBottom";
		}
		int effectTime = Integer.parseInt(jsonObject.get("effectTime").toString());
		String rootPath = request.getServletContext().getRealPath("/");
		System.out.println("Make3DController.java - rootPath : " + rootPath);
		return make3DService.apply(background,obj,effectKind,rootPath,effectTime, request);
	}
	
	@RequestMapping(value="/test")
	@ResponseBody
	public HashMap<String,Object> test(HttpServletRequest request){
		String str = "{ \"effectKind\":\"8\",\"effectTime\":3,\"background\":{\"imgUrl\":\"http://localhost:8088/resources/cvimgs/backGround20170607174359.png\",\"top\":312,\"left\":475.75},\"obj\":[{\"top\":344,\"left\":1079.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607174353.png\"},{\"top\":462,\"left\":657.75,\"imgUrl\":\"http://localhost:8088/resources/cvimgs/result20170607174359.png\"}]}";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		JSONObject background = (JSONObject)jsonObject.get("background");
		JSONArray obj = (JSONArray)jsonObject.get("obj");
		String effectKind = jsonObject.get("effectKind").toString();
		if(effectKind.equals("8")){
			effectKind="left";
		}
		if(effectKind.equals("4")){
			effectKind="fadeIn";
		}
		int effectTime = Integer.parseInt(jsonObject.get("effectTime").toString());
		String rootPath = request.getServletContext().getRealPath("/");
		return make3DService.apply(background,obj,effectKind,rootPath,effectTime,request);
	}
}
