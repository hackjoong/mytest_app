package com.Toonivie.OpenGl.Make3D;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import org.springframework.stereotype.Service;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Util.ImageTypeConvert;
import com.Toonivie.Util.PutObjOnBackground;
import com.Toonivie.Vo.MoveObject3DVo;
import com.Toonivie.Vo.MoveObjectVo;
import com.jogamp.opengl.DefaultGLCapabilitiesChooser;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLDrawableFactory;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

@Service
public class Make3DService {
	static HttpServletRequest request_url;
	public static DisplayMode dm, dm_old;
	public static Dimension xgraphic;
	public static Point point = new Point(0, 0);
	private static float p1 = 0;
	private static float p2 = 0;
	private static float p3 = 2;
	private static float p4 = 0;
	private static float p5 = 0;
	private static float p6 = 0;
	private static float p7 = 0;
	private static float p8 = 1;
	private static float p9 = 0;
	static float backgroundMaxX = 6;
	static float backgroundMaxY = backgroundMaxX * 540 / 960; //no
	static float foregroundMaxX = 1;
	static float foregroundMaxY = foregroundMaxX * 255 / 130;
	//static float xRatio = (float)540.0 / (float)960.0; //no, no use
	private GLU glu = new GLU();
	static private int[] texture=new int[10];
//	static final int width = 960; //no
//	static final int height = 540; //no
	static int imgWidth = 0; //add by shkwak, 2017-07-12
	static int imgHeight = 0; //add
	static VideoWriter videoWriter;
	static int count = 0;
	final static int frame =20;
	static int effectTimeStatic;
	static ArrayList<ArrayList<HashMap<String, Integer>>> pngMapList;
	 float AmbientColor[]    = { 0.0f, 0.0f, 0.2f, 0.0f };				//주변광
	 float DiffuseColor[]    = { 0.5f, 0.5f, 0.5f, 0.0f };  			//분산광
	 float SpecularColor[]   = { 0.5f, 0.5f, 0.5f, 0.0f };  			//반사광
	 float Position[]        = { (float)0.0, (float)0.0, (float)1.0 };	//조명 위치

	private void panningLeft(){
		p1 -= 2.0f/(2.0f*effectTimeStatic*frame*2); //*2.5 by shkwak, 2017-11-22
		p3 = (float) Math.abs(Math.sqrt(2*2 - p1 * p1));
	}
	private void panningRight(){
		p1 += 2.0f/(2.0f*effectTimeStatic*frame*2);
		p3 = (float) Math.abs(Math.sqrt(2*2 - p1 * p1));
	}
	private void panningTop(){
		p2 += 2.0f/(2.0f*effectTimeStatic*frame*2);
		p3 = (float) Math.abs(Math.sqrt(2*2 - p2 * p2));
	}
	private void panningBottom(){
		p2 -= 2.0f/(2.0f*effectTimeStatic*frame*2);
		p3 = (float) Math.abs(Math.sqrt(2*2 - p1 * p1));
	}
	private void initCamera(){
		System.out.println("Make3DService.java:94 - initCamera()");
		p1=0; //eyex
		p2=0; //eyey		
		p3=2; //eyez
		p4=0;
		p5=0;
		p6=0;
		p7=0;
		p8=1;
		p9=0;
		
		/* 참고 by shkwak, 2017-11-22
		 * void gluLookAt(
			GLdouble eyex, 
			GLdouble eyey, 
			GLdouble eyez, 
			GLdouble centerx, 
			GLdouble centery, 
			GLdouble centerz, 
			GLdouble upx, 
			GLdouble upy, 
			GLdouble upz 
		);*/
	}
	
	/* 3D 효과 코드 */
	public HashMap<String, Object> apply(JSONObject background, JSONArray obj, String effectKind, String rootPath, int effectTime, HttpServletRequest request){
		request_url = request;
		System.out.println("Make3DService.java:108 - apply() background : ↓");	
		HashMap<String, Object> resultMap = new HashMap<>();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME); //c 나 c++ 어샘블리 같은 다른 언어들로 구현된 라이브러리 호출시 사용.
		initCamera(); //initCamera() -------------------------------------------------------------------------------------------(1)
		imgWidth = Integer.parseInt(background.get("imgWidth").toString()); //add by shkwak, 2017-7-12
		imgHeight = Integer.parseInt(background.get("imgHeight").toString()); //add
		System.out.println("Make3DService.java:114 - imgWidth : " + imgWidth + ", imgHeight : " + imgHeight);
		MoveObject3DVo backgroundVo = changeMoveBackgroundVo(background);
		System.out.println(backgroundVo); 
		List<MoveObject3DVo> otherList = makeOtherList(obj, backgroundVo);
		System.out.println("Make3DService.java:118 - effectKind : " + effectKind);
		if (effectKind.equals("left")||effectKind.equals("right")||effectKind.equals("top")||effectKind.equals("bottom")
		||effectKind.equals("ElementLeft")||effectKind.equals("ElementRight")||effectKind.equals("ElementTop")||effectKind.equals("ElementDown")) {
			System.out.println("Make3DService.java:120 - make3D 전"); //
			resultMap = make3D(backgroundVo, otherList, effectKind, rootPath, effectTime); //panning -> make3D() ---------------(2)
		}else if(effectKind.equals("zoomIn")||effectKind.equals("zoomOut")||effectKind.equals("ZoomInLeftTop")||effectKind.equals("ZoomInLeftBottom")
				||effectKind.equals("ZoomInRightTop")||effectKind.equals("ZoomInRightBottom")||effectKind.equals("ZoomOutLeftTop")||effectKind.equals("ZoomOutLeftBottom")
				||effectKind.equals("ZoomOutRightTop")||effectKind.equals("ZoomOutRightBottom")){
			System.out.println("Make3DService.java:123 - zoom 전");
			resultMap = zoom(backgroundVo, otherList, effectKind, rootPath, effectTime);
		}
		return resultMap;
	}
	
	/* 실제 3D 효과 코드 */
	private HashMap<String, Object> make3D(MoveObject3DVo background, List<MoveObject3DVo> objList, String effectKind,	String rootPath, int effectTime){
		HashMap<String, Object> resultMap = new HashMap<>();	
		System.out.println("Make3DService.java:136 - make3D() in");
		System.out.println(background);
		
		imgWidth = Integer.parseInt(background.getImgWidth()); //add by shkwak, 2017-07-12
		imgHeight = Integer.parseInt(background.getImgHeight());
		System.out.println("make3D() - imgWidth : " + imgWidth + ", imgHeight : " + imgHeight); //OK, Vo 사용하지 않고 사이즈 정보 처리 방법도 有!
		
		/* 3개 중 하나 사용  openGL 기본 프로파일 설정*/
		GLProfile glp = GLProfile.get(GLProfile.GL2); //OK The desktop OpenGL profile 1.x up to 3.0-------------------------------------------------(3)
		//GLProfile glp = GLProfile.getDefault(); // GLProfile object, reflecting the best for the running platform.
		//GLProfile glp = GLProfile.getMaxFixedFunc(true); //Uses the default device
		GLCapabilities caps = new GLCapabilities(glp); //openGL 적용 프로파일 설정. 기초환경설정?
		caps.setHardwareAccelerated(true); //Enables or disables hardware acceleration.
		caps.setDoubleBuffered(false); //Enables or disables double buffering.
		caps.setAlphaBits(8); //Sets the number of bits requested for the color buffer's alpha component. 
		caps.setRedBits(8);
		caps.setBlueBits(8);
		caps.setGreenBits(8);
		caps.setOnscreen(false); //Sets whether the drawable surface supports onscreen. Defaults to true.
		GLDrawableFactory factory = GLDrawableFactory.getFactory(glp); //Provides a virtual machine- and operating system-independent mechanism for creating GLDrawables.
		GLAutoDrawable drawable = factory.createOffscreenAutoDrawable(factory.getDefaultDevice(), caps, new DefaultGLCapabilitiesChooser(), imgWidth, imgHeight); //width, height
		drawable.display();
		drawable.getContext().makeCurrent();
		
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);		
		videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/make3D"+today+".mp4", 
				VideoWriter.fourcc('H', '2', '6', '4'), 20, new Size(imgWidth, imgHeight)); //mp4 생성
		
		new Make3DService().init(drawable, objList, rootPath, background); //init() --------------------------------------------(4)
		Mat thumbNail = new Mat();
		effectTimeStatic = effectTime; //효과 시간 
		try {
			thumbNail = render(drawable, objList, rootPath, effectTime, effectKind); //render() --------------------------------(5)
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl", "/resources/cvMov/make3D"+today+".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", imgHeight); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", imgWidth);
		return resultMap;
	}
	
	/* init() 편집할 이미지 합치는 부분*/ 
	private void init(GLAutoDrawable drawable, List<MoveObject3DVo> objList, String rootPath, MoveObject3DVo background){
		//System.out.println(setPngImage(objList, rootPath).size());
		final GL2 gl = drawable.getGL().getGL2();
		gl.glShadeModel(GL2.GL_SMOOTH);			//셰이딩 방식을 설정한다. Enable Smooth Shading
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0f);	//화면배경 색상을 설정한다. Black Background
		gl.glClearDepth(1.0f);					//Depth Buffer Setup
		//gl.glEnable(GL2.GL_DEPTH_TEST);
		//gl.glEnable(GL2.GL_CULL_FACE);
		gl.glEnable(GL2.GL_BLEND);				//Enable Depth Testing
//		gl.glEnable(GL2.GL_LIGHTING);
//		gl.glEnable(GL2.GL_LIGHT0);
//		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[]{(float) 0.0, (float)0.0, (float)1.0}, 0);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA); //
		gl.glDepthFunc(GL2.GL_LEQUAL);			//The Type Of Depth Testing To Do
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); //Really Nice Perspective Calculations

		// textureSetting
		gl.glEnable(GL2.GL_TEXTURE_2D);
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path= "";
		try {
			int index = 0;
			for (MoveObject3DVo obj : objList) {
				path = rootPath+obj.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.88:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
				putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
				File im = new File(rootPath + obj.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.250:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""));
				System.out.println();
				Texture t = TextureIO.newTexture(im, true);
				texture[index] = t.getTextureObject(gl);
				gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
				gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
				gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[index]);
				index++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		File im = new File(rootPath + background.getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.250:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""));
		Texture t = null;
		try {
			t = TextureIO.newTexture(im, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		texture[9] = t.getTextureObject(gl);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[9]);
		//File video = new File("adsf.mp4");
		//if (video.exists()) {
		//	video.delete();
		//}		
	}
	
	/* render() */
	private Mat render(GLAutoDrawable drawable, List<MoveObject3DVo> objList, String rootPath, int effectTime, String effectKind) throws Exception {
		Mat thumbNail = new Mat();
		int flag = 0;
		float angle = 0.0f;
		float scale = 1.0f;
		
		for(int j=0;j<effectTime*frame;j++){
			final GL2 gl = drawable.getGL().getGL2();
			gl.glLoadIdentity();
			gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); //화면과 버퍼를 지운다. Clear Screen And Depth Buffer
			setCamera(gl, glu, p1, p2, p3, p4, p5, p6, p7, p8, p9); //setCamera -> gluLookAt : 화면이동 및 회전 구현
			
			//final GL2 gl2 = drawable.getGL().getGL2();
			//gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); //화면과 버퍼를 지운다. Clear Screen And Depth Buffer
			//setCamera(gl, glu, p1, p2, p3, p4, p5, p6, p7, p8, p9); //카메라 회전x
			
			int index=0;
			float depthPlus = 1/objList.size();
			float depth = 1/objList.size();
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[9]); //Texture 이름을 데이터와 연결한다.
			gl.glBegin(GL2.GL_QUADS); //gl 시작!
			
			//gl2.glBindTexture(GL2.GL_TEXTURE_2D, texture[9]); //Texture 이름을 데이터와 연결한다.
			//gl2.glBegin(GL2.GL_QUADS); //gl 시작!
			
			/* 고정값 -> 변동값, modify by shkwak, 2017-07-13
			static float backgroundMaxX = 6;
			static float backgroundMaxY = backgroundMaxX * 540 / 960; //no
			static float foregroundMaxX = 1;
			static float foregroundMaxY = foregroundMaxX * 255 / 130;
			static float xRatio = (float)540.0 / (float)960.0; //no
			*/
			backgroundMaxY = backgroundMaxX * imgHeight / imgWidth;
			//xRatio = (float)bimgHeight / (float)bimgWidth;
			// 3D 입체화 - 배경 매핑 
			gl.glTexCoord3f(0.0f, 0.0f, 0.0f);
			gl.glVertex3f(-3.0f, 0-backgroundMaxY/2, 0.0f); //기존			
			
			gl.glTexCoord3f(0.0f, 1.0f, 0.0f);
			gl.glVertex3f(-3.0f, backgroundMaxY-backgroundMaxY/2, 0.0f); //기존
			
			gl.glTexCoord3f(1.0f, 1.0f, 0.0f);
			gl.glVertex3f(backgroundMaxX-3.0f, backgroundMaxY-backgroundMaxY/2, 0.0f); //기존
			
			gl.glTexCoord3f(1.0f, 0.0f, 0.0f);
			gl.glVertex3f(backgroundMaxX-3.0f, 0-backgroundMaxY/2, 0.0f); //기존
				
			gl.glEnd(); //gl 끝!
			
/*			// 3D 입체화 - 배경 매핑 
			gl.glTexCoord3f(0.0f, 0.0f, 0.0f); //텍스처 매핑의 좌표를 설정하는 함수
			//gl.glVertex2f(-3.5f, -2.0f);
			gl.glVertex3f(-3.0f, 0-backgroundMaxY/2, 0.0f); //기존
			gl.glTexCoord3f(0.0f, 1.0f, 0.0f);
			//gl.glVertex2f(-3.5f, 2.0f);
			gl.glVertex3f(-3.0f, backgroundMaxY-backgroundMaxY/2, 0.0f); //기존
			gl.glTexCoord3f(1.0f, 1.0f, 0.0f);
			//gl.glVertex2f(3.5f, 2.0f);
			gl.glVertex3f(backgroundMaxX-3.0f, backgroundMaxY-backgroundMaxY/2, 0.0f); //기존
			gl.glTexCoord3f(1.0f, 0.0f, 0.0f);
			//gl.glVertex2f(3.5f, -2.0f);
			gl.glVertex3f(backgroundMaxX-3.0f, 0-backgroundMaxY/2, 0.0f); //기존	
			gl.glEnd(); //gl 끝!
*/		
			for(MoveObject3DVo vo : objList){
				
				gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[index]);
				Mat image = Imgcodecs.imread(rootPath+objList.get((int)index).getImgUrl().replace("http://localhost:8088/", "").replace("http://192.168.0.250:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""));
				//MoveObject3DVo vo = objList.get((int) i);
				float width = (float)image.cols();
				float height = (float)image.rows();				
				float left = vo.getLeft();
				float top = vo.getTop();
				//중앙값 구하기
				float selected_pic_x = (float)(width/2)+left;
				float selected_pic_y = (float)(height/2)+top;
				//gl 좌표로 변환 
				float from_center_x = (float) (selected_pic_x*(float)6.0/(float)imgWidth)-3.0f;
				float from_center_y = backgroundMaxY-(selected_pic_y)*(float)6.0/(float)imgWidth-backgroundMaxY/2;
/*				float from_center_x = ((float)(selected_pic_x - (float)imgWidth / 2.0)*(float)(1.0 / (float)(imgWidth / 2.0)))*(float)3;
				float from_center_y = (-(float)(selected_pic_y - (float)imgHeight / 2.0)*(float)(1.0 / (float)(imgHeight / 2.0)))*(float)3;*/
				float to_center_x;
				float to_center_y;
				if(from_center_x < 0) {
					to_center_x = Math.abs(from_center_x);
				}else {
					to_center_x = (float)from_center_x * (float)-1;
				}
				if(from_center_y < 0) {
					to_center_y = Math.abs(from_center_y);
				}else {
					to_center_y = (float)from_center_y * (float)-1;
				}
				if (effectKind.equals("ElementLeft")) {
					//물체 회전 및 사이즈업
					gl.glTranslatef(from_center_x, from_center_y,  ((float)index+1f)*.15f);
					//gl.glScalef(scale, scale, scale);
					gl.glRotatef(angle, 0f, -1.0f, 0f); // 회전 각도 방향 설정
					gl.glTranslatef( to_center_x, to_center_y, -1*((float)index+1f)*.15f);
					//angle += 0.2f;
					//scale += 0.001f;
				}else if(effectKind.equals("ElementRight")){
					//물체 회전 및 사이즈업
					gl.glTranslatef( from_center_x, from_center_y, ((float)index+1f)*.15f);
					//gl.glScalef(scale, scale, scale);
					gl.glRotatef(angle, 0f, 1.0f, 0f); // 회전 각도 방향 설정
					gl.glTranslatef( to_center_x, to_center_y, -1*((float)index+1f)*.15f);
					//angle += 0.2f;
					//scale += 0.001f;
				}else if(effectKind.equals("ElementTop")){
					//물체 회전 및 사이즈업
					gl.glTranslatef( from_center_x, from_center_y, ((float)index+1f)*.15f);
					//gl.glScalef(scale, scale, scale);
					gl.glRotatef(angle, 1f, 0f, 0f); // 회전 각도 방향 설정
					gl.glTranslatef( to_center_x, to_center_y, -1*((float)index+1f)*.15f);
					//angle += 0.2f;
					//scale += 0.001f;
				}else if(effectKind.equals("ElementDown")){
					//물체 회전 및 사이즈업
					gl.glTranslatef( from_center_x, from_center_y, ((float)index+1f)*.15f);
					//gl.glScalef(scale, scale, scale);
					gl.glRotatef(angle, -1f, 0f, 0f); // 회전 각도 방향 설정
					gl.glTranslatef( to_center_x, to_center_y,-1*((float)index+1f)*.15f);
					//angle += 0.2f;
					//scale += 0.001f;

				}
				
/*				gl.glBegin(GL2.GL_QUADS);
				gl.glTexCoord2f(0.0f, 0.0f);
				gl.glVertex2f((float) (left*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top+height)*(float)6.0/(float)imgWidth-backgroundMaxY/2);
				gl.glTexCoord2f(0.0f, 1.0f);
				gl.glVertex2f((float) (left*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top)*(float)6.0/(float)imgWidth-backgroundMaxY/2);
				gl.glTexCoord2f(1.0f, 1.0f);
				gl.glVertex2f((float) ((float) (left*6.0/imgWidth)+((float)width)*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top)*(float)6.0/(float)imgWidth-backgroundMaxY/2);
				gl.glTexCoord2f(1.0f, 0.0f);
				gl.glVertex2f((float) ((float) (left*6.0/imgWidth)+((float)width)*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top+height)*(float)6.0/(float)imgWidth-backgroundMaxY/2);
				gl.glEnd();*/
			
				gl.glBegin(GL2.GL_QUADS);
				gl.glTexCoord3f(0.0f, 0.0f, 0.0f);
				gl.glVertex3f((float) (left*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top+height)*(float)6.0/(float)imgWidth-backgroundMaxY/2, ((float)index+1f)*.15f);
				gl.glTexCoord3f(0.0f, 1.0f, 0.0f);
				gl.glVertex3f((float) (left*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top)*(float)6.0/(float)imgWidth-backgroundMaxY/2, ((float)index+1f)*.15f);
				gl.glTexCoord3f(1.0f, 1.0f, 0.0f);
				gl.glVertex3f((float) ((float) (left*6.0/imgWidth)+((float)width)*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top)*(float)6.0/(float)imgWidth-backgroundMaxY/2, ((float)index+1f)*.15f);
				gl.glTexCoord3f(1.0f, 0.0f, 0.0f);
				gl.glVertex3f((float) ((float) (left*6.0/imgWidth)+((float)width)*(float)6.0/(float)imgWidth)-3.0f, backgroundMaxY-(top+height)*(float)6.0/(float)imgWidth-backgroundMaxY/2, ((float)index+1f)*.15f);
				gl.glEnd();

				gl.glLoadIdentity();
				index++;
				depth+=depthPlus;
			}
			angle += 0.2f;
			scale += 0.001f;
			BufferedImage im = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_3BYTE_BGR);
			im = new AWTGLReadBufferUtil(drawable.getGLProfile(), false).readPixelsToBufferedImage(drawable.getGL(), 0, 0, imgWidth, imgHeight, true);
			BufferedImage im2 = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_3BYTE_BGR);
			im2.getGraphics().drawImage(im, 0, 0, null);
			Mat out = screenshot(im2);
			gl.glFlush();

			if (effectKind.equals("left")) {
				panningLeft(); 
			}else if(effectKind.equals("right")){
				panningRight();
			}else if(effectKind.equals("top")){
				panningTop();
			}else if(effectKind.equals("bottom")){
				panningBottom();
			}

			if(frame*effectTime/4==j){
				thumbNail= out.clone();
			}
			videoWriter.write(out);
		}
		videoWriter.release();
		return thumbNail;
	}

	private void setCamera(GL2 gl, GLU glu, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8, float p9) {
		// Change to projection matrix.
		gl.glMatrixMode(GL2.GL_PROJECTION); 				//Select The ModelView Matrix
		gl.glLoadIdentity(); 								//Reset The ModelView Matrix

		// Perspective.
		float widthHeightRatio = (float)imgWidth / (float)imgHeight;
		glu.gluPerspective(90, widthHeightRatio, 1, 1000); 	//Calculate The Aspect Ratio Of The Window
		glu.gluLookAt(p1, p2, p3, p4, p5, p6, p7, p8, p9); 	//카메라의 위치와 방향을 설정한다. -> glRotate();
		
		// Change back to model view matrix.
		gl.glMatrixMode(GL2.GL_MODELVIEW); 					//Select The ModelView Matrix
		gl.glLoadIdentity(); 								//Reset The ModelView Matrix
	}
	
	/* 카메라 연출 - Zoom 효과 */
	private HashMap<String,Object> zoom(MoveObject3DVo background, List<MoveObject3DVo> objList, String effectKind, String rootPath, int effectTime){
		HashMap<String,Object> resultMap = new HashMap<>();
		Mat im = Imgcodecs.imread(rootPath + background.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.250:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			
		PutObjOnBackground putObjOnBackground = new PutObjOnBackground();
		String path;
		for(MoveObjectVo obj : objList){
			path = rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.44:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), "");
			putObjOnBackground.resize_mat(path, obj.getHeight(), obj.getWidth());
			Mat fore = Imgcodecs.imread(rootPath + obj.getImgUrl().replace("http://localhost:8088", "").replace("http://192.168.0.250:8088", "").replace("www."+request_url.getServerName()+".com","").replace(request_url.getScheme()+"://"+request_url.getServerName()+":"+request_url.getServerPort(), ""), Imgcodecs.IMREAD_UNCHANGED);
			putObjOnBackground.setBackMat((int)obj.getLeft(), (int)obj.getTop(), im, fore.clone());
		}
		ImageTypeConvert imagetypeconvert = new ImageTypeConvert(im.width(), im.height());
		im = imagetypeconvert.bgraTobgr(im);
        
		if(effectKind.equals("zoomOut")){
			resultMap = zoomOut(im, effectTime, rootPath);
		}else if (effectKind.equals("zoomIn")){
			resultMap = zoomIn(im, effectTime, rootPath);
		}
		else if(effectKind.equals("ZoomInLeftTop")||effectKind.equals("ZoomInLeftBottom")||effectKind.equals("ZoomInRightTop")||effectKind.equals("ZoomInRightBottom")) {
			resultMap = cornerzoomIn(im, effectTime, rootPath, effectKind);
		}
		else if(effectKind.equals("ZoomOutLeftTop")||effectKind.equals("ZoomOutLeftBottom")||effectKind.equals("ZoomOutRightTop")||effectKind.equals("ZoomOutRightBottom")) {
			resultMap = cornerzoomOut(im, effectTime, rootPath, effectKind);
		}
		return resultMap;
	}
	
	private HashMap<String,Object> zoomOut(Mat im, int effectTime, String rootPath){ //im 이용하여 사이즈 정보 활용!!

		HashMap<String,Object> resultMap = new HashMap<>();
		Mat black = new Mat(im.size(),CvType.CV_8UC3,new Scalar(0,0,0)); //사이즈 수정 필요, new Size(width,height) -> im.size()
		int totalFrame = effectTime * frame; //20
		float hPerW = (float)im.rows()/(float)im.cols();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/zoomOut"+today+".mp4", 
				VideoWriter.fourcc('H', '2', '6', '4'), 20, im.size()); //new Size(width, height) -> im.size(), 2017-07-13
		Mat thumbNail= new Mat();
		for(int i=0;i<totalFrame;i++){
			Mat resizeIm = new Mat();
			Mat blackC = black.clone();
			Imgproc.resize(im, resizeIm, new Size((int)((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4)),(int)(((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4))*hPerW)));
			Mat blackSubmat = blackC.submat(new Rect((black.cols()-resizeIm.cols())/2,(black.rows()-resizeIm.rows())/2,resizeIm.cols(),resizeIm.rows()));
			resizeIm.copyTo(blackSubmat);
			if(i==totalFrame/4){
				thumbNail = blackC.clone();
			}
			videoWriter.write(blackC);
		}
		videoWriter.release();


		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl", "/resources/cvMov/zoomOut"+today+".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	
	private HashMap<String,Object> zoomIn(Mat im,int effectTime,String rootPath){
		System.out.println("Make3DService.java:272 - zoomIn : im.size()");
		HashMap<String,Object> resultMap = new HashMap<>();
		System.out.println(im.size());
		Mat black = new Mat(im.size(),CvType.CV_8UC3,new Scalar(0,0,0)); //사이즈 수정 필요
		int totalFrame = effectTime * frame;
		float hPerW = (float)im.rows()/(float)im.cols(); //※Mat col() = width()
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/zoomIn"+today+".mp4", 
				VideoWriter.fourcc('H', '2', '6', '4'), 20, im.size());
		Mat thumbNail= new Mat();
		List<Mat> mats = new ArrayList<>();
		for(int i=0;i<totalFrame;i++){ 
			Mat resizeIm = new Mat();
			Mat blackC = black.clone();
			Imgproc.resize(im, resizeIm, new Size((int)((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4)),(int)(((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4))*hPerW)));
			System.out.println(resizeIm.cols() + " x " + resizeIm.rows());
			Mat blackSubmat = blackC.submat(new Rect((black.cols()-resizeIm.cols())/2,(black.rows()-resizeIm.rows())/2,resizeIm.cols(),resizeIm.rows()));
			resizeIm.copyTo(blackSubmat);
/*			if(i==totalFrame/2){
				thumbNail = blackC.clone();
			}*/
			mats.add(blackC);
			//videoWriter.write(blackC);
		}
		for(int i=mats.size()-1;i>=0;i--){
			videoWriter.write(mats.get(i));
			if(i==totalFrame/4){
				thumbNail = mats.get(i).clone();
			}
		}
		videoWriter.release();
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl", "/resources/cvMov/zoomIn"+today+".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", black.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", black.size().width);
		return resultMap;
	}
	
	//유덕희 효과
	private HashMap<String,Object> cornerzoomIn(Mat im,int effectTime,String rootPath, String startPoint){
		//startPoint : leftTop, leftBottom, rightTop, rightBottom
		System.out.println("Make3DService.java:272 - zoomIn : im.size()");
		HashMap<String,Object> resultMap = new HashMap<>();
		System.out.println(im.size());
		Mat black = new Mat(im.size(),CvType.CV_8UC3,new Scalar(0,0,0)); //사이즈 수정 필요
		int totalFrame = effectTime * frame;
		float hPerW = (float)im.rows()/(float)im.cols(); //※Mat col() = width()
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/"+startPoint+"_zoomIn"+today+".mp4", 
				VideoWriter.fourcc('H', '2', '6', '4'), 20, im.size());
		Mat thumbNail= new Mat();
		System.err.println(black.cols());
		
		int startwidth = 0;
		int startheight = 0;
		List<Mat> mats = new ArrayList<>();
		for(int i=0;i<totalFrame;i++){
			Mat resizeIm = new Mat();
			Mat blackC = black.clone();
			
			Imgproc.resize(im, resizeIm, new Size((int)((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4)),(int)(((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4))*hPerW)));
			System.out.println(resizeIm.cols() + " x " + resizeIm.rows());
			switch (startPoint) {
			case "ZoomInLeftTop":
				startwidth = 0;
				startheight = 0;
				break;
			case "ZoomInLeftBottom":
				startwidth = 0;
				startheight = (black.rows()-resizeIm.rows());
				break;
			case "ZoomInRightTop":
				startwidth = (black.cols()-resizeIm.cols());
				startheight = 0;
				break;
			case "ZoomInRightBottom":
				startwidth = (black.cols()-resizeIm.cols());
				startheight = (black.rows()-resizeIm.rows());
				break;
			default:
				startwidth = 0;
				startheight = 0;
				break;
			}
			Mat blackSubmat = blackC.submat(new Rect(startwidth,startheight,resizeIm.cols(),resizeIm.rows()));
			resizeIm.copyTo(blackSubmat);
			mats.add(blackC);
		}
		System.out.println(mats.size());
		for(int i=mats.size()-1;i>=0;i--){
			videoWriter.write(mats.get(i));
			if(i==totalFrame/4){
				thumbNail = mats.get(i).clone();
			}
		}
		videoWriter.release();
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl", "/resources/cvMov/"+startPoint+"_zoomIn"+today+".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", black.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", black.size().width);
		return resultMap;
	}
	
	private HashMap<String,Object> cornerzoomOut(Mat im, int effectTime, String rootPath, String startPoint){ //im 이용하여 사이즈 정보 활용!!
		System.out.println("Make3DService.java:236 - zoomOut : im.size()");
		HashMap<String,Object> resultMap = new HashMap<>();
		System.out.println(im.size() + ", " + im.width()); //960x540
		Mat black = new Mat(im.size(),CvType.CV_8UC3,new Scalar(0,0,0)); //사이즈 수정 필요, new Size(width,height) -> im.size()
		int totalFrame = effectTime * frame; //20
		float hPerW = (float)im.rows()/(float)im.cols();
		CreateFileUtils createFileUtils = new CreateFileUtils();
		String today = createFileUtils.getToday(1);
		videoWriter = new VideoWriter();
		videoWriter.open(rootPath+"resources/cvMov/"+startPoint+"_zoomOut"+today+".mp4", 
				VideoWriter.fourcc('H', '2', '6', '4'), 20, im.size()); //new Size(width, height) -> im.size(), 2017-07-13
		Mat thumbNail= new Mat();
		int startwidth = 0;
		int startheight = 0;
		for(int i=0;i<totalFrame;i++){
			Mat resizeIm = new Mat();
			Mat blackC = black.clone();
			Imgproc.resize(im, resizeIm, new Size((int)((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4)),(int)(((float)im.cols()-((float)i/(float)totalFrame)*((float)im.cols()-4))*hPerW)));
			System.out.println(resizeIm.cols() + " x " + resizeIm.rows());
			switch (startPoint) {
			case "ZoomOutLeftTop":
				startwidth = 0;
				startheight = 0;
				break;
			case "ZoomOutLeftBottom":
				startwidth = 0;
				startheight = (black.rows()-resizeIm.rows());
				break;
			case "ZoomOutRightTop":
				startwidth = (black.cols()-resizeIm.cols());
				startheight = 0;
				break;
			case "ZoomOutRightBottom":
				startwidth = (black.cols()-resizeIm.cols());
				startheight = (black.rows()-resizeIm.rows());
				break;
			default:
				startwidth = 0;
				startheight = 0;
				break;
			}
			Mat blackSubmat = blackC.submat(new Rect(startwidth,startheight,resizeIm.cols(),resizeIm.rows()));
			resizeIm.copyTo(blackSubmat);
			if(i==totalFrame/4){
				thumbNail = blackC.clone();
			}
			videoWriter.write(blackC);
		}
		videoWriter.release();
		/*String thumbNailImgName = "src/main/webapp/resources/cvimgs/thumbNail" + today + ".png";*/
		//유덕희 주석 처리_180402
		String thumbNailImgName = rootPath+"resources/cvimgs/thumbNail" + today + ".png";
		Imgcodecs.imwrite(thumbNailImgName, thumbNail);
		resultMap.put("videoUrl", "/resources/cvMov/"+startPoint+"_zoomOut"+today+".mp4");
		resultMap.put("effectTime", effectTime);
		resultMap.put("thumbNailUrl", "/resources/cvimgs/thumbNail" + today + ".png");
		resultMap.put("videoHeight", im.size().height); //add by shkwak, 2017-07-12
		resultMap.put("videoWidth", im.size().width);
		return resultMap;
	}
	
	public Mat screenshot(BufferedImage im) {
		// Tries to create an image, otherwise throws an exception.
		Mat out= new Mat();
		try {
			out = new Mat(im.getHeight(), im.getWidth(), CvType.CV_8UC3);
			BufferedImage image = im;
			byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
			out.put(0, 0, pixels);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}
	
	/* changeMoveBackgroundVo() */
	private MoveObject3DVo changeMoveBackgroundVo(JSONObject obj) {
		MoveObject3DVo moveObjectVo = new MoveObject3DVo();
		String imgUrl = obj.get("imgUrl").toString();
		Double d3 = new Double(obj.get("top").toString());
		Double d4 = new Double(obj.get("left").toString());
		float top = d3.floatValue();
		float left = d4.floatValue();
		String imgWidth = obj.get("imgWidth").toString(); //add by shkwak, 2017-07-12
		String imgHeight = obj.get("imgHeight").toString(); //add
		
		moveObjectVo.setImgUrl(imgUrl);
		moveObjectVo.setLeft(left);
		moveObjectVo.setTop(top);
		moveObjectVo.setImgWidth(imgWidth); //add by shkwak, 2017-07-12
		moveObjectVo.setImgHeight(imgHeight); //add
		return moveObjectVo;
	}

	private List<MoveObject3DVo> makeOtherList(JSONArray others, MoveObjectVo backgroundVo) {
		List<MoveObject3DVo> moveObjectVos = new ArrayList<>();
		for (int i = 0; i < others.size(); i++) {
			JSONObject object = (JSONObject) others.get(i);
			System.out.println(object.get("imgUrl").toString());
			String imgUrl = object.get("imgUrl").toString();
			Double d3 = new Double(object.get("top").toString());
			Double d4 = new Double(object.get("left").toString());
			float top = d3.floatValue();
			float left = d4.floatValue();

			MoveObject3DVo vo = new MoveObject3DVo();
			vo.setImgUrl(imgUrl);
			vo.setTop(top - backgroundVo.getTop());
			vo.setLeft(left - backgroundVo.getLeft());
			vo.setZ_index((float) (i * 0.5));
			vo.setHeight((new Double(object.get("height").toString())).floatValue());
			vo.setWidth((new Double(object.get("width").toString())).floatValue());
			
			moveObjectVos.add(vo);
		}
		return moveObjectVos;
	}
}