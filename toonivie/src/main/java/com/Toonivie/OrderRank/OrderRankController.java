package com.Toonivie.OrderRank;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.WebtoonVo;

@Controller
@RequestMapping("/order")
public class OrderRankController {
	@Autowired
	OrderRankService service;
	
	@RequestMapping(value="/lastest", method=RequestMethod.POST)
	@ResponseBody
	public List<ToonVo> Latest(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<ToonVo> latestToonList = service.lastest();
		return latestToonList;
	}
	
	@RequestMapping(value="/commentOrder", method=RequestMethod.POST)
	@ResponseBody
	public List<ToonVo> commentOrder(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<ToonVo> latestToonList = service.commentOrder();
		return latestToonList;
	}
	
	
	@RequestMapping(value="/countOrder", method=RequestMethod.POST)
	@ResponseBody
	public List<ToonVo> countOrder(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<ToonVo> latestToonList = service.countOrder();
		return latestToonList;
	}
	
	
	//webtoon sort
	@RequestMapping(value="/lastest_won", method=RequestMethod.POST)
	@ResponseBody
	public List<WebtoonVo> Latest_won(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<WebtoonVo> latestWonList = service.lastest_won();
		return latestWonList;
	}
	

	@RequestMapping(value="/commentOrder_won", method=RequestMethod.POST)
	@ResponseBody
	public List<WebtoonVo> commentOrder_won(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<WebtoonVo> commentWonList = service.commentOrder_won();
		return commentWonList;
	}
	
	
	@RequestMapping(value="/countOrder_won", method=RequestMethod.POST)
	@ResponseBody
	public List<WebtoonVo> countOrder_won(Model model){
		//model.addAttribute("lastest",service.Latest());		
		List<WebtoonVo> countWonList = service.countOrder_won();
		return countWonList;
	}
}
