package com.Toonivie.OrderRank;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.WebtoonVo;

@Mapper
public interface OrderRankDao {
	
	public List<ToonVo> lastest();
	
	public List<ToonVo> commentOrder();
	
	public List<ToonVo> countOrder();
	
	
	
	//원본웹툰 정렬
	public List<WebtoonVo> lastest_won();

	public List<WebtoonVo> commentOrder_won();
	
	public List<WebtoonVo> countOrder_won();
}
