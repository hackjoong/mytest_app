package com.Toonivie.OrderRank;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.WebtoonVo;

@Service
public class OrderRankService {
	
	@Autowired
	OrderRankDao dao;
	
	public List<ToonVo> lastest(){
		return dao.lastest();
	}
	
	public List<ToonVo> commentOrder(){
		return dao.commentOrder();
	}
	
	public List<ToonVo> countOrder(){
		return dao.countOrder();
	}
	
	
	//원본웹툰 정렬
	public List<WebtoonVo> lastest_won(){
		return dao.lastest_won();
	}

	public List<WebtoonVo> commentOrder_won(){
		return dao.commentOrder_won();
	}
	
	public List<WebtoonVo> countOrder_won(){
		return dao.countOrder_won();
	}
}
