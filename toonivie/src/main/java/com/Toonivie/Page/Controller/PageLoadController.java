package com.Toonivie.Page.Controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.Toonivie.ContentView.ContentViewService;
import com.Toonivie.Member.MemberDao;
import com.Toonivie.Mypage.MypageService;
import com.Toonivie.OpenCv.Grabcut.GrabcutService;
import com.Toonivie.OrderRank.OrderRankService;
import com.Toonivie.Signup.SignupDao;
import com.Toonivie.Slidebanner.SlidebannerDao;
import com.Toonivie.Slidebanner.SlidebannerService;
import com.Toonivie.ToonInfo.ToonInfoDao;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.UserVo;
import com.Toonivie.Vo.VisitVo;
import com.Toonivie.Webtoon2D.Webtoon2DService;
import com.Toonivie.comment.CommentService;
import com.Toonivie.common.CommonService;
import com.Toonivie.series.SeriesDao;
import com.Toonivie.uploadWebtoon.UploadWebtoonDao;
import com.Toonivie.uploadWebtoon.UploadWebtoonService;
import com.Toonivie.viewOrder.ViewOrderService;

@Controller
public class PageLoadController {
	@Autowired
	GrabcutService grabcutService;
	@Autowired
	UploadWebtoonDao dao;
	@Autowired
	PageLoadService ploadservice;
	@Autowired
	CommentService commentService;
	@Autowired
	SlidebannerService service;
	@Autowired
	UploadWebtoonService upservice;
	@Autowired
	ContentViewService CTService;
	@Autowired
	ViewOrderService VOService;
	@Autowired
	Webtoon2DService webtoonservice;
	@Autowired
	ToonInfoDao tooninfodao;
	@Autowired
	CommonService commonservice;
	@Autowired
	MypageService mypageservice;
	@Autowired
	OrderRankService orderservice;
	@Autowired
	SeriesDao seriesDao;
	@Autowired
	CommonService commonService;
	@Autowired
	MemberDao memberDao;
	@Autowired
	SignupDao signupDao;
	
	@Autowired SlidebannerDao slidebannerDao;

	@RequestMapping("/findLoginerr")
	@ResponseBody
	public Integer findLoginerr(HttpServletRequest request) {
		String id = request.getParameter("id");
		UserVo currentUser = signupDao.getUserById(id);
		System.out.println(currentUser.getId() + " ::::");
		int resultVal = -1;
		if (currentUser != null) {
			resultVal = signupDao.isAccountLock(id);
		}
		System.out.println("resultVal: " + resultVal);

		return resultVal;

	}

	@RequestMapping("/myBooks")
	public String myBook(Model model, HttpSession session) {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String viewId = currentUser.getId();
			model.addAttribute("BooksView", VOService.viewAllTop6(viewId));
		}
		return "mybook/mybook";
	}

	@RequestMapping("/signup/auth")
	public String authChoice() {
		return "signup/select/auth";
	}

	@RequestMapping("/webtoon_upload")
	public String webtoon_upload() {
		return "adminPage/webtoon_upload";
	}
	// add by yoo_dh0416 2017-10-19

	@RequestMapping("/")
	public String indexPage(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		model.addAttribute("webmovieRank", orderservice.countOrder());
		model.addAttribute("wonjakRank", orderservice.countOrder_won());
		model.addAttribute("banList", service.BannerMain("webmovie"));
		model.addAttribute("webmovielast", orderservice.lastest());
		model.addAttribute("wonjaklast", orderservice.lastest_won());

		// visit count add ydh 180305
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String ip = req.getHeader("X-FORWARDED-FOR");
		if (ip == null) {
			ip = req.getRemoteAddr();
		}

		int visitip = commonService.getTodayIp(ip);
		if (visitip == 0) {
			VisitVo vo = new VisitVo();
			vo.setVip(ip);
			memberDao.insertIp(vo);
		}
		return "home/index_ranking";
	}
	// today and total visitor count

	@RequestMapping("/menu/visitorcount")
	@ResponseBody
	public Object getcount() {
		int todayCount = signupDao.getVisitTodayCount();
		int totalCount = signupDao.getVisitTotalCount();

		Map<String, Object> retVal = new HashMap<String, Object>();
		retVal.put("totalcount", totalCount);
		retVal.put("todaycount", todayCount);

		return retVal;
	}

	// today and total visitor count end
	@RequestMapping("/logerr")
	public String logerr() {
		return "home/logerr";
	}

	@RequestMapping("/movietoon")
	public ModelAndView indexPage(HttpServletRequest request) throws Exception {
		return commonservice.main(request, "/home/index");
	}

	@RequestMapping("/login")
	public String login(Model model, HttpServletRequest request) {
		String value = "t";
		if (request.getQueryString() == null) {
			value = "f";
		}
		model.addAttribute("login_fail_check", value);
		return "home/login";
	}

	@RequestMapping("/login_findPW")
	public String login_findPW() {
		return "home/login_findPW"; // add by shkwak, 2018-02-08
	}

	@RequestMapping("/adminPage")
	public String adminPage() {
		return "adminPage/adminPage";
	}

	@RequestMapping("/webmovie")
	public String webmovie(@RequestParam(value = "id", required = false) String id, Model model) {
		System.out.println("id:" + id);
		/* model.addAttribute("comment", commentService.list()); */
		model.addAttribute("toon", dao.getContents());
		return "webmovie/webmovie_show";
	}

	@RequestMapping(value = "/webmovie2/{tno}", method = RequestMethod.GET)
	public String webmovie2(@PathVariable int tno, Model model, HttpSession session) throws Exception {
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String viewid = currentUser.getId();
			VOService.insertViewOrder(tno, viewid);
		}
		System.out.println("id:" + tno);
		model.addAttribute("toonInfor", upservice.getContentDetail(tno));
		model.addAttribute("countComment", commentService.countComment(tno));
		model.addAttribute("toon", dao.getContents());
		model.addAttribute("toonsunwi", dao.getContents_sunwi());
		CTService.increaseViewCount(tno);
		model.addAttribute("commentAll", commentService.list(tno));
		// add by yoodh_180207
		String nickname = commonservice.getIdToon(tno);
		model.addAttribute("profile", commonservice.writerprofileToon(nickname));
		try {
			if (!webtoonservice.fileSel(tno).getFree_open() && session.getAttribute("currentUser") == null) {
				return "home/login";
			}
		} catch (Exception e) {

		}

		// nextView and preview_ydh 180226
		int wmno = upservice.getContentDetail(tno).getWmno();
		Date wridate = upservice.getContentDetail(tno).getWridate();
		// menu
		model.addAttribute("wmno", wmno);
		Map<String, String> map = new HashMap<String, String>();
		String queryStr = "tno > " + tno + " AND wmno = " + wmno;
		map.put("queryStr", queryStr);
		map.put("table", "tooninfo");
		map.put("column", "tno");
		// next
		model.addAttribute("nextView", commonservice.getnextAndprev(map));
		// prev
		queryStr = "tno < " + tno + " AND wmno = " + wmno + " order by wridate desc";
		map.clear();
		map.put("queryStr", queryStr);
		map.put("table", "tooninfo");
		map.put("column", "tno");
		model.addAttribute("prevView", commonservice.getnextAndprev(map));

		return "webmovie/webmovie_show_2";
	}

	@RequestMapping("/cv/effect")
	public String cvEffect() {
		grabcutService.init();
		return "mainPage/index";
	}

	// webtoon view Controller
	@RequestMapping("/wonjak")
	public String wonjakadmin(Model model, HttpSession session) {
		model.addAttribute("wonjak", webtoonservice.fileSelAll());
		System.out.println(webtoonservice.fileSelAll().toString());
		model.addAttribute("countwonjak", webtoonservice.countAll());
		model.addAttribute("mySeriesInfowonjak", seriesDao.mySeriesInfoWonjak_noId());
		return "wonjak/wonjak";
	}

	@RequestMapping("/mypage/wonjak")
	public String wonjakall(Model model, HttpSession session) {
		UserVo userVo = (UserVo) session.getAttribute("currentUser");
		String nickname = userVo.getNickname();
		String id = userVo.getId();
		model.addAttribute("wonjak", webtoonservice.fileSeluser(nickname));
		model.addAttribute("mySeriesInfowonjak", seriesDao.mySeriesInfoWonjak(id));
		return "mypage/wonjak";
	}

	@RequestMapping("/mypage/webmovie")
	public String webmovieall(Model model, HttpSession session) {

		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		String kinds = "webmovie";
		model.addAttribute("myToonInfoAll", mypageservice.myTooninfoAll(id));
		model.addAttribute("mySeriesInfoAll", seriesDao.mySeriesInfoMovie(id));
		return "mypage/toon/webtoonList";
	}

	@RequestMapping("/mypage/series")
	public String series(Model model, HttpSession session) {

		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		model.addAttribute("mySeriesInfoAll", mypageservice.mySeriesinfoAll(id));
		return "mypage/series";
	}

	// admin series
	@RequestMapping("/admin/series")
	public String Adminseries(Model model, HttpSession session) {

		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		String id = currentUser.getId();
		model.addAttribute("mySeriesInfoAll", seriesDao.mySeriesInfoMovie_noId());
		return "mypage/series";
	}

	@RequestMapping("/webtoon_wonjak")
	public ModelAndView wonjakmain(HttpServletRequest request) {
		return webtoonservice.wonjakmain(request, "/wonjak/wonjakList");
	}

	@RequestMapping(value = "/webtoon/{wtno}", method = RequestMethod.GET)
	public String wonjakDetail(Model model, @PathVariable int wtno, HttpSession session) {
		model.addAttribute("selToon", webtoonservice.fileSel(wtno));
		model.addAttribute("wonjak", webtoonservice.fileSelAll());
		model.addAttribute("wonjaksunwi", webtoonservice.fileSelAll_sunwi());
		model.addAttribute("countComment", webtoonservice.countComment(wtno));
		String nickname = webtoonservice.getId(wtno);
		model.addAttribute("profile", webtoonservice.writerprofile(nickname));

		webtoonservice.increaseViewCount(wtno);
		try {
			if(!webtoonservice.fileSel(wtno).getFree_open() && session.getAttribute("currentUser") == null) {
				return "home/login";
			}
		}catch(Exception e) {
			
		}
		// nextView and preview_ydh 180226
		int wmno = webtoonservice.fileSel(wtno).getWmno();
		Date wridate = webtoonservice.fileSel(wtno).getWtwridate();
		// menu
		model.addAttribute("wmno", wmno);
		Map<String, String> map = new HashMap<String, String>();
		String queryStr = "wtno > " + wtno + " AND wmno = " + wmno;
		map.put("queryStr", queryStr);
		map.put("table", "webtooninfo");
		map.put("column", "wtno");
		// next
		model.addAttribute("nextView", commonservice.getnextAndprev(map));
		// prev
		queryStr = "wtno < " + wtno + " AND wmno = " + wmno + " order by wtwridate desc";
		map.clear();
		map.put("queryStr", queryStr);
		map.put("table", "webtooninfo");
		map.put("column", "wtno");
		model.addAttribute("prevView", commonservice.getnextAndprev(map));
		return "/wonjak/webtoon_show";
	}

	@RequestMapping("/wonjak/list")
	@ResponseBody
	public ModelAndView index(HttpServletRequest request) {
		return ploadservice.index(request, "/adminPage/wtcommentList");
	}

	// multi language
	// eng
	@RequestMapping("/index_en")
	public ModelAndView index_en(HttpServletRequest request) throws Exception {
		return commonservice.main(request, "/home/index_en");
	}

	/*
	 * @RequestMapping("/index_en") public String index_en(Model model) {
	 * model.addAttribute("toon", dao.getContents());
	 * model.addAttribute("toonsunwi", dao.getContents_sunwi());
	 * model.addAttribute("banList",service.BannerMain()); return "home/index_en"; }
	 */
	@RequestMapping("/login_en")
	public String login_en() {
		return "home/login_en";
	}

	@RequestMapping("/adminPage_en")
	public String adminPage_en() {
		return "adminPage/en/adminPage_en";
	}

	@RequestMapping("/webmovie_en")
	public String webmovie_en(Model model, @RequestParam(value = "id", required = false) String id) {
		System.out.println("en_id:" + id);
		model.addAttribute("toon", dao.getContents());
		return "webmovie/en/webmovie_show_en";
	}

	@RequestMapping("/cv/effect_en")
	public String cvEffect_en() {
		grabcutService.init();
		return "mainPage/index_en";
	}

	@RequestMapping("/cv/effect_en_2")
	public String cvEffect_en_2() {
		grabcutService.init();
		return "mainPage/index_en_2";
	}

	@RequestMapping(value = "/webmovie2_en/{tno}", method = RequestMethod.GET)
	public String webmovie2_en(@PathVariable int tno, Model model) throws Exception {

		System.out.println("en_id:" + tno);
		model.addAttribute("toonInfor", upservice.getContentDetail(tno));
		model.addAttribute("countComment", commentService.countComment(tno));
		model.addAttribute("toon", dao.getContents());
		model.addAttribute("toonsunwi", dao.getContents_sunwi());
		CTService.increaseViewCount(tno);
		// ���������� ������ ��������_������_180207
		String nickname = commonservice.getIdToon(tno);
		System.out.println(nickname);
		model.addAttribute("profile", commonservice.writerprofileToon(nickname));
		return "webmovie/en/webmovie_show_2_en";
	}

	// jp
	@RequestMapping("/index_jp")
	public ModelAndView index_jp(HttpServletRequest request) throws Exception {
		return commonservice.main(request, "/home/index_jp");
	}

	/*
	 * @RequestMapping("/index_jp") public String index_jp(Model model) {
	 * model.addAttribute("toon", dao.getContents());
	 * model.addAttribute("banList",service.BannerMain()); return "home/index_jp"; }
	 */
	@RequestMapping("/login_jp")
	public String login_jp() {
		return "home/login_jp";
	}

	@RequestMapping("/webmovie_jp")
	public String webmovie_jp(Model model, @RequestParam(value = "id", required = false) String id) {
		System.out.println("id:" + id);
		model.addAttribute("toon", dao.getContents());
		return "webmovie/jp/webmovie_show_jp";
	}

	@RequestMapping(value = "/webmovie2_jp/{tno}", method = RequestMethod.GET)
	public String webmovie2_jp(@PathVariable int tno, Model model) throws Exception {

		System.out.println("jp_id:" + tno);
		model.addAttribute("toonInfor", upservice.getContentDetail(tno));
		model.addAttribute("countComment", commentService.countComment(tno));
		model.addAttribute("toon", dao.getContents());
		model.addAttribute("toonsunwi", dao.getContents_sunwi());
		CTService.increaseViewCount(tno);
		// ���������� ������ ��������_������_180207
		String nickname = commonservice.getIdToon(tno);
		model.addAttribute("profile", commonservice.writerprofileToon(nickname));
		return "webmovie/jp/webmovie_show_2_jp";
	}

	// ch
	@RequestMapping("/index_ch")
	public ModelAndView index_ch(HttpServletRequest request) throws Exception {
		return commonservice.main(request, "/home/index_ch");
	}

	/*
	 * @RequestMapping("/index_ch") public String index_ch(Model model) {
	 * model.addAttribute("toon", dao.getContents());
	 * model.addAttribute("banList",service.BannerMain()); return "home/index_ch"; }
	 */
	@RequestMapping("/login_ch")
	public String login_ch() {
		return "home/login_ch";
	}

	@RequestMapping("/webmovie_ch")
	public String webmovie_ch(Model model, @RequestParam(value = "id", required = false) String id) {
		System.out.println("id:" + id);
		model.addAttribute("toon", dao.getContents());
		return "webmovie/ch/webmovie_show_ch";
	}

	@RequestMapping(value = "/webmovie2_ch/{tno}", method = RequestMethod.GET)
	public String webmovie2_ch(@PathVariable int tno, Model model) throws Exception {

		System.out.println("ch_id:" + tno);
		model.addAttribute("toonInfor", upservice.getContentDetail(tno));
		model.addAttribute("countComment", commentService.countComment(tno));
		model.addAttribute("toon", dao.getContents());
		model.addAttribute("toonsunwi", dao.getContents_sunwi());
		CTService.increaseViewCount(tno);
		// ���������� ������ ��������_������_180207
		String nickname = commonservice.getIdToon(tno);
		model.addAttribute("profile", commonservice.writerprofileToon(nickname));
		return "webmovie/ch/webmovie_show_2_ch";
	}

	/*
	 * @RequestMapping(value="/ffmpegConvert",method=RequestMethod.POST)
	 * 
	 * @ResponseBody public HashMap<String,Object>
	 * ffmpegConvert(MultipartHttpServletRequest req,HttpServletRequest request)
	 * throws IOException{ HashMap<String,Object> resultMap=new
	 * HashMap<String,Object>(); MultipartFile file = req.getFile("upfile");
	 * CreateFileUtils cfu = new CreateFileUtils(); String
	 * fileName=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
	 * cfu.CreateFile(file, request, "/resources/cvMov/", fileName); String rootPath
	 * = request.getSession().getServletContext().getRealPath("/"); String
	 * samplingRate = request.getParameter("sampling_rate"); String bitRate =
	 * request.getParameter("bit_rate"); String outputFormat =
	 * request.getParameter("output_format"); String outputFileName =
	 * request.getParameter("ouputfilename");
	 * 
	 * ffmpegConverter ff = new ffmpegConverter(fileName, rootPath, samplingRate,
	 * bitRate, outputFormat, outputFileName); String outputName = ff.convert();
	 * 
	 * resultMap.put("videoUrl", "/resources/cvMov/" + outputName);
	 * 
	 * return resultMap; }
	 */

	@RequestMapping(value = "/upload_singlefile", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> upload_singlefile(MultipartHttpServletRequest req, HttpSession session,
			HttpServletRequest request) throws Exception {
		HashMap<String, String> resultMap = new HashMap<>();
		MultipartFile file = req.getFile("upfile");
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + cfu.getFileType(file.getOriginalFilename());
		cfu.CreateFile(file, request, "/resources/uploadfiles/", filename);
		resultMap.put("resultUrl", filename);
		return resultMap;
	}

	/* toonivie platform v2.0 2018-04-16 psh add */
	@RequestMapping("/test00")
	public String basics() {
		return "Platform/basics";
	}

	@RequestMapping("/test01")
	public String home_page() {
		return "Platform/home_page";
	}

	@RequestMapping("/test02")
	public String webtoonmovie_page() {
		return "Platform/webtoonmovie_page";
	}

	@RequestMapping("/test03")
	public String webtoon_page() {
		return "Platform/webtoon_page";
	}

	@RequestMapping("/test04")
	public String complete_page() {
		return "Platform/complete_page";
	}

	@RequestMapping("/test05")
	public String short_page() {
		return "Platform/short_page";
	}

	@RequestMapping("/test06")
	public String webtoon_view_page() {
		return "Platform/webtoon_list_page";
	}

	@RequestMapping("/test07")
	public String webtoon_view() {
		return "Platform/webtoon_view";
	}

	@RequestMapping("/test08")
	public String webtoonmovie_list_page() {
		return "Platform/webtoonmovie_list_page";
	}

	@RequestMapping("/test09")
	public String webtoonmovie_view() {
		return "Platform/webtoonmovie_view";
	}
}