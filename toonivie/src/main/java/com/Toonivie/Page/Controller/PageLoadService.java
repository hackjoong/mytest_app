package com.Toonivie.Page.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.Toonivie.Webtoon2D.Webtoon2DDao;
import com.Toonivie.Webtoon2D.Webtoon2DService;

@Service
public class PageLoadService {
	@Autowired
	Webtoon2DService wtservice;
	@Autowired
	Webtoon2DDao wtdao;
	
	public ModelAndView index(HttpServletRequest request,String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		//댓글&답글 전체
		model.addObject("allwtcomment", wtservice.list());
		model.addObject("allwtdap", wtservice.listDap());
		
		//댓글&답글 개수
		model.addObject("countwtComm", wtdao.countAllcomment());
		model.addObject("countwtDap", wtdao.countAlldap());


		return model;
	}

}
