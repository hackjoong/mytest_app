package com.Toonivie.PsdParse;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Util.CreateFileUtils;

@Controller
@RequestMapping("/psdParse")
public class PsdParseController {
	@Autowired
	PsdParseService parseService;

	@RequestMapping(value = "/psdUpload", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> psdUpload(MultipartHttpServletRequest req, HttpSession session,
			HttpServletRequest request) throws Exception {
		HashMap<String, Object> resultMap = new HashMap<>();
		MultipartFile file = req.getFile("psd");
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + cfu.getFileType(file.getOriginalFilename());
		cfu.CreateFile(file, request, "/resources/uploadfiles/", filename);
		
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String imgUrl = request.getSession().getServletContext().getRealPath("resources/uploadfiles/")
				+ filename;
		resultMap.put("psd_ThumbPath", parseService.psdMakeThumbnail(imgUrl, rootPath));
		resultMap.put("resultUrl", filename);
		return resultMap;
	}

	@RequestMapping(value = "/psdParseExe", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> psdParseExe(HttpServletRequest request) {
		String imgUrl = request.getSession().getServletContext().getRealPath("resources/uploadfiles/")
				+ request.getParameter("imgUrl");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		return parseService.psdParseExe(imgUrl, rootPath);
	}
}
