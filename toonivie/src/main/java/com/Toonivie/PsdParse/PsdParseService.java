package com.Toonivie.PsdParse;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;
/*
import com.alternativagame.resource.utils.psd.PSDParser;
import com.alternativagame.resource.utils.psd.layer.PSDLayerPixelData;
import com.alternativagame.resource.utils.psd.layer.PSDLayerStructure;
import com.alternativagame.resource.utils.psd.section.PSDLayerAndMask;
*/

import com.alternativagame.resource.utils.psd.PSDParser;
import com.alternativagame.resource.utils.psd.layer.PSDLayerPixelData;
import com.alternativagame.resource.utils.psd.layer.PSDLayerStructure;
import com.alternativagame.resource.utils.psd.section.PSDLayerAndMask;
@Service
public class PsdParseService {

	public HashMap<String, Object> psdParseExe(String imgUrl, String rootPath) {
		HashMap<String, Object> resultMap = new HashMap<>();
		
		//Java ScriptEnging, shkwak, 2017-06-28
		//ScriptEngineManager scriptEngineMgr = new ScriptEngineManager();
		//ScriptEngine jsEngine = scriptEngineMgr.getEngineByName("JavaScript");
		PSDParser parser = null;		//20190806 : psd 데이터 정보 확인
		BufferedImage image = null;		//20190806 : 이미지 저장버퍼
		
		try {
			System.out.println("imgUrl #### "+ imgUrl);
			parser = new PSDParser(new FileInputStream(imgUrl));
		} catch (Exception e) {
			e.printStackTrace();
		}
		PSDLayerAndMask layerAndMask = parser.getLayerAndMask();  //20190806 : 레이어 객체
		
		List<PSDLayerStructure> layers = layerAndMask.getLayers(); //20190806 : layer getter
		List<PSDLayerPixelData> images = layerAndMask.getImageLayers(); //20190806 : imagelayer
		List<HashMap<String,Object>> resultList = new ArrayList<>();
		
		int i = 0;
		
		for (PSDLayerStructure layer : layers) {
			PSDLayerPixelData pixelData = images.get(i);
			image = pixelData.getImage();
			HashMap<String,Object> map = new HashMap<>();
			if (image != null) {
				if(layer.getName().equals("</Layer group>")) {
					continue;
				}
				String name_change = layer.getName().toString();
				name_change = name_change.replaceAll("[:\\\\/%*?!:|\"<>]", "_"+i);
				try{
					ImageIO.write(image, "png", new File(rootPath + "resources/psdimgs/" + name_change + ".png"));
					map.put("imgUrl", "resources/psdimgs/" + name_change  + ".png");
					map.put("width", image.getWidth());
					map.put("height", image.getHeight());
					map.put("layer_name", layer.getName());
					resultList.add(map);
					System.out.println(layer.getName() + "_" + i);
				}catch(Exception e){
				}
			}
			i++;
		}
		resultMap.put("result", resultList);
		return resultMap;
	}

	public String psdMakeThumbnail(String imgUrl, String rootPath) {
		/*
		PSDParser parser = null;
		BufferedImage image, canvas = null;
		
		try {
			parser = new PSDParser(new FileInputStream(imgUrl));
		} catch (Exception e) {
			e.printStackTrace();
		}
		PSDLayerAndMask layerAndMask = parser.getLayerAndMask();
		
		List<PSDLayerStructure> layers = layerAndMask.getLayers();
		List<PSDLayerPixelData> images = layerAndMask.getImageLayers();
		
		int i = 0;
		

 
		Graphics2D Gcanvas;
		layers.size
		for (PSDLayerStructure layer : layers) {
			PSDLayerPixelData pixelData = images.get(i);
			image = pixelData.getImage();
			if (image != null) {
				if(layer.getName().equals("</Layer group>")) {
					continue;
				}
				canvas = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
				String name_change = layer.getName().toString();
				name_change = name_change.replaceAll("[:\\\\/%*?!:|\"<>]", "_"+i);
				try{
					ImageIO.write(image, "png", new File(rootPath + "resources/psdimgs/" + name_change + ".png"));

				}catch(Exception e){
				}
			}
				
				
				
				
				
				
		}
			i++;
	}
	*/
		
		PSDParser parser = null;
		try {
			System.out.println("imgUrl #### "+ imgUrl);
			parser = new PSDParser(new FileInputStream(imgUrl));
		} catch (Exception e) {
			e.printStackTrace();
		}

		PSDLayerAndMask layerAndMask = parser.getLayerAndMask();
		
		List<PSDLayerStructure> layers = layerAndMask.getLayers();
		List<PSDLayerPixelData> images = layerAndMask.getImageLayers();
		String psd_ThumbPath = "/resources/psdimgs/" + "thumbnail_psd" + ".png";
		
		BufferedImage image_merge = null;
		PSDLayerPixelData pixelData = null;
		Point point_xy = new Point(0, 0);
		int i = 0;
		for (PSDLayerStructure layer : layers) {
			try {
				//layer 좌표값 
				point_xy.x = layers.get(i).getBounds().getLeft();
				point_xy.y = layers.get(i).getBounds().getTop();
				
			}catch(Exception e) {
				break;
			}
			if(i==0) {
				pixelData = images.get(0);
			}
			PSDLayerPixelData pixelData_after = images.get(i);
			
			/*
			 * @@이호진
			 * 	이미지가 너무 큰 경우, 임의적으로 크기를 자르는데, 이 때 레이어가 분리되면서 이미지가 null값이 올 수 있음.
			 * 
			 */
			image_merge = pixelData.getImage();
			BufferedImage image_after = pixelData_after.getImage();
			//if(image_after != null) {
			/*	
			try{
					ImageIO.write(image_after, "png", new File(rootPath + "/resources/psdimgs/" + "image_after_init_"+ i + ".png"));
				}catch(Exception e){
					e.printStackTrace();
				}
				*/
				Graphics2D g_before = null;
				try {
					g_before = image_merge.createGraphics();
				}catch(Exception e) {
					System.out.println(e);
					return null;
				}
				g_before.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f));
				g_before.drawImage(image_after, point_xy.x, point_xy.y, null);
//				g_before.drawImage(image_after, image_merge.getWidth()-image_after.getWidth(), image_merge.getHeight()-image_after.getHeight(), null);
				/*
				try{
					ImageIO.write(image_merge, "png", new File(rootPath + "/resources/psdimgs/" + "image_merge_after_"+ i + ".png"));
					ImageIO.write(image_after, "png", new File(rootPath + "/resources/psdimgs/" + "image_after_after_"+ i + ".png"));
				}catch(Exception e){
					e.printStackTrace();
				}
				*/
				g_before.dispose();
				i++;
				int test = 0;
				try {
					test = image_merge.getWidth()-image_after.getWidth();
				}catch(Exception e) {
					System.out.println(e);
					return null;
				}
				if(test<0) {
					psd_ThumbPath = "";
				}
			//}
			
		}
		if (image_merge != null) {
			try{
				ImageIO.write(image_merge, "png", new File(rootPath + "/resources/psdimgs/" + "thumbnail_psd" + ".png"));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return psd_ThumbPath;
	}
}
