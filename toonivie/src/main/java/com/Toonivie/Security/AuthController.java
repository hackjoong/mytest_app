package com.Toonivie.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Toonivie.Signup.SignupDao;
import com.Toonivie.Vo.UserVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

@Controller
public class AuthController {
	@Autowired
	SignupDao userDao;

	@Autowired
	HttpSession session;

	private static final String roleAdmin = "ROLE_ADMIN";
	private static final String roleInventor = "ROLE_INVENTOR";
	private static final String roleWriter = "ROLE_WRITER";
	private static final String roleCoworker  ="ROLE_COWORKER";
	private static final String rolePatientntLawyer = "ROLE_PATIENTENTLAWYER";
	private static final String roleGuest = "anonymousUser";

	@RequestMapping("/authError")
	public String no() {
		return "error/authorityerror";
	}

	@RequestMapping("/beforeLogin.do")
	public String beforeLogin(HttpServletRequest request) {
		return "redirect:/login";
	}

	@RequestMapping("/loginProcess.do")
	public String loginProcess(HttpServletRequest request) {
		try {
			String userId = SecurityContextHolder.getContext().getAuthentication().getName();
			//System.out.println(userId);
			UserVo currentUser = userDao.getUserById(userId);
			if(userDao.isAccountLock(userId)>=5) {
				System.out.println("5번넘음");
				return "redirect:/logerr";
			}
			else {
				if (currentUser == null) {
					return "redirect:/error/notPermit";
				} else {
					session.setAttribute("currentUser", currentUser);
					userDao.UpdateConnDate(userId);
					return "redirect:/";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/";
		}
	}


	@RequestMapping("/logoutProcess.do")
	public String logoutProcess(HttpServletRequest req, HttpServletResponse res) {
		UserVo currentUser = (UserVo) session.getAttribute("currrentUser");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(req, res, auth);
		}
		session.invalidate();
		SecurityContextHolder.clearContext();
		return "redirect:/";

	}
	
	@RequestMapping("/logoutProcess_en.do")
	public String logoutProcess_en(HttpServletRequest req, HttpServletResponse res) {
		UserVo currentUser = (UserVo) session.getAttribute("currrentUser");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(req, res, auth);
		}
		session.invalidate();
		SecurityContextHolder.clearContext();
		return "redirect:/index_en";

	}

	@RequestMapping("/notLogin")
	public String notLogin() {
		return "error/notLogin";
	}

}