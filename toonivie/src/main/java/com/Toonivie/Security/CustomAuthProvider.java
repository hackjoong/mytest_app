package com.Toonivie.Security;


import com.Toonivie.Security.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;


@Service
public class CustomAuthProvider implements AuthenticationProvider {
	@Autowired
	SecAlgorithm secAlgo;
	@Autowired
	UserAuthService userAuthService;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		// auth으에에에에에에에에에
		System.out.println("into authenticate");
		String id = auth.getName();
		String pwd = (String) auth.getCredentials();

		User validUser = userAuthService.loadUserByUsername(id);

		try {
			if (validUser == null)
				throw new BadCredentialsException("에러");
			if (secAlgo.validatePassword(pwd, validUser.getPassword()))
				return new UsernamePasswordAuthenticationToken(validUser, validUser.getPassword(),
						validUser.getAuthorities());
		} catch (Exception e) {
			System.out.println("customProvider ] 에러?)");
		}

		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}