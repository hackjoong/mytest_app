package com.Toonivie.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;


@Configuration
@EnableWebSecurity
public class SecConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("UserAuthService")
	private UserDetailsService UserAuthService;

	@Autowired
	private CustomAuthProvider customAuthProvider;

	@Override
	public void configure(WebSecurity web) throws Exception {
		// static files
		web.ignoring().antMatchers("/resources/**", "/webjars/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.headers()
			.frameOptions()
			.disable()
			.and()
			.formLogin()
			.loginPage("/login")
			.loginProcessingUrl("/login.do")
			.usernameParameter("id")
			.passwordParameter("pw")
			.successForwardUrl("/loginProcess.do")
			.and()
			.logout()
			.and()
			.exceptionHandling()
			.accessDeniedPage("/authError");
		http
			.authorizeRequests()
			.antMatchers("/signup/checkId")
			.permitAll();
		http
			.authorizeRequests()
			.antMatchers("/adminPage")
			.hasRole("ADMIN")
			.and()
			.authorizeRequests()
			.antMatchers("/member/**")
			.hasRole("ADMIN")
			.and()
			.authorizeRequests()
			.antMatchers("/adminPage_en")
			.hasRole("ADMIN")
			.and()
			.authorizeRequests()
			.antMatchers("/writer/**")
			.hasRole("ADMIN")
			.and()
			.authorizeRequests()
			.antMatchers("/cv/effect")
			.hasAnyRole("ADMIN","WRITER","USER","COWORKER");
		
		http.csrf().ignoringAntMatchers("/sceneApply/**");
		http.csrf().ignoringAntMatchers("/grabcut/**");
		http.csrf().ignoringAntMatchers("/sailencyCut/**");
		http.csrf().ignoringAntMatchers("/applyAS/**");
		http.csrf().ignoringAntMatchers("/ffmpegConvert");
		http.csrf().ignoringAntMatchers("/ffmpeg_framepick");
		http.csrf().ignoringAntMatchers("/adminPage/**");
		http.csrf().ignoringAntMatchers("/uploadAudio").ignoringAntMatchers("/uploadVideo").ignoringAntMatchers("/applyTextAndAudio");
		http.csrf().ignoringAntMatchers("/dap/**").ignoringAntMatchers("/comment/**").ignoringAntMatchers("/webtoon/select/**");
		http.csrf().ignoringAntMatchers("/order/**");
		http.csrf().ignoringAntMatchers("/signup/findID").ignoringAntMatchers("/signup/findPW");
		http.csrf().ignoringAntMatchers("/writer/**").ignoringAntMatchers("/members/**");
		http.csrf().ignoringAntMatchers("/webtoon_upload2/**");
		http.csrf().ignoringAntMatchers("/uploadWebtoon/**").ignoringAntMatchers("/menu/**");
		http.csrf().ignoringAntMatchers("/series/**");
		http.csrf().ignoringAntMatchers("/color_trans/**");
		http.csrf().ignoringAntMatchers("/upload/**");
		http.csrf().ignoringAntMatchers("/tempsave").ignoringAntMatchers("/tempload").ignoringAntMatchers("/historylist").ignoringAntMatchers("/signup/certnum");
		http.csrf().ignoringAntMatchers("/signup/certview").ignoringAntMatchers("/findLoginerr");
		http.csrf().ignoringAntMatchers("/ObjectDetection");
		http.csrf().ignoringAntMatchers("/make3D/**");
		http.csrf().ignoringAntMatchers("/MultiApplyObject/**");
	}

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(UserAuthService);

		auth.authenticationProvider(customAuthProvider);
	}



}
