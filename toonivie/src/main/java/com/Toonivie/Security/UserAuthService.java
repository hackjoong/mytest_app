package com.Toonivie.Security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.Toonivie.Signup.SignupDao;
import com.Toonivie.Vo.UserVo;



@Service("UserAuthService")
public class UserAuthService implements UserDetailsService {

	@Autowired
	SignupDao signupDao;
	@Autowired
	SecAlgorithm secAlgo;

	// Return Spring Security User for authentication
	private User buildUserForAuth(UserVo user, List<GrantedAuthority> auths) {
		return new User(user.getId(), user.getPw(), auths);
	}

	// Get authorities name and return spring security authorities type
	private List<GrantedAuthority> buildAuth(String userRole) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userRole));
		return authorities;
	}

	// return User for authentication at AuthenticationManagerBuilder
	@Override
	public User loadUserByUsername(String userid) throws UsernameNotFoundException {
		UserVo userVo = signupDao.getUserById(userid);
		
		if (userVo == null) {
			System.out.println("user is null");
			throw new UsernameNotFoundException(userid);
		}
		else if(userVo != null && userVo.getLogerror()<5) {
			signupDao.logerrorUp(userVo.getId());
			System.out.println("아이디는 있네");
		}

		List<GrantedAuthority> authorities = buildAuth(userVo.getRole());

		User user = buildUserForAuth(userVo, authorities);
		return user;

	}

}
