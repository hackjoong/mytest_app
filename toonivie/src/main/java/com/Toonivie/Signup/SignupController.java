package com.Toonivie.Signup;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Security.SecAlgorithm;
import com.Toonivie.Vo.UserVo;


@Controller
@RequestMapping("/signup")
public class SignupController {
	@Autowired
	SignupService signupService;
	
	@Autowired
	HttpServletResponse response;
	
	@Autowired
	SignupValidate signupValidate;
	
	@Autowired
	SignupDao signupDao;
	
	@RequestMapping("/signup")
	public String signup() {
		return "signup/signup"; 
	}
	
	@RequestMapping("/signup2")
	public String signup2() {
		return "signup/signup2"; 
	}
	
	@RequestMapping("/signup_en")
	public String signup_en() {
		return "signup/signup_en"; 
	}
	
	@RequestMapping("/signup_jp")
	public String signup_jp() {
		return "signup/signup_jp"; 
	}
	
	@RequestMapping("/signup_ch")
	public String signup_ch() {
		return "signup/signup_ch"; 
	}
	
	@RequestMapping("/signupPage")
	public String signupPage() {
		return "signup/signupPage"; 
	}
	
	@RequestMapping("/writer")
	public String signupWriter() {
		return "signup/writer/signupWriter";
	}
	@RequestMapping("/manager")
	public String signupManager() {
		return "signup/manager/signupManager";
	}
	
	@RequestMapping("/checkId")
	@ResponseBody
	public Map<String,Object> checkId(HttpServletRequest request){
		String id = request.getParameter("id");
		return signupService.checkId(id);
	}
	@RequestMapping("/checkNickname")
	@ResponseBody
	public Map<String,Object> checkNickname(HttpServletRequest request){
		String nickname = request.getParameter("nickname");
		return signupService.checkNickname(nickname);
	}
	
	
	@RequestMapping("/insertUser")
	public String insertUser(HttpServletRequest request) {
		String id=request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String role = request.getParameter("role");
		String nickname = request.getParameter("nickname");
		String work = request.getParameter("work");

		String phone = request.getParameter("phone1")+"-"+request.getParameter("phone2")+"-"+request.getParameter("phone3");
		String tel = phone.toString();
		String matching = request.getParameter("jakga");
		return signupService.insertUser(id, pw, name, email, role, nickname, work, tel, matching);
	}
	@RequestMapping("/insertUser_en")
	public String insertUser_en(HttpServletRequest request) {
		String id=request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		return signupService.insertUser_en(id, pw, name, email);
	}
	
	@RequestMapping("/insertUser_jp")
	public String insertUser_jp(HttpServletRequest request) {
		String id=request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		return signupService.insertUser_jp(id, pw, name, email);
	}
	
	@RequestMapping("/insertUser_ch")
	public String insertUser_ch(HttpServletRequest request) {
		String id=request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		return signupService.insertUser_ch(id, pw, name, email);
	}
	
	@RequestMapping(value = "/certnum", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> certnum(@RequestParam("id") String id, @RequestParam("email") String email, HttpServletRequest request, Model model) throws IOException {
		Map<String,Object> resultMap = new HashMap<>();
		id = request.getParameter("id");
		email = request.getParameter("email");
		UserVo currentUser = signupService.certnum(id, email);
		System.out.println(id+","+email+","+currentUser);
		String certNum;
		if (currentUser == null) {
			resultMap.put("message","null");
			resultMap.put("certNum", "null");
			resultMap.put("name", "null");
			return resultMap;
		} else {
			System.out.println(id+"."+email);
			SendEmailUtils sendEmailUtils = new SendEmailUtils();
			String pw = getRamdomCertNum(6);			
			System.out.println("아이디: "+id+", 인증번호"+pw);
			String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(); //get url, add by shkwak, 2018-02-08
			//기존 : http://192.168.0.44:8088/login - 로컬
			String mailContent = "<div>안녕하세요.<br/>"
								+ "투니비입니다.<br/>"
								+ id+"님의 인증번호가 발급되었습니다.<br/><br/>"
								+ "인증번호: " + pw +"<br/><br/>"
								+ "위의 비밀번호를 타이핑하기 힘들경우에는 마우스로 더블클릭한 후 Ctrl+C를 눌러서 복사한후,<br/>"
								+ "인증번호 입력칸에서 Ctrl+V를 눌러서 복사하세요."
								+ "</div>";
			
			sendEmailUtils.sendEmail(currentUser.getEmail(), "[투니비] 인증번호 발급알림", mailContent);
			System.err.println("hashpw"+signupValidate.hashPw(pw));
			certNum = signupValidate.hashPw(pw);
			resultMap.put("message","TRUE");
			resultMap.put("certNum", certNum);
			resultMap.put("name", currentUser.getName());
		}
		return resultMap;
	}
	@RequestMapping(value="/certview", method=RequestMethod.POST)
	@ResponseBody
	public int certnum(HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		String resultNum = request.getParameter("certNum");
		String inputNum = request.getParameter("inputNum");
		
		boolean NumTrue = SecAlgorithm.validatePassword(inputNum, resultNum);
		
		if(NumTrue == true) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	
	//계정찾기
	@RequestMapping("/findAccount")
	public String findAccount() {
		return "find/account";
	}
	@RequestMapping(value = "/findID", method = RequestMethod.POST)
	public String FindID(@RequestParam("name") String name, @RequestParam("email") String email, HttpServletRequest request, Model model) throws IOException {
		name = request.getParameter("name");
		email = request.getParameter("email");
		UserVo currentUser = signupService.checkID(name, email);
		System.out.println(name+","+email+","+currentUser);

		if (currentUser == null) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script>alert('일치하는 정보가 없습니다.');</script>");
			out.flush();
			return "find/account";
		} else {
			System.out.println(name+"."+email);
			model.addAttribute("findid", signupService.findID(name, email));
			
		}
		return "find/id";
	}
	@RequestMapping(value = "/findPW", method = RequestMethod.POST)
	public String findPW(HttpServletRequest request, Model model, UserVo userVo)throws Exception {
		String ChangeID = request.getParameter("id");
		String name = request.getParameter("name");
		//String email = request.getParameter("email");
		String id = ChangeID;
		UserVo currentUser = signupService.checkPW(id, name);
				
		if(currentUser == null) {
			System.err.println("NULL");
			return "find/account";
		} else {
			SendEmailUtils sendEmailUtils = new SendEmailUtils();
			String pw = getRamdomPassword(10);			
			signupService.updatePW(pw, id);
			System.out.println("아이디: "+id+", 임시비밀번호"+pw);
			String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort(); //get url, add by shkwak, 2018-02-08
			//기존 : http://192.168.0.44:8088/login - 로컬
			String mailContent = "<div>안녕하세요.<br/>"
								+ "투니비입니다.<br/>"
								+ id+"님의 임시비밀번호가 발급되었습니다.<br/>"
								+ "확인 후 곧 바로 <a href='" + url + "/login_findPW'>투니비 홈페이지</a>" 
								+ "에 로그인 하셔서 비밀번호를 변경해 주시기 바랍니다.<br/><br/>"
								+ "Password: " + pw +"<br/><br/>"
								+ "위의 비밀번호를 타이핑하기 힘들경우에는 마우스로 더블클릭한 후 Ctrl+C를 눌러서 복사한후,<br/>"
								+ "비밀번호 입력칸에서 Ctrl+V를 눌러서 복사하세요."
								+ "</div>";
			
			sendEmailUtils.sendEmail(currentUser.getEmail(), "[투니비] 임시비밀번호 발급알림", mailContent);
			signupDao.UpdateConnDate(id);
		}
		return "redirect:/login_findPW"; //login -> login_findPW, 2018-02-08 by shkwak
	
	}
	
	public static String getRamdomPassword(int len) {
		char[] charSet = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
				'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

		int idx = 0;
		StringBuffer sb = new StringBuffer();

		System.out.println("charSet.length :::: " + charSet.length);

		for (int i = 0; i < len; i++) {

			idx = (int) (charSet.length * Math.random()); // 36 * 생성된 난수를 Int로 추출 (소숫점제거)
			System.out.println("idx :::: " + idx);
			sb.append(charSet[idx]);
		}

		return sb.toString();
	}
	
	public static String getRamdomCertNum(int len) {
		char[] charSet = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'	};

		int idx = 0;
		StringBuffer sb = new StringBuffer();

		System.out.println("charSet.length :::: " + charSet.length);

		for (int i = 0; i < len; i++) {

			idx = (int) (charSet.length * Math.random()); // 36 * 생성된 난수를 Int로 추출 (소숫점제거)
			System.out.println("idx :::: " + idx);
			sb.append(charSet[idx]);
		}

		return sb.toString();
	}
}