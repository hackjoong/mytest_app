package com.Toonivie.Signup;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.Toonivie.Vo.UserVo;

@Mapper
public interface SignupDao {
	public int checkId(String id);
	public int checkNickname(String nickname);
	public UserVo getUserById(String id);
	
	
	public void insertUser(UserVo userVo);
	
	//인증번호 발급
	public UserVo certnum(UserVo userVo);
	
	//계정 찾기
	public UserVo checkID(UserVo userVo);
	
	public UserVo FindID(@Param("name") String name, @Param("email") String email);
	
	public UserVo FindPW(@Param("id") String id, @Param("name") String name);
	
	public UserVo checkPW(UserVo userVo);

	public int updatePW(UserVo userVo);
	
	//최근 접속일 업데이트 - 로그인 시에만
	public int UpdateConnDate(String userId);
	
	//today visit count
	public int getVisitTodayCount();
	
	//total visit count
	public int getVisitTotalCount();
	
	//login error count UP
	public int logerrorUp(String id);
	
	public int isAccountLock(String id);
}
