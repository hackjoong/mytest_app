package com.Toonivie.Signup;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Security.SecAlgorithm;
import com.Toonivie.Vo.UserVo;


@Service
public class SignupService {
	@Autowired
	SignupDao signupDao;
	@Autowired
	SignupValidate signupValidate;
	
	private static final String roleAdmin = "ROLE_ADMIN";
	private static final String roleInventor = "ROLE_INVENTOR";
	private static final String roleWriter = "ROLE_WRITER";
	private static final String roleCoworker  ="ROLE_COWORKER";
	private static final String rolePatientntLawyer = "ROLE_PATIENTENTLAWYER";
	private static final String roleGuest = "anonymousUser";
	
	public Map<String,Object> checkId(String id){
		Map<String,Object> resultMap = new HashMap<>();
		int userExist = signupDao.checkId(id);
		if(userExist==0) {
			resultMap.put("code", 1);
			resultMap.put("message", "TRUE");
		}else {
			resultMap.put("code", 0);
			resultMap.put("message", "false");
		}
		return resultMap;
	}
	public Map<String,Object> checkNickname(String nickname){
		Map<String,Object> resultMap = new HashMap<>();
		int userExist = signupDao.checkNickname(nickname);
		if(userExist==0) {
			resultMap.put("code", 1);
			resultMap.put("message", "TRUE");
		}else {
			resultMap.put("code", 0);
			resultMap.put("message", "false");
		}
		return resultMap;
	}
	
	public String insertUser(String id,String pw,String name,String email, String role, String nickname, String work, String tel, String matching) {
		if(signupValidate.validateId(id,signupDao.checkId(id))!=1||signupValidate.validateEmail(email)!=1||signupValidate.validateName(name)!=1||signupValidate.validatePw(pw)!=1) {
			return "redirect:/authError";
		}
		String nohashpw = pw;
		pw=signupValidate.hashPw(pw);
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setPw(pw);
		userVo.setName(name);
		userVo.setEmail(email);
		userVo.setRole(role);
		userVo.setNickname(nickname);
		userVo.setWork(work);
		userVo.setTel(tel);
		userVo.setMatching(matching);
		userVo.setNohashpw(nohashpw);
		signupDao.insertUser(userVo);
		return "redirect:/";
	}
	public String insertUser_en(String id,String pw,String name,String email) {
		if(signupValidate.validateId(id,signupDao.checkId(id))!=1||signupValidate.validateEmail(email)!=1||signupValidate.validateName(name)!=1||signupValidate.validatePw(pw)!=1) {
			return "redirect:/error/authError";
		}
		pw=signupValidate.hashPw(pw);
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setPw(pw);
		userVo.setName(name);
		userVo.setEmail(email);
		userVo.setRole("ROLE_USER");
		signupDao.insertUser(userVo);
		return "redirect:/index_en";
	}
	public String insertUser_jp(String id,String pw,String name,String email) {
		if(signupValidate.validateId(id,signupDao.checkId(id))!=1||signupValidate.validateEmail(email)!=1||signupValidate.validateName(name)!=1||signupValidate.validatePw(pw)!=1) {
			return "redirect:/error/authError";
		}
		pw=signupValidate.hashPw(pw);
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setPw(pw);
		userVo.setName(name);
		userVo.setEmail(email);
		userVo.setRole("ROLE_USER");
		signupDao.insertUser(userVo);
		return "redirect:/index_jp";
	}
	public String insertUser_ch(String id,String pw,String name,String email) {
		if(signupValidate.validateId(id,signupDao.checkId(id))!=1||signupValidate.validateEmail(email)!=1||signupValidate.validateName(name)!=1||signupValidate.validatePw(pw)!=1) {
			return "redirect:/error/authError";
		}
		pw=signupValidate.hashPw(pw);
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setPw(pw);
		userVo.setName(name);
		userVo.setEmail(email);
		userVo.setRole("ROLE_USER");
		signupDao.insertUser(userVo);
		return "redirect:/index_ch";
	}
	
	//인증번호 발급
	public UserVo certnum(String id, String email) {
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setEmail(email);

		return signupDao.certnum(userVo);
	}
	
	
	//계정 찾기
	public UserVo checkID(String name, String email) {
		UserVo userVo = new UserVo();
		userVo.setName(name);
		userVo.setEmail(email);

		return signupDao.checkID(userVo);
	}
	
	public UserVo findID(String name, String email) {
		
		return signupDao.FindID(name, email);
	}
	public UserVo findPW(String id, String name) {
		return signupDao.FindPW(id, name);
	}
	
	public UserVo checkPW(String id, String name) {
		UserVo userVo = new UserVo();
		userVo.setId(id);
		userVo.setName(name);		

		return signupDao.checkPW(userVo);
	}

	public int updatePW(String pw, String id) throws Exception {
		pw=signupValidate.hashPw(pw);
		UserVo userVo = new UserVo();
		userVo.setPw(pw);
		userVo.setId(id);
		return signupDao.updatePW(userVo);
	}

}
