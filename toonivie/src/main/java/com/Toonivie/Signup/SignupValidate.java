package com.Toonivie.Signup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.Toonivie.Security.SecAlgorithm;

@Repository
public class SignupValidate {
	@Autowired
	SecAlgorithm secAlgorithm;
	public int validateId(String id,int checkId) {
		//시작은 영문으로만, '_'를 제외한 특수문자 안되며 영문, 숫자, '_'으로만 이루어진 5 ~ 12자 이하
		String regex = "^[a-zA-Z]{1}[a-zA-Z0-9_]{3,11}$";
		//중복된 아이디가 있거나 정규식에 어긋나거나 널 또는 빈 값일시
		if(checkId!=0||!id.matches(regex)||"".equals(id)) {
			return 0;
		}else {
			return 1;
		}
	}
	public int validateEmail(String email) {
		String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
		//이메일이 정규식에 맞지 않거나 빈값 혹은 널일시에
		if(!email.matches(regex)||"".equals(email)) {
			return 0;
		}else {
			return 1;
		}
	}
	public int validateName(String name) {
		//2자 이상
		if("".equals(name)||name.length()<2) {
			return 0;
		}else {
			return 1;
		}
	}
	public int validatePw(String pw) {
		//8자이상, 20자 이하
		if(pw.length()<8||pw.length()>20) {
			return 0;
		}else {
			return 1;
		}
	}
	public String hashPw(String pw) {
		String hashedPw = "";
		try {
			hashedPw=secAlgorithm.createHash(pw);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return hashedPw;
	}
}
