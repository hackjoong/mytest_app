package com.Toonivie.Slidebanner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.ToonInfo.ToonInfoDao;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.BannerVo;
import com.Toonivie.Webtoon2D.Webtoon2DDao;

@Controller
@RequestMapping("/banner")
public class SlidebannerController {
	@Autowired
	SlidebannerService service;
	@Autowired
	HttpServletRequest request;
	@Autowired
	ToonInfoDao ToonInfodao;
	@Autowired
	Webtoon2DDao webtoon2dDao;
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload_tooniviefile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		MultipartFile upfile = req.getFile("upfile");
		String kinds = request.getParameter("kinds");
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = "banner" + cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); 
																												
		cfu.CreateFile(upfile, request, "/resources/uploadbanner/", filename);
		// resultMap.put("resultUrl",filename);

		String b_file = "/resources/uploadbanner/" + filename; // request.getParameter("upfile");
		service.InputBanner(b_file,kinds);
		return filename;
	}
	
	@RequestMapping("/list")
	public String Banner_list(Model model, HttpServletRequest request) throws Exception{
		String kinds = "webmovie";
		model.addAttribute("banner", service.BannerList(kinds));
		model.addAttribute("CountBanner",service.CountBanner(kinds));
		model.addAttribute("nono",ToonInfodao.ToonList());
		return "adminPage/slider";
	}
	@RequestMapping("/list/wonjak")
	public String Banner_list_wonjak(Model model, HttpServletRequest request) throws Exception{
		String kinds = "origintoon";
		model.addAttribute("banner", service.BannerList(kinds));
		model.addAttribute("CountBanner",service.CountBanner(kinds));
		model.addAttribute("nono",webtoon2dDao.fileSelAll());
		return "adminPage/sliderwonjak";
	}
	@RequestMapping("/list_en")
	public String Banner_list_en(Model model, HttpServletRequest request) throws Exception{
		String kinds = "webmovie";
		model.addAttribute("banner", service.BannerList(kinds));
		model.addAttribute("CountBanner",service.CountBanner(kinds));
		return "adminPage/en/slider_en";
	}
	
	@RequestMapping("/delete/{sno}")
	@ResponseBody
	public int delete_tooniviefile(@PathVariable int sno) {
		
		String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
		
		String today = request.getParameter("banner");
		
		File file = new File(rootPath+ today);
		if (file.exists()) {
			file.delete();
		}
		
		return service.DeleteBanner(sno);
	}
	
	@RequestMapping(value = "/order/insert", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> order_insert(HttpServletRequest request) {
		HashMap<String, Object> resultMap = new HashMap<>();
		String mArray = request.getParameter("data");

		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(mArray);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		String sno = jsonObject.get("sno").toString();
		String ono = jsonObject.get("ono").toString();
		String[] nSno = sno.split("_slide");
		String[] nOno = ono.split("");

		BannerVo vo = new BannerVo();
		int returnVal = 8088;
				
		for (int i = 0; i<nOno.length;i++) {
			vo.setSno(Integer.parseInt(nSno[i]));
			vo.setOno(Integer.parseInt(nOno[i]));
			returnVal = service.Order(vo);			
		}
		
		//"data" : "{'sno':'slide_19slide_18slide_17'},{'ono':'132'}"
		
		
		resultMap.put("result", returnVal);
		
		return resultMap;		
	}
	@RequestMapping(value = "/juso/insert", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> juso_insert(HttpServletRequest request) {
		HashMap<String, Object> resultMap = new HashMap<>();
		String mArray = request.getParameter("data");

		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(mArray);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		String sno = jsonObject.get("sno").toString();
		String juso = jsonObject.get("juso").toString();
		String kinds = jsonObject.get("kinds").toString();
		String[] nSno = sno.split("_slide");
		String[] nJuso = juso.split(",");
		String apPath = null;
		if(kinds.equals("origintoon")) {
			apPath = "/webtoon/";
		}
		else if(kinds.equals("webmovie")) {
			apPath = "/webmovie2/";
		}
		BannerVo vo = new BannerVo();
		int returnVal = 8088;
		for (int i = 0; i<nJuso.length;i++) {
			vo.setSno(Integer.parseInt(nSno[i]));
			vo.setJuso(apPath+nJuso[i]);
			returnVal = service.jusoOrder(vo);			
		}
		
		//"data" : "{'sno':'slide_19slide_18slide_17'},{'ono':'132'}"
		
		
		resultMap.put("result", returnVal);
		
		return resultMap;		
	}
}
