package com.Toonivie.Slidebanner;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.BannerVo;

@Mapper
public interface SlidebannerDao {
	
	public void InsertBanner(BannerVo vo);
	
	public int DeleteBanner(int sno);
	
	public List<BannerVo> BannerAll();
	
	public List<BannerVo> BannerList(String kinds);
	
	public List<BannerVo> BannerMain(String kinds);
	
	public int CountBanner(String kinds);

	public int Order(BannerVo vo);
	public int jusoOrder(BannerVo vo);
	
	public int temp();
}
