package com.Toonivie.Slidebanner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.BannerVo;

@Service
public class SlidebannerService {
	
	@Autowired
	SlidebannerDao dao;
	
	public void InputBanner(String b_file, String kinds) {
		BannerVo vo = new BannerVo();
		
		vo.setB_file(b_file);
		vo.setKinds(kinds);
		dao.InsertBanner(vo);
	}
	public List<BannerVo> BannerAll(String kinds){
		return dao.BannerList(kinds);
	}
	
	public List<BannerVo> BannerList(String kinds){
		return dao.BannerList(kinds);
	}
	public List<BannerVo> BannerMain(String kinds){
		return dao.BannerMain(kinds);
	}
	
	
	public int DeleteBanner(int sno) {
		return dao.DeleteBanner(sno);
		
	}
	public int CountBanner(String kinds) {
		return dao.CountBanner(kinds);
	}
	
	public int Order(BannerVo vo) {
		return dao.Order(vo);
		
	}
	public int jusoOrder(BannerVo vo) {
		return dao.jusoOrder(vo);
		
	}


}
