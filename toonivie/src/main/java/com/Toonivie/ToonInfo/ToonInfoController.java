package com.Toonivie.ToonInfo;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.series.SeriesDao;

@Controller
@RequestMapping("/webtoon")
public class ToonInfoController {
	@Autowired
	ToonInfoService service;
	@Autowired
	SeriesDao seriesDao;

	
	@RequestMapping("/list")
	public String webtoon_list(Model model, HttpServletRequest request) throws Exception {
		model.addAttribute("webtoon",service.ToonList());
		model.addAttribute("countToon",service.ToonCount());
		model.addAttribute("mySeriesInfoAll", seriesDao.mySeriesInfoMovie_noId());
		return "adminPage/webtoonList";
	}
	
	@RequestMapping("/list_en")
	public String webtoon_list_en(Model model, HttpServletRequest request) throws Exception {
		model.addAttribute("webtoon",service.ToonList());
		model.addAttribute("countToon",service.ToonCount());
		return "adminPage/en/webtoonList_en";
	}
	
	@RequestMapping("/delete/{tno}")
	@ResponseBody
	public int ToonDelete(@PathVariable int tno, HttpServletRequest request) throws Exception {
		service.CommentDelete(tno);
		int returnVal = service.ToonDelete(tno);
		if(returnVal ==1) {
			String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
			
			String file = request.getParameter("file");
			String thumbnail = request.getParameter("thumbnail");
			
			File file_del = new File(rootPath+ file);
			File thumb_del = new File(rootPath+thumbnail);
			if (file_del.exists()) {
				file_del.delete();
			}
			if (thumb_del.exists()) {
				thumb_del.delete();
			}
			return returnVal;
		}
		else {
			return returnVal;
		}
	}
	

	
	@RequestMapping("/update/{tno}")
	@ResponseBody
	public int ToonUpdate(String file, String genre,String title, String writer, String content) throws Exception{
		ToonVo vo = new ToonVo();
		vo.setFile(file);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setContent(content);
		
		return service.ToonUpdate(vo);
	}

}
