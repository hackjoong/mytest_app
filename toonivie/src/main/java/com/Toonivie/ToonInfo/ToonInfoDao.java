package com.Toonivie.ToonInfo;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.ToonVo;

@Mapper
public interface ToonInfoDao {

	public List<ToonVo> ToonList();

	public int ToonDelete(int tno) throws Exception;
	
	public int CommentDelete(int tno) throws Exception;

	public int ToonCount() throws Exception;
	
	public int ToonUpdate(ToonVo vo) throws Exception;
	
	//���� ��ü
	public List<Map<String,Object>> toonall(Map<String, String> map);
	
	/*����������_������_180207*/
	//�г����� ������ ������ �����ֱ�1 �ܰ�
	public String getIdToon(int wtno);
	//������ ��¥ �����ֱ�
	public String writerprofileToon(String nickname);
	
	//series search and paging
	public int selectSeriesListCount(Map<String, String> map);
	
	//series search and paging
	public int selectSeriesListCountwonjak(Map<String, String> map);
	
	//webmovie search and paging
	public int selectToonListCount(Map<String, String> map);
	public List<Map<String,Object>> getToonList(Map<String, String> map);
	
	//wonjak search and paging
	public int selectWonjakListCount(Map<String, String> map);
	public List<Map<String,Object>> getWonjakList(Map<String, String> map);
	
	public Map<String, Object> getNextAndprev(Map<String,String> map);
	
	public List<ToonVo> getToonWriterList();
	
	public List<ToonVo> getwonwriterList();

}
