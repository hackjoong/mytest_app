package com.Toonivie.ToonInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.ToonVo;

@Service
public class ToonInfoService {
	@Autowired
	ToonInfoDao dao;
	
	

	
	public int ToonDelete(int tno) throws Exception {
		return dao.ToonDelete(tno);
	}
	
	public int ToonCount() throws Exception{
		return dao.ToonCount();
		
	}
	
	public int ToonUpdate(ToonVo vo) throws Exception{
		return dao.ToonUpdate(vo);
	}
	public int CommentDelete(int tno)throws Exception{
		return dao.CommentDelete(tno);
	}

	
	
	public List<ToonVo> ToonList(){

        return dao.ToonList();
	}

}
