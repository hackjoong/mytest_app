package com.Toonivie.Upload.AjaxController;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.ColorTrans.Color_Trans_Module;
import com.Toonivie.Util.CreateFileUtils;



@Controller
@RequestMapping(value="/upload")
public class FileUploadController {

	@RequestMapping(value = "/uploadImg", method = RequestMethod.POST)
	@ResponseBody
    public HashMap<String,Object> testFile(MultipartHttpServletRequest req,HttpSession session, HttpServletRequest request) throws Exception {
    	HashMap<String,Object> resultMap = new HashMap<>();
    	MultipartFile file = req.getFile("img");
    	CreateFileUtils cfu = new CreateFileUtils();
    	String filename=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
    	cfu.CreateFile(file, request, "/resources/cvimgs/", filename);
    	cfu.CreateFile(file, request, "/resources/cvimgs/", "Original_img.png");// add hoo 2018-10-04
    	if(cfu.getFileType(file.getOriginalFilename()).equals("mov")||cfu.getFileType(file.getOriginalFilename()).equals("avi")){
    		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    		VideoCapture videoCapture=new VideoCapture(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+filename);
    		
    		Mat image = new Mat();
    		videoCapture.read(image);
    		System.out.println(image.cols());
    	}else{
    		File imgFile = new File(request.getSession().getServletContext().getRealPath("resources/cvimgs/")+filename);
        	BufferedImage bi = ImageIO.read(imgFile);        	
        	resultMap.put("width", Integer.toString(bi.getWidth()));
        	resultMap.put("height", Integer.toString(bi.getHeight()));
    	}
    	resultMap.put("resultUrl","/resources/cvimgs/"+filename);
    	resultMap.put("filename",filename);
    	
    	//color palette default 5 colors by KH 2018-03-05
    	Color_Trans_Module color_Trans = new Color_Trans_Module();
    	resultMap.put("color_palette", color_Trans.init_color_getpalette(filename, 5, request));
    	
    	return resultMap;
    }
	
	@RequestMapping(value = "/uploadImg_wt", method = RequestMethod.POST)
	@ResponseBody
    public HashMap<String,Object> File_wt(MultipartHttpServletRequest req,HttpServletRequest request) throws Exception {
    	HashMap<String,Object> resultMap = new HashMap<>();
    	MultipartFile file = req.getFile("wt_image");
    	File filepath = new File(request.getServletContext().getRealPath("resources/cvimgs/") + "wt_img/");
        if(!filepath.exists()){
        	filepath.mkdirs(); //디렉토리가 존재하지 않는다면 생성
        }
    	CreateFileUtils cfu = new CreateFileUtils();
    	String filename=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
    	cfu.CreateFile(file, request, "/resources/cvimgs/wt_img/", filename);

		File imgFile = new File(request.getSession().getServletContext().getRealPath("resources/cvimgs/wt_img/")+filename);
    	BufferedImage bi = ImageIO.read(imgFile);
    	resultMap.put("width", Integer.toString(bi.getWidth()));
    	resultMap.put("height", Integer.toString(bi.getHeight()));
	
    	resultMap.put("resultUrl",filename);
    	
    	
    	return resultMap;
    }
	
	@RequestMapping(value = "/ex", method = RequestMethod.POST)
	@ResponseBody
    public HashMap<String,String> exFile(MultipartHttpServletRequest req,HttpSession session, HttpServletRequest request) throws Exception {
    	HashMap<String,String> resultMap = new HashMap<>();
    	MultipartFile file = req.getFile("bigFile");
    	CreateFileUtils cfu = new CreateFileUtils();
    	String filename=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
    	cfu.CreateFile(file, request, "/resources/cvimgs/", filename);
    	resultMap.put("resultUrl",filename);
    	return resultMap;
    }
	
	@RequestMapping("/fileUploadEx")
	public String fileUploadEx(){
		return "mainPage/upLoadEx";
	}
	
	
}
