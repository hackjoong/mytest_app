package com.Toonivie.Util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

public class DataURL_tofile {
	
	public void dataURL_toPNG(String imgUrl, String saveFilePath, String name, HttpServletRequest request) {
		/**
		 * imgbase64 (imgbase64data:image/png;base64,iVBORw0KGgoAA)
		 * saveFilePath 
		 * savename
		 */ 
		CreateFileUtils createFileUtils = new CreateFileUtils();
		PathUtils pathUtils = new PathUtils();
		
/*		String imgbase64 =  request.getParameter("imgUrl");
		String saveFilePath = "src/main/webapp/resources/cvimgs/subtitle";
		String savename = createFileUtils.getToday(1);*/
		String imgbase64 = imgUrl;
//		String savename = createFileUtils.getToday(1);
		String savename = name;
		try {
			// create a buffered image
			BufferedImage image = null;
			
			String[] base64Arr = imgbase64.split(","); // image/png;base64,
			byte[] imageByte = Base64.decodeBase64(base64Arr[1]); // base64 to byte array
			
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
			
			// write the image to a file
			File outputfile = new File(pathUtils.getRootPath(request) + saveFilePath + savename + ".png");
			if (outputfile.exists()) {
				outputfile.delete();
			}
			ImageIO.write(image, "png", outputfile);
			System.out.println(outputfile);	
			
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
