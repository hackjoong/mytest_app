package com.Toonivie.Util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;


public class Ext_API {

	public String input_BG_img (String bg_path, String bg_filename, String rootpath) {
		//making multipartfile
		Path path = Paths.get(bg_path);
		String name = bg_filename;
		String originalFileName = bg_filename;
		String contentType = "image/png";
		CreateFileUtils cfu = new CreateFileUtils();
		String result_name = null;

		byte[] content = null;
		try {
			content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}

		MultipartFile file = new MockMultipartFile(name, originalFileName, contentType, content);
		byte[] result;
		System.out.println("test 4channel file chk "+file.getOriginalFilename());
		RestTemplate restTemplate = new RestTemplate();
		LinkedMultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();

		URI uri = null;
		try {
			uri = new URI("http://192.168.0.158:3000/upload");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		try {
			mvm.add("upfile", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
		} catch (IOException e) {
			e.printStackTrace();
		} // MultipartFile
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(mvm, headers);

		result = restTemplate.postForObject(uri, requestEntity, byte[].class);

		try {
			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(result));
			File ResultDir = new File(rootpath+"/inpainting/");
			if(ResultDir.exists()==false) {
				ResultDir.mkdirs();
			}
			result_name = cfu.getToday(0)+"inpainting.png";
			ImageIO.write(bufferedImage, "png", new File(rootpath+"/inpainting/"+result_name));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result_name;
	}
	
	public Map<String, Object> paint_chainer(String img_path, String img_filename, String rootpath) {
			//making multipartfile
			Path path = Paths.get(img_path);
			String name = img_filename;
			String originalFileName = img_filename;
			String contentType = "image/png";
			CreateFileUtils cfu = new CreateFileUtils();
			String result_name = null;
			String input_id = "TIKP3DYNGS1W0E2QC7GKTNHP0VUECLL2";
			Map<String, Object> resultMap = new HashMap<String, Object>();

			byte[] content = null;
			try {
				content = Files.readAllBytes(path);
			} catch (final IOException e) {
			}

			MultipartFile file = new MockMultipartFile(name, originalFileName, contentType, content);
			byte[] result;
			
			RestTemplate restTemplate = new RestTemplate();
			LinkedMultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
			URI uri = null;
			try {
//				uri = new URI("http://192.168.0.216:8000/upload");
				uri = new URI("http://192.168.0.158:3000/uploadsketch");
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			try {
//				mvm.add("line", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
				//mvm.add("id", input_id);
				mvm.add("upfile", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
			} catch (IOException e) {
				e.printStackTrace();
			} // MultipartFile
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(mvm, headers);

			result = restTemplate.postForObject(uri, requestEntity, byte[].class);

			try {
				BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(result));
				File ResultDir = new File(rootpath+"/coloring/");
				if(ResultDir.exists()==false) {
					ResultDir.mkdirs();
				}
				result_name = cfu.getToday(0)+"coloring.png";
				ImageIO.write(bufferedImage, "png", new File(rootpath+"/coloring/"+result_name));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Mat im = Imgcodecs.imread(rootpath+"/coloring/"+result_name);
			resultMap.put("resultUrl",  "/resources/cvimgs/coloring/"+ result_name);
			resultMap.put("width", im.cols());
			resultMap.put("height", im.rows());
			
			return resultMap;
	}
	
}

class MultipartInputStreamFileResource extends InputStreamResource {
	
	private final String filename;
	
	MultipartInputStreamFileResource(InputStream inputStream, String filename) {
		super(inputStream);
		this.filename = filename;
	}
	
	@Override
	public String getFilename() {
		return this.filename;
	}
	
	@Override
	public long contentLength() throws IOException {
		return -1; 
	}
}