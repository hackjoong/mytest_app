package com.Toonivie.Util;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public class ImageTypeConvert {
	Mat alpha;
	Mat tempVar;
	Mat convertMat;
	Mat bgrMat;
	List<Mat> bgraMatList = new ArrayList<>();		
	List<Mat> alphaList = new ArrayList<>();
	public ImageTypeConvert(int width, int height) {
		this.convertMat = new Mat(height,width, CvType.CV_64FC3);	
		this.convertMat.setTo(new Scalar(255, 255, 255));			// alpha -> white(255) or // alpha -> black(0)
		this.alpha = convertMat.clone();							//	alpha of bgra
		this.tempVar = convertMat.clone(); 							//	255 - alpha = tempVar
		this.bgrMat = convertMat.clone();
	}
	
	public Mat bgraTobgr(Mat BGRAMat) {
		if(BGRAMat.channels() == 4) {
			/*
				((255 - alpha) / 255) * BGRMat.r + (alpha / 255) * BGRAMat.r,
				((255 - alpha) / 255) * BGRMat.g + (alpha / 255) * BGRAMat.g,
				((255 - alpha) / 255) * BGRMat.b + (alpha / 255) * BGRAMat.b
			 */
			
			bgrMat.create(BGRAMat.height(), BGRAMat.width(), CvType.CV_64FC3);
			
			//	bgraMat -> b, g, r, alpha
			Core.split(BGRAMat, bgraMatList);	
			
			//	alpha data -> Mat
			alphaList.add(bgraMatList.get(3));
			alphaList.add(bgraMatList.get(3));
			alphaList.add(bgraMatList.get(3));
			Core.merge(alphaList, alpha);
			alphaList.clear();
			alpha.convertTo(alpha, CvType.CV_64FC3);
			
			//	bgra -> bgr
			bgraMatList.remove(3);
			Core.merge(bgraMatList, BGRAMat);
			bgraMatList.clear();
			BGRAMat.convertTo(BGRAMat, CvType.CV_64FC3);
			
			//	((255 - alpha) / 255) * BGRMat + (alpha / 255) * BGRAMat --- calculate
			Core.subtract(convertMat, alpha, tempVar);		//	(1 - alpha) = tempVar
			Core.divide(tempVar, convertMat, tempVar);		//	tempVar / 255 = tempVar
			Core.multiply(tempVar, bgrMat, bgrMat);			//	tempVar * bgrMat = bgrMat
			tempVar.release();
			Core.divide(alpha, convertMat, alpha);			//	alpha / 255			
			Core.multiply(alpha, BGRAMat, BGRAMat);			//	alpha * bgraMat
			alpha.release();			
			Core.add(bgrMat, BGRAMat, BGRAMat);				//	bgrMat + bgraMat = returMat
			BGRAMat.convertTo(BGRAMat, CvType.CV_8UC3);		//	video write 8bit(64bit error check)
			
		}
		return BGRAMat;
	}

}
