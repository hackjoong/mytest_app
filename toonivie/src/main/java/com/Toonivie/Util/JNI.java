package com.Toonivie.Util;


public class JNI {
	static String rootpath;
	static {
        System.loadLibrary("PutText");
       
    }
	
	public JNI(String rootPath) {
		this.rootpath = rootPath;
	}

    
	private native void puttext(String img_path, long img_mat, String font_path, int font_size, String font_txt, int font_position_x, int font_position_y, int font_color_r, int font_color_g, int font_color_b, int font_thickness);
    //private native long puttext(long img_mat, String font_path, int font_size, String font_txt, int font_position_x, int font_position_y, int font_color_r, int font_color_g, int font_color_b, int font_thickness);

    public void init_puttext(String img_path, long img_mat, String font_path, int font_size, String font_txt, int font_position_x, int font_position_y, int font_color_r, int font_color_g, int font_color_b, int font_thickness) {
    	System.out.println("-----------------JNI----------------------");
    	System.out.println(img_path);
    	System.out.println(img_mat);
    	System.out.println(font_path);
    	System.out.println(font_size);
    	System.out.println(font_txt);
    	System.out.println(font_position_x);
    	System.out.println(font_position_y);
    	System.out.println(font_color_r);
    	System.out.println(font_color_g);
    	System.out.println(font_color_b);
    	System.out.println(font_thickness);
    	puttext(img_path, img_mat, font_path, font_size, font_txt, font_position_x, font_position_y, font_color_r, font_color_g, font_color_b, font_thickness);
    }
}
