package com.Toonivie.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class PageNavigator { //
	private String root;
	private boolean nowFirst; //
	private boolean nowEnd; // 
	private int totalArticleCount; // 
	private int newArticleCount; // 
	private int totalPageCount; //total page count
	private int pageNo; //now page
	private String navigator;
	private int pageSize;
	private int viewListSize;
	Map<String, String> pageMap;

	//
	public PageNavigator(int pageNo, int totalArticleCount, String root, int pageSize, int viewListSize){
		pageMap = new HashMap<String,String>();
		this.totalArticleCount = totalArticleCount;
		this.pageNo = pageNo;
		this.root = root;
		this.pageSize = pageSize;
		this.viewListSize = viewListSize;
		setPaganationMap(pageNo);
		this.setNavigator();
	}
	public PageNavigator(String pageNo, int totalArticleCount, String root, int pageSize, int viewListSize){
		pageMap = new HashMap<String,String>();
		this.totalArticleCount = totalArticleCount;
		int temp_pageno =0;
		if(pageNo == null || "".equals(pageNo)) {
			temp_pageno = 1;
		}else {
			temp_pageno = Integer.parseInt(pageNo);
		}
		this.pageNo = temp_pageno;
		this.root = root;
		this.pageSize = pageSize;
		this.viewListSize = viewListSize;
		setPaganationMap(temp_pageno);
		this.setNavigator();
	}
	public void setPaganationMap(int pageNo){

		int start = ( (pageNo-1) * viewListSize );
		int end = viewListSize;
		pageMap.put("start_index", start+"");
		pageMap.put("end_index", end+"");
	}
	public Map<String, String> getPageMap( Map<String, String> paramterMap){
		for(Entry<String,String> entry :paramterMap.entrySet()){
			pageMap.put(entry.getKey(), entry.getValue());
		}
		return this.pageMap;
	}
	public Map<String, String> getPageMap(){
		return this.pageMap;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public boolean isNowFirst() {
		return nowFirst;
	}
	public void setNowFirst(boolean nowFirst) {
		this.nowFirst = nowFirst;
	}
	public boolean isNowEnd() {
		return nowEnd;
	}
	public void setNowEnd(boolean nowEnd) {
		this.nowEnd = nowEnd;
	}
	public int getTotalArticleCount() {
		return totalArticleCount;
	}
	public void setTotalArticleCount(int totalArticleCount) {
		this.totalArticleCount = totalArticleCount;
	}
	public int getNewArticleCount() {
		return newArticleCount;
	}
	public void setNewArticleCount(int newArticleCount) {
		this.newArticleCount = newArticleCount;
	}
	public int getTotalPageCount() {
		return totalPageCount;
	}
	public void setTotalPageCount(int totalPageCount) {
		this.totalPageCount = totalPageCount;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public String getNavigator() {
		return navigator;
	}
	public void setNavigator() {
		//logic desc - delete by shkwak, 2018-02-22		
		StringBuffer tmpNavigator = new StringBuffer();
		int totalPageCount;
		//jsp
		String javascriptFunctionName="javascript:listarticle";
		/*String javascriptFunctionName="";*/

		//css  <ul>class
		String defaultClassUl="pagenation-ul";

		//css <li>class (non active css)
		String disabledClassLi="nation_disabled";

		//css <li>class (active css)
		String activeClassLi="dn";


		//basic <a>class
		String defaultClassA="";
		
		//
		if(totalArticleCount % viewListSize == 0){
			totalPageCount = totalArticleCount / viewListSize;
			
		}else{
			totalPageCount = totalArticleCount / viewListSize+1;
		}

		int prePage = (pageNo -1)/ pageSize * pageSize;
		//[prev]

		//
		if( pageNo <= pageSize  ){
			disabledClassLi="dn";
		}else{
			disabledClassLi="";
		}

		int startPage = prePage + 1;
		int endPage = prePage + pageSize;
		if(endPage > totalPageCount){
			endPage = totalPageCount;
		}
		pageNo = pageNo -1;
		tmpNavigator.append(" <ul class='"+defaultClassUl+"'> \n");
		tmpNavigator.append(" 	<li class='"+disabledClassLi+" cp' onclick=\"location.href='"+javascriptFunctionName+"(1)'\"><a class='"+defaultClassA+"'><span> << </span></a></li> \n");
		tmpNavigator.append(" 	<li class='"+disabledClassLi+" cp' onclick=\"location.href='"+javascriptFunctionName+"("+pageNo+")'\"><a class='"+defaultClassA+"'><span> <  </span></a></li> \n");
		pageNo = pageNo +1;
		//
		for (int i = startPage; i <= endPage; i++) {
			if (pageNo == i) {
				activeClassLi = "nation_active";
			} else {
				activeClassLi = " ";
			}
			tmpNavigator.append(" 	<li class='"+activeClassLi+" cp' onclick=\"location.href='"+javascriptFunctionName+"("+i+")'\"><a class='"+defaultClassA+"'><span>"+i+"</span></a></li> \n");
		}

		//
		int nextPage =  ( ( ( ( pageNo -1 ) / pageSize ) + 1 ) * pageSize ) +1;

		double num_count;
		// ydh_180129)
		if(pageNo % pageSize == 0) {
			num_count  = (pageNo/5);
		}
		else {
			num_count  = (pageNo/5)+0.1;
		}
		
		// [next, end] active/non select
		if((int)(Math.ceil(num_count)*pageSize) >= totalPageCount) {
			disabledClassLi ="dn";
		}
		else if(( ( pageNo -1 ) / pageSize )-1 < (totalPageCount/pageSize)-1){
			disabledClassLi="";
		}
		else{

			disabledClassLi="dn";
		}

		
		pageNo = pageNo +1;
		tmpNavigator.append(" 	<li class='"+disabledClassLi+" cp' onclick=\"location.href='"+javascriptFunctionName+"("+pageNo+")'\"><a class='"+defaultClassA+"'><span> >  </span></a></li> \n");
		tmpNavigator.append(" 	<li class='"+disabledClassLi+" cp' onclick=\"location.href='"+javascriptFunctionName+"("+nextPage+")'\"><a class='"+defaultClassA+"'><span> >> </span></a></li> \n");
		tmpNavigator.append(" </ul> ");
		pageNo = pageNo -1;

		//script add
		this.navigator = tmpNavigator.toString();
	}
}