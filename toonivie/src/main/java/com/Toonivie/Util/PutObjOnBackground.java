package com.Toonivie.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class PutObjOnBackground {

	public Mat setBackMat(int initX, int initY, Mat background, Mat object) {

		Mat backgroundRoi = new Mat();
		Mat mask = new Mat();
		Mat objectClone = object.clone();
		
		/* 4channel -> 3channel */
		if(background.channels() != object.channels()) {
			if(background.channels() < 4) {
				Imgproc.cvtColor(background, background, Imgproc.COLOR_BGR2BGRA, 4);
			}
			if(object.channels() < 4) {
				Imgproc.cvtColor(objectClone, objectClone, Imgproc.COLOR_BGR2BGRA, 4);
			}
		}
		/* 4channel -> 1channel */
		List<Mat> maskList = new ArrayList<>();
		Core.split(object, maskList);
		mask = maskList.get(maskList.size()-1);
		
		/* Gradation Binarization  */
		//Imgproc.threshold(mask,mask,127,255,Imgproc.THRESH_TOZERO);
		
		Rect cropRect = new Rect();
		int cropRectWidth = 0, cropRectHeight = 0, cropRectX = 0, cropRectY = 0, backgroundRoiX = 0, backgroundRoiY = 0;
		/*		x < 0, y < 0 Area		 */
		if(initX < 0 && initY < 0 && 0 < initX + object.width() && 0 < initY + object.height()) {
			
			if(background.width() < initX + object.width()) {
				cropRectWidth = background.width();
			}else {
				cropRectWidth = object.width() - Math.abs(initX);
			}
			if(background.height() < initY + object.height()) {
				cropRectHeight = background.height();
			}else {
				cropRectHeight = object.height() - Math.abs(initY);
			}
			cropRectX = Math.abs(initX);
			cropRectY = Math.abs(initY);
			backgroundRoiX = 0;
			backgroundRoiY = 0;
			
		}else if(initX < 0 && 0 < initX + object.width() && 0 < initY && initY < background.height()) {
			/*		x < 0, 0 < y Area		 */
			if(background.width() < initX + object.width()) {
				cropRectWidth = background.width();
			}else {
				cropRectWidth = object.width() - Math.abs(initX);
			}
			if(background.height() < initY + object.height()) {
				cropRectHeight = background.height() - Math.abs(initY); 
			}else {
				cropRectHeight = object.height();
			}
			cropRectX = Math.abs(initX);
			cropRectY = 0;
			backgroundRoiX = 0;
			backgroundRoiY = initY;
		}else if(0 < initX && initX < background.width() && initY < 0 && 0 < initY + object.height()) {
			/*		0 < x, y < 0 Area		 */
			if(background.width() < initX + object.width()) {
				cropRectWidth = background.width() - Math.abs(initX); 
			}else {
				cropRectWidth = object.width();
			}
			if(background.height() < initY + object.height()) {
				cropRectHeight = background.height();
			}else {
				cropRectHeight = object.height() - Math.abs(initY);
			}
			cropRectX = 0;
			cropRectY = Math.abs(initY);
			backgroundRoiX = initX;
			backgroundRoiY = 0;
			
		}else if(0 <= initX && 0 <= initY && initX < background.width() && initY < background.height()) {
			/*		0 < x, 0 < y Area		 */
			if(background.width() < initX + object.width()) {
				cropRectWidth = background.width() - Math.abs(initX); 
			}else {
				cropRectWidth = object.width();
			}
			if(background.height() < initY + object.height()) {
				cropRectHeight = background.height() - Math.abs(initY); 
			}else {
				cropRectHeight = object.height();
			}
			cropRectX = 0;
			cropRectY = 0;
			backgroundRoiX = initX;
			backgroundRoiY = initY;
		}	
		
		cropRect = new Rect(cropRectX, cropRectY, cropRectWidth, cropRectHeight);
		mask = mask.submat(cropRect);
		objectClone = objectClone.submat(cropRect);
		backgroundRoi = new Mat(background, new Rect(backgroundRoiX, backgroundRoiY, cropRectWidth, cropRectHeight));
		objectClone.copyTo(backgroundRoi, mask);
		return background;
	}
	
public Mat setBackMat(List<Map<String,Object>> others, Mat im2) {//fore Mat�� clone�븳 寃껋쓣 �꽔�쓣寃�!
		Mat im = im2.clone();
		int i = 0;
		for(Map<String, Object> vo : others){
			int initX = Math.round((float) vo.get("left"));
			int initY = Math.round((float)vo.get("top"));
			Mat fore = new Mat((long) vo.get("mat"));
			//int initX, int initY, Mat im, Mat fore
			Mat imageRoi;
			int rectWidth = fore.cols();
			int rectHeight = fore.rows();
			Mat mask = null;
			Mat parameterPerson = fore.clone();
			Mat parameterPersonClone = parameterPerson.clone();
			Imgproc.cvtColor(parameterPerson, parameterPerson, Imgproc.COLOR_BGRA2BGR);
			//Imgproc.cvtColor(parameterPerson, parameterPerson, Imgproc.COLOR_RGBA2RGB);
			List<Mat> personMatList = new ArrayList<>();
			//Mat person = parameterPerson.clone();
			//add apha channel byKH 180419
			Imgproc.cvtColor(parameterPersonClone, parameterPersonClone, Imgproc.COLOR_BGR2BGRA);
			Core.split(parameterPersonClone, personMatList);
			
			//why 3?
			//mask = personMatList.get(3);
			mask = personMatList.get(personMatList.size()-1);
			
			if (initX + rectWidth <= 0) {
			} else if (initX < 0 && initX + rectWidth > 0) {
				if (initY < 0 && initY + rectHeight < 0) {
				} else if (initY >= im.rows()) {
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), Math.abs(initY), rectWidth - Math.abs(initX),
							rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, 0, rectWidth - Math.abs(initX), rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, initY, rectWidth - Math.abs(initX), im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Rect rectCrop = new Rect(Math.abs(initX), 0, rectWidth - Math.abs(initX), rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(0, initY, rectWidth - Math.abs(initX), rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
				}
			} else if (initX > 0 && initX < im.cols() && initX + rectWidth > im.cols()) {
				if (initY < 0 && initY + rectHeight < 0) {
				} else if (initY >= im.rows()) {
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), im.cols() - initX, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, im.cols() - initX, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, im.cols() - Math.abs(initX), im.rows() - Math.abs(initY));
	
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
				} else {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Rect rectCrop = new Rect(0, 0, im.cols() - initX, rectHeight);
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, im.cols() - initX, rectHeight));
					cropImage.copyTo(imageRoi, cropMask);
				}
			} else if (initX >= im.cols()) {
			} else {
				if (initY < 0 && initY + rectHeight <= 0) {
				} else if (initY >= im.rows()) {
				} else if (initY < 0 && initY + rectHeight > 0) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Rect rectCrop = new Rect(0, Math.abs(initY), rectWidth, rectHeight - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, 0, rectWidth, rectHeight - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					return im;
				} else if (initY > 0 && initY < im.rows() && initY + rectHeight > im.rows()) {
					Mat maskClone = mask.clone();
					Mat personClone = parameterPerson.clone();
					Rect maskCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Rect rectCrop = new Rect(0, 0, rectWidth, im.rows() - Math.abs(initY));
					Mat cropMask = maskClone.submat(maskCrop);
					Mat cropImage = personClone.submat(rectCrop);
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, im.rows() - Math.abs(initY)));
					cropImage.copyTo(imageRoi, cropMask);
					return im;
				} else {
					imageRoi = new Mat(im, new Rect(initX, initY, rectWidth, rectHeight));
					parameterPerson.copyTo(imageRoi, mask);
					
				}
			}
			
		}
		return im;
	}
	public void resize_mat(String img_url, float height, float width) {
		System.out.println("resizeMAT" + img_url);
		if(width == 0.0 && height == 0.0) {
			return;
		}
		Mat obj = Imgcodecs.imread(img_url, -1);
		try {
			Size size = new Size(width, height);
			Imgproc.resize(obj, obj, size);
			Imgcodecs.imwrite(img_url, obj);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
