package com.Toonivie.Vo;

public class AttachVideoVo {
	String videoUrl;
	String videoWidth; //add by shkwak, 2017-06-19
	String videoHeight; //add
	int time;
	String openEffect;
	String closeEffect;
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getVideoWidth() { //add
		return videoWidth;
	}
	public void setVideoWidth(String videoWidth) { //add
		this.videoWidth = videoWidth;
	}
	public String getVideoHeight() { //add
		return videoHeight;
	}
	public void setVideoHeight(String videoHeight) { //add
		this.videoHeight = videoHeight;
	}	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getOpenEffect() {
		return openEffect;
	}
	public void setOpenEffect(String openEffect) {
		this.openEffect = openEffect;
	}
	public String getCloseEffect() {
		return closeEffect;
	}
	public void setCloseEffect(String closeEffect) {
		this.closeEffect = closeEffect;
	}
	
}
