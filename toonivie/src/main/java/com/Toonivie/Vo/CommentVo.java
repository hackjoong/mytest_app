package com.Toonivie.Vo;

import java.util.Date;

public class CommentVo {

	int cno;
	int tno;
	String comment;
	String nickname;
	String pw;
	Date cdate;
	String dapCount;

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public int getTno() {
		return tno;
	}

	public void setTno(int tno) {
		this.tno = tno;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public String getDapCount() {
		return dapCount;
	}

	public void setDapCount(String dapCount) {
		this.dapCount = dapCount;
	}

}
