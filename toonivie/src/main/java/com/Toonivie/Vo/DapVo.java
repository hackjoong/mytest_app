package com.Toonivie.Vo;

import java.util.Date;

public class DapVo {
	
	int dno;
	int cno;
	String content;
	String dwriter;
	Date ddate;
	String pwd;
	
	public int getDno() {
		return dno;
	}
	public void setDno(int dno) {
		this.dno = dno;
	}
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getDwriter() {
		return dwriter;
	}
	public void setDwriter(String dwriter) {
		this.dwriter = dwriter;
	}
	public Date getDdate() {
		return ddate;
	}
	public void setDdate(Date ddate) {
		this.ddate = ddate;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
