package com.Toonivie.Vo;

public class GenreVo {
	int genreSeq;
	int genreName;
	public int getGenreSeq() {
		return genreSeq;
	}
	public void setGenreSeq(int genreSeq) {
		this.genreSeq = genreSeq;
	}
	public int getGenreName() {
		return genreName;
	}
	public void setGenreName(int genreName) {
		this.genreName = genreName;
	}
	
}
