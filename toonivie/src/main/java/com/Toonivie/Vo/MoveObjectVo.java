package com.Toonivie.Vo;

import org.json.simple.JSONObject;

public class MoveObjectVo {
	String imgUrl;
	float left;
	float top;
	float degree; //2017-12-15 add by. KH
	String imgWidth; //add by shkwak, 2017-07-12
	String imgHeight; //add
	float Height;
	float Width;
	String effectKind; 
	float effectTime; 
	String info;
	MoveObjectVo backgroundVo;
	JSONObject background;


	
	@Override
	public String toString() {
		return "MoveObjectVo [imgUrl=" + imgUrl + ", left=" + left + ", top=" + top + ", degree=" + degree
				+ ", imgWidth=" + imgWidth + ", imgHeight=" + imgHeight + ", Height=" + Height + ", Width=" + Width
				+ ", effectKind=" + effectKind + ", effectTime=" + effectTime + ", info=" + info + ", backgroundVo="
				+ backgroundVo + ", background=" + background + "]";
	}
	public float getDegree() {
		return degree;
	}
	public void setDegree(float degree) {
		this.degree = degree;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public float getLeft() {
		return left;
	}
	public void setLeft(float left) {
		this.left = left;
	}
	public float getTop() {
		return top;
	}
	public void setTop(float top) {
		this.top = top;
	}
	public String getImgWidth() { //add by shkwak, 2017-07-12
		return imgWidth;
	}
	public void setImgWidth(String imgWidth) { //add
		this.imgWidth = imgWidth;
	}
	public String getImgHeight() { //add
		return imgHeight;
	}
	public void setImgHeight(String imgHeight) { //add
		this.imgHeight = imgHeight;
	}
	public float getHeight() {
		return Height;
	}
	public void setHeight(float height) {
		Height = height;
	}
	public float getWidth() {
		return Width;
	}
	public void setWidth(float width) {
		Width = width;
	}
	public String getEffectKind() {
		return effectKind;
	}
	public void setEffectKind(String effectKind) {
		this.effectKind = effectKind;
	}
	public float getEffectTime() {
		return effectTime;
	}
	public void setEffectTime(float effectTime) {
		this.effectTime = effectTime;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public MoveObjectVo getBackgroundVo() {
		return backgroundVo;
	}
	public void setBackgroundVo(MoveObjectVo backgroundVo) {
		this.backgroundVo = backgroundVo;
	}
	public JSONObject getBackground() {
		return background;
	}
	public void setBackground(JSONObject background) {
		this.background = background;
	}
	
	
}
