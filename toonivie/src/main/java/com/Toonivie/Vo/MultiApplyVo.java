package com.Toonivie.Vo;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opencv.core.Mat;

public class MultiApplyVo {

	//입력변수
	String effectKind; 
	float effectTime; 
	JSONObject background; 
	JSONObject obj; 
	String rootPath; 
	JSONArray otherObj; 
	//변환변수
	MoveObjectVo objVo;
	MoveObjectVo backgroundVo;
	List<MoveObjectVo> otherList;
	int rotateVector;
	int biggerVector;
	Mat im2; //배경 매트릭스
	
	@Override
	public String toString() {
		return "MultiApplyVo [effectKind=" + effectKind + ", effectTime=" + effectTime + ", background=" + background
				+ ", obj=" + obj + ", rootPath=" + rootPath + ", otherObj=" + otherObj + ", objVo=" + objVo
				+ ", backgroundVo=" + backgroundVo + ", otherList=" + otherList + ", rotateVector=" + rotateVector
				+ ", biggerVector=" + biggerVector + ", im2=" + im2 + "]";
	}
	public Mat getIm2() {
		return im2;
	}
	public void setIm2(Mat im2) {
		this.im2 = im2;
	}
	public String getEffectKind() {
		return effectKind;
	}
	public void setEffectKind(String effectKind) {
		this.effectKind = effectKind;
	}
	public MoveObjectVo getObjVo() {
		return objVo;
	}
	public void setObjVo(MoveObjectVo objVo) {
		this.objVo = objVo;
	}
	public MoveObjectVo getBackgroundVo() {
		return backgroundVo;
	}
	public void setBackgroundVo(MoveObjectVo backgroundVo) {
		this.backgroundVo = backgroundVo;
	}
	public List<MoveObjectVo> getOtherList() {
		return otherList;
	}
	public void setOtherList(List<MoveObjectVo> otherList) {
		this.otherList = otherList;
	}
	public int getRotateVector() {
		return rotateVector;
	}
	public void setRotateVector(int rotateVector) {
		this.rotateVector = rotateVector;
	}
	public int getBiggerVector() {
		return biggerVector;
	}
	public void setBiggerVector(int biggerVector) {
		this.biggerVector = biggerVector;
	}
	public float getEffectTime() {
		return effectTime;
	}
	public void setEffectTime(float effectTime) {
		this.effectTime = effectTime;
	}
	public JSONObject getBackground() {
		return background;
	}
	public void setBackground(JSONObject background) {
		this.background = background;
	}
	public JSONObject getObj() {
		return obj;
	}
	public void setObj(JSONObject obj) {
		this.obj = obj;
	}
	public String getRootPath() {
		return rootPath;
	}
	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
	public JSONArray getOtherObj() {
		return otherObj;
	}
	public void setOtherObj(JSONArray others) {
		this.otherObj = others;
	}
	
	
}
