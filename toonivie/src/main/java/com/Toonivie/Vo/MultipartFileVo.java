package com.Toonivie.Vo;

import org.springframework.web.multipart.MultipartFile;

public class MultipartFileVo {
	
    private MultipartFile imgFile;
    private String shrinking_ratio_h;
    private String shrinking_ratio_w;
    
    public String getShrinking_ratio_h() {
		return shrinking_ratio_h;
	}

	public void setShrinking_ratio_h(String shrinking_ratio_h) {
		this.shrinking_ratio_h = shrinking_ratio_h;
	}
	public String getShrinking_ratio_w() {
		return shrinking_ratio_w;
	}
	
	public void setShrinking_ratio_w(String shrinking_ratio_w) {
		this.shrinking_ratio_w = shrinking_ratio_w;
	}

	public MultipartFile getImgFile() {
        return imgFile;
    }
 
    public void setImgFile(MultipartFile imgFile) {
        this.imgFile = imgFile;
    }


}
