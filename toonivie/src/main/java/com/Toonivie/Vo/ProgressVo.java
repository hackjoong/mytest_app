package com.Toonivie.Vo;

public class ProgressVo {
	private static int totalCount = 0;
	private static int successCount = 0;
	private static int failCount = 0;
	private static int nullCount = 0;
	
	private static int currentStateCount = 0;
	
	private static String currentState;

	public static int getTotalCount() {
		return totalCount;
	}

	public static void setTotalCount(int totalCount) {
		ProgressVo.totalCount = totalCount;
	}

	public static int getSuccessCount() {
		return successCount;
	}

	public static void setSuccessCount(int successCount) {
		ProgressVo.successCount = successCount;
	}

	public static int getFailCount() {
		return failCount;
	}

	public static void setFailCount(int failCount) {
		ProgressVo.failCount = failCount;
	}

	public static int getNullCount() {
		return nullCount;
	}

	public static void setNullCount(int nullCount) {
		ProgressVo.nullCount = nullCount;
	}

	public static int getCurrentStateCount() {
		return currentStateCount;
	}

	public static void setCurrentStateCount(int currentStateCount) {
		ProgressVo.currentStateCount = currentStateCount;
	}

	public static String getCurrentState() {
		return currentState;
	}

	public static void setCurrentState(String currentState) {
		ProgressVo.currentState = currentState;
	}
	
}
