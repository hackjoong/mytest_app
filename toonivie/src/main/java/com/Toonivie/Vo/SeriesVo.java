package com.Toonivie.Vo;

import java.util.Date;

public class SeriesVo {
	
	int wmno;
	String wmfile;
	String wmgenre;
	String wmtitle;
	String id;
	String wmthumbnail;
	String wmwriter;
	String wmcontent;
	Date wridate;
	Date upadatedate;
	int viewCount;
	int commentCount;
	String kinds;

	public int getWmno() {
		return wmno;
	}
	public void setWmno(int wmno) {
		this.wmno = wmno;
	}
	public String getWmfile() {
		return wmfile;
	}
	public void setWmfile(String wmfile) {
		this.wmfile = wmfile;
	}
	public String getWmgenre() {
		return wmgenre;
	}
	public void setWmgenre(String wmgenre) {
		this.wmgenre = wmgenre;
	}
	public String getWmtitle() {
		return wmtitle;
	}
	public void setWmtitle(String wmtitle) {
		this.wmtitle = wmtitle;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWmthumbnail() {
		return wmthumbnail;
	}
	public void setWmthumbnail(String wmthumbnail) {
		this.wmthumbnail = wmthumbnail;
	}
	public String getWmwriter() {
		return wmwriter;
	}
	public void setWmwriter(String wmwriter) {
		this.wmwriter = wmwriter;
	}
	public String getWmcontent() {
		return wmcontent;
	}
	public void setWmcontent(String wmcontent) {
		this.wmcontent = wmcontent;
	}
	public Date getWridate() {
		return wridate;
	}
	public void setWridate(Date wridate) {
		this.wridate = wridate;
	}
	public Date getUpadatedate() {
		return upadatedate;
	}
	public void setUpadatedate(Date upadatedate) {
		this.upadatedate = upadatedate;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public String getKinds() {
		return kinds;
	}
	public void setKinds(String kinds) {
		this.kinds = kinds;
	}

}
