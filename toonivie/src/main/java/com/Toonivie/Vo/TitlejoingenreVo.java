package com.Toonivie.Vo;

public class TitlejoingenreVo {
	int toonivieTitleSeq;
	int genreSeq;
	public int getToonivieTitleSeq() {
		return toonivieTitleSeq;
	}
	public void setToonivieTitleSeq(int toonivieTitleSeq) {
		this.toonivieTitleSeq = toonivieTitleSeq;
	}
	public int getGenreSeq() {
		return genreSeq;
	}
	public void setGenreSeq(int genreSeq) {
		this.genreSeq = genreSeq;
	}
	
}
