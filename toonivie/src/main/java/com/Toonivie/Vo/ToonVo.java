package com.Toonivie.Vo;

import java.util.Date;

public class ToonVo {
	
	int tno;
	String file; //add
	String thumbnail;
	String genre;
	String title;
	String id;
	String writer;
	String content;
	Date wridate;
	Date updatedate;
	int viewCount;
	int commentCount;
	int wmno;
	Boolean free_open;
	
	
	
	@Override
	public String toString() {
		return "ToonVo [tno=" + tno + ", file=" + file + ", thumbnail=" + thumbnail + ", genre=" + genre + ", title="
				+ title + ", id=" + id + ", writer=" + writer + ", content=" + content + ", wridate=" + wridate
				+ ", updatedate=" + updatedate + ", viewCount=" + viewCount + ", commentCount=" + commentCount
				+ ", wmno=" + wmno + ", free_open=" + free_open + "]";
	}
	public int getTno() {
		return tno;
	}
	public void setTno(int tno) {
		this.tno = tno;
	}
	public String getFile() { //add
		return file;
	}
	public void setFile(String file) { //add
		this.file = file;
	}	
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getWridate() {
		return wridate;
	}
	public void setWridate(Date wridate) {
		this.wridate = wridate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public int getWmno() {
		return wmno;
	}
	public void setWmno(int wmno) {
		this.wmno = wmno;
	}
	public Boolean getFree_open() {
		return free_open;
	}
	public void setFree_open(Boolean free_open) {
		this.free_open = free_open;
	}
	
	

}
