package com.Toonivie.Vo;

public class TooniviecontentsVo {
	int toonivieContentsSeq;
	int toonivieTitleSeq;
	String thumbnailUrl;
	String writerWord;
	public int getToonivieContentsSeq() {
		return toonivieContentsSeq;
	}
	public void setToonivieContentsSeq(int toonivieContentsSeq) {
		this.toonivieContentsSeq = toonivieContentsSeq;
	}
	public int getToonivieTitleSeq() {
		return toonivieTitleSeq;
	}
	public void setToonivieTitleSeq(int toonivieTitleSeq) {
		this.toonivieTitleSeq = toonivieTitleSeq;
	}
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public String getWriterWord() {
		return writerWord;
	}
	public void setWriterWord(String writerWord) {
		this.writerWord = writerWord;
	}
	
}
