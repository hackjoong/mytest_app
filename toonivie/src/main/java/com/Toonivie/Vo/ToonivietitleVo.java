package com.Toonivie.Vo;

public class ToonivietitleVo {
	int toonivieTitleSeq;
	String titleName;
	int userSeq;
	String registrationDate;
	String imgUrl;
	String introText;
	public int getToonivieTitleSeq() {
		return toonivieTitleSeq;
	}
	public void setToonivieTitleSeq(int toonivieTitleSeq) {
		this.toonivieTitleSeq = toonivieTitleSeq;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public int getUserSeq() {
		return userSeq;
	}
	public void setUserSeq(int userSeq) {
		this.userSeq = userSeq;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getIntroText() {
		return introText;
	}
	public void setIntroText(String introText) {
		this.introText = introText;
	}
	
}
