package com.Toonivie.Vo;

import java.util.Date;

public class UserVo {

	int userseq;
	String id;
	String pw;
	String name;
	String email;
	String role;
	String nickname;
	String work;
	String tel;
	String matching;
	String nohashpw;
	String icon;
	Date joindate;
	Date conndate;
	int logerror;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserSeq(int userseq) {
		this.userseq = userseq;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMatching() {
		return matching;
	}
	public void setMatching(String matching) {
		this.matching = matching;
	}
	public String getNohashpw() {
		return nohashpw;
	}
	public void setNohashpw(String nohashpw) {
		this.nohashpw = nohashpw;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Date getJoindate() {
		return joindate;
	}
	public void setJoindate(Date joindate) {
		this.joindate = joindate;
	}
	public Date getConndate() {
		return conndate;
	}
	public void setConndate(Date conndate) {
		this.conndate = conndate;
	}
	public int getLogerror() {
		return logerror;
	}
	public void setLogerror(int logerror) {
		this.logerror = logerror;
	}
	
}
