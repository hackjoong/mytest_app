package com.Toonivie.Vo;

public class ViewOrderVo {
	int viewContentNo;
	String viewId;
	
	public int getViewContentNo() {
		return viewContentNo;
	}
	public void setViewContentNo(int viewContentNo) {
		this.viewContentNo = viewContentNo;
	}
	public String getViewId() {
		return viewId;
	}
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}


}
