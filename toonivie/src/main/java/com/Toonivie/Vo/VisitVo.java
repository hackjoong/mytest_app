package com.Toonivie.Vo;

import java.util.Date;

public class VisitVo {
	
	int vno;
	String vip;
	Date vtime;
	
	public int getVno() {
		return vno;
	}
	public void setVno(int vno) {
		this.vno = vno;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	public Date getVtime() {
		return vtime;
	}
	public void setVtime(Date vtime) {
		this.vtime = vtime;
	}

}
