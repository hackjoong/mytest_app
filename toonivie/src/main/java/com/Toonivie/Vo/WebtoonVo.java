package com.Toonivie.Vo;

import java.util.Date;

public class WebtoonVo {
	int wtno;
	String path;
	String wtname;
	String wtwriter;
	String wtgenre;
	String wtcontent;
	String id;
	int wtviewCount;
	int wtcommCount;
	String thumbnail;
	Date wtwridate;
	int wmno;
	Boolean free_open;
	
	
	
	@Override
	public String toString() {
		return "WebtoonVo [wtno=" + wtno + ", path=" + path + ", wtname=" + wtname + ", wtwriter=" + wtwriter
				+ ", wtgenre=" + wtgenre + ", wtcontent=" + wtcontent + ", id=" + id + ", wtviewCount=" + wtviewCount
				+ ", wtcommCount=" + wtcommCount + ", thumbnail=" + thumbnail + ", wtwridate=" + wtwridate + ", wmno="
				+ wmno + ", free_open=" + free_open + "]";
	}
	public Boolean getFree_open() {
		return free_open;
	}
	public void setFree_open(Boolean free_open) {
		this.free_open = free_open;
	}
	public int getWtno() {
		return wtno;
	}
	public void setWtno(int wtno) {
		this.wtno = wtno;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getWtname() {
		return wtname;
	}
	public void setWtname(String wtname) {
		this.wtname = wtname;
	}
	public String getWtwriter() {
		return wtwriter;
	}
	public void setWtwriter(String wtwriter) {
		this.wtwriter = wtwriter;
	}
	public String getWtgenre() {
		return wtgenre;
	}
	public void setWtgenre(String wtgenre) {
		this.wtgenre = wtgenre;
	}
	public String getWtcontent() {
		return wtcontent;
	}
	public void setWtcontent(String wtcontent) {
		this.wtcontent = wtcontent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getWtviewCount() {
		return wtviewCount;
	}
	public void setWtviewCount(int wtviewCount) {
		this.wtviewCount = wtviewCount;
	}
	public int getWtcommCount() {
		return wtcommCount;
	}
	public void setWtcommCount(int wtcommCount) {
		this.wtcommCount = wtcommCount;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Date getWtwridate() {
		return wtwridate;
	}
	public void setWtwridate(Date wtwridate) {
		this.wtwridate = wtwridate;
	}
	public int getWmno() {
		return wmno;
	}
	public void setWmno(int wmno) {
		this.wmno = wmno;
	}

}
