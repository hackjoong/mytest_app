package com.Toonivie.Vo;

public class WriterVo {
	
	int wno;
	String wid;
	String wpw;
	String wemail;
	String wname;
	String wnickname;
	String wwork;
	String role;
	
	
	public int getWno() {
		return wno;
	}
	public void setWno(int wno) {
		this.wno = wno;
	}
	public String getWid() {
		return wid;
	}
	public void setWid(String wid) {
		this.wid = wid;
	}
	public String getWpw() {
		return wpw;
	}
	public void setWpw(String wpw) {
		this.wpw = wpw;
	}
	public String getWemail() {
		return wemail;
	}
	public void setWemail(String wemail) {
		this.wemail = wemail;
	}
	public String getWname() {
		return wname;
	}
	public void setWname(String wname) {
		this.wname = wname;
	}
	public String getWnickname() {
		return wnickname;
	}
	public void setWnickname(String wnickname) {
		this.wnickname = wnickname;
	}
	public String getWwork() {
		return wwork;
	}
	public void setWwork(String wwork) {
		this.wwork = wwork;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}
