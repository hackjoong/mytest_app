package com.Toonivie.Vo;

import java.util.Date;

public class WtCommentVo {
	int cno;
	int wtno;
	String comment;
	String pw;
	String nickname;
	Date cdate;
	int wtdapCount;
	
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public int getWtno() {
		return wtno;
	}
	public void setWtno(int wtno) {
		this.wtno = wtno;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Date getCdate() {
		return cdate;
	}
	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	public int getWtdapCount() {
		return wtdapCount;
	}
	public void setWtdapCount(int wtdapCount) {
		this.wtdapCount = wtdapCount;
	}


}
