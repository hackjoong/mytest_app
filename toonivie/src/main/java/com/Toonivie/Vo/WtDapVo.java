package com.Toonivie.Vo;

import java.util.Date;

public class WtDapVo {
	
	int wdno;
	int cno;
	String wtcontent;
	Date wtddate;
	String wtdwriter;
	String wtpwd;
	
	public int getWdno() {
		return wdno;
	}
	public void setWdno(int wdno) {
		this.wdno = wdno;
	}
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getWtcontent() {
		return wtcontent;
	}
	public void setWtcontent(String wtcontent) {
		this.wtcontent = wtcontent;
	}
	public Date getWtddate() {
		return wtddate;
	}
	public void setWtddate(Date wtddate) {
		this.wtddate = wtddate;
	}
	public String getWtdwriter() {
		return wtdwriter;
	}
	public void setWtdwriter(String wtdwriter) {
		this.wtdwriter = wtdwriter;
	}
	public String getWtpwd() {
		return wtpwd;
	}
	public void setWtpwd(String wtpwd) {
		this.wtpwd = wtpwd;
	}

}
