package com.Toonivie.Webtoon2D;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Security.SecAlgorithm;
import com.Toonivie.Signup.SignupValidate;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.UserVo;
import com.Toonivie.Vo.WtCommentVo;
import com.Toonivie.Vo.WtDapVo;

@Controller
public class Webtoon2DController {
	@Autowired
	Webtoon2DService service;
	@Autowired
	SignupValidate signupValidate;
	
	@RequestMapping(value ="/webtoon_upload2/webtoon_upload", method=RequestMethod.POST)
	@ResponseBody
	public String fileUp(MultipartHttpServletRequest req, HttpServletRequest request) throws IOException {
		//썸네일
		MultipartFile upFile = req.getFile("thumbnail");
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = "Thumbnail"+cfu.getToday(1)+"."+CreateFileUtils.getFileType(upFile.getOriginalFilename());
		cfu.CreateFile(upFile, request, "/resources/webtoon/thumbnail/", filename);
		
		//원작 웹툰 파일들
		Iterator<String> files = req.getFileNames();
		String i = "0";
		String path="";
		int sum = Integer.parseInt(i+1);
		while(files.hasNext()){

			String uploadFile = files.next();
			if(uploadFile.equals("thumbnail")) {
				break;
			}
			MultipartFile mFile = req.getFile(uploadFile);
			/*CreateFileUtils cfu = new CreateFileUtils();*/
			String fileName = cfu.getToday(1)+"("+sum+")"+"."+cfu.getFileType(mFile.getOriginalFilename());
			cfu.CreateFile(mFile, request, "/resources/webtoon/", fileName);
			sum+=1;
			/*String thumbnail = "/resources/webtoon/"+fileName;*/
			path += fileName+",";
		}
		int a1 = path.indexOf(',');
		path = path.substring(0, path.length()-1);
		
		String thumbnail = "/resources/webtoon/thumbnail/"+filename;
		String wttitle = request.getParameter("title");
		String wtwriter = request.getParameter("writer");
		String wtgenre = request.getParameter("genre");
		String wtcontent = request.getParameter("content");
		Boolean free_open = Boolean.valueOf(request.getParameter("check_free"));
		int wmno = Integer.parseInt(request.getParameter("serieswonjak"));//-> this is wonjak series number
		
		service.wonjakfileUp(path, wttitle, wtwriter, wtgenre, wtcontent,thumbnail, wmno, free_open);

		
		return null;
	}
	@RequestMapping("/webtoon_upload2/webtoon_delete/{wtno}")
	@ResponseBody
	public int fileDel(HttpServletRequest request) throws Exception {
		int wtno = Integer.parseInt(request.getParameter("wtno"));
		int resultVal = service.fileDel(wtno);
		
		if(resultVal ==1) {
			
			String path = request.getParameter("path");
			String[] pathArr = path.split(",");
			for(int i = 0; i<pathArr.length;i++) {
				String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
				File file = new File(rootPath+"/resources/webtoon/"+pathArr[i]);
				if(file.exists()) {
					file.delete();
				}
			}
			service.CountCommentDel(wtno);
			service.AdmincommentDelService(wtno);
			return resultVal;
		}
		else {
			return 0;	
		}

	}
	
	@RequestMapping("/webtoon_upload2/webtoon_update/{wtno}")
	@ResponseBody
	public int fileMod(HttpServletRequest request, MultipartHttpServletRequest req) {
		int resultVal;
		MultipartFile upFile = req.getFile("thumbnail");
		int wtno = Integer.parseInt(request.getParameter("wtno"));
		String wtname = request.getParameter("title");
		String wtgenre = request.getParameter("genre");
		String wtwriter = request.getParameter("writer");
		String wtcontent = request.getParameter("content");
		Boolean free_open = Boolean.valueOf(request.getParameter("check_free"));
		
		int wmno = Integer.parseInt(request.getParameter("modseries"));
		
		//썸네일
		if(upFile !=null) {
			String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
			String beforeThumbnail = request.getParameter("beforethumb");
			File file = new File(rootPath+beforeThumbnail);
			if(file.exists()) {
				file.delete();
			}
			
			CreateFileUtils cfu = new CreateFileUtils();
			String filename = "Thumbnail"+cfu.getToday(1)+"."+CreateFileUtils.getFileType(upFile.getOriginalFilename());
			cfu.CreateFile(upFile, request, "/resources/webtoon/thumbnail/", filename);
			String thumbnail = "/resources/webtoon/thumbnail/"+filename;
			resultVal = service.fileModwonjak(wtno, wtname, wtwriter, wtgenre, wtcontent, thumbnail, wmno, free_open);
		}
		else {
			resultVal = service.nofileModwonjak(wtno, wtname, wtwriter, wtgenre, wtcontent,wmno, free_open);
		}
		return resultVal;
	}
	
	
	@RequestMapping("/comment/insert_toon")
	@ResponseBody
	private int insertComment(@RequestParam int wtno, @RequestParam String comment, @RequestParam String pw, @RequestParam String nickname, HttpSession session)
			throws Exception {
		WtCommentVo vo = new WtCommentVo();
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if(currentUser==null) {
			pw= signupValidate.hashPw(pw);
		}
		vo.setWtno(wtno);
		vo.setComment(comment);
		vo.setNickname(nickname);
		vo.setPw(pw);

		service.CountCommentUp(wtno);//댓글개수 업데이트
		return service.inputComment(vo);
	}
	
	@RequestMapping("/comment/list_toon")
	@ResponseBody
	private List<WtCommentVo> list(Model model, HttpServletRequest request, int wtno) throws Exception {
		model.addAttribute("countComment",service.countComment(wtno));		
		return service.list(wtno);
	}
	
	@RequestMapping("/comment/update_toon") //
	@ResponseBody
	private int mCommentServiceUpdateProc(@RequestParam("cno") int cno, @RequestParam("comment") String comment) throws Exception {

		WtCommentVo vo = new WtCommentVo();
		vo.setCno(cno);
		vo.setComment(comment);

		return service.commentUpdateService(vo);
	}
	
	@RequestMapping("/comment/delete_toon") 
	@ResponseBody
	private int mCommentServiceDelete(@RequestParam int cno, @RequestParam int wtno) throws Exception {
		service.CountCommentDel(wtno);
		return service.commentDeleteService(cno);
	}
	
	// 답글 시작
	@RequestMapping("/dap/insert_toon")
	@ResponseBody
	private int insertDap(@RequestParam int cno, @RequestParam String content, @RequestParam("nickname")String nickname,@RequestParam String pwd, HttpSession session) throws Exception {
		WtDapVo vo = new WtDapVo();
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if(currentUser==null) {
			pwd= signupValidate.hashPw(pwd);
		}
		vo.setWtcontent(content);;
		vo.setWtdwriter(nickname);
		vo.setWtpwd(pwd);
		vo.setCno(cno);
		service.CountDapUp(cno);
		return service.inputdap(vo);
	}
	
	@RequestMapping("/dap/list_toon")
	@ResponseBody
	private List<WtDapVo> dapList(@RequestParam Integer cno) throws Exception {
		return service.dapList(cno);
	}
	
	
	@RequestMapping("/dap/update_toon")
	@ResponseBody
	private int mDapServiceUpdateProc(@RequestParam int dno, @RequestParam("content") String content, @RequestParam String dwriter) throws Exception {

		WtDapVo vo = new WtDapVo();
		vo.setWdno(dno);
		vo.setWtcontent(content);
		vo.setWtdwriter(dwriter);

		return service.dapUpdateService(vo);
	}
	

	@RequestMapping("/dap/delete_toon/{wdno}") //답글 삭제
	@ResponseBody
	private int mDapServiceDelete(@PathVariable int wdno, @RequestParam("cno") int cno) throws Exception {
		service.CountDapDown(cno);
		return service.dapDeleteService(wdno);
	}
	
	//댓글 수정,삭제시 비밀번호 체크부분
	@RequestMapping("/comment/checkPw_toon")
	@ResponseBody
	public int checkPw(HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String inputpw = request.getParameter("inputPw");
		int cno = Integer.parseInt(request.getParameter("cno"));
		String realpw = service.getPw_toon(cno);
		boolean checkpw = SecAlgorithm.validatePassword(inputpw, realpw);
		
		if(checkpw == true) {
			return 1;
		}
		else {
			return 0;
		}
		
	}
	//답글 수정,삭제시 비밀번호 체크부분
	@RequestMapping("/dap/checkPw_toon")
	@ResponseBody
	public int checkPwForDap(HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String inputpw = request.getParameter("inputPw");
		int wdno = Integer.parseInt(request.getParameter("wdno"));
		String realpw = service.getPwForDap_toon(wdno);
		System.out.println(inputpw+","+realpw);
		
		boolean checkpw = SecAlgorithm.validatePassword(inputpw, realpw);
		//boolean checkpw = (inputpw.equals(realpw));

		
		if(checkpw == true) {
			return 1;
		}
		else {
			return 0;
		}
		
	}

	
	/*//이미지 리사이즈
	public void ImageResize(String fileName) throws IOException {
		String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
		File fullpath  = new File(rootPath+fileName);
		BufferedImage srcImg = ImageIO.read(fullpath);
		
		int width = srcImg.getWidth();
		int height = srcImg.getHeight();
		
	}*/
}
