package com.Toonivie.Webtoon2D;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;
import com.Toonivie.Vo.WebtoonVo;
import com.Toonivie.Vo.WtCommentVo;
import com.Toonivie.Vo.WtDapVo;

@Mapper
public interface Webtoon2DDao {
	//file multi upload
	public void wonjakfileUp(WebtoonVo vo);
	
	//webtoon del
	public int fileDel(int wtno);
	
	//webtoon mod
	public int nofileModwonjak(WebtoonVo wvo);
	
	//webtoon mod->include thumbnail
	public int fileModwonjak(WebtoonVo wvo);
	
	//total original image 
	public List<WebtoonVo> fileSelAll();
	
	//webtoon ranking
	public List<WebtoonVo> fileSelAll_sunwi();
	
	//total image
	public List<Map<String,Object>> fileSelAll_1(Map<String, String> map);
	
	//mypage webtoon
	public List<WebtoonVo> fileSeluser(String nickname);
	
	//webtoon count
	public int countAll();
	
	
	//image select 
	public WebtoonVo fileSel(int wtno);
	
	
	//view count
	public void increaseViewCount(int wtno);
	
	//comment all count
	public int countAllcomment();
	
	//��� ��ü ����
	public int countAlldap();
	
	public int countComment(int wtno);
	
	public int insertComment(WtCommentVo vo);
	
	//comment count +1
	public int CountCommentUp(int wtno) throws Exception;
	
	//comment list
	public List<WtCommentVo> commentList(int wtno) throws Exception;
	
	//comment all
	public List<WtCommentVo> commentListAll();
	
	//dap all
	public List<WtDapVo> dapListAll();

	//comment mod
	public int commentUpdate(WtCommentVo vo) throws Exception;
	
	//comment count -1
	public int CountCommentDel(int wtno) throws Exception;
	
	//comment del
	public int commentDelete(int cno) throws Exception;
	
	//adminpage comment del
	public int AdmincommentDel(int wtno);
		
	//comment for dap del
	public int commentForDap(int cno) throws Exception;
	
	// dap insert
	public int insertdap(WtDapVo vo) throws Exception;
	
	// dap list
	public List<WtDapVo> dapList(int cno) throws Exception;

	// dap modify
	public int dapUpdate(WtDapVo vo) throws Exception;

	// dap delete
	public int dapDelete(int wdno) throws Exception;

	// dap count update +1
	public int CountDapUp(int cno) throws Exception;
	
	// dap count update -1
	public int CountDapDown(int cno) throws Exception;
	
	/*webtoon part*/
	//show profile from nickname step 1
	public String getId(int wtno);
	//writer profile get
	public String writerprofile(String nickname);
	
	public String getPw_toon(int cno);
	
	public String getPwForDap_toon(int wdno);
}