package com.Toonivie.Webtoon2D;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.Toonivie.Slidebanner.SlidebannerService;
import com.Toonivie.Util.PageNavigator;
import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;
import com.Toonivie.Vo.WebtoonVo;
import com.Toonivie.Vo.WtCommentVo;
import com.Toonivie.Vo.WtDapVo;
import com.Toonivie.series.SeriesDao;
import com.Toonivie.series.SeriesService;

@Service
public class Webtoon2DService {
	@Autowired
	Webtoon2DDao dao;
	@Autowired
	SlidebannerService SlidebanService;
	@Autowired
	SeriesService seriesService;
	@Autowired
	SeriesDao seriesDao;
	

	
	public void wonjakfileUp(String path,String wttitle, String wtwriter, String wtgenre, String wtcontent, String thumbnail, int wmno, Boolean free_open) {
		WebtoonVo vo = new WebtoonVo();
		
		vo.setPath(path);
		vo.setWtname(wttitle);
		vo.setWtwriter(wtwriter);
		vo.setWtgenre(wtgenre);
		vo.setWtcontent(wtcontent);
		vo.setId(wtwriter);
		vo.setThumbnail(thumbnail);
		vo.setWmno(wmno);
		vo.setFree_open(free_open);
		dao.wonjakfileUp(vo);
	}
	
	public int fileDel(int wtno) {
		return dao.fileDel(wtno);
	}
	public int nofileModwonjak(int wtno, String wtname, String wtwriter, String wtgenre, String wtcontent, int wmno, Boolean free_open) {
		WebtoonVo wvo = new WebtoonVo();
		wvo.setWtno(wtno);
		wvo.setWtname(wtname);
		wvo.setWtwriter(wtwriter);
		wvo.setWtgenre(wtgenre);
		wvo.setWtcontent(wtcontent);
		wvo.setId(wtwriter);
		wvo.setWmno(wmno);
		wvo.setFree_open(free_open);
		return dao.nofileModwonjak(wvo);
	}
	
	public int fileModwonjak(int wtno, String wtname, String wtwriter, String wtgenre, String wtcontent, String thumbnail, int wmno, Boolean free_open) {
		WebtoonVo wvo = new WebtoonVo();
		wvo.setWtno(wtno);
		wvo.setWtname(wtname);
		wvo.setWtwriter(wtwriter);
		wvo.setWtgenre(wtgenre);
		wvo.setWtcontent(wtcontent);
		wvo.setId(wtwriter);
		wvo.setThumbnail(thumbnail);
		wvo.setWmno(wmno);
		wvo.setFree_open(free_open);
		return dao.fileModwonjak(wvo);
	}
	
	public List<WebtoonVo> fileSelAll() {
		return dao.fileSelAll();
	}
	public List<WebtoonVo> fileSeluser(String nickname){
		return dao.fileSeluser(nickname);
	}
	
	public int countAll() {
		return dao.countAll();
	}
	
	public WebtoonVo fileSel(int wtno) {
		return dao.fileSel(wtno);
	}
	
	//��ȸ��
	public void increaseViewCount(int wtno) {
		dao.increaseViewCount(wtno);
	}
	
	//��۰� ��� ����!!!!!!!!!!!
	//�������� ��� ����
	public int countComment(int wtno) {
		return dao.countComment(wtno);
		
	}
	//��� insert
	public int inputComment(WtCommentVo vo) {
		return dao.insertComment(vo);
	}
	
	//��� ���� ������Ʈ+1
	public int CountCommentUp(int wtno) throws Exception {
		return dao.CountCommentUp(wtno);
	}
	
	//��� ��ü
	public List<WtCommentVo> list(){
		return dao.commentListAll();
	}
	
	//��� ��ü
	public List<WtDapVo> listDap(){
		return dao.dapListAll();
	}
	
	// ��� ���
	public List<WtCommentVo> list(int wtno) throws Exception {
		return dao.commentList(wtno);
	}
	
	// ��� ����
	public int commentUpdateService(WtCommentVo vo) throws Exception {
		return dao.commentUpdate(vo);
	}
	
	// ��� ���� ������Ʈ-1
	public int CountCommentDel(int wtno) throws Exception {
		return dao.CountCommentDel(wtno);
	}
	
	// ��� ����
	public int commentDeleteService(int cno) throws Exception {
		dao.commentForDap(cno);
		return dao.commentDelete(cno);
	}
	
	//������������ ��� ����
	public int AdmincommentDelService(int wtno) {
		return dao.AdmincommentDel(wtno);
	}
	
	// ��� �Է�
	public int inputdap(WtDapVo vo) throws Exception {
		return dao.insertdap(vo);
	}

	// ��� ���
	public List<WtDapVo> dapList(int cno) throws Exception {
		return dao.dapList(cno);
	}
	
	// ��� ����
	public int dapUpdateService(WtDapVo vo) throws Exception {
		return dao.dapUpdate(vo);
	}

	// ��� ����
	public int dapDeleteService(int wdno) throws Exception {
		return dao.dapDelete(wdno);
	}
	
	//��� ���� ������Ʈ+1
	public int CountDapUp(int cno) throws Exception {
		return dao.CountDapUp(cno);
	}
	
	//��� ���� ������Ʈ-1
	public int CountDapDown(int cno) throws Exception {
		return dao.CountDapDown(cno);
	}
	
	public List<WebtoonVo> fileSelAll_sunwi() {
		return dao.fileSelAll_sunwi();
	}
	
	public ModelAndView wonjakmain(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		String kinds = "origintoon";
		int totalContents = seriesDao.SeriesListWonjak(kinds).size();
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		model.addObject("nation",nation);
		model.addObject("wonjak", this.fileSelAll());
		model.addObject("banList",SlidebanService.BannerMain(kinds));
		model.addObject("wonjaksunwi", this.fileSelAll_sunwi());
		model.addObject("newwonjak",this.getWonjakList(nation.getPageMap()));
		
		return model;
	}
	
	
	
	/*public List<Map<String, Object>> getWonjakList(Map<String, String> map){
		return dao.fileSelAll_1(map);	
	}*/
	public List<Map<String, Object>> getWonjakList(Map<String, String> map){
		return dao.fileSelAll_1(map);	
	}
	
	//���������� ������ ��������
	public String getId(int wtno) {
		return dao.getId(wtno);
	}

	public String writerprofile(String nickname) {
		return dao.writerprofile(nickname);
	}
	
	public String getPw_toon(int cno) {
		return dao.getPw_toon(cno);
	}
	
	public String getPwForDap_toon(int wdno) {
		return dao.getPwForDap_toon(wdno);
	}

	


		

	

}