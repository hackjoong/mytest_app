package com.Toonivie.WriterInfo;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/writer")
public class WriterController {
	@Autowired
	WriterService writerService;
	
	@RequestMapping("/list")
	public String writerList(HttpServletRequest request, Model model) throws Exception {
		model.addAttribute("CountWriter", writerService.WriterCount());
		model.addAttribute("WriterNameAll", writerService.WriterNameAll());
		model.addAttribute("WriterToonAll", writerService.WriterToonAll());
		model.addAttribute("WriterInfoAll", writerService.writerlist());
		return "adminPage/writerList";
	}
	@RequestMapping("/update")
	@ResponseBody
	public int UpdateWriter(HttpServletRequest request) {
		String noChanged = request.getParameter("noChangePW");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String nickname = request.getParameter("nickname");
		String work = request.getParameter("work");
		
		
		if(noChanged.equals("false")) {//비번체인지
			String pw = request.getParameter("pw");
			System.out.println("nochanged.equals(false) ininin!!!");
			return writerService.MemberUpdatePW(id, pw, email, name, work, nickname);
		}
		else {
			return writerService.MemberUpdate_default(id, email, name, work, nickname);
		}
		
	}


}
