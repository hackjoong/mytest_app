package com.Toonivie.WriterInfo;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.UserVo;

@Mapper
public interface WriterDao {
	
	public int WriterCount();
	
	public List<ToonVo> WriterNameAll();
	
	public List<ToonVo> WriterToonAll();
	
	public List<ToonVo> writerlist();
	
	public int MemberUpdatePW(UserVo vo);
	
	public int MemberUpdate_default(UserVo vo);

}
