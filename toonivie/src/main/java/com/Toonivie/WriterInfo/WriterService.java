package com.Toonivie.WriterInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Signup.SignupValidate;
import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.UserVo;

@Service
public class WriterService {
	@Autowired
	WriterDao writerDao;
	@Autowired
	SignupValidate signupValidate;
	
	public int WriterCount() {
		return writerDao.WriterCount();
	}
	
	public List<ToonVo> WriterNameAll() {
		return writerDao.WriterNameAll();
	}
	public List<ToonVo> WriterToonAll(){
		return writerDao.WriterToonAll();
	}
	
	public List<ToonVo> writerlist() {
		return writerDao.writerlist();
	}
	
	public int MemberUpdatePW(String id, String pw, String email, String name, String work, String nickname) {
		String nohashpw = pw;
		pw=signupValidate.hashPw(pw);
		UserVo vo = new UserVo();
		vo.setId(id);
		vo.setPw(pw);
		vo.setEmail(email);
		vo.setName(name);
		vo.setNickname(nickname);
		vo.setWork(work);
		vo.setNohashpw(nohashpw);
		return writerDao.MemberUpdatePW(vo);

	}
	public int MemberUpdate_default(String id, String email, String name, String work, String nickname) {
		UserVo vo = new UserVo();
		vo.setId(id);

		vo.setEmail(email);
		vo.setName(name);
		vo.setNickname(nickname);
		vo.setWork(work);
		
		return writerDao.MemberUpdate_default(vo);

	}

}
