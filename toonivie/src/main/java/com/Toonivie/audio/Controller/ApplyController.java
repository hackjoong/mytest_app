package com.Toonivie.audio.Controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.audio.Service.FfmpegConver;

@Controller
@RequestMapping("/applyAS")
public class ApplyController {

	private static String convert(String ffmpegPath,String Params,boolean isSmiExist){
		FfmpegConver a = new FfmpegConver(ffmpegPath,Params,isSmiExist);
		a.start();
		 
		try {
			a.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return a.getResult();			
	}
    @RequestMapping(value="/applyTextAndAudio",method=RequestMethod.POST)
    @ResponseBody
    public HashMap<String,Object> getInfo(HttpServletRequest request){
    	//String rootPath = request.getServletContext().getRealPath("/");
        HashMap<String,Object> resultMap = new HashMap<>();
        //String ffmpegPath = "./ffmpeg/bin";   // ��) /work/ffmpeg
        String rootPath = request.getSession().getServletContext().getRealPath("/");
        String ffmpegPath = rootPath+"resources\\lib\\ffmpeg\\bin";
        String data = request.getParameter("data");
        boolean isSmiExist=true;
        try {
        	JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(data);

        }catch (Exception e) {
        	e.printStackTrace();
		}
		String r_data  = convert(ffmpegPath,data,isSmiExist);
		System.out.println(r_data);
		resultMap.put("isSmiExist",isSmiExist);
		String[] resultArr = r_data.split("/main/webapp");
		String resultVideo = resultArr[1];
		resultMap.put("videoUrl", resultVideo);
		resultMap.put("smiUrl", resultVideo.replaceAll(".mp4", ".smi"));
		resultMap.put(convert(ffmpegPath, data, isSmiExist), resultVideo);
        return resultMap;
    }
}
