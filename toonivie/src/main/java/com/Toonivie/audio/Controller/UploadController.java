package com.Toonivie.audio.Controller;

import com.Toonivie.audio.Util.PathUtils;
import com.Toonivie.audio.Service.UploadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class UploadController {
    @Autowired
    UploadService uploadService;


    @RequestMapping(value = "/uploadAudio", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String,Object> uploadAudio(MultipartHttpServletRequest req, HttpServletRequest request) throws Exception {
        MultipartFile file = req.getFile("audio");
        String rootPath = PathUtils.getRootPath(request);
        String dirPath = "/resources/subTitleMov/";
        return uploadService.uploadFile(file,rootPath,dirPath);
    }

    @RequestMapping(value = "/uploadVideo", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String,Object> uploadVideo(MultipartHttpServletRequest req, HttpServletRequest request) throws Exception {
        MultipartFile file = req.getFile("video");
        String rootPath = PathUtils.getRootPath(request);
        String dirPath = "/resources/subTitleMov/";
        HashMap<String,Object> videoMap = uploadService.uploadFile(file, rootPath, dirPath);
        HashMap<String,Object> resultMap = uploadService.putVideoInfo(videoMap, rootPath);
        System.out.println(resultMap.get("width"));
        return resultMap;
    }
}
