package com.Toonivie.audio.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping("/audionsubtitle")
    public String index(Model model){
    	String rootPath = "src/main/webapp";
    	model.addAttribute("rootPath",rootPath);
        return "/audionsubtitle/index";
    }
    
    @RequestMapping("/audionsubtitle_en")
    public String index_en(Model model){
    	String rootPath = "src/main/webapp";
    	model.addAttribute("rootPath",rootPath);
        return "/audionsubtitle/index_en";
    }

}
