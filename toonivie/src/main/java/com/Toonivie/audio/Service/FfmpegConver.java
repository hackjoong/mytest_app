package com.Toonivie.audio.Service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.Toonivie.Vo.ProgressVo;

public class FfmpegConver extends Thread{
	String ffmpegPath;
	String ffprobePath;
	String Result;
	String Params;
	JSONObject Video;
	//JSONArray TextArray;
	JSONArray TextArray_ko;
	JSONArray TextArray_en;
	JSONArray TextArray_ch;
	JSONArray TextArray_jp;
	JSONArray AudioArray;
	String SMI_folder = "smi";
	String Result_folder = "src/main/webapp/resources/finalResult";
	String Audio_folder = "audio";
	String Blank_folder = "blank";
	String Temp_folder = "temp";
	boolean isSmiExist;
	String Result_Path = "./"+Result_folder+"/";//비디오 경로
	String Text_Path = Result_Path+SMI_folder+"/";
	String Audio_Path = Result_Path+Audio_folder+"/";//오디오 경로
	String Temp_Path = Result_Path+Temp_folder+"/";//오디오 경로
	String Blank_Path = Result_Path+Blank_folder+"/";//오디오 경로

	String SMI_Path = Text_Path+"sample.smi";
	
	String Result_video;//비디오 경로+확장자
	String Result_ext = ".mp4"; //결과물 확장자
	String Audio_ext = ".mp3"; //결과물 확장자

	
	File f;
	int video_index;
	int audio_index;
	int temp_index;
	int blank_index;

	public FfmpegConver(String ffmpeg,String Params,boolean isSmiExist) {
		// TODO Auto-generated constructor stub
		this.ffmpegPath = ffmpeg+"/"+"ffmpeg";
		this.ffprobePath = ffmpeg+"/"+"ffprobe"; 
		this.Params = Params;
		this.video_index = 0;
		this.audio_index = 0;
		this.temp_index = 0;
		this.blank_index = 0;
		this.isSmiExist=isSmiExist;
		make_folder();
		json_parse(this.Params);
	}
	
	public void make_folder(){

        String saveFolder=  Result_Path;  //경로        
        String saveFolder2 =  Audio_Path;  //경로     
        String saveFolder3 =  Temp_Path;  //경로     
        String saveFolder4 =  Blank_Path;  //경로     
        String saveFolder5 =  Text_Path;  //경로  


		File targetDir = new File(saveFolder);  
        File targetDir2 = new File(saveFolder2);  
        File targetDir3 = new File(saveFolder3);  
        File targetDir4 = new File(saveFolder4);  
        File targetDir5 = new File(saveFolder5);  
        
        if(!targetDir.exists()) {    //디렉토리 없으면 생성.
         targetDir.mkdirs();
        }
        if(!targetDir2.exists()) {    //디렉토리 없으면 생성.
        	targetDir2.mkdirs();
         }
        
        if(!targetDir3.exists()) {    //디렉토리 없으면 생성.
        	targetDir3.mkdirs();
         }
        if(!targetDir4.exists()) {    //디렉토리 없으면 생성.
        	targetDir4.mkdirs();
         }
        
        if(!targetDir5.exists()) {    //디렉토리 없으면 생성.
        	targetDir5.mkdirs();
         }

	}
	
	public void json_parse(String jsonInfo){
        try {
        	 
            JSONParser jsonParser = new JSONParser();
             
            //JSON데이터를 넣어 JSON Object 로 만들어 준다.
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonInfo);
             
            //books의 배열을 추출
            this.Video = (JSONObject)jsonObject.get("Video");
            this.TextArray_ko = (JSONArray) jsonObject.get("TextList_ko");
            this.TextArray_en = (JSONArray) jsonObject.get("TextList_en");
            this.TextArray_ch = (JSONArray) jsonObject.get("TextList_ch");
            this.TextArray_jp = (JSONArray) jsonObject.get("TextList_jp");
            System.err.println(TextArray_ko+":::"+TextArray_en+":::"+TextArray_ch+":::"+TextArray_jp);
            this.AudioArray = (JSONArray) jsonObject.get("AudioList");
            String video_url = this.Video.get("VideoUrl").toString();
            String[] split_string = video_url.split("/");
            String[] video_string = split_string[split_string.length-1].split("\\.");
            this.Result_video = Result_Path+video_string[0]+Result_ext;
            this.SMI_Path = Result_Path+video_string[0]+".smi";
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());	
        }
	}
	
/*	private void insert_smi(String ffmpegPath, JSONObject video){
			
		
		String[] cmdLine = new String[]{ffmpegPath,
									        "-i",                           // 변환시킬 파일위치
									
									        get_file_path(),
									    									        
									        "-filter_complex",
									
									        "[0:v]subtitles="+SMI_Path+":force_style='Fontsize=40':charenc=UTF-8",          //비트레이트           
									        
									        "-y",
									        
									        get_add_file_path()};  // 저장하는 위치입니다.
		exec_FFMPEG(cmdLine);
	}*/
	private void create_smi(JSONArray text_array_ko,JSONArray text_array_en,JSONArray text_array_ch,JSONArray text_array_jp){
        BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(SMI_Path));
	        /*if (null != bufferedWriter)
	            System.out.println("sample.smi 파일이 성공적으로 생성되었습니다."); */
			bufferedWriter.write("<SAMI>\n");
			bufferedWriter.write("<HEAD>\n");
			bufferedWriter.write("<TITLE>Idea concert</TITLE>\n");
			bufferedWriter.write("<STYLE TYPE='text/css'>\n");
			bufferedWriter.write("<!--\n");
			bufferedWriter.write("P { margin-left:8pt; margin-right:8pt; margin-bottom:2pt;\n");
			bufferedWriter.write("    margin-top:2pt; font-size:30pt; text-align:center;\n");
			bufferedWriter.write("    font-family:굴림, Arial; font-weight:normal; color:red;\n");
			bufferedWriter.write("    background-color:black; }\n");
			bufferedWriter.write(".KRCC { Name:한국어; lang:ko-KR; SAMIType:CC; }\n");
			bufferedWriter.write(".ENCC { Name:영어; lang:en-US; SAMIType:CC; }\n");
			bufferedWriter.write(".CNCC { Name:중국어; lang:zh-CN; SAMIType:CC; }\n");
			bufferedWriter.write(".JPCC { Name:일본어; lang:ja-jp; SAMIType:CC; }\n");
			bufferedWriter.write("-->\n");
			bufferedWriter.write("</STYLE>\n");
			bufferedWriter.write("</HEAD>\n");
			bufferedWriter.write("<BODY>\n");
			if(text_array_ko !=null){ //korea
				for(int i=0; i<text_array_ko.size(); i++){
				//루프 부분
					JSONObject textObject = (JSONObject) text_array_ko.get(i);
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyStart")+"000><P Class=KRCC >"+textObject.get("Text")+"\n");
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyEnd")+"000><P Class=KRCC>&nbsp;\n");
				}
			}
			
			if(text_array_en !=null){ //USA
				for(int i=0; i<text_array_en.size(); i++){
				//루프 부분
					JSONObject textObject = (JSONObject) text_array_en.get(i);
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyStart")+"000><P Class=ENCC >"+textObject.get("Text")+"\n");
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyEnd")+"000><P Class=ENCC>&nbsp;\n");
				}
			}
			if(text_array_ch !=null){ //china
				for(int i=0; i<text_array_ch.size(); i++){
				//루프 부분
					JSONObject textObject = (JSONObject) text_array_ch.get(i);
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyStart")+"000><P Class=CNCC >"+textObject.get("Text")+"\n");
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyEnd")+"000><P Class=CNCC>&nbsp;\n");
				}
			}
			if(text_array_jp !=null){ //japan
				for(int i=0; i<text_array_jp.size(); i++){
				//루프 부분
					JSONObject textObject = (JSONObject) text_array_jp.get(i);
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyStart")+"000><P Class=JPCC >"+textObject.get("Text")+"\n");
					bufferedWriter.write("<SYNC Start="+textObject.get("ApplyEnd")+"000><P Class=JPCC>&nbsp;\n");
				}
			}
			//루프 부분			
			bufferedWriter.write("</BODY>\n");
			bufferedWriter.write("</SAMI>\n");

			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void add_audio(String ffmpegPath,JSONArray audio){
		if(audio !=null){
			int count = this.audio_index;
		    for(int i =0; i<count;i++){
		    	String url = null;
		    	if(i == 0) url = get_audio_path(0);
		    	else url = get_audio_path();
				String[] cmdLine = new String[]{ffmpegPath,
				        "-i",                           // 변환시킬 파일위치
						
				        url,    
				        
						"-i",                           // 변환시킬 파일위치
						
						get_audio_path(i+1),      
				    									        
				        "-filter_complex",
				
				        "amix=inputs=2:duration=first:dropout_transition=0",          //비트레이트           
				
				        "-codec:a",
				
				        "libmp3lame",

				        "-y",
				        
				        get_add_audio_path()};  // 저장하는 위치입니다.	
					exec_FFMPEG(cmdLine);
		    }
		}

	}
		
	
	private void extract_audio(String ffmpegPath,JSONArray audio){
		if(audio !=null){
			JSONObject audioObject = null;
			String CutStart = null;

			String AudioUrl = null;
			String CutEnd= null;
			String RealCE = null;
			String ApplyStart = null;

			int ex = 0;
			for(int i=0; i<audio.size(); i++){
				audioObject = (JSONObject) audio.get(i);
				AudioUrl = audioObject.get("AudioUrl").toString();
				CutStart = audioObject.get("CutStart").toString();
				CutEnd = audioObject.get("CutEnd").toString();
				ApplyStart = audioObject.get("ApplyStart").toString();
				ex = Integer.parseInt(CutEnd) -  Integer.parseInt(CutStart);
				RealCE = Integer.toString(ex);
				String[] cmdLine = new String[]{ffmpegPath,
				        "-i",                           // 변환시킬 파일위치
						
				        AudioUrl,      
				    									        
				        "-acodec",
				
				        "copy",          //비트레이트           
				
				        "-ss",
				
				        CutStart,
				        
				        "-t",
						
				         RealCE,

				        "-y",
				        
				        get_add_temp_audio_path()};  // 저장하는 위치입니다.	
					exec_FFMPEG(cmdLine);
					
					cmdLine = new String[]{ffmpegPath,
					        "-f",                           // 변환시킬 파일위치
							
					        "lavfi",      
					    									        
					        "-i",
					
					        "aevalsrc=0:0::duration="+ApplyStart,          //비트레이트           
					
					        "-ab",
					
					        "320k",

					        "-y",
					        
					        get_add_blank_audio_path()};  // 저장하는 위치입니다.	
						exec_FFMPEG(cmdLine);
					
						cmdLine = new String[]{ffmpegPath,
						        "-i",                           // 변환시킬 파일위치
								
						        "concat:\""+get_blank_audio_path()+"|"+get_temp_audio_path()+"\"",      
						    									        
						        "-codec",
												
						        "copy",

						        "-y",
						        
						        get_add_audio_path()};  // 저장하는 위치입니다.	
							exec_FFMPEG(cmdLine);
			}
		}

	}
	private String exec_FFMPEG(String[] cmdLine){
		String cmd="";
		for(int i=0;i<cmdLine.length;i++) {
			cmd+=cmdLine[i]+" ";
		}
		System.out.println("cmdLine+ : "+cmd);

		
		String result = "";
		// 프로세스 속성을 관리하는 ProcessBuilder 생성.
				ProcessBuilder pb = new ProcessBuilder(cmdLine);
				pb.redirectErrorStream(true);
				Process p = null;
				 try { 
				        // 프로세스 작업을 실행
				        p = pb.start();
				} catch (Exception e) {         
				     e.printStackTrace();
				     p.destroy();
				     System.out.println(e.getMessage());
				}

				 result = exhaustInputStream(p.getInputStream());   // 자식 프로세스에서 발생되는 inputstrem를 소비시켜야합니다.
				
				  try {

				        // p의 자식 프로세스의 작업이 완료될 동안 p를 대기시킴
				        p.waitFor();

				 } catch (InterruptedException e) {

				         p.destroy();
				         System.out.println(e.getMessage());

				 }


				// 정상 종료가 되지 않았을 경우

				 if (p.exitValue() != 0) {
					 System.out.println("변환 중 에러 발생");
				 }

				   // 변환을 하는 중 에러가 발생하여 파일의 크기가 0일 경우

				  p.destroy();
				  return result;
	}
	
	private void merge_media(String ffmpegPath,JSONObject Video_data,JSONArray audio){
		String[] cmdLine = new String[]{ffmpegPath,
		        "-i",                           // 변환시킬 파일위치
		        
		        get_file_path(),      
		        		
		        "-i",                           // 변환시킬 파일위치
		        
		        get_audio_path(),
		        
		        "-c:v",
		        
		        "copy",
		        
		        "-c:a",
		        
		        "aac",
		        
		        "-strict",
		        
		        "experimental",
		        		        
		        get_add_file_path()};  // 저장하는 위치입니다.

		//System.out.println(cmdLine.toString());
		exec_FFMPEG(cmdLine);
	}
	
	private void convert(String ffmpegPath, String ffprobePath, JSONObject Video_data) {
		
		String orginal_url = Video_data.get("VideoUrl").toString();
		String orginal_width = Video_data.get("Width").toString();
		String orginal_height = Video_data.get("Height").toString();
		String orginal_fps = Video_data.get("Fps").toString();
		String[] cmdLine = null;
		String result = "";
		cmdLine = new String[]{ffmpegPath,
		        "-i",                           // 변환시킬 파일위치
		        
		        orginal_url,      
		        		
		        "-s",
				
		        orginal_width+"x"+orginal_height,     //화면 사이즈
		
		        "-r",
		
		        orginal_fps,           //영상 프레임
				
		        "-f",
		
		        "mp4",

		        "-y",
		        
		        "-an",
		        
		        get_file_path()};  // 저장하는 위치입니다.

		exec_FFMPEG(cmdLine);
		
		
		cmdLine = new String[]{ffprobePath,
		        "-i",                           // 변환시킬 파일위치
		        
		        orginal_url,      
		        		
		        "-show_streams",
		        
		        "-select_streams",
		        
		        "a",

		        "-loglevel",
		        
		        "error"};  // 저장하는 위치입니다.

				result = exec_FFMPEG(cmdLine);
				if(result == ""){

					cmdLine = new String[]{ffprobePath,
					        "-i",                           // 변환시킬 파일위치
					        
					        orginal_url,      
					        		
					        "-show_entries",
					        
					        "format=duration",
					        
					        "-v",

					        "quiet",

					        "-of",

					        "csv=\"p=0\""};  // 저장하는 위치입니다.

						result = exec_FFMPEG(cmdLine);
						
						cmdLine = new String[]{ffmpegPath,
						        "-f",                           // 변환시킬 파일위치
								
						        "lavfi",      
						    									        
						        "-i",
						
						        "aevalsrc=0:0::duration="+result,          //비트레이트           
						
						        "-ab",
						
						        "320k",

						        "-y",
						        
						        get_audio_path()};  // 저장하는 위치입니다.	
						exec_FFMPEG(cmdLine);
				}else {
					cmdLine = new String[]{ffmpegPath,
					        "-i",                           // 변환시킬 파일위치
					        
					        orginal_url,      
					        		
					        "-vn",
					        
					        "-y",
					        
					        get_audio_path()};  // 저장하는 위치입니다.

					exec_FFMPEG(cmdLine);					
				}
	}

		private String exhaustInputStream(final InputStream is) {
		    // InputStream.read() 에서 블럭상태에 빠지기 때문에 따로 쓰레드를 구현하여 스트림을 소비한다
		         try {

		                BufferedReader br = new BufferedReader(new InputStreamReader(is));

		                String cmd = null;
		                String commend = "";

		                while((cmd = br.readLine()) != null) { // 읽어들일 라인이 없을때까지 계속 반복
		                	commend += cmd+"\n";
		                   //System.out.println(cmd);

		                }

		                br.close();
		                return commend;
		             } catch(IOException e) {

		                e.printStackTrace();
		                return "";
		             }

		   }
		public String get_file_path(){
			System.out.println(Result_Path+video_index+Result_ext);
			return Result_Path+video_index+Result_ext;
		}
		public String get_add_file_path(){
			video_index++;
			System.out.println(Result_Path+video_index+Result_ext);
			return Result_Path+video_index+Result_ext;
		}
		public String get_audio_path(){
			return Audio_Path+audio_index+Audio_ext;
		}
		public String get_add_audio_path(){
			audio_index++;
			return Audio_Path+audio_index+Audio_ext;
		}
		public String get_audio_path(int k){
			return Audio_Path+k+Audio_ext;
		}
		public String get_temp_audio_path(){
			return Temp_Path+temp_index+Audio_ext;
		}
		public String get_blank_audio_path(){
			return Blank_Path+blank_index+Audio_ext;
		}

		public String get_add_temp_audio_path(){
			temp_index++;
			return Temp_Path+temp_index+Audio_ext;
		}
		public String get_add_blank_audio_path(){
			blank_index++;
			return Blank_Path+blank_index+Audio_ext;
		}

		public void delete_all_tmp_file(){
			
			String filename = get_file_path();
		    File file = new File( filename );
		    File fileNew = new File( Result_video );
		    if(fileNew.exists()){
		    	fileNew.delete();
		    }
		    if(file.exists())file.renameTo( fileNew );
		    
		    for(int i =0; i<=this.video_index;i++){
		    	File file_tmp = new File(Result_Path+i+Result_ext);
		    	if(file_tmp.exists())
		        	file_tmp.delete();
		    }
		    
		    
		    for(int i =0; i<=this.audio_index;i++){
		    	File file_tmp = new File(Audio_Path+i+Audio_ext);
		    	if(file_tmp.exists())
		        	file_tmp.delete();
		    }
		    
		    for(int i =0; i<=this.temp_index;i++){
		    	File file_tmp = new File(Temp_Path+i+Audio_ext);
		    	if(file_tmp.exists())
		        	file_tmp.delete();
		    }

		    for(int i =0; i<=this.blank_index;i++){
		    	File file_tmp = new File(Blank_Path+i+Audio_ext);
		    	if(file_tmp.exists())
		        	file_tmp.delete();
		    }

		}
		public String getResult() { 
			 delete_all_tmp_file();
			 return Result_video; 
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			convert(ffmpegPath,ffprobePath,this.Video);
			extract_audio(ffmpegPath,this.AudioArray);
			add_audio(ffmpegPath,this.AudioArray);
			merge_media(ffmpegPath,this.Video,this.AudioArray);
			//if(isSmiExist==false) {
				create_smi(this.TextArray_ko,this.TextArray_en,this.TextArray_ch, this.TextArray_jp);
				//insert_smi(ffmpegPath,this.Video);
			//}
		}
}
