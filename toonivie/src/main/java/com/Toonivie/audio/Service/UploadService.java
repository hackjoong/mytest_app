package com.Toonivie.audio.Service;

import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.Toonivie.audio.Util.CreateFileUtils;

@Service
public class UploadService {

    public HashMap<String,Object> uploadFile(MultipartFile file,String rootPath,String middlePath){
        HashMap<String,Object> resultMap = new HashMap<>();
        CreateFileUtils cfu = new CreateFileUtils();
        String filename=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
        cfu.CreateFile(file, rootPath, middlePath, filename);
        resultMap.put("resultUrl",middlePath+filename);
        return resultMap;
    }
    
    public HashMap<String,Object> putVideoInfo(HashMap<String,Object> resultMap,String rootPath) {
    	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    	String fileFullPath = rootPath+resultMap.get("resultUrl").toString();
    	VideoCapture videoCapture = new VideoCapture(fileFullPath);
    	if(!videoCapture.isOpened()) {
    		System.out.println("error!");
    	}
    	resultMap.put("width",(int)videoCapture.get(Videoio.CAP_PROP_FRAME_WIDTH));
    	resultMap.put("height",(int)videoCapture.get(Videoio.CAP_PROP_FRAME_HEIGHT));
    	resultMap.put("fps",(int)videoCapture.get(Videoio.CAP_PROP_FPS));
    	return resultMap;
    }
}
