package com.Toonivie.comment;



import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Security.SecAlgorithm;
import com.Toonivie.Signup.SignupValidate;
import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;
import com.Toonivie.Vo.UserVo;

@Controller

public class CommentController {

	@Autowired
	CommentService Commentservice;
	@Autowired
	SignupValidate signupValidate;


	@RequestMapping("/comment/list")
	@ResponseBody
	private List<CommentVo> list(Model model, int tno) throws Exception {
		model.addAttribute("countComment",Commentservice.countComment(tno));
		return Commentservice.list(tno);
	}

	@RequestMapping("/comment/insert")
	@ResponseBody
	private int insertComment(@RequestParam int tno, @RequestParam String comment, @RequestParam String pw, @RequestParam String nickname, HttpSession session)
			throws Exception {
		CommentVo vo = new CommentVo();
		
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if(currentUser==null) {
			System.out.println("null");
			pw=signupValidate.hashPw(pw);
		}
		vo.setTno(tno);
		vo.setComment(comment);
		vo.setNickname(nickname);
		vo.setPw(pw);
		
		Commentservice.CountCommentUp(tno);

		return Commentservice.inputComment(vo);
	}

	@RequestMapping("/comment/update") 
	@ResponseBody
	private int mCommentServiceUpdateProc(@RequestParam int cno, @RequestParam String comment) throws Exception {

		CommentVo vo = new CommentVo();
		vo.setCno(cno);
		vo.setComment(comment);

		return Commentservice.commentUpdateService(vo);
	}

	//@RequestMapping("/comment/delete/{cno}") //
	@RequestMapping("/comment/delete") //
	@ResponseBody
	private int mCommentServiceDelete(@RequestParam int cno, @RequestParam int tno) throws Exception {
		
		Commentservice.CountCommentDel(tno);

		return Commentservice.commentDeleteService(cno);
	}

	//
	@RequestMapping("/dap/insert")
	@ResponseBody
	private int insertDap(@RequestParam int cno, @RequestParam String content, @RequestParam("nickname")String nickname,@RequestParam String pwd, HttpSession session) throws Exception {
		DapVo vo = new DapVo();
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if(currentUser==null) {
			System.out.println("null");
			pwd=signupValidate.hashPw(pwd);
		}
		vo.setContent(content);
		vo.setDwriter(nickname);
		vo.setPwd(pwd);
		vo.setCno(cno);
		Commentservice.CountDapUp(cno);
		return Commentservice.inputdap(vo);
	}

	@RequestMapping("/dap/list")
	@ResponseBody
	private List<DapVo> dapList(@RequestParam Integer cno) throws Exception {
		return Commentservice.dapList(cno);
	}
	
	@RequestMapping("/dap/update") // 
	@ResponseBody
	private int mDapServiceUpdateProc(@RequestParam int dno, @RequestParam("content") String content, @RequestParam String dwriter) throws Exception {

		DapVo vo = new DapVo();
		vo.setDno(dno);
		vo.setContent(content);
		vo.setDwriter(dwriter);

		return Commentservice.dapUpdateService(vo);
	}

	@RequestMapping("/dap/delete/{dno}") //
	@ResponseBody
	private int mDapServiceDelete(@PathVariable int dno, @RequestParam("cno") int cno) throws Exception {
		Commentservice.CountDapDown(cno);
		return Commentservice.dapDeleteService(dno);
	}
	
	
	
	//�꽴占썹뵳�딆쁽占쎈읂占쎌뵠筌욑옙!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	@RequestMapping("/admin/commentList")
	public String adminCoList(Model model) throws Exception {
		//占쎈솊疫뀐옙 �겫占썽겫占�
		model.addAttribute("CommentList", Commentservice.admincoAll());
		model.addAttribute("CountCommentAll", Commentservice.count());
		//占쎈뼗疫뀐옙 �겫占썽겫占�
		model.addAttribute("replyList", Commentservice.admindapAll());
		model.addAttribute("CountDapAll", Commentservice.countdap());
		return "adminPage/commentList";
	}
	
	@RequestMapping("/admin/commentmodify")
	@ResponseBody
	public int Admincommodify(HttpServletRequest request) {
		String cnoSt = request.getParameter("cno");
		int cno = Integer.parseInt(cnoSt);
		String comment = request.getParameter("comment");
		
		CommentVo vo = new CommentVo();
		vo.setCno(cno);
		vo.setComment(comment);
		
		int resultVal = Commentservice.Admincommodify(vo);
		
		return resultVal;
		
	}
	
	@RequestMapping("/admin/commentdelete")
	@ResponseBody
	public int Admincomdelete(HttpServletRequest request) throws Exception {
		String cnoSt = request.getParameter("cno");
		int cno = Integer.parseInt(cnoSt);
		System.out.println(cno);
		int resultVal = Commentservice.commentDeleteService(cno);
		return resultVal;
	}
	
	@RequestMapping("/admin/dapmodify")
	@ResponseBody
	public int Admindapmodify(HttpServletRequest request) throws Exception {
		String dapSt = request.getParameter("dno");
		int dno = Integer.parseInt(dapSt);
		String content = request.getParameter("content");
		String dwriter = request.getParameter("dwriter");
		
		DapVo vo = new DapVo();
		vo.setDno(dno);
		vo.setContent(content);
		vo.setDwriter(dwriter);
		
		int resultVal = Commentservice.dapUpdateService(vo);
		return resultVal;
	}
	
	@RequestMapping("/admin/dapdelete")
	@ResponseBody
	public int Admindapdelete(HttpServletRequest request) throws Exception {
		String cnoSt = request.getParameter("dno");
		int dno = Integer.parseInt(cnoSt);
		
		int resultVal = Commentservice.dapDeleteService(dno);
		return resultVal;
	}
	//�뙎湲� �닔�젙,�궘�젣�떆 鍮꾨�踰덊샇 泥댄겕遺�遺�
	@RequestMapping("/comment/checkPw")
	@ResponseBody
	public int checkPw(HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String inputpw = request.getParameter("inputPw");
		int cno = Integer.parseInt(request.getParameter("cno"));
		String realpw = Commentservice.getPw(cno);
		boolean checkpw = SecAlgorithm.validatePassword(inputpw, realpw);
		//boolean checkpw = (inputpw.equals(realpw));
		
		if(checkpw == true) {
			return 1;
		}
		else {
			return 0;
		}
		
	}	
	//답글 수정,삭제시 비밀번호 체크부분
	@RequestMapping("/dap/checkPw")
	@ResponseBody
	public int checkPwForDap(HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String inputpw = request.getParameter("inputPw");
		int dno = Integer.parseInt(request.getParameter("dno"));
		String realpw = Commentservice.getPwForDap(dno);
		
		boolean checkpw = SecAlgorithm.validatePassword(inputpw, realpw);
		//boolean checkpw = (inputpw.equals(realpw));

		
		if(checkpw == true) {
			return 1;
		}
		else {
			return 0;
		}
		
	}	
}