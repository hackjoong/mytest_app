package com.Toonivie.comment;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;

@Mapper
public interface CommentDao {

	// ��� �Է�
	public int insertComment(CommentVo vo) throws Exception;
	
	// ��� ���� ������Ʈ+1
	public int CountCommentUp(int tno) throws Exception;

	// ��� ���
	public List<CommentVo> commentList(int tno) throws Exception;

	// ��� ����
	public int commentUpdate(CommentVo vo) throws Exception;

	// ��� ����
	public int commentDelete(int cno) throws Exception;
	
	//��۰� ���� ��۱��� ����
	public int commentForDap(int cno) throws Exception;
	
	// ��� ���� ������Ʈ-1
	public int CountCommentDel(int tno) throws Exception;

	// ��� �Է�
	public int insertdap(DapVo vo) throws Exception;

	// ��� ���
	public List<DapVo> dapList(int cno) throws Exception;

	// ��� ����
	public int dapUpdate(DapVo vo) throws Exception;

	// ��� ����
	public int dapDelete(int dno) throws Exception;
	
	// ��� ����
	public int countComment(int tno) throws Exception;
	
	//adminpage
	//��� ��ü �ҷ�����
	public List<CommentVo> admincoAll() throws Exception;
	
	//��� ����
	public int count();
	
	//��� ��ü �ҷ�����
	public List<DapVo> admindapAll() throws Exception;

	//��� ����
	public int countdap();
	
	//��� ����
	public int Admincommodify(CommentVo vo);
	
	// ��� ���� ������Ʈ+1
	public int CountDapUp(int cno) throws Exception;
	// ��� ���� ������Ʈ-1
	public int CountDapDown(int cno) throws Exception;
	//��ۻ��� ������ ���� ��й�ȣ �˻�
	public String getPw(int cno);
	//��ۻ��� ������ ���� ��й�ȣ �˻�
	public String getPwForDap(int dno);
}