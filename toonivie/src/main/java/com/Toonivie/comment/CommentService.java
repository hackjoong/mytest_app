package com.Toonivie.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.CommentVo;
import com.Toonivie.Vo.DapVo;

@Service
public class CommentService {

	@Autowired
	CommentDao commentdao;

	// inputcomment
	public int inputComment(CommentVo vo) throws Exception {
		return commentdao.insertComment(vo);
	}
	
	// comment+1
	public int CountCommentUp(int tno) throws Exception {
		return commentdao.CountCommentUp(tno);
	}

	// commentList
	public List<CommentVo> list(int tno) throws Exception {
		return commentdao.commentList(tno);
	}

	// commentUpdate
	public int commentUpdateService(CommentVo vo) throws Exception {
		return commentdao.commentUpdate(vo);
	}

	// commendDelete
	public int commentDeleteService(int cno) throws Exception {
		commentdao.commentForDap(cno);
		return commentdao.commentDelete(cno);
	}
	
	//dap
	public int commentForDap(int cno) throws Exception{
		return commentdao.commentDelete(cno);
	}
	
	// dap-1
	public int CountCommentDel(int tno) throws Exception {
		return commentdao.CountCommentDel(tno);
	}

	// insertDap
	public int inputdap(DapVo vo) throws Exception {
		return commentdao.insertdap(vo);
	}

	// dapList
	public List<DapVo> dapList(int cno) throws Exception {
		return commentdao.dapList(cno);
	}

	// dapupdate
	public int dapUpdateService(DapVo vo) throws Exception {
		return commentdao.dapUpdate(vo);
	}

	// dapdelete
	public int dapDeleteService(int dno) throws Exception {
		return commentdao.dapDelete(dno);
	}
	
	// countcomment
	public int countComment(int tno) throws Exception {
		return commentdao.countComment(tno);
	}
	
	//adminpage
	//commentAll
	public List<CommentVo> admincoAll() throws Exception{
		return commentdao.admincoAll();
	}
	
	//commentcount
	public int count() {
		return commentdao.count();
	}
	
	//dapAll
	public List<DapVo> admindapAll() throws Exception{
		return commentdao.admindapAll();
	}
	
	//dapcount
	public int countdap() {
		return commentdao.countdap();
	}
	
	//modify admin comment
	public int Admincommodify(CommentVo vo) {
		return commentdao.Admincommodify(vo);
	}
	
	// countdap+1
	public int CountDapUp(int cno) throws Exception {
		return commentdao.CountDapUp(cno);
	}
	
	// countdap-1
	public int CountDapDown(int cno) throws Exception {
		return commentdao.CountDapDown(cno);
	}
	
	//check pw
	public String getPw(int cno) {
		return commentdao.getPw(cno);
	}
	//dap pw get
	public String getPwForDap(int dno) {
		return commentdao.getPwForDap(dno);
	}
}