package com.Toonivie.common;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CommonController {

	@Autowired
	CommonService commonService;
	

	@RequestMapping("/searchItem")
	public ModelAndView searchToon(HttpServletRequest request) throws Exception{
		return commonService.searchToon(request,"/home/index");
	}
	
	@RequestMapping("/webtoon_wonjak/searchItem")
	public ModelAndView wonjak_searchToon(HttpServletRequest request) throws Exception{
		return commonService.wonjakSearch(request,"/wonjak/wonjakList");
	}
	@RequestMapping("/menu/writer")
	public ModelAndView writermenu(HttpServletRequest request) {
		return commonService.writermenu(request,"menu/writerList");
	}
}
