package com.Toonivie.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.Toonivie.Member.MemberDao;
import com.Toonivie.Slidebanner.SlidebannerDao;
import com.Toonivie.Slidebanner.SlidebannerService;
import com.Toonivie.ToonInfo.ToonInfoDao;
import com.Toonivie.Util.PageNavigator;
import com.Toonivie.Webtoon2D.Webtoon2DDao;
import com.Toonivie.series.SeriesDao;
import com.Toonivie.uploadWebtoon.UploadWebtoonDao;

@Service
public class CommonService {
	@Autowired
	ToonInfoDao tooninfoDao;
	@Autowired
	UploadWebtoonDao dao;
	@Autowired
	SlidebannerService service;
	@Autowired
	Webtoon2DDao webtoon2dDao;
	@Autowired
	SlidebannerService slidebannerSerivce;
	@Autowired
	SlidebannerDao slidebannerDao;
	@Autowired
	SeriesDao seriesDao;
	@Autowired
	MemberDao memberDao;
	
	//series
	public ModelAndView main(HttpServletRequest request, String viewName) throws Exception {
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}

		//series
		String kinds = "webmovie";
		int totalContents = seriesDao.SeriesList(kinds).size();
		
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		model.addObject("nation",nation);
		model.addObject("toonsunwi", dao.getContents_sunwi());
		model.addObject("banList",service.BannerMain(kinds));
		model.addObject("series",this.getIndexList(nation.getPageMap()));
		return model;
	}
	
	public List<Map<String, Object>> getSeriesAndtoonList(Map<String, String> map){
		return seriesDao.SeriesAndtoonListMap(map);		
	}
	public List<Map<String, Object>> getIndexList(Map<String, String> map){
		return seriesDao.indexListMap(map);		
	}
	//webmovie
	/*public ModelAndView main(HttpServletRequest request, String viewName) throws Exception {
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		int totalContents = tooninfoDao.ToonCount();
		String kinds = "webmovie";
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		model.addObject("nation",nation);
		//model.addObject("toon", dao.getContents());
		model.addObject("toonsunwi", dao.getContents_sunwi());
		model.addObject("banList",service.BannerMain(kinds));
		model.addObject("newtoon", this.getToonList(nation.getPageMap()));
		return model;
	}
	
	public List<Map<String, Object>> getToonList(Map<String, String> map){
		return tooninfoDao.toonall(map);		
	}*/
	

	//getId_180207
	public String getIdToon(int tno) {
		return tooninfoDao.getIdToon(tno);
	}
	
	public String writerprofileToon(String nickname) {
		return tooninfoDao.writerprofileToon(nickname);
	}

	//series paging and search
	public ModelAndView searchToon(HttpServletRequest request, String viewName) throws Exception{
		ModelAndView model = new ModelAndView(viewName);
		String queryStr = "";
		String search = request.getParameter("searchValue");
		int totalContents;
		if(search != null) {
			queryStr += "(wmtitle like concat('%','"+search+"','%') or wmwriter like concat('%','"+search+"','%'))";
		}
		Map<String, String> searchMap = new HashMap<String,String>();
		Map<String, Object> map = new HashMap<String,Object>();
		
		searchMap.put("queryStr", queryStr);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
			totalContents = tooninfoDao.selectSeriesListCount(searchMap);
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		String kinds = "webmovie";
		nation.getPageMap().put("queryStr", queryStr);
		//model.addObject("newtoon", this.getTooninfoList(nation.getPageMap()));
		model.addObject("series",this.getSeriesAndtoonList(nation.getPageMap()));
		model.addObject("nation",nation);
		model.addObject("toonsunwi", dao.getContents_sunwi());
		model.addObject("search_data", search);
		model.addObject("banList",service.BannerMain(kinds));
		return model;
	}
	
	//webmovie paging and search
	/*public ModelAndView searchToon(HttpServletRequest request, String viewName) throws Exception{
		ModelAndView model = new ModelAndView(viewName);
		String queryStr = "";
		String search = request.getParameter("searchValue");
		int totalContents;
		if(search != null) {
			queryStr += "(tooninfo.title like concat('%','"+search+"','%') or tooninfo.writer like concat('%','"+search+"','%'))";
		}
		Map<String, String> searchMap = new HashMap<String,String>();
		Map<String, Object> map = new HashMap<String,Object>();
		
		searchMap.put("queryStr", queryStr);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}

			totalContents = tooninfoDao.selectToonListCount(searchMap);

		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		String kinds = "webmovie";
		nation.getPageMap().put("queryStr", queryStr);
		model.addObject("newtoon", this.getTooninfoList(nation.getPageMap()));
		model.addObject("nation",nation);
		model.addObject("toonsunwi", dao.getContents_sunwi());
		model.addObject("search_data", search);
		model.addObject("banList",service.BannerMain(kinds));
		return model;
	}*/
	public List<Map<String, Object>> getTooninfoList(Map<String, String> map){
		return tooninfoDao.getToonList(map);
	}
	//series paging and search
	public ModelAndView wonjakSearch(HttpServletRequest request, String viewName) throws Exception{
		ModelAndView model = new ModelAndView(viewName);
		String queryStr = "";
		String search = request.getParameter("searchValue");
		int totalContents;
		if(search != null) {
			queryStr += "(wmtitle like concat('%','"+search+"','%') or wmwriter like concat('%','"+search+"','%'))";
		}
		Map<String, String> searchMap = new HashMap<String,String>();
		Map<String, Object> map = new HashMap<String,Object>();
		
		searchMap.put("queryStr", queryStr);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
			totalContents = tooninfoDao.selectSeriesListCountwonjak(searchMap);
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		String kinds = "origintoon";
		nation.getPageMap().put("queryStr", queryStr);
		model.addObject("newwonjak", this.getWebtooninfoList(nation.getPageMap()));
		model.addObject("nation",nation);
		model.addObject("wonjaksunwi", webtoon2dDao.fileSelAll_sunwi());
		model.addObject("search_data", search);
		model.addObject("banList",slidebannerSerivce.BannerMain(kinds));
		return model;
	}

	/*//wonjak paging and search
	public ModelAndView wonjakSearch(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		String queryStr = "";
		String search = request.getParameter("searchValue");
		int totalContents;
		if(search != null) {
			queryStr += "(webtooninfo.wtname like concat('%','"+search+"','%') or webtooninfo.wtwriter like concat('%','"+search+"','%'))";
		}
		Map<String, String> searchMap = new HashMap<String,String>();
		Map<String, Object> map = new HashMap<String,Object>();
		
		searchMap.put("queryStr", queryStr);
		int pageNum = 1;
		if(request.getParameter("pageNum")=="") {
			pageNum = 1;
		} else if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}

			totalContents = tooninfoDao.selectWonjakListCount(searchMap);

		PageNavigator nation = new PageNavigator(pageNum, totalContents, "marman", 5, 6);
		String kinds = "origintoon";
		nation.getPageMap().put("queryStr", queryStr);
		model.addObject("newwonjak", this.getWebtooninfoList(nation.getPageMap()));
		model.addObject("nation",nation);
		model.addObject("wonjaksunwi", webtoon2dDao.fileSelAll_sunwi());
		model.addObject("search_data", search);
		model.addObject("banList",slidebannerSerivce.BannerMain(kinds));
		return model;
	}*/
	public List<Map<String, Object>> getWebtooninfoList(Map<String, String> map){
		return tooninfoDao.getWonjakList(map);
	}
	
	public Map<String, Object> getnextAndprev(Map<String, String> map){
		return tooninfoDao.getNextAndprev(map);
		
	}
	//get ip today
	public int getTodayIp(String ip) {
		return memberDao.getTodayIp(ip);
	}
	
	//menu
	public ModelAndView writermenu(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		model.addObject("banList",slidebannerDao.BannerAll());
		model.addObject("toonwriters", tooninfoDao.getToonWriterList());
		model.addObject("oriwriters",tooninfoDao.getwonwriterList());
		return model;
	}


}
