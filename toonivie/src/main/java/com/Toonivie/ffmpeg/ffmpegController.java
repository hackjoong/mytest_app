package com.Toonivie.ffmpeg;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.ColorTrans.Color_Trans_Module;
import com.Toonivie.Util.CreateFileUtils;

@Controller
public class ffmpegController {
	///FFmpeg Convert
	///2017-05-15, shkwak
	///Version 0.1.0	
	/*
	@RequestMapping(value="/ffmpegConvert",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> ffmpegConvert(HttpServletRequest request) throws IOException{
		HashMap<String,Object> resultMap=new HashMap<String,Object>();
		String fileName = "rain20170718152606.avi"; //default
		String basePath = request.getSession().getServletContext().getRealPath("resources/"); //"/resources/cvMov"; //"C:\\Users\\shkwak\\Desktop\\cvimgs\\video";
		//fileName = request.getParameter("videofilename"); //Ajax POST 포함한 data 값 가져오기
		System.out.println(fileName + ", " + basePath); //ex. rain2017-04-12_23-48-25.avi
		
		ffmpegConverter ff = new ffmpegConverter(fileName, basePath);
		ff.convert();
		
		resultMap.put("result",fileName);
		
		return resultMap;
	}*/
	
	@RequestMapping(value="/ffmpegConvert",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> ffmpegConvert(MultipartHttpServletRequest req,HttpServletRequest request) throws IOException{
		HashMap<String,Object> resultMap=new HashMap<String,Object>();
		MultipartFile file = req.getFile("upfile");
	   	CreateFileUtils cfu = new CreateFileUtils();
		String rootPath = request.getSession().getServletContext().getRealPath("/");
//		String rootPath = "./ffmpeg/bin/";+
		String samplingRate = request.getParameter("sampling_rate");
		String bitRate = request.getParameter("bit_rate");
		String outputFormat = request.getParameter("output_format");
		String outputFileName = request.getParameter("ouputfilename");
//		String fileName=cfu.getToday(1)+"."+cfu.getFileType(file.getOriginalFilename());
		String fileName=outputFileName;
		cfu.CreateFile(file, request, "/resources/cvMov/", fileName+"."+outputFormat);
		int resolution_H = Integer.parseInt(request.getParameter("resolution"));
		int resolution_W = (int) Math.ceil((double)resolution_H * (double) 1.77777777777777);
		
		ffmpegConverter ff = new ffmpegConverter(fileName, rootPath, samplingRate, bitRate, outputFormat, outputFileName+"_"+resolution_H+"p", resolution_H, resolution_W);
		String outputName = ff.convert();
		
		resultMap.put("resolution", resolution_H);
		resultMap.put("videoUrl", "/resources/cvMov/" + outputName);
		
		return resultMap;
	} 

	@RequestMapping(value="/ffmpeg_framepick",method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String,Object> ffmpeg_framepick(MultipartHttpServletRequest req,HttpServletRequest request) throws IOException{
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		HashMap<String,Object> resultMap=new HashMap<String,Object>();
		MultipartFile file = req.getFile("img");
	   	CreateFileUtils cfu = new CreateFileUtils();
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String fileName= file.getOriginalFilename();
		String target_nameonly= "frame_cut" + cfu.getToday(1) +".png";
		String target_name = "/resources/cvimgs/" + target_nameonly;
		cfu.CreateFile(file, request, "/resources/cvMov/", fileName );
		ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(fileName, rootPath, target_name, "0.1");
		String result_name = ff.convert("/resources/cvMov/");
		System.out.println("RESULT !:" + rootPath+result_name);
		Mat result_img = Imgcodecs.imread(rootPath+result_name, -1);
		resultMap.put("resultUrl", "/resources/cvimgs/"+target_nameonly);
		resultMap.put("color_filename", target_nameonly);
		resultMap.put("width", result_img.width());
		resultMap.put("height", result_img.height());
		resultMap.put("videoUrl", fileName);
    	//color palette default 5 colors by KH 2018-03-05
    	Color_Trans_Module color_Trans = new Color_Trans_Module();
    	resultMap.put("color_palette", color_Trans.init_color_getpalette(target_nameonly, 5, request));
    	
		System.out.println(resultMap.toString());
		return resultMap;
	}
}
