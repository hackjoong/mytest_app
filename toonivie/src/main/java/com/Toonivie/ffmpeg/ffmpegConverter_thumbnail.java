package com.Toonivie.ffmpeg;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
 
public class ffmpegConverter_thumbnail{
 private String filename, basePath, cutSecond, outputFormat, outputFileName; //modify by shkwak, 2017-08-14
 
 public ffmpegConverter_thumbnail(String filename, String basePath, String outputFileName, String cutSecond) {
  this.filename = filename;
  this.basePath = basePath;
  this.cutSecond = cutSecond;
  this.outputFormat = "jpg";
  this.outputFileName = outputFileName;
  System.err.println(basePath);
  System.out.println("ffmpegConverter.java:20 - " + cutSecond +  ", " + outputFormat + ", " + outputFileName);
 }
 
 public String convert() {
        //File fOriginal = new File(basePath + "/resources/cvMov/" + fileName); //upload file path_name
	 	File fOriginal = new File(basePath + "/resources/uploadfiles/" + filename); //upload file path_name
        String outputName = outputFileName + "." + outputFormat;
        File fResult = new File(basePath  + "/resources/thumbnail/" + outputName); //output file path_name
        //String ffmpegPath = "./ffmpeg/bin/ffmpeg";
        String ffmpegPath = basePath+"resources\\lib\\ffmpeg\\bin\\ffmpeg";
        String[] cmdLine = new String[]{ffmpegPath, "-i", fOriginal.getPath(), "-an","-ss", cutSecond, "-r","1", "-vframes", "1", "-y",  fResult.getPath()};
        String cmd="";
		for(int i=0;i<cmdLine.length;i++) {
			cmd+=cmdLine[i]+" ";
		}
		System.err.println("cmdLine : "+cmd);
        
        // 만약 mp4 파일이라면 변환과정 없이 스킵~
        /*if(fOriginal.getPath().endsWith(".mp4")) {
         return outputName;
        }*/
              
        // 프로세스 속성을 관리하는 ProcessBuilder 생성.
        ProcessBuilder pb = new ProcessBuilder(cmdLine);
        pb.redirectErrorStream(true);
        Process p = null;
       
        try { 
            // 프로세스 작업을 실행함.
            p = pb.start();
           
        } catch (Exception e) {         
            e.printStackTrace();
            p.destroy();
            return null; 
        }
        
        // 자식 프로세스에서 발생되는 인풋스트림 소비시킴;;
        exhaustInputStream(p.getInputStream());   
          
        try {
            // p의 자식 프로세스의 작업이 완료될 동안 p를 대기시킴
            p.waitFor();
        } catch (InterruptedException e) {
            p.destroy();
        }
 
         // 정상 종료가 되지 않았을 경우
        if (p.exitValue() != 0) {
         System.out.println("변환 중 에러 발생");
            return null;
        }
 
        // 변환을 하는 중 에러가 발생하여 파일의 크기가 0일 경우
        if (fResult.length() == 0) {
            System.out.println("변환된 파일의 사이즈가 0임");
            return null;
        } else {
        	//System.out.println(fResult.);
        }
 		
        System.out.println("변환 성공 ^^");
        //fOriginal.delete(); // 원본 파일 삭제
        return outputName;
 }
 
 public String convert(String resultpath) {
     File fOriginal = new File(basePath + "/resources/cvMov/" + filename); //upload file path_name
	 //File fOriginal = new File(basePath + "/resources/uploadfiles/" + filename); //upload file path_name
     String outputName = outputFileName ;
     File fResult = new File(basePath  + outputName); //output file path_name
     //String ffmpegPath = "./ffmpeg/bin/ffmpeg";
     String ffmpegPath = basePath+"resources\\lib\\ffmpeg\\bin\\ffmpeg";
     String[] cmdLine = new String[]{ffmpegPath, "-i", fOriginal.getPath(), "-an","-ss", cutSecond, "-r","1", "-vframes", "1", "-y",  fResult.getPath()};
     String cmd="";
		for(int i=0;i<cmdLine.length;i++) {
			cmd+=cmdLine[i]+" ";
		}
		System.err.println("cmdLine : "+cmd);
     
     // 만약 mp4 파일이라면 변환과정 없이 스킵~
     /*if(fOriginal.getPath().endsWith(".mp4")) {
      return outputName;
     }*/
           
     // 프로세스 속성을 관리하는 ProcessBuilder 생성.
     ProcessBuilder pb = new ProcessBuilder(cmdLine);
     pb.redirectErrorStream(true);
     Process p = null;
    
     try { 
         // 프로세스 작업을 실행함.
         p = pb.start();
        
     } catch (Exception e) {         
         e.printStackTrace();
         p.destroy();
         return null; 
     }
     
     // 자식 프로세스에서 발생되는 인풋스트림 소비시킴;;
     exhaustInputStream(p.getInputStream());   
       
     try {
         // p의 자식 프로세스의 작업이 완료될 동안 p를 대기시킴
         p.waitFor();
     } catch (InterruptedException e) {
         p.destroy();
     }

      // 정상 종료가 되지 않았을 경우
     if (p.exitValue() != 0) {
      System.out.println("변환 중 에러 발생");
         return null;
     }

     // 변환을 하는 중 에러가 발생하여 파일의 크기가 0일 경우
     if (fResult.length() == 0) {
         System.out.println("변환된 파일의 사이즈가 0임");
         return null;
     } else {
     	//System.out.println(fResult.);
     }
		
     System.out.println("변환 성공 ^^");
     //fOriginal.delete(); // 원본 파일 삭제
     return outputName;
}
 
 public String seriesconvert() {
     //File fOriginal = new File(basePath + "/resources/cvMov/" + fileName); //upload file path_name
	 	File fOriginal = new File(basePath + "/resources/series/uploadfiles/" + filename); //upload file path_name
     String outputName = outputFileName + "." + outputFormat;
     File fResult = new File(basePath  + "/resources/series/thumbnail/" + outputName); //output file path_name
     String ffmpegPath = basePath+"resources\\lib\\ffmpeg\\bin\\ffmpeg";
     String[] cmdLine = new String[]{ffmpegPath, "-i", fOriginal.getPath(), "-an","-ss", cutSecond, "-r","1", "-vframes", "1", "-y",  fResult.getPath()};
     String cmd="";
		for(int i=0;i<cmdLine.length;i++) {
			cmd+=cmdLine[i]+" ";
		}
		System.err.println("cmdLine : "+cmd);
     
     // 만약 mp4 파일이라면 변환과정 없이 스킵~
     /*if(fOriginal.getPath().endsWith(".mp4")) {
      return outputName;
     }*/
           
     // 프로세스 속성을 관리하는 ProcessBuilder 생성.
     ProcessBuilder pb = new ProcessBuilder(cmdLine);
     pb.redirectErrorStream(true);
     Process p = null;
    
     try { 
         // 프로세스 작업을 실행함.
         p = pb.start();
        
     } catch (Exception e) {         
         e.printStackTrace();
         p.destroy();
         return null; 
     }
     
     // 자식 프로세스에서 발생되는 인풋스트림 소비시킴;;
     exhaustInputStream(p.getInputStream());   
       
     try {
         // p의 자식 프로세스의 작업이 완료될 동안 p를 대기시킴
         p.waitFor();
     } catch (InterruptedException e) {
         p.destroy();
     }

      // 정상 종료가 되지 않았을 경우
     if (p.exitValue() != 0) {
      System.out.println("변환 중 에러 발생");
         return null;
     }

     // 변환을 하는 중 에러가 발생하여 파일의 크기가 0일 경우
     if (fResult.length() == 0) {
         System.out.println("변환된 파일의 사이즈가 0임");
         return null;
     } else {
     	//System.out.println(fResult.);
     }
		
     System.out.println("변환 성공 ^^");
     //fOriginal.delete(); // 원본 파일 삭제
     return outputName;
}
 
 private void exhaustInputStream(final InputStream is) {
  // InputStream.read() 에서 블럭상태에 빠지기 때문에 따로 쓰레드를 구현하여 스트림을 소비한다.
  new Thread() {
   public void run() {
      try {
         BufferedReader br = new BufferedReader(new InputStreamReader(is));
         String cmd;
         while((cmd = br.readLine()) != null) { // 읽어들일 라인이 없을때까지 계속 반복
            System.out.println(cmd);
         }
      } catch(IOException e) {
         e.printStackTrace();
      }
   }
  }.start();
 }    
}