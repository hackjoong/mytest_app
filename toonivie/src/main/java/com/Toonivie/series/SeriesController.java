package com.Toonivie.series;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.ContentView.ContentViewDao;
import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.UserVo;
import com.Toonivie.Webtoon2D.Webtoon2DService;
import com.Toonivie.ffmpeg.ffmpegConverter_thumbnail;
import com.Toonivie.uploadWebtoon.UploadWebtoonDao;
import com.Toonivie.viewOrder.ViewOrderService;

@Controller
@RequestMapping("/series")
public class SeriesController {
	@Autowired
	SeriesService service;
	@Autowired
	SeriesDao seriesDao;
	@Autowired
	ViewOrderService VOService;
	@Autowired
	ContentViewDao contentViewDao;
	@Autowired
	UploadWebtoonDao uploadToonDao;
	@Autowired
	Webtoon2DService webtoonservice;
	
	//series upload yoodh 180221
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload_tooniviefile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		MultipartFile upfile = req.getFile("upfile");
		String kinds = request.getParameter("kinds"); //webmovie or origin toon check
		String wmthumbnail = null;
		String wmfile = null;
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); 
		if(kinds.equals("webmovie")) {
			//webmovie
			cfu.CreateFile(upfile, request, "/resources/series/uploadfiles/", filename);
			
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String cutSecond = "00:00:02"; //10 -> 02
			int idx = filename.indexOf(".");
			String outputFileName = filename.substring(0, idx);
			ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
			String outputName = ff.seriesconvert();
			wmthumbnail = "/resources/series/thumbnail/"+outputFileName+".jpg";
			wmfile = "/resources/series/uploadfiles/" + filename; // request.getParameter("upfile");
		}
		else if(kinds.equals("origintoon")) {
			//webmovie
			cfu.CreateFile(upfile, request, "/resources/series/thumbnail/", filename);
			wmthumbnail = "/resources/series/thumbnail/"+filename;
			wmfile = "/resources/series/uploadfiles/" + filename; // request.getParameter("upfile");
		}
	
		String id = request.getParameter("id");
		String wmgenre = request.getParameter("genre");
		String wmtitle = request.getParameter("title");
		String wmwriter = request.getParameter("writer");
		String wmcontent = request.getParameter("content");
		
		service.InputSeries(wmfile,wmthumbnail, id, wmgenre, wmtitle, wmwriter, wmcontent, kinds); // modify

		return filename;
	}
	
	//series view yoodh 180222
	@RequestMapping(value="/movie/{wmno}", method= RequestMethod.GET)
	public String webmovie2(@PathVariable int wmno, Model model, HttpSession session) throws Exception{
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String viewid = currentUser.getId();
			VOService.insertViewOrder(wmno ,viewid);
		}
		System.out.println("id:"+wmno);
		
		
		//seriesInfo
		model.addAttribute("seriesInfo", seriesDao.SeriesInfo(wmno));
		//seriesList
		model.addAttribute("seriesall", seriesDao.selectSeriesmovie(wmno));
		//ranking
		model.addAttribute("toonsunwi", uploadToonDao.getContents_sunwi());
		//review update
		contentViewDao.increaseViewCountSeries(wmno);
		return "series/seriesview";
	}
	//series view yoodh 180222
	@RequestMapping(value="/toon/{wmno}", method= RequestMethod.GET)
	public String origintoon(@PathVariable int wmno, Model model, HttpSession session) throws Exception{
		UserVo currentUser = (UserVo) session.getAttribute("currentUser");
		if (currentUser != null) {
			String viewid = currentUser.getId();
			VOService.insertViewOrder(wmno ,viewid);
		}
		System.out.println("id:"+wmno);
		//seriesInfo
		model.addAttribute("seriesInfo", seriesDao.SeriesInfo(wmno));
		//seriesList
		model.addAttribute("seriesall", seriesDao.selectSerieswonjak(wmno));
		//ranking
		model.addAttribute("toonsunwi", webtoonservice.fileSelAll_sunwi());
		//review update
		model.addAttribute("address","toon");
		contentViewDao.increaseViewCountSeries(wmno);
		System.out.println("adsf");
		return "series/seriesview";
	}
	
	//series delete yoodh 180221
	@RequestMapping("/delete/{wmno}")
	@ResponseBody
	public int ToonDelete(@PathVariable int wmno, HttpServletRequest request) throws Exception {
		
		
		int returnVal = seriesDao.SeriesDelete(wmno);
		if(returnVal ==1) {
			String rootPath = System.getProperty("user.dir")+"/src/main/webapp";
			
			String file = request.getParameter("file");
			String thumbnail = request.getParameter("thumbnail");
			
			File file_del = new File(rootPath+ file);
			File thumb_del = new File(rootPath+thumbnail);
			if (file_del.exists()) {
				file_del.delete();
			}
			if (thumb_del.exists()) {
				thumb_del.delete();
			}
			int toonWnoDel = seriesDao.toonWmnoDelete(wmno);
			return returnVal;
		}
		else {
			return returnVal;
		}
	}
	
	//series update yoodh 180221
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public int update_SeriesFile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		String changed = request.getParameter("movieChanged");
		System.out.println("changed: " + changed); //on-null
		int resultVal;
		int tno = Integer.parseInt(request.getParameter("wmno"));
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		String kinds = request.getParameter("kinds");
		String id = request.getParameter("idx");
		String thumbnail = null;
		String file = null;
		if(changed == null) { 
			MultipartFile upfile = req.getFile("upfile3");
			CreateFileUtils cfu = new CreateFileUtils();
			String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename());
			
			/*if(kinds.equals("webmovie")) {
				System.out.println("webmovie");
				cfu.CreateFile(upfile, request, "/resources/series/uploadfiles/", filename);
				String rootPath = request.getSession().getServletContext().getRealPath("/");
				String cutSecond = "00:00:02";
				int idx = filename.indexOf(".");
				String outputFileName = filename.substring(0, idx);
				ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
				String outputName = ff.seriesconvert();
				thumbnail = "/resources/series/thumbnail/"+outputFileName+".jpg";
				file = "/resources/series/uploadfiles/" + filename;
			}
			else if(kinds.equals("origintoon")) {
				System.out.println("origintoon");*/
				cfu.CreateFile(upfile, request, "/resources/series/thumbnail/", filename);
				thumbnail = "/resources/series/thumbnail/"+filename;
			/*}*/
			resultVal = service.UpdateSeriesInfo(tno, file,thumbnail,id, genre, title, writer, content,kinds);
		}
		else { 
			resultVal = service.UpdateSeriesInfo_noFile(tno,id, genre, title, writer, content, kinds);
		}		
		return resultVal;
	}


}
