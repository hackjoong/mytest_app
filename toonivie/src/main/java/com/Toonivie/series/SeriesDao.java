package com.Toonivie.series;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.Toonivie.Vo.SeriesVo;

@Mapper
public interface SeriesDao {
	public void InsertSeries(SeriesVo vo);
	
	public List<SeriesVo> SeriesList(String kinds);
	
	public List<SeriesVo> SeriesListWonjak(String kinds);
	
	public List<Map<String,Object>> indexListMap(Map<String,String> map);
	
	public List<Map<String,Object>> SeriesAndtoonListMap(Map<String,String> map);
	
	public List<SeriesVo> selectSeriesmovie(int wmno);
	
	public List<SeriesVo> selectSerieswonjak(int wmno);
	
	public SeriesVo SeriesInfo(int wmno);
	
	public int SeriesDelete(int wmno);
	
	public int UpdateSeriesInfo(SeriesVo vo);
	
	public int UpdateSeriesInfo_noFile(SeriesVo vo);
	
	public List<SeriesVo> mySeriesInfoMovie(String id);
	
	public List<SeriesVo> mySeriesInfoWonjak(String id);
	
	public List<SeriesVo> mySeriesInfoMovie_noId();
	
	public List<SeriesVo> mySeriesInfoWonjak_noId();
	
	public int toonWmnoDelete(int wmno);
}
