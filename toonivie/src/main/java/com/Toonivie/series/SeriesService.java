package com.Toonivie.series;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.SeriesVo;
import com.Toonivie.Vo.ToonVo;

@Service
public class SeriesService {
	@Autowired
	SeriesDao seriesDao;
	
	public void InputSeries(String file, String thumbnail, String id, String wmgenre, String title, String writer, String content, String kinds) {
		SeriesVo vo = new SeriesVo();
		vo.setWmfile(file);
		vo.setWmthumbnail(thumbnail);
		vo.setId(id);
		vo.setWmtitle(title);
		vo.setWmgenre(wmgenre);
		vo.setWmwriter(writer);
		vo.setWmcontent(content);
		vo.setKinds(kinds);
		
		seriesDao.InsertSeries(vo);
	}
	public int UpdateSeriesInfo(int wmno, String file, String thumbnail,String id, String genre, String title, String writer, String content, String kinds) { // modify
		SeriesVo vo = new SeriesVo();
		vo.setWmno(wmno);
		vo.setWmfile(file);
		vo.setWmthumbnail(thumbnail);
		vo.setWmgenre(genre);
		vo.setWmtitle(title);
		vo.setWmwriter(writer);
		vo.setWmcontent(content);
		vo.setId(id);
		vo.setKinds(kinds);

		return seriesDao.UpdateSeriesInfo(vo);
	}
	public int UpdateSeriesInfo_noFile(int wmno, String id, String genre, String title, String writer, String content, String kinds) { // modify
		SeriesVo vo = new SeriesVo();
		vo.setWmno(wmno);
		vo.setWmgenre(genre);
		vo.setWmtitle(title);
		vo.setWmwriter(writer);
		vo.setWmcontent(content);
		vo.setId(id);
		vo.setKinds(kinds);

		
		return seriesDao.UpdateSeriesInfo_noFile(vo);
	}
}
