package com.Toonivie.update;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Toonivie.Member.MemberService;

@Controller
public class UpdateMemberController {
	@Autowired
	MemberService service;
	@RequestMapping("/member/update")
	@ResponseBody
	public int updateMember(HttpServletRequest request) {		
		String nochanged = request.getParameter("noChangedPW");
		System.out.println(nochanged);
		String no = request.getParameter("no");
		int userseq = Integer.parseInt(no);
		String id = request.getParameter("id");
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String role = request.getParameter("auth");
		
		if(nochanged.equals("false")) {
			String pw = request.getParameter("pw");
			System.out.println("nochanged.equals(false) ininin!!!");
			return service.MemberUpdate(userseq, id, email, name, pw, role);
		}
		else {
			return service.MemberUpdate_2(userseq, id, email, name, role);
		}
		
	}
	
	
	@RequestMapping("/member/update_en")
	@ResponseBody
	public int updateMember_en(HttpServletRequest request) {		
		String nochanged = request.getParameter("noChangedPW_en");
		System.out.println("memberChanged_en: "+nochanged);
		String no = request.getParameter("no");
		int userseq = Integer.parseInt(no);
		String id = request.getParameter("id");
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String role = request.getParameter("auth");
		
		if(nochanged.equals("false")) {
			System.out.println("nochanged.equals(false) ininin!!!");
			String pw = request.getParameter("pw");
			return service.MemberUpdate(userseq, id, email, name, pw, role);
		}
		else {
			return service.MemberUpdate_2(userseq, id, email, name, role);
		}
		
	}
}
