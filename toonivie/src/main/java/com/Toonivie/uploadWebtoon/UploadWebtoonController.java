package com.Toonivie.uploadWebtoon;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.Toonivie.Util.CreateFileUtils;
import com.Toonivie.Vo.MultipartFileVo;
import com.Toonivie.ffmpeg.ffmpegConverter_thumbnail;

@Controller
@RequestMapping("/uploadWebtoon")
public class UploadWebtoonController {
	@Autowired
	UploadWebtoonService service;
	@Autowired
	HttpServletRequest request;
	@Autowired
	MultipartHttpServletRequest req;
	@Autowired
	HttpSession session;

	@RequestMapping(value = "/upload", method = {RequestMethod.POST, RequestMethod.GET})
	@ResponseBody
	public String upload_tooniviefile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		MultipartFile upfile = req.getFile("upfile");
		System.out.println(upfile);
		// webmovie file upload
		CreateFileUtils cfu = new CreateFileUtils();
		String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); // file naming
		cfu.CreateFile(upfile, request, "/resources/uploadfiles/", filename);
		int radio = 0;
		//webmovie ffmpeg add by yoo_dh0416, 2017-11-28
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String cutSecond = "00:00:02"; //10 -> 02
		int idx = filename.indexOf(".");
		String outputFileName = filename.substring(0, idx);
		ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
		String outputName = ff.convert();
		
		System.out.println(upfile.getOriginalFilename());
		String thumbnail = "/resources/thumbnail/"+outputFileName+".jpg";
		//webmovie ffmpeg END
		
		String file = "/resources/uploadfiles/" + filename; // request.getParameter("upfile");
		String id = request.getParameter("id");
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		Boolean free_open = Boolean.valueOf(request.getParameter("check_free"));
		/*int radio = Integer.parseInt(request.getParameter("seriestitle"));*/
		String radiost = request.getParameter("seriestitle");
		if(radiost == null) {
			radio = 0;
		}
		else {
			radio = Integer.parseInt(request.getParameter("seriestitle"));
		}
		
		System.err.println("id : "+id+" & genre : "+genre+" & title : "+title+" & writer : "+writer+" & content : "+content+" & file : "+file+" & thumbnail : "+thumbnail+" & radio : "+radio);
		service.InputTooninfo(file,thumbnail, id, genre, title, writer, content,radio, free_open); // modify

		return file;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update_tooniviefile(MultipartHttpServletRequest req, HttpSession session, HttpServletRequest request)
			throws Exception {
		String changed = request.getParameter("movieChanged");
		System.out.println("changed: " + changed); //on-null
		
		int tno = Integer.parseInt(request.getParameter("tno"));
		String genre = request.getParameter("genre");
		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		Boolean free_open = Boolean.valueOf(request.getParameter("check_free"));
		
		int wmno = Integer.parseInt(request.getParameter("seriestitle"));
		if(changed == null) {
			MultipartFile upfile = req.getFile("upfile3");

			CreateFileUtils cfu = new CreateFileUtils();
			String filename = cfu.getToday(1) + "." + CreateFileUtils.getFileType(upfile.getOriginalFilename()); // file naming
			cfu.CreateFile(upfile, request, "/resources/uploadfiles/", filename);
			
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String cutSecond = "00:00:02";
			int idx = filename.indexOf(".");
			String outputFileName = filename.substring(0, idx);
			ffmpegConverter_thumbnail ff = new ffmpegConverter_thumbnail(filename, rootPath, outputFileName, cutSecond);
			String outputName = ff.convert();

			String thumbnail = "/resources/thumbnail/"+outputFileName+".jpg";

			
			String file = "/resources/uploadfiles/" + filename;		
			service.UpdateTooninfo(tno, file,thumbnail, genre, title, writer, content,wmno, free_open);
		}
		else {
			service.UpdateTooninfo_noFile(tno, genre, title, writer, content,wmno, free_open);
		}		

		return "OK";
	}
	/*cropped image add by KH 2018-02-22*/
	@ResponseBody
	@RequestMapping(value ="/webtoon_upload_parts", method=RequestMethod.POST)
	public Map<String, Object> saveImage(MultipartFileVo vo, HttpServletRequest request) {
		Map<String, Object> hmap = new HashMap<String, Object>();
		try {
			double shrinking_ratio_h = Double.parseDouble(vo.getShrinking_ratio_h());
			double shrinking_ratio_w = Double.parseDouble(vo.getShrinking_ratio_w());
		//	double shrinking_ratio = 1;
			MultipartFile mFile = vo.getImgFile();
			CreateFileUtils cfu = new CreateFileUtils();
			String fileName = cfu.getToday(1)+"."+"png";
			cfu.CreateFile(mFile, request, "/resources/webtoon/", fileName);
			String real_path = request.getSession().getServletContext().getRealPath("/") + "/resources/webtoon/"+ fileName;
			System.out.println(real_path);
			BufferedImage bimg = ImageIO.read(new File(real_path));
			BufferedImage resized = resize(bimg, (int)(bimg.getHeight()/shrinking_ratio_h), (int)(bimg.getWidth()/shrinking_ratio_w));
			File outputfile = new File(real_path);
			ImageIO.write(resized, "png", outputfile);
			int width = resized.getWidth();
			int height = resized.getHeight();
			hmap.put("filepath", "/resources/webtoon/"+fileName);
			hmap.put("width", width);
			hmap.put("height", height);
			return hmap;
		} catch(Exception e) {
			e.printStackTrace();
			return hmap;
		}
	}
	
	/*cropped image add by KH 2018-02-22*/
	@ResponseBody
	@RequestMapping(value ="/webtoon_upload_bg", method=RequestMethod.POST)
	public Map<String, Object> saveImage_bg(MultipartFileVo vo, HttpServletRequest request) {
		Map<String, Object> hmap = new HashMap<String, Object>();
		try {
			double shrinking_ratio_h = Double.parseDouble(vo.getShrinking_ratio_h());
			double shrinking_ratio_w = Double.parseDouble(vo.getShrinking_ratio_w());
//			double shrinking_ratio = 1;
			MultipartFile mFile = vo.getImgFile();
			CreateFileUtils cfu = new CreateFileUtils();
			String fileName = "BG"+cfu.getToday(1)+"."+"png";
			cfu.CreateFile(mFile, request, "/resources/webtoon/", fileName);
			String real_path = request.getSession().getServletContext().getRealPath("/") + "/resources/webtoon/"+ fileName;
			BufferedImage bimg = ImageIO.read(new File(real_path));
			BufferedImage resized = resize(bimg, (int)(bimg.getHeight()/shrinking_ratio_h), (int)(bimg.getWidth()/shrinking_ratio_w));
			File outputfile = new File(real_path);
			ImageIO.write(resized, "png", outputfile);
			int width = resized.getWidth();
			int height = resized.getHeight();
			hmap.put("filepath", "/resources/webtoon/"+fileName);
			hmap.put("width", width);
			hmap.put("height", height);
			return hmap;
		} catch(Exception e) {
			e.printStackTrace();
			return hmap;
		}
	}

    public BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
}
