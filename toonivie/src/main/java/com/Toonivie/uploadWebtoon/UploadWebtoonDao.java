package com.Toonivie.uploadWebtoon;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.ToonVo;

@Mapper
public interface UploadWebtoonDao {

	public void InsertTooninfo(ToonVo toonVo);
	
	public void UpdateTooninfo(ToonVo toonVo);
	
	public void UpdateTooninfo_noFile(ToonVo toonVo);

	public List<ToonVo> getContents();
	
	public List<ToonVo> getContents_sunwi();
	
	public ToonVo getContentDetail(int tno);
}
