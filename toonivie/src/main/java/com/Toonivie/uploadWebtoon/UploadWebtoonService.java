package com.Toonivie.uploadWebtoon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.ToonVo;

@Service
public class UploadWebtoonService {
	@Autowired
	UploadWebtoonDao dao;

	public void InputTooninfo(String file,String thumbnail, String id, String genre, String title, String writer, String content,int radio, Boolean free_open) { // modify
		ToonVo vo = new ToonVo();
		vo.setFile(file);
		vo.setThumbnail(thumbnail);
		vo.setId(id);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setContent(content);
		vo.setWmno(radio);
		vo.setFree_open(free_open);

		dao.InsertTooninfo(vo);
	}

	public void UpdateTooninfo(int tno, String file, String thumbnail, String genre, String title, String writer, String content, int wmno, Boolean free_open) { // modify
		ToonVo vo = new ToonVo();
		vo.setTno(tno);
		vo.setFile(file);
		vo.setThumbnail(thumbnail);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setContent(content);
		vo.setWmno(wmno);
		vo.setFree_open(free_open);
		System.out.println(wmno);

		dao.UpdateTooninfo(vo);
	}
	public void UpdateTooninfo_noFile(int tno, String genre, String title, String writer, String content, int wmno, Boolean free_open) { // modify
		ToonVo vo = new ToonVo();
		vo.setTno(tno);
		vo.setGenre(genre);
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setContent(content);
		vo.setWmno(wmno);
		vo.setFree_open(free_open);
		System.out.println(wmno);

		dao.UpdateTooninfo_noFile(vo);
	}
	
	public ToonVo getContentDetail(int tno) {
		return dao.getContentDetail(tno);
	}

}
