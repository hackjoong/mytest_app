package com.Toonivie.viewOrder;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.ViewOrderVo;

@Mapper
public interface ViewOrderDao {

	public void insertViewOrder(ViewOrderVo vo);
	
	public List<ToonVo> viewAllTop6(String viewId);
}
