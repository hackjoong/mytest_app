package com.Toonivie.viewOrder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Toonivie.Vo.ToonVo;
import com.Toonivie.Vo.ViewOrderVo;

@Service
public class ViewOrderService {
	@Autowired
	ViewOrderDao VODao;
	
	public void insertViewOrder(int tno, String viewid) {
		ViewOrderVo vo = new ViewOrderVo();
		
		vo.setViewContentNo(tno);
		vo.setViewId(viewid);
		VODao.insertViewOrder(vo);
	}
	
	public List<ToonVo> viewAllTop6(String viewId){
		return VODao.viewAllTop6(viewId);
		
	}


}
