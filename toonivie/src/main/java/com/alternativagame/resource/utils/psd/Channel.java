package com.alternativagame.resource.utils.psd;

import java.io.IOException; 
 
 
public class Channel { 
 public static final int ALPHA = -1; 
 public static final int RED = 0; 
 public static final int GREEN = 1; 
 public static final int BLUE = 2; 
 
 private int id; 
 private int dataLength; 
 private byte[] data; 
 
 public Channel(PsdInputStream stream) throws IOException { 
  id = stream.readShort(); 
  dataLength = stream.readInt(); 
 } 
 
 public Channel(int id) { 
  this.id = id; 
 } 
 
 public int getId() { 
  return id; 
 } 
 
 public int getDataLength() { 
  return dataLength; 
 } 
  
 public void setData(byte[] data) { 
  this.data = data; 
 } 
  
 public byte[] getData() { 
  return data; 
 } 
 
}