/**
 * 
 */
package com.alternativagame.resource.utils.psd.images;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * RAW 데이터로 저장된 pds
 * @author idea-11
 *
 */
public class RAWParser {

    byte[] result = null;

    /**
     * @param bounds
     * @param in
     * @throws IOException
     */
    public RAWParser(int width, int heigth, DataInputStream in) throws IOException {
	result = new byte[width * heigth];
	in.read(result);
    }

    
    public byte[] getData() {
	return result;
    }
}
