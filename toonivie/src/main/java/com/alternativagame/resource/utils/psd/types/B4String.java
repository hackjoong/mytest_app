/**
 * 
 */
package com.alternativagame.resource.utils.psd.types;

import java.io.DataInputStream;
import java.io.IOException;


public class B4String {

    private final String value;

    /**
     * @param in
     * @throws IOException
     */
    public B4String(DataInputStream in) throws IOException {
	byte[] data = new byte[4];
	value = new String(data, "EUC-KR");
	data = value.getBytes();
	in.read(data);
    }

    public String getValue() {
	return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return value;
    }

}
