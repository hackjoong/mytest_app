<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />

<title>TOONIVIE</title>

<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon_v2.1/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon_v2.1/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon_v2.1/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon_v2.1/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon_v2.1/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon_v2.1/favicon-72x72.png">

<!-- jquery -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- main_banner -->
<link rel="stylesheet" href="/resources/common/css/owl.carousel.css">
<script src="/resources/common/js/owl.carousel.js"></script>

<!-- bootstrap  -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/platform.css">

</head>
<body>
<!-- header -->
<header>
	<!-- pc_header  -->
	<div id="pc_header">
		<!-- header_top  -->
		<div class="header_top">
			<div class="container">
				<div class="clearfix header_top_box">
					<div class="header_morepage">
						<ul>
							<li><a href="#">연재 신청</a></li>
							<li><a href="#">내 서재</a></li>
							<li><a href="#">찜목록</a></li>
						</ul>
					</div>
					<div class="header_age">
						<div class="w50">
							<div class="age_all">
								<a class="age_active" href="#">전연령</a>
							</div>
							<div class="age_19">
								<a href="#">성인</a>
							</div>
						</div>
					</div>
					<div class="header_infopage">
						<ul>
							<li><a href="#">로그인</a></li>
							<li><a href="#">회원가입</a></li>
							<li><a href="#">쪽지</a></li>
							<li><a href="#">고객센터</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- //header_top  -->
		<!-- header_bot  -->
		<div class="header_bot">
			<div class="container">
				<div class="warp mt35">
					<a href="/test01"><img src="/resources/image/v2.0/TOONIVIE_logo2.png" alt="TOONIVIE_logo" title="TOONIVIE_logo" /></a>
					<div class="quick_menu">
						<div class="quick_menu_box">
							<ul>
								<li><a href="/test02">웹툰무비</a></li>
								<li><a href="/test03">웹툰연재</a></li>
								<li><a href="/test04">완결</a></li>
								<li><a href="/test05">단편</a></li>
								<li><a href="#">이벤트</a></li>
							</ul>
						</div>
					</div>
					<div class="search_nav">
						<div class="fl search">
							<input type="text" placeholder="search"/>
							<img src="/resources/image/v2.0/search.png" alt="찾기" title="찾기" />
						</div>
						<nav class="fl main_nav"></nav>
					</div>
				</div>
			</div>
		</div>
		<!-- //header_bot  -->
	</div>
	<!-- //pc_header  -->
	<!-- mobile_header  -->
	<div id="mobile_header">
		<!-- mobile_header_top  -->
		<div class="mobile_header_top">
			<div class="warp">
				<div class="mobile_header_top_box">
					<img src="/resources/image/v2.0/TOONIVIE_logo2.png" alt="TOONIVIE_logo" title="TOONIVIE_logo">
					<div class="mobile_header_age">
						<div class="mobile_age_all">
							<a class="age_active" href="#">전연령</a>
						</div>
						<div class="mobile_age_19">
							<a href="#">성인</a>
						</div>
					</div>
					<div class="mobile_search_nav">
						<div class="mobile_search">
							<img src="/resources/image/v2.0/search_mobile.png" alt="찾기" title="찾기">
						</div>
						<div class="mobile_nav"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- //mobile_header_top  -->
		<!-- mobile_header_bot -->
		<div class="mobile_header_bot">
			<div class="warp">
				<div class="mobile_quick_menu">
					<ul>
						<li><a href="/test02">웹툰무비</a></li>
						<li><a href="/test03">웹툰연재</a></li>
						<li><a href="/test04">완결</a></li>
						<li><a href="/test05">단편</a></li>
						<li><a href="#">이벤트</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- //mobile_header_bot -->
	</div>
	<!-- //mobile_header  -->
	
</header>
<!-- //header -->
<!-- //main -->
<div class="w100 fl">
	<!-- main_banner  -->
	<div id="main_banner">
		<div class="owl-carousel owl-theme">
           <div class="item" style="background: #0F100F;">
            	<div class="warp">
            		<a href="#"><img src="/resources/image/v2.0/ad/Tomoon_bnr.png" alt="" title="" /></a>
            	</div>
            </div>
            <div class="item" style="background: #F4A5A0;">
            	<div class="warp">
            		<a href="#"><img src="/resources/image/v2.0/fullimage1.jpg" alt="" title="" /></a>
            	</div>
            </div>
        </div>
	</div>
	<!-- //main_banner  -->
	<main id="main">
		<!-- tooni_pick  -->
		<div id="tooni_pick">
			<div class="">
				<div class="warp">
					<h2>Toonivie`s Pick <em>이달의 작품</em></h2>
					<div class="tooni_pick_list">
						<!-- 뿌려질틀  -->
						<div class="pick_list_box">
							<div class="pick_list_img hot"></div>
							<div class="pick_list_text">
								<div class="pick_list_title">
									<div class="w95" style="padding-top: 23px;">
										<h4>나의 로맨틱 홀리데이</h4>
										<p style="padding-top: 5px;"><span class="cyon fl">로맨스/액션</span><span class="fr" style="color:#717272">최경국</span></p>
									</div>
								</div>
								<div class="pick_list_intro">
									<div class="w90">
										<p>로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!</p>
									</div>
								</div>
							</div>
						</div>
						<!-- 뿌려질틀  -->
						<div class="pick_list_box">
							<div class="pick_list_img up"></div>
							<div class="pick_list_text">
								<div class="pick_list_title">
									<div class="w95" style="padding-top: 23px;">
										<h4>나의 로맨틱 홀리데이</h4>
										<p style="padding-top: 5px;"><span class="cyon fl">로맨스/액션</span><span class="fr" style="color:#717272">최경국</span></p>
									</div>
								</div>
								<div class="pick_list_intro">
									<div class="w90">
										<p>로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!</p>
									</div>
								</div>
							</div>
						</div>
						<div class="pick_list_box">
							<div class="pick_list_img new"></div>
							<div class="pick_list_text">
								<div class="pick_list_title">
									<div class="w95" style="padding-top: 23px;">
										<h4>나의 로맨틱 홀리데이</h4>
										<p style="padding-top: 5px;"><span class="cyon fl">로맨스/액션</span><span class="fr" style="color:#717272">최경국</span></p>
									</div>
								</div>
								<div class="pick_list_intro">
									<div class="w90">
										<p>로맨틱 영화를 꿈꾸던 여자,액션 영화 속에 빠지다!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- tooni_pick  -->
		<!-- AD  -->
		<div class="AD" style="background: #306782;">
			<div class="warp" style="height: 100%;">
				<a href=""><img src="/resources/image/v2.0/ad/myer_img_01.png" alt="" /></a>
			</div>
		</div>
		<!-- //AD  -->
		<!-- tooni_top  -->
		<div id="tooni_top">
			<div class="">
				<div class="warp">
					<h2>Top <em>인기순위 작품</em></h2>
					<!-- tooni_top_pc -->
					<div id="tooni_top_pc">
						<div class="tooni_top_content">
							<ul class="tooni_top_btn">
								<li class="fl tooni_top_WM tooni_top_btn_active" onclick="tooni_top_btn(this)" data-toonitop="1">WEBTOON MOVIE</li>
								<li class="fl tooni_top_W" onclick="tooni_top_btn(this)" data-toonitop="2">WEBTOON</li>
								<li class="fl tooni_top_SW" onclick="tooni_top_btn(this)" data-toonitop="3">단편</li>
							</ul>
							<!-- WEBTOON MOVIE -->
							<div id="webtoon_movie_content" class="tooni_top_rank">
								<!-- rank_first -->
								<div class="tooni_top_rank_first">
									<div class="rank_first_img"></div>
									<div class="rank_first_intro">
										<div class="first_intro_box">
											<div class="first_intro_title">
												<h3>나의 로멘틱 홀리데이</h3>
												<p>최경국</p>
											</div>
											<div class="first_intro_text">
												<span>로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.</span>
												<div class="first_intro_tag">
													<div>#로맨스</div>
													<div>#액션</div>
													<div>#판타지</div>
													<div>#감성</div>
													<div>#로맨스</div>
													<div>#감성</div>
												</div>
											</div>
											<div class="first_intro_view">
												<span>절찬 연재중!! <em>무료공개 3화</em></span>
												<button>첫화보기</button>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_first -->
								<!-- rank_other  -->
								<div class="tooni_top_rank_other">
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">2</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>지우개똥</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">3</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>우주 유실물</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">4</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>투문</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">5</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>브라이트 하우스</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_other  -->
							</div>
							<!-- //WEBTOON MOVIE -->
							<!-- WEBTOON  -->
							<div id="webtoon_content" class="tooni_top_rank dn">
								<!-- rank_first -->
								<div class="tooni_top_rank_first">
									<div class="rank_first_img"></div>
									<div class="rank_first_intro">
										<div class="first_intro_box">
											<div class="first_intro_title">
												<h3>나의 로멘틱 홀리데이</h3>
												<p>최경국</p>
											</div>
											<div class="first_intro_text">
												<span>로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.</span>
												<div class="first_intro_tag">
													<div>#로맨스</div>
													<div>#액션</div>
													<div>#판타지</div>
													<div>#감성</div>
													<div>#로맨스</div>
													<div>#감성</div>
												</div>
											</div>
											<div class="first_intro_view">
												<span>절찬 연재중!! <em>무료공개 3화</em></span>
												<button>첫화보기</button>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_first -->
								<!-- rank_other  -->
								<div class="tooni_top_rank_other">
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">2</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>지우개똥</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">3</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>우주 유실물</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">4</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>브라이트 하우스</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">5</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>투문</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_other  -->
							</div>
							<!-- //WEBTOON  -->
							<!-- webtoon_short -->
							<div id="webtoon_short" class="tooni_top_rank dn">
								<!-- rank_first -->
								<div class="tooni_top_rank_first">
									<div class="rank_first_img"></div>
									<div class="rank_first_intro">
										<div class="first_intro_box">
											<div class="first_intro_title">
												<h3>나의 로멘틱 홀리데이</h3>
												<p>최경국</p>
											</div>
											<div class="first_intro_text">
												<span>로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.</span>
												<div class="first_intro_tag">
													<div>#로맨스</div>
													<div>#액션</div>
													<div>#판타지</div>
													<div>#감성</div>
													<div>#로맨스</div>
													<div>#감성</div>
												</div>
											</div>
											<div class="first_intro_view">
												<span>절찬 연재중!! <em>무료공개 3화</em></span>
												<button>첫화보기</button>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_first -->
								<!-- rank_other  -->
								<div class="tooni_top_rank_other">
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">2</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>지우개똥</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">3</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>우주 유실물</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">4</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>브라이트 하우스</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
									<div class="top_rank_other_box">
										<div class="other_box_content">
											<div class="other_rank">5</div>
											<div class="other_img"></div>
											<div class="other_writer">
												<h4>투문</h4>
												<p>로맨스/액션</p>
											</div>
										</div>
									</div>
								</div>
								<!-- //rank_other  -->
							</div>
							<!-- //webtoon_short -->
						</div>
					</div>
					<!-- //tooni_top_pc -->
					<!-- tooni_top_mobile -->
					<div id="tooni_top_mobile" class="dn">
						<div class="tooni_top_content_m">
							<div class="tooni_top_WM_m">웹툰무비</div>
							<div class="tooni_top_W_m">웹툰</div>
							<div class="tooni_top_SW_m">단편</div>
						</div>
					</div>
					<!-- tooni_top_mobile  -->
				</div>
			</div>
		</div>
		<!-- //tooni_top  -->
		<!-- tooni_new  -->
		<div id="tooni_new">
			<div class="warp">
				<h2>New <em>최신 업데이트 작품</em></h2>
				<!-- tooni_new_pc -->
				<div id="tooni_new_pc">
					<div class="tooni_new_box">
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="new_content_img new"></div>
								<div class="new_content_intro">
									<h4>브라이트 하우스</h4>
									<p class="new_content_intro_writer">회사원</p>
									<p style="color:#20beca;">일상/코믹</p>
									<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="new_content_img new"></div>
								<div class="new_content_intro">
									<h4>브라이트 하우스</h4>
									<p class="new_content_intro_writer">회사원</p>
									<p style="color:#20beca;">일상/코믹</p>
									<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="new_content_img new"></div>
								<div class="new_content_intro">
									<h4>브라이트 하우스</h4>
									<p class="new_content_intro_writer">회사원</p>
									<p style="color:#20beca;">일상/코믹</p>
									<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="new_content_img new"></div>
								<div class="new_content_intro">
									<h4>브라이트 하우스</h4>
									<p class="new_content_intro_writer">회사원</p>
									<p style="color:#20beca;">일상/코믹</p>
									<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- //tooni_new_pc -->
				<!-- tooni_new_mobile -->
				<div id="tooni_new_mobile">
					<div class="tooni_new_box_m clearfix">
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="p10">
									<div class="new_content_img_m new"></div>
								</div>
								<div class="new_content_intro_m clearfix">
									<h4>브라이트 하우스</h4>
									<span class="new_content_intro_m_genre">일상/코믹</span>
									<span class="new_content_intro_m_writer">회사원</span>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="p10">
									<div class="new_content_img_m new"></div>
								</div>
								<div class="new_content_intro_m clearfix">
									<h4>브라이트 하우스</h4>
									<span class="new_content_intro_m_genre">일상/코믹</span>
									<span class="new_content_intro_m_writer">회사원</span>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="p10">
									<div class="new_content_img_m new"></div>
								</div>
								<div class="new_content_intro_m clearfix">
									<h4>브라이트 하우스</h4>
									<span class="new_content_intro_m_genre">일상/코믹</span>
									<span class="new_content_intro_m_writer">회사원</span>
								</div>
							</div>
						</div>
						<div class="tooni_new_content">
							<div class="new_content">
								<div class="p10">
									<div class="new_content_img_m new"></div>
								</div>
								<div class="new_content_intro_m clearfix">
									<h4>브라이트 하우스</h4>
									<span class="new_content_intro_m_genre">일상/코믹</span>
									<span class="new_content_intro_m_writer">회사원</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- //tooni_new_mobile -->
				
			</div>
		</div>
		<!-- //tooni_new  -->
		<!-- tooni_hot  -->
		<div id="tooni_hot">
			<div class="warp">
				<h2 style="margin: 0;">Hot <em>이달의 신작</em></h2>
				<div class="tooni_hot_box">
					<div class="tooni_hot_content">
						<div class="hot_content">
							<div class="hot_content_img"></div>
							<div class="hot_content_intro">
								<h4>브라이트 하우스</h4>
								<p class="hot_content_intro_writer">회사원</p>
								<p style="color:#20beca;">일상/코믹</p>
								<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
							</div>
						</div>
					</div>
					<div class="tooni_hot_content">
						<div class="hot_content">
							<div class="hot_content_img"></div>
							<div class="hot_content_intro">
								<h4>브라이트 하우스</h4>
								<p class="hot_content_intro_writer">회사원</p>
								<p style="color:#20beca;">일상/코믹</p>
								<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
							</div>
						</div>
					</div>
					<div class="tooni_hot_content">
						<div class="hot_content">
							<div class="hot_content_img"></div>
							<div class="hot_content_intro">
								<h4>브라이트 하우스</h4>
								<p class="hot_content_intro_writer">회사원</p>
								<p style="color:#20beca;">일상/코믹</p>
								<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
							</div>
						</div>
					</div>
					<div class="tooni_hot_content">
						<div class="hot_content">
							<div class="hot_content_img"></div>
							<div class="hot_content_intro">
								<h4>브라이트 하우스</h4>
								<p class="hot_content_intro_writer">회사원</p>
								<p style="color:#20beca;">일상/코믹</p>
								<p>애견 게스트 하우스'브라이트 하우스' 에서 일어나는 다양한 이야기.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //tooni_hot  -->
		<!-- AD  -->
		<div class="AD" style="background: #FFA72A;">
			<div class="warp" style="height: 100%;">
				<a href="#"><img src="/resources/image/v2.0/ad/myer_img_04.png" alt="" /></a>
			</div>
		</div>
		<!-- //AD  -->
		<!-- AD  -->
		<div class="AD" style="background: #2E2E40;">
			<div class="warp" style="height: 100%;">
				<a href="#"><img src="/resources/image/v2.0/ad/myer_img_05.png" alt="" /></a>
			</div>
		</div>
		<!-- //AD  -->
	</main>
</div>
<!-- //main  -->
<!-- footer  -->
<footer id="footer">
	<div class="container">
		<!-- pc_footer -->
		<div id="pc_footer">
			<div class="footer_top">
				<ul>
					<li><a href="#">About 투니비</a></li>
					<li><a href="#">서비스약관</a></li>
					<li><a href="#">개인정보 처리방침</a></li>
					<li><a href="#">연재/광고 제휴 제안</a></li>
					<li><a href="#">쿠폰/상품권 등록</a></li>
					<li><a href="#">고객센터</a></li>
				</ul>
			</div>
			<div class="footer_bot">
				<div class="footer_bot_1">
					<div class="footer_logo">
						<div class="footer_logo_box">
							<a href="#"><img src="/resources/image/v2.0/footer_logo.png" alt="투니비 로고" title="투니비 로고"/></a>
						</div>
					</div>
					<div class="company_info">
						<p class="mb15" style="color:#fff;">(주) 아이디어 콘서트</p>
						<p>대표자명 : 전달용 | 사업자번호 : 338-88-00131 |</p>
						<p class="mb15">주소 : 경기도 성남시 수정구 성남대로 1342 가천대학교 법학대학 비전타워 7층 경기창조혁신센터</p>
						<p>TEL : 070. 8825. 5004</p>
						<p>Email : ideaconcert@ideaconcert.com</p>
					</div>
					<div class="languge">
						<p>Languge</p>
						<select>
						  <option value="volvo">한국어</option>
						  <option value="saab">English</option>
						  <option value="opel">日本語</option>
						  <option value="audi">中国語</option>
						</select>
					</div>
				</div>
				<div class="footer_bot_2">
					<div class="sns">
						<div class="sns_box">
							<a href="#"><img src="/resources/image/v2.0/sns_blog.png" alt="sns_blog" title="" /></a>
							<a href="#"><img src="/resources/image/v2.0/sns_facebook.png" alt="sns_facebook" title="sns_facebook" /></a>
							<a href="#"><img src="/resources/image/v2.0/sns_youtube.png" alt="sns_youtube" title="sns_youtube" /></a>
						</div>
					</div>
					<div class="copyright">
						<div>
							<p>투니비 웹사이트에 게시된 모든 컨텐츠들은 저작원법에 의거 보호받고 있습니다.</p>
							<p>저작권자 또는 (주)아이디어 콘서트의 승인없이 컨텐츠의 일부 또는 전부를 복제·전송·배포 및 기타의 방법으로 저작물을 이용할 경우에는</p>
							<p>저작권법에 의해 법적 조치에 처해질 수 있으므로 주의하시길 바랍니다.</p>
						</div>
					</div>
					<p class="copyright2">Copyright ⓒ 2015-2018 Ideaconcert All Rights Reserved.</p>
				</div>
			</div>
		</div>
		<!-- //pc_footer -->
		<!-- mobile_footer -->
		<div id="mobile_footer">
			<div id="footer_top">
				<div class="footer_top_top">
					<ul>
						<li><a href="#">서비스약관</a></li>
						<li><a href="#">개인정보 처리방침</a></li>
						<li><a href="#">연재/광고 제휴 제안</a></li>
						<li><a href="#">고객센터</a></li>
					</ul>
				</div>
				<div class="footer_top_bot">
					<p class="white">(주) 아이디어 콘서트</p>
					<p>사업자번호 : 338-88-00131</p>
					<p>주소 : 경기도 성남시 수정구 성남대로 1342 가천대학교 법학대학 비전타워 7층 경기창조혁신센터</p>
					<p>TEL : 070. 8825. 5004</p>
					<p>Email : ideaconcert@ideaconcert.com</p>
					<p>Copyright ⓒ 2015-2018 Ideaconcert All Rights Reserved.</p>
				</div>
			</div>
		</div>
		<!-- //mobile_footer -->
	</div>
</footer>
<!-- //footer  -->
</body>
<script>
$(document).ready(function() {
	// main_banner add psh 2018-04-25 
	var owl = $('.owl-carousel');
	owl.owlCarousel({
	    items:1,
	    nav:true,
	    loop:true,
	    autoplay:true,
	    autoplayTimeout:5000,
	    autoplayHoverPause:true
	});
	
	var pick_list_img_width =  $(".pick_list_img").width();
    var new_content_img_m_width = $(".new_content_img_m").width();
   
    $(".pick_list_img").css("height",pick_list_img_width);
    $(".new_content_img_m").css("height",new_content_img_m_width);
  
    
	
	$(window).resize(function(){
        var pick_list_img_width =  $(".pick_list_img").width();
        var new_content_img_m_width = $(".warp").width();
        $(".pick_list_img").css("height",pick_list_img_width);
        $(".new_content_img_m").css("height",new_content_img_m_width); 
    });
	
  })


// tooni top content change    add psh 2018-04-25
function tooni_top_btn(top_btn){
	tooni_top_data = $(top_btn).data("toonitop");
	if(tooni_top_data == 1){
		//alert("웹툰 무비");
		$(".tooni_top_WM").addClass("tooni_top_btn_active");
		$(".tooni_top_W").removeClass("tooni_top_btn_active");
		$(".tooni_top_SW").removeClass("tooni_top_btn_active");
		$("#webtoon_movie_content").removeClass("dn");
		$("#webtoon_content").addClass("dn");
		$("#webtoon_short").addClass("dn");

		
	}else if (tooni_top_data == 2){
		//alert("웹툰");
		$(".tooni_top_WM").removeClass("tooni_top_btn_active");
		$(".tooni_top_W").addClass("tooni_top_btn_active");
		$(".tooni_top_SW").removeClass("tooni_top_btn_active");
		$("#webtoon_movie_content").addClass("dn");
		$("#webtoon_content").removeClass("dn");
		$("#webtoon_short").addClass("dn");
		
		
	}else if (tooni_top_data == 3){
		//alert("단편");
		$(".tooni_top_WM").removeClass("tooni_top_btn_active");
		$(".tooni_top_W").removeClass("tooni_top_btn_active");
		$(".tooni_top_SW").addClass("tooni_top_btn_active");
		$("#webtoon_movie_content").addClass("dn");
		$("#webtoon_content").addClass("dn");
		$("#webtoon_short").removeClass("dn");
		
	}
}

/* $(".WM_h3").click(function(){
	$(".tooni_top_WM .WM_cont").slideDown(800);
	$(".tooni_top_WT .WM_cont").slideUp(600);
	$(".tooni_top_WM").removeClass("tooni_top_small").addClass("tooni_top_big");
	$(".tooni_top_WT").removeClass("tooni_top_big").addClass("tooni_top_small");
});
$(".WT_h3").click(function(){
	$(".tooni_top_WM .WM_cont").slideUp(600);
	$(".tooni_top_WT .WM_cont").slideDown(800);
	$(".tooni_top_WM").removeClass("tooni_top_big").addClass("tooni_top_small");
	$(".tooni_top_WT").removeClass("tooni_top_small dn").addClass("tooni_top_big");
}); */
</script>

</html>