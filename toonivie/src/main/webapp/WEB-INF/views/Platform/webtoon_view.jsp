<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />

<title>TOONIVIE</title>

<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon_v2.1/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon_v2.1/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon_v2.1/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon_v2.1/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon_v2.1/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon_v2.1/favicon-72x72.png">

<!-- jquery -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- main_banner -->
<link rel="stylesheet" href="/resources/common/css/owl.carousel.css">
<script src="/resources/common/js/owl.carousel.min.js"></script>

<!-- bootstrap  -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/platform.css">

</head>
<body>
<!-- header -->
<header>
	<div id="view_header">
		<div class="w100 pt15">
			<a href="/test01"><img class="logo" src="/resources/image/v2.0/TOONIVIE_logo2.png" width="150px" alt="투니비 로고" title="투니비 로고" /></a>
			<div class="list_back" onclick="location.href='/test08'"></div>
			<div class="view_header_title">지우개 똥</div>
			<div class="view_header_infor">
				<div>1화</div>
				<div>우리들의 첫만남</div>
			</div>
			<div class="view_header_btns">
				<ul>
					<li><a href="#"><img src="/resources/image/v2.0/previous.png" alt="이전화" title="이전화" /></a></li>
					<li><a href="#"><img src="/resources/image/v2.0/like_off.png" alt="좋아요" title="좋아요"/></a></li>
					<li><a href="#"><img src="/resources/image/v2.0/next.png" alt="다음화" title="다음화"/></a></li>
				</ul>
			</div>
		</div>
	</div>
</header>
<!-- //header -->
<!-- main -->
<main id="main" onclick="show_header_nav()">
	<div class="warp2">
		<div class="scroll_box dn" style="height: 70px;"></div>
		<!-- view_video -->
		<div class="view_webtoon">
			<img src="/resources/image/v2.0/webtoon03.png" alt="" />
			<img src="/resources/image/v2.0/webtoon04.png" alt="" />
		</div>
		<!-- //view_video -->
		<!-- view_copyright -->
		<div class="view_copyright">
			<p>
				투니비에 게시된 모든 게시물은 <em class="cyon">저작권법에 의거 보호</em> 받고있습니다.<br>
				저작권자와 투니비의 승인 없이 컨텐츠의 일부 또는 전부를<br>
				복제,전송,배포 및 기타의 방법으로 저작물을 이용할 경우<br>
				<em class="cyon">저작권법에 의해 법적 조지체 처해질 수 있으므로 주의하시기 바랍니다.</em>
			</p>
			<img src="/resources/image/v2.0/footer_logo.png" alt="투니비 로고" title="투니비 로고"/>
		</div>
		<!-- //view_copyright -->
		<!-- view_comment -->
		<div class="view_comment" onclick="hide_header_nav()">
			<h4>별점 & 댓글 남기기</h4>
			<!-- comment_input  -->
			<div class="comment_input">
				<div class="comment_input_area">
					<div class="w95" style="height: 85%;">
						<div class="comment_input_area_rating">
							<span>별점</span>
							<div class="comment_input_area_rating_star"></div>
						</div> 
						<textarea class="comment_input_text" placeholder="작품에 대한 소중한 의견과 응원글을 남겨주세요."></textarea>
					</div>
					<div class="comment_input_area_total">
						<span>0/400</span>
					</div>
				</div>
				<div class="comment_input_btn">댓글 등록</div>
			</div>
			<!-- //comment_input  -->
			<!-- comment_list -->
			<div class="comment_list">
				<!-- comment_align  -->
				<div class="comment_align">
					<ul>
						<li class="comment_latest"><img src="/resources/image/v2.0/content_align_check_off.png" alt="댓글 최신순"  title="댓글 최신순"/>최신순</li>
						<li class="comment_popularity comment_align_active"><img src="/resources/image/v2.0/content_align_check_on.png" alt="댓글 인기순" title="댓글 인기순"/>인기순</li>
					</ul>
				</div>
				<!-- //comment_align  -->
				<!-- comment_list_box -->
				<div class="comment_list_box">
					<!-- comment_list_best -->
					<div class="comment_list_best best">
						<!-- 뿌려질틀 -->
						<div class="comment_infor">
							<div class="w95">
								<h5>ID</h5>
								<p>댓글입력</p>
								<div class="comment_infor_bottom">
									<span><em class="red">♡</em> 좋아요 <span>26</span></span>
									<span class="cib_b">덧글 <span>20</span></span>
									<span class="cib_b">2018-05-03 16:54</span>
									<div class="fr report"></div>
								</div>
							</div>
						</div>
						<!-- 뿌려질틀 -->
						<div class="comment_infor">
							<div class="w95">
								<h5>ID</h5>
								<p>댓글입력</p>
								<div class="comment_infor_bottom">
									<span><em class="red">♡</em> 좋아요 <span>26</span></span>
									<span class="cib_b">덧글 <span>20</span></span>
									<span class="cib_b">2018-05-03 16:54</span>
									<div class="fr report"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- //comment_list_best -->
					<!-- comment_list_other -->
					<div class="comment_list_other">
						<!-- 뿌려질틀 -->
						<div class="comment_infor">
							<div class="w95">
								<h5>ID</h5>
								<p>댓글입력</p>
								<div class="comment_infor_bottom">
									<span><em class="red">♡</em> 좋아요 <span>26</span></span>
									<span class="cib_b">덧글 <span>20</span></span>
									<span class="cib_b">2018-05-03 16:54</span>
									<div class="fr report"></div>
								</div>
							</div>
						</div>
						<!-- 뿌려질틀 -->
						<div class="comment_infor">
							<div class="w95">
								<h5>ID</h5>
								<p>댓글입력</p>
								<div class="comment_infor_bottom">
									<span><em class="red">♡</em> 좋아요 <span>26</span></span>
									<span class="cib_b">덧글 <span>20</span></span>
									<span class="cib_b">2018-05-03 16:54</span>
									<div class="fr report"></div>
								</div>
							</div>
						</div>
						<div class="comment_infor">
							<div class="w95">
								<h5>ID</h5>
								<p>댓글입력</p>
								<div class="comment_infor_bottom">
									<span><em class="red">♡</em> 좋아요 <span>26</span></span>
									<span class="cib_b">덧글 <span>20</span></span>
									<span class="cib_b">2018-05-03 16:54</span>
									<div class="fr report"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- //comment_list_other -->
				</div>
				<!-- //comment_list_box -->
			</div>
			<!-- //comment_list -->
		</div>
		<!-- //view_comment -->
	</div>
</main>
<!-- main -->
<!-- nav -->
<nav>
 <div id="view_nav">
 	<div class="view_nav_list">
 		<div class="warp">
 			<h4><em class="cyon">다음화</em> 이어보기</h4>
 			<div class="view_nav_list_content">
 				<div class="owl-carousel owl-theme">
 					<!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		            <!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		            <!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		            <!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		            <!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		            <!-- item  -->
		            <div class="item">
		              <div class="view_nav_list_content_box">
		              	<div class="view_nav_list_img"></div>
		              	<div class="view_nav_list_infor">
		              		<h4>1화</h4>
		              		<p>우리들의 첫만남우리들의 첫만남</p>
		              		<span class="cyon fr">무료</span>
					 	</div>
		              </div>
		            </div>
		            <!-- //item  -->
		        </div>
 			</div> 
 			
 		</div>
 	</div>
 	<div class="view_nav_btns">
 		<ul>
 			<li class="view_nav_btns_previous"><a href="#"><img src="/resources/image/v2.0/previous.png" alt="" /><span>이전화</span></a></li>
 			<li class="view_nav_btns_list_back"><a href="#"><img src="/resources/image/v2.0/list_back.png" alt="" /><span>목록</span></a></li>
 			<li class="view_nav_btns_next"><a href="#"><img src="/resources/image/v2.0/next.png" alt="" /><span>다음화</span></a></li>
 		</ul>
 		 		
 	</div>
 </div>
</nav>
<!-- //nav -->
</body>
<script>
$(document).ready(function(){
	
	// scroll event add psh 2018-05-04
	var wScroll = $(this).scrollTop();
	if(wScroll == 0){
		$("#view_header").addClass("scrTop");
	}
	
	// view_nav_list add psh 2018-05-03 
	var owl = $('.owl-carousel');
	owl.owlCarousel({
	    items:5,
	    nav:true,
	    margin:15,
	});
  })
  
// scroll event add psh 2018-05-04
$(window).scroll(function(){
	var wScroll = $(this).scrollTop();
	var wScroll_bottom = $(document).height() - $(window).height();
	
	if(wScroll == 0){
		//scroll top
		$("#view_header").removeClass("on").removeClass("dn");
		$("#view_header").addClass("scrTop").removeClass("scrMid");
		$(".scroll_box").addClass("dn");
		$("#view_nav").removeClass("on").removeClass("dn");
		
	}else if(wScroll > 50){
		//scroll mid
		$("#view_header").addClass("on").addClass("dn");
		$("#view_header").addClass("scrMid").removeClass("scrTop");
		$(".scroll_box").removeClass("dn");
		$("#view_nav").addClass("on").addClass("dn");
	}
	if(wScroll == wScroll_bottom){
		//scroll bot
		$("#view_header").removeClass("dn");
		$(".scroll_box").addClass("dn");
		$("#view_nav").removeClass("on").removeClass("dn");
	}
	
	
});
function hide_header_nav(){
	$("#view_header").addClass("dn")
	$("#view_nav").addClass("dn")
}

function show_header_nav(){
	var find_scrTop = $("#view_header").hasClass("scrTop");
	var find_scrMid = $("#view_header").hasClass("scrMid");
	
	if(find_scrTop == true){
		//alert("scroll top");
		$("#view_header").toggleClass("on").toggleClass("dn");
		$(".scroll_box").toggleClass("dn");
		$("#view_nav").toggleClass("on").toggleClass("dn");
	}else if(find_scrMid == true){
		//alert("scroll mid");
		$("#view_header").toggleClass("dn");
		$("#view_nav").toggleClass("on").toggleClass("dn");
	}
}


</script>

</html>