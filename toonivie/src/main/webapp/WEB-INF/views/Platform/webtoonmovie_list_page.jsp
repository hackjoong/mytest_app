<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />

<title>TOONIVIE</title>

<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon_v2.1/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon_v2.1/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon_v2.1/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon_v2.1/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon_v2.1/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon_v2.1/favicon-72x72.png">

<!-- jquery -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- main_banner -->
<link rel="stylesheet" href="/resources/common/css/owl.carousel.css">
<script src="/resources/common/js/owl.carousel.min.js"></script>

<!-- bootstrap  -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/platform.css">

</head>
<body>
<!-- header -->
<header>
	<!-- header_top  -->
	<div class="header_top">
		<div class="container">
			<div class="clearfix header_top_box">
				<div class="header_morepage">
					<ul>
						<li><a href="#">연재 신청</a></li>
						<li><a href="#">내 서재</a></li>
						<li><a href="#">찜목록</a></li>
					</ul>
				</div>
				<div class="header_age">
					<div class="w50">
						<div class="age_all">
							<a class="age_active" href="#">전연령</a>
						</div>
						<div class="age_19">
							<a href="#">성인</a>
						</div>
					</div>
				</div>
				<div class="header_infopage">
					<ul>
						<li><a href="#">로그인</a></li>
						<li><a href="#">회원가입</a></li>
						<li><a href="#">쪽지</a></li>
						<li><a href="#">고객센터</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- //header_top  -->
	<!-- header_bot  -->
	<div class="header_bot">
		<div class="container">
			<div class="warp mt35">
				<a href="/test01"><img src="/resources/image/v2.0/TOONIVIE_logo2.png" alt="TOONIVIE_logo" title="TOONIVIE_logo" /></a>
				<div class="quick_menu">
					<div class="quick_menu_box">
						<ul>
							<li class="quick_menu_active"><a href="/test02">웹툰무비</a></li>
							<li><a href="/test03">웹툰연재</a></li>
							<li><a href="/test04">완결</a></li>
							<li><a href="/test05">단편</a></li>
							<li><a href="#">이벤트</a></li>
						</ul>
					</div>
				</div>
				<div class="search_nav">
					<div class="fl search">
						<input type="text" placeholder="search"/>
						<img src="/resources/image/v2.0/search.png" alt="찾기" title="찾기" />
					</div>
					<nav class="fl main_nav"></nav>
				</div>
			</div>
		</div>
	</div>
	<!-- //header_bot  -->
</header>
<!-- //header -->
<!-- main -->
<div id="main">
	<div class="w100 fl">
		<!-- webtoonmovie_list_intro -->
		<div id="webtoonmovie_list_intro">
			<div class="warp" style="height: 100%;">
				<div class="webtoonmovie_list_intro_img"></div>
				<!-- webtoon_list_intro_text -->
				<div class="webtoonmovie_list_intro_text new">
					<div class="w90">
						<p>웹툰무비</p>
						<h3>지우개똥</h3>
						<p class="webtoonmovie_list_writer">뱀도마뱀</p>
						<div class="webtoonmovie_list_intro_box">
							<span>로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.로맨틱 영화를 꿈구던 여자 , 액션 영화속에 빠지다.</span>
							<div class="webtoonmovie_list_intro_tag">
								<div>#로맨스</div>
								<div>#액션</div>
								<div>#판타지</div>
								<div>#감성</div>
							</div>
						</div>
						<div class="webtoonmovie_list_intro_btn">
							<button>첫화보기</button>
							<button><em>♡</em> 구독하기</button>
						</div>
						<ul class="webtoonmovie_list_other">
							<li><a href="#">웹툰으로 보기 >></a></li>
							<li><a href="#">작가의 다른작품 >></a></li>
						</ul>
					</div>
				</div>
				<!-- //webtoon_list_intro_text -->
			</div>
		</div>
		<!-- //webtoonmovie_list_intro -->
	</div>
	<!-- webtoonmovie_list_content -->
	<div id="webtoonmovie_list_content">
		<div class="warp">
			<!-- content_align  -->
			<div class="content_align">
				<h4>지우개똥</h4>
				<ul>
					<li class="content_latest" onclick="content_align(this)"><img src="/resources/image/v2.0/content_align_check_off.png" alt="요일별 최신순"  title="요일별 최신순"/>최신순</li>
					<li class="content_popularity content_align_active" onclick="content_align(this)"><img src="/resources/image/v2.0/content_align_check_on.png" alt="요일별 인기순" title="요일별 인기순"/>인기순</li>
				</ul>
			</div>
			<!-- //content_align  -->
			<!-- webtoonmovie_list -->
			<div class="webtoonmovie_list">
				<!-- 뿌려질틀 -->
				<div class="webtoonmovie_list_box" onclick="location.href='/test09'">
					<div class="webtoonmovie_list_img"></div>
					<div class="webtoonmovie_list_title">
						<h3>1화</h3>
						<p>창 밖이 궁금해</p>
					</div>
					<div class="webtoonmovie_list_date">2018.05.02</div>
					<div class="webtoonmovie_list_coin">3코인</div>
					<div class="webtoonmovie_list_rating"></div>
				</div>
				<!-- 뿌려질틀 -->
				<div class="webtoonmovie_list_box">
					<div class="webtoonmovie_list_img"></div>
					<div class="webtoonmovie_list_title">
						<h3>1화</h3>
						<p>창 밖이 궁금해</p>
					</div>
					<div class="webtoonmovie_list_date">2018.05.02</div>
					<div class="webtoonmovie_list_coin">3코인</div>
					<div class="webtoonmovie_list_rating"></div>
				</div>
				<div class="webtoonmovie_list_box">
					<div class="webtoonmovie_list_img"></div>
					<div class="webtoonmovie_list_title">
						<h3>1화</h3>
						<p>창 밖이 궁금해</p>
					</div>
					<div class="webtoonmovie_list_date">2018.05.02</div>
					<div class="webtoonmovie_list_coin">3코인</div>
					<div class="webtoonmovie_list_rating"></div>
				</div>
				<div class="webtoonmovie_list_box">
					<div class="webtoonmovie_list_img"></div>
					<div class="webtoonmovie_list_title">
						<h3>1화</h3>
						<p>창 밖이 궁금해</p>
					</div>
					<div class="webtoonmovie_list_date">2018.05.02</div>
					<div class="webtoonmovie_list_coin">3코인</div>
					<div class="webtoonmovie_list_rating"></div>
				</div>
				<div class="webtoonmovie_list_box">
					<div class="webtoonmovie_list_img"></div>
					<div class="webtoonmovie_list_title">
						<h3>1화</h3>
						<p>창 밖이 궁금해</p>
					</div>
					<div class="webtoonmovie_list_date">2018.05.02</div>
					<div class="webtoonmovie_list_coin">3코인</div>
					<div class="webtoonmovie_list_rating"></div>
				</div>
			</div>
			<!-- //webtoonmovie_list -->
			<!-- webtoonmovie_quick_box  -->
			<div class="webtoonmovie_quick_menu">
				<!-- quick_webtoob -->
				<div class="quick_webtoonmovie">
					<div class="w90">
						<div class="quick_webtoonmovie_title">Toonivie 추천작품</div>
						<!-- quick_webtoonmovie_tag  -->
						<div class="quick_webtoonmovie_tag">
							<div>#로맨스</div>
							<div>#액션</div>
							<div>#판타지</div>
							<div>#감성</div>
						</div>
						<!-- //quick_webtoonmovie_tag  -->
						<!-- quick_webtoonmovie_list  -->
						<div class="quick_webtoonmovie_list">
							<div class="quick_webtoonmovie_list_box">
								<div class="quick_webtoonmovie_img"></div>
								<div class="quick_webtoonmovie_intro">
									<h4>브라이트 하우스</h4>
									<p>작가</p>
								</div>
							</div>
							<div class="quick_webtoonmovie_list_box">
								<div class="quick_webtoonmovie_img"></div>
								<div class="quick_webtoonmovie_intro">
									<h4>브라이트 하우스</h4>
									<p>작가</p>
								</div>
							</div>
							<div class="quick_webtoonmovie_list_box">
								<div class="quick_webtoonmovie_img"></div>
								<div class="quick_webtoonmovie_intro">
									<h4>브라이트 하우스</h4>
									<p>작가</p>
								</div>
							</div>
							<div class="quick_webtoonmovie_list_box">
								<div class="quick_webtoonmovie_img"></div>
								<div class="quick_webtoonmovie_intro">
									<h4>브라이트 하우스</h4>
									<p>작가</p>
								</div>
							</div>
							<div class="quick_webtoonmovie_list_box">
								<div class="quick_webtoonmovie_img"></div>
								<div class="quick_webtoonmovie_intro">
									<h4>브라이트 하우스</h4>
									<p>작가</p>
								</div>
							</div>
						</div>
						<!-- //quick_webtoonmovie_list  -->
					</div>
				</div>
				<!-- //quick_webtoob -->
				<!-- quick_AD -->
				<div class="quick_AD">AD 광고<br>높이는 미정</div>
				<!-- //quick_AD -->
			</div>
			<!-- //webtoonmovie_quick_box  -->
		</div>
	</div>
	<!-- //webtoonmovie_list_content -->
</div>
<!-- //main  -->
<!-- footer  -->
<footer id="footer">
	<div class="container">
		<div class="footer_top">
			<ul>
				<li><a href="#">About 투니비</a></li>
				<li><a href="#">서비스약관</a></li>
				<li><a href="#">개인정보 처리방침</a></li>
				<li><a href="#">연재/광고 제휴 제안</a></li>
				<li><a href="#">쿠폰/상품권 등록</a></li>
				<li><a href="#">고객센터</a></li>
			</ul>
		</div>
		<div class="footer_bot">
			<div class="footer_bot_1">
				<div class="footer_logo">
					<div class="footer_logo_box">
						<a href="#"><img src="/resources/image/v2.0/footer_logo.png" alt="투니비 로고" title="투니비 로고"/></a>
					</div>
				</div>
				<div class="company_info">
					<p class="mb15" style="color:#fff;">(주) 아이디어 콘서트</p>
					<p>대표자명 : 전달용 | 사업자번호 : 338-88-00131 |</p>
					<p class="mb15">주소 : 경기도 성남시 수정구 성남대로 1342 가천대학교 법학대학 비전타워 7층 경기창조혁신센터</p>
					<p>TEL : 070. 8825. 5004</p>
					<p>Email : ideaconcert@ideaconcert.com</p>
				</div>
				<div class="languge">
					<p>Languge</p>
					<select>
					  <option value="volvo">한국어</option>
					  <option value="saab">English</option>
					  <option value="opel">日本語</option>
					  <option value="audi">中国語</option>
					</select>
				</div>
			</div>
			<div class="footer_bot_2">
				<div class="sns">
					<div class="sns_box">
						<a href="#"><img src="/resources/image/v2.0/sns_blog.png" alt="sns_blog" title="" /></a>
						<a href="#"><img src="/resources/image/v2.0/sns_facebook.png" alt="sns_facebook" title="sns_facebook" /></a>
						<a href="#"><img src="/resources/image/v2.0/sns_youtube.png" alt="sns_youtube" title="sns_youtube" /></a>
					</div>
				</div>
				<div class="copyright">
					<div>
						<p>투니비 웹사이트에 게시된 모든 컨텐츠들은 저작원법에 의거 보호받고 있습니다.</p>
						<p>저작권자 또는 (주)아이디어 콘서트의 승인없이 컨텐츠의 일부 또는 전부를 복제·전송·배포 및 기타의 방법으로 저작물을 이용할 경우에는</p>
						<p>저작권법에 의해 법적 조치에 처해질 수 있으므로 주의하시길 바랍니다.</p>
					</div>
				</div>
				<p class="copyright2">Copyright ⓒ 2015-2018 Ideaconcert All Rights Reserved.</p>
			</div>	
		</div>
	</div>
</footer>
<!-- //footer  -->
<%-- <c:import url="/WEB-INF/views/header&footer/footer.jsp" /> --%>
</body>
<script>
$(document).ready(function() {
	// main_banner add psh 2018-04-25 
	var owl = $('.owl-carousel');
	owl.owlCarousel({
	    items:1,
	    nav:true,
	    loop:true,
	    margin:10,
	    autoplay:true,
	    autoplayTimeout:5000,
	    autoplayHoverPause:true
	});
  })


// tooni webtoon movie content change  add psh 2018-04-26
function tooni_WM_menu(WM_menu){
	tooni_WM_menu_data = $(WM_menu).data("wmmenu");
	if(tooni_WM_menu_data == "day"){
		$(".WM_menu_day").addClass("WM_menu_active");
		$(".WM_menu_genre").removeClass("WM_menu_active");
		$("#WM_day").removeClass("dn");
		$("#WM_genre").addClass("dn");
		
	}else if(tooni_WM_menu_data == "genre"){
		$(".WM_menu_day").removeClass("WM_menu_active");
		$(".WM_menu_genre").addClass("WM_menu_active");
		$("#WM_day").addClass("dn");
		$("#WM_genre").removeClass("dn");
	}
}

//tooni webtoon movie day add psh 2018-04-26
function WM_day_menu(WM_day_menu){
	var menu_active  = WM_day_menu;
	$(menu_active).addClass("WM_day_menu_active").siblings().removeClass("WM_day_menu_active")
	WM_day_menu_data = $(WM_day_menu).data("wmdaymenu");
	if(WM_day_menu_data == "1"){
		//alert("월");
		$(".WM_day_mon").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "2"){
		//alert("화");
		$(".WM_day_tue").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "3"){
		//alert("수");
		$(".WM_day_wed").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "4"){
		//alert("목");
		$(".WM_day_thr").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "5"){
		//alert("금");
		$(".WM_day_fri").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "6"){
		//alert("토");
		$(".WM_day_sat").removeClass("dn").siblings().addClass("dn");
	}else if(WM_day_menu_data == "7"){
		//alert("일");
		$(".WM_day_sun").removeClass("dn").siblings().addClass("dn");
	}
}

//tooni webtoon movie genre add psh 2018-04-26
function WM_genre_menu(WM_genre_menu){
	var menu_active = WM_genre_menu;
	$(menu_active).addClass("WM_genre_menu_active").siblings().removeClass("WM_genre_menu_active");
	WM_genre_menu_data = $(WM_genre_menu).data("wmgenremenu");
	if(WM_genre_menu_data == 1){
		//alert("로맨스");
		$(".WM_genre_romance").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 2){
		//alert("판타지/무협");
		$(".WM_genre_fantasy").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 3){
		//alert("일상/드라마");
		$(".WM_genre_drama").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 4){
		//alert("학원/액션");
		$(".WM_genre_action").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 5){
		//alert("코믹/미스터리");
		$(".WM_genre_comic").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 6){
		//alert("BL");
		$(".WM_genre_bl").removeClass("dn").siblings().addClass("dn");
	}else if(WM_genre_menu_data == 7){
		//alert("성인");
		$(".WM_genre_adult").removeClass("dn").siblings().addClass("dn");
	}
}

//tooni webtoon movie content_align add psh 2018-04-26
/* function content_align(content_align){
	var menu_active = content_align;
	$(menu_active).addClass("content_align_active").siblings().removeClass("content_align_active").children("img").attr("src","/resources/image/v2.0/content_align_check_off.png");
	var c_a_a_child_img = $(menu_active).children("img");
	c_a_a_child_img.attr("src","/resources/image/v2.0/content_align_check_on.png");
} */
</script>

</html>