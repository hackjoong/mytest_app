<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>관리자 페이지</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/imgslide.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/memberList.css">
<link rel="stylesheet" href="/resources/common/css/webtoonList.css">
<link rel="stylesheet" href="/resources/common/css/slider.css">

<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/jquery-ui.min.js"></script>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/indexAjaxForm.js"></script>
<script src="/resources/common/js/setCsrf.js"></script>
<script src="/resources/common/js/loading.js"></script>

<body>
<c:if test="${currentUser == null ||currentUser.getRole()!='ROLE_ADMIN' }">
	<script>
		alert("로그인해주세요");
		location.href = "/login";
	</script>
</c:if>
	<c:import url="/WEB-INF/views/header&footer/admin_header.jsp" />
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>