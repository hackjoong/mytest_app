<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script src="/resources/common/js/form.js"></script>

<style type="text/css">
.center {
    margin-top:50px;   
}

.modal-header {
	padding-bottom: 5px;
}

.modal-footer {
    	padding: 0;
	}
    
.modal-footer .btn-group button {
	height:40px;
	border-top-left-radius : 0;
	border-top-right-radius : 0;
	border: none;
	border-right: 1px solid #ddd;
}
	
.modal-footer .btn-group:last-child > button {
	border-right: 0;
}
.modal_modifyToon{
	margin-top:35%;
}
</style>

<body>


<!-- line modal -->
<div class="modal fade" id="dapmodal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content modal_modifyToon">
		<div class="modal-header">
			<button type="button" class="close close_btn_dap" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">답글 정보</h3>
		</div>
		<div class="modal-body">
            <!-- content goes here -->
			<form name="form1" id="modify_dap" method="post">
			<input type="hidden" name="id" value="${currentUser.getId() }">
              <div class="form-group" style="width:49%; float: left;clear: both;">
                <label class="fl" for="exampleInputEmail1">CommNo</label>
                <input type="text" class="form-control" id="dapno" name="dno" placeholder="title" required="required">
              </div>
              <div class="form-group" style="width:49%; float: right;">
                <label class="fl" for="exampleInputPassword1">작성자</label>
                <input type="text" class="form-control commnick" id="dapnick" name="dwriter" placeholder="writer" required="required">
              </div>
              <div class="form-group">
                <label class="fl" for="exampleInputPassword1">내용</label>
                <textarea style="height:100px;" class="form-control commcont" id="dapcont" name="content" placeholder="content" required="required"></textarea>
              </div>
              <button type="button" onclick="modifyDap()" class="btn btn-default submit">작성</button>
              <button type="button" class="btn btn-default modify-dap">수정</button>
              <button type="button" class="btn btn-default cancel-btn-dap">취소</button>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default close_btn_dap" data-dismiss="modal"  role="button">Close</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
<script>
$(function(){
	$('.form-control').attr('readonly',true);

	$('.submit, .cancel-btn-dap').hide();
	$('.modify-dap').click(function(){
		var s = confirm("내용을 편집 하시겠습니까?");
		if(s == true){
			$(this).hide();
			$('.submit, .cancel-btn-dap').show();
			$('.form-control').removeAttr('readonly');
			$('#dapno, #dapnick').attr('readonly',true);
			$('.modal-title').text("답글 수정");
		}


	});
	
	$('.close_btn_dap').click(function(){
		$('.form-control').attr('readonly',true);
		$('.submit, .cancel-btn-dap').hide();
		$('.modify-dap').show();
		$('.modal-title').text("답글 정보");
	});
	$('.cancel-btn-dap').click(function(){
		var s = confirm("내용 수정을 취소하시겠습니까?");
		
		if(s == true){
			$('.form-control').attr('readonly',true);
			$('.submit, .cancel-btn-dap').hide();
			$('.modify-dap').show();
			$('.modal-title').text("답글 정보");
		}
	})
});

function modifyDap(){
		var dno = $('[name=dno]').val();
		var content = $('[name=content]').val();
		var dwriter= $('[name=dwriter]').val();
	$.ajax({
		url:'/admin/dapmodify',
		type:'post',
		data:{
			'dno':dno,
			'content':content,
			'dwriter':dwriter
		},
		success :function (data){
				if (data == 1) {
					alert("수정되었습니다.");
					$('.close_btn_dap').click();
					dathome();
				}
				else{
					alert("수정 불가!");
					$('.close_btn_dap').click();
					dathome();
				}
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "error:" + error);
			}
		})
	}
</script>
