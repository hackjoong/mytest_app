<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link href="/resources/common/css/commentAdmin.css" rel="stylesheet">
<script>
function load(cno){
	var comNo = $('.cno_'+cno).text();
	var comNick = $('.nickname_'+cno).text();
	var comCont = $('.comment_'+cno).text();
	$('#modify_comment #commno').val(comNo);
	$('#modify_comment #commnick').val(comNick);
	$('#modify_comment #commcont').val(comCont);
}

function loaddap(dno){
	var dapNo = $('.dno_'+dno).text();
	var dapNick = $('.dwriter_'+dno).text();
	var dapCont = $('.content_'+dno).text();
	$('#modify_dap #dapno').val(dapNo);
	$('#modify_dap #dapnick').val(dapNick);
	$('#modify_dap #dapcont').val(dapCont);
}







function commDelete(cno){
	var s = confirm("해당 댓글을 삭제하시겠습니까?");
	if(s == true){
		$.ajax({
			url:'/admin/commentdelete',
			type:'post',
			data:{
				'cno':cno
			},
			success :function (data){
				if (data == 1) {
					swal("삭제되었습니다.");
					dathome();
				}
				else{
					swal("삭제 불가!");
					dathome();
				}
			},
			error : function(request, status, error) {
				swal("code:" + request.status + "\n" + "error:" + error);
			}
		})
	}
}

function dapDelete(dno){
	var s = confirm("해당 답글을 삭제하시겠습니까?");
	if(s == true){
		$.ajax({
			url:'/admin/dapdelete',
			type:'post',
			data:{
				'dno':dno
			},
			success :function (data){
				if (data == 1) {
					swal("삭제되었습니다.");
					dathome();
				}
			},
			error : function(request, status, error) {
				swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					});
			}
		})
	}
}

function dathome(){
	$.ajax({
		type : "GET",
		url : "/admin/commentList",

		success : function(data) {
			$('#Context').html(data);
		},
		error : function() {
			swal('페이지를 불러오지 못했습니다.');

		}
	});
}
</script>
<c:import url="/WEB-INF/views/adminPage/comment/commodify.jsp" />
<c:import url="/WEB-INF/views/adminPage/comment/dapmodify.jsp" />
<c:set value="${CountCommentAll }" var="comm" />
<c:set value="${CountDapAll }" var="dap" />
<h2 class="re_admin_h2">댓글 관리 - 웹툰 무비</h2>
<div>
	<p class="fl re_admin_totalcomm">
		댓글 총 개수:
		<c:out value="${comm }" />
	</p>
</div>
<div class="commentwrap re_admin_commentwrap">
	<div style="width: 100%; float: left; height: 500px; overflow: auto; margin-bottom: 30px;">
		<table class="table table-striped" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="5%">tNO</th>
					<th scope="col" width="5%">cNO</th>
					<th scope="col" width="10%">작성자</th>
					<th scope="col" width="40%">내용</th>
					<th scope="col" width="20%">작성일</th>
					<th scope="col" width="20%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${CommentList }">
					<tr>
						<td scope="row" data-toggle="modal" data-target="#commodal"
							data-backdrop="static" data-keyboard="false">${item.getTno() }</td>

						<td scope="row" data-toggle="modal" data-target="#commodal"
							data-backdrop="static" data-keyboard="false"
							onclick="load(${item.getCno()})" class="cno_${item.getCno() }">${item.getCno() }</td>

						<td scope="row" data-toggle="modal" data-target="#commodal"
							data-backdrop="static" data-keyboard="false"
							onclick="load(${item.getCno()})"
							class="nickname_${item.getCno() }">${item.getNickname() }</td>

						<td scope="row" data-toggle="modal" data-target="#commodal"
							data-backdrop="static" data-keyboard="false"
							onclick="load(${item.getCno()})"
							class="comment_${item.getCno() }">${item.getComment() }</td>

						<td scope="row" data-toggle="modal" data-target="#commodal"
							data-backdrop="static" data-keyboard="false"
							onclick="load(${item.getCno()})"><fmt:formatDate
								value="${item.getCdate() }" type="date"
								pattern="yyyy.MM.dd hh:mm" /></td>

						<td scope="row"><button class="re_admin_btn"
								onclick="commDelete(${item.getCno()})">삭제</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	<p class="fl re_admin_totalcomm_1">
		답글 총 개수:
		<c:out value="${dap }" />
	</p>
	<!-- 답글쪽 -->
	<div style="width: 100%; float: right; height: 500px; overflow: auto; margin-bottom: 50px;">
		<table class="table table-striped" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="5%">cNo</th>
					<th scope="col" width="5%">rNO</th>
					<th scope="col" width="10%">작성자</th>
					<th scope="col" width="40%">내용</th>
					<th scope="col" width="20%">작성일</th>
					<th scope="col" width="20%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemdap" items="${replyList }">
					<tr>
						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false">${itemdap.getCno() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getDno()})"
							class="dno_${itemdap.getDno() }">${itemdap.getDno() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getDno()})"
							class="dwriter_${itemdap.getDno() }">${itemdap.getDwriter() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getDno()})"
							class="content_${itemdap.getDno() }">${itemdap.getContent() }</td>

						<td class="re_admin_loaddap_pc" scope="row" data-toggle="modal" data-target="#dapmodal"
							onclick="loaddap(${itemdap.getDno()})" data-backdrop="static"
							data-keyboard="false"><fmt:formatDate
								value="${itemdap.getDdate() }" type="date"
								pattern="yyyy.MM.dd hh:mm" /></td>
						<td scope="row"><button class="re_admin_btn"
								onclick="dapDelete(${itemdap.getDno()})">삭제</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>