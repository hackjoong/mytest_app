<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/resources/common/js/en/memberList.js"></script>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<div id="memberwrap">
	<p class="Total">
		Total number of member is
		<c:out value="${countMember_en }"></c:out>
		.
	</p>
	<br /> <br />
	<table id="memberlist">
		<tr id="mtr" class="head">
			<th id="mth">ID</th>
			<th id="mth">Name</th>
			<th id="mth">E-mail</th>
			<th id="mth">Authority</th>
			<th id="mth">Choice</th>
		</tr>
		<c:forEach items="${MemberList_en }" var="UserList">
		<tr id="mtr" class="mtr_list">
			<td id="mtd" onclick="memberInfo_en(${UserList.getUserseq() })" class="id_${UserList.getUserseq() }">${UserList.getId() }</td>
			<td id="mtd" onclick="memberInfo_en(${UserList.getUserseq() })" class="name_${UserList.getUserseq() }">${UserList.getName() }</td>
			<td id="mtd" onclick="memberInfo_en(${UserList.getUserseq() })" class="email_${UserList.getUserseq() }">${UserList.getEmail() }</td>
			<td id="mtd" onclick="memberInfo_en(${UserList.getUserseq() })" class="auth_${UserList.getUserseq() }">
				<c:if test="${UserList.getRole()=='ROLE_USER' }">User</c:if>
				<c:if test="${UserList.getRole()=='ROLE_ADMIN' }">Admin</c:if>
			</td>
			<td id="mtd" class="choice_box">
			<c:if test="${UserList.getRole()=='ROLE_USER' }">
				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}">
				<input type="button" value="Delete" class="choice" onclick="mDelete(${UserList.getUserseq()})" />
			</c:if>
			</td>
		</tr>
		</c:forEach>
	</table>
	<div class="info-modify">
		<p>
			Clicking on the member information you wish to modify will result in a modification form here.
		</p>
	</div>
	<div class="w100 fl wrap logins form-modify">
		<div class="login-form">
			<h3 class="login-title">Modify Member Information</h3>
			<form name="form1" id="form1" method="post" action="/member/update_en">
				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"> 
				<input type="hidden" name="userSeq2" value="">
			
				<div class="form-group fl w100">
					<label for="name" class="cols-sm-2 control-label fl">ID</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="id" id="id" placeholder="ID" readonly="readonly" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label fl">E-mail</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="email" id="email"	placeholder="E-mail" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100">
					<label for="username" class="cols-sm-2 control-label fl">Name</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="name" id="name" placeholder="Name" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100 pwform" style="width: 49.8%; float: left;">
					<label for="password" class="cols-sm-2 control-label fl">Password</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="pw" id="pw" placeholder="Password" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100 pwform" style="width: 49.8%; float: right;">
					<label for="confirm" class="cols-sm-2 control-label fl">Password Check</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="confirm" id="confirm" placeholder="Confirm Password" />
						</div>
					</div>
				</div>
				<div id="change_pw" style="float: right; margin: 0;">
					<input type="checkbox" id="checkbox_pw" class="checkbox_pw checkbox_pw_en" name="noChangedPW_en" checked="checked"/>No password change
					</div>
				<div class="cols-sm-10">
					<span class="check_pw_span" style='width:200px;float:left;'></span>
				</div>
				<div class="form-group fl w100">
					<label for="confirm" class="cols-sm-2 control-label fl">Authority</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="radio" class="auth auth_user_en" name="auth" value="ROLE_USER">User
							<input type="radio" class="auth auth_admin_en" name="auth" value="ROLE_ADMIN">Admin
						</div>
					</div>
				</div>
				
				<input type="button" class="submits submit-btn" value="Submit"
					onclick="modify_member();" style="width: 49%; float: left;" /> 
				<input type="button" class="submits submit-btn" value="Cancel"
					onclick="close_info();" style="width: 49%; float: right;" />
			</form>
		</div>
	</div>
</div>