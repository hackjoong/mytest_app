<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/en/banner_en.js"></script>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />

<div class="sliderwrap">
	<p class="Total">
		Total number of banners is  <c:out value="${CountBanner }"></c:out>.
	</p>
	<br/><br/>
	<div class="all">
	<div class="scroll">

		<table id="sliderlist">
		<tr id="str" class="head">
			<th id="sth">
				Image file
			</th>
			<th id="sth">
				Previous sequence
			</th>
			<th id="sth">
				Sequence
			</th>
			<th id="sth">
				Choice
			</th>
		</tr>
		<c:forEach items="${banner}" var="bannerList">
		<tr id="${bannerList.getSno() }_slide" class="bannerItem">
			<td id="std" width="45%">
				<img src="${bannerList.getB_file() }" id="bannerList_img">
			</td>
			<td id="std" width="30%">
				${bannerList.getOno() }
			</td>
			<td id="std"width="10%">
				<select name="order_${bannerList.getSno() }" form="order" class="order_${bannerList.getSno() } order">
				<c:forEach var="i" begin="1" end="${CountBanner }" step="1">
					<option><c:out value="${i}"></c:out></option>
				</c:forEach>
				</select>
				<%-- <input type="button" id="order_btn" onclick="submit(${bannerList.getSno()});" value="순서저장"> --%>
			</td>
			<td id="std" width="15%">
				<button class="choice" onclick="bDelete(${bannerList.getSno()})">Delete</button>
			</td>



			<td style="display: none">
				<input type="text" id="B_FILE_${bannerList.getSno() }" value="${bannerList.getB_file() }" name="filename" readonly="readonly">
			</td>
		</tr>
		</c:forEach>
		</table>
</div>
	<input type="button" id="order_btn" onclick="submit_Order();" value="Sequence Save">
</div>

	
	
	
	
	
	<!-- sdfaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa -->
	
	
	
	
	
	<div class="uploadwrap">
		<form id="upload_banner" action="/banner/upload" enctype="multipart/form-data">
			<div class="w100 fl mt15 re apd-zone">
				<div class="bannerbox">
					<input class="upload-baner" placeholder="Select File" disabled="disabled" style="width:76%">
					<label for="banner-info">Upload</label><input type="file" name="upfile" id="banner-info" class="kinp upload-inp king2 upload-hidden">
				</div>
			</div>
			<div class="w100 fl mt15 banner-upload-btns">
				<button type="button" class="btn-gray fl" onclick="upload_singlefile_1()">Upload</button>
				<button type="reset" class="btn-gray fl resetImg">cancel</button>
			</div>
		</form>
	</div>
	<div class="img">
		
		
		<div class="p">
			<img id="blah">
			<h4>Preview is displayed here.</h4>
		</div>
	</div>
</div>
<script>
function submit_Order() {
	var isOK = false;	
	var length = $('#sliderlist').find('select').length;
	for(var i=0;i<$('#sliderlist').find('select').length;i++){
		for(var j=0;j<$('#sliderlist').find('select').length;j++){
			if (j == i) continue;
			if( $('#sliderlist').find('select').eq(i).val() == $('#sliderlist').find('select').eq(j).val()) {
		    	isOK = false;
		    	break;
		   	}else{
				isOK = true;
		   	}	
		}			   
	}		
	console.log(isOK);	
	
	if (!isOK) {
		alert("중복된 값이 있습니다.");
	} else {
		var order = $('#sliderlist').find('.order option:checked').text();
		console.log(order);

		var snos = "";
		for (var i=0; i<$('#sliderlist').find('.bannerItem').length; i++) {
			var imsi = $('#sliderlist').find('.bannerItem').eq(i).attr('id');
			snos += imsi;
		}
		console.log(snos);
		
		var jsonString = "";
		jsonString+="{\"sno\":\"" + snos + "\",";
		jsonString+="\"ono\":\"" + order +"\"}";
		var data = {};
		data['data'] = jsonString;
		$.ajax({
			url:'/banner/order/insert',
			dataType: "json",
			type:'post',
			data: data,
			success:function(data){
				console.log(data.result);
				alert("성공적으로 변경되었습니다.");
				bannerList();
			},
			error : function(request, status, error) {
		          console.log("code:" + request.status + "\n" + "error:" + error);
		       }
		});
	}
	
	
	
}
function bannerList() {
    $.ajax({
        type: "GET",
        url: "banner/list_en",
        dataType: "text",
        success: function(data) {
            $('#Context').html(data);
            console.log(data);
        },
        error: function() {
            alert('페이지를 불러오지 못했습니다.');
        }
    });
}
</script>
