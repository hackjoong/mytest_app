<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/en/webtoonList.js"></script>
<script src="/resources/common/js/loading.js"></script>
<link rel="stylesheet" href="/resources/common/css/loading.css">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<style>
#language {
	width: 25%;
}

#language:hover {
	background-color: #ccc;
}
</style>

<div id="toonwrap">

	<p class="Total">
		Total number of Webtoon is
		<c:out value="${countToon }"></c:out>
		.
	</p>
	<br />
	<br />

	<table id="toonlist">
		<tr id="mtr" class="head">
			<th id="mth">Gnere</th>
			<th id="mth">Title</th>
			<th id="mth">Writer</th>
			<th id="mth">Content</th>
			<th id="mth">Date of preparation</th>
			<th id="mth">Choice</th>
		</tr>
		<c:forEach items="${webtoon }" var="ToonList">
			<tr id="mtr" class="list_wtr">
				<td id="mtd" style="display: none"
					class="file_${ToonList.getTno() }">${ToonList.getFile() }</td>
				<td id="mtd" width="11%" onclick="wUpdate(${ToonList.getTno()})"
					class="genre_${ToonList.getTno() }">${ToonList.getGenre() }</td>
				<td id="mtd" width="15%" onclick="wUpdate(${ToonList.getTno()})"
					class="title_${ToonList.getTno() }">${ToonList.getTitle() }</td>
				<td id="mtd" width="11%" onclick="wUpdate(${ToonList.getTno()})"
					class="writer_${ToonList.getTno() }">${ToonList.getWriter() }
				</td>
				<td id="mtd" width="35%" onclick="wUpdate(${ToonList.getTno()})"
					class="content_${ToonList.getTno() }"><span class="ellipsis">${ToonList.getContent() }</span>
				</td>
				<td id="mtd" width="18%" onclick="wUpdate(${ToonList.getTno()})">

					<fmt:formatDate value="${ToonList.getWridate() }" type="date"
						pattern="yyyy.MM.dd hh:mm:ss" />
				</td>
				<td id="mtd" width="10%" class="choice_box"><input
					type="button" value="Delete" class="choice"
					onclick="wDelete(${ToonList.getTno()})" /></td>
			</tr>
		</c:forEach>
	</table>
	<div class="w100 fl h100 re upload from">
		<div class="menu-cont-title">
			<span class="fl toonupload-text">Register a webtoon movie</span>
		</div>
		<div class="w100 fl">
			<form id="upload_webtoon_movie" action="/uploadWebtoon/upload"
				enctype="multipart/form-data">
				<div class="w100 fl mt15 re apd-zone">
					<label for="ex_filename"
						class="cols-sm-2 control-label fl movie-text">Video</label>
					<div id="change_no" style="width:25%; float: right;">
						<input type="checkbox" id="checkbox" name="movieChanged"
							checked="checked" />No change
					</div>
					<div class="filebox">
						<input class="upload-name" value="Select File" readonly="readonly" style="width:81%;">
						<label for="ex_filename" style="width:18%;">Upload</label><input type="file"
							name="upfile" id="ex_filename"
							class="kinp upload-inp king2 upload-hidden">
					</div>
					<div class="w100 fl tooni-title">
						<div class="tooni-head2">Enter Webtoon Movie Information</div>
						<div class="tooni-cont2">
							<input type="hidden" name="tno" id="tooninfo" placeholder="번호">

							<div class="form-group fl w100">
								<label for="genre" class="cols-sm-2 control-label fl">Genre</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="genre" id="tooninfo" placeholder="Genre">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">Title</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="title" id="tooninfo" placeholder="Title">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">Writer</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="writer" id="tooninfo"
											placeholder="Writer">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">Content</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<textarea rows="7" cols="30" name="content" id="tooncontent"
											placeholder="Content"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl mt15">
						<button type="button" class="btn-cyan fl submit_toon"
							onclick="upload_singlefile1()">Upload</button>
						<button type="reset" class="btn-cyan fl cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>