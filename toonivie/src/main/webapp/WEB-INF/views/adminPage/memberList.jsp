<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/resources/common/js/memberList.js"></script>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />

<h2 class="re_admin_h2">회원 관리</h2>
<div id="memberwrap" class="re_memberwrap">
	<p class="Total">
		총 유저는
		<c:out value="${countMember }"></c:out>
		명 입니다.
	</p>
	<br /> <br />
	<table id="memberlist" class="re_memberlist">
		<tr id="mtr" class="head">
			<th id="mth">ID</th>
			<th id="mth">이름</th>
			<th id="mth">이메일</th>
			<th id="mth">권한</th>
			<th id="mth" class="re_getUserseq">가입일</th>
			<th id="mth" class="re_getJoindate_600">최종접속일</th>
			<th id="mth">선택</th>
		</tr>
		<c:forEach items="${MemberList }" var="UserList">
		<fmt:timeZone value="KST">
		<tr id="mtr" class="mtr_list">
			<td id="mtd" onclick="memberInfo(${UserList.getUserseq() })" class="id_${UserList.getUserseq() }">${UserList.getId() }</td>
			<td id="mtd" onclick="memberInfo(${UserList.getUserseq() })" class="name_${UserList.getUserseq() }">${UserList.getName() }</td>
			<td id="mtd" class="re_getEmail email_${UserList.getUserseq() }" onclick="memberInfo(${UserList.getUserseq() })">${UserList.getEmail() }</td>
			<td id="mtd" onclick="memberInfo(${UserList.getUserseq() })" class="auth_${UserList.getUserseq() }">
				<c:if test="${UserList.getRole()=='ROLE_USER' }">사용자</c:if>
				<c:if test="${UserList.getRole()=='ROLE_ADMIN' }">관리자</c:if>
				<c:if test="${UserList.getRole()=='ROLE_WRITER' }">작가</c:if>
				<c:if test="${UserList.getRole()=='ROLE_COWORKER' }">매니저</c:if>
			</td>
			<td id="mtd" class="re_getJoindate" onclick="memberInfo(${UserList.getUserseq() })" class="regdate_${UserList.getUserseq() }">
				<fmt:formatDate  type ="date" value="${UserList.getJoindate() }" pattern="yyyy-MM-dd HH:mm:ss" />	
			</td>
			<td id="mtd" class="re_getJoindate_1 re_getJoindate_600" onclick="memberInfo(${UserList.getUserseq() })" class="regdate_${UserList.getUserseq() }">
				<fmt:formatDate  type ="date" value="${UserList.getJoindate() }" pattern="yyyy-MM-dd" />	
			</td>
			<td id="mtd" class="re_getUserseq" onclick="memberInfo(${UserList.getUserseq() })" class="condate_${UserList.getUserseq() }">
				<fmt:formatDate type ="date" value="${UserList.getConndate() }" pattern="yyyy-MM-dd HH:mm:ss" />
			</td>
			<td id="mtd" class="choice_box">
				<c:if test="${UserList.getRole()=='ROLE_USER'||UserList.getRole()=='ROLE_WRITER'||UserList.getRole()=='ROLE_COWORKER' }">
					<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}">
					<img src="/resources/image/icon/delete_icon.png" alt="삭제" class="cp w25 re_admin_mDelete" style="margin: 5px 0;" onclick="mDelete(${UserList.getUserseq()})">
					<%-- <input type="button" value="삭제" class="choice" onclick="mDelete(${UserList.getUserseq()})" /> --%>
				</c:if>
			</td>
		</tr>
		</fmt:timeZone>
		</c:forEach>
	</table>
	<div class="info-modify re_info_modify">
		<p>
			수정할 회원정보를 클릭하면 이곳에 수정 폼이 뜹니다.
		</p>
	</div>
	<div class="w100 fl logins form-modify admm2 re_form_modify" style="width:30%; float: right; margin-top:-10px;">
		<div class="login-form re_login_form">
			<h3 class="login-title">회원정보수정</h3>
			<form name="form1" id="form1" method="post" action="/member/update">
				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"> 
				<input type="hidden" name="userSeq2" value="">
			
				<div class="form-group fl w100">
					<label for="name" class="cols-sm-2 control-label fl">아이디</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="id" id="id" placeholder="ID" readonly="readonly" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label fl">이메일</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="email" id="email"	placeholder="E-mail" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100">
					<label for="username" class="cols-sm-2 control-label fl">이름</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="name" id="name" placeholder="Name" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100 pwform" style="width: 49.8%; float: left;">
					<label for="password" class="cols-sm-2 control-label fl">비밀번호</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="pw" id="pw" placeholder="Password" />
						</div>
					</div>
				</div>
				<div class="form-group fl w100 pwform" style="width: 49.8%; float: right;">
					<label for="confirm" class="cols-sm-2 control-label fl">비밀번호
						확인</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="confirm" id="confirm" placeholder="Confirm Password" />
						</div>
					</div>
				</div>
				<div id="change_pw" style="float: right; margin: 0;">
					<input type="checkbox" id="checkbox_pw" class="checkbox_pw checkbox_pw_kr" name="changedPW_kr" checked="checked"/>비밀번호 변동 없음
					</div>
				<div class="cols-sm-10">
					<span class="check_pw_span"></span>
				</div>
				<div class="form-group fl w100">
					<label for="confirm" class="cols-sm-2 control-label fl">권한</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="radio" class="auth auth_user .admm6" name="auth" value="ROLE_USER">사용자&nbsp;&nbsp;
							<input type="radio" class="auth auth_admin .admm6" name="auth" value="ROLE_ADMIN">관리자&nbsp;&nbsp;
							<input type="radio" class="auth auth_writer .admm6" name="auth" value="ROLE_WRITER">작가&nbsp;&nbsp;
							<input type="radio" class="auth auth_manager .admm6" name="auth" value="ROLE_COWORKER">매니저
						</div>
					</div>
				</div>
				
				<input type="button" class="submits submit-btn" value="수정"
					onclick="modify_member();" style="width: 49%; float: left;" /> 
				<input type="button" class="submits submit-btn" value="취소"
					onclick="close_info();" style="width: 49%; float: right;" />
			</form>
		</div>
	</div>
</div>