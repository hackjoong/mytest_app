<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/banner.js"></script>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<h2 class="re_admin_h2">원작 웹툰 - 웹툰 무비</h2>
<div class="sliderwrap re_admin_wtoonwrap admin_mt30">
	<p class="Total">
		 총 배너의 개수는 <c:out value="${CountBanner }"></c:out>개 입니다.
	</p>
	<br/>
	<br class="re_admin_brSlider">
	<div class="all re_admin_all">
		<input type="button" id="order_btn" style="margin-left:10px;" onclick="submit_Address_wonjak();" value="주소저장">
		<input type="button" id="order_btn" onclick="submit_Order_wonjak();" value="순서저장">
	<div class="scroll admm3 re_admin_scrolladmm3">

		<table id="sliderlist">
		<tr id="str" class="head">
			<th id="sth">
				이미지 파일
			</th>
			<th id="sth" class="re_admin_sliderDN">
				이전 순서
			</th>
			<th id="sth">
				순서
			</th>
			<th id="sth" class="re_admin_sliderDN">
				이전 주소
			</th>
			<th id="sth">
				주소
			</th>
			<th id="sth">
				선택
			</th>
		</tr>
		<c:forEach items="${banner}" var="bannerList">
		<tr id="${bannerList.getSno() }_slide" class="bannerItem">
			<td id="std" class="re_admin_banner_img">
				<img src="${bannerList.getB_file() }" id="bannerList_img" class="re_admin_bannerList_img">
			</td>
			<td id="std" class="re_admin_sliderDN" width="15%">
				${bannerList.getOno() }
			</td>
			<td id="std" class="re_admin_banner_order">
				<select name="order_${bannerList.getSno() }" form="order" class="order_${bannerList.getSno() } order order-no">
				<c:forEach var="i" begin="1" end="${CountBanner }" step="1">
					<option><c:out value="${i}"></c:out></option>
				</c:forEach>
				</select>
			</td>
			<!-- 주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소주소 -->
			<td id="std" class="re_admin_sliderDN" width="10%">
				${bannerList.getJuso() }
			</td>
			<td id="std" class="re_admin_banner_juso">
				<select name="tttnnnooo" form="" class="order-tno">
				<c:forEach var="ttno" items="${nono }">
					<option value="${ttno.getWtno() }">${ttno.getWtname() }</option>
				</c:forEach>
				</select>
			</td>
			<td id="std" class="re_admin_slider_delete" width="15%">
				<button class="choice re_admin_slider_choice" onclick="bDelete(${bannerList.getSno()})">삭제</button>
			</td>



			<td style="display: none">
				<input type="text" id="B_FILE_${bannerList.getSno() }" value="${bannerList.getB_file() }" name="filename" readonly="readonly">
			</td>
		</tr>
		</c:forEach>
		</table>
</div>

</div>

	<!-- 수정본  -->
	<div class="re_admin_uploadwrap7">
		<span class="fl toonupload-text re_admin_toonupload">배너 올리기</span>
		<form id="upload_banner" action="/banner/upload" enctype="multipart/form-data">
			<div class="w100 fl re apd-zone">
				<div class="bannerbox">
					<input class="upload-baner fl" placeholder="파일선택" disabled="disabled">
					<label class="re_admin_banner_info" for="banner-info">등록</label><input type="file" name="upfile" id="banner-info" class="kinp upload-inp king2 upload-hidden">
				</div>
			</div>
			<input type="hidden" value="origintoon" name="kinds">
			<div class="w100 fl mt15 re_banner_banner-_pload_btns">
				<button type="button" class="btn-gray fl" onclick="upload_singlefile_1()">업로드</button>
				<button type="reset" class="btn-gray fl resetImg">취소</button>
			</div>
		</form>
		<div class="re_admin_img">
			<div class="p" style="height: 100%;">
				<img id="blah">
				<h4 class="re_admin_slider_4h">이곳에 미리보기가 표시됩니다.</h4>
			</div>
		</div>
	</div>
	<!-- //수정본  -->
	
</div>
<script>
function submit_Order_wonjak() {
	var isOK = true;	
	var length = $('#sliderlist').find('.order-no').length;
	for(var i=0;i<$('#sliderlist').find('.order-no').length;i++){
		for(var j=0;j<$('#sliderlist').find('.order-no').length;j++){
			if (j == i) continue;
			if( $('#sliderlist').find('.order-no').eq(i).val() == $('#sliderlist').find('.order-no').eq(j).val()) {
		    	isOK = false;
		    	break;
		   	}else{
				isOK = true;
		   	}	
		}			   
	}			
	
	if (!isOK) {
		swal("순서에 중복된 값이 있습니다.");
	} else {
		var order = $('#sliderlist').find('.order option:checked').text();

		var snos = "";
		for (var i=0; i<$('#sliderlist').find('.bannerItem').length; i++) {
			var imsi = $('#sliderlist').find('.bannerItem').eq(i).attr('id');
			snos += imsi;
		}
		var jsonString = "";
		jsonString+="{\"sno\":\"" + snos + "\",";
		jsonString+="\"ono\":\"" + order +"\"}";
		var data = {};
		data['data'] = jsonString;
		$.ajax({
			url:'/banner/order/insert',
			dataType: "json",
			type:'post',
			data: data,
			success:function(data){
				swal("배너 순서가 성공적으로 변경되었습니다.");
				bannerList_wonjak();
			},
			error : function(request, status, error) {
		       }
		});
	}
}

function submit_Address_wonjak(){
	var isOK = true;	
	var length = $('#sliderlist').find('[name=tttnnnooo]').length;
	for(var i=0;i<$('#sliderlist').find('[name=tttnnnooo]').length;i++){
		for(var j=0;j<$('#sliderlist').find('[name=tttnnnooo]').length;j++){
			if (j == i) continue;
			if( $('#sliderlist').find('[name=tttnnnooo]').eq(i).val() == $('#sliderlist').find('[name=tttnnnooo]').eq(j).val()) {
		    	isOK = false;
		    	break;
		   	}else{
				isOK = true;
		   	}	
		}			   
	}			
	
	if (!isOK) {
		swal("배너주소에 중복된 값이 있습니다.");
	} else {
		var snos = "";
		var jusos = "";
		for (var i=0; i<$('#sliderlist').find('.bannerItem').length; i++) {
			var imsi = $('#sliderlist').find('.bannerItem').eq(i).attr('id');
			snos += imsi;
			var juso = $('#sliderlist').find('.bannerItem').eq(i).find('.order-tno option:checked').val()+",";
			jusos+=juso;
		}
		
		var kinds = $('[name=kinds]').val();
		var jsonString = "";
		jsonString+="{\"sno\":\"" + snos + "\",";
		jsonString+="\"kinds\":\"" + kinds + "\",";
		jsonString+="\"juso\":\"" + jusos +"\"}";
		var data = {};
		data['data'] = jsonString;
		$.ajax({
			url:'/banner/juso/insert',
			dataType: "json",
			type:'post',
			data: data,
			success:function(data){
				swal("배너주소가 성공적으로 변경되었습니다.");
				bannerList_wonjak();
			},
			error : function(request, status, error) {
		       }
		});
	}
}
function bannerList_wonjak() {
    $.ajax({
        type: "GET",
        url: "/banner/list/wonjak",
        dataType: "text",
        success: function(data) {
            $('#Context').html(data);
        },
        error: function() {
            swal('페이지를 불러오지 못했습니다.');
        }
    });
}
</script>
