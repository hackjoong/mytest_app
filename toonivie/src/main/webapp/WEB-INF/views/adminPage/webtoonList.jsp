<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" content="${_csrf.parameterName}" />
<!-- custom CSS -->
<link href="/resources/common/css/admin.css" rel="stylesheet">
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webtoonList.css">
<link rel="stylesheet" href="/resources/common/css/loading.css">
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/webtoonList.js"></script>
<script src="/resources/common/js/loading.js"></script>
<script src="/resources/common/js/setCsrf.js"></script>
<style type="text/css">
.table-striped-wonjak{
	overflow: auto;
}
.upload {margin-top: 0 !important;}
.striped-wonjak{
	display: block;
	height:600px;
	overflow: auto;
}
#wtoonwrap {
	width: 90%;
	margin-top: 60px;
	margin-left: 30px;
	height: 800px;
}
.choice3 {
    width: 80%;
    height: 33px;
    /* padding:15px 10px; */
    border-radius: 50px;
    background: none;
    color: #777;
    font-size: 12px;
    border: 1px solid #ccc;
    line-height: 0;
    outline: none;
}
</style>
<h2 class="re_admin_h2">작품 관리 - 웹툰 무비</h2>
<div id="toonwrap" class="m_re_toonwrap re_admin_toonwrap">
	<!-- 수정본 -->
	<div class="fl re_admin_wonjak_table" style="width:60%;">
		<div class="fl" style="width:100%;">
			<span class="fr" style="margin-top:9px; font-size:14px; color:#d3394c">※해당 웹툰무비를 클릭하면 수정할 수 있습니다.</span>
		</div>
		<div class="striped-wonjak" style="width:100%;">
		<table class="table table-striped table-striped-wonjak " style="width: 100%; font-size:14px;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="14%">제목</th>
					<th scope="col" width="14%">장르</th>
					<th scope="col" width="14%">작가</th>
					<th class="m_wonjak_col1_ah re_adming_1000" scope="col" width="20%">내용</th>
					<th class="m_wonjak_col1_ah re_adming_1000" scope="col" width="10%">공개 여부</th>
					<th class="m_wonjak_col2_ah" scope="col" width="18%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${webtoon }" var="ToonList">
					<tr>
						<td id="mtd" class="dn wmno_${ToonList.getTno()}">${ToonList.getWmno() }</td>
						<td id="mtd" class="dn idx_${ToonList.getTno()}">${ToonList.getId() }</td>
						<td id="mtd" class="dn tth_${ToonList.getTno()}">${ToonList.getThumbnail() }</td>
						<td id="mtd" class="dn tfile_${ToonList.getTno()}">${ToonList.getFile() }</td>
						<td id="mtd" width="20%" onclick="adtoonUpdate(${ToonList.getTno()})" class="title_${ToonList.getTno() }">${ToonList.getTitle() }</td>
						<td id="mtd" width="11%" onclick="adtoonUpdate(${ToonList.getTno()})" class="genre_${ToonList.getTno() }">${ToonList.getGenre() }</td>
						<td id="mtd" width="11%" onclick="adtoonUpdate(${ToonList.getTno()})" class="writer_${ToonList.getTno() }">${ToonList.getWriter()}</td>
						<td id="mtd" width="20%" onclick="adtoonUpdate(${ToonList.getTno()})" class="content_${ToonList.getTno() } re_adming_1000"><span class="ellipsis" style="width: 150px;">${ToonList.getContent() }</span></td>
						<td scope="row" class="free_open_${ToonList.getTno() }"  width="10%" onclick="adtoonUpdate(${ToonList.getTno()})" data-open="${ToonList.getFree_open()}"><span>
						<c:if test="${ToonList.getFree_open() eq true}">
  						 	 O
						</c:if>
						<c:if test="${ToonList.getFree_open() eq false}">
  						 	 X
						</c:if>
						</span></td>
						<td id="mtd" width="15%" class="choice_box"><input type="button" value="삭제" class="choice"onclick="adtoonDelete(${ToonList.getTno()})" /></td>
				</c:forEach>
			</tbody>
		</table>
		</div>
	</div>
	<!-- //수정본 -->
	<!-- 원본 -->
	<div class="w100 fl h100 re upload from m_upload_re re_admin_upload">
		<div class="menu-cont-title">
			<span class="fl toonupload-text re_admin_toonupload">웹툰무비 올리기</span>
		</div>
		<div class="w100 fl">
			<form id="upload_mywebtoon_movie" method="POST" 
				enctype="multipart/form-data">
				<input type="hidden" name="csrf" value="${_csrf.token}" />
				<input type="hidden" name="csrf_header" value="${_csrf.headerName}" />
				<input type="hidden" name="csrf_parameter" value="${_csrf.parameterName}"/>
				<div class="w100 fl mt15 re apd-zone" style="margin-top:0;">
					<label for="ex_filename" class="cols-sm-2 control-label fl movie-text">영상</label>
					<div id="change_no">
						<input type="checkbox" id="checkbox" name="movieChanged" checked="checked" />변동 없음
					</div>
						<input type="checkbox" id="check_free" name="check_free" value="true" />공개 여부
					<div class="filebox">
						<input class="upload-name re_admin_upload_name" value="파일선택" readonly="readonly">
						<label for="mywebmovieFile" class="re_admin_mywebmovieFile">업로드</label>
						<input type="file" name="upfile" id="mywebmovieFile" class="kinp upload-inp king2 upload-hidden" required="required">
					</div>
					<div class="w100 fl tooni-title">
						<div class="tooni-head2" style="margin-top:10px;">웹툰무비 정보입력</div>
						<div class="tooni-cont2">
							<input type="hidden" name="tno" placeholder="번호">
							<input type="hidden" name="idx" id="tooninfo_no" placeholder="ID">
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">시리즈 작품</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
									<select name="seriestitle" class="fl form-control uploadToon-input">
										<option value="0" selected>없음</option>
										<c:forEach var="seriesTitle" items="${mySeriesInfoAll }" >
										<option value="${seriesTitle.getWmno() }">${seriesTitle.getWmtitle() }</option>
										</c:forEach>
									</select>

									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="genre" class="cols-sm-2 control-label fl">장르</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="genre" id="tooninfo_genre" placeholder="장르" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">제목</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="title" id="tooninfo_title" placeholder="제목" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">작가</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="writer" id="tooninfo_writer"
											placeholder="작가" value="" required="required">
										<input type="hidden" name="id" id="tooninfo_id"
											placeholder="아이디" value="${currentUser.getId() }" readonly="readonly" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">내용</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<textarea rows="7" cols="30" name="content" id="tooncontent"
											placeholder="내용"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl mt15">
						<button type="button" class="btn-orange fl submit_toon"
							onclick="mypage_upload_moviefile1()">업로드</button>
						<button type="reset" class="btn-orange fl cancel">취소</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- //원본 -->	
</div>