<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<script src="/resources/common/js/checkId.js"></script>
<title>Insert title here</title>
</head>
<style>
.writerwrap {
	width: 90%;
	margin-top: 60px;
	margin-left: 30px;
	height: 800px;
}

.writer_choice {
	margin: 5px 0 20px 0;
	font-size: 15px;
}

.view_writer {
	border: 0;
	padding: .2em .75em;
	margin-left: 5px;
	background-color: #2C3E50;
	color: #fff;
}

.writer_content {
	clear: both;
	width: 100%;
	font-size: 16px;
}

.writer_name {
	font-weight: bold;
}

#Writer_toon_info, #Writer_info {
	width: 100%;
	margin-bottom: 30px;
}

.wtr {
	border-bottom: 1px solid #ccc;
	line-height: 25px;
}

.writer_head {
	background-color: #eee;
	border-top: 1px solid #000;
}

.wth {
	text-align: center;
	padding: 2px 0;
}

.wtr>td {
	padding: 2px 0;
}

.m0 {
	margin: 0;
	line-height: 1.5;
}

.viewAll {
	overflow: auto;
	width: 100%;
	height: 540px;
}
#nickcheckbtn {
	width: 13%;
	height: 34px;
	background: #304051;
	border: 0;
	color: #fff;
	collspace:collspace;
	font-size:12px;
}
</style>
<script>
	function search_writer() {
		var ws = $("#selectBox option:selected").val();
		$(".writer_content").hide();
		$("." + ws).show();

		if (ws == "전체") {
			$(".writer_content").show();
		}
	}
</script>
<body>
<h2 class="re_admin_h2">작가 관리</h2>
	<div class="writerwrap re_admin_wtoonwrap">
		<div class="fl re_admin_writer_contents">
			<p class="Total">
				총
				<c:out value="${CountWriter }"></c:out>
				명의 작가가 활동중 입니다.
			</p>
			<div class="fr writer_choice">
				<select name="hello" id="selectBox">
					<option selected>전체</option>
					<c:forEach var="WriterName" items="${WriterNameAll }">
						<option>${WriterName.getWriter() }</option>
					</c:forEach>
				</select>
				<button type="button" class="view_writer" onclick="search_writer()">검색</button>
			</div>
			<div class="fl viewAll">
				<c:forEach var="WriterName" items="${WriterNameAll }">
					<div class="fl writer_content ${WriterName.getWriter() }">

						<p class="fl m0 admm5">
							<span class="writer_name cp writer_${WriterName.getWriter() }"
								 style="color: #6192CC;">${WriterName.getWriter() }</span>
							작가님 (${WriterName.getId()})
						</p>
						<table id="Writer_toon_info">
							<tr class="wtr writer_head">
								<th class="wth re_admin_WriterToon" width="15%">장르</th>
								<th class="wth" width="30%">제목</th>
								<th class="wth" width="10%">조회수</th>
								<th class="wth" width="10%">댓글수</th>
								<th class="wth" width="35%">작성일</th>
							</tr>
							<c:forEach var="WriterToon" items="${WriterToonAll }">
								<c:if test="${WriterToon.getWriter()== WriterName.getWriter()}">
									<tr class="wtr">
										<td class="re_admin_WriterToon">${WriterToon.getGenre() }</td>
										<td>${WriterToon.getTitle() }</td>
										<td>${WriterToon.getViewCount() }</td>
										<td>${WriterToon.getCommentCount() }</td>
										<td class="re_admin_getWridate_pc">
											<fmt:formatDate value="${WriterToon.getWridate() }" type="date" pattern="yyyy.MM.dd hh:mm:ss" />
										</td>
										<td class="re_admin_getWridate_m">
											<fmt:formatDate value="${WriterToon.getWridate() }" type="date" pattern="yyyy.MM.dd" />
										</td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
						<table id="Writer_info" style="display: none;">
							<tr class="wtr writer_head">
								<th class="wth" width="15%">아이디</th>
								<th class="wth" width="30%">이름</th>
								<th class="wth" width="10%">이메일</th>
								<th class="wth" width="10%">닉네임</th>
								<th class="wth" width="35%">대표작품</th>
							</tr>
							<c:forEach var="WriterInfo" items="${WriterInfoAll }">
								<c:if test="${WriterInfo.getId() == WriterName.getId()}">
									<tr class="wtr">
										<td class="id_${WriterInfo.getNickname() }">${WriterInfo.getId() }</td>
										<td class="name_${WriterInfo.getNickname() }">${WriterInfo.getName() }</td>
										<td class="email_${WriterInfo.getNickname() }">${WriterInfo.getEmail() }</td>
										<td class="nickname_${WriterInfo.getNickname() }">${WriterInfo.getNickname() }</td>
										<td class="work_${WriterInfo.getNickname() }">${WriterInfo.getWork() }</td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
					</div>
					
				</c:forEach>
			</div>
			
		</div>
		<div class="info-modify fr 1 admm4 re_info_modify">
			<p>작가이름을 클릭하면 이곳에 작가정보가 출력됩니다.(준비중)</p>
		</div>
		<div class="info-modify fr 2 re_admin_artistInfo">
			<h2>작가정보</h2>
			<form class="writer-change" method="post" action="/writer/update">

				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}">
				<div class="form-group fl w100" style=" width:49%; float: left;">
					<label for="name" class="cols-sm-2 control-label fl">아이디</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="id" id="id"
								placeholder="ID" style="" />
						</div>
					</div>
				</div>

				<div class="form-group" style="width:49%; float: right;">
					<label for="email" class="cols-sm-2 control-label fl">이메일</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="email" id="email"
								placeholder="E-mail" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="username" class="cols-sm-2 control-label fl">이름</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="name" id="name"
								placeholder="Name" />
						</div>
					</div>
				</div>
				
				<div class="form-group fl" style="width:49%">
					<label for="username" class="cols-sm-2 control-label fl">닉네임</label>
					<span class="re_admin_nickname" style="float: right;"><input type="checkbox" class="checkbox_nick" name="checkbox_nick" checked>닉네임 변경 안함</span>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="nickname" id="nickname"
								placeholder="Name" style="border-radius:0;"/>
							<input type="hidden" id="nickChecked" size="30">
							<button type="button" id="nickcheckbtn" style="width:25%;">중복검사</button>
						</div>
					</div>
				</div>
				<div class="form-group fr" style="width:49%">
					<label for="username" class="cols-sm-2 control-label fl">대표작품</label>
					<span class="re_admin_nickname2" style="coler:red;color: red;font-weight: normal;font-size: 11.3px;float: right;">※ 없으면 미기입!</span>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="work"
								placeholder="Name" />
						</div>
					</div>
				</div>
				<div class="fr" style="font-size:15px; width:55%;"><span style="float: right;"><input type="checkbox" class="checkbox_pw" name="checkbox_pw" checked>비밀번호 변경 안함</span></div>
				<div class="changePw">
				<div class="form-group fl w100">
					<label for="password" class="cols-sm-2 control-label fl">비밀번호</label>
					<span class="re_PW_redText" style="coler:red;color: red;font-weight: normal;font-size: 14px;float: right;">※ 비밀번호는 8자 이상 20자 이하로 입력!</span>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="pw"
								id="pw" minlength="8" maxlength="20" placeholder="Password" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="confirm" class="cols-sm-2 control-label fl">비밀번호
						확인</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw"
								name="confirm" id="confirm" minlength="8" maxlength="20" placeholder="Confirm Password"/>
						</div>
					</div>
					<div class="cols-sm-10">
						<span class="check_pw_span" style="font-size:15px;"></span>
					</div>
				</div>
				</div>
				<input type="button" class="submits submit-btn info-submit" value="수정" style="width: 49%; float: left; background:#304051;" /> 
				<input type="button" class="submits submit-btn" value="취소"
					onclick="close_info()" style="width: 49%; float: right; background:#304051;" />
			</form>
		</div>
	</div>
</body>
<script>



var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$('.changePw').hide();
	$('.checkbox_pw').click(function(){
		if($('.checkbox_pw').prop('checked')==false){
			$('.2').css('height','80%');
		    $('.changePw').show();
		}
		else{
			$('.2').css('height','');
			$('.changePw').hide();
		}
	})
	$('.checkbox_nick').click(function(){
		if($('.checkbox_nick').prop('checked')==false){
			$('#nickname').removeAttr('readonly');
			$('#nickname').css('width','75%');
			$('#nickcheckbtn').show();
		}
		else{
			$('#nickname').attr('readonly',true);
			$('#nickname').css('width','100%');
			$('#nickcheckbtn').hide();
		}
	})
});

$(".writer_name").click(function() {
	$('.1').hide();
	$('.2').show();
	var nickname = $(this).text();

	Info_list(nickname);
});

function Info_list(nickname){
	var idValue= $('.id_'+nickname).text();
	var nameValue= $('.name_'+nickname).text();
	var emailValue= $('.email_'+nickname).text();
	var nickValue= $('.nickname_'+nickname).text();
	var workValue= $('.work_'+nickname).text();
	console.log(idValue+","+nameValue+","+emailValue+","+nickValue+","+workValue);
	
	$('[name=id]').val(idValue);
	$('[name=email]').val(emailValue);
	$('[name=name]').val(nameValue);
	$('[name=nickname]').val(nickValue);
	$('[name=work]').val(workValue);
	
	$('input').attr('readonly',true);
	$('.checkbox_pw, .checkbox_nick').attr('disabled',true);
	$('#nickcheckbtn, #checkbtn').hide();
	$('#nickname').css('width','100%');
	$('.checkbox_pw').prop('checked', true);
	$('.info-submit').val("수정").attr("onclick","modify_start()");
}
function modify_start(){
	var a = confirm("열람을 취소하고 수정하시겠습니까?");
	if (a == true){
		$('input').not("#id, #nickname").removeAttr('readonly');
		$('.checkbox_pw, .checkbox_nick').removeAttr('disabled');
		$('.info-submit').val("정보 수정").attr('onclick','modify_writer()');
		$('.writer-change').attr('onsubmit','return modify_writer()');
		/* $('#nickname').css('width','75%');
		$('#nickcheckbtn').show(); */
	}

}
$(".confirm_pw").keyup(function() {
	var pw = $("[name = pw]").val();
	var re_pw = $("[name=confirm]").val();
	if (pw != re_pw || pw == "" || re_pw == "") {
		$(".check_pw_span").css("color", "red");
		$(".check_pw_span").text("비밀번호가 일치하지 않습니다.");
	} else {
		$(".check_pw_span").css("color", "blue");
		$(".check_pw_span").text("비밀번호가 일치합니다.");
	}
});

function close_info() {
    var a = confirm("열람/수정을 취소하시겠습니까?");
    if (a == true) {
		$('.2').css('height','');
    	$('.changePw').hide();
    	$('.checkbox_pw').attr("checked", true);
    	$('.info-submit').val("수정").attr("onclick","modify_start()");
        $('.2').hide();
        $('.1').show();
        $('#nickname').css('width','100%');
		$('#nickcheckbtn, #checkbtn').hide();
		
    }
}

function modify_writer(){
	var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
    var pwReg = /([0-9A-Za-z]{4,11})/g;
    var nameReg = /([가-힣A-Za-z]{2,4})/g;
    var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
    var idValue = $("[name = id]").val();
    var pwValue = $("[name = pw]").val();
    var nameValue = $("[name = name]").val();
    var emailValue = $("[name = email]").val();
    var confirmValue = $("[name = confirm]").val();
    var nicknameValue = $("[name = nickname]").val();
    var workValue = $("[name = work]").val();
    var check = $('input:checkbox[name="checkbox_pw"]').is(":checked");
 
    if($('.checkbox_pw').prop('checked')==false){// 체크 안되어있으면
    	if ( idValue == "") {
            alert("아이디를 확인해주세요.");
            return false;
        } else if (emailReg.test(emailValue) == false || emailValue == "") {
            alert("이메일을 확인해주세요.");
            return false;
        } else if (nameReg.test(nameValue) == false || nameValue == "") {
            alert("이름을 확인해주세요.");
            return false;
        }else if (nicknameValue == "") {
			alert("닉네임을 확인해주세요.");
			return false;
		} /* else if($("#nickChecked").val() != "nickChecked"){
			alert("닉네임 중복확인해주세요");
			return false;
		}  */else if(pwValue.length<8 || confirmValue.length<8 || pwValue.length>20 || confirmValue.length>20){
			alert("비밀번호 길이가 맞지 않습니다. 다시 입력해주세요");
			return false;
		} else if (pwValue != confirmValue || pwReg.test(pwValue) == false ||
            pwValue == "") {
            alert("비밀번호를 확인해주세요.");
            return false;
        }
	}
	else{
		 if (idValue == "") {
		        alert("아이디를 확인해주세요.");
		        return false;
		    } else if (emailReg.test(emailValue) == false || emailValue == "") {
		        alert("이메일을 확인해주세요.");
		        return false;
		    } else if (nameReg.test(nameValue) == false || nameValue == "") {
		        alert("이름을 확인해주세요.");
		        return false;

		    }
	}
    $.ajax({
    	url:'/writer/update',
    	type:'POST',
    	data:{
    		'id': idValue,
            'name': nameValue,
            'email': emailValue,
            'pw': pwValue,
            'nickname':nicknameValue,
            'work': workValue,
            'noChangePW':check
    	},
    	success:function(data){
    		if(data == 1){
    			alert('수정되었습니다.');
    			home();
    			
    		}
    	},
        error: function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    	
    });
}
function home(){
	$.ajax({
		type : "GET",
		url : "/writer/list",
		error : function() {
			alert('페이지를 불러오지 못했습니다.');
		},
		success : function(data) {
			$('#Context').html(data);
		}

	});
}

</script>
</html>