<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link href="/resources/common/css/commentAdmin.css" rel="stylesheet">
<script>
function load(cno){
	var comNo = $('.cno_'+cno).text();
	var comNick = $('.nickname_'+cno).text();
	var comCont = $('.comment_'+cno).text();
	$('#modify_comment #commno').val(comNo);
	$('#modify_comment #commnick').val(comNick);
	$('#modify_comment #commcont').val(comCont);
}

function loaddap(dno){
	var dapNo = $('.dno_'+dno).text();
	var dapNick = $('.dwriter_'+dno).text();
	var dapCont = $('.content_'+dno).text();
	$('#modify_dap #dapno').val(dapNo);
	$('#modify_dap #dapnick').val(dapNick);
	$('#modify_dap #dapcont').val(dapCont);
}







function commDelete(cno){
	swal("서비스 준비중!");
}

function dapDelete(dno){
	swal("서비스 준비중!");
}

function dathome(){
	$.ajax({
		type : "GET",
		url : "/admin/commentList",

		success : function(data) {
			$('#Context').html(data);
		},
		error : function() {
			swal('페이지를 불러오지 못했습니다.');

		}
	});
}
</script>
<c:import url="/WEB-INF/views/adminPage/comment/commodify.jsp" />
<c:import url="/WEB-INF/views/adminPage/comment/dapmodify.jsp" />
<c:set value="${countwtComm }" var="comm" />
<c:set value="${countwtDap }" var="dap" />
<h2 class="re_admin_h2">댓글 관리 - 원작 웹툰</h2>
<div>
	<p class="fl re_admin_totalcomm">
		댓글 총 개수:
		<c:out value="${comm }" />
	</p>
</div>
<div class="commentwrap re_admin_commentwrap">
	<div style="width: 100%; float: left; height: 500px; overflow: auto; margin-bottom: 30px;;">
		<table class="table table-striped" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="5%">tNo</th>
					<th scope="col" width="5%">cNo</th>
					<th scope="col" width="10%">작성자</th>
					<th scope="col" width="40%">내용</th>
					<th scope="col" width="20%">작성일</th>
					<th scope="col" width="20%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${allwtcomment }">
					<tr>
						<td scope="row">${item.getWtno() }</td>

						<td scope="row" class="cno_${item.getCno() }">${item.getCno() }</td>

						<td scope="row"	onclick="load(${item.getCno()})"
							class="nickname_${item.getCno() }">${item.getNickname() }</td>

						<td scope="row"	onclick="load(${item.getCno()})"
							class="comment_${item.getCno() }">${item.getComment() }</td>

						<td scope="row"onclick="load(${item.getCno()})">
						<fmt:formatDate value="${item.getCdate() }" pattern="yyyy.MM.dd hh:mm"/></td>

						<td scope="row"><button class="choice3"
								onclick="commDelete(${item.getCno()})">삭제</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	<p class="fl re_admin_totalcomm_1">
		답글 총 개수:
		<c:out value="${dap }" />
	</p>
	<!-- 답글쪽 -->
	<div style="width: 100%; float: right; height: 500px; overflow: auto; margin-bottom: 50px;">
		<table class="table table-striped" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="5%">cNO</th>
					<th scope="col" width="5%">rNO</th>
					<th scope="col" width="10%">작성자</th>
					<th scope="col" width="40%">내용</th>
					<th scope="col" width="20%">작성일</th>
					<th scope="col" width="20%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemdap" items="${allwtdap }">
					<tr>
						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false">${itemdap.getCno() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getWdno()})"
							class="dno_${itemdap.getWdno() }">${itemdap.getWdno() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getWdno()})"
							class="dwriter_${itemdap.getWdno() }">${itemdap.getWtdwriter() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							data-backdrop="static" data-keyboard="false"
							onclick="loaddap(${itemdap.getWdno()})"
							class="content_${itemdap.getWdno() }">${itemdap.getWtcontent() }</td>

						<td scope="row" data-toggle="modal" data-target="#dapmodal"
							onclick="loaddap(${itemdap.getWdno()})" data-backdrop="static"
							data-keyboard="false"><fmt:formatDate
								value="${itemdap.getWtddate() }" type="date"
								pattern="yyyy.MM.dd hh:mm" /></td>
						<td scope="row"><button class="choice3"
								onclick="dapDelete(${itemdap.getWdno()})">삭제</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>