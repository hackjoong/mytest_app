<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<title>Tonivie_audio</title>

<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon_v2.1/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon_v2.1/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon_v2.1/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon_v2.1/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon_v2.1/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon_v2.1/favicon-72x72.png">

<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/loading.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link href="/resources/common/css/uploadtoon.css" rel="stylesheet">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/jquery-ui.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.2.3/wavesurfer.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.2.3/plugin/wavesurfer.regions.min.js"></script>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/en/loading_en.js"></script>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />

<script src="/resources/common/js/audionvideo.js"></script>
<!-- add by ydh for progress bar 180316 -->
<script src="/resources/common/js/progress.js"></script>
<link rel="stylesheet" href="/resources/common/css/progress.css">
<script>
function focus_eve1(param){
	$(".audio-time-start, .audio-time-end").css("color","#7b7c7c");
	$(".audio-time-start").removeClass("focusjm");
	$(param).find(".audio-time-start, .audio-time-end").css("color","#20beca");
	$(param).find(".audio-time-start").addClass("focusjm");
}
function focus_eve2(param){
	$(".audio-start-input, .audio-end-input").css("color","#7b7c7c");
	$(".audio-start-input").removeClass("focusdb");
	$(param).find(".audio-start-input, .audio-end-input").css("color","#20beca");
	$(param).find(".audio-start-input").addClass("focusdb");
}
$(function(){
	$(".jamak-item-txt").click(function(){
		
	})
	$(document).keydown(function(e){
		var start = $(".start-time").text();
		if($("#txt-title").hasClass("active-tab") === true){
			if(e.ctrlKey && e.which == 188){
				$(".focusjm").val(start);
			}
			else if(e.ctrlKey && e.which == 190){
				$(".focusjm").parent().next().next().find(".audio-time-end").val(start);
			}
		}
		else if($("#audio-title").hasClass("active-tab") === true){
			if(e.altKey && e.which == 188){
				$(".focusdb").val(start);
			}
			else if(e.altKey && e.which == 190){
				$(".focusdb").parent().next().next().find(".audio-end-input").val(start);
				//	$(".audio-item-txt:last-child").find(".audio-end-input").val(start);
			}
		}
		
	})
})

</script>
<style>
.switch {
  margin-top:2px;
  position: absolute;
  display: inline-block;
  width: 90px;
  height: 34px;
  margin: 0;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
  width:36px;
  height:15px;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 1px;
  bottom: 1px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

/* input:checked + .slider {
  background-color: #ff6700;
} */

input:focus + .slider {
  box-shadow: 0 0 1px #ff6700;
}

input:checked + .slider:before {
  -webkit-transform: translateX(21px);
  -ms-transform: translateX(21px);
  transform: translateX(21px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
  width:13px;
  height:13px;
}
</style>
</head>
<body style="width:1903px; height: 914px; overflow-y: hidden">
<div class="popup-blackbg lyric-popup-bg">  
</div>
<div class="heedo-popup lyric-popup p40 effect_modal_small">
  <div style="margin-top: 120px; text-align: center; color:#5d5d5d;">Choose the form to download.</div>
  <div class="fl" style="width:430px; margin-top: 25px;">
    <div class="fl p15 white cp tc mr10 downS" style="background-color:#20beca; color:#fff; width: 48%;">
      <span class='f3'>Subtitle File Download</span><br>
      <span class='f1'>smi File Download</span>
    </div>
    <div class="fl p15 white cp tc downV" style="background-color:#20beca; color:#fff; width: 48%; margin-right: 1%;">
      <span class='f3'>Video File Downloa</span><br>
      <span class='f1'>mp4 File Download</span>
    </div>
  </div>
</div>
<div class="popup-blackbg translate-popup-bg">  
</div>
<div class="heedo-popup translate-popup p20 effect_modal_long">
  <div style="margin-top: 150px; text-align: center; color:#5d5d5d;">Select the desired language.</div>
  <div class="fl" style=" margin-left: 25px; margin-top: 20px;">
  	
    <div class="fl p10 cp tc ml5 playbtn" style="background-color:#20beca; width:150px; color:#fff;" data-lang="ko">
      <span class='f3 language_font'>Korean 자막</span><br>
      <span class='f1 language_font'>재생</span>
    </div>
    <div class="fl p10 cp tc ml10 playbtn" style="background-color:#20beca; width:150px; color:#fff;" data-lang="en">
      <span class='f3 language_font'>English subtitles</span><br>
      <span class='f1 language_font'>Play</span>
    </div>
    <div class="fl p10 cp tc ml10 playbtn" style="background-color:#20beca; width:150px; color:#fff;" data-lang="ch">
      <span class='f3 language_font'>中文字幕</span><br>
      <span class='f1 language_font'>再生</span>
    </div>
    <div class="fl p10 cp tc ml10 playbtn" style="background-color:#20beca; width:150px; color:#fff;" data-lang="jp">
      <span class='f3 language_font'>日本語字幕</span><br>
      <span class='f1 language_font'>再生</span>
    </div>
  </div>
</div>
<div class="popup-blackbg new-popup-bg">  
</div>
<div class="heedo-popup new-popup p20 effect_modal_small">
  <div class="fl" style="margin-top: 135px; margin-left: 20px; margin-right: 20px;">
    <div class="fl p20 white cp tc mr10 new_project" style="background-color:#20beca; width: 180px;">
      <span class='f5'>New Project</span><br>
      <span class='f1'>Dubbing and subtitlin<br> delete new start.</span>
    </div>
    <div class="fl p20 white cp tc ml10 change_project" style="background-color:#20beca; width: 180px;">
      <span class='f5'>Video changes</span><br>
      <span class='f1'>Dubbing and subtitling<br> is maintained.</span>
    </div>
  </div>
</div>
<div class="w100 fl header-bar">
    <div class="fl logo-zone">
        <div class="ab position50">
            <Img src="/resources/image/v2.0/TOONIVIE_logo.png" class="upload-logo cp" onclick="location.href='/'">
        </div>
    </div>
    <div class="tc brl h100 header-cont fl position50">
        <div class="header_language" style="float: right;"> 
			<ul>
				<li><a href="/audionsubtitle">KOR</a></li>
				<li class="language_on"><a href="/audionsubtitle_en">ENG</a></li>
			</ul>
		</div>
    </div>
    <div class="fr mt20">
        <ul>
        	 <li class="collabor"><a href="/cv/effect">Cooperation Tool</a></li>
            <li class="collabor"><a href="/cv/effect_en" target="_blank">Video Editing Toll</a></li>
        </ul>
    </div>
</div>
<div class="w100 fl">
    <div class="w50 fl leftvideo focused"  style="background: #f5f5f5; height: 1000px;">
        <div class="video-frame">
            <div class="none-video">
                <form id="videoForm"  action="/uploadVideo" enctype="multipart/form-data">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                    <input type="file" class="video-input" name="video" id="videoInput">
                </form>
                <img class="position_img" src="/resources/image/v2.0/audionsubtitle/uploadVideo.png" alt="웹툰 무비 업로드 버튼" title="웹툰 무비 업로드 버튼" />
                <span class="position50 position_img2" style="color:#5d5d5d;">Click to add video.</span>
            </div>
            <div class="video-box">
                <video class="video" id="vid">
                    <source id="videoSrc" src="" type="video/mp4">
                </video>
            </div>
        </div>
        <div class="video-segment">
            <div class="video-control" id="scrubber">
                <div id="progress"></div>
            </div>
            <div class="fl" style="color:#7b7c7c; margin-left: 5%; width: 94%; margin-left: 3%; margin-top: 5px;">
                <span class="start-time fl">00:00</span>
                <span class="end-time fr">00:00</span>
            </div>
        </div>
        <div class="video-ctr" style="padding-top: 0;">
            <div class="clear" style="position:relative; width: 90%;">
            <label class="switch" style="margin-left: 0; margin-top: 15px;">
              <span style="color:#7b7c7c; font-weight: 400;">Subtitle</span>
        	  <input type="checkbox" class="jamakcheck" checked>
        	  <span class="slider round" style="margin-left:60px;"></span>
      		</label>
                <ul class="ctr-ul">
                    <li>
                        <img src="/resources/image/v2.0/audionsubtitle/play.png" style="height: 45px" id="play"/>
                    </li>
                    <li>
                        <img src="/resources/image/v2.0/audionsubtitle/pause.png" style="height: 45px" id="pause"/>
                    </li>
                    <li>
                        <img src="/resources/image/v2.0/audionsubtitle/stop.png" style="height: 45px" id="stop"/>
                    </li>
                </ul>
                <img src="/resources/image/v2.0/audionsubtitle/upload.png" style="height:45px; cursor:pointer; position:absolute; top:0;right:0;" id="upload_movie" title="upload_movie"/>
            </div>
        </div>
        <div class="video-timeline">
            <div class="timeline-title">
                <span>Timeline</span>
            </div>
            <div class="timeline-header">
                <div class="tab-header text-tab-header active-tab2" data-tab2="txt-timeline" >              
                Subtitle
                </div>
                <div class="tab-header audio-tab-header" data-tab2="dubing-timeline">
                  Dubbing
                </div>
            </div>
            <div class="timeline-content txt-timeline mt10">
                <div class="timeline">
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                    <div class="segment"></div>
                </div>
                <div class="timeline-timetxt mb15">
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                    <span class="times">00:00</span>
                </div>
            </div>
            <div class="timeline-content dubing-timeline mt10">
                <div class="timeline2">
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                    <div class="segment2"></div>
                </div>
                <div class="timeline-timetxt mb15">
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                    <span class="times2">00:00</span>
                </div>
            </div>
        </div>
        <div class="w90 fl tc f3 pv10 cp as_submit_subtext" onclick="submit_subtext()"  id="save">
            <img src="/resources/image/v2.0/TOONIVIE_make_logo.png" style="height: 20px; margin-right: 5px;"/>Saving subtitles and dubbing
        </div>
    </div>
    <div class="fr" style="width: 50%; background: #f5f5f5;">
        <div class="w100 fl">
            <div class="audio-text">
                <div class="audio-txt-title">
                    <div class="audio-txt-title-tab txt-title active-tab" id="txt-title" data-tab1="txt-content" data-tab3="append-btn1">
                        <span>Subtitle</span>
                    </div>
                    <div class="audio-txt-title-tab audio-title" id="audio-title" data-tab1="audio-content" data-tab3="append-btn2">
                        <span>Dubbing</span>
                    </div>
                </div>
                <div class="aut-contents txt-content">
                    <div class="audio-txt-item jamak-item-txt" onclick="focus_eve1(this)">
                        <div class="audio-time-txt">
                            <div class="fl">
                              <div class="audio-time-frame">
                                  <input type="text" value="00:00" class="audio-time-input audio-time-start" onfocus="video_focus_event()" onblur="video_blur_event()">
                              </div>
                              <div class="fl mr5 pt5" style="color: #7b7c7c;">~</div>
                              <div class="audio-time-frame">
                                  <input type="text" value="00:00" class="audio-time-input audio-time-end" onfocus="video_focus_event()" onblur="video_blur_event()">
                              </div>
                            </div>
                            <div class="fr">
                              <ul class="translate">
                                <li class="translate-active language_font" data-lang="ko">Korean</li>
                                <li class="language_font" data-lang="en">English</li>
                                <li class="language_font" data-lang="ch">中國語</li>
                                <li class="language_font" data-lang="jp">日本語 </li>
                              </ul>
                            </div>
                        </div>
                        <div class="audio-content-txt">
                            <textarea class="audio-txt audio-txt-ko" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Enter the subtitles."></textarea>
                            <textarea class="audio-txt audio-txt-en dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>
                            <textarea class="audio-txt audio-txt-ch dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>
                            <textarea class="audio-txt audio-txt-jp dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>
                        </div>
                        <div class="audio-footer">
                            <button class="cont-save-btn txt-cont-save-ko txt-cont-save" data-index="0" data-lang="ko">Save</button>
                            <button class="cont-save-btn txt-cont-save-en dn txt-cont-save" data-index="0" data-lang="en">Save</button>
                            <button class="cont-save-btn txt-cont-save-ch dn txt-cont-save" data-index="0" data-lang="ch">Save</button>
                            <button class="cont-save-btn txt-cont-save-jp dn txt-cont-save" data-index="0" data-lang="jp">Save</button>                            
                            <button class="cont-save-btn txt-cont-modify-ko dn dni txt-cont-modify" data-index="0" data-lang="ko">Modify</button>
                            <button class="cont-save-btn txt-cont-modify-en dn dni txt-cont-modify" data-index="0" data-lang="en">Modify</button>
                            <button class="cont-save-btn txt-cont-modify-ch dn dni txt-cont-modify" data-index="0" data-lang="ch">Modify</button>
                            <button class="cont-save-btn txt-cont-modify-jp dn dni txt-cont-modify" data-index="0" data-lang="jp">Modify</button>
                        </div>
                    </div>
                </div>
                <div class="aut-contents audio-content dn">
                    <div class="audio-txt-item audio-item audio-item-txt" data-container="waveform0" onclick="focus_eve2(this)">
                        <div class="audio-time-txt">
                            <div class="audio-time-frame">
                                <input type="text" value="00:00" class="audio-time-input audio-start-input" onfocus="video_focus_event()" onblur="video_blur_event()">
                            </div>
                            <div class="fl mr5 pt5" style="color: #7b7c7c;">~</div>
                            <div class="audio-time-frame">
                                <input type="text" value="00:00" class="audio-time-input audio-end-input" onfocus="video_focus_event()" onblur="video_blur_event()">
                            </div>                            
                            <div class="fl ml10">
                              <button class="btn-ivory region-btn" type="button" style="color:#fff; padding:6px; font-size:14px; background-color: #20beca;" data-index="0">Application time</button>
                            </div>
                            <div class="fl ml10 white" style="margin-top:7px;">
                              <span class="fl" style="color:#7B7C7C;">Audio Timeline:<span class="audio-region-activeTime"></span></span>                              
                            </div>
                            <div class="fr">
                                <%--<button class="btn saved" type="button" data-index2="0" onclick="fileUploadAudio($(this))">save</button>--%>
                                <form class="audioForm"  action="/uploadAudio" enctype="multipart/form-data">
                                    <input type="file" name="audio" class="audio-input dn" data-index2="0" onChange="fileUploadAudio($(this))" data-id="waveform0" style="display:none;"  data-init="false">
                                    <button class="load-btn btn-ivory" type="button" style="background-color: #20beca; color:#fff;">Open File</button>
                                </form>
                            </div>
                        </div>
                        <div class="audio-content-txt">
                            <div class="audio-wave well" id="waveform0" style="margin-bottom:0; padding: 0; background: #fff; border:0;">

                            </div>
                            <div class="audio-wave-info">
                                <div class="time-cheker mt5">
                                    <span class="fl audio-startTime">00:00</span>
                                    <span class="fr audio-endTime">00:00</span>
                                </div>
                            </div>                            
                        </div>
                        <div class="audio-footer">                                                  
                          <button type="button" class="cont-save-btn wave-play mr10 fl" data-index2='0'>Play</button>                              
                            <button type="button" class="cont-save-btn audio-cont-save" data-index='0'>Save</button>   
                            <button type="button" class="cont-save-btn audio-cont-modify dn" data-index='0'>Modify</button>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="w30 fl tc btns-zone" style="background-color: #20beca; margin-left: 35%; border-radius:5px; margin-top: 22px; margin-bottom: 15px;">
                <button type="button" class="append-btn1 apdbtn"><img class="as_append_btn" src="/resources/image/v2.0/audionsubtitle/append_btn.png" >Add subtitles</button>
                <button type="button" class="append-btn2 dn apdbtn"><img class="as_append_btn" src="/resources/image/v2.0/audionsubtitle/append_btn.png">Add Dubbing</button>
            </div>
        </div>
    </div>
</div>
<script>
	$('.audio-cont-save').click(function(){
	})
	$(document).on("click",".translate>li",function(){
		var data = $(this).data("lang");
		$(this).addClass("translate-active").siblings().removeClass("translate-active");
		$(this).parents(".audio-txt-item").find(".audio-txt-"+data+"").removeClass("dn").siblings().addClass("dn");
		$(this).parents(".audio-txt-item").find(".txt-cont-save-"+data+"").removeClass("dn").siblings(".txt-cont-save").addClass("dn");
		$(this).parents(".audio-txt-item").find(".txt-cont-modify-"+data+"").removeClass("dn").siblings(".txt-cont-modify").addClass("dn");
	});
    function downloadURI(uri, name) {
	  var link = document.createElement("a");
	  link.download = name;
	  link.href = uri;
	  document.body.appendChild(link);
	  link.click();
	  document.body.removeChild(link);
	  delete link;
	}
    var i = 0;
    var a = 0;
    var b = 0;
    var x = 0;
    var y = 0;
    var z = 0;
    
    $(".lyric-popup-bg").click(function(){
    	$(".lyric-popup-bg").hide();
    	$(".lyric-popup").hide();
    })
    var videoObj = new Object();
    /* $("#save").click(function(){
    	loadingOn();
    }) */
    function submit_subtext(){
    	loadingOn();
    	var $div = "<div id='chargeBar3' style='clear:both;'></div>";
    	$(".loading-element>.w100").append($div);
    	setTimeout(function(){
    		//프로그래스바 랜덤
    		var chargeBar3 = new Progress.bar({ id: "progress6",  autoRemove: false, backgroundSpeed: 5, type: "charge", showPercentage: true });
            chargeBar3.renderTo(document.getElementById('chargeBar3'));
            
            var percent6 = 0;
            window.setInterval(function() {
                percent6 = percent6 + Math.floor(Math.random()*10);
                percent6 = percent6 >= 80 ? submitSubtext(chargeBar3) : percent6;
                chargeBar3.update(percent6);
            }, 500);
            
    		//submitSubtext();
    	},800);
    }
    function submitSubtext(chargeBar3){
	   //loadingOn();
        var jsonData = {};
        var data = {};
        jsonData["Video"]=videoObj;
        jsonData["TextList_ko"]=JsonArray_ko;
        jsonData["TextList_en"]=JsonArray_en;
        jsonData["TextList_ch"]=JsonArray_ch;
        jsonData["TextList_jp"]=JsonArray_jp;
        jsonData["AudioList"]=audioArray;
        console.log(jsonData);
        data["data"]=JSON.stringify(jsonData);        
        $.ajax({
            url:"/applyAS/applyTextAndAudio",
            method:"POST",
            data:data,
            dataType : "json",
            timeout:300000,
            async:false,
            success:function(result){
            	
            	$("#progress6").remove();
            	//var $div = "<div id='chargeBar3' style='clear:both;'></div>";
            	//$(".loading-element>.w100").append($div);
            	var chargeBar3 = new Progress.bar({ id: "progress6", autoRemove: false, backgroundSpeed: 5, type: "charge", showPercentage: true });
             	$('#progress6>span').text("80%");
             	
                chargeBar3.renderTo(document.getElementById('chargeBar3'));
                
            	var percent6 = 80;
            	$("#innerDiv").css("width","80%");
            	window.setInterval(function() {
                    percent6 = percent6 + Math.floor(Math.random()*10);
                    percent6 = percent6 >= 100 ? 100 : percent6;
                    chargeBar3.update(percent6);
                }, 500); 
            	
            	setTimeout(function () {
            		
	                $(".downV").attr("data-url",result.videoUrl);
	                if(result.isSmiExist==true){
	                	$(".downS").attr("data-url",result.smiUrl);
	                }else{
	                	$(".downS").addClass("dis");
	                }
					$(".lyric-popup-bg").show();
			    	$(".lyric-popup").show();
			    	loadingOff();
			    	$('#chargeBar3').remove();
            	},3000);
            },
            error : function(request, status, error) {
            	loadingOff();
                alert("code:" + request.status + "\n" + "error:" + error);                
            }
        })
    }
    $(".downV").click(function(){
        downloadURI($(this).data("url"),"toonivie.mp4");
    });
    $(".downS").click(function(){
        downloadURI($(this).data("url"),"toonivie.smi");
    });
    $(function () {

        $(".segment").each(function () {
            i++;
            var width = $(".timeline").outerWidth();
            var calc = width / 10
            $(this).css({left: calc * i});
        });
        $(".times").each(function () {
            a++;
            var width = $(".timeline").outerWidth();
            var calc = width / 10
            $(this).css({left: calc * a - 15});
        });
        $(".segment2").each(function () {
            y++;
            var width = $(".timeline2").outerWidth();
            var calc = width / 10
            $(this).css({left: calc * y});
        });
        $(".times2").each(function () {
            z++;
            var width = $(".timeline2").outerWidth();
            var calc = width / 10
            $(this).css({left: calc * z - 15});
        });
        $(".video-input").change(function () {
            if ($(this).val() == "") {
                alert('Please add video!')
            } else {
                var video1 = $("#vid").get(0);
                $(".none-video").hide();
                $(".video-box").show();
                /*$(".start-time").text("00:00");
                $(".end-time").text(humanReadable(video1.duration));
                console.log(video1.duration);
                $(".times").each(function () {
                    x++
                    var du = video1.duration / 10 * x
                    $(this).text(humanReadable(du));
                })
                $(".times2").each(function () {
                    b++
                    var du = video1.duration / 10 * b
                    $(this).text(humanReadable(du));
                })*/
            }
        });
    })

    //초->분 변환
    function humanReadable(seconds) {
        seconds = parseInt(seconds);
        var pad = function (ex) {
            return (ex < 10) ? "0" + ex : ex;
        }
        return pad(parseInt(seconds / 60 % 60)) + ":" +
            pad(seconds % 60)
    }
    //분->초 변환
    function mintosec(min){
        min = ''+min;
        min = min.replace(/\:/g,'');
       
        var m = min.substr(0,2);
        var s = min.substr(2,4);
        m = parseInt(m)*60;
        s = parseInt(s)*1;
        return m+s;
    }
    function mintosec_cut(min){
        min = ''+min;
        min = min.replace(/\:/g,'');
       
        var m = min.substr(0,1);
        var s = min.substr(1,3);
        m = parseInt(m)*60;
        s = parseInt(s)*1;
        return m+s;
    }
    //lefttab
     $(".tab-header").click(function (e) { //왼쪽 버튼 클릭시
        var $data = $(this).attr('data-tab2');
        $(this).addClass('active-tab2').siblings().removeClass('active-tab2');
        $(".timeline-content").addClass('dn');
        $("." + $data + "").removeClass('dn');
        if($data=="txt-timeline"){
        	var data1 = "txt-content";
        	var data2 = "append-btn1";
        	LeftFromRight(data1,data2);
       	} else if($data == "dubing-timeline"){
        	var data1 = "audio-content";
        	var data2 = "append-btn2";
        	LeftFromRight(data1,data2);
        }
    });
     //왼쪽 버튼 클릭시 오른쪽 탭도 동기화 되도록 하는 함수
	function LeftFromRight(data1,data2){
		$('.audio-txt-title-tab[data-tab1='+data1+']').addClass('active-tab').siblings().removeClass('active-tab');
		$(".aut-contents").addClass('dn');
		$(".apdbtn").addClass('dn');
		$("." + data1 + "").removeClass('dn');
		$("." + data2 + "").removeClass('dn');
	}

    //righttab
    $(".audio-txt-title-tab").click(function (e) { //오른쪽 탭 클릭시
        var $data = $(this).attr('data-tab1');
        var $data2 = $(this).attr('data-tab3');
        $(this).addClass('active-tab').siblings().removeClass('active-tab');
        $(".aut-contents").addClass('dn');
        $(".apdbtn").addClass('dn');
        $("." + $data + "").removeClass('dn');
        $("." + $data2 + "").removeClass('dn');
        if($data == "txt-content"){
        	var data1 = "txt-timeline";
        	RightFromLeft(data1);
        }else if($data == "audio-content"){
        	var data1 = "dubing-timeline";
        	RightFromLeft(data1);
        }
    });
    //탭 클릭시 왼쪽도 동기화 되도록 하는 함수
    function RightFromLeft(data1){
    	$(".tab-header[data-tab2="+data1+"]").addClass("active-tab2").siblings().removeClass("active-tab2");
    	$(".timeline-content").addClass('dn');
        $("." + data1 + "").removeClass('dn');
    }
   
    $(function(){
        $(".dubing-timeline").addClass("dn")
    })
// video progress
    $(document).ready(function () {
        var $video = $("#vid");
        var $scrubber = $("#scrubber");
        var $progress = $("#progress");

        $video.bind("timeupdate", videoTimeUpdateHandler);
        $scrubber.bind("mousedown", scrubberMouseDownHandler);

        function videoTimeUpdateHandler(e) {
            var video = $video.get(0);
            var percent = video.currentTime / video.duration;
            updateProgressWidth(percent);
        }

        function scrubberMouseDownHandler(e) {
            var $this = $(this);
            var x = e.pageX - $this.offset().left;
            var percent = x / $this.width();
            updateProgressWidth(percent);
            updateVideoTime(percent);
        }

        function updateProgressWidth(percent) {
            $progress.width((percent * 100) + "%");
        }

        function updateVideoTime(percent) {
            var video = $video.get(0);
            video.currentTime = percent * video.duration;
        }
        
    });
    //업로드 아이콘 클릭 이벤트들
    $(".new-popup-bg").click(function(){
    	$(".new-popup").hide();
    	$(".new-popup-bg").hide();
    });
    $("#upload_movie").click(function(){
    	if($("#videoSrc").attr("src") == null || $("#videoSrc").attr("src") =='' ){
    		$("#videoInput").click();
    	}
    	else{
    		$(".new-popup").show();
            $(".new-popup-bg").show();
    	}
    });
    $(".new_project").click(function(){
    	if(confirm("새 프로젝트를 할 경우 기존 프로젝트는 사라지게 됩니다. 그래도 하시겠습니까?")){
    		window.location.reload();
    	}
    });
    $(".change_project").click(function(){
    	alert("기존 영상은 유지됩니다.");
    	$(".new-popup").hide();
    	$(".new-popup-bg").hide();
    	$("#videoInput").click();
    });
  	//업로드 아이콘 클릭 이벤트들 end
    $(".translate-popup-bg").click(function(){
    	$(".translate-popup").hide();
    	$(".translate-popup-bg").hide();
    })
    $("#play").click(function(){
      if($("#vid")[0].duration > 0){
        if($('.jamakcheck').prop("checked")==true){
            $(".translate-popup").show();
            $(".translate-popup-bg").show();
          } else{
            $('.lyric-ko,.lyric-en,.lyric-ch,.lyric-jp').remove();
            playvid($("#vid")[0]);
          }
      }
      else{
        alert("No video or replay time is 0.");  
      }
      
      
    })
    //video controler
    $(".playbtn").click(function(){
    	playVideo($(this).data("lang"))
    })
    
    var video = $("#vid")[0];    
    function playVideo(data){
    	$(".translate-popup").hide();
    	$(".translate-popup-bg").hide();
        playvid($("#vid")[0]);
        vTime = $("#vid")[0].currentTime;
        dTime = Math.ceil($("#vid")[0].duration);
        var JsonArray = "";
        
        if(data == "ko"){
        	$("#vid").attr("data-lang","ko") 
        	for(var i = 0; i < JsonArray_ko.length ; i++){
                var $width = JsonArray_ko[i].ApplyEnd - JsonArray_ko[i].ApplyStart
                $width = $width / dTime * 100;
                //console.log('현재 :'+vTime+', 데이터 :'+JsonArray_ko[i].ApplyStart+'');
                if($("#lyric-txt"+JsonArray_ko[i].index+"").length == 0){
                	$(".video-box").append(""+
                        "<pre class='lyric dn lyric-ko' id='lyric-txt"+JsonArray_ko[i].index+"-"+data+"' data-begin='"+JsonArray_ko[i].ApplyStart+"' data-end='"+JsonArray_ko[i].ApplyEnd+"'>"+JsonArray_ko[i].Text+"</pre>"+
                    "");	
                }                        
            }
        }else if(data == "en"){
        	$("#vid").attr("data-lang","en")
        	for(var i = 0; i < JsonArray_en.length ; i++){
                var $width = JsonArray_en[i].ApplyEnd - JsonArray_en[i].ApplyStart
                $width = $width / dTime * 100;
                //console.log('현재 :'+vTime+', 데이터 :'+JsonArray_en[i].ApplyStart+'');
                if($("#lyric-txt"+JsonArray_en[i].index+"").length == 0){
                	$(".video-box").append(""+
                		"<pre class='lyric dn lyric-en' id='lyric-txt"+JsonArray_en[i].index+"-"+data+"' data-begin='"+JsonArray_en[i].ApplyStart+"' data-end='"+JsonArray_en[i].ApplyEnd+"'>"+JsonArray_en[i].Text+"</pre>"+
                    "");	
                }                        
            }
        }else if(data == "ch"){
        	$("#vid").attr("data-lang","ch")
        	for(var i = 0; i < JsonArray_ch.length ; i++){
                var $width = JsonArray_ch[i].ApplyEnd - JsonArray_ch[i].ApplyStart
                $width = $width / dTime * 100;
                //console.log('현재 :'+vTime+', 데이터 :'+JsonArray_ch[i].ApplyStart+'');
                if($("#lyric-txt"+JsonArray_ch[i].index+"").length == 0){
                	$(".video-box").append(""+
                        "<pre class='lyric dn lyric-ch' id='lyric-txt"+JsonArray_ch[i].index+"-"+data+"' data-begin='"+JsonArray_ch[i].ApplyStart+"' data-end='"+JsonArray_ch[i].ApplyEnd+"'>"+JsonArray_ch[i].Text+"</pre>"+
                    "");	
                }                        
            }
        }else{
        	$("#vid").attr("data-lang","jp")
        	for(var i = 0; i < JsonArray_jp.length ; i++){
                var $width = JsonArray_jp[i].ApplyEnd - JsonArray_jp[i].ApplyStart
                $width = $width / dTime * 100;
                //console.log('현재 :'+vTime+', 데이터 :'+JsonArray_jp[i].ApplyStart+'');
                if($("#lyric-txt"+JsonArray_jp[i].index+"").length == 0){
                	$(".video-box").append(""+
                        "<pre class='lyric dn lyric-jp' id='lyric-txt"+JsonArray_jp[i].index+"-"+data+"' data-begin='"+JsonArray_jp[i].ApplyStart+"' data-end='"+JsonArray_jp[i].ApplyEnd+"'>"+JsonArray_jp[i].Text+"</pre>"+
                    "");	
                }                        
            }
        }
        
        $(".lyric-time").each(function(){
            var width = $(this).data("width");
            var ApplyStart = $(this).data("begin");
            ApplyStart = ApplyStart / dTime * 100;

            $(this).css({"width":width+"%","left":ApplyStart+"%"});
        });

    }
    
    function playvid(vid){
        vid.play();
    }
    function pausevid(vid) {
        vid.pause();
    }
    video.onloadedmetadata = function() {
        var video1 = $("#vid").get(0);

        $(".end-time").text(humanReadable(video1.duration));
        /*console.log(video1.duration);*/
        $(".times").each(function () {
            x++
            var du = video1.duration / 10 * x
            $(this).text(humanReadable(du));
        })
        $(".times2").each(function () {
            b++
            var du = video1.duration / 10 * b
            $(this).text(humanReadable(du));
        })
    };
    
    //add by ydh 180308  -->  video stop
    $("#stop").click(function(){     
      $("#vid")[0].currentTime = 0;
      $("#vid")[0].pause();
      $(".lyric-time2").each(function(){
          var index = $(this).data("index");
          wavesurfer[index].stop();   
      })
    })
    //시간 업데이트 될때 - 비디오 1초씩 될때임
    video.addEventListener("timeupdate", function () {
    	if(video.currentTime != 0){
    		var vTime = video.currentTime;
            var dTime = video.duration;
            var txt = vTime.toFixed(1);
            var data = $("#vid").attr("data-lang")
            $(".start-time").text(humanReadable(txt));
            $(".lyric").addClass("dn");
            $(".lyric-"+data+"").each(function(){            
                vTime = Math.floor(vTime);
                var start = $(this).data("begin");
                var end = $(this).data("end");
                if(vTime >= start && vTime<end){
                    $(this).removeClass("dn")
                }else if(vTime > end){
                    $(this).addClass("dn");
                }
            });
            	$(".lyric-time2").each(function(){
                    var index = $(this).data("index");
                    var value = $(this).data("begin");
                    var end = $(this).data("end");
                    txt = Math.floor(vTime);
                    dTime = Math.floor(dTime);
                    console.log(txt+","+value);
                    if(txt == value){
                        wavesurfer[index].play();
                        console.log(wavesurfer[index]);
                    }else if(txt == end){ //비디오 재생때 더빙마지막 시간이랑 비디오 재생시간이랑 같을때 멈추는것
                        wavesurfer[index].stop();
                        $(".wave-play").each(function(){
                          $(this).html("Play");
                        })
                    }
                    
                })
    	}
    	else if(video.currentTime == 0){
    		$('.start-time').text("00:00");
    	}
           
              
    }, false);   
    var n = 0;
    $(".append-btn1").click(function(){
    	n++;
        var element = ''+
        '<div class="audio-txt-item jamak-item-txt" onclick="focus_eve1(this)">'+
            '<div class="audio-time-txt">'+
              '<div class="fl">'+
                '<div class="audio-time-frame">'+
                  '<input type="text" value="00:00" class="audio-time-input audio-time-start" onfocus="video_focus_event()" onblur="video_blur_event()">'+
                '</div>'+
                '<div class="fl mr5 pt5">~</div>'+
                '<div class="audio-time-frame">'+
                  '<input type="text" value="00:00" class="audio-time-input audio-time-end" onfocus="video_focus_event()" onblur="video_blur_event()">'+
                '</div>'+
              '</div>'+
              '<div class="fr">'+
                '<ul class="translate">'+
                  '<li class="translate-active language_font" data-lang="ko">Korean</li>'+
                  '<li class="language_font" data-lang="en">English</li>'+
                  '<li class="language_font" data-lang="ch">中國語</li>'+
                  '<li class="language_font" data-lang="jp">日本語 </li>'+
                '</ul>'+
              '</div>'+
            '</div>'+
            '<div class="audio-content-txt">'+
            '<textarea class="audio-txt audio-txt-ko" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Enter the subtitles."></textarea>'+
            '<textarea class="audio-txt audio-txt-en dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>'+
            '<textarea class="audio-txt audio-txt-ch dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>'+
            '<textarea class="audio-txt audio-txt-jp dn" onfocus="video_focus_event()" onblur="video_blur_event()" placeholder="Please enter caption"></textarea>'+
          '</div>'+
          '<div class="audio-footer">'+
              '<button class="cont-save-btn txt-cont-save-ko txt-cont-save" data-index="'+n+'" data-lang="ko">Save</button>'+
              '<button class="cont-save-btn txt-cont-save-en dn txt-cont-save" data-index="'+n+'" data-lang="en">Save</button>'+
              '<button class="cont-save-btn txt-cont-save-ch dn txt-cont-save" data-index="'+n+'" data-lang="ch">Save</button>'+
              '<button class="cont-save-btn txt-cont-save-jp dn txt-cont-save" data-index="'+n+'" data-lang="jp">Save</button>'+                          
              '<button class="cont-save-btn txt-cont-modify-ko dn dni txt-cont-modify" data-index="'+n+'" data-lang="ko">Modify</button>'+
              '<button class="cont-save-btn txt-cont-modify-en dn dni txt-cont-modify" data-index="'+n+'" data-lang="en">Modify</button>'+
              '<button class="cont-save-btn txt-cont-modify-ch dn dni txt-cont-modify" data-index="'+n+'" data-lang="ch">Modify</button>'+
              '<button class="cont-save-btn txt-cont-modify-jp dn dni txt-cont-modify" data-index="'+n+'" data-lang="jp">Modify</button>'+
          '</div>'+            
            '</div>'
        $(".txt-content").append(element);
    });

    var h = 0;
    var idn = 1;
    $(".append-btn2").click(function(){
        h++
        if($(".audio-input").last().val() == ""){
            alert("이 전 작업을 완료해주세요.")
        }else{
            var element = ''+
            '<div class="audio-txt-item audio-item audio-item-txt" data-container="waveform'+h+'" onclick="focus_eve2(this)">'+
                '<div class="audio-time-txt">'+
                '<div class="audio-time-frame">'+
                '<input type="text" value="00:00" class="audio-time-input audio-start-input" onfocus="video_focus_event()" onblur="video_blur_event()">'+
                '</div>'+
                '<div class="fl mr5 pt5">~</div>'+
                '<div class="audio-time-frame">'+
                '<input type="text" value="00:00" class="audio-time-input audio-end-input" onfocus="video_focus_event()" onblur="video_blur_event()">'+
                '</div>'+               
                '<div class="fl ml10">'+
              '<button class="btn-ivory region-btn" style="color:#fff; padding:6px; font-size:14px; background-color: #20beca;" type="button" data-index="'+h+'">Application time</button>'+
              '</div>'+           
              '<div class="fl ml10 white" style="margin-top:7px;">'+
              '<span class="fl" style="color:#7B7C7C;">Audio Timeline:<span class="audio-region-activeTime"></span></span>'+                             
              '</div>'+
                '<div class="fr">' +
                '<form action="/uploadAudio" enctype="multipart/form-data" class="audioForm">' +
                '<input type="file" name="audio" class="audio-input dn" data-index2='+h+' onChange="fileUploadAudio($(this))" data-id="waveform'+h+'" data-init="false">'+
                '<button class="load-btn btn-ivory" type="button" style="background-color: #20beca; color:#fff;">Open File</button>' +
                '</form>' +
                '</div>'+
                '</div>'+
                '<div class="audio-content-txt">'+
                '<div class="audio-wave well" id="waveform'+h+'" style="margin-bottom:0; padding: 0; background: #fff; border:0;">'+
                '</div>'+
                '<div class="audio-wave-info">'+
                '<div class="time-cheker mt5">'+
                '<span class="fl audio-startTime">00:00</span>'+
                '<span class="fr audio-endTime">00:00</span>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div class="audio-footer">'+
                '<button type="button" data-index2="'+h+'" class="cont-save-btn fl wave-play">Play</button>'+
                '<button type="button" class="cont-save-btn audio-cont-save" data-index="'+h+'">Save</button>'+
                '<button type="button" class="cont-save-btn audio-cont-modify dn" data-index="'+h+'">Modify</button>'
                '</div>'
            '</div>'

            $(".audio-content").append(element);
        }

    });
    $("#videoInput").change(function(){    	
    	loadingOn();
    	setTimeout(function(){
    		$("#videoForm").ajaxForm({
                url:"/uploadVideo",
                method:"POST",
                enctype:"multipart/form-data",
                async:false,
                timeout:60000,
                success:function(result){
                    setTimeout(function () {                	
    	                videoObj.VideoUrl='${rootPath}'+result.resultUrl;
    			        videoObj.Width=result.width;
    			        videoObj.Height=result.height;
    			        videoObj.Fps=result.fps;
    	                $("#videoSrc").attr("src",result.resultUrl);
    	                $("#vid")[0].load();
    	                loadingOff();
                    },3000);
                },
                error : function(request, status, error) {
                    alert("code:" + request.status + "\n" + "error:" + error);
                    loadingOff()
                }
            });
            $("#videoForm").submit();
    	},1); 
		      
    });
    
    var wavesurfer = new Array(10) ;
    function exeRender(func,resultUrl,waveForm,index,$this) {
        surferArray.push(func(resultUrl,waveForm,index,$this));
    }
    var wavesurferIndex=0;
    function fileUploadAudio(e){
        var $this = $(e);        
        $this.parents("form").ajaxForm({
            url:"/uploadAudio",
            method:"POST",
            enctype:"multipart/form-data",
            async:false,
            success:function(result){
                /*console.log(result);*/
                var $data_ele = $($this);
                var $data = $data_ele.data("index2");
                var init = $this.attr("data-init");
//                alert(init);
                if(init == "true"){
                    changeEx($data,result.resultUrl);
                    $this.attr("data-url",result.resultUrl);                                      
                }else{
                    $this.attr("data-init","true");
                    exeRender(renderWaveForm,result.resultUrl,$data,wavesurferIndex,$this);
                    $this.attr("data-url",result.resultUrl);                    
                }
                wavesurferIndex++;
//                renderWaveForm(result.resultUrl,"waveform"+$data+"");				
            },
            error : function(request, status, error) {
                alert("code:" + request.status + "\n" + "error:" + error);
            }
        });
        $this.parents("form").submit();
    }
    var containera = new Array(10);    
    function renderWaveForm(url, parentSelector,index,$this) {
        //alert(parentSelector);
        containera[index] = document.getElementById("waveform"+parentSelector);
        //console.log("index: "+index);     	
        wavesurfer[index] = WaveSurfer.create({
            container: containera[index],
            scrollParent : true
        });        
        wavesurfer[index].on('audioprocess', function () {
            var startTime = Math.floor(wavesurfer[index].getCurrentTime());
            $(wavesurfer[index].container).parents(".audio-item").find(".audio-startTime").text(humanReadable(startTime))
        });
        $("#pause").click(function(){
        	$(".lyric-time2").each(function(){
        		var index = $(this).data("index");
        		wavesurfer[index].pause();
        	})
        	
        	$("#vid")[0].pause();
        })
        //플레이 2개 주석 처리함 - 유덕희
        /* $("#play").click(function(){
          //$("#vid")[0].play(); //This is error(autoplay no!!!!!!!!!!!!!)
          $(".lyric-time2").each(function(){
            var vTime = video.currentTime;
            var index = $(this).data("index");
                var value = $(this).data("begin");
                var end = $(this).data("end");
            vTime = Math.floor(vTime);
            if(value <= vTime && end > vTime){
              wavesurfer[index].play();
            }           
          })
        }); */
        wavesurfer[index].load(url);
        wavesurfer[index].on('ready', function () {
            var endTime = Math.floor(wavesurfer[index].getDuration());
            /*console.log(wavesurfer[index]);*/
            $(wavesurfer[index].container).parents(".audio-item").find(".audio-endTime").text(humanReadable(endTime))
            // Enable creating regions by dragging            
        });
        wavesurfer[index].on('region-created',function(){
            wavesurfer[index].clearRegions();            
        });
        $(document).on("click",".audio-item",function(){
            wavesurfer.container = $(this).data("container")
        });
        wavesurfer[index].on('region-update-end',function(){
        	var title = $(wavesurfer[index].container).find(".wavesurfer-region").attr("title");        	
        	$(wavesurfer[index].container).parents(".audio-item").find(".audio-region-activeTime").text(title);
        	
        })
//      $(document).on("click",".audio-cont-save",function(){
//          var container = $(this).parents(".audio-item").data("continaer")
//          $("#"+container+"").destroy();
//      });
        $(document).on("click",".region-btn",function(){          
          var startTime = $(this).parents(".audio-txt-item").find(".audio-start-input").val();
          var endTime = $(this).parents(".audio-txt-item").find(".audio-end-input").val();
          var indexx = $(this).data("index");
          startTime = mintosec(startTime);
          endTime = mintosec(endTime);   
          endTime = endTime - startTime;
          addRegion(0,endTime,indexx);
          var title = $(wavesurfer[index].container).find(".wavesurfer-region").attr("title");          
          $(wavesurfer[index].container).parents(".audio-item").find(".audio-region-activeTime").text(title);
        });
        function addRegion(start,end,indexx){
          //드래그 활성화
          /* wavesurfer[indexx].enableDragSelection({
            
          }); */
          wavesurfer[indexx].addRegion({
            start:start,
            end:end,
            loop:true,            
            resize:false,
            color: 'rgba(255, 103, 0, 0.2)'
          })
        }
        wavesurfer[index].on('pause', function () {
        	$(wavesurfer[index].container).parents(".audio-item").find(".wave-play").text("Play");
        });
        wavesurfer[index].on('play', function () {
        	$(wavesurfer[index].container).parents(".audio-item").find(".wave-play").text("정지");
        });
        return wavesurfer[index];
    };
    $(document).on('click','.wave-play',function(){
        var $data = $(this).data("index2");       
       	wavesurfer[$data].playPause();   
    });
    function changeEx(index,url){
//        alert("aa");
        wavesurfer[index].load(url);
        return false;
    }
    
    
    
    var surferArray = new Array();
    // make Json array
    var JsonArray_ko = new Array();
    var JsonArray_en = new Array();
    var JsonArray_ch = new Array();
    var JsonArray_jp = new Array();
    var g = 0;
// 자막 저장버튼 클릭
	function saveLyric(element){
		if($(".video-input").val() == ""){
            alert("Please upload videos.")
            return false
        }else{
        	var $root = element.parents(".audio-txt-item");
            var ApplyStart = $root.find(".audio-time-start").val();
            var ApplyEnd= $root.find(".audio-time-end").val();            
            var obj = new Object();
            var index = element.data("index");
            var $data = element.attr("data-lang");
            var Text = $root.find(".audio-txt-"+$data+"").val();
            ApplyStart = mintosec(ApplyStart);            
            ApplyEnd = mintosec(ApplyEnd);
            
            if($data == "ko"){
            	for(var i = 0; i < JsonArray_ko.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_ko[i].ApplyStart < ApplyStart && JsonArray_ko[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_ko[i].ApplyStart && JsonArray_ko[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
    					
                    }
                }	
            }else if($data == "en"){
            	for(var i = 0; i < JsonArray_en.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_en[i].ApplyStart < ApplyStart && JsonArray_en[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_en[i].ApplyStart && JsonArray_en[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
    					
                    }
                }
            }else if($data == "ch"){
            	for(var i = 0; i < JsonArray_ch.lchgth ; i++){
                    if(Applychd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_ch[i].ApplyStart < ApplyStart && JsonArray_ch[i].Applychd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_ch[i].ApplyStart && JsonArray_ch[i].ApplyStart < Applychd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
    					
                    }
                }
            }else{
            	for(var i = 0; i < JsonArray_jp.lchgth ; i++){
                    if(Applychd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_jp[i].ApplyStart < ApplyStart && JsonArray_jp[i].Applychd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_jp[i].ApplyStart && JsonArray_jp[i].ApplyStart < Applychd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
    					
                    }
                }
            }			

            
            $root.find(".txt-cont-save-"+$data+"").addClass("dni");
            $root.find(".txt-cont-modify-"+$data+"").removeClass("dn dni");
            
			obj.index = index;
            obj.ApplyStart = ApplyStart;
            obj.ApplyEnd = ApplyEnd;
            obj.Text = Text;           
			
            if($data == "ko"){
	            JsonArray_ko.push(obj);
	            console.log(JsonArray_ko);
            }else if($data == "en"){
            	JsonArray_en.push(obj);
	            console.log(JsonArray_en);
            }else if($data == "ch"){
            	JsonArray_ch.push(obj);
	            console.log(JsonArray_ch);
            }else{
            	JsonArray_jp.push(obj);
	            console.log(JsonArray_jp);
            }
            vTime = $("#vid")[0].currentTime;
            //dTime = Math.floor($("#vid")[0].duration);
            dTime = Math.ceil($("#vid")[0].duration);
            var $width = ApplyEnd - ApplyStart
            $width = $width / dTime * 100;
            $(".timeline").append(""+
            		"<span class='lyric-time' id='lyric"+index+"-"+$data+"' data-begin='"+(ApplyStart)+"' data-end='"+(ApplyEnd)+"' data-width='"+$width+"'></span>"+
                "");
            $(".lyric-time").each(function(){
                var width = $(this).data("width");
                var ApplyStart = $(this).data("begin");

                ApplyStart = ApplyStart / dTime * 100;

                $(this).css({"width":width+"%","left":ApplyStart+"%"});
            });
        
        }
	}
	//자막 수정 함수
	function modifyLyric(element){
		if($(".video-input").val() == ""){
            alert("Please upload videos.")
            return false
        }else{        	        	
            var $root = element.parents(".audio-txt-item");
            var ApplyStart = $root.find(".audio-time-start").val();
            var ApplyEnd= $root.find(".audio-time-end").val();     
            console.log($root+","+$root.find(".audio-time-start").val());
            var obj = new Object();
            var index = element.data('index');
            var $data = element.data("lang");
            var Text = $root.find(".audio-txt-"+$data+"").val();
            var JsonArray = "";
            ApplyStart = mintosec(ApplyStart);            
            ApplyEnd = mintosec(ApplyEnd);
            
            if($data == "ko"){
            	for(var i = 0; i < JsonArray_ko.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_ko[i].ApplyStart < ApplyStart && JsonArray_ko[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_ko[i].ApplyStart && JsonArray_ko[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
                    	JsonArray_ko[index].ApplyStart = ApplyStart;
                    	JsonArray_ko[index].ApplyEnd = ApplyEnd;
                    	JsonArray_ko[index].Text = Text;
                    	$("#lyric"+index+"-"+$data+"").remove();
                    	$("#lyric-txt"+index+"-"+$data+"").remove();
                    	console.log(JsonArray_ko[index]);
                    }
                }   	
            }else if($data == "en"){
            	for(var i = 0; i < JsonArray_en.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_en[i].ApplyStart < ApplyStart && JsonArray_en[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_en[i].ApplyStart && JsonArray_en[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
                    	JsonArray_en[index].ApplyStart = ApplyStart;
                    	JsonArray_en[index].ApplyEnd = ApplyEnd;
                    	JsonArray_en[index].Text = Text;
                    	$("#lyric"+index+"-"+$data+"").remove();
                    	$("#lyric-txt"+index+"-"+$data+"").remove();
                    	console.log(JsonArray_en[index]);
                    }
                }  
            }else if($data == "ch"){
            	for(var i = 0; i < JsonArray_ch.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_ch[i].ApplyStart < ApplyStart && JsonArray_ch[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_ch[i].ApplyStart && JsonArray_ch[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
                    	JsonArray_ch[index].ApplyStart = ApplyStart;
                    	JsonArray_ch[index].ApplyEnd = ApplyEnd;
                    	JsonArray_ch[index].Text = Text;
                    	$("#lyric"+index+"-"+$data+"").remove();
                    	$("#lyric-txt"+index+"-"+$data+"").remove();
                    	console.log(JsonArray_ch[index]);
                    }
                }  
            }else if($data == "jp"){
            	for(var i = 0; i < JsonArray_jp.length ; i++){
                    if(ApplyEnd < ApplyStart){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(JsonArray_jp[i].ApplyStart < ApplyStart && JsonArray_jp[i].ApplyEnd > ApplyStart ){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else if(ApplyStart < JsonArray_jp[i].ApplyStart && JsonArray_jp[i].ApplyStart < ApplyEnd){
                        alert("시간을 다시 설정해주세요.");
                        return false;
                    }else{
                    	JsonArray_jp[index].ApplyStart = ApplyStart;
                    	JsonArray_jp[index].ApplyEnd = ApplyEnd;
                    	JsonArray_jp[index].Text = Text;
                    	$("#lyric"+index+"-"+$data+"").remove();
                    	$("#lyric-txt"+index+"-"+$data+"").remove();
                    	console.log(JsonArray_jp[index]);
                    }
                } 
            }
          
            
            vTime = $("#vid")[0].currentTime;
            //dTime = Math.floor($("#vid")[0].duration);
            dTime = Math.ceil($("#vid")[0].duration);
            var $width = ApplyEnd - ApplyStart
            $width = $width / dTime * 100;
            $(".timeline").append(""+
                "<span class='lyric-time' id='lyric"+index+"-"+$data+"' data-begin='"+(ApplyStart)+"' data-end='"+(ApplyEnd)+"' data-width='"+$width+"'></span>"+
             "");
            $(".lyric-time").each(function(){
                var width = $(this).data("width");
                var ApplyStart = $(this).data("begin");

                ApplyStart = ApplyStart / dTime * 100;

                $(this).css({"width":width+"%","left":ApplyStart+"%"});
            });

        }
	}
	$(document).on("click",".txt-cont-save",function(){
		saveLyric($(this));
	})
	$(document).on("click",".txt-cont-modify",function(){
		modifyLyric($(this));
	})
	
  /*   $(document).on("click",".txt-cont-save-ko",function(){
        if($(".video-input").val() == ""){
            alert("Please upload videos.")
            return false
        }else{
            var $root = $(this).parents(".audio-txt-item");
            var ApplyStart = $root.find(".audio-time-start").val();
            var ApplyEnd= $root.find(".audio-time-end").val();
            var Text = $root.find(".audio-txt").val();
            var obj = new Object();
            var index = "";
            ApplyStart = mintosec(ApplyStart);            
            ApplyEnd = mintosec(ApplyEnd);
			

            for(var i = 0; i < JsonArray.length ; i++){
                if(ApplyEnd < ApplyStart){
                    alert("시간을 다시 설정해주세요.");
                    return false;
                }else if(JsonArray[i].ApplyStart < ApplyStart && JsonArray[i].ApplyEnd > ApplyStart ){
                    alert("시간을 다시 설정해주세요.1");
                    return false;
                }else if(ApplyStart < JsonArray[i].ApplyStart && JsonArray[i].ApplyStart < ApplyEnd){
                    alert("시간을 다시 설정해주세요.2");
                    return false;
                }else{
					
                }
            }
            $(this).addClass("dn");
            $root.find(".txt-cont-modify").removeClass("dn");
            index = g++;
            
			obj.index = index;
            obj.ApplyStart = ApplyStart;
            obj.ApplyEnd = ApplyEnd;
            obj.Text = Text;           

            JsonArray.push(obj);
            console.log(JsonArray);
            
            vTime = $("#vid")[0].currentTime;
            //dTime = Math.floor($("#vid")[0].duration);
            dTime = Math.ceil($("#vid")[0].duration);
            var $width = ApplyEnd - ApplyStart
            $width = $width / dTime * 100;
            $(".timeline").append(""+
            		"<span class='lyric-time' id='lyric"+index+"' data-begin='"+(ApplyStart)+"' data-end='"+(ApplyEnd)+"' data-width='"+$width+"'></span>"+
                "");
            $(".lyric-time").each(function(){
                var width = $(this).data("width");
                var ApplyStart = $(this).data("begin");

                ApplyStart = ApplyStart / dTime * 100;

                $(this).css({"width":width+"%","left":ApplyStart+"%"});
            });

        }


    }); */
    $("#pause").click(function(){     
      $("#vid")[0].pause();
    })
    $(document).on("click",".load-btn",function(){
       $(this).siblings("input[type=file]").click();
    });

    //더빙 저장버튼 클릭
    var audioArray = new Array();
    var k = 0;
    $(document).on("click",".audio-cont-save",function(){		
        var $root = $(this).parents(".audio-item");
        var ApplyStart = $root.find(".audio-start-input").val();
        var ApplyEnd = $root.find(".audio-end-input").val();
        var CutStart = "";
        var CutEnd = "";
        var AudioUrl = $root.find(".audio-input").attr("data-url");
        var object = new Object();
        var index = "";
        var times = []
        var title = $root.find("region").attr("title"); //오디오 적용시간
        var btnIndex = $root.find(".audio-cont-save").data("index");
        ApplyStart = mintosec(ApplyStart);
        ApplyEnd = mintosec(ApplyEnd);
		//console.log(title);
        if($root.find("region").length == 0){
            alert("Please specify audio area.")
            return false
        }
        function getstartTime(){           
            times = title.split('-');
            return times
        }
        getstartTime();
		//console.log(times);
        if($(".video-input").val() == ""){
            alert("Please upload videos.")
            return false
        }else{        	
        	$(this).addClass("dn");
        	$root.find(".audio-cont-modify").removeClass("dn")
        	
        	index = k++;            
        	
            CutStart = mintosec_cut(times[0]);
            CutEnd = mintosec_cut(times[1]);
            
            //console.log(CutStart+","+CutEnd);
            
            object.index = index;
            object.AudioUrl = '${rootPath}'+AudioUrl;
            object.ApplyStart = ApplyStart;
            object.ApplyEnd = ApplyEnd;
            object.CutStart = CutStart;
            object.CutEnd = CutEnd;

            audioArray.push(object);
            console.log(audioArray)

            $root.find(".region-time").text(title);                        
            vTime = $("#vid")[0].currentTime;
            dTime = Math.ceil($("#vid")[0].duration);
            var $width = Math.ceil(ApplyEnd) - Math.floor(ApplyStart)
            $width = $width / dTime * 100;

            $(".timeline2").append(""+
            		"<span class='lyric-time2' id='subtitle"+index+"' data-begin='"+ApplyStart+"' data-end='"+ApplyEnd+"' data-width='"+$width+"' data-index='"+index+"'></span>"+
                "");
            $(".lyric-time2").each(function(){
            	//선 위치 조정
                var index = $(this).data("index");
                var a = $('#waveform'+index).parent().prev().find(".audio-region-activeTime").text();
                var arr = new Array(); //시작부분을 찾기 위한배열
                var timearr = new Array();//분 초 구분 위한 배열
                var arr = a.split("-"); //시작부분을 분리
                var timearr = arr[0].split(":"); //분 초를 분리
                console.log(timearr[0]+":S:S:S:S:S:S:S:S:"+timearr[1]);
                var bun,cho;
                console.log(timearr[1]);
                //분-> 초변경
                if(timearr[0].charAt(0) >0 ){
                  bun = parseInt(timearr[0].charAt(0))*60;
                }
                else{
                  bun = 0;
                }
                cho = parseInt(timearr[1]);
                
                var ApplytotalTime = bun+cho;
                //console.log(totalTime+"/"+wavesurfer[index].getDuration());
                wavesurfer[index].seekTo(ApplytotalTime/wavesurfer[index].getDuration());
                //wavesurfer[index].seekTo(0.1/wavesurfer[index].getDuration());
				//wavesurfer[index].stop();
            	
                var width = $(this).data("width");
                var ApplyStart = $(this).data("begin");

                ApplyStart = ApplyStart / dTime * 100;

                $(this).css({"width":width+"%","left":ApplyStart+"%"});
                
            });
        }
    })
    
    
    //더빙 수정버튼 클릭        
    $(document).on("click",".audio-cont-modify",function(){		
        var $root = $(this).parents(".audio-item");
        var ApplyStart = $root.find(".audio-start-input").val();
        var ApplyEnd = $root.find(".audio-end-input").val();
        var CutStart = "";
        var CutEnd = "";
        var AudioUrl = $root.find(".audio-input").attr("data-url");
        var object = new Object();
        var index = $(this).data("index");
        var times = []
        var title = $root.find("region").attr("title");
        ApplyStart = mintosec(ApplyStart);
        ApplyEnd = mintosec(ApplyEnd);
		//console.log(title);
        if($root.find("region").length == 0){
            alert("Please specify audio area.")
            return false
        }
        function getstartTime(){           
            times = title.split('-');
            return times
        }
        getstartTime();
		//console.log(times);
        if($(".video-input").val() == ""){
            alert("Please upload videos.")
            return false
        }else{        	        	
            CutStart = mintosec(times[0]);
            CutEnd = mintosec(times[1]);
            
            //console.log(CutStart+","+CutEnd);
            $("#subtitle"+index+"").remove();
            for(var i = 0; i < audioArray.length ; i++){
            	audioArray[index].AudioUrl = '${rootPath}'+AudioUrl;
	            audioArray[index].ApplyStart = ApplyStart;
	            audioArray[index].ApplyEnd = ApplyEnd;
	            audioArray[index].CutStart = CutStart;
	            audioArray[index].CutEnd = CutEnd;
            }            
                        
            console.log(audioArray); 
            $root.find(".region-time").text(title);                        
            vTime = $("#vid")[0].currentTime;
            dTime = Math.ceil($("#vid")[0].duration);
            var $width = Math.ceil(ApplyEnd) - Math.floor(ApplyStart)
            $width = $width / dTime * 100;

            $(".timeline2").append(""+
                "<span class='lyric-time2' id='subtitle"+index+"' data-begin='"+ApplyStart+"' data-end='"+ApplyEnd+"' data-width='"+$width+"' data-index='"+index+"'></span>"+
                "");
            $(".lyric-time2").each(function(){
            	//선 위치 조정
                var index = $(this).data("index");
                var a = $('#waveform'+index).parent().prev().find(".audio-region-activeTime").text();
                var arr = new Array(); //시작부분을 찾기 위한배열
                var timearr = new Array();//분 초 구분 위한 배열
                var arr = a.split("-"); //시작부분을 분리
                var timearr = arr[0].split(":"); //분 초를 분리
                console.log(timearr[0]+":S:S:S:S:S:S:S:S:"+timearr[1]);
                var bun,cho;
                //분 -> 초변경
                if(timearr[0].charAt(0) >0 ){
                  bun = parseInt(timearr[0].charAt(0))*60;
                }
                else{
                  bun = 0;
                }
                cho = parseInt(timearr[1]);
                
                var ApplytotalTime = bun+cho;
                wavesurfer[index].seekTo(ApplytotalTime/wavesurfer[index].getDuration());

                var width = $(this).data("width");
                var ApplyStart = $(this).data("begin");

                ApplyStart = ApplyStart / dTime * 100;

                $(this).css({"width":width+"%","left":ApplyStart+"%"});
            });
        }
    })
    // 자막 수정
    
    $(document).on("click",".txt-cont-modify",function(){
    	
    	
    });
    
    
</script>
</body>
</html>