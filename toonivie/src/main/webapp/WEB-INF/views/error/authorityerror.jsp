<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>요청 거부</title>
</head>
<style>
* {
	margin: 0 auto;
	padding: 0;
}

.errorwrap {
	width: 1000px;
	display: block;
	margin: 250px auto;
}

.logo {
	width: 35%;
	display: block;
}

h3 {
	text-align: center;
}
h1{
	font-size: 70px;
	width:100%;
	text-align: center;
	color: red;
}
h5{
	text-align: center;
	margin-top: 30px;
	color: blue;
	cursor: pointer;
}
</style>
<body>
	<div class="errorwrap">
		<img src="/resources/image/icon/OOPS.png" alt="looking" class="logo">
			 <h1>403 ERROR! </h1>
		<h3>Access Denied!</h3>
		<h3>access to the specified resource has been forbidden</h3>
		<h5 onclick="location.href='/'">여기를 클릭하시면  메인페이지로 이동합니다.</h5>

	</div>
</body>
</html>