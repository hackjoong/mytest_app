<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>아이디 / 비밀번호 찾기</title>
<meta id="_csrf_parameter" name="_csrf_parameter" />
<meta id="_csrf" name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta id="_csrf_header" name="_csrf_header"	content="${_csrf.headerName}" />
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">

</head>
<style>
a{
color: #000; 
}
.container{
	clear:both;
	padding: 0;
	/* height: 73.4%; */
}
#findID, #findPW{
	margin-top: 20px;
}
#findID {
	width: 49%;
	float: left;
}
#findPW {
	width: 49%;
	float: right;
}
.findBtn { 
	float: right;
	margin-right: 8px;
}
.backBtn { 
	float: right;
	margin-right: 8px;
	margin-top: 25px;
}
</style>
<script>
	$(document).ready(function() {
		$(".login_findid").click(function() {
			if ($("#name").val() == '') {
				alert("이름을 입력해주십시요");
				return false;
			} else {
				var popupX = (window.screen.width/2) - (200 / 2);
				var popupY= (window.screen.height/2) - (300 / 2);
				window.open("", "popup_window", 'status=no, height=100, width=400, left='+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
				$("#findID").submit();
			}

		})
	})
</script>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<!-- 수정본2 -->
	<div class="w100 fl wrap logins re_min_hnone">
		<div class="login-form lft mt90 pb500 re_duqor re_bnone">
			<h3 class="login-title" style="font-size: 1.4em;">아이디 / 비밀번호 찾기</h3>
			<span class="login-subtitle re_log_subtitle">가입시 입력했던 아이디 비밀번호를 찾을수 있습니다.</span>
			<!-- 아이디찾기 -->
			<form name="form1" id="findID_" action="/signup/findID" method="POST">
				<div class="form-group fl w100 act mt20">
					<label for="email" class="cols-sm-2 control-label fl" style="font-size:20px; margin-top:15px;">아이디 찾기</label>
					<input type="text" name="name" id="name" autocomplete="off" class="login-id lg-input" style="margin-top:5px;" placeholder="이름">
					<input type="text" name="email" id="email" class="login-id lg-input mmt5 m_login_mail" autocomplete="off" placeholder="E-mail">
					<input type="submit" value="찾기" id="id" class="login-id lg-input orangebox m_login_mail bg_org">
				</div>
			</form>
			<!-- //아이디 찾기 -->
			<!-- 비밀번호 찾기 -->
			<form name="form1" id="findPW_" action="/signup/findPW" method="post">
				<div class="form-group fl w100 act">
					<label for="email" class="cols-sm-2 control-label fl" style="font-size:20px; margin-top:15px;">비밀번호 찾기</label>
					<input type="text" name="id" id="id" autocomplete="off" class="login-id lg-input" style="margin-top:5px;" placeholder="아이디">
					<input type="text" name="name" id="name" class="login-id lg-input mmt5 m_login_mail" autocomplete="off" placeholder="이름">
					<input type="submit" value="찾기" class="login-id lg-input orangebox m_login_mail bg_org">
				</div>
			</form>
			<!-- //비밀번호 찾기 -->
		</div>
	</div>
	<!-- //수정본2 -->
	<!-- 수정본 -->
	<!-- <div class="w100 fl wrap logins">
		<div class="login-form lft">
			<h3 class="login-title">아이디 / 비밀번호 찾기</h3>
			<span class="login-subtitle">가입시 입력했던 아이디 비밀번호를 찾을수 있습니다.</span>
			
			아이디 찾기
			<form name="form1" id="findID_" action="/signup/findID" method="POST" target="popup_window">
				<div class="form-group fl w100 act">
					<label for="username" class="cols-sm-2 control-label" style="font-size:20px; margin-top:15px;">아이디 찾기</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" name="name" id="name" class="inp name w100 ipg ipg_1" autocomplete="off" placeholder="이름">
							<input type="text" name="email" id="email" class="inp email w100 ipg" autocomplete="off" placeholder="E-mail">
						</div>
						<div class="tc mb50">
							<input type="submit" value="찾기" class="btn_0 join-okbtn login_findid findBtn_" id="id">
						</div>
					</div>
				</div>
			</form>
			//아이디 
			
			비밀번호 찾기
			<form name="form1" id="findPW_" action="/signup/findPW" method="post">
				<div class="form-group fl w100 act">
					<label for="username" class="cols-sm-2 control-label" style="font-size:20px; margin-top:15px;">비밀번호 찾기</label>
					<div class="input-group w100">
						<input type="text" name="id" id="id" class="inp w100 ipg ipg_1" autocomplete="off" placeholder="아이디">
						<input type="text" name="name" id="name" class="inp name w100 ipg" autocomplete="off" placeholder="이름">
					</div>
					<div class="tc mb50">
						<input type="submit" value="찾기" class="btn_0 join-okbtn findBtn_">
					</div>
				</div>
			</form>
			//비밀번호 찾기
		</div>
	</div> -->
	<!-- //수정본 -->
	<!-- 원본  -->
	<!-- <div class="container pd-0">
		<form name="form1" id="findID" action="/signup/findID" method="POST"
			target="popup_window">
			<div class="w100 fl mt50 info">
				<h2 class="fl w100">
					아이디 찾기
					<small class="w100 fl mt10" style="font-size: 16px;">가입 시 입력했던 이름과 이메일을 입력해주세요.</small>
				</h2>
				<table class="table join-table mt50 fl">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>이름</span></th>
						<td class="td-join">
							<input type="text" name="name" id="name" class="inp name w100" autocomplete="off">
						</td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>이메일</span></th>
						<td class="td-join">
							<input type="text" name="email" id="email" class="inp email w100" autocomplete="off">
						</td>
					</tr>
				</table>
				<div class="tc mb50">
					<input type="submit" value="찾기" class="btn_0 join-okbtn findBtn">
				</div>
			</div>
		</form>

		<form name="form1" id="findPW" action="/signup/findPW" method="post">
			<div class="w100 fr mt50 info">
				<h2 class="fl w100">
					비밀번호 찾기<span style="font-size:22px;">(임시비밀번호 발급)</span>
				<small class="w100 fl mt10" style="font-size: 16px;">가입 시 입력했던 아이디와 이름을 입력해주세요.</small>
				</h2>				
				<table class="table join-table mt50 fl">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>아이디</span></th>
						<td class="td-join">
							<input type="text" name="id" id="id" class="inp w100" autocomplete="off">
						</td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>이름</span></th>
						<td class="td-join">
							<input type="text" name="name" id="name" class="inp name w100" autocomplete="off">
						</td>
					</tr>
				</table>
				<div class="tc mb50">
					<input type="submit" value="찾기" class="btn_0 join-okbtn findBtn">
				</div>				
			</div>
		</form>
		<div class="back backBtn" style="/*float: right;*/">
			<a href="/login">뒤로가기</a>
		</div>
	</div> -->
	<!-- //원본  -->
	
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>