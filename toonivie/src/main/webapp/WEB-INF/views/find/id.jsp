<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>아이디 찾기</title>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
<style>
.table {
	margin-top: 30px;
	table-layout: fixed;
}
td {
	text-align: center;
}
</style>
<script>
$(document).ready(function() {
	$("#close").click(function() {
		window.close();
	})
});
</script>
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
<div class="w100 fl wrap logins mt90 re_duqor">
	<div class="login-form pb150 re_bnone">		
		<h3 class="login-title">TOONIVIE</h3>
		<span class="login-subtitle m_service" style="color: red;">회원님의  ID는<span style="font-weight: bold;"><c:out value="${findid.getId() }"/></span> 입니다.</span>
		<form action="/login.do" method="post">	
			<input type="text" name="id" class="login-id lg-input" placeholder="아이디">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="비밀번호">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<button class="submits">로그인</button>
		</form>
		<div class="w100 fl joins">
			<button class="lg-input join-btn" onclick="location.href='/signup/auth'">회원가입</button>
			<span class="half-find"><a href="/signup/findAccount" style="color:#337ab7">아이디 / 비밀번호 찾기</a></span>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>