<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/sweetalert2.all.min.js"></script>
<script src="/resources/common/js/admin_header.js"></script>
<script src="/resources/common/js/sweetalert2.all.min.js"></script>

<link href="/resources/common/css/admin.css" rel="stylesheet">
<link rel="stylesheet" href="/resources/common/css/slidemyheader.css" >
<!-- add by ydh 180309 - header mybutton -->
<title>webtoon_AdminPage</title>
</head>
<style>
.banner_list{
	display: none;
}
</style>
<body>
	<header class="re_admin_header">
		<img src="/resources/image/logo.png" alt="logo" class="logo m_re_logo logo_m" onclick="location.href='/'">
		<div id="header_info">
			<span class="fl"><b>${currentUser.getName()}</b>님</span>
			<!-- <button onclick="location.href='/logoutProcess.do'">로그아웃</button> -->
			<c:if test="${sessionScope.currentUser != null }">

						<!-- <button class="btn-login" style="float: right; margin-right:12%;">로그아웃</button> -->
						<ul class="fl">
							<li class="btn-login btn-my cp" style="border-radius:0; margin-right: 10%;"><span>MY</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">내 정보</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">내 서재</li>
									<li class="cp MYLIST" onclick="location.href='/logoutProcess.do'">로그아웃</li>
								<c:if test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
									<li class="cp MYLIST" onclick="location.href='/adminPage'">설정관리</li>
								</c:if>
								</ul>
							</li>
						</ul>
						<script>
						$(function(){
							$('.btn-my').mouseover(function(){
								$(this).find("ul").stop().slideDown();
							})
							$('.btn-my').mouseout(function(){
								$(this).find("ul").stop().slideUp();
							})
							$('.MYLIST').mouseover(function(){
								$(this).stop().css({"background-color":"#bbb","color":"#fff"});
								
							})
							$('.MYLIST').mouseout(function(){
								$(this).stop().css({"background-color":"#fff","color":"#767676"});
							})
						})
						</script>
					</c:if>
		</div>
	</header>
	<div id="admin_cont" class="re_admin_cont">
		<div class="admin_menu_ShowHide" onclick="admin_menu_ShowHide()">메뉴 
			<img src="/resources/image/admin_menu.png" alt="" style="width: 32px; float: right;"/>
		</div>
		<nav class="re_admin_nav">
			<ul>
				<li id="1">회원 관리 </li>
				<li id="2">작품 관리</li>
				<ul class="work_list" style="display: none;">
						<li id="2-1">- 웹툰무비</li>
						<li id="2-2">- 원작웹툰</li>
						<li id="2-3">- 시리즈</li>
				</ul>
				<li id="3">작가 관리</li>
				<li id="4">배너 관리</li>
				<ul class="banner_list">
						<li id="4-1">- 웹툰무비</li>
						<li id="4-2">- 원작웹툰</li>
				</ul>
				<li id="5">댓글 관리</li>
				<ul class="comments_list" style="display: none;">
						<li id="5-1">- 웹툰무비</li>
						<li id="5-2">- 원작웹툰</li>
				</ul>
				<!-- <li id="6">원작 관리</li>
				<li id="7">원작 댓글 관리</li> -->
				<!-- <li onclick="location.href='/admin/showWriter'">작가회원 관리</li>
				<li onclick="location.href='/admin/manageType'">분류 관리</li> -->
				<!-- <li onclick="location.href='/admin/permitContent'">작품허가</li>
				<li onclick="location.href='/qna/regularQnaAdd'">자주 묻는 질문 등록</li>
				<li onclick="location.href='/IPC_admin/manageType'">특허 분류 관리</li> -->
			</ul>
		</nav>
		<div id="Context" class="re_Context_1"></div>
	</div>