<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<link href="/resources/common/css/admin.css" rel="stylesheet">

<title>webtoon_AdminPage</title>
</head>
<script type="text/javascript">
	function home() {
		
		$.ajax({
			type : "GET",
			url : "/member/list_en",
			dataType : "text",
			error : function() {
				alert('통신실패!!');
			},
			success : function(data) {
				$('#Context').html(data);
			}

		});
	}

	home();
	
	

	$(function() {
		$('#1').css({"background":"#eee"});
		$('li').not('#1').css({"background-color":"#fff"});

		$('#1').click(function() {
			$(this).css({"background":"#eee","transition":"1s"});
			$('li').not(this).css({"background-color":"#fff","transition":"0.5s"});
			$.ajax({
				type : "GET",
				url : "/member/list_en",
				dataType : "text",
				error : function() {
					alert('Failed to retrieve page.');
				},
				success : function(data) {
					$('#Context').html(data);
				}

			});
			
		})
		$('#2').click(function() {
			$(this).css({"background":"#eee","transition":"1s"});
			$('li').not(this).css({"background-color":"#fff","transition":"0.5s"});
			$.ajax({
				type : "GET",
				url : "/webtoon/list_en",
				dataType : "text",
				error : function() {
					alert('Failed to retrieve page.');
				},
				success : function(data) {
					$('#Context').html(data);
				}

			});
		})
		$('#3').click(function() {
			$(this).css({"background":"#eee","transition":"1s"});
			$('li').not(this).css({"background-color":"#fff","transition":"0.5s"});
			
			$.ajax({
				type : "GET",
				url : "/banner/list_en",
				dataType : "text",
				error : function() {
					alert('Failed to retrieve page.');
				},
				success : function(data) {
					$('#Context').html(data);
				}

			});
		})

	})
</script>
<body>
	<header> <!-- <img src="/resources/image/ideapc_logo.jpg" alt="logo" onclick="location.href='/IPC_admin/'" style="cursor: pointer;"> -->
	<img src="/resources/image/logo.png" style="width:7%; height:7%;" alt="logo"
		class="logo" onclick="location.href='/index_en'">

	<div id="header_info">
		<span><b>${currentUser.getName()}</b></span>
		<button onclick="location.href='/logoutProcess_en.do'">Logout</button>
	</div>
	</header>
	<div id="admin_cont">
		<nav>
		<ul>
			<li id="1">Member management</li>
			<!-- <li onclick="location.href='/admin/showWriter'">작가회원 관리</li>
			<li onclick="location.href='/admin/manageType'">분류 관리</li> -->
			<li id="2">Work management</li>
			<li id="3">Banner management</li>
			<!-- <li onclick="location.href='/admin/permitContent'">작품허가</li>
			<li onclick="location.href='/qna/regularQnaAdd'">자주 묻는 질문 등록</li>
			<li onclick="location.href='/IPC_admin/manageType'">특허 분류 관리</li> -->
		</ul>
		</nav>
		<div id="Context" style="width: 85%; float: right;"></div>
	</div>