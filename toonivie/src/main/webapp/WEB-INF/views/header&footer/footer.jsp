<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<footer class="col-xs-12 mt-10 pt-50 pb-50 wraps_footer">
	<div class="container">
		<div class="w100 fl footer">
			<span>Copyright ⓒ 2017-2018 TOONIVIE All Rights Reserved.</span>
				<div style="clear:both; display: inline; font-size: 15px;">
					<span class="fr ml15" style="margin-top:4px;">TOTAL : <span id="totalcount"></span></span>
					<span class="fr" style="margin-top:4px;">TODAY : <span id="todaycount"></span></span>
				</div>
			
			<!-- <span>COPYRIGHT 2016 @ IDEAWEBTOON ALL RIGHTS RESERVED</span> -->
		</div>
	</div>
</footer>
<script>
$(function(){
	$.ajax({
		url:'/menu/visitorcount',
		type:'post',
		success: function(data) {
			document.getElementById("todaycount").innerHTML = data.todaycount;
			document.getElementById("totalcount").innerHTML = data.totalcount;
		}
	});
})

</script>
