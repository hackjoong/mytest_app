<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header class="pt30">
	<div class="container pd-0">
		<div class="col-md-12 pd-0 header-pd">
			<div class="col-md-8 col-xs-6 pd-0 header-logo">
				<img src="/resources/image/logo.png" alt="looking" class="logo" onclick="location.href='/'">
			</div>
			<div class="col-md-4 col-xs-6 pd-0 header-search-login">
				<div class="w100 fl search-header">
					<div class="search">
						<input type="text">
						<img src="/resources/image/magnifier icon.png" alt="search">
					</div>					
					<button class="btn-login" onclick="location.href='/login'">로그인</button>
				</div>
			</div>
			<div class="w100 fl today-header mt30">
				<ul>
					<li>투니비홈</li>
					<li>연재</li>
					<li>완결</li>
					<li>랭킹</li>
					<li>마이페이지</li>
				</ul>
			</div>
		</div>
	</div>
</header>