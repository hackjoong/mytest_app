<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
input::-webkit-input-placeholder {
	color: #ccc;
}

.btn-my {
	position: relative;
	float: right;
	margin-right: 12%;
}

.btn-my>span {
	width: 100%;
	text-align: center;
	margin: 0 auto;
	display: block;
	vertical-align: middle;
	margin-top: 8px;
}

.myul {
	position: absolute;
	top: 102%;
	width: 100%;
	display: none;
	background-color: #fff;
}

.MYLIST {
	text-align: center;
	padding: 7px 0;
}

.MYLIST:nth-child(2), .MYLIST:nth-child(3), .MYLIST:nth-child(4) {
	border-top: 1px solid #ccc;
}
</style>
<script>
$(function() {
	var search = document.getElementById("search_input");
	var userInfo = document.getElementById("userInfo");
	var s = location.pathname;
	if(s == "/index_jp"){
		search.style.display="";
		
	}
	else{
		search.style.display="none";
	}
})
</script>
<header class="pt30">
	<div class="container pd-0">
		<div class="language" style="float: right; margin-bottom: 10px;">
			<a href="/index_ch"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/ChinaIcon.png">中国語</a>&nbsp;&nbsp; <a
				href="/index_jp"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/JapanIcon.png">日本語</a>&nbsp;&nbsp; <a
				href="/index_en"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/USAIcon.png">English</a>&nbsp;&nbsp; <a
				href="/" style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/KoreaIcon.png">한국어</a>
		</div>
		<div class="col-md-12 pd-0 header-pd">
			<div class="col-md-8 col-xs-6 pd-0 header-logo">
				<img src="/resources/image/logo.png" alt="looking" class="logo"
					onclick="location.href='/index_jp'">
			</div>

			<div class="col-md-4 col-xs-6 pd-0 header-search-login">
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_USER' }">
					<font class="userInfo" id="userInfo"
						style="float:right; position: absolute; right:12%; margin: 10px 0; font-size: 15px;">Welcome ${sessionScope.currentUser.getName()}様
						歓迎します 
					</font>

				</c:if>
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_ADMIN' }">
					<font class="userInfo" id="userInfo"
						style="float:right;position: absolute; right:12%; margin: 10px 0; font-size: 15px;">${sessionScope.currentUser.getName()}管理者
					</font>

				</c:if>
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_WRITER' }">
					<font class="userInfo" id="userInfo"
						style="float:right; position: absolute; right:12%; margin: 10px 0; font-size: 15px;">${sessionScope.currentUser.getName()}作家
					</font>

				</c:if>
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_COWORKER' }">
					<font class="userInfo" id="userInfo"
						style="float:right; position: absolute; right:12%; margin: 10px 0; font-size: 15px;">${sessionScope.currentUser.getName()} マネージャー
					</font>

				</c:if>
				<div class="w100 fl search-header">
					<div class="search" id="search_input">
						<input type="text" id="searchValue" placeholder="タイトル、ジャンル、作家" style="font-size:16px;" > 
						<img src="/resources/image/magnifier icon.png" alt="search" onclick="myFunction()">
					</div>
					<c:if test="${sessionScope.currentUser != null }">
						<ul>
							<li class="btn-login btn-my cp" style="border-radius:0;"><span>私</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">マイページ</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">私の書斎</li>
									<li class="cp MYLIST" onclick="location.href='/logoutProcess.do'">ログアウト</li>
								<c:if test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
									<li class="cp MYLIST" onclick="location.href='/adminPage'">設定管理</li>
								</c:if>
								</ul>
							</li>
						</ul>
						<script>
						$(function(){
							$('.btn-my').mouseover(function(){
								$(this).find("ul").stop().slideDown();
							})
							$('.btn-my').mouseout(function(){
								$(this).find("ul").stop().slideUp();
							})
							$('.MYLIST').mouseover(function(){
								$(this).stop().css({"background-color":"#bbb","color":"#fff"});
								
							})
							$('.MYLIST').mouseout(function(){
								$(this).stop().css({"background-color":"#fff","color":"#767676"});
							})
						})
						</script>
					</c:if>
					<c:if test="${sessionScope.currentUser == null }">
						<button class="btn-login" onclick="location.href='/login_jp'" style="float: right; margin-right:10%;">ログイン</button>
					</c:if>
				</div>
			</div>
			<div class="w100 fl today-header mt30">
				<ul>
					<li>メイン</li>
					<li>新作</li>
					<li>ジャンル</li>
					<li>ランキング</li>
					<li>原作</li>
				</ul>
			</div>
		</div>
	</div>
</header>