<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="w100 fl header-bar">
	<div class="fl logo-zone">
		<div class="ab position50">
			<Img src="/resources/image/v2.0/TOONIVIE_logo.png" class="upload-logo cp" onclick="location.href='/'">
		</div>		
	</div>
	<!-- <div class="tc brl h100 header-cont fl position50">
		<div class="re w100 fl h50 ab position50 header-wrap">			
			<img src="/resources/image/toolbar.png" class="h100">
		</div>
	</div> -->
	<div class="tutorial dn"><!-- hide by shkwak, 2018-10-26 -->
		<a href="javascript:void(0)">튜토리얼</a>
	</div>
	<div class="header_language dn" style="margin-left: 59.5%;"> 
		<ul>
			<li class="language_on"><a href="/cv/effect">KOR</a></li>
			<li><a href="/cv/effect_en">ENG</a></li>
		</ul>
	</div>
	<div class="fr">
		<ul class="top_btn_link">
			<li class="collabor dn"><a href="javascript:void(0)">협업툴 바로가기</a></li>
			<li class="collabor"><a href="/audionsubtitle" target="_blank">자막/더빙 툴 바로가기</a></li>
		</ul>	
	</div>
</div>
