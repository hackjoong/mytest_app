<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="w100 fl header-bar">
	<div class="fl logo-zone">
		<div class="ab position50">
			<Img src="/resources/image/v2.0/TOONIVIE_logo.png" class="upload-logo cp" onclick="location.href='/'">
		</div>		
	</div>
	<!-- <div class="tc brl h100 header-cont fl position50">
		<div class="re w100 fl h50 ab position50 header-wrap">			
			<img src="/resources/image/toolbar.png" class="h100">
		</div>
	</div> -->
	<div class="tutorial">
		<a href="javascript:void(0)" onclick="">Tutorial</a>
	</div>
	<div class="header_language"> 
		<ul>
			<li><a href="/cv/effect">KOR</a></li>
			<li class="language_on"><a href="/cv/effect_en">ENG</a></li>
		</ul>
	</div>
	<div class="fr">
		<ul class="top_btn_link">
			<li class="collabor"><a href="javascript:void(0)">Cooperation Tool</a></li>
			<li class="collabor"><a href="/audionsubtitle_en" target="_blank">Audionsubtitle Tool</a></li>
		</ul>	
	</div>
</div>
