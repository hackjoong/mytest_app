<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/slidemyheader.css" >
<!-- add by ydh 180309 - header mybutton -->

<link href="/resources/common/css/admin.css" rel="stylesheet">

<title>webtoon_writerPage</title>
</head>
<body>
<header>        
        <!-- <img src="/resources/image/ideapc_logo.jpg" alt="logo" onclick="location.href='/IPC_admin/'" style="cursor: pointer;"> -->
        <div id="header_info">
            <span style="font-size:20px;"><b>${currentUser.getName()}</b>님</span>
            <!-- <button onclick="location.href='/logoutProcess.do'">로그아웃</button> -->
            <c:if test="${sessionScope.currentUser != null }">

						<!-- <button class="btn-login" style="float: right; margin-right:12%;">로그아웃</button> -->
						<ul>
							<li class="btn-login btn-my cp" style="border-radius:0; margin-right: 10%;"><span>MY</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">내 정보</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">내 서재</li>
									<li class="cp MYLIST" onclick="location.href='/logoutProcess.do'">로그아웃</li>
								<c:if test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
									<li class="cp MYLIST" onclick="location.href='/adminPage'">설정관리</li>
								</c:if>
								</ul>
							</li>
						</ul>
						<script>
						$(function(){
							$('.btn-my').mouseover(function(){
								$(this).find("ul").stop().slideDown();
							})
							$('.btn-my').mouseout(function(){
								$(this).find("ul").stop().slideUp();
							})
							$('.MYLIST').mouseover(function(){
								$(this).stop().css({"background-color":"#bbb","color":"#fff"});
								
							})
							$('.MYLIST').mouseout(function(){
								$(this).stop().css({"background-color":"#fff","color":"#767676"});
							})
						})
						</script>
					</c:if>
        </div>        
    </header>
    <div id="admin_cont"></div>
        <nav>
            <ul>
                <li onclick="location.href='/writer/uploadwebtoon'">웹툰업로드</li>
                
            </ul>  
        </nav>
    
    