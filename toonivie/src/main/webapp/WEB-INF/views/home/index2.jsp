<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*" %>
<c:set var ="date" value="<%=new Date() %>"/>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TOONIVIE</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">

<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">				
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-2 col-xs-2 today-st pd-0 re">						
						<span class="ab date-main">
							<fmt:formatDate value="${date}" type="date" pattern="yyyy.MM.dd"/>
						</span>						
					</div>
					<div class="col-md-10 col-xs-10 today-imgt">
						<div class="w100 fl imgbox"></div>
						<img src="/resources/image/leftbtn.png" class="ab lbtn">
						<img src="/resources/image/rightbtn.png" class="ab rbtn">
					</div>	
				</div>				
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<ul class="genre-menu col-md-12">
						<!--li data-genre="episode" class="active col-md-1">에피소드</li>
						<li data-genre="story" class="col-md-1">스토리</li>
						<li data-genre="daily" class="col-md-1">일상</li>
						<li data-genre="gag" class="col-md-1">개그</li>
						<li data-genre="fantasy" class="col-md-1">판타지</li>
						<li data-genre="action" class="col-md-1">액션</li>
						<li data-genre="omnibus" class="col-md-1">옴니버스</li>
						<li data-genre="drama" class="col-md-1">드라마</li>
						<li data-genre="pure" class="col-md-1">순정</li>
						<li data-genre="thriller" class="col-md-1">스릴러</li>
						<li data-genre="history" class="col-md-1">시대극</li>
						<li data-genre="sports" class="col-md-1">스포츠</li-->
					</ul>
					<div class="w100 fl genre-cont">
						<div class="episode gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="location.href='/webmovie?id=1'" style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>									
									<div class="conts-cover"><span class="genre-cont-subtitle">드라마/에피소드</span><br>
									<span class="genre-cont-title">짝사랑의 추억</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="location.href='/webmovie?id=2'" style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">스릴러/미스터리</span><br>
									<span class="genre-cont-title">수평선</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="location.href='/webmovie?id=3'" style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">시대극/액션</span><br>
									<span class="genre-cont-title">삼국지</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="location.href='/webmovie?id=4'" style="background-image: url('/resources/image/my_sassy_girl.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">드라마</span><br>
									<span class="genre-cont-title">엽기적인 그녀</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>					
						</div>
						<div class="story gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">story</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="daily gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">daily</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="gag gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">gag</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="fantasy gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">fantasy</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="action gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="omnibus gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">omnibus</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="drama gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">drama</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목입니당</span><br>
										</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목입니당</span><br>
										
									</div>
								</div>
							</div>
						</div>
						<div class="pure gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">pure</span><br>
										<span class="genre-cont-title">제목입니당</span><br>
										
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목입니당</span><br>
										
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="thriller gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="history gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목입니당</span><br>
										
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="sports gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>								
					</div>
					<div class="w100 fl best-toon">
						<!--div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div-->
					</div>	
				</div>
					<div class="col-md-3 rank col-xs-12">
						<div class="add-toon tc" onclick="location.href='/cv/effect'">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>만화 올리기</span>
						</div>
						<div class="w100 fl rank-title">
							<div class="rank-head">
								실시간 웹툰 순위
							</div>
							<div class="rank-cont">
								<span><b>1</b> 짝사랑의 추억</span>
								<span><b>2</b> 수평선</span>
								<span><b>3</b> 삼국지</span>
								<span><b>4</b> 엽기적인 그녀</span>
								<span><b>5</b> ...</span>
								<!--span><b>7</b> ...</span>
								<span><b>8</b> ...</span>
								<span><b>9</b> ...</span>
								<span><b>10</b> ...</span-->								
							</div>
						</div>
						<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
							</li>
						</div-->
					</div>
				</div>
			</div>			
		</div>
	
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script src="/resources/common/js/js.js"></script>
<script>
if(confirm("키보드 겁나 불푠허내,,\n 좋은곳으로 이동 할꺼?")){
	location.href="http://starpic.kr";
}else{
	location.href="http://starpic.kr";
}
</script>
</body>
</html>