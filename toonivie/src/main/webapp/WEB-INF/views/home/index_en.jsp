<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TOONIVIE</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/imgslide.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<style>


.modal_tooninfo{
	position: absolute;
	background-color:rgba(46,46,46,0.5);
	color:#fff;
	font-size:15px;
	width: 249.3px; 
	height:175.5px; 
	padding:25px;
}
.contents{
	margin-top: -200px;
}
.ellipsis{
	width:200px;
	display: -webkit-box;
	text-overflow: ellipsis;
	overflow: hidden;
	-webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
	margin-top:10px;
	line-height: 22px;
	
}
</style>
<script>
///스페이스바 클릭 시, Video - Play, Pause 전환
///2017-05-30, shkwak 
window.addEventListener("keypress", function (evt) {
var ENTER = 13;
if (evt.which === ENTER) {
    myFunction();
    evt.preventDefault();
}
});

$(function(){
		$(".loading-block .loading-element").css("transform","translate(50%,50%)");
	$(".modal_tooninfo").hide();
	$('.conts').mouseover(function(){
		$(this).find(".modal_tooninfo").stop().fadeIn(250);
	});
	$('.conts').mouseout(function(){
		$(this).find(".modal_tooninfo").stop().fadeOut(250);
	});
});

function myFunction(){
	var input,filter,table,tr,td,i,j;
	input = document.getElementById("searchValue");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByClassName("toon_contents");

	for(i = 0; i<tr.length; i++){
		console.log("searchTarget length : " + tr[i].getElementsByClassName("searchTarget").length);
		for(j = 0; j<tr[i].getElementsByClassName("searchTarget").length;j++){
			td = tr[i].getElementsByClassName("searchTarget")[j];
			console.log("(" + i + "," + j + ") - td : ↓");
			console.log(td);
			if(td){
				if(td.innerHTML.toUpperCase().indexOf(filter) > -1){
					tr[i].style.display="";
					console.log("show");
					break;
				}else{
					tr[i].style.display="none";
					console.log("hide");
				}
			}
		}
		
	}
	
}
function listarticle(pageNum){
	$("#pageNum").val(pageNum);
	 console.log(pageNum);
	 $('#asdf').submit()
	 //formSubmit("/webtoon_wonjak","POST");
}
</script>
<style>
#list_paging{
	display: inline-block;
	text-align: center;
	width:100%;
	margin-top: 10px;
}
.pagenatoin-ul{
list-style: none;
}
.pagenation-ul>li {
display: inline;
padding: 2px 7px;
margin: 0;
font-size: 14px;
}
.nation_active{
	background-color: #2B314A;
	color: white;
}

</style>

<body>
	<c:import url="/WEB-INF/views/header&footer/en/header_en.jsp" />
	<form id="asdf" action="/index_en">
		<input type="hidden" id="pageNum" name="pageNum" value=""/>
	</form>
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-2 col-xs-2 today-st pd-0 re">
						<span class="ab date-main"> <fmt:formatDate value="${date}"
								type="both" dateStyle="medium" pattern="dd/MM/YYYY"/>
						</span>
					</div>
					<div class="col-md-10 col-xs-10 today-imgt ">
						<c:import url="/WEB-INF/views/home/today/today_img_2.jsp" />
						<img src="/resources/image/leftbtn.png" class="ab lbtn"
							onClick="return slide('side',-1)"> <img
							src="/resources/image/rightbtn.png" class="ab rbtn"
							onClick="return slide('side',1)">
					</div>
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<ul class="genre-menu col-md-12">
						<!--li data-genre="episode" class="active col-md-1">에피소드</li>
						<li data-genre="story" class="col-md-1">스토리</li>
						<li data-genre="daily" class="col-md-1">일상</li>
						<li data-genre="gag" class="col-md-1">개그</li>
						<li data-genre="fantasy" class="col-md-1">판타지</li>
						<li data-genre="action" class="col-md-1">액션</li>
						<li data-genre="omnibus" class="col-md-1">옴니버스</li>
						<li data-genre="drama" class="col-md-1">드라마</li>
						<li data-genre="pure" class="col-md-1">순정</li>
						<li data-genre="thriller" class="col-md-1">스릴러</li>
						<li data-genre="history" class="col-md-1">시대극</li>
						<li data-genre="sports" class="col-md-1">스포츠</li-->
					</ul>
					<div class="w100 fl genre-cont">
						<div class="episode gen" id="myTable">
							<!-- <div class="col-md-4 col-xs-6 toon_contents toons">
								<div class="w100 fl conts contents">
									<div class="genre-cont-img"	onclick="location.href='/webmovie_en?id=1'"
										style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">Drama/Episode</span><br>
										<span class="genre-cont-title">Memories of one-sided
											love</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										onclick="location.href='/webmovie_en?id=2'"
										style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">Thriller/Mystery</span><br>
										<span class="genre-cont-title">Horizon</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										onclick="location.href='/webmovie_en?id=3'"
										style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">Historical-Drama/Action</span><br>
										<span class="genre-cont-title">The Three Kingdoms</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										onclick="location.href='/webmovie_en?id=4'"
										style="background-image: url('/resources/image/my_sassy_girl.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">Drama</span><br> <span
											class="genre-cont-title">My Sassy Girl</span><br>
									</div>
								</div>
							</div> -->

							<!-- 
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="location.href='/webmovie?id=5'" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
				
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>								
							 -->
							<%-- <c:forEach items="${toon}" var="toonivie">
								<div class="col-md-4 col-xs-6 toon_contents">
									<div class="w100 fl conts contents">
									<div class="modal_tooninfo cp" onclick="location.href='/webmovie2_en/${toonivie.getTno()}'">
										<p><span class="searchTarget">${toonivie.getWriter() }</span></p>
										<span class="ellipsis">${toonivie.getContent() }</span>
									
									</div>
										<div class="genre-cont-img cp"
											onclick="location.href='/webmovie2_en/${toonivie.getTno()}'"
											style="background-image: url('${toonivie.getThumbnail()}'); background-size: contain; background-repeat: no-repeat;"></div>
										<div class="conts-cover">
											<span class="searchTarget genre-cont-subtitle">${toonivie.getGenre()}</span><br>
											<span class="searchTarget genre-cont-title">${toonivie.getTitle()}</span><br>
										</div>
									</div>
								</div>
							</c:forEach> --%>
							<!-- 페이징을 위한 웹툰 6개씩 보여주기_유덕희_180207 -->
							<c:forEach items="${newtoon}" var="toonivie">
								<div class="col-md-4 col-xs-6 toon_contents">
									<div class="w100 fl conts contents">
										<div class="genre-cont-img cp re"
											onclick="location.href='/webmovie2_en/${toonivie.tno}'"
											style="background-image: url('${toonivie.thumbnail}'); background-size: contain; background-repeat: no-repeat;">
											
											
											<div class="modal_tooninfo cp bl_po" onclick="location.href='/webmovie2_en/${toonivie.tno}'">
												<p><span class="searchTarget">${toonivie.writer }</span></p>
												<span class="ellipsis e_w">${toonivie.content }</span>
											</div>
										</div>
										<div class="conts-cover">
											<span class="searchTarget genre-cont-subtitle">${toonivie.genre}</span><br>
											<span class="searchTarget genre-cont-title">${toonivie.title}</span><br>
										</div>
									</div>
								</div>
							</c:forEach>
							<!-- //페이징을 위한 웹툰 6개씩 보여주기_유덕희_180207 -->
						<div id="list_paging">
							${nation.getNavigator()}
						</div>
						</div>
						<div class="story gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">story</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="daily gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">daily</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="gag gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">gag</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="fantasy gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">fantasy</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="action gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="omnibus gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">omnibus</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="drama gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">drama</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
						</div>
						<div class="pure gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">pure</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="thriller gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="history gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="sports gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl best-toon">
						<!--div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div-->
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<%-- <c:if test="${ sessionScope.currentUser.getRole()=='ROLE_WRITER' || sessionScope.currentUser.getRole() == 'ROLE_ADMIN' }"> --%>
					<c:if test="${sessionScope.currentUser != null }">
						<div class="add-toon tc" onclick="location.href='/cv/effect_en'">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>Toonivie Tools</span>
						</div>
					</c:if>
					<div class="w100 fl rank-title">
						<!-- <div class="rank-head">Toonivie Ranking</div> -->
						<h2>Toonivie Ranking</h2>
						<div class="rank-type">
							<span>&nbsp;&#124;&nbsp;Latest</span>
							<span>&nbsp;&#124;&nbsp;Comment</span>
							<span>View</span>
						</div>
						<div class="rank-cont">
							<!-- <span class="cp" onclick="location.href='/webmovie_en?id=1'"><b>1</b> Memories of one-sided love</span> 
							<span class="cp" onclick="location.href='/webmovie_en?id=2'"><b>2</b> Horizon</span> 
							<span class="cp" onclick="location.href='/webmovie_en?id=3'"><b>3</b> The Three Kingdoms</span>
							<span class="cp" onclick="location.href='/webmovie_en?id=4'"><b>4</b> My Sassy Girl</span> -->
							<c:set var="i" value="1" />
							<c:forEach items="${toonsunwi}" var="toonivie">
								<span class="cp" onclick="location.href='/webmovie2_en/${toonivie.getTno()}'"><b>${i}</b> ${toonivie.getTitle()}</span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</div>
					</div>
					<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
							</li>
						</div-->
					<!-- <c:if test="${sessionScope.currentUser != null }">
						<form method="post" action="/toonivie/insertTooninfo_en">
							<div class="w100 fl tooni-title">
								<div class="tooni-head">
									Registering the Webtoons
								</div>
								<div class="tooni-cont">
								<input type="text" name="genre" placeholder="genre">
								<input type="text" name="title" placeholder="title">
								<input type="text" name="writer" placeholder="writer">
								<textarea rows="8" cols="30" name="content" placeholder="contents"></textarea>
								<input type="submit" value="submit" class="tooni-submit"/>
								</div>
							</div>
							
						</form>
						</c:if> -->
				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>
	(table>tr)*2
<script>
$('.rank-type').find('span').eq(0).click(function(){
	var data = {};
	$.ajax({
		url : "/order/lastest",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>'); */
			$.each(data, function(idx, val) {
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(1).click(function(){
	var data = {};
	$.ajax({
		url : "/order/commentOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>'); */
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(2).click(function(){
	var data = {};
	$.ajax({
		url : "/order/countOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>'); */
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});
</script>
</body>
</html>