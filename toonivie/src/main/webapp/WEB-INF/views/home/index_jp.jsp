<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TOONIVIE</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">

<style>
* {
	margin: 0 auto;
	padding: 0;
	border-collapse: collapse;
	box-sizing: border-box;
	font-family: 'Noto Sans Korean' !important;
	list-style: none;
}
</style>
<style>


.modal_tooninfo{
	position: absolute;
	background-color:rgba(46,46,46,0.5);
	color:#fff;
	font-size:15px;
	width: 249.3px; 
	height:175.5px; 
	padding:25px;
}
.contents{
	margin-top: -200px;
}
.ellipsis{
	width:200px;
	display: -webkit-box;
	text-overflow: ellipsis;
	overflow: hidden;
	-webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
	margin-top:10px;
	line-height: 22px;
	
}
</style>
<script>
///스페이스바 클릭 시, Video - Play, Pause 전환
///2017-05-30, shkwak 
window.addEventListener("keypress", function (evt) {
var ENTER = 13;
if (evt.which === ENTER) {
    myFunction();
    evt.preventDefault();
}
});

$(function(){
		$(".loading-block .loading-element").css("transform","translate(50%,50%)");
	$(".modal_tooninfo").hide();
	$('.conts').mouseover(function(){
		$(this).find(".modal_tooninfo").stop().fadeIn(250);
	});
	$('.conts').mouseout(function(){
		$(this).find(".modal_tooninfo").stop().fadeOut(250);
	});
});

function myFunction(){
	var input,filter,table,tr,td,i,j;
	input = document.getElementById("searchValue");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByClassName("toon_contents");

	for(i = 0; i<tr.length; i++){
		for(j = 0; j<tr[i].getElementsByClassName("searchTarget").length;j++){
			td = tr[i].getElementsByClassName("searchTarget")[j];
			if(td){
				if(td.innerHTML.toUpperCase().indexOf(filter) > -1){
					tr[i].style.display="";
					break;
				}else{
					tr[i].style.display="none";
				}
			}
		}
		
	}
	
}

function listarticle(pageNum){
	$("#pageNum").val(pageNum);
	 $('#asdf').submit()
	 //formSubmit("/webtoon_wonjak","POST");
}
</script>
<style>
#list_paging{
	display: inline-block;
	text-align: center;
	width:100%;
	margin-top: 10px;
}
.pagenatoin-ul{
list-style: none;
}
.pagenation-ul>li {
display: inline;
padding: 2px 7px;
margin: 0;
font-size: 14px;
}
.nation_active{
	background-color: #2B314A;
	color: white;
}

</style>
<body>
	<c:import url="/WEB-INF/views/header&footer/jp/header_jp.jsp" />
	<form id="asdf" action="/index_jp">
		<input type="hidden" id="pageNum" name="pageNum" value=""/>
	</form>
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-2 col-xs-2 today-st pd-0 re">
						<span class="ab date-main"> <fmt:formatDate value="${date}"
								type="date" pattern="yyyy.MM.dd" />
						</span>
					</div>
					<div class="col-md-10 col-xs-10 today-imgt ">
						<c:import url="/WEB-INF/views/home/today/today_img_2.jsp" />
						<img src="/resources/image/leftbtn.png" class="ab lbtn"
							onClick="return slide('side',-1)"> <img
							src="/resources/image/rightbtn.png" class="ab rbtn"
							onClick="return slide('side',1)">
					</div>
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<ul class="genre-menu col-md-12">
					</ul>
					<div class="w100 fl genre-cont">
						<div class="episode gen" id="myTable">

							<c:forEach items="${newtoon}" var="toonivie">
								<div class="col-md-4 col-xs-6 toon_contents">
									<div class="w100 fl conts contents">
										<div class="genre-cont-img cp re"
											onclick="location.href='/webmovie2_jp/${toonivie.tno}'"
											style="background-image: url('${toonivie.thumbnail}'); background-size: contain; background-repeat: no-repeat;">
											
											
											<div class="modal_tooninfo cp bl_po" onclick="location.href='/webmovie2_jp/${toonivie.tno}'">
												<p><span class="searchTarget">${toonivie.writer }</span></p>
												<span class="ellipsis e_w">${toonivie.content }</span>
											</div>
										</div>
										<div class="conts-cover">
											<span class="searchTarget genre-cont-subtitle">${toonivie.genre}</span><br>
											<span class="searchTarget genre-cont-title">${toonivie.title}</span><br>
										</div>
									</div>
								</div>
							</c:forEach>
							<!-- //페이징을 위한 웹툰 6개씩 보여주기_유덕희_180207 -->
						<div id="list_paging">
							${nation.getNavigator()}
						</div>
						</div>
						<div class="story gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">story</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="daily gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">daily</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="gag gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">gag</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="fantasy gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">fantasy</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="action gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="omnibus gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">omnibus</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="drama gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">drama</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
						</div>
						<div class="pure gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">pure</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="thriller gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="history gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="sports gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl best-toon">
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<%-- <c:if test="${ sessionScope.currentUser.getRole()=='ROLE_WRITER' || sessionScope.currentUser.getRole() == 'ROLE_ADMIN' }"> --%>
					<c:if test="${sessionScope.currentUser != null }">
						<div class="add-toon tc" onclick="location.href='/cv/effect'">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>トゥニビ製作ツール</span>
						</div>
					</c:if>					
					<div class="w100 fl rank-title">
						<!-- <div class="rank-head">リアルタイム順位</div> -->
						<h2>リアルタイム順位</h2>
						<div class="rank-type" style="">
							<span>&nbsp;&#124;&nbsp;最新</span><span>&nbsp;&#124;&nbsp;コメント</span>
							<span>照会</span>
						</div>
						<div class="rank-cont">
							<!--< span class="cp" onclick="location.href='/webmovie_jp?id=1'"><b>1</b> 片思いの思い出</span>
							<span class="cp" onclick="location.href='/webmovie_jp?id=2'"><b>2</b> 水平線</span>
							<span class="cp" onclick="location.href='/webmovie_jp?id=3'"><b>3</b> 三国志</span> 
							<span class="cp" onclick="location.href='/webmovie_jp?id=4'"><b>4</b> 猟奇的な彼女</span>
							 --><c:set var="i" value="1" />
							<c:forEach items="${toonsunwi}" var="toonivie">
								<span class="cp"  onclick="location.href='/webmovie2_jp/${toonivie.getTno()}'"><b>${i}</b> ${toonivie.getTitle()}</span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>
<script>
$('.rank-type').find('span').eq(0).click(function() {
		var data = {};
		$.ajax({
			url : "/order/lastest",
			type : 'post',
			data : data,
			async : false,
			success : function(data) {
				$('.rank-cont').empty();
				$.each(data,function(idx, val) {
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2_jp/'+val.tno+'" style="text-decoration:none; color:#000;"><b>'+ (idx+1)+ "</b> "+ val.title+ ' </a></div>');
				});
				$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					});
			}
		});
});
$('.rank-type').find('span').eq(1).click(function() {
	var data = {};
	$.ajax({
		url : "/order/commentOrder",
		type : 'post',
		data : data,
		async : false,
		success : function(data) {
			$('.rank-cont').empty();
			$.each(data,function(idx, val) {
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_jp/'+val.tno+'" style="text-decoration:none; color:#000;"><b>'+ (idx+1)+ "</b> "+ val.title+ ' </a></div>');
			});
			$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				});
		}
	});
});

$('.rank-type').find('span').eq(2).click(function() {
	var data = {};
	$.ajax({
		url : "/order/countOrder",
		type : 'post',
		data : data,
		async : false,
		success : function(data) {
			$('.rank-cont').empty();
			$.each(data,function(idx, val) {
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_jp/'+val.tno+'" style="text-decoration:none; color:#000;"><b>'+ (idx+1)+ "</b> "+ val.title+ ' </a></div>');
			});
			$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				});
		}
	});
});

</script>
</body>
</html>