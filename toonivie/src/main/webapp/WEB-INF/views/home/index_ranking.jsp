<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon/favicon-72x72.png">

<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/owl.carousel.css">
<title>TOONIVIE</title>
<style>
.ranking-title{
	font-size:20px;
	margin-top:10px;
	color:#333;
	text-align:left;
	font-weight:bold;
}
.rank1_imgbox{
	width:100%;
	height:150px;
	overflow: hidden;
	position: relative;
	background-repeat: no-repeat !important;
	background-position: center !important;
	background-size: contain !important;
}
.rank1_infobox{
	width:80%;
	position: absolute;
	top: 80px;
	left:12px;
	/* color:#fff; */
}
.rank1_infobox p.title,.rank_infobox p.title{
	font-size:16px;
	font-weight:bold;
	margin-bottom:5px;
}
.rank1_infobox span.rank_num1{
	font-size:38px;
	font-weight:bold;
	color:#6AC599;
	/* margin-left:5px; */
	vertical-align: text-bottom;
}
.rank1_infobox p.synopsys,.rank_infobox p.synopsys {
	width:100%;
	padding-left:24px;
	font-size:12px;
	line-height:16px;
	display: inline-block;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space:nowrap;
}
span.rank_num{
	font-size:38px;
	color:#ccc;
	margin:0 15px;
	line-height: 95px;
	float: left;
}
.rank_thumb{
	width:100px;
	height:70px;
	background-size: cover;
	border: 1px solid #ddd;
	float: left;
	margin-top:10px;
	overflow: hidden;
	background-position: center center;
}
.rank_infobox{
	width:55%;
	height:80px;
	float: right;
	padding: 8px 0 0 0;
}
.rank_num_10{
	margin-left:0;
	margin-right:10px;
}

.order_thumb{
	/* width:58px; */
	width:100%;
	/* height:58px; */
	/* height:108px; */
	padding-top: 60%;
	background-size: cover;
	border: 1px solid #ddd;
	float: left;
	overflow: hidden;
	background-position: center center;
}
.lastest_li{
	padding:2px;
	width:33.33333%;
}

.popularity{
	margin-top:20px;
}
.lastest_ul>li>div{
	margin-top:6px;
	/* margin-left:4px; */
}

</style>
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
<div class="w100 fl">
	<div class="container pd-0">
		<div class="col-md-12 today pd-0">
			<div class="col-md-12 col-xs-12 today-wrap">
				<div class="col-md-2 col-xs-2 today-st pd-0 re">
					<span class="ab date-main"> <fmt:formatDate value="${date}"
							type="date" pattern="yyyy.MM.dd" />
					</span>
				</div>
				<div class="col-md-10 col-xs-10 today-imgt ">
					<c:import url="/WEB-INF/views/home/today/today_img_2.jsp" />
					<img src="/resources/image/leftbtn.png" class="ab lbtn"
						onClick="return slide('side',-1)"> <img
						src="/resources/image/rightbtn.png" class="ab rbtn"
						onClick="return slide('side',1)">
				</div>
			</div>
		</div>
		<c:set var="i" value="1"/>
		<div class="col-xs-12 genre pd-0 ranking_genre">
			<div class="w100 mt10 mb5">
				<div class="ranking_last">
					<h3 class="ranking-title fl">웹툰무비 최신작</h3>
					<ul class="w100 fl lastest_ul ranking_lastest_ul">
						<c:forEach items="${webmovielast }" var="LastWebmovie" end="2" step="1" >
							<li class="fl lastest_li cp" onclick="location.href='/webmovie2/${LastWebmovie.getTno()}'">
								<div class="order_thumb" style="background-image:url('${LastWebmovie.getThumbnail()}');"></div>
								<div class="fl">
									<p class="title ranking_title" style="font-size:16px; font-weight: bold; margin-bottom:0;">${LastWebmovie.getTitle() }</p>
									<p class="synopsys" style="font-size:14px; color:#888;">${LastWebmovie.getWriter() }</p>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="ranking_last">
					<h3 class="ranking-title fl">원작웹툰 최신작</h3>
					<ul class="w100 fl lastest_ul">
						<c:forEach items="${wonjaklast }" var="LastWonjak" end="2" step="1" >
							<li class="fl lastest_li cp" onclick="location.href='/webtoon/${LastWonjak.getWtno()}'">
								<div class="order_thumb" style="background-image:url('${LastWonjak.getThumbnail()}');"></div>
								<div class="fl">
									<p class="title ranking_title" style="font-size:16px; font-weight: bold; margin-bottom:0;">${LastWonjak.getWtname() }</p>
									<p class="synopsys" style="font-size:14px;">${LastWonjak.getWtwriter() }</p>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="w100">
				<div class="w100 fl genre-cont rank-wrap popularity">
					<div class="w35 episode fl ranking_popularity">
						<h3 class="ranking-title fl">웹툰무비 인기순위</h3>
						<!-- <span class="fr cp" style="width:4%;margin-top:14px;" onclick="location.href='/movietoon'">
							<img src="/resources/image/plus.png" width="100%;" style="float: right;">
						</span> -->
						<a href="/movietoon" class="fr cp" style=";margin-top:14px; font-size: 12px;">
							더보기+
						</a>
						<div class="w100">
							<ul class="ranking_border" style="clear:both;">
							<c:set var="i" value="1"/>
								<c:forEach var="movierank" items="${webmovieRank }" end="4" step="1" >
										<c:choose>
											<c:when test="${i==1 }">
											<li class="cp" style="border:1px solid #ccc;clear:both;" onclick="location.href='/webmovie2/${movierank.getTno()}'">
												<%-- <div class="rank1_imgbox" style="background: url('${movierank.getThumbnail() }');">
													<!-- <img src="/resources/image/slider-gradient.png" style="width:inherit; height:150px;"> -->
													<div class="rank1_infobox">
														<p class="title">
															<span class="rank_num1">${i}</span>${movierank.getTitle()}</p>
														<p class="synopsys">${movierank.getContent() }</p>
													</div>
												</div> --%>
												<div class="rank1_imgbox" style="background: url('${movierank.getThumbnail() }');">
													<div class="rank1_infobox">
														<div class="re_rank_num1 fl">${i}</div>
														<div class="re_rank_textbox">
															<div class="re_rank_tit">${movierank.getTitle()}</div>
															<div class="re_rank_synopsys">${movierank.getContent() }</div>
														</div>
													</div>
												</div>
											</li>	
											</c:when>
											<c:otherwise>
											<li class="cp" style="border-bottom:1px solid #ccc;clear:both;" onclick="location.href='/webmovie2/${movierank.getTno()}'">
												<span class="rank_num rank_num_${i }">${i}</span>
												<div class="rank_thumb" style="background-image:url('${movierank.getThumbnail() }');"></div>
												<div class="rank_infobox">
													<p class="title ranking_title_sub">${movierank.getTitle()}</p>
													<p class="synopsys" style="padding-left:1px;">${movierank.getContent() }</p>
												</div>
											</li>
											</c:otherwise>
										</c:choose>										
									<c:set var="i" value="${i+1 }"/>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="w35 episode fl ranking_popularity ranking_popularity_1">
						<h3 class="ranking-title fl">원작웹툰 인기순위</h3>
						<!-- <span class="fr cp" style="width:4%;margin-top:14px;" onclick="location.href='/webtoon_wonjak'">
							<img src="/resources/image/plus.png" width="100%;" style="float: right;">
						</span> -->
						<a href="/webtoon_wonjak" class="fr cp" style=";margin-top:14px; font-size: 12px;">
							더보기+
						</a>
						<div class="w100">
						<ul class="ranking_border" style="clear:both;">
							<c:set var="j" value="1"/>
								<c:forEach var="wonjakrank" items="${wonjakRank }" end="4" step="1" >
									
										
										<c:choose>
											<c:when test="${j==1 }">
											<li class="cp" style="border:1px solid #ccc;clear:both;" onclick="location.href='/webtoon/${wonjakrank.getWtno()}'">
												<%-- <div class="rank1_imgbox" style="background: url('${wonjakrank.getThumbnail() }');">
													<!-- <img src="/resources/image/slider-gradient.png" style="width:inherit; height:150px;"> -->
													<div class="rank1_infobox">
														<p class="title">
															<span class="rank_num1">${j }</span>${wonjakrank.getWtname()}</p>
														<p class="synopsys">${wonjakrank.getWtcontent() }</p>
													</div>
												</div> --%>
												<div class="rank1_imgbox" style="background: url('${wonjakrank.getThumbnail() }');">
													<div class="rank1_infobox">
														<div class="re_rank_num1 fl">${j }</div>
														<div class="re_rank_textbox">
															<div class="re_rank_tit">${wonjakrank.getWtname()}</div>
															<div class="re_rank_synopsys">${wonjakrank.getWtcontent() }</div>
														</div>
													</div>
												</div>
											</li>	
											</c:when>
											<c:otherwise>
											<li class="cp" style="border-bottom:1px solid #ccc;clear:both;" onclick="location.href='/webtoon/${wonjakrank.getWtno()}'">
												<span class="rank_num rank_num_${j }">${j}</span>
												<div class="rank_thumb" style="background-image:url('${wonjakrank.getThumbnail() }');"></div>
												<div class="rank_infobox">
													<p class="title ranking_title_sub ">${wonjakrank.getWtname()}</p>
													<p class="synopsys" style="padding-left:1px;">${wonjakrank.getWtcontent() }</p>
												</div>
											</li>
											</c:otherwise>
										</c:choose>										
									<c:set var="j" value="${j+1 }"/>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="w18 episode ranking_AD">
						<h3 class="ranking-title ranking-ad">AD</h3>
						<div class="main-banner cp">
							<a href="http://markman.co.kr/" target="_blank">
								<img class="ranking_AD_pc" src="/resources/image/sidebanner/banner_markman.png" title="마크맨 바로가기" alt="마크맨 바로가기"/>
								<img class="ranking_AD_MB" src="/resources/image/sidebanner/banner_markman_MB.png" title="마크맨 바로가기" alt="마크맨 바로가기"/>
								<!-- <img class="ranking_AD_MB" src="/resources/image/sidebanner/banner_markman_MB.png" title="마크맨 바로가기" alt="마크맨 바로가기"/>
								<img class="ranking_AD_MB" src="/resources/image/sidebanner/banner_markman_MB.png" title="마크맨 바로가기" alt="마크맨 바로가기"/> -->
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>