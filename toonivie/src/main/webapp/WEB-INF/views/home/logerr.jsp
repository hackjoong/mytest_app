<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>인증번호발급</title>
<meta id="_csrf_parameter" name="_csrf_parameter" />
<meta id="_csrf" name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta id="_csrf_header" name="_csrf_header"	content="${_csrf.headerName}" />
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">

</head>
<style>
a{
color: #000; 
}
.container{
	clear:both;
	padding: 0;
	/* height: 73.4%; */
}
#findID, #findPW{
	margin-top: 20px;
}
#findID {
	width: 49%;
	float: left;
}
#findPW {
	width: 49%;
	float: right;
}
.findBtn { 
	float: right;
	margin-right: 8px;
}
.backBtn { 
	float: right;
	margin-right: 8px;
	margin-top: 25px;
}
</style>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<!-- 수정본2 -->
	<div class="w100 fl wrap logins re_min_hnone">
		<div class="login-form lft mt90 pb500 re_duqor re_bnone" style="height:500px;">
			<h3 class="login-title" style="font-size: 1.4em;">인증번호 발급</h3>
			<span class="login-subtitle re_log_subtitle">계정 활성화를 위해 인증번호를 발급받아야합니다.</span>
			<form name="form1" method="POST">
				<div class="form-group fl w100 act mt20">
					<label for="email" class="cols-sm-2 control-label fl" style="font-size:20px; margin-top:15px;">정보입력</label>
					<input type="text" name="certId" id="name" autocomplete="off" class="login-id lg-input" style="margin-top:5px;" placeholder="ID">
					<input type="text" name="email" id="email" class="login-id lg-input mmt5 m_login_mail" autocomplete="off" placeholder="E-mail">
					<input type="button" value="인증번호 전송" id="certNumsubmit" class="login-id lg-input orangebox m_login_mail bg_org">
				</div>
			</form>
			<form name="form1" method="POST">
				<div class="form-group fl w100 act">
					<label for="email" class="cols-sm-2 control-label fl " style="font-size:20px; margin-top:15px;">인증번호</label><br/><br/>
					<input type="hidden" name="certNum" value="TOONIVIE"> 
					<input type="hidden" name="certName">
					<input type="hidden" name="certId">  
					<input type="text" name="certinput" id="certNum" autocomplete="off" class="login-id lg-input" style="margin-top:5px; width:30%; float: left;" placeholder="인증번호입력">
					<input type="button" value="확인" class="login-id lg-input orangebox bg_org" id="cert_ok_btn" style="margin-top:5px; width:20%; float: left; border: 0;">
					<div id="ViewTimer" style="margin-top:13px;width:40%; float:right;"></div>
				</div>
			</form>
			<form name="findPW_cert" action="/signup/findPW" method="POST">
				<div class="form-group fl w100 act">
					<input type="hidden" name="id" id="certId">
					<input type="hidden" name="name" id="certName">  
				</div>
			</form>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
<script>		// 최초 설정 시간(기본 : 초)
var SetTime = 180;
function msg_time() {	// 1초씩 카운트

	m = Math.floor(SetTime / 60) + "분 " + (SetTime % 60)+"초";	// 남은 시간 계산
	var msg = m;
	document.all.ViewTimer.innerHTML = msg;		// div 영역에 보여줌 		
	SetTime--;					// 1초씩 감소
	if (SetTime < 0) {			// 시간이 종료 되었으면..
		clearInterval(tid);		// 타이머 해제
		$('[name=certNum]').val("TOONIVIE");
		SetTime = 180;
	}
}

$('#certNumsubmit').click(function(){
	var id = $('[name=certId]').val();
	var email = $('[name=email]').val();
	$.ajax({
		url:'/signup/certnum',
		type:'POST',
		data:{
			'id':id,
			'email':email
		},
		success:function(data){
			
			if(data.message == "null"){
				swal("일치하는 정보가 없습니다");
				$('[name=certId]').val("");
				$('[name=email]').val("");
			}
			else if(data.message == "TRUE"){
				swal("인증번호가 전송되었습니다");
				$('[name=certNum]').val(data.certNum);
				$('#certName').val(data.name);
				$('#certId').val(id);
				tid=setInterval('msg_time()',1000) 
			}
		},
		error:function(request,status,error){
        }
	})
})
$('#cert_ok_btn').click(function(){
	var bunho = $("[name=certNum]").val();
	var InputNum = $("[name=certinput]").val();
	if(bunho == "TOONIVIE" && $("#ViewTimer").text()=="0분 0초" ){
		swal("인증 유효시간이 만료되었습니다. 다시 인증번호를 발급받으세요.");
		$("[name=certinput]").val("");
		$("#ViewTimer").text("");
		clearInterval(tid);
	}
	else if(bunho == "TOONIVIE" && $("#ViewTimer").text()==""){
		swal("인증번호 발급 후 입력해주세요");
		$("[name=certinput]").val("");
	}
	else{
		$.ajax({
			url:'/signup/certview',
			type:'POST',
			data:{
				"certNum":bunho,
				"inputNum":InputNum
			},
			success:function(data){
				if(data == 1){
					swal("인증되었습니다. 등록하신 이메일로 임시비밀번호를 발송해드렸습니다.");
					clearInterval(tid);
					$("[name=findPW_cert]").submit();
				}
				else{
					swal("인증번호가 다릅니다.");
					$("[name=certinput]").val("");
				}
			}
		})
	}
})
</script>
</html>