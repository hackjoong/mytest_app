<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>로그인</title>
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
<div class="w100 fl mt90 logins wrap re_duqor">
	<div class="login-form pb150 re_bnone">		
		<h3 class="login-title">TOONIVIE</h3>
		<span class="login-subtitle m_service">TOONIVIE의 서비스를 이용할 수 있습니다.</span>
		<div class="login-subtitle mt20 login-failtitle" style="color: #f00; font-size:14px;"></div>
		<form action="/login.do" method="post" id="login_form1">
		<input type="hidden" id="login_fail_input" value="${login_fail_check }">	
			<input type="text" name="id" class="login-id lg-input" placeholder="아이디">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="비밀번호">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<button class="submits">로그인</button>
		</form>
		<div class="w100 fl joins">
			<button class="lg-input join-btn" onclick="location.href='/signup/auth'">회원가입</button>
			<span class="half-find"><a href="/signup/findAccount" style="color:#337ab7;">아이디 / 비밀번호 찾기</a></span>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script>
$(document).ready(function() {
	$(".login-id").focus();
	loginFail_check();
});
$("#login_form1").submit(function(){
	localStorage.setItem('id',$('[name=id]').val());
})
function loginFail_check(){
	var value = $("#login_fail_input").val();
	if(value=="t"|| value==null){
		$.ajax({
			url:'/findLoginerr',
			type:'post',
			data:{
				'id':localStorage.getItem('id')
			},
			success:function(data){
				if(data>=5){
					alert("로그인 5회 틀리셨습니다. 계정 인증후 이용해주세요.");
					location.href="/logerr";
				}
				if(data == -1){
					alert("사용자 정보가 없습니다");
				}
				else if(data<5){
					$('.login-failtitle').text("로그인 5회중"+data+"회 틀렸습니다.");
					alert("아이디와 비밀번호를 확인 해 주세요");	
				}
				
			}
		})
		
	}
}
</script>
</body>
</html>