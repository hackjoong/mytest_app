<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>



<title>login</title>
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/en/header_en.jsp" />
<div class="w100 fl wrap logins">
	<div class="login-form">		
		<h3 class="login-title">TOONIVIE</h3>
		<span class="login-subtitle">You can use the service of TOONIVIE.</span>
		<form action="/login.do" method="post">
			<input type="text" name="id" class="login-id lg-input" placeholder="ID">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="Password">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<input type="submit" class="submits" value="login">
		</form>
		<div class="w100 fl joins">
			<button class="lg-input join-btn" onclick="location.href='/signup/signup_en'">Register</button>
			<span class="half-find"><a href="/signup/findAccount">Find ID</a></span>
			<span class="half-find"><a href="/signup/findAccount">Find Password</a></span>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script>
$(document).ready(function() {
	$(".login-id").focus();
});
</script>
</body>
</html>