<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
<title>Login-FindPW</title>
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
<div class="w100 fl wrap logins mt90 re_duqor">
	<div class="login-form pb150 re_bnone">		
		<h3 class="login-title">TOONIVIE</h3>
		<span class="login-subtitle re_log_subtitle" style="color:red;">임시비밀번호가 발급되었습니다.<br>로그인 후 비밀번호를 변경해 주시기 바랍니다.</span>
		<form action="/login.do" method="post">	
			<input type="text" name="id" class="login-id lg-input mtop40" placeholder="아이디">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="비밀번호">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<button class="submits">로그인</button>
		</form>
		<div class="w100 fl joins">
			<button class="lg-input join-btn" onclick="location.href='/signup/auth'">회원가입</button>
			<span class="half-find"><a href="/signup/findAccount" style="color:#337ab7">아이디 / 비밀번호 찾기</a></span>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script>
$(document).ready(function() {
	$(".login-id").focus();
});
</script>
</body>
</html>