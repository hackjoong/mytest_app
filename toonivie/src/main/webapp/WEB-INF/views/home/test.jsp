<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="Expires" content="-1">
<META http-equiv="Pragma" content="no-cache">
<META http-equiv="Cache-Control" content="No-Cache">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />

<link href="/resources/common/css/jquery-ui.min.css" rel='stylesheet'>

<title>webcam streaming</title>

</head>

<style>
@charset "UTF-8";
.wrapper{
	display: grid;
	grid-template-columns:repeat(3,1fr);
	grid-gap:5px;
}
video, canvas, img{
	width: 400px;
	height: 400px;
	background-color: #000;
}
</style>
<body>
 <div class="wrapper">
  <div>
  <h1>Video from webCam</h1>
  <video ></video>
  </div>
  <div>
  <h1>Video on Canvas</h1>
  <canvas></canvas>
  </div>
  
  <div>
   <h1> Video from Server</h1>
  <img alt="" />
  </div>
 </div>
<script type="text/javascript">
(function(){
	var video = document.querySelector('video');
	var canvas = document.querySelector('canvas');
	var img = document.querySelector('img');
	var context=canvas.getContext('2d');
	var url = "ws://localhost:8088/wsServer";
	
	var socket = new WebSocket(url);
	
	socket.onopen=onOpen;
	function onOpen(event){
		
	}

	var constraints={
			video:true,
			audio:false
	};
	
	navigator.mediaDevices.getUserMedia(constraints).then(function(stream){
		video.srcObject=stream;
		video.play();
	}).catch(function(err){
		
	});

	 setInterval(main ,3000);
	
	
    function main(){
    	drawCanvas();
    	readCanvas();
    }
	
	function drawCanvas(){
		
		context.drawImage(video,0,0,canvas.width, canvas.height);
	}
	
	 console.log(canvas.toDataURL('image/jpeg',1));
	
	function readCanvas(){
		var canvasData = canvas.toDataURL('image/jpeg',1);
		var decodeAstring = atob(canvasData.split(',')[1]);
		
		var charArray =[];
		
		for(var i=0; i<decodeAstring.length;i++){
			
			charArray.push(decodeAstring.charCodeAt(i));
		}
		
       socket.send( new Blob([new Uint8Array(charArray)],{
    	   tpye:'image/jpeg'
       }));		
	
        socket.addEventListener('message',function(event){
        	img.src=window.URL.createObjectURL(event.data);
        });
		
	}	
})();
</script> 
</body>
</html>
