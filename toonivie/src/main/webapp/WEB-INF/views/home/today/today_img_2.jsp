<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
//$('.width ul').css("width",(total+1)*100+"%");
var timer;
var pos = 0;
var total;
 $(function(){
	 total = $('.width ul li').length-1;
	$(".width ul").css({"width":(total+1)*100+"%"});
	$(".width ul li").css({"width":100/(total+1)+"%"});
	timer = setTimeout(function(){
		slide("auto","");
	},3000);
});

function slide(type, num){
	clearTimeout(timer);
	type=="auto" ? pos++ : type=="side" ? pos+=num :pos=num;
	pos = pos >total ? 0: pos<0 ? total : pos;
	$(".slide>.width>ul").stop().animate({marginLeft:(pos*-100)+"%"},800)
	timer = setTimeout(function(){
		slide("auto","");
	},3000);
} 
</script>
<div class="w100 fl imgbox slide">
	<div class="width">
		<ul>
			<c:forEach items="${banList}" var="items">
				<li><a href="${items.getJuso() }"><img src="${items.getB_file() }"></a></li>
			</c:forEach>
		</ul>
	</div>
</div>

	