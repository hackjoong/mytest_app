<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl re h100">
	<div class="tabbable" id="tabs-57137">
		<ul class="nav nav-tabs re_nav_tabs w100">
			<li class="active re_color_tab" style="width: 50%; margin-bottom: -2px;">
				<a class='image_select_menu' data-menu_name="image_cut" href="#panel-33950" data-toggle="tab">이미지 분리</a>
			</li>
			<li class="re_color_tab" style="width: 50%;  margin-bottom: -2px;">
				<a class='image_select_menu' data-menu_name="psd_cut" href="#panel-902313" data-toggle="tab" style="margin-right: 0;">PSD 분리</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="panel-33950">
				<form id="uploadGrabcutImgFrom" action="/upload/uploadImg" enctype="multipart/form-data"> 
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<div class="w100 fl">				   				
					<div class="w100 fl mt5 re apd-zone">
						<span class="fl">이미지 분리</span>
						<span class="fr help" title="도움말">
							<a href="javascript:void(0);" onclick="javascript:tab1_img('kr');">
								<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
							</a>
						</span>
						<div class='img-frame' id="tab1_1st" style="padding-top:15px;">            	
			            	<img src='/resources/image/v2.0/img_upload.png' class='upload-img upload-img-cut re_upload_img_cut'>
			            	<input type='file' name='img' id='img' class='kinp upload-inp'>
			            	<video id="frame_preview" class="dn"  style="width: 100%; height: 100%;"  autoplay loop>
							  <source src="" type="video/mp4">
							</video>
			           	</div>
			           	<div style="width: 100%;text-align: center;height: 20px;">
			           		<span id="img_name"></span>
			           	</div>
			           	<div class="w50 fl bg_before_re" id="tab1_2nd">
				           	<button id="cut_img_ori" class="btn-ivory cut-img fl disabled" type="button" style="width:98%;" disabled="disabled" onclick="fileUploadGrabcutImg()">이미지 업로드</button>				           	
				           	<button id="frame_getset" class="btn-ivory fl disabled dn" type="button" style="width:98%;margin-top: 5px;" disabled="disabled" onclick="movie_color_change()">프레임 추출 하기</button>    
				        </div>
				        <div class="w50 fr bg_before_re" id="tab1_7nd">
				        	<button class="btn-ivory fr cut-img-add psh_braggable disabled" onclick="clearRect_re()" disabled="disabled" type="button" id="" style="width:98%;" >배경 저장하기</button>    		
				           	<button id="frame_go" class="btn-ivory fr dn" type="button" style="width:98%;margin-top: 5px;" onclick="movie_color_make()">프레임 색상 변환</button>        		
				        </div> 
			           	<div>
			           		<div class="tooni-head2_blur" style="margin-top: 10px; position: relative;">배경 색상 채우기
								<label class="switch" style="margin-left: 130px;">
									<div style="position: absolute; top: 1px; right: 35px; font-size: 0.85em; width: 100%;">BLUR </div>
									<input type="checkbox" class="jamakcheck">
									<span id="grabcut_blur_status" class="slider round"></span>
								</label>
								<img src="/resources/image/v2.0/blurSetBtn.png" title="blurOption" alt="blurOption" class="disabled" id="blurSetBnt" style="position: absolute; top: -2px; cursor: pointer; right: -2px;"/>
			           		</div>
			           	</div>
			           	<div class="w100 fl mb10" id="tab1_3rd">
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled" id="fill_black" disabled="disabled" onclick="set_filling_color(0)" src="/resources/image/v2.0/fill_black.png" alt="검정색채우기" title="검정색채우기"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">검정색채우기</div>
			           		</div>
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled" id="fill_white" disabled="disabled" onclick="set_filling_color(1)" src="/resources/image/v2.0/fill_white.png" alt="흰색채우기" title="흰색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">흰색채우기</div>  
			           		</div>
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled"  id="fill_surrounding" disabled="disabled" onclick="set_filling_color(2)" src="/resources/image/v2.0/fill_surrounding.png" alt="주변색채우기" title="주변색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px; ">주변색채우기</div>
			           		</div>
			           		<div class="w25 fl cursor_pointer" style="margin: 0 0 0 6%;">
			           			<img class="w100 cut-img disabled" id="grabcut_color_picker" disabled="disabled" style="margin-top: 10px; border-radius:10px;" src="/resources/image/v2.0/grabcut_color_picker.png" alt="지정색채우기" title="지정색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">지정색채우기</div>
			           		</div>
			           		<div class="w25 fl cursor_pointer" style="margin: 0 0 0 6%;">
			           			<img class="w100 cut-img disabled" id="fill_auto" disabled="disabled" onclick="set_filling_color(4)" style="margin-top: 10px; border-radius:10px;" src="/resources/image/v2.0/fill_auto.png" alt="자동채우기" title=""자동채우기"" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">자동채우기</div>
			           		</div>
		           			<div class="w25 fl cursor_pointer" style="margin: 0 0 0 6%;">
			           			<img class="w100 cut-img disabled" id="fill_color" disabled="disabled" onclick="set_filling_color(5), auto_coloring()" style="margin-top: 10px; border-radius:10px;" src="/resources/image/v2.0/auto_coloring.png" alt="자동채색" title="자동채색" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">자동채색</div>
			           		</div>
			           	</div>
			           	<div class="tooni-head2" style="margin-top: 10px;">이미지 분리 종류
           					<label class="switch">
				              <div style="position: absolute; top: 1px; right: 90px; font-size: 0.85em; width: 100%;">SaliencyCut </div>
				        	  <input type="checkbox" class="jamakcheck" unchecked>
				        	  <span id="SaliencyCut_status" class="slider round"></span>
				      		</label>
			           	</div>
			           	
			           	<div class="w100 fl" id="tab1_4th" style="margin-bottom: 10px;">
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="cutModeBtn1" disabled="disabled" onclick="setMode(0)" src="/resources/image/v2.0/cutModeBtn1.png" alt="이미지 분리 - 박스형" title="이미지 분리 - 박스형"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">박스형</div>
			           		</div>
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="cutModeBtn2" disabled="disabled" onclick="setMode(1)" src="/resources/image/v2.0/cutModeBtn2.png" alt="이미지 분리 - 자유형" title="이미지 분리 - 자유형" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">자유형</div>
			           		</div>
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="ObjectDetection" disabled="disabled" onclick="" src="/resources/image/v2.0/cutModeBtn3.png" alt="이미지 분리 - 객체인지" title="이미지 분리 - 객체인지"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">객체인지</div>
			           		</div>
			           	</div>
			           	<ul class="nav nav-tabs re_nav_tabs w100">
							<li class="active re_color_tab" id="tab1_5th" style="width: 50%; margin-bottom: -2px;">
								<a class='image_select_menu' data-menu_name="color_palette" href="" data-toggle="tab">컬러팔레트</a>
							</li>
							<li class="re_color_tab" id="tab1_6th" style="width: 50%;  margin-bottom: -2px;">
								<a class='image_select_menu' data-menu_name="color_correction" href="" data-toggle="tab" style="margin-right: 0;">색상보정(RGB)</a>
							</li>
						</ul>
						<div class="tab_color_palette" style="width: 100%; height: 150px;">
							<div class="w100 fl mt10 mb10">
					           	<button class="btn-ivory fl tab1_btn_color disabled color_change_btns" type="button" style="width:49%;" onclick="get_count()">색상추출</button>
					           	<button class="btn-gray fr disabled color_change_btns" id="change_color_trans" type="button"  style="width:49%;" >색상변경</button>
						        <button class="btn-ivory disabled fl color_change_btns" id="ROI_select" onclick='ROI_setting()' type="button" style="width:50%; margin-top: 5px;" >관심영역(ROI) 설정</button>      
					           	<div class="fl" style="margin-top:12px;">
					           		<span class="fl mr10">반전</span>
					           		<label class="switch switch_roi fl">
							           	<input id="roi_negative" type="checkbox" class="jamakcheck">  		
							           	<span id="" class="slider round"></span>
						           	</label>
					           	</div>
					           	
					        </div>
					        <div class="color_box w100" style="height: 100px;">
								<div class="color_box_text" style="line-height: 100px;">Color Palette</div>
							</div>
						</div>
			           	<div class="tab_color_correction dn" style="width: 100%; height: 150px;">
			           		<div class="color-correction" style="margin-top: 30px;">
					   			<div class="color-bar">
					   				<div class="color-bar-control" id="red"></div>
					   				<div class="color-bar-control" id="green"></div>
					   				<div class="color-bar-control" id="blue"></div>
					   			</div>
					   			<div class="w100 fl" style="color:#BBB;">
					   				<span class="fl ml5">0</span>
					   				<span class="fr mr5">255</span>
					   			</div>
							</div>
							<div class="w100 fl mt15">     		
					           	<button class="btn-cyan fl disabled " id="preview_btn" type="button" style="width:49%; border-radius:0;" onclick='changeColorExe(false)'>미리보기</button> 
					           	<button class="btn-cyan fr disabled " id="applc_btn" type="button" style="width:49%; border-radius:0;" onclick='changeColorExe(true)'>적용하기</button> 
					        </div>
					        <div class="w100 fl mt5">
					           	<button class="btn-cyan fr disabled " id="go_back" type="button" style="width:100%; border-radius:0;" onclick="getColorValueAvr(true)">원래대로</button>   
					        </div>
			           	</div>
					</div>								
				</div>
				</form>		  
			</div>
			<div class="tab-pane" id="panel-902313">
				<div class="menu-cont-title mt20">
				<span class="fr help" title="도움말" onclick="javascript:tab1_psd('kr');" style="padding-bottom:10px;">
					<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
				</span>
				</div> 	
				<div class="w100 fl">	
					<form id="upLoadPsdfileForm" action="/psdParse/psdUpload">			   				
						<div class="w100 fl mt15 re apd-zone">
							<div class='img-frame' id="tab1_psd_1st"> 
				            	<img id='upload_psd' src='/resources/image/toolicon_1.png' class='upload-img' data-uploaded='false'>
				            	<input type='file' name='psd' id='psd' class='kinp upload-inp inp"+i+"'>
				           	</div>
				           	
				           	<div id="psd_file_name" class="w100 fl mt15 file_Name_Psd">
				           	PSD Name
				           	</div>
				           	
				           	<div class="w100 fl mt5" id="tab1_psd_3rd">
				           		<button type="button" class="btn-cyan fl psd_Open disabled" style="width:100%;" onclick="fileUploadPsdParseImg()">PSD 레이어 분리하기</button>     		
				           	</div>
				           		<div class="menu-cont-title mt20">
								<span class="fl" style="margin-top:9px;">영상 사이즈 지정하기</span>	
							</div>
				            <div class="w100 fl mt5" id="tab1_psd_4th">
				           		<input type="button" class="videoSizeBtn btn-cyan fr mt5 videSizeSelect" style="width:50%;" value="1280*720">
				           		<input type="button" class="videoSizeBtn btn-cyan fr mt5" style="width:50%;" value="1920*1080">		
				           	</div>
				           	<div class="menu-cont-title mt20">
								<span class="fl" style="margin-top:9px;">레이어 이미지 분리하기</span>	
							</div>
				           	<div class="w100 fl mt5" id="tab1_psd_5th">
				           		<button type="button" id="crop_Close_btn" class="btn-gray fr disabled" style="width:49%;">끄기</button>    
				           		<button type="button" id="crop_Open_btn" class="btn-gray fl disabled" style="width:49%;">켜기</button>       		
				           	</div>
   					        <div class="w100 fl mt5" id="tab1_psd_6th">
				           		<button type="button" id="crop_btn" class="btn-cyan fl disabled" style="width:100%;" onclick="">분리할 이미지 저장하기</button>           		
				           	</div>
				           	
						</div>
					</form>
					<div class="menu-cont-title mt20 psh_dn">
						<span class="fl" style="margin-top:9px;">색상 보정 (RGB)</span>		
					</div> 	
					<div class="w100 fl psh_dn">				   						
						<div class="color-correction">
				   			<div class="color-bar for_psd">
				   				<div class="color-bar-control" id="red_psd"></div>
				   				<div class="color-bar-control" id="green_psd"></div>
				   				<div class="color-bar-control" id="blue_psd"></div>
				   			</div>
				   			<div class="w100 fl" style="color:#BBB;">
				   				<span class="fl ml5">0</span>
				   				<span class="fr mr5">255</span>
				   			</div>
						</div>
				        <div class="w100 fl mt5">
				           	<button class="btn-cyan fr" id="change_psd_color" type="button" style="width:100%;" onclick=''>적용</button> 
				        </div>
				         <div class="w100 fl mt5">
				           	<button class="btn-cyan fr" id="return_psd_color" onclick="" type="button" style="width:100%;" onclick=''>원래대로</button> 
				         </div>
					</div>		
				</div>	
			</div> 
		</div>	
	</div>
</div>
<style>
.btn-gray {
  border: none;
  background: #ccc;
  height: 30px;
  color: white;
  font-weight: 300;
  border-radius:5px;
}
</style>

<script>


$(".color-go").click(function(){
	$(".color-correction").removeClass("dn");
});
$(".videoSizeBtn").click(function(){
	$(".videoSizeBtn").removeClass("videSizeSelect");
	$(this).addClass("videSizeSelect");
	videoFrameSizeSelect();
});
$("#go-blur").click(function(){
	blurSigmaXV = $( "#blurSet" ).slider("value");
	popupOff($(".blur-set-modal"));	
});

$(".img-frame").click(function(){
	$(".psd_Open").removeClass("disabled");
});

function auto_coloring(){
	var randomId = new Date().getTime();
	var data = {};
	data["ori_background"] = ori_background;
	$.ajax({
		url : "/grabcut/coloring",
		dataType : "json",
		type : "POST",
		async: false,
		data : data,
		beforeSend:function(){
			loadingOn();
		},
		success : function(data) {
			setTimeout(function () {
				grabcutUrl=data.resultUrl;
				$("#canvas-before").attr("width",fixWidth);
				$("#canvas-before").attr("height",fixHeight);
				img_resize_rate_w = data.width / fixWidth;
				img_resize_rate_h = data.height / fixHeight;
				context.clearRect(0,0,1000,1000);
				$("#canvas-before").css({"background-image":"url('')"});
				$("#canvas-before").css({"background-image":"url("+data.resultUrl+"?v="+randomId+")"});
				$("#canvas-before").data("img_url","/resources/cvimgs/"+data.resultUrl);
				loadingOff();
			},1000);
		},
		error : function(request, status, error) {
			swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})
			loadingOff();
		}
	});
}

var frame_color = false;
var video_url;
function movie_color_change(){
	$('#uploadGrabcutImgFrom').ajaxForm({
 		url : "/ffmpeg_framepick",
		method:"POST",
		enctype: "multipart/form-data",
		async: false,
		beforeSend:function(){
		},
	    success : function(result) {
	    	frame_color = true;
	    	$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9, .wrap11").remove();

			$(".wrap_BG").addClass("dn");
			$(".saved-image-item").each(function(){
				$(this).attr("data-image","");
				$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			});												
			$(".save-bg").attr("data-image","");
			$(".canvas-append-zone").css("z-index","99");
			$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			listNum=1; 
			$("#canvas-after").css("background-image","url('')");
			$("#canvas-before").data("img_url","/resources/cvimgs/"+result.resultUrl);
			$("#canvas-before").attr("width",fixWidth);
			$("#canvas-before").attr("height",fixHeight);
			img_resize_rate_w = result.width / fixWidth;
			img_resize_rate_h = result.height / fixHeight;
			
			$("#canvas-before").css("background-size","960px 540px");
			$(".canvas-before").removeClass("dn");			
			$("#canvas-before").parent().removeClass("dn");
			img_url = "/resources/cvimgs/"+result.resultUrl;
			//init_color_picker();
			
			// 이미지분리종류
			$("#cutModeBtn1").attr("src","/resources/image/v2.0/cutModeBtn1_click.png");
			$("#cutModeBtn2").attr("src","/resources/image/v2.0/cutModeBtn2.png");
			// 배경색상채우기
			
			// get color palette by KH 2018-03-05
			color_filename = result.color_filename;
			put_color(result.color_palette);
			
			// ROI init by KH 18-03-12
			$('#ROI_select').removeClass('disabled');
			if(ROI_status == 'true' || ROI_status == 'using'){
				ROI_setting(); // ROI reset by KH 18-03-19
			}
			
			$('.color_change_btns').removeClass('disabled');
			
			$(".go-edit").addClass("dn")
			$(".go-next").removeClass("dn");
			$(".canvas-wrap").removeClass("dn");			
			grabcutUrl=result.resultUrl;

			//psd_c = document.getElementById("canvas_bg");
		    //psd_ctx = psd_c.getContext("2d");
		    //psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);
		    $(".second_saved_img").addClass("dn");
		    $("#layered_img_cropped").children().remove(); // 튀나온 박스 썸네일
		    $(".cropped_remove").remove(); // 드랍된 분리된 이미지
		    
		    $(".use-image-canvas-wrap").css("top",'25%');
		    
			$(".canvas-append-zone").removeClass('dn');
			psd_img_mode='false';
			itemList_Length=9; // 9로 지정하면 안됨! 수정 필요 by shkwak, 2018-03-13

			$('#cut_img_ori').html('이미지 다시 따기'); //이미지 재 업로드 -> 이미지 다시 따기
			$('.saved-image-item').attr('title', '');
			// data값 초기화
			$('.saved-image-item').data('image','');
			set_filling_color(0);
			setMode(0);
			
			// psh add 20180409 이미지 컷 초기화
			cropper_croped_status_wt = 'false'; // 크롭퍼 종료
			
			$(".re_img_upload_preview").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
			$("#wt_cut_save").text('이미지 업로드').addClass('disabled'); //이미지 분리 버튼
			$("#wt_whole_cropArea").cropper('destroy').removeAttr('src'); //크롭퍼 비활성화
			$('#wt_cut_saveName').val("");	
			$(".imgPreview_").show();
			$('#wt_cut_uploadCount').text("");
			$(".wtcut_box").children(".wt_cut_appendImg").remove();
			$("#wt_cut_save_all").addClass('disabled');
			
			//psh add 20180409 psd 초기화
			$("#canvas_bg").data("image","").css("background-image","url('')").addClass("dn"); // 스크롤되는 배경
			$(".psd_LPC_img").css("background",""); // 미리보기
			$(".psd_LPC_img").css("height","0");
			$("#upload_psd").attr("src","/resources/image/toolicon_1.png"); // psd 추가 이미지
			
			//for mask grabcut by KH 180426
			ori_background = result.resultUrl;
			//초기화
			canvas_index = [];	

			video_url = result.videoUrl;
	    	setTimeout(function () {
		    	loadingOff();
			},1000);
		},
	    error : function(request, status, error) {
	        swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})
	        loadingOff();
	    }
	});
	$("#uploadGrabcutImgFrom").submit();
	getColorValueAvr(false); // getColorValueAvr(); >> modify >> hoo 2018-10-04  
	savedBackImg = false; // 배경 저장 여부 초기화, add by shkak, 2017-11-09

}

function movie_color_make(){
	try{
		color_info.video_url = video_url;
		var info = JSON.stringify(color_info);
	}catch(err){
		swal("색상 변환을 해주세요.");
		return;
	}
	
	$.ajax({
		url : "/color_trans/color_change_frame",
		type : "POST",
		async: true,
		data : {
			'info' : info
		},
		beforeSend:function(){
			loadingOn();
		},
		success : function(data) {
			setTimeout(function () {
	    	$("#moveVideoResult").siblings('a').remove();
	    	$("#moveVideoResult").remove();
	    	$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data + "'></video>");
	    	popupOn($(".video-modal"));
	
			loadingOff();
			},1000);
		},
		error : function(request, status, error) {
			swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})
			loadingOff();
		}
	});
}

</script>