<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 h100 fl re">
<span class="fl">연출 효과</span>
<!-- add hoo 2018-10-26-->
<span id="transEffectName" id="tab2help1" class="fl" style="margin-left: 35px; text-align: left; cursor: pointer; color:blue;">리스트 보기</span> 
	<span class="help" id="tab2help2" title="연출방식2" onclick="javascript:tab2_2('kr');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/q2.png" style="width:10%; cursor:pointer; float: right; margin-bottom: 2px;">
	</span>			   				
	<span class="help" title="연출방식1" onclick="javascript:tab2('kr');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/q1.png" style="width:10%; cursor:pointer; float: right; margin-bottom: 2px;">
	</span>	
	<div class="w100 fl" id="tab2_1st">
		<div class="direction-list direction_list1 direction_list_color" data-direction="direction1" >
			<img class="direction_list_img" src="/resources/image/v2.0/effect_character.png" alt="" />
			<div class="direction_list_text">인물</div>	
		</div>
		<div class="direction-cont direction1">
			<div class="row">				
				
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-5.png)" 
						data-type="0" data-effecttype="fastMove" title="Fast Move Effect" data-ctgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">fast Move</div>
				</div>
				<div class="col-xs-4 not-allowed dn">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-5.png)" 
						data-type="0" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4 not-allowed dn">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-6.png)" 
						data-type="0" title="No Service"></div>
					<div class="effect_text" >No Service</div>
				</div>
				<div class="col-xs-4 not-allowed dn">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-7.png)" 
						data-type="1" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4 not-allowed dn">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-8.png)" 
						data-type="1" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4">
					<div id="tab2_2nd" class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-1.png)" 
						data-type="0" data-effecttype="walk" title="Walk Effect" data-ctgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Walk</div>
				</div>
				<div class="col-xs-4">
					<div id="tab22_3rd" class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-9.png)" 
						data-effecttype="11" title="Eye Blink" data-ctgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Eye Blink</div>
				</div>				
			</div>	
			<div class="row result-1 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-1">완료</button>
					</div>
				</div>
			</div>
		</div>	
		<div class="direction-list direction_list2" data-direction="direction2">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_objects.png" style="" alt="effect_objects" />
			<div class="direction_list_text">사물</div>
		</div>
		<div class="direction-cont direction2">
			<div class="row">								
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-1.png)" 
						data-type="0" data-effecttype="move" title="Move Effect" data-ojgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Move</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-2.png)" 
						data-effecttype="2" title="Smaller Effect" data-ojgif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Smaller</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-3.png)" 
						data-effecttype="3" title="Bigger Effect" data-ojgif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Bigger</div>
				</div>				
				<!-- <div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-4.png)" 
						data-effecttype="4" title="Rotation-Move Effect"></div>
				</div> -->
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-4.png)" 
						data-type="0" data-effecttype="rotateMove" data-ojgif="4" title="Rotation-Move Effect" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Move</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-5.png)" 
						data-effecttype="5" title="Rotation-Smaller Effect" data-ojgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Small</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-6.png)" 
						data-effecttype="6" title="Rotation-Bigger Effect" data-ojgif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Bigger</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-7.png)" 
						data-effecttype="7" title="Bigger-Smaller Effect" data-ojgif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Bigger-Smaller</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-8.png)" 
						data-effecttype="8" title="Rotation-Left Effect" data-ojgif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Left</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-9.png)" 
						data-effecttype="9" title="Rotation-Right Effect" data-ojgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Right</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-10.png)" 
						data-effecttype="10" title="Vibration Effect" data-ojgif="10" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Vibration</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-1.png)" 
						data-type="0" data-effecttype="afterImage-Move" title="Afterimage-Move Effect" data-ojgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Afterimage-Move</div>
				</div>
			</div>	
			<div class="row result-2 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-2">완료</button>
					</div>
				</div>
			</div>
			<div class="row result-1 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-1">완료</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list direction_list3" data-direction="direction3">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_background.png" alt="effect_background" />
			<div class="direction_list_text">배경</div>
		</div>
		<div class="direction-cont direction3"><!-- 구름,물결,연기,태풍,폭발,햇빛 -->
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-1.png)"
						 data-effecttype="3-1" title="Cloud Effect" data-bggif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cloud</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-2.png)"
						 data-effecttype="3-2" title="Wave Effect" data-bggif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Wave</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-3.png)"
						 data-effecttype="3-3" title="Smoke Effect" data-bggif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Smoke</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-4.png)"
						 data-effecttype="3-4" title="Typhoon Effect" data-bggif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Typhoon</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-5.png)"
						 data-effecttype="3-5" title="Explosion Effect 1" data-bggif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Explosion1</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-6.png)"
						 data-effecttype="3-6" title="Explosion Effect 2" data-bggif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Explosion2</div>
				</div>

				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-7.png)" 
						data-effecttype="3-7" title="Sunlight Effect" data-bggif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Sunlight</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-8.png)"
						 data-effecttype="3-8" title="Fire Effect" data-bggif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fire</div>
				</div>			
			</div>	
			<div class="row result-3 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">완료</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list direction_list4" data-direction="direction4">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_transitions.png" alt="effect_transitions" />
			<div class="direction_list_text">장면(신) 전환</div>
		</div>
		<div class="direction-cont direction4">
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-1.png)" 
						data-effecttype="t1" title="Fade-in Effect" data-trgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-in</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-2.png)" 
						data-effecttype="t2" title="Fade-out Effect 1" data-trgif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-out1</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-3.png)" 
						data-effecttype="t3" title="Fade-out Effect 2" data-trgif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-out2</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="t4" title="Ink Effect 1(Disppear)" data-trgif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Disppear)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-5.png)" 
						data-effecttype="t5" title="Ink Effect 2(Appear)" data-trgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Appear)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-6.png)" 
						data-effecttype="t6" title="Ink Effect 3(Center)" data-trgif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Center)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-7.png)" 
						data-effecttype="t7" title="Ink Effect(Wipe)" data-trgif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Wipe)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-8.png)" 
						data-effecttype="t8" title="Cover Effect 1(Left to Right)" data-trgif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cover(Left to Right)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-9.png)" 
						data-effecttype="t9" title="Cover Effect 2(Top to Down)" data-trgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cover(Top to Down)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-10.png)" 
						data-effecttype="t10" title="Blind Effect(Random)" data-trgif="10" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Blind(Random)</div>
				</div>
				<!-- <div class="col-xs-4 not-allowed">
					<div class="direction-item transform-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-11.png)" 
						data-effecttype="t11" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<!-- <div class="col-xs-4 not-allowed">
					<div class="direction-item transform-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-12.png)" 
						data-effecttype="t12" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->	
			</div>
			<div class="row result-4 dn">
				<div class="col-xs-12 result-btn-box dn" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">완료</button>
					</div>
				</div>
			</div>	
		</div>		
	</div>	
</div>
<script>

$("#transEffectName").click(function(){ // add hoo 2018-10-26 리스트 전환 
	if(!$(".direction-item").hasClass("dn")){
		$(".direction-list").css("padding","2px 0px").css("margin-bottom","5px");
		$(".direction_list_img").css("top","0px").css("width","30px");
		$(".direction-item").addClass("dn");
		$(".direction-cont").children(".row").children("div").removeClass("col-xs-4");
		$("#transEffectName").text("효과 보기");
	}else{
		$(".direction-list, .direction_list_img").removeAttr('style');
		$(".direction-item").removeClass("dn");
		$(".direction-cont").children(".row").children("div").addClass("col-xs-4");
		$("#transEffectName").text("리스트 보기");
	}
});

$(".effect_text").click(function(){ // add hoo 2018-10-26 텍스트 클릭 시 div 클릭
	$(this).parent("div").children(".human-effect").trigger("click");
});

var effectObjFlag=0;
var effectObjImgUrl;
var effectObjItem;


var startEndFlag=0;
var effectKind="";

/* 연출 - 인물연출/사물연출/배경연출/장면연출 타이틀 클릭 이벤트 */
$(".direction-list").click(function(){ //체크 OK
	var data = $(this).data("direction"); //direction1 ~ direction4
    var direction_num = data.substring(9);
	 if(!$(".direction-cont").is(":animated")){	//	 
		 if($("." + data).height() == 0){
			 $(".direction-cont").animate({maxHeight:"0", opacity:"0"}, 400);
			 $("." + data).animate({maxHeight:"600px", opacity:"1"}, 400); 
		 }else{
			 $("." + data).animate({maxHeight:"0", opacity:"0"}, 400);
		 }		 
	 }else{
		 return false		 
	 }
	 for(var i = 1; i < 5; i++){
			if(direction_num == i){
				$(".direction_list"+i).addClass('direction_list_color');
				
			}else{
				$(".direction_list"+i).removeClass('direction_list_color');
			}
		}
});

/* 인물연출 : 항목 클릭 이벤트 */
$(".human-effect").click(function(){

	var $this = $(this).data('type');
	effectKind= $(this).data('effecttype');	
	
	startEndFlag = 0;
	$(".effect-time").attr("value","1");
	popupOn($(".human-effect-modal"));	
	if($this == 0){
		$(".go-start").removeClass('dn')
		$(".go-target").addClass('dn');
	}else{
		$(".go-start").addClass('dn')
		$(".go-target").removeClass('dn');
	}	
});

/* 사물연출 : 항목 클릭 이벤트 */
$(".object-effect").click(function(){
	var data = $(this).data('effecttype');
	effectKind = data;
	effectObjFlag = 0;
	$(".object-effect-time").attr("value","3");
	popupOn($(".object-effect-modal"), data);
});

/* 사물연출 : 설정완료 클릭 이벤트 */
$(".go-object").click(function(){
	var regExp = /^[1-9]+$/;
	if(!regExp.test($('.object-effect-time').val())){
		swal("정수만 입력 가능합니다.");
		return;
	}
	effectTime = $(".object-effect-time").val();
	if(effectKind==1||effectKind==2||effectKind==3||effectKind==4||effectKind==5||
			effectKind==6||effectKind==7||effectKind==8||effectKind==9||effectKind==10||effectKind==11 ){ //modify by shkwak, 2017-09-17, 항목 추가 //effectkind 11항목 추가
		swal("효과를 적용시킬 이미지를 '분리된 이미지'탭에서 선택 후 효과 적용하기 버튼을 클릭해주세요.");
		effectObjFlag = 1;
		popupOff($(".object-effect-modal"));	
	}	
});

$('#layered_img').on('click', '.saved-image-item-image',function() {
	if(effectObjFlag==1){
		if(psd_img_mode == 'true'){
			return;
		}
		effectObjImgUrl=$(this).data("url");
		var index;
		for(var i=1;i<itemList_Length+1;i++){
			if($(this).hasClass("itemNum"+i)){
				index=i;
			}	
		}		
		var offset = $(".canvas-code"+index).offset();
		var top = offset.top;
		var left = offset.left;
		var bOffset = $(".canvas-codeBG").offset();
		var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
		var bTop = bOffset.top;
		var bLeft = bOffset.left;
		var target_wrap = $(this).parent().data('target');
 		var ori_matrix = $("."+target_wrap).css("transform");
 		// 여기 변경. 리스트<오브젝트> 제작후 효과들 저장. 호과적용후 리셋
		MultiObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, index);
	}
	effectObjFlag = 0;
});
$('#layered_img_cropped').on('click', '.saved-image-item-image',function() {
	if(effectObjFlag==1){
		effectObjImgUrl=$(this).data("url");
		var index;
		for(var i=1;i<itemList_Length+1;i++){
			if($(this).hasClass("append_itemNum"+i)){
				index=i;
			}	
		}		
		var offset = $(".canvas-code"+index).offset();
		var top = offset.top;
		var left = offset.left;
		var bOffset = $(".canvas-codeBG").offset();
		if(psd_img_mode=='true'){
			bImgUrl = psd_layer_cut_imgurl;
			bTop = bOffset.top;
			bLeft = bOffset.left;
		}
		var target_wrap = $(this).parent().data('target');//add by KH, 2017-12-15
 		var ori_matrix = $("."+target_wrap).css("transform");//add by KH, 2017-12-15
		//effectObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, index); //tab2.jsp:216 - effectObjexeGo()
 		MultiObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, index);
	}
		
	effectObjFlag = 0;
});

//rotate후 변환 행렬에서 각도 뽑아내기
function mtod(matrix){ 
	if(matrix == "none"){ //각도 조절이 없을경우 
		return 0;
	}
	var values = matrix.split('(')[1];
    values = values.split(')')[0];
    values = values.split(',');
	var a = values[0];
	var b = values[1];
	var c = values[2];
	var d = values[3];
	
	var scale = Math.sqrt(a*a + b*b);
	
	// arc sin, convert from radians to degrees, round
	// DO NOT USE: see update below
	var sin = b/scale;
	var angle = Math.round(Math.asin(sin) * (180/Math.PI));
	
	
	return angle;
}

var btop_start_position;
function effectObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, obj_index){ //effectObjexeGo
	var jsonString ="";
	var obj_wh = ori2obj_resizing(obj_index);
	if(psd_img_mode == 'true'){
 		top = (top/shrinking_ratio_h - (btop_start_position - bTop)/shrinking_ratio_h);
		left = left/shrinking_ratio_w;
		bTop = bTop/shrinking_ratio_h;
		bLeft = bLeft/shrinking_ratio_w; 
	}else{
		top = Math.round(top * img_resize_rate_h);
		left = Math.round(left * img_resize_rate_w);
		bTop = Math.round(bTop * img_resize_rate_h);
		bLeft = Math.round(bLeft * img_resize_rate_w);
	}
	jsonString+="{ \"effectKind\":\""+effectKind+"\",";
	jsonString+="\"effectTime\":"+effectTime+",";
	jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
	jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"degree\":"+0+"},";
	jsonString+="\"otherObj\":[";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			obj_wh = ori2obj_resizing(canvas_index[i-1]);
			jsonString +="{";	
			var offset_obj = $(".canvas-code"+canvas_index[i-1]).offset();
			var top_obj = offset_obj.top;
			var left_obj = offset_obj.left;
			if(psd_img_mode == 'true'){

 				top_obj = (top_obj/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop));
				left_obj = offset_obj.left/shrinking_ratio_w; 
			}else{
				top_obj = Math.round(offset_obj.top * img_resize_rate_h);
				left_obj = Math.round(offset_obj.left * img_resize_rate_w);
			}
			var imgUrl=$(".canvas-code"+canvas_index[i-1]).data("image");
			var degree= 0;
			jsonString += "\"top\":"+top_obj+",\"left\":"+left_obj+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"degree\":"+degree+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	var data={};
	data["data"]=jsonString;
	$.ajax({
	       url : "/applyObject/getApply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async : false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})

	       }
	});
}

/* 배경연출 : 항목 클릭 이벤트 */ //add by shkwak, 2017-07-14
$(".state-effect").click(function(){
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	popupOn($(".transform-effect-modal"), data);
});

/* 장면연출 : 항목 클릭 이벤트 */
$(".transform-effect").click(function(){
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	popupOn($(".transform-effect-modal"), data);
});

/* 템플릿, 장면(신)연출 : 효과 적용 */
$(".go-transform").click(function(){ //index.jsp:115 - 설정 완료 클릭
	organize_level();
	var bOffset = $(".canvas-codeBG").offset(); //캔버스의 위치 반환
	var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image')); 
	var bTop = bOffset.top; //top: 298.5
	var bLeft = bOffset.left; //left: 475.75
	var obj_wh;
	if(psd_img_mode == 'true'){
		bImgUrl = psd_layer_cut_imgurl;
		bTop = bOffset.top/shrinking_ratio_h;
		bLeft = bOffset.left/shrinking_ratio_w;
	}else{
		bTop = Math.round(bTop * img_resize_rate_h);
		bLeft = Math.round(bLeft * img_resize_rate_w);
	}
	var jsonString ="";
	jsonString+="{ \"effectKind\":\"" + effectKind + "\",";
	jsonString+="\"effectTime\":" + $(".transform-effect-time").val() + ",";
	jsonString+="\"background\":{\"imgUrl\":\"" + bImgUrl + "\",\"top\":" + bTop + ",\"left\":" + bLeft+"},";
	jsonString+="\"obj\":[";
	var count = 0;
	for(var i=1;i<10;i++){ //for문 돌리는 이유? => 상단 분리된 이미지(분리된) 개수가 9개 + 배경 1개
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){ //for문을 돌려서 분리된 객체가 배경에 위치한 정보를 추가한다.
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			obj_wh = ori2obj_resizing(canvas_index[i-1]);
			jsonString +="{";	
			var offset = $(".canvas-code"+canvas_index[i-1]).offset();
			var top = offset.top;
			var left = offset.left;
			if(psd_img_mode == 'true'){
				top = (top/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop));
				left = offset.left/shrinking_ratio_w;
			}else{
				top = Math.round(top * img_resize_rate_h);
				left = Math.round(left * img_resize_rate_w);
			}
			var imgUrl = $(".canvas-code"+canvas_index[i-1]).data("image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	popupOff($(".transform-effect-modal"));
	var data={};
	data["data"]=jsonString;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content"); //csrf??
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content"); //THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
		url : "/sceneApply/apply",
	    dataType : "json", 	//서버에서 반환되는 데이터 형식 지정
	    type : "POST",		//method 옵션의 또다른 이름, http 전송방식 설정
	    data : data, 		//key, value pairs를 통해 서버로 parameter를 전달
	    async : false, 		//동기방식 사용
	    timeout : 300000,	//제한시간 설정
	    success : function(data) { //통신이 성공하면 호출되는 함수
	    	var randomId = new Date().getTime();
	    	insertVideoTab(data);
	    	$("#moveVideoResult").remove();
	    	$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl + "'></video>");
	    	popupOn($(".video-modal"));
		},
	    error : function(request, status, error) { //통신 중 실패했을 경우 호출되는 Event
			swal({
  				title: 'Error!',
  				text: "code:" + request.status + "\n" + "error:" + error,
  				type: 'error',
			})
		}
	});
})



var imgEffectList={};
$(".go-start").click(function(){
	var regExp = /^[1-9]+$/;
	if(!regExp.test($('.effect-time').val())){
		swal("정수만 입력 가능합니다.");
		return;
	}
	popupOff($(".effect-modal"));
	effectTime = $(".effect-time").val();
	swal("분리된 이미지의 시작 위치를 지정해주세요.");
	$(".result-1").removeClass("dn");
	
});

$(".go-target").click(function(){
	popupOff($(".effect-modal"));
	effectTime=$(".effect-time").val();
	effectKind="rotate";
	swal("효과를 지정 할 대상을 분리된 이미지 탭에서 설정해주세요.");
	$(".result-2").removeClass("dn");
});

$('#layered_img').on('click', '.draggg',function() {
	$(".draggg").removeClass('select-effect');
	$(this).addClass("select-effect");
});

$(".result-btn-1").click(function(){
	if(startEndFlag==0){
		organize_level();
		setFirstPosition_new(selected_element);
		swal("분리된 이미지의 마지막 위치를 지정해주세요.");
		startEndFlag++;
	}else{
		$(".result-1").addClass("dn");
		setLastPosition_new(selected_element);
	}
});

function setBackgroundPosition(){
	var backgourndOffset = $(".canvas-codeBG").offset();
	if(psd_img_mode == 'true'){
		var top = backgourndOffset.top/shrinking_ratio_h;
		var left = backgourndOffset.left/shrinking_ratio_w;
		bImgUrl = psd_layer_cut_imgurl;
	}else{
		var top = backgourndOffset.top * img_resize_rate_h;
		var left = backgourndOffset.left * img_resize_rate_w;
		bImgUrl = $(".canvas-codeBG").data("image");
	}
	data+="\"background\" : {"+
	"\"imgUrl\":\""+$(".canvas-codeBG").attr("data-image")+"\","+ 
	"\"top\":"+top+","+
	"\"left\":"+left+""+
	"}}";
	moveObject(data);
}	

var background_postion = {};
function setBackgroundPosition_new(){
	var background = {};
	var others = [];
	var other_objs = {};
	var other = [];
	var otherObj_content = {};
	var backgourndOffset = $(".canvas-codeBG").offset();
	var btop;
	var bleft;
	if(psd_img_mode == 'true'){
		btop = backgourndOffset.top/shrinking_ratio_h;
		bleft = backgourndOffset.left/shrinking_ratio_w;
		bImgUrl = psd_layer_cut_imgurl;
	}else{
		btop = backgourndOffset.top * img_resize_rate_h;
		bleft = backgourndOffset.left * img_resize_rate_w;
		bImgUrl = $(".canvas-codeBG").data("image");
	}
	
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			otherObj_content = {};
			obj_wh = ori2obj_resizing(canvas_index[i-1]);
			var offset_obj = $(".canvas-code"+canvas_index[i-1]).offset();
			var top_obj = offset_obj.top;
			var left_obj = offset_obj.left;
			if(psd_img_mode == 'true'){
 				top_obj = (top_obj/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - btop));
				left_obj = offset_obj.left/shrinking_ratio_w; 
			}else{
				top_obj = Math.round(offset_obj.top * img_resize_rate_h);
				left_obj = Math.round(offset_obj.left * img_resize_rate_w);
			}
			var imgUrl=$(".canvas-code"+canvas_index[i-1]).data("image");
			var degree= 0;
			otherObj_content["top"] = top_obj;
			otherObj_content["left"] = left_obj;
			otherObj_content["height"] = obj_wh.height;
			otherObj_content["width"] = obj_wh.width;
			otherObj_content["degree"] = degree;
			otherObj_content["imgUrl"] = imgUrl;

			other.push(otherObj_content);
			
		}
	}
	//background["imgUrl"] = $(".canvas-codeBG").attr("data-image");
	background["imgUrl"] = bImgUrl;
	background["top"] = btop;
	background["left"] = bleft;
	
	var data = {};
	data["background"] = background;
	data["firstInfo"] = firstInfo;
	data["lastInfo"] = lastInfo;
	data["others"] = other;
	
	$('.saved-image-item-image ').css({"border-color": "", 
        "border-weight":"", 
        "border-style":""});
	
	return data;	
}
	
var data="";
function setFirstPosition(){
	var obj_wh;
	var bTop = $(".canvas-codeBG").offset().top;
	data="";
	data+="{";
	var firstInfo = new Array();
	data+="\"firstInfo\" : [";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			obj_wh = ori2obj_resizing(canvas_index[i-1]);
			var appendString ="{";	
			var offset = $(".canvas-code"+canvas_index[i-1]).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop/shrinking_ratio_h));
				left = offset.left/shrinking_ratio_w;
			}else{
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+canvas_index[i-1]).data("image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
		}
	}	
	data +="],";
}
	
var firstInfo = new Array();
function setFirstPosition_new(){
	var bTop = $(".canvas-codeBG").offset().top;
	for(var i=1;i<itemList_Length+1;i++){
		var FirstPosition = {};
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			var obj_wh = ori2obj_resizing(this.canvas_index[i-1]);
			var offset = $(".canvas-code"+this.canvas_index[i-1]).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop/shrinking_ratio_h));
				left = offset.left/shrinking_ratio_w;
			}else{
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+this.canvas_index[i-1]).data("image");
			FirstPosition["top"] = top;
			FirstPosition["left"] = left;
			FirstPosition["height"] = obj_wh.height;
			FirstPosition["width"] = obj_wh.width;
			FirstPosition["imgUrl"] = imgUrl;
			FirstPosition["effectKind"] = effectKind;
			FirstPosition["effectTime"] = effectTime;
			firstInfo.push(FirstPosition);
		}
	}
}
	
function setLastPosition(){
	var bTop = $(".canvas-codeBG").offset().top;
	var lastInfo = new Array();
	data+="\"lastInfo\" : [";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			var obj_wh = ori2obj_resizing(canvas_index[i-1]);			
			var appendString ="{";	
			var offset = $(".canvas-code"+canvas_index[i-1]).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop/shrinking_ratio_h));
				left = offset.left/shrinking_ratio_w;
			}else{
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+canvas_index[i-1]).data("image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
	}
}	
	data +="],";
	data +="\"effectKind\":\""+effectKind+"\",";
	data +="\"effectTime\":\""+effectTime+"\",";
}

var lastInfo = new Array();

function setLastPosition_new(){
	var bTop = $(".canvas-codeBG").offset().top;
	for(var i=1;i<itemList_Length+1;i++){
		var LastPosition = {};
		if($(".canvas-code"+canvas_index[i-1]).length>0){
			var obj_wh = ori2obj_resizing(this.canvas_index[i-1]);			
			var appendString ="{";	
			var offset = $(".canvas-code"+this.canvas_index[i-1]).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio_h - ((btop_start_position/shrinking_ratio_h) - bTop/shrinking_ratio_h));
				left = offset.left/shrinking_ratio_w;
			}else{
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+this.canvas_index[i-1]).data("image");
			LastPosition["top"] = top;
			LastPosition["left"] = left;
			LastPosition["height"] = obj_wh.height;
			LastPosition["width"] = obj_wh.width;
			LastPosition["imgUrl"] = imgUrl;
			LastPosition["effectKind"] = effectKind;
			LastPosition["effectTime"] = effectTime;
			var exit_loop = false;
			for(var j=0;j<firstInfo.length;j++){
				var item = firstInfo[j];
				if(item["top"] == top && item["left"] == left){
					exit_loop = true;
					firstInfo.splice(j, 1);
					
				}
			}
			if(exit_loop){
				continue;
			}
			lastInfo.push(LastPosition);
		}
	}	
		
	for(var u=1;u<itemList_Length+1;u++){
		if($(".canvas-code"+canvas_index[u-1]).length>0){
			for(var j=0;j<lastInfo.length;j++){
				var offset = $(".canvas-code"+(canvas_index[u-1])).offset();
				var item = lastInfo[j];
					
				
				if(item["top"] == offset.top && item["left"] == offset.left){
					if(psd_img_mode == 'true'){
						$('.append_itemNum'+(canvas_index[u-1])).css({"border-color": "#20beca", 
					        "border-weight":"1px", 
					        "border-style":"solid"});
					}else{
						$('.itemNum'+(canvas_index[u-1])).css({"border-color": "#20beca", 
					        "border-weight":"1px", 
					        "border-style":"solid"});
					}
					
				}
				
			}
		}
	}
}
//check
function removeUrl(imgUrl){
	imgUrl = imgUrl.replace("url(\"","");
	imgUrl = imgUrl.replace("\")","");
	return imgUrl;
}

//resizing
function ori2obj_resizing(canvas_code){
	
	if(psd_img_mode == 'true'){
		var result = {width: 0, height: 0};
		return result;
	}
	var a_width = ($(".wrap"+canvas_code).css('width').replace('px',''));
	var a_height = ($(".wrap"+canvas_code).css('height').replace('px',''));
	try{
		a_width = a_width * img_resize_rate_w;
		a_height = a_height * img_resize_rate_h;
	}catch(err){
	}
		
	var result = {width: a_width, height: a_height};
	return result;
	
	
}
</script>