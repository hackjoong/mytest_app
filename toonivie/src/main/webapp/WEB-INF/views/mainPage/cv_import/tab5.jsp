<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re p15">	
	<div class="menu-cont-title"><!-- mt20 -->
		<span class="fl" style="margin-top:9px;">텍스트</span>
		<span class="fr help" title="도움말" onclick="javascript:tab5('kr');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
		</span>		
	</div> 	
	<div class="w100 fl" id="tab5_1st">	
		<div class="w100 fl mt15 re apd-zone">
           	<div class="w100 fl tooni-title">
				<div class="input_title">웹툰무비 텍스트 입력</div>
				<div class="tooni-cont2" id="tab5_2nd" style="background">
					<textarea rows="8" cols="30" id="subtitle_data" placeholder="텍스트"></textarea>
				</div>
			</div>
           	<div class="w100 fl mt15">
           		<div class="input_title">텍스트 색상</div>
            	<select id="subtitle_color" class="input_text">
				    <option value="#000000" style="background:black">검정</option>
				    <option value="#ff0000" style="background:red">빨강</option>
				    <option value="#ffff00" style="background:yellow">노랑</option>
				    <option value="#ffffff" style="background:white">흰색</option>
				    <option value="#0000ff" style="background:blue">파랑</option>
				</select>
           		<div class="input_title mt15">텍스트 폰트</div>
            	<select id="subtitle_font" class="input_text">
				    <option value="Binggrae.ttf">Binggrae</option>
				    <option value="Binggrae-Bold.ttf">Binggrae-Bold</option>
				    <option value="BinggraeⅡ.ttf">BinggraeⅡ</option>
				    <option value="BinggraeⅡ-Bold.ttf">BinggraeⅡ-Bold</option>
				    <option value="gabia_bombaram.ttf">gabia_bombaram</option>
				    <option value="gabia_napjakBlock.ttf">gabia_napjakBlock</option>
				    <option value="gabia_solmee.ttf">gabia_solmee</option>
				    <option value="KCC-eunyoung.ttf">KCC-eunyoung</option>
				    <option value="SDSwaggerTTF.ttf">SDSwaggerTTF</option>
				    <option value="TmonMonsori.ttf">TmonMonsori</option>
				    <option value="TmonTium.ttf">TmonTium</option>
				    <option value="tvNBold.ttf">tvN즐거운이야기 Bold</option>
				    <option value="tvNLight.ttf">tvN즐거운이야기 Light</option>
				    <option value="tvNMedium.ttf">tvN즐거운이야기 Medium</option>
				    <option value="YaBold.ttf">야놀자 야체 Bold</option>
				    <option value="YaRegular.ttf">야놀자 야체 Regular</option>
				</select>      	
           		<div class="input_title mt15">텍스트 크기</div>
            	<select id="subtitle_size" class="input_text">
					<c:forEach var="num" begin="14" end="50" step="2">
						<c:choose>
							<c:when test = "${num eq '24'}">
								<option selected="selected" value="${num}pt">${num}pt</option>
							</c:when>
							<c:otherwise>
								<option value="${num}pt">${num}pt</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>  	
           	</div>
           	<div class="w100 fl mt15" id="tab5_6th">
           		<button type="button" class="btn-cyan fl set-subtitle" style="width:100%;">텍스트 넣기</button>           		
           	</div>
           	<div class="w100 fl mt15" id="tab5_7th">
           		<button type="button" class="btn-cyan fl subtitle-effect-item" style="width:100%;">완료</button>           		
           	</div>
		</div>										
	</div>	
</div>
<script>
var subtitle_data;
var canvas_txt;
var ctx_txt;
var text_url;

var font_size;
var font_type;
var font_txt;
var font_color_r;
var font_color_g;
var font_color_b;
var font_bold;


	$(".set-subtitle").click(function(){
		$(".wrap11").remove();
		$("#wrap11").remove();
		if($("#subtitle_data").val() == ""){
			return;
		}
		subtitle_data = $("#subtitle_data").val();
		var subtitle_length = (subtitle_data.length * $("#subtitle_size").val().replace('pt',''))+40;
		
		var element = "" +
		"<div class=\"use-image-canvas-wrap wrap" + "11" + "\" style='width:" + subtitle_length + "px;height:" + "100" + "px'>" +
		"<div class=\"w100 fl h100\">" +
		"<canvas id=\"wrap11\" class=\" canvas-code11  \"" + "style='border: 1px solid #e9e9e9;' ></canvas>" +
		"</div>" +
		"</div>";
		
		$(".canvas-append-zone").append(element);
		canvas_txt = document.getElementById('wrap11');
		ctx_txt = canvas_txt.getContext("2d");
		offsetX = canvas_txt.offsetLeft;
		offsetY = canvas_txt.offsetTop;
		ctx_txt.textBaseline = "top";
		ctx_txt.font = "normal " + $("#subtitle_size").val() + " " + $('#subtitle_font').val().replace('.ttf','');
		ctx_txt.fillStyle = $("#subtitle_color").val();
		ctx_txt.fillText(subtitle_data, 0, 0,  ctx_txt.measureText(subtitle_data).width);
		canvas_txt.width = ctx_txt.measureText(subtitle_data).width;
		canvas_txt.height = $("#subtitle_size").val().replace('pt','')*2;
		offsetX = canvas_txt.offsetLeft;
		offsetY = canvas_txt.offsetTop;
		ctx_txt.textBaseline = "top";
		ctx_txt.font = "normal " + $("#subtitle_size").val() + " " + $('#subtitle_font').val().replace('.ttf','');
		ctx_txt.fillStyle = $("#subtitle_color").val();
		ctx_txt.fillText(subtitle_data, 0, 0,  ctx_txt.measureText(subtitle_data).width);
		
		$(".wrap11").draggable({
			disabled : false
		});
		
		//변수 설정 
		font_type = $('#subtitle_font').val();
		font_size = parseInt($("#subtitle_size").val().replace('.pt',''));
		font_txt = $("#subtitle_data").val();
		font_color_r = hexToRgb($("#subtitle_color").val()).r;
		font_color_g = hexToRgb($("#subtitle_color").val()).g;
		font_color_b = hexToRgb($("#subtitle_color").val()).b;
		font_bold = -1;
		
		$('.wrap11').width(ctx_txt.measureText(subtitle_data).width);
		$('.wrap11').height($("#subtitle_size").val().replace('pt','')*2);
		//canvas2dataurl();
		$(".wrap11").css("top",'35%');
	});
	
	$(".subtitle-effect-item").click(function(){
 		var data = $(this).data('effecttype');
 		
 		titleName = $("#subtitle_data").val();
 		
		effectKind = data;
		$(".subtitle-effect-time").attr("value","2");
		popupOn($(".subtitle-effect-modal"), data);
	});
	
	$(".insert-subtitle").click(function(){
		//111111
		var effectKind = '';
		var effectTime = $(".subtitle-effect-time").val();
		var bOffset = $(".canvas-codeBG").offset(); //캔버스의 위치 반환
		var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image')); //removeUrl() : url 좌우 필요없는 내용 제거
		var bTop = bOffset.top; //top: 298.5
		var bLeft = bOffset.left; //left: 475.75
		var offset_obj = $(".wrap11").offset();
		var top = offset_obj.top;
		var left = offset_obj.left;
		
		bTop = Math.round(bTop * img_resize_rate_h)
		bLeft = Math.round(bLeft * img_resize_rate_w)
		top = Math.round(top * img_resize_rate_h)
		left = Math.round(left * img_resize_rate_w)
		
		font_size = Math.round(font_size * img_resize_rate_h);
		
		var effectObjImgUrl = 'undefined';
		var degree = '0';
		var obj_wh;
		
		var jsonString ="";
		jsonString+="{ \"effectKind\":\""+effectKind+"\",";
		jsonString+="\"effectTime\":"+effectTime+",";
		jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
		jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"degree\":"+degree+",\"height\":\""+0+"\",\"width\":\""+0+"\",\"font_type\":\""+font_type+"\",\"font_size\":\""+font_size+"\",\"font_txt\":\""+font_txt+"\",\"font_color_r\":\""+font_color_r+"\",\"font_color_g\":\""+font_color_g+"\",\"font_color_b\":\""+font_color_b+"\",\"font_bold\":\""+font_bold+"\",\"text_url\":\""+text_url+"\"},";
		jsonString+="\"otherObj\":[";
		var count = 0;
		for(var i=1;i<10;i++){
			if($(".canvas-code"+i).length>0){
				count++;	
			}		
		}
		var forCount=1;
		for(var i=1;i<10;i++){
			if($(".canvas-code"+i).length>0){
				jsonString +="{";	
				obj_wh = ori2obj_resizing(i);
				var offset = $(".canvas-code"+i).offset();
				var top = offset.top;
				var left = offset.left;
				var imgUrl=$(".canvas-code"+i).data("image");
				var degree= mtod($(".wrap"+i).css("transform"));
				jsonString += "\"top\":"+top+",\"left\":"+left+",\"degree\":"+degree+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
				if(forCount<count){
					jsonString+=",";
				}
				forCount++;
			}
		}
		jsonString+="]}";
		var data={};
		data["data"]=jsonString;
		$.ajax({
		       url : "/addText/Apply",
		       dataType : "json",
		       type : "POST",
		       data : data,
		       async : false,
		       success : function(data) {
		    	   var randomId = new Date().getTime();
		    	   insertVideoTab(data);
		    	   $("#moveVideoResult").siblings('a').remove();
		    	   $("#moveVideoResult").remove();
		    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
		    	   popupOn($(".video-modal"));
		       },
		       error : function(request, status, error) {
		          swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					})
		       }
		});
	});
	//by KH 180607
	function hexToRgb(hex) {
	    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    return result ? {
	        r: parseInt(result[1], 16),
	        g: parseInt(result[2], 16),
	        b: parseInt(result[3], 16)
	    } : null;
	}
	
</script>