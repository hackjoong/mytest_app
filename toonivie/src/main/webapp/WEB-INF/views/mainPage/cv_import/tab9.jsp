<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="/resources/common/css/introjs.css">
<div class="w100 fl h100 re">
	<div class="w100 fl scroll-pd">
		<div class="menu-cont-title">
			<span class="fl">이미지 컷</span>
			<span class="fr help" title="도움말" onclick="tab9('kr')"><img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;"></span>
		</div>
	</div>
	<div class="tab-pane" id="panel-333333">
		<form id="fileUploadWebtoonImg" action="/upload/uploadImg_wt" enctype="multipart/form-data"> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
		<div class="w90 fl ml15">				   				
			<div class="w100 fl re apd-zone mt10" id="tab9_1st">
				<div class='img-frame'>            	
	            	<img src='/resources/image/v2.0/webtoon_upload.png' id='wt_image_view' class='upload-img upload-img-cut re_img_upload_preview'>
	            	<input type='file' name='wt_image' id='wt_image' class='kinp upload-inp' accept='image/gif, image/jpeg, image/png'>
	           	</div>
	           	<!-- add hoo 2018-10-17 -->
	           	<div style="width: 100%;text-align: center;height: 20px;">
	           		<span id="wt_image_name"></span>
	           	</div>
			</div>
			<!-- 추출된 웹툰 컷 출력 -->
	        <div class="w100 fl wt_cut_before" style="margin-top:10px">
	           	<button class=" fr btn-cyan disabled" type="button" id='wt_cut_save' style="width:100%;" >이미지 업로드</button> 
     	    </div>
     	    <div class="menu-cont-title mt10">
				<span class="fl">이미지 컷 미리보기</span>
				<span class="fr" id='wt_cut_uploadCount'></span>
				<span id="transImgName" class="fl disabled" style="margin-left: 35px; text-align: left; cursor: pointer; color:blue;">리스트 보기</span>
			</div>
			<div class="wtcut_box w100" id="tab9_3rd" style="overflow: auto; height: 445px; margin-top: 5px; display: inline-block; border:2px solid #20beca">
				<div class="imgPreview_">Image Preview</div>
			</div>
  				<div class="w100 fl mt5" id="tab9_4th">
	           	<input class="w100" type="text" id="wt_cut_saveName" style="height: 30px; text-align: center; border:0px;" placeholder="파일 이름을 입력해주세요">
	         </div>
  				<div class="w100 fl mt5" id="tab9_5th">
	           	<button class="btn-cyan fr disabled" id="wt_cut_save_all" type="button" style="width:100%; height: 40px; border-radius:0;" onclick=''>이미지 컷 전체 다운로드</button> 
	         </div>
		</div>
		</form>
	</div>
</div>

<script>
var transImgNameFlag = false;
$(document).ready(function() {
	$('#transImgName').click(function(){
		if($('.wt_cut_appendImg').find('span').hasClass("dn")){
			$('.wt_cut_appendImg').find('img:first').addClass("dn");
			$('.wt_cut_appendImg').find('span').removeClass("dn");
			$('#transImgName').text("이미지 보기");
			transImgNameFlag = true;
		}else{
			$('.wt_cut_appendImg').find('span').addClass("dn");
			$('.wt_cut_appendImg').find('img:first').removeClass("dn");
			$('#transImgName').text("리스트 보기");
			transImgNameFlag = false;
		}
	});
});
</script>