<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl re h100">
	<div class="menu-cont-title">
		<span class="fl" style="margin-top:9px;">Image segmentation</span>
		<button class="fr cp cut-img-add">Save background</button>
	</div>
	<form id="uploadGrabcutImgFrom" action="/upload/uploadImg" enctype="multipart/form-data"> 
	<div class="w100 fl">				   				
		<div class="w100 fl mt15 re apd-zone">
			<div class='img-frame'>            	
            	<img src='/resources/image/toolicon_1.png' class='upload-img upload-img-cut'>
            	<input type='file' name='img' id='img' class='kinp upload-inp inp"+i+"' accept='image/gif, image/jpeg, image/png'>
           	</div>
           	<div class="w100 fl mt15">
           		<button class="btn-cyan cut-img fl disabled" style="width:49%;" type="button" disabled="disabled" onclick="fileUploadGrabcutImg()">Image Picking</button>
           		<button class="btn-cyan fr" style="width:49%;" type="button" onclick="saveResultToList1()">Save</button>
           	</div>
		</div>
		<!-- 예전꺼 -->
		<!-- <div class="menu-cont-img-cut">
			<div class="w100 fl menu-cont-img-cut">
			</div>
			<button onclick="setMode(0)">새로운 영역</button>
			<button onclick="setMode(1)">배경제거</button>	
			<button onclick="setMode(2)">전경</button>
			<button class="color-go">색보정</button>				   				   				
			<button onclick="fileUploadGrabcutImg()" class="w100 fl btn-select-img go-edit">
				이미지 선택하기
			</button>
			<button class="w100 fl btn-select-img dn go-next">
				다음
			</button>	
			<button class="w100 fl btn-select-img dn go-save" onclick="">
				저장
			</button>			   				
		</div>	   --> 									
	</div>
	</form>		  
	<div class="menu-cont-title mt20">
		<span class="fl" style="margin-top:9px;">PSD file segmentation</span>
		<button class="fr cp cut-img-add">Add PSD File</button>
	</div> 	
	<div class="w100 fl">	
		<form id="upLoadPsdfileForm" action="/">			   				
		<div class="w100 fl mt15 re apd-zone">
			<div class='img-frame'>            	
            	<img src='/resources/image/toolicon_1.png' class='upload-img'>
            	<input type='file' name='psd' id='psd' class='kinp upload-inp inp"+i+"'>
           	</div>
           	<div class="w100 fl mt15">
           		<button type="button" class="btn-cyan fl" style="width:100%;" onclick="fileUploadPsdParseImg()">Image segmentation</button>           		
           	</div>
		</div>
		</form>
		<!-- div id="progressbar"></div>
		<div class="color-correction">
   			<div class="color-bar">
   				<div class="color-bar-control" id="green"></div>
   			</div>
   			<div class="w100 fl" style="color:#BBB;">
   				<span class="fl ml5">0</span>
   				<span class="fr mr5">100</span>
   			</div>
		</div -->		
	</div>	
	
	<div class="menu-cont-title mt20">
		<span class="fl" style="margin-top:9px;">Color correction</span>		
	</div> 	
	<div class="w100 fl">				   						
		<div class="color-correction">
   			<div class="color-bar">
   				<div class="color-bar-control" id="red"></div>
   				<div class="color-bar-control" id="green"></div>
   				<div class="color-bar-control" id="blue"></div>
   			</div>
   			<div class="w100 fl" style="color:#BBB;">
   				<span class="fl ml5">0</span>
   				<span class="fr mr5">255</span>
   			</div>
		</div>
		<div class="w100 fl mt15">
           	<button class="btn-cyan fl" type="button" style="width:49%;" onclick=changeColorExe()>Apply</button>
           	<button class="btn-cyan fr" type="button" style="width:49%;">Save</button>           		
        </div>
	</div>							
</div>

<script>

$(".color-go").click(function(){
	$(".color-correction").removeClass("dn");
});
</script>