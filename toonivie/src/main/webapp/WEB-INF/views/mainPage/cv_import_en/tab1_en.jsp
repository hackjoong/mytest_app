<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl re h100">
	<div class="tabbable" id="tabs-57137">
		<ul class="nav nav-tabs re_nav_tabs w100">
			<li class="active re_color_tab" style="width: 50%; margin-bottom: -2px;">
				<a class='image_select_menu' data-menu_name="image_cut" href="#panel-33950" data-toggle="tab">Image Separate</a>
			</li>
			<li class="re_color_tab" style="width: 50%;  margin-bottom: -2px;">
				<a class='image_select_menu' data-menu_name="psd_cut" href="#panel-902313" data-toggle="tab" style="margin-right: 0;">PSD Separate</a>
			</li>
			<!-- <li class="re_color_tab">
				<a class='image_select_menu' data-menu_name="wt_cut" href="#panel-333333" data-toggle="tab">컷 분리</a>
			</li> -->
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="panel-33950">
				<!-- <div class="menu-cont-title mt20">
					<span class="fl" style="margin-top:9px;">이미지 분리하기</span>
					<button class="fr cp cut-img-add psh_braggable" onclick="clearRect_re()">배경 저장</button>
				</div> -->
				<form id="uploadGrabcutImgFrom" action="/upload/uploadImg" enctype="multipart/form-data"> 
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<div class="w100 fl">				   				
					<div class="w100 fl mt15 re apd-zone">
						<span class="fr help" title="help" onclick="javascript:tab1_img('en');">
								<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
						</span>
						<div class='img-frame' id="tab1_1st">            	
			            	<img src='/resources/image/v2.0/img_upload_en.png' class='upload-img upload-img-cut re_upload_img_cut'>
			            	<input type='file' name='img' id='img' class='kinp upload-inp' accept='image/gif, image/jpeg, image/png'>
			           	</div>
			           	<div class="tooni-head2" style="margin-top: 20px;">Image Separate type</div>
			           	<div class="w100 fl" id="tab1_2nd">
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="cutModeBtn1" disabled="disabled" onclick="setMode(0)" src="/resources/image/v2.0/cutModeBtn1.png" alt="이미지 분리 - 박스형" title="이미지 분리 - 박스형"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Box type</div>
			           		</div>
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="cutModeBtn2" disabled="disabled" onclick="setMode(1)" src="/resources/image/v2.0/cutModeBtn2.png" alt="이미지 분리 - 자유형" title="이미지 분리 - 자유형" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Free type</div>
			           		</div>
			           		<div class="cutmode_box">
			           			<img class="w100 cut-img disabled" id="ObjectDetection" disabled="disabled" onclick="" src="/resources/image/v2.0/cutModeBtn3.png" alt="이미지 분리 - 객체인지" title="이미지 분리 - 객체인지"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Object recognition</div>
			           		</div>
			           	</div>
			           	<!-- <button class="btn-cyan fl cut-img disabled" type="button" id="cutModeBtn1" style="width:49%;" disabled="disabled" onclick="setMode(0)">박스형</button>
				        <button class="btn-gray fr cut-img disabled" type="button" id="cutModeBtn2" style="width:49%;" disabled="disabled" onclick="setMode(1)">자유형</button>   --> 
			           	<div>
			           		<div class="tooni-head2_blur" style="margin-top: 20px; position: relative;">Filling background color
			           			<label class="switch">
					              <div style="position: absolute; top: 1px; right: 35px; font-size: 0.85em; width: 100%;">BLUR </div>
					        	  <input type="checkbox" class="jamakcheck" checked>
					        	  <span id="grabcut_blur_status" class="slider round"></span>
					      		</label>
					      		<!-- <button class="btn-cyan cut-img disabled" type="button" id="grabcut_blur_status" style="width:32%;" disabled="disabled">블러</button> -->
			           		</div>
			           	</div>
			           	<div class="w100 fl" id="tab1_3rd">
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled" id="fill_black" disabled="disabled" onclick="set_filling_color(0)" src="/resources/image/v2.0/fill_black.png" alt="검정색채우기" title="검정색채우기"/>
			           			<div style="text-align: center;  font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Fill black</div>
			           		</div>
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled" id="fill_white" disabled="disabled" onclick="set_filling_color(1)" src="/resources/image/v2.0/fill_white.png" alt="흰색채우기" title="흰색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Fill white</div>  
			           		</div>
			           		<div class="w25 fl fill_mar">
			           			<img class="w100 fill_color cut-img disabled"  id="fill_surrounding" disabled="disabled" onclick="set_filling_color(2)" src="/resources/image/v2.0/fill_surrounding.png" alt="주변색채우기" title="주변색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px; ">Fill color around</div>
			           		</div>
			           		<div class="w25 fl" style="margin: 0 0 0 6%;">
			           			<img class="w100 cut-img disabled" id="grabcut_color_picker" disabled="disabled" style="margin-top: 10px; border-radius:10px;" src="/resources/image/v2.0/grabcut_color_picker.png" alt="지정색채우기" title="지정색채우기" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Fill Custom color</div>
			           		</div>
			           		<div class="w25 fl" style="margin: 0 0 0 6%;">
			           			<img class="w100 cut-img disabled" id="fill_auto" disabled="disabled" onclick="set_filling_color(4)" style="margin-top: 10px; border-radius:10px;" src="/resources/image/v2.0/fill_auto.png" alt="자동채우기" title=""자동채우기"" />
			           			<div style="text-align: center; font-size: 0.85em; color:#7b7c7c; margin-top: 5px;">Fill auto</div>
			           		</div>
			           	</div>
			           	<div class="w100 fl mt20 mb20 bg_before_re" id="tab1_4th">
				           	<button id="cut_img_ori" class="btn-ivory cut-img fl disabled" type="button" style="width:49%;" disabled="disabled" onclick="fileUploadGrabcutImg()">Image Separate</button>
				           	<button class="btn-ivory fr cut-img-add psh_braggable disabled" onclick="clearRect_re()" disabled="disabled" type="button" id="" style="width:49%;" >Save B.G.</button>       		
				        </div>
			           	<ul class="nav nav-tabs re_nav_tabs w100">
							<li class="active re_color_tab" id="tab1_5th" style="width: 50%; margin-bottom: -2px;">
								<a class='image_select_menu' data-menu_name="color_palette" href="" data-toggle="tab">Color Palette</a>
							</li>
							<li class="re_color_tab" id="tab1_6th" style="width: 50%;  margin-bottom: -2px;">
								<a class='image_select_menu' data-menu_name="color_correction" href="" data-toggle="tab" style="margin-right: 0;">Color calibration</a>
							</li>
						</ul>
						<div class="tab_color_palette" style="width: 100%; height: 150px;">
							<div class="w100 fl mt10 mb10">
					           	<button class="btn-ivory fl tab1_btn_color disabled color_change_btns" type="button" style="width:49%;" onclick="get_count()">Color extraction</button>
					           	<button class="btn-gray fr disabled color_change_btns" id="change_color_trans" type="button"  style="width:49%;" >Change color</button>
					           	<button class="btn-ivory fr disabled" id="ROI_select" onclick='ROI_setting()' type="button" style="width:100%; margin-top: 5px;" >Specify the ROI</button>         		
					        </div>
					        <div class="color_box w100" style="height: 100px;">
								<div class="color_box_text" style="line-height: 100px;">Color Palette</div>
							</div>
						</div>
			           	<div class="tab_color_correction dn" style="width: 100%; height: 150px;">
			           		<div class="color-correction" style="margin-top: 20px;">
					   			<div class="color-bar">
					   				<div class="color-bar-control" id="red"></div>
					   				<div class="color-bar-control" id="green"></div>
					   				<div class="color-bar-control" id="blue"></div>
					   			</div>
					   			<div class="w100 fl" style="color:#BBB;">
					   				<span class="fl ml5">0</span>
					   				<span class="fr mr5">255</span>
					   			</div>
							</div>
							<div class="w100 fl mt15">     		
					           	<button class="btn-cyan fl" type="button" style="width:49%; border-radius:0;" onclick='changeColorExe(false)'>Preview</button> <!-- add by KH, 2017-12-18 -->
					           	<button class="btn-cyan fr" type="button" style="width:49%; border-radius:0;" onclick='changeColorExe(true)'>Apply</button> <!-- add by KH, 2017-12-18 -->
					        </div>
					        <div class="w100 fl mt5">
					           	<button class="btn-cyan fr" id="go_back" type="button" style="width:100%; border-radius:0;" onclick='getColorValueAvr()'>Back again</button> <!-- add by KH, 2017-12-18 -->
					        </div>
			           	</div>
			           	
			           	
			        
			           	
			           	
			           	<!-- <div class="w100 fl mt15">
			           		<button id="cut_img_ori" class="btn-cyan cut-img fl disabled" style="width:100%;" type="button" disabled="disabled" onclick="fileUploadGrabcutImg()">이미지 따기</button>           		
			           	</div>
			           	<div class="w100 fl mt5">
				           	<button class="btn-cyan fl cut-img disabled" type="button" id="cutModeBtn1" style="width:49%;" disabled="disabled" onclick="setMode(0)">박스형</button>
				           	<button class="btn-gray fr cut-img disabled" type="button" id="cutModeBtn2" style="width:49%;" disabled="disabled" onclick="setMode(1)">자유형</button>          		
				        </div>			           	
			           	<div class="w100 fl mt5 psh_dn">
			           		<button class="btn-gray cut-img fl disabled" id="fg_bg" style="width:100%;" type="button" disabled="disabled" onclick="fg_bg_selection()">FG-BG 선택</button>           		
			           	</div>
			           	add by KH 2017-12-21
			           	<div class="w100 fl mt5"> 
				           	<button class="btn-cyan  fill_color cut-img disabled" type="button" id="fill_black" style="width:32%;" disabled="disabled" onclick="set_filling_color(0)">검정색 채우기</button>
				           	<button class="btn-gray  fill_color cut-img disabled" type="button" id="fill_white" style="width:32%;" disabled="disabled" onclick="set_filling_color(1)">흰색 채우기</button>  
				           	<button class="btn-gray  fill_color cut-img disabled" type="button" id="fill_surrounding" style="width:33%;" disabled="disabled" onclick="set_filling_color(2)">주변색 채우기</button>  
			           	</div>
  				        <div class="w100 fl mt5"> 
				           	<button class="btn-cyan cut-img disabled" type="button" id="grabcut_blur_status" style="width:32%;" disabled="disabled" onclick="">블러</button>
				           	<button class="btn-gray cut-img disabled" type="button" id="grabcut_color_picker" style="width:32%;" disabled="disabled" onclick="">임의색 채우기</button>
				           	<label class="btn-gray cut-img disabled" id="grabcut_color_picker_pre" style="width:32%; background-color: black; vertical-align: bottom; margin-bottom: 0;"></label>
			           	</div> -->
					</div>
					<!-- 예전꺼 -->
					<!-- <div class="menu-cont-img-cut">
						<div class="w100 fl menu-cont-img-cut">
						</div>
						<button onclick="setMode(0)">새로운 영역</button>
						<button onclick="setMode(1)">배경제거</button>	
						<button onclick="setMode(2)">전경</button>
						<button class="color-go">색보정</button>				   				   				
						<button onclick="fileUploadGrabcutImg()" class="w100 fl btn-select-img go-edit">
							이미지 선택하기
						</button>
						<button class="w100 fl btn-select-img dn go-next">
							다음
						</button>	
						<button class="w100 fl btn-select-img dn go-save" onclick="">
							저장
						</button>			   				
					</div>	   --> 	
					
					<!-- <div class="menu-cont-title mt20 mb15 psh_dn">
						<span class="fl" style="margin-top:9px;">붓 크기</span>		
					</div> 	
					<div class="slidecontainer psh_dn">
					  <input type="range" min="1" max="100" value="20" class="slider" id="fg_width">
					  <p>Value: <span id="fg_width_val">20</span></p>
					  <input type="range" min="1" max="100" value="20" class="slider" id="bg_width">
					  <p>Value: <span id="bg_width_val">20</span></p>
					</div> -->
					
					<!-- <div class="menu-cont-title mt20">
						<span class="fl" style="margin-top:9px;">컬러 팔레트</span>	
						<span class="fr" style="margin-top:12px; color:#ff8c24; font-size: 12px;">※ 최대 10개까지 입니다.</span>		
					</div> 	
					
		           	<div class="w100 fl mt15">
			           	<button class="btn-cyan fl tab1_btn_color disabled color_change_btns" type="button" id="" style="width:49%;" onclick="get_count()">색상 추출</button>
			           	<button class="btn-gray fr disabled color_change_btns" type="button" id="change_color_trans" style="width:49%;" >색상 변경</button>          		
			        </div>
	           		<div class="w100 fl mt5 mb5">
		           		<button class="btn-cyan fl disabled" id="ROI_select" style="width:100%;" type="button" onclick='ROI_setting()'>ROI 설정</button>           		
		           	</div>
					추출된 팔렛트 출력
					<div class="color_box w100">
						<div class="color_box_text">Color Palette</div>
					<div class="" id="palette1" style="">
						</div>
						<div class="" id="palette2" style="">
						</div>
						<div class="" id="palette3" style="">
						</div>
						<div class="" id="palette4" style="">
						</div>
						<div class="" id="palette5" style="">
						</div>
					</div>
					<div class="menu-cont-title mt20">
						<span class="fl" style="margin-top:9px;">색상 보정 (RGB)</span>		
					</div> 	
					<div class="w100 fl">				   						
						<div class="color-correction">
				   			<div class="color-bar">
				   				<div class="color-bar-control" id="red"></div>
				   				<div class="color-bar-control" id="green"></div>
				   				<div class="color-bar-control" id="blue"></div>
				   			</div>
				   			<div class="w100 fl" style="color:#BBB;">
				   				<span class="fl ml5">0</span>
				   				<span class="fr mr5">255</span>
				   			</div>
						</div>
						<div class="w100 fl mt15">
				       <button class="btn-cyan fl" type="button" style="width:49%;" onclick=changeColorExe()>적용하기</button>
				           	<button class="btn-cyan fr" type="button" style="width:49%;">저장하기</button>           		
				           	<button class="btn-cyan fl" type="button" style="width:49%;" onclick='changeColorExe(false)'>미리보기</button> add by KH, 2017-12-18
				           	<button class="btn-cyan fr" type="button" style="width:49%;" onclick='changeColorExe(true)'>적용하기</button> add by KH, 2017-12-18
				        </div>
				        <div class="w100 fl mt5">
				           	<button class="btn-cyan fr" type="button" style="width:100%;" onclick='changeColorExe(true, rgb_ori)'>원래대로</button> add by KH, 2017-12-18
				           	<button class="btn-cyan fr" id="go_back" type="button" style="width:100%;" onclick='getColorValueAvr()'>원래대로</button> add by KH, 2017-12-18
				        </div>
					</div> -->														
				</div>
				</form>		  
			</div>
			<div class="tab-pane" id="panel-902313">
				<div class="menu-cont-title mt20">
				<span class="fr help" title="도움말" onclick="javascript:tab1_psd('en');" style="padding-bottom:10px;">
					<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
				</span>
					<!-- <span class="fl" style="margin-top:9px;">PSD 파일 분리하기</span> -->
					<!-- <button class="psh_braggable fr cp cut-img-add" id="psd_bg_selection">PSD 배경 지정</button> -->
				</div> 	
				<div class="w100 fl">	
					<form id="upLoadPsdfileForm" action="/psdParse/psdUpload">			   				
						<div class="w100 fl mt15 re apd-zone">
							<div class='img-frame' id="tab1_psd_1st"> 
				            	<img id='upload_psd' src='/resources/image/v2.0/img_upload_en.png' class='upload-img' data-uploaded='false'>
				            	<input type='file' name='psd' id='psd' class='kinp upload-inp inp"+i+"'>
				           	</div>
				           	<!-- 뿌려질것 -->
				           	<div id="psd_file_name" class="w100 fl mt15 file_Name_Psd">
				           	PSD Name
				           	</div>
				           	<!-- //뿌려질것 -->
				           	<div class="w100 fl mt5" id="tab1_psd_3rd">
				           		<button type="button" class="btn-cyan fl psd_Open disabled" style="width:100%;" onclick="fileUploadPsdParseImg()">PSD Separate</button>     		
				           	</div>
				           		<div class="menu-cont-title mt20">
								<span class="fl" style="margin-top:9px;">Select PSD layer Background</span>	
							</div>
				            <div class="w100 fl mt5" id="tab1_psd_4th">
				           		<input type="button" class="btn-cyan psh_braggable fr cp cut-img-add mt5 disabled" style="width:100%; color:#fff;" id="psd_bg_selection" value="Select PSD layer Background">		
				           	</div>
				           	<div class="menu-cont-title mt20">
								<span class="fl" style="margin-top:9px;">PSD Layer Separate</span>	
							</div>
				           	<div class="w100 fl mt5" id="tab1_psd_5th">
				           		<button type="button" id="crop_Close_btn" class="btn-gray fr disabled" style="width:49%;">on</button>    
				           		<button type="button" id="crop_Open_btn" class="btn-gray fl disabled" style="width:49%;">off</button>       		
				           	</div>
   					        <div class="w100 fl mt5" id="tab1_psd_6th">
				           		<button type="button" id="crop_btn" class="btn-cyan fl disabled" style="width:100%;" onclick="">Save the Separate layer</button>           		
				           	</div>
				           	
						</div>
					</form>
					<div class="menu-cont-title mt20 psh_dn">
						<span class="fl" style="margin-top:9px;">색상 보정 (RGB)</span>		
					</div> 	
					<div class="w100 fl psh_dn">				   						
						<div class="color-correction">
				   			<div class="color-bar for_psd">
				   				<div class="color-bar-control" id="red_psd"></div>
				   				<div class="color-bar-control" id="green_psd"></div>
				   				<div class="color-bar-control" id="blue_psd"></div>
				   			</div>
				   			<div class="w100 fl" style="color:#BBB;">
				   				<span class="fl ml5">0</span>
				   				<span class="fr mr5">255</span>
				   			</div>
						</div>
				        <div class="w100 fl mt5">
				           	<button class="btn-cyan fr" id="change_psd_color" type="button" style="width:100%;" onclick=''>적용</button> <!-- add by KH, 2017-12-18 -->
				        </div>
				         <div class="w100 fl mt5">
				           	<button class="btn-cyan fr" id="return_psd_color" onclick="" type="button" style="width:100%;" onclick=''>원래대로</button> <!-- add by KH, 2017-12-18 -->
				         </div>
					</div>						
					<!-- div id="progressbar"></div>
					<div class="color-correction">
			   			<div class="color-bar">
			   				<div class="color-bar-control" id="green"></div>
			   			</div>
			   			<div class="w100 fl" style="color:#BBB;">
			   				<span class="fl ml5">0</span>
			   				<span class="fr mr5">100</span>
			   			</div>
					</div -->		
				</div>	
			</div>
			<%-- <%-- <div class="tab-pane" id="panel-333333">
				<div class="menu-cont-title mt20">
					<span class="fl" style="margin-top:9px;">컷 분리하기</span>
				</div>
				<form id="fileUploadWebtoonImg" action="/upload/uploadImg_wt" enctype="multipart/form-data"> 
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<div class="w100 fl">				   				
					<div class="w100 fl re apd-zone" style="margin-top: 22px;">
						<div class='img-frame'>            	
			            	<img src='/resources/image/toolicon_1.png' class='upload-img upload-img-cut'>
			            	<input type='file' name='wt_image' id='wt_image' class='kinp upload-inp' accept='image/gif, image/jpeg, image/png'>
			           	</div>
					</div>
					<!-- 추출된 웹툰 컷 출력 -->
			        <div class="w100 fl mt15">
			           	<button class=" fr btn-cyan disabled" type="button" id='wt_cut_save' style="width:100%;" >컷 분리하기</button> 
		     	    </div>
		     	    <div class="menu-cont-title mt20">
						<span class="fl"">분리된 컷 미리보기</span>
						<span class="fr" id='wt_cut_uploadCount'></span>
					</div>
					<div class="wtcut_box w100" style="overflow: auto; height: 400px; margin-top: 5px; display: inline-block; border:2px solid #323235">
						<div class="imgPreview_">Img Preview</div>
					</div>
    				<div class="w100 fl mt5">
			           	<input class="w100" type="text" id="wt_cut_saveName" style="height: 40px; text-align: center;" placeholder="예시: 우주유실물1화">
			         </div>
    				<div class="w100 fl mt5">
			           	<button class="btn-cyan fr disabled" id="wt_cut_save_all" type="button" style="width:100%;" onclick=''>전체 저장</button> 
			         </div>
       				
				</div>
				</form>
			</div> --%> 
		</div>	
	</div>
</div>
<style>
.btn-gray {
  border: none;
  background: #ccc;
  height: 30px;
  color: white;
  font-weight: 300;
  border-radius:5px;
}
</style>

<script>

$(".color-go").click(function(){
	$(".color-correction").removeClass("dn");
});

$(".img-frame").click(function(){
	$(".psd_Open").removeClass("disabled");
});


</script>