<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 h100 fl re">			   				
	<div class="w100 fl">
		<div class="direction-list" data-direction="direction1">Character effect</div>
		<div class="direction-cont direction1">
			<div class="row">				
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-2.png)"
						data-type="0" data-effecttype="move" title="Move Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-3.png)" 
						data-type="0" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-4.png)" 
						data-type="0" data-effecttype="fastMove" title="Fast Move Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-5.png)" 
						data-type="0" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-6.png)" 
						data-type="0" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-7.png)" 
						data-type="1" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-8.png)" 
						data-type="1" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-8.png)" 
						data-type="0" data-effecttype="walk" title="Walk Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/human-effect/human-effect-8.png)" 
						data-type="0" data-effecttype="rotateMove" title="Move with Rotate"></div>
				</div>
								
			</div>	
			<div class="row result-1 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-1">complete</button>
					</div>
				</div>
			</div>
		</div>	
		<div class="direction-list" data-direction="direction2">Object production</div>
		<div class="direction-cont direction2">
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-1.png)" 
						data-effecttype="1" title="Smaller Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-2.png)" 
						data-effecttype="2" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-3.png)" 
						data-effecttype="3" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-4.png)" 
						data-effecttype="4" title="Bigger Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-5.png)" 
						data-effecttype="6" title="Rotation-Left Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-6.png)" 
						data-effecttype="5" title="Rotation-Right Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-7.png)" 
						data-effecttype="7" title="Vibration Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-7.png)" 
						data-effecttype="8" title="Rotate-Smaller Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-7.png)" 
						data-effecttype="9" title="Rotate-Bigger Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-7.png)" 
						data-effecttype="10" title="Bigger And Smaller"></div>
				</div>
			</div>	
			<div class="row result-2 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-2">complete</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list" data-direction="direction3">Background production</div>
		<div class="direction-cont direction3"><!-- 구름,물결,연기,태풍,폭발,햇빛 -->
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-1.png)"
						 data-effecttype="3-1" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-2.png)"
						 data-effecttype="3-2" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-3.png)"
						 data-effecttype="3-3" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-4.png)"
						 data-effecttype="3-4" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-5.png)"
						 data-effecttype="3-5" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect" style="background-image:url(/resources/image/effect/background-effect/background-effect-6.png)"
						 data-effecttype="3-6" title="No Service"></div>
				</div>				
			</div>	
			<div class="row result-3 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">complete</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list" data-direction="direction4">Scene presentation</div>
		<div class="direction-cont direction4">
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-1.png)" 
						data-effecttype="1" title="Fade-in Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-2.png)" 
						data-effecttype="2" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-3.png)" 
						data-effecttype="3" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="4" title="Fade-out Effect"></div>
				</div>	
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="11" title="ink-in1 Effect"></div>
				</div>	
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="12" title="ink-in2 Effect"></div>
				</div>	
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="13" title="ink-in3 Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="14" title="ink-in1 disappear Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="15" title="left to right Effect"></div>
				</div>	
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="16" title="up to down Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="17" title="up to down Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="18" title="zizic Effect"></div>
				</div>							
			</div>
			<div class="row result-4 dn">
				<div class="col-xs-12 result-btn-box dn" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">complete</button>
					</div>
				</div>
			</div>	
		</div>		
	</div>	
</div>
<script>
var effectObjFlag=0;
var effectObjImgUrl;
var effectObjItem;

var effect_time;
var startEndFlag=0;
var effectKind="";

/* 연출 - 인물연출/사물연출/배경연출/장면연출 타이틀 클릭 이벤트 */
$(".direction-list").click(function(){ //체크 OK
	var data = $(this).data("direction"); //direction1 ~ direction4
	 if(!$(".direction-cont").is(":animated")){	//	 
		 if($("." + data).height() == 0){
			 $(".direction-cont").animate({maxHeight:"0", opacity:"0"}, 400);
			 $("." + data).animate({maxHeight:"400px", opacity:"1"}, 400); 
		 }else{
			 $("." + data).animate({maxHeight:"0", opacity:"0"}, 400);
		 }		 
	 }else{
		 return false		 
	 }
});
/*
$(".go-translate").click(function(){ //no use
	popupOff($(".translate-effect-modal"));
});
*/

/* 인물연출 : 항목 클릭 이벤트 */
$(".human-effect").click(function(){
	var $this = $(this).data('type');
	effectKind= $(this).data('effecttype');	
	startEndFlag = 0;
	console.log("tab2.jsp:153 - human-effect.click");
	$(".effect-time").attr("value","1"); //10->1, modify by shkwak, 2017-06-20
	popupOn($(".effect-modal"));	
	if($this == 0){
		$(".go-start").removeClass('dn')
		$(".go-target").addClass('dn');
	}else{
		$(".go-start").addClass('dn')
		$(".go-target").removeClass('dn');
	}	
});

/* 사물연출 : 항목 클릭 이벤트 */
$(".object-effect").click(function(){
	var data = $(this).data('effecttype'); //1~7
	effectKind = data;
	effectObjFlag = 0; //의미?
	$(".object-effect-time").attr("value","3"); //10->3, by shkwak
	popupOn($(".object-effect-modal"), data);
});

/* 사물연출 : 설정완료 클릭 이벤트 */
$(".go-object").click(function(){ //effectObjFlag=0 조건식이 있어야 할듯, shkwak
	effectTime = $(".object-effect-time").val();
	//effectKind = $(this).data('obj');
	if(effectKind==5||effectKind==6||effectKind==7||effectKind==4||effectKind==1||effectKind==8||effectKind==9||effectKind==10){ //추가 순서대료
		alert("Click on the 'Saved images' tab to apply the effect.");
		effectObjFlag = 1;
		popupOff($(".object-effect-modal"));	
	}	
});

$(".saved-image-item-image").click(function(){
	if(effectObjFlag==1){
		effectObjImgUrl=$(this).attr("data-url");
		console.log("effectObjImgUrl : "+effectObjImgUrl);
		var index;
		for(var i=1;i<10;i++){
			if($(this).hasClass("itemNum"+i)){
				index=i;
			}	
		}		
		console.log("tab2.jsp:203 - saved-image-item-image.click : " + effectObjImgUrl);
		var offset = $(".canvas-code"+index).offset();
		var top = offset.top;
		var left = offset.left;
/* 		var bOffset = $(".canvas-code10").offset();
		var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); */
		var bOffset = $(".canvas-codeBG").offset();
		var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
		var bTop = bOffset.top;
		var bLeft = bOffset.left;
		effectObjexeGo(top,left,bTop,bLeft,bImgUrl, index); //tab2.jsp:216 - effectObjexeGo()
	}
	effectObjFlag = 0;
})

function effectObjexeGo(top,left,bTop,bLeft,bImgUrl, obj_index){ //effectObjexeGo
	var jsonString ="";
	jsonString+="{ \"effectKind\":\""+effectKind+"\",";
	jsonString+="\"effectTime\":"+effectTime+",";
	jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
	jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+"},";
	jsonString+="\"otherObj\":[";
	var count = 0;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			var imgUrl=$(".canvas-code"+i).data("image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	console.log(jsonString);
	var data={};
	data["data"]=jsonString;
	$.ajax({
	       url : "/applyObject/getApply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async:false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab2.jsp:272 - " + data.videoUrl);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	});
}

/* 배경연출 : 항목 클릭 이벤트 */ //add by shkwak, 2017-07-14
$(".state-effect").click(function(){ 
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	console.log("tab2.jsp:298 - state-effect.click");
	popupOn($(".transform-effect-modal"), data);
});

/* 장면연출 : 항목 클릭 이벤트 */
$(".transform-effect").click(function(){ 
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	console.log("tab2.jsp:307 - transform-effect.click");
	popupOn($(".transform-effect-modal"), data);
});

/* 템플릿 : 효과 적용 */
$(".go-transform").click(function(){
	var bOffset = $(".canvas-codeBG").offset();
	var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
	var bTop = bOffset.top;
	var bLeft = bOffset.left;
	var jsonString ="";
	console.log("tab2.jsp:318 - effectKind : " + effectKind);
	jsonString+="{ \"effectKind\":\"" + effectKind + "\",";
	jsonString+="\"effectTime\":" + $(".transform-effect-time").val() + ",";
	jsonString+="\"background\":{\"imgUrl\":\"" + bImgUrl + "\",\"top\":" + bTop + ",\"left\":" + bLeft+"},";
	jsonString+="\"obj\":[";
	var count = 0;
	for(var i=1;i<10;i++){ //for문 돌리는 이유?
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			var imgUrl = $(".canvas-code"+i).attr("data-image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	console.log("tab2.jsp:346 - jsonString : " + jsonString);
	popupOff($(".transform-effect-modal"));
	var data={};
	data["data"]=jsonString;
	$.ajax({
	       url : "/sceneApply/apply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async:false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab2.jsp:359 - videoUrl : " + data.videoUrl);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl + "'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	});
})

var imgEffectList={};
$(".go-start").click(function(){
	popupOff($(".effect-modal"));
	effect_time=$(".effect-time").val();
	//effectKind="move";
	alert("Please specify the starting position of the saved image.");
	$(".result-1").removeClass("dn");
	
});

$(".go-target").click(function(){
	popupOff($(".effect-modal"));
	effect_time=$(".effect-time").val();
	effect_time=$(".effect-time").val();
	effectKind="rotate";
	alert("Please set the target to the saved image tab.");
	$(".result-2").removeClass("dn");
});

/* 
$(".layered_img").click(function(){
	$(".draggg").removeClass('select-effect');
	$(this).addClass("select-effect");
}); */
//append element onclick by KH 2018-02-07
$('#layered_img').on('click', '.draggg',function() {
	$(".draggg").removeClass('select-effect');
	$(this).addClass("select-effect");
});


$(".result-btn-1").click(function(){
	if(startEndFlag==0){
		setFirstPosition();
		alert("Please specify the last position of the stored image.");
		startEndFlag++;
	}else{
		setLastPosition();
		setBackgroundPosition();
	}
});

function setBackgroundPosition(){
/* 	var backgourndOffset = $(".canvas-code10").offset(); */
	var backgourndOffset = $(".canvas-codeBG").offset();
	//console.log("top"+backgourndOffset.top+"  left:"+backgourndOffset.left);
	data+="\"background\" : {"+
	//"\"imgUrl\":\""+removeUrl($(".canvas-code10").css('background-image'))+"\","+
	/* "\"imgUrl\":\""+$(".canvas-code10").attr("data-image")+"\","+ */
	"\"imgUrl\":\""+$(".canvas-codeBG").attr("data-image")+"\","+
	"\"top\":"+backgourndOffset.top+","+
	"\"left\":"+backgourndOffset.left+""+
	"}}";
	moveObject(data);
}	

var data="";
function setFirstPosition(){
	data="";
	data+="{";
	var firstInfo = new Array();
	data+="\"firstInfo\" : [";
	var count = 0;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			var appendString ="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			var imgUrl = $(".canvas-code"+i).attr("data-image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
		}
	}	
	data +="],";
}
	
function setLastPosition(){
	var lastInfo = new Array();
	data+="\"lastInfo\" : [";
	var count = 0;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			var appendString ="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			var imgUrl = $(".canvas-code"+i).attr("data-image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
		}
	}	
	data +="],";
	data +="\"effectKind\":\""+effectKind+"\",";
	data +="\"effectTime\":"+effect_time+",";
	//console.log(data);
}

function removeUrl(imgUrl){
	imgUrl = imgUrl.replace("url(\"","");
	imgUrl = imgUrl.replace("\")","");
	return imgUrl;
}
</script>