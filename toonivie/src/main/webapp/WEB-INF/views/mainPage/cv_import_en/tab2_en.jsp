<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 h100 fl re">
	<span class="fl">Production effect</span>
	<span class="fr help" title="도움말" onclick="javascript:tab2('en');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
	</span>			   				

	<div class="w100 fl">
		<div class="direction-list direction_list1 direction_list_color" data-direction="direction1" id="tab2_1st">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_character.png" style="width: 60px;" alt="" />
			<div class="direction_list_text">Character effect</div>	
		</div>
		<div class="direction-cont direction1">
			<div class="row">				
				
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-5.png)" 
						data-type="0" data-effecttype="fastMove" title="Fast Move Effect" data-ctgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">fast Move</div>
				</div>
				<div class="col-xs-4 not-allowed">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-5.png)" 
						data-type="0" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4 not-allowed">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-6.png)" 
						data-type="0" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4 not-allowed">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-7.png)" 
						data-type="1" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4 not-allowed">
					<div class="direction-item human-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/human-effect/human-effect-8.png)" 
						data-type="1" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-1.png)" 
						data-type="0" data-effecttype="walk" title="Walk Effect" data-ctgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Walk</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/human-effect/human-effect-9.png)" 
						data-effecttype="11" title="Eye Blink" data-ctgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Eye Blink</div>
				</div>
				<!--<div class="col-xs-4">
					<div class="direction-item human-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-4.png)" 
						data-type="0" data-effecttype="rotateMove" title="Move with Rotate"></div>
				</div>
				-->								
			</div>	
			<div class="row result-1 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-1">Check</button>
					</div>
				</div>
			</div>
		</div>	
		<div class="direction-list direction_list2" data-direction="direction2" id="tab2_2nd">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_objects.png" style="width: 60px;" alt="effect_objects" />
			<div class="direction_list_text">Object effect</div>
		</div>
		<div class="direction-cont direction2">
			<div class="row">								
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-1.png)" 
						data-type="0" data-effecttype="move" title="Move Effect" data-ojgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Move</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-2.png)" 
						data-effecttype="2" title="Smaller Effect" data-ojgif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Smaller</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-3.png)" 
						data-effecttype="3" title="Bigger Effect" data-ojgif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Bigger</div>
				</div>				
				<!-- <div class="col-xs-4">
					<div class="direction-item object-effect" style="background-image:url(/resources/image/effect/object-effect/object-effect-4.png)" 
						data-effecttype="4" title="Rotation-Move Effect"></div>
				</div> -->
				<div class="col-xs-4">
					<div class="direction-item human-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-4.png)" 
						data-type="0" data-effecttype="rotateMove" title="Rotation-Move Effect" data-ojgif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Move</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-5.png)" 
						data-effecttype="5" title="Rotation-Smaller Effect" data-ojgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Small</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-6.png)" 
						data-effecttype="6" title="Rotation-Bigger Effect" data-ojgif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Bigger</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-7.png)" 
						data-effecttype="7" title="Bigger-Smaller Effect" data-ojgif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Bigger-Smaller</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-8.png)" 
						data-effecttype="8" title="Rotation-Left Effect" data-ojgif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Left</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-9.png)" 
						data-effecttype="9" title="Rotation-Right Effect" data-ojgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rotation-Right</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item object-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/object-effect/object-effect-10.png)" 
						data-effecttype="10" title="Vibration Effect" data-ojgif="10" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Vibration</div>
				</div>
			</div>	
			<div class="row result-2 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-2">Check</button>
					</div>
				</div>
			</div>
			<div class="row result-1 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl result-btn-1">Check</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list direction_list3" data-direction="direction3" id="tab2_3rd">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_background.png" style="width: 60px;" alt="effect_background" />
			<div class="direction_list_text">Background effect</div>
		</div>
		<div class="direction-cont direction3"><!-- 구름,물결,연기,태풍,폭발,햇빛 -->
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-1.png)"
						 data-effecttype="3-1" title="Cloud Effect" data-bggif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cloud</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-2.png)"
						 data-effecttype="3-2" title="Wave Effect" data-bggif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Wave</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-3.png)"
						 data-effecttype="3-3" title="Smoke Effect" data-bggif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Smoke</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-4.png)"
						 data-effecttype="3-4" title="Typhoon Effect" data-bggif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Typhoon</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-5.png)"
						 data-effecttype="3-5" title="explosion Effect 1" data-bggif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">explosion</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-6.png)"
						 data-effecttype="3-6" title="explosion Effect 2" data-bggif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">explosion</div>
				</div>

				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-7.png)" 
						data-effecttype="3-7" title="Sunlight Effect" data-bggif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Sunlight</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item state-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/background-effect/background-effect-8.png)"
						 data-effecttype="3-8" title="Fire Effect" data-bggif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fire</div>
				</div>			
			</div>	
			<div class="row result-3 dn">
				<div class="col-xs-12 result-btn-box" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">Check</button>
					</div>
				</div>
			</div>
		</div>
		<div class="direction-list direction_list4" data-direction="direction4" id="tab2_4th">
			<img class="direction_list_img" src="/resources/image/v2.0/effect_transitions.png" style="width: 60px;" alt="effect_transitions" />
			<div class="direction_list_text">Scene effect</div>
		</div>
		<div class="direction-cont direction4">
			<div class="row">
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-1.png)" 
						data-effecttype="t1" title="Fade-in Effect" data-trgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-in</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-2.png)" 
						data-effecttype="t2" title="Fade-out Effect 1" data-trgif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-out1</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-3.png)" 
						data-effecttype="t3" title="Fade-out Effect 2" data-trgif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Fade-out2</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-4.png)" 
						data-effecttype="t4" title="Ink Effect 1(Disppear)" data-trgif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Disppear)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-5.png)" 
						data-effecttype="t5" title="Ink Effect 2(Appear)" data-trgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Appear)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-6.png)" 
						data-effecttype="t6" title="Ink Effect 3(Center)" data-trgif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Center)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-7.png)" 
						data-effecttype="t7" title="Ink Effect(Wipe)" data-trgif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Ink(Wipe)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-8.png)" 
						data-effecttype="t8" title="Cover Effect 1(Left to Right)" data-trgif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cover(Left to Right)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-9.png)" 
						data-effecttype="t9" title="Cover Effect 2(Top to Down)" data-trgif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Cover(Top to Down)</div>
				</div>
				<div class="col-xs-4">
					<div class="direction-item transform-effect effect_titleName" style="background-image:url(/resources/image/v2.0/effect/transform-effect/transform-effect-10.png)" 
						data-effecttype="t10" title="Blind Effect(Random)" data-trgif="10" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Blind(Random)</div>
				</div>
				<!-- <div class="col-xs-4 not-allowed">
					<div class="direction-item transform-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-11.png)" 
						data-effecttype="t11" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<!-- <div class="col-xs-4 not-allowed">
					<div class="direction-item transform-effect disabled effect_titleName btn_disabled" style="background-image:url(/resources/image/effect/transform-effect/transform-effect-12.png)" 
						data-effecttype="t12" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->	
			</div>
			<div class="row result-4 dn">
				<div class="col-xs-12 result-btn-box dn" style="padding-left:5px; padding-right:5px;">
					<div class="w100 fl">
						<button class="btn-cyan w100 fl">완료</button>
					</div>
				</div>
			</div>	
		</div>		
	</div>	
</div>
<script>
var effectObjFlag=0;
var effectObjImgUrl;
var effectObjItem;

var effect_time;
var startEndFlag=0;
var effectKind="";

/* 연출 - 인물연출/사물연출/배경연출/장면연출 타이틀 클릭 이벤트 */
$(".direction-list").click(function(){ //체크 OK
	var data = $(this).data("direction"); //direction1 ~ direction4
    var direction_num = data.substring(9);
	 if(!$(".direction-cont").is(":animated")){	//	 
		 if($("." + data).height() == 0){
			 $(".direction-cont").animate({maxHeight:"0", opacity:"0"}, 400);
			 $("." + data).animate({maxHeight:"600px", opacity:"1"}, 400); 
		 }else{
			 $("." + data).animate({maxHeight:"0", opacity:"0"}, 400);
		 }		 
	 }else{
		 return false		 
	 }
	 /* alert(direction_num); */
	 for(var i = 1; i < 5; i++){
			if(direction_num == i){
				$(".direction_list"+i).addClass('direction_list_color');
				
			}else{
				$(".direction_list"+i).removeClass('direction_list_color');
			}
		}
});
/*
$(".go-translate").click(function(){ //no use
	popupOff($(".translate-effect-modal"));
});
*/

/* 인물연출 : 항목 클릭 이벤트 */
$(".human-effect").click(function(){

	var $this = $(this).data('type');
	effectKind= $(this).data('effecttype');	
	
	startEndFlag = 0;
	console.log("tab2.jsp:246 - human-effect.click");
	$(".effect-time").attr("value","1"); //10->1, modify by shkwak, 2017-06-20
	popupOn($(".human-effect-modal"));	
	if($this == 0){
		$(".go-start").removeClass('dn')
		$(".go-target").addClass('dn');
		if(psd_img_mode == 'true'){
			auto_crop($('.canvas-codeBG'));
		}
	}else{
		$(".go-start").addClass('dn')
		$(".go-target").removeClass('dn');
	}	
});

/* 사물연출 : 항목 클릭 이벤트 */
$(".object-effect").click(function(){
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	var data = $(this).data('effecttype'); //1~7
	effectKind = data;
	effectObjFlag = 0; //의미?
	$(".object-effect-time").attr("value","3"); //10->3, by shkwak
	popupOn($(".object-effect-modal"), data);
});

/* 사물연출 : 설정완료 클릭 이벤트 */
$(".go-object").click(function(){ //effectObjFlag=0 조건식이 있어야 할듯, shkwak
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	effectTime = $(".object-effect-time").val();
	//effectKind = $(this).data('obj');
	if(effectKind==1||effectKind==2||effectKind==3||effectKind==4||effectKind==5||
			effectKind==6||effectKind==7||effectKind==8||effectKind==9||effectKind==10||effectKind==11 ){ //modify by shkwak, 2017-09-17, 항목 추가 //effectkind 11항목 추가
		alert("Click on the ' Saved Images ' tab to apply the image to the effect.");
		effectObjFlag = 1;
		popupOff($(".object-effect-modal"));	
	}	
});

$('#layered_img').on('click', '.saved-image-item-image',function() {
	if(effectObjFlag==1){
		if(psd_img_mode == 'true'){
			//alert('레이어에서 분리된 이미지를 선택해주세요.');
			return;
		}
		effectObjImgUrl=$(this).data("url");
		console.log("effectObjImgUrl : "+effectObjImgUrl);
		var index;
		for(var i=1;i<itemList_Length+1;i++){
			if($(this).hasClass("itemNum"+i)){
				index=i;
			}	
		}		
		console.log("tab2.jsp:203 - saved-image-item-image.click : " + effectObjImgUrl);
		var offset = $(".canvas-code"+index).offset();
		var top = offset.top;
		var left = offset.left;
/* 		var bOffset = $(".canvas-code10").offset();
		var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); */
		var bOffset = $(".canvas-codeBG").offset();
		var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
		var bTop = bOffset.top;
		var bLeft = bOffset.left;
		var target_wrap = $(this).parent().data('target');//add by KH, 2017-12-15 
 		console.log(target_wrap);
 		var ori_matrix = $("."+target_wrap).css("transform");//add by KH, 2017-12-15
		effectObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, index); //tab2.jsp:216 - effectObjexeGo()
	}
	effectObjFlag = 0;
});
$('#layered_img_cropped').on('click', '.saved-image-item-image',function() {
	//alert("aa");
	if(effectObjFlag==1){
		//set layer background by KH 2018-03-02
		//auto_crop($('.canvas-codeBG'));
		effectObjImgUrl=$(this).data("url");
		console.log("effectObjImgUrl : "+effectObjImgUrl);
		var index;
		for(var i=1;i<itemList_Length+1;i++){
			if($(this).hasClass("append_itemNum"+i)){
				index=i;
			}	
		}		
		console.log("tab2.jsp:203 - saved-image-item-image.click : " + effectObjImgUrl);
		var offset = $(".canvas-code"+index).offset();
		var top = offset.top;
		var left = offset.left;
/* 		var bOffset = $(".canvas-code10").offset();
		var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); */
		var bOffset = $(".canvas-codeBG").offset();
		//var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
		//var bTop = bOffset.top;
		//var bLeft = bOffset.left;
		if(psd_img_mode=='true'){
			bImgUrl = psd_layer_cut_imgurl;
			bTop = bOffset.top;
			bLeft = bOffset.left;
		}
		var target_wrap = $(this).parent().data('target');//add by KH, 2017-12-15 
 		console.log(target_wrap);
 		var ori_matrix = $("."+target_wrap).css("transform");//add by KH, 2017-12-15
		effectObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, index); //tab2.jsp:216 - effectObjexeGo()
	}
		
	effectObjFlag = 0;
});
/*//로딩 팝업 적용 버전 - 임시 주석처리_171023
function effectObjexeGo(top,left,bTop,bLeft,bImgUrl){ //effectObjexeGo
	loadingOn(); //주석처리(임시)_171023
	
	var jsonString ="";
	jsonString+="{ \"effectKind\":\""+effectKind+"\",";
	jsonString+="\"effectTime\":"+effectTime+",";
	jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
	jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+"},";
	jsonString+="\"otherObj\":[";
	var count = 0;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			var imgUrl=$(".canvas-code"+i).attr("data-image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	console.log(jsonString);
	var data={};
	data["data"]=jsonString;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
	       url : "/applyObject/getApply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async:false,
	       timeout:300000,
	       function(data) {
	    	   setTimeout(function () {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab2.jsp:272 - " + data.videoUrl);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	    	   loadingOff();
	    	   },1000);
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	});
}
*/
//matrix to degree 2017-12-15 by.KH
//rotate후 변환 행렬에서 각도 뽑아내기
function mtod(matrix){ 
	if(matrix == "none"){ //각도 조절이 없을경우 
		return 0;
	}
	var values = matrix.split('(')[1];
    values = values.split(')')[0];
    values = values.split(',');
	var a = values[0];
	var b = values[1];
	var c = values[2];
	var d = values[3];
	
	var scale = Math.sqrt(a*a + b*b);
	
	// arc sin, convert from radians to degrees, round
	// DO NOT USE: see update below
	var sin = b/scale;
	var angle = Math.round(Math.asin(sin) * (180/Math.PI));
	
	// works!
	console.log('Rotate: ' + angle + 'deg');
	
	return angle;
}

var btop_start_position;
function effectObjexeGo(top,left,bTop,bLeft,bImgUrl, ori_matrix, obj_index){ //effectObjexeGo
	var jsonString ="";
	console.log("bTop "+bTop +"top "+ top);
	console.log(obj_wh);
	if(psd_img_mode == 'true'){
		top = (top/shrinking_ratio - (btop_start_position - bTop)/shrinking_ratio);
		left = left/shrinking_ratio;
		bTop = bTop/shrinking_ratio;
		bLeft = bLeft/shrinking_ratio;
	}else{
		var obj_wh = ori2obj_resizing(obj_index);
		top = Math.round(top * img_resize_rate_h);
		left = Math.round(left * img_resize_rate_w);
		bTop = Math.round(bTop * img_resize_rate_h);
		bLeft = Math.round(bLeft * img_resize_rate_w);
	}
	jsonString+="{ \"effectKind\":\""+effectKind+"\",";
	jsonString+="\"effectTime\":"+effectTime+",";
	jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
//	jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"degree\":\""+degree+"\"},";
	jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"degree\":"+0+"},";
	jsonString+="\"otherObj\":[";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			
			jsonString +="{";	
			var offset_obj = $(".canvas-code"+i).offset();
			var top_obj = offset_obj.top;
			var left_obj = offset_obj.left;
			if(psd_img_mode == 'true'){
/*   				if(btop_start_position > top){
					jsonString += "}"
					if(forCount<count){
						jsonString+=",";
					}
					continue;
				}   */
				top_obj = (top_obj/shrinking_ratio - ((btop_start_position/shrinking_ratio) - bTop));
				left_obj = offset_obj.left/shrinking_ratio;
			}else{
				obj_wh = ori2obj_resizing(i);
				top_obj = Math.round(offset_obj.top * img_resize_rate_h);
				left_obj = Math.round(offset_obj.left * img_resize_rate_w);
			}
			//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			var imgUrl=$(".canvas-code"+i).data("image");
//			var degree= mtod($(".wrap"+i).css("transform"));
			var degree= 0;
			//alert(degree); //angel degree show
			jsonString += "\"top\":"+top_obj+",\"left\":"+left_obj+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"degree\":"+degree+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	console.log(jsonString);
	var data={};
	data["data"]=jsonString;
	$.ajax({
	       url : "/applyObject/getApply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async : false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab2.jsp:272 - " + data.videoUrl);
	    	   console.log("tab2.jsp:1231231232131231 - " + data);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	});
}

/* 배경연출 : 항목 클릭 이벤트 */ //add by shkwak, 2017-07-14
$(".state-effect").click(function(){ 
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	console.log("tab2.jsp:431 - state-effect.click");
	popupOn($(".transform-effect-modal"), data);
});

/* 장면연출 : 항목 클릭 이벤트 */
$(".transform-effect").click(function(){ 
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	console.log("tab2.jsp:440 - transform-effect.click");
	popupOn($(".transform-effect-modal"), data);
});

/* 템플릿, 장면(신)연출 : 효과 적용 */
$(".go-transform").click(function(){ //index.jsp:115 - 설정 완료 클릭
/* 	var bOffset = $(".canvas-code10").offset(); //캔버스의 위치 반환
	var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); //removeUrl() : url 좌우 필요없는 내용 제거 */
	var bOffset = $(".canvas-codeBG").offset(); //캔버스의 위치 반환
	var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image')); //removeUrl() : url 좌우 필요없는 내용 제거
	var bTop = bOffset.top; //top: 298.5
	var bLeft = bOffset.left; //left: 475.75
	var obj_wh;
	if(psd_img_mode == 'true'){
		bImgUrl = psd_layer_cut_imgurl;
		bTop = bOffset.top/shrinking_ratio;
		bLeft = bOffset.left/shrinking_ratio;
	}else{
		bTop = Math.round(bTop * img_resize_rate_h);
		bLeft = Math.round(bLeft * img_resize_rate_w);
	}
	var jsonString ="";
	console.log("tab2.jsp:318 - effectKind : " + effectKind);
	jsonString+="{ \"effectKind\":\"" + effectKind + "\",";
	jsonString+="\"effectTime\":" + $(".transform-effect-time").val() + ",";
	jsonString+="\"background\":{\"imgUrl\":\"" + bImgUrl + "\",\"top\":" + bTop + ",\"left\":" + bLeft+"},";
	jsonString+="\"obj\":[";
	var count = 0;
	for(var i=1;i<10;i++){ //for문 돌리는 이유? => 상단 저장된 이미지(분리된) 개수가 9개 + 배경 1개
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){ //for문을 돌려서 분리된 객체가 배경에 위치한 정보를 추가한다.
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			if(psd_img_mode == 'true'){
				top = (top/shrinking_ratio - ((btop_start_position/shrinking_ratio) - bTop));
				left = offset.left/shrinking_ratio;
			}else{
				obj_wh = ori2obj_resizing(i);
				top = Math.round(top * img_resize_rate_h);
				left = Math.round(left * img_resize_rate_w);
			}
			//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			var imgUrl = $(".canvas-code"+i).data("image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";
	console.log("tab2.jsp:480 - jsonString : " + jsonString);
	popupOff($(".transform-effect-modal"));
	var data={};
	data["data"]=jsonString;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content"); //csrf??
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content"); //THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
	       url : "/sceneApply/apply",
	       dataType : "json", 	//서버에서 반환되는 데이터 형식 지정
	       type : "POST",		//method 옵션의 또다른 이름, http 전송방식 설정
	       data : data, 		//key, value pairs를 통해 서버로 parameter를 전달
	       async : false, 		//동기방식 사용
	       timeout : 300000,	//제한시간 설정
	       success : function(data) { //통신이 성공하면 호출되는 함수
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab2.jsp:359 - videoUrl : " + data.videoUrl);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl + "'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) { //통신 중 실패했을 경우 호출되는 Event
	       	   alert("code:" + request.status + "\n" + "error:" + error);
	       }
	});
})



var imgEffectList={};
$(".go-start").click(function(){
	popupOff($(".effect-modal"));
	effect_time=$(".effect-time").val();
	//effectKind="move";
	alert("specify a start location.");
	$(".result-1").removeClass("dn");
	
});

$(".go-target").click(function(){
	popupOff($(".effect-modal"));
	effect_time=$(".effect-time").val();
	effect_time=$(".effect-time").val();
	effectKind="rotate";
	alert("Click on the ' Saved Images ' tab to apply the image to the effect.");
	$(".result-2").removeClass("dn");
});
/* 
$(".layered_img").click(function(){
	$(".draggg").removeClass('select-effect');
	$(this).addClass("select-effect");
}); */
//append element onclick by KH 2018-02-07
$('#layered_img').on('click', '.draggg',function() {
	$(".draggg").removeClass('select-effect');
	$(this).addClass("select-effect");
});

$(".result-btn-1").click(function(){
	if(startEndFlag==0){
		setFirstPosition();
		alert("specify the last location.");
		startEndFlag++;
	}else{
		$(".result-1").addClass("dn");
		setLastPosition();
		setBackgroundPosition();
	}
});

function setBackgroundPosition(){
/* 	var backgourndOffset = $(".canvas-code10").offset(); */
	var backgourndOffset = $(".canvas-codeBG").offset();
	if(psd_img_mode == 'true'){
		var top = backgourndOffset.top/shrinking_ratio;
		var left = backgourndOffset.left/shrinking_ratio;
		bImgUrl = psd_layer_cut_imgurl;
	}else{
		var top = backgourndOffset.top * img_resize_rate_h;
		var left = backgourndOffset.left * img_resize_rate_w;
		bImgUrl = $(".canvas-codeBG").data("image");
	}
	//console.log("top"+backgourndOffset.top+"  left:"+backgourndOffset.left);
	data+="\"background\" : {"+
	//"\"imgUrl\":\""+removeUrl($(".canvas-code10").css('background-image'))+"\","+
	"\"imgUrl\":\""+$(".canvas-codeBG").attr("data-image")+"\","+ 
/* 	"\"imgUrl\":\""+bImgUrl+"\","+ */
	"\"top\":"+top+","+
	"\"left\":"+left+""+
	"}}";
	moveObject(data); //moveObject.js:12
}	

var data="";
function setFirstPosition(){
	var obj_wh;
	var bTop = $(".canvas-codeBG").offset().top;
	data="";
	data+="{";
	var firstInfo = new Array();
	data+="\"firstInfo\" : [";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			var appendString ="{";	
			var offset = $(".canvas-code"+i).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio - ((btop_start_position/shrinking_ratio) - bTop/shrinking_ratio));
				left = offset.left/shrinking_ratio;
			}else{
				obj_wh = ori2obj_resizing(i);
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+i).data("image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
		}
	}	
	data +="],";
}
	
function setLastPosition(){
	var bTop = $(".canvas-codeBG").offset().top;
	var lastInfo = new Array();
	data+="\"lastInfo\" : [";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			var appendString ="{";	
			var offset = $(".canvas-code"+i).offset();
			if(psd_img_mode == 'true'){
				top = (offset.top/shrinking_ratio - ((btop_start_position/shrinking_ratio) - bTop/shrinking_ratio));
				left = offset.left/shrinking_ratio;
			}else{
				var obj_wh = ori2obj_resizing(i);
				var top = offset.top * img_resize_rate_h;
				var left = offset.left * img_resize_rate_w;
			}
			var imgUrl = $(".canvas-code"+i).data("image");
			appendString += "\"top\":"+top+",\"left\":"+left+",\"height\":\""+obj_wh.height+"\",\"width\":\""+obj_wh.width+"\",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				appendString+=",";
			}
			data+=appendString;
			forCount++;
		}
	}	
	data +="],";
	data +="\"effectKind\":\""+effectKind+"\",";
	data +="\"effectTime\":\""+effect_time+"\",";
	//console.log(data);
}
//check
function removeUrl(imgUrl){
	console.log("tab2.jsp : 748 removeUrl function: "+ imgUrl);
	imgUrl = imgUrl.replace("url(\"","");
	imgUrl = imgUrl.replace("\")","");
	return imgUrl;
}

//resizing

function ori2obj_resizing(canvas_code){
	//초기설정.(얼마나 늘어 났는지)	
	var a_width = ($(".wrap"+canvas_code).css('width').replace('px',''));
	var a_height = ($(".wrap"+canvas_code).css('height').replace('px',''));
	
	var result = {width: a_width, height: a_height};
	return result;
	
	
}
</script>