<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re">
	<!-- <div class="w100 fl">
		<div class="menu-cont-title p15">
			<span class="fl" style="margin-top:9px;">PSD 파일 분리하기</span>
		</div>		
		<div class="img-sort-list scrollbar-rail">		
			<div class="w100 fl srt">
				<div class="img-sort-item">
					<span class="fl">Image 1</span>
					<span class="fr cp">X</span>
				</div>
				<div class="img-sort-item">
					<span class="fl">Image 2</span>
					<span class="fr cp">X</span>
				</div>
				<div class="img-sort-item">
					<span class="fl">Image 3</span>
					<span class="fr cp">X</span>
				</div>		
				<div class="img-sort-item">
					<span class="fl">Image 4</span>
					<span class="fr cp">X</span>
				</div>
			</div>	
		</div>
	</div> -->
	<div class="w100 fl scroll-pd mt10">
		<div class="menu-cont-title">
			<span class="fl">Camera effect</span>
		</div>
		<div class="camera-effect">
			<div class="row">
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="1"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-1.png)" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="2"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-2.png)" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="3"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-3.png)" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="4"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-4.png)" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="5"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-5.png)" title="Zoom-out Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="6"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-6.png)" title="Zoom-in Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="7"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-7.png)" title="No Service"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="8"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-8.png)" title="Camera-Left Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="9"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-9.png)" title="Camera-Right Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="10"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-10.png)" title="Camera-Top Effect"></div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item" data-effecttype="11"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-11.png)" title="Camera-Bottom Effect"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
/* 카메라 연출(효과) 클릭 이벤트 */
$(".camera-effect-item").click(function(){
	var data = $(this).data('effecttype');
	effectKind = data;
	effectObjFlag = 0;
	console.log("tab3.jsp:90 - camera-effect-item.click : effectKind = " + ', ' + effectKind);
	$(".camera-effect-time").attr("value","3"); //체크
	popupOn($(".camera-effect-modal"), data);
});

/* 카메라 연출(효과) 설정완료 클릭 이벤트 */
$(".go-camera").click(function(){
	var bOffset = $(".canvas-codeBG").offset(); //index.jsp:114
	var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
	//$(".canvas-code10").css("width",backGroundWidth); //"960px"
	//$(".canvas-code10").css("height",backGroundHeight);
	//$(".canvas-codeBG").css("width",backGroundWidth); //"960px"
	//$(".canvas-codeBG").css("height",backGroundHeight);
	console.log("tab3.jsp:99 - go-camera.click");
	console.log(bImgUrl);
	var bTop = bOffset.top;
	var bLeft = bOffset.left;
	var jsonString = "";
	jsonString+="{\"effectKind\":\"" + effectKind + "\",";
	jsonString+="\"effectTime\":" + $(".camera-effect-time").val() + ",";
	jsonString+="\"background\":{\"imgUrl\":\"" + bImgUrl + "\",\"top\":" + bTop + ",\"left\":" + bLeft+",";
	jsonString+="\"imgWidth\":" + backGroundWidth + ",\"imgHeight\":" + backGroundHeight + "},"; //add by shkwak, 2017-07-12
	jsonString+="\"obj\":[";
	var count = 0;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<10;i++){
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";	
	console.log("tab3.jsp:130 - jsonString : ");
	console.log(jsonString);
	popupOff($(".camera-effect-modal"));
	var data={};
	data["data"]=jsonString;
	$.ajax({
	       url : "/make3D/apply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async:false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab3.jsp:144 - videoUrl : ");
	    	   console.log(data.videoUrl);
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("" +
	                   "<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	    });
	
});
	$(function(){
		var $w = $(".tab3").outerWidth();
		
		$(".img-sort-list").scrollbar({});
		$(".srt").sortable({
			axis: "y" ,
			sort:function(){
				$(".img-sort-list").css({width:$w});
			},
			stop: function( ) {
				$(".img-sort-list").css({width:'auto'});
			}
		});
	
		if($(".img-sort-item").length >= 4){
			$(".img-sort-list").css("width","auto");
		}else{
			$(".img-sort-list").css("width","100%");
		}
		
	})
</script>