<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re">
	<div class="w100 fl scroll-pd mt10">
		<div class="menu-cont-title">
			<span class="fl">Camera effect</span>
			<span class="fr help" title="도움말" onclick="javascript:tab3('en');" style="padding-bottom:10px;">
				<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
			</span>
		</div>
		<div class="camera-effect" id="tab3_1st">
			<div class="row">
				<!-- <div class="col-xs-4 not-allowed">
					<div class="camera-effect-item disabled effect_titleName btn_disabled" data-effecttype="1"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-1.png)" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<!-- <div class="col-xs-4 not-allowed">
					<div class="camera-effect-item disabled effect_titleName btn_disabled" data-effecttype="2"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-2.png)" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<!-- <div class="col-xs-4 not-allowed">
					<div class="camera-effect-item disabled effect_titleName btn_disabled" data-effecttype="3"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-3.png)" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<!-- <div class="col-xs-4 not-allowed">
					<div class="camera-effect-item disabled effect_titleName btn_disabled" data-effecttype="4"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-4.png)" title="No Service"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="5"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-5.png)" title="Zoom-out Effect"
						data-cegif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Zoom-out</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="6"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-6.png)" title="Zoom-in Effect"
						data-cegif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Zoom-in</div>
				</div>
				<!-- <div class="col-xs-4 not-allowed">
					<div class="camera-effect-item disabled effect_titleName btn_disabled" data-effecttype="7"
						style="background-image: url(/resources/image/camera-effect/Camera-effect-7.png)" title="No Service"
						data-cegif="7" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">No Service</div>
				</div> -->
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="8"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-8.png)" title="Camera-Left Effect"
						data-cegif="8" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Camera-Left</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="9"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-9.png)" title="Camera-Right Effect"
						data-cegif="9" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Camera-Right</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="10"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-10.png)" title="Camera-Top Effect"
						data-cegif="10" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Camera-Top</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="11"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-11.png)" title="Camera-Bottom Effect"
						data-cegif="11" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Camera-Bottom</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="12"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-12.png)" title="Element Left"
						data-cegif="12" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Element Left</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="13"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-13.png)" title="Element Right"
						data-cegif="13" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Element Right</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="14"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-14.png)" title="Element Down"
						data-cegif="14" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Element Down</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="15"
						style="background-image: url(/resources/image/v2.0/effect/camera-effect/Camera-effect-15.png)" title="Element Top"
						data-cegif="15" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Element Top</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="16"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-16.png)" title="ZoomIn(LeftTop)"
					data-cegif="16" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomIn LeftTop</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="17"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-17.png)" title="ZoomIn(LeftBottom)"
					data-cegif="17" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomIn LeftBottom</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="18"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-18.png)" title="ZoomIn(RightTop)"
					data-cegif="18" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomIn RightTop</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="19"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-19.png)" title="ZoomIn(RightBottom)"
					data-cegif="19" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomIn RightBottom</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="20"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-20.png)" title="ZoomOut(LeftTop)"
					data-cegif="20" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomOut LeftTop</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="21"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-21.png)" title="ZoomOut(LeftBottom)"
					data-cegif="21" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomOut LeftBottom</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="22"
					style="background-image:url(/resources/image/v2.0/effect/camera-effect/Camera-effect-22.png)" title="ZoomOut(RightTop)"
					data-cegif="22" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomOut RightTop</div>
				</div>
				<div class="col-xs-4">
					<div class="camera-effect-item effect_titleName" data-effecttype="23"
					style="background-image:url(/resources/image//v2.0/effect/camera-effect/Camera-effect-23.png)" title="ZoomOut(RightBottom)"
					data-cegif="23" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">ZoomOut RightBottom</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
/* 카메라 연출(효과) 클릭 이벤트 */
$(".camera-effect-item").click(function(){
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	var data = $(this).data('effecttype');
	effectKind = data;
	effectObjFlag = 0;
	console.log("tab3.jsp:90 - camera-effect-item.click : effectKind = " + ', ' + effectKind);
	$(".camera-effect-time").attr("value","3"); //체크
	popupOn($(".camera-effect-modal"), data);
});

/* 카메라 연출(효과) 설정완료 클릭 이벤트 */
$(".go-camera").click(function(){

/* 	var bOffset = $(".canvas-code10").offset(); //index.jsp:114
	var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); */
	var bOffset = $(".canvas-codeBG").offset(); //index.jsp:114
	var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image'));
	//$(".canvas-code10").css("width",backGroundWidth); //"960px"
	//$(".canvas-code10").css("height",backGroundHeight);
	//$(".canvas-codeBG").css("width",backGroundWidth); //"960px"
	//$(".canvas-codeBG").css("height",backGroundHeight);
	console.log("tab3.jsp:99 - go-camera.click");
	console.log(bImgUrl);
	var bTop = bOffset.top;
	var bLeft = bOffset.left;
	var bWidth = backGroundWidth;
	var bHeight = backGroundHeight;
	if(psd_img_mode == 'true'){
		bImgUrl = psd_layer_cut_imgurl;
		bTop = bTop/shrinking_ratio;
		bLeft = bLeft/shrinking_ratio;
		bWidth = Math.round(bWidth/shrinking_ratio);
		bHeight = Math.round(bHeight/shrinking_ratio);
	}else{
		bTop = Math.round(bTop * img_resize_rate_h);
		bLeft = Math.round(bLeft * img_resize_rate_w);
	}
	var jsonString = "";
	jsonString+="{\"effectKind\":\"" + effectKind + "\",";
	jsonString+="\"effectTime\":" + $(".camera-effect-time").val() + ",";
	jsonString+="\"background\":{\"imgUrl\":\"" + bImgUrl + "\",\"top\":" + bTop + ",\"left\":" + bLeft+",";
	jsonString+="\"imgWidth\":" + bWidth + ",\"imgHeight\":" + bHeight + "},"; //add by shkwak, 2017-07-12
	jsonString+="\"obj\":[";
	var count = 0;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			count++;	
		}		
	}
	var forCount=1;
	for(var i=1;i<itemList_Length+1;i++){
		if($(".canvas-code"+i).length>0){
			jsonString +="{";	
			var offset = $(".canvas-code"+i).offset();
			var top = offset.top;
			var left = offset.left;
			if(psd_img_mode == 'true'){
				top = (top/shrinking_ratio - ((btop_start_position/shrinking_ratio) - bTop));
				left = left/shrinking_ratio;;
			}else{
				top = Math.round(top * img_resize_rate_h);
				left = Math.round(left * img_resize_rate_w);
			}
			/* var imgUrl = removeUrl($(".canvas-code"+i).css('background-image')); */
			var imgUrl = $(".canvas-code" + i + "").data("image");
			jsonString += "\"top\":"+top+",\"left\":"+left+",\"imgUrl\":\""+imgUrl+"\"}";
			if(forCount<count){
				jsonString+=",";
			}
			forCount++;
		}
	}
	jsonString+="]}";	
	console.log("tab3.jsp:130 - jsonString : ");
	console.log(jsonString);
	popupOff($(".camera-effect-modal"));
	var data={};
	data["data"]=jsonString;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
	       url : "/make3D/apply",
	       dataType : "json",
	       type : "POST",
	       data : data,
	       async:false,
	       success : function(data) {
	    	   var randomId = new Date().getTime();
	    	   insertVideoTab(data);
	    	   console.log("tab3.jsp:144 - videoUrl : ");
	    	   console.log(data.videoUrl);
			   $("#moveVideoResult").siblings('a').remove();
	    	   $("#moveVideoResult").remove();
	    	   $(".video-modal").append("" +
	                   "<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
	    	   popupOn($(".video-modal"));
	       },
	       error : function(request, status, error) {
	          alert("code:" + request.status + "\n" + "error:" + error);
	       }
	    });
	
});
	$(function(){
		var $w = $(".tab3").outerWidth();
		
		$(".img-sort-list").scrollbar({});
		$(".srt").sortable({
			axis: "y" ,
			sort:function(){
				$(".img-sort-list").css({width:$w});
			},
			stop: function( ) {
				$(".img-sort-list").css({width:'auto'});
			}
		});
	
		if($(".img-sort-item").length >= 4){
			$(".img-sort-list").css("width","auto");
		}else{
			$(".img-sort-list").css("width","100%");
		}
		
	})
</script>