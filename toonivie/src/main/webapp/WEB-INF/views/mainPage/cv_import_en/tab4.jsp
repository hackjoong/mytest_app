<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl effect-direction h100 re scroll-pd" >
	<div class="w100 fl mt10">
		<div class="menu-cont-title">
			<span class="fl">Template</span>
		</div>
		<div class="template-effect">
			<div class="row">
				<div class="col-xs-4"><!-- 꽃잎 효과 -->
					<div class="template-effect-item" style="background-image:url(/resources/image/template/template-1.png)" 
						data-effecttype="5" title="Flower Effect"></div>
				</div>
				<div class="col-xs-4"><!-- 낙엽 효과 --> 
					<div class="template-effect-item" style="background-image:url(/resources/image/template/template-2.png)" 
						data-effecttype="6" title="Leaf Effect"></div>
				</div>
				<div class="col-xs-4"><!-- 반짝이 효과 -->
					<div class="template-effect-item" style="background-image:url(/resources/image/template/template-3.png)" 
						data-effecttype="7" title="Twinkle Effect"></div>
				</div>
				<div class="col-xs-4"><!-- 속도선 효과 -->
					<div class="template-effect-item" style="background-image:url(/resources/image/template/template-4.png)" 
						data-effecttype="8" title="Speed Effect"></div>
				</div>
				<div class="col-xs-4"><!-- 하트 효과 -->
					<div class="template-effect-item" style="background-image:url(/resources/image/template/template-5.png)" 
						data-effecttype="9" title="Heart Effect"></div>
				</div>	
				<div class="col-xs-4"><!-- 비 효과, add by shkwak, 2017-06-29 -->
					<div class="template-effect-item" style="background-image:url(/resources/image/template/toolicon_4_rain.png)" 
						data-effecttype="10" title="Rain Effect"></div>
				</div>			
			</div>	
		</div>
	</div>
</div>	
<script>
var activeTab;
/* 템플릿 : 항목 클릭 이벤트 */
$(".template-effect-item").click(function(){ //체크 OK, transform-effect -> template-effect-item
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4"); //add by shkwak
	console.log("tab4.jsp:38 - transform-effect.click");
	popupOn($(".transform-effect-modal"), data); //index.jsp
});

/* 사용하지 않는 듯 */
$(".effect-frame").click(function(){		
	if(!$(".menu-cont-img-zone").is(":animated")){
		$(".effect-frame").removeClass("active-effect-tab");	   
	    $(this).addClass("active-effect-tab")
	    $(".menu-cont-img-zone").addClass("dn")
	    activeTab = $(this).data("tab");
	    setActiveTab(activeTab);
	    $("." + activeTab).removeClass("dn");
	}else{
	   	return
	}
});

function setActiveTab(tab){
	activeTab=tab;
	console.log("tab4.jsp:58 - setActiveTab(tab)");
}
</script>