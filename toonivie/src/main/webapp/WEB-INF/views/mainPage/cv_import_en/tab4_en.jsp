<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl effect-direction h100 re scroll-pd" >
	<div class="w100 fl mt10">
		<div class="menu-cont-title">
			<span class="fl">Template</span>
			<span class="fr help" title="help" onclick="javascript:tab4('en');" style="padding-bottom:10px;">
				<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
			</span>
		</div>
		<div class="template-effect" id="tab4_1st">
			<div class="row">
				<div class="col-xs-4"><!-- 꽃잎 효과 -->
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-1.png)" 
						data-effecttype="5" title="Flower Effect" data-tpgif="1" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Flower</div>
				</div>
				<div class="col-xs-4"><!-- 낙엽 효과 --> 
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-2.png)" 
						data-effecttype="6" title="Leaf Effect" data-tpgif="2" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Leaf</div>
				</div>
				<div class="col-xs-4"><!-- 반짝이 효과 -->
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-3.png)" 
						data-effecttype="7" title="Twinkle Effect" data-tpgif="3" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Twinkle</div>
				</div>
				<div class="col-xs-4"><!-- 속도선 효과 -->
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-4.png)" 
						data-effecttype="8" title="Speed Effect" data-tpgif="4" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Speed</div>
				</div>
				<div class="col-xs-4"><!-- 하트 효과 -->
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-5.png)" 
						data-effecttype="9" title="Heart Effect" data-tpgif="5" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Heart</div>
				</div>	
				<div class="col-xs-4"><!-- 비 효과, add by shkwak, 2017-06-29 -->
					<div class="template-effect-item effect_titleName" style="background-image:url(/resources/image/v2.0/effect/template/template-6.png)" 
						data-effecttype="10" title="Rain Effect" data-tpgif="6" onmouseover="change_gif(this)" onmouseout="change_jpg(this)"></div>
					<div class="effect_text">Rain</div>
				</div>			
			</div>	
		</div>
	</div>	
</div>

<script>
var activeTab;
$(".template-effect-item").click(function(){
	if(psd_img_mode == 'true'){
		auto_crop($('.canvas-codeBG'));
	}
	var data = $(this).data('effecttype');
	effectKind = data;
	$(".transform-effect-time").attr("value","4");
	console.log("tab4.jsp:38 - transform-effect.click");
	popupOn($(".transform-effect-modal"), data);
});

$(".effect-frame").click(function(){		
	if(!$(".menu-cont-img-zone").is(":animated")){
		$(".effect-frame").removeClass("active-effect-tab");	   
	    $(this).addClass("active-effect-tab")
	    $(".menu-cont-img-zone").addClass("dn")
	    activeTab = $(this).data("tab");
	    setActiveTab(activeTab);
	    $("." + activeTab).removeClass("dn");
	}else{
	   	return
	}
});

function setActiveTab(tab){
	activeTab=tab;
	console.log("tab4.jsp:58 - setActiveTab(tab)");
}
</script>