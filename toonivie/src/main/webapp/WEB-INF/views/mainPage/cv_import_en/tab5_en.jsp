<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re p15">	
	<div class="menu-cont-title"><!-- mt20 -->
		<span class="fl" style="margin-top:9px;">Text</span>
		<span class="fr help" title="도움말" onclick="javascript:tab5('en');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
		</span>			
	</div> 	
	<div class="w100 fl" id="tab5_1st">	
		<div class="w100 fl mt15 re apd-zone">
           	<div class="w100 fl tooni-title">
				<div class="input_title">Enter text</div>
				<div class="tooni-cont2" id="tab5_2nd" style="background">
					<textarea rows="8" cols="30" id="subtitle_data" placeholder="Text"></textarea>
				</div>
			</div>
           	<div class="w100 fl mt15">
           		<div class="input_title">Text color</div>
            	<select id="subtitle_color" class="input_text">
				    <option value="#000000" style="background:black">Black</option>
				    <option value="#ff0000" style="background:red">Red</option>
				    <option value="#ffff00" style="background:yellow">Yellow</option>
				    <option value="#ffffff" style="background:white">White</option>
				    <option value="#0000ff" style="background:blue">Blue</option>
				</select>
           		<div class="input_title mt15">Text Font</div>
            	<select id="subtitle_font" class="input_text">
				    <option value="Arial">Arial</option>
				    <option value="Verdana">Verdana</option>
				    <option value="Times New Roman">Times New Roman</option>
				    <option value="Courier New">Courier New</option>
				    <option value="serif">Serif</option>
				    <option value="sans-serif">Sans-Serif</option>
				</select>      	
           		<div class="input_title mt15">Text Size</div>
            	<select id="subtitle_size" class="input_text">
					<c:forEach var="num" begin="10" end="50" step="2">
						<c:choose>
							<c:when test = "${num eq '24'}">
								<option selected="selected" value="${num}px">${num}px</option>
							</c:when>
							<c:otherwise>
								<option value="${num}px">${num}px</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>  	
           	</div>
           	<div class="w100 fl mt15" id="tab5_6th">
           		<button type="button" class="btn-cyan fl set-subtitle" style="width:100%;">Create text</button>           		
           	</div>
           	<div class="w100 fl mt15" id="tab5_7th">
           		<button type="button" class="btn-cyan fl subtitle-effect-item" style="width:100%;">Apply</button>           		
           	</div>
		</div>										
	</div>	
</div>
<script>
var subtitle_data;
var canvas_txt;
var ctx_txt;
var text_url;
	$(".set-subtitle").click(function(){
		$(".wrap11").remove();
		if($("#subtitle_data").val() == ""){
			return;
		}
	
		subtitle_data = $("#subtitle_data").val();
		/* $(".effect_titleName").attr('title',$("#subtitle_data").val()); */
		
		console.log(subtitle_data);
		var subtitle_length = (subtitle_data.length * $("#subtitle_size").val().substring(0,2)-3)+40;
		
		var element = "" +
		"<div class=\"use-image-canvas-wrap wrap" + "11" + "\" style='width:" + subtitle_length + "px;height:" + "100" + "px'>" +
		"<div class=\"w100 fl h100\">" +
		"<canvas id=\"wrap11\""+" width='" + subtitle_length + "px' height='" + "100" + "px' class=\"use-image-canvas canvas-code" + "11" + "\"></canvas>" +
		"</div>" +
		"</div>";
		
		
		$(".canvas-append-zone").append(element);
		canvas_txt = document.getElementById('wrap11');
		ctx_txt = canvas_txt.getContext("2d");
		offsetX = canvas_txt.offsetLeft;
		offsetY = canvas_txt.offsetTop;
		
		ctx_txt.font      = "normal " + $("#subtitle_size").val() + " " + $("#subtitle_font").val();
		ctx_txt.fillStyle = $("#subtitle_color").val();
		ctx_txt.fillText(subtitle_data, 20, 60);
		
		$(".wrap11").draggable({
			disabled : false
		});
		canvas2dataurl();
	});
	
	$(".subtitle-effect-item").click(function(){
 		var data = $(this).data('effecttype');
 		
 		titleName = $("#subtitle_data").val();
 		/* alert(titleName); */
 		
		effectKind = data;
		$(".subtitle-effect-time").attr("value","4");
		console.log("tab5.jsp:38 - subtitle-effect.click");
		popupOn($(".subtitle-effect-modal"), data);
	});
	
	$(".insert-subtitle").click(function(){
		var effectKind = '';
		var effectTime = $(".subtitle-effect-time").val();
/* 		var bOffset = $(".canvas-code10").offset(); //캔버스의 위치 반환
		var bImgUrl = removeUrl($(".canvas-code10").css('background-image')); //removeUrl() : url 좌우 필요없는 내용 제거 */
		var bOffset = $(".canvas-codeBG").offset(); //캔버스의 위치 반환
		var bImgUrl = removeUrl($(".canvas-codeBG").css('background-image')); //removeUrl() : url 좌우 필요없는 내용 제거
		var bTop = bOffset.top; //top: 298.5
		var bLeft = bOffset.left; //left: 475.75
		var offset_obj = $(".wrap11").offset();
		var top = offset_obj.top;
		var left = offset_obj.left;
		var effectObjImgUrl = 'undefined';
		var degree = '0';
		var subtitle_data = $("#subtitle_data").val();
		
		canvas2dataurl(wrap11);
		
		var jsonString ="";
		jsonString+="{ \"effectKind\":\""+effectKind+"\",";
		jsonString+="\"effectTime\":"+effectTime+",";
		jsonString+="\"background\":{\"imgUrl\":\""+bImgUrl+"\",\"top\":"+bTop+",\"left\":"+bLeft+"},";
		jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"degree\":"+degree+",\"text_url\":\""+text_url+"\"},";
/*  		jsonString+="\"obj\":{\"imgUrl\":\""+effectObjImgUrl+"\",\"top\":"+top+",\"left\":"+left+",\"degree\":"+degree+"},";*/
		jsonString+="\"otherObj\":[";
		var count = 0;
		for(var i=1;i<10;i++){
			if($(".canvas-code"+i).length>0){
				count++;	
			}		
		}
		var forCount=1;
		for(var i=1;i<10;i++){
			if($(".canvas-code"+i).length>0){
				jsonString +="{";	
				
				var offset = $(".canvas-code"+i).offset();
				var top = offset.top;
				var left = offset.left;
				//var imgUrl = removeUrl($(".canvas-code"+i).css('background-image'));
				var imgUrl=$(".canvas-code"+i).data("image");
				var degree= mtod($(".wrap"+i).css("transform"));
				jsonString += "\"top\":"+top+",\"left\":"+left+",\"degree\":"+degree+",\"imgUrl\":\""+imgUrl+"\"}";
				if(forCount<count){
					jsonString+=",";
				}
				forCount++;
			}
		}
		jsonString+="]}";
		console.log(jsonString);
		var data={};
		data["data"]=jsonString;
		$.ajax({
		       url : "/addText/Apply",
		       dataType : "json",
		       type : "POST",
		       data : data,
		       async : false,
		       success : function(data) {
		    	   var randomId = new Date().getTime();
		    	   insertVideoTab(data);
		    	   console.log("tab2.jsp:272 - " + data.videoUrl);
		    	   $("#moveVideoResult").siblings('a').remove();
		    	   $("#moveVideoResult").remove();
		    	   $(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
		    	   popupOn($(".video-modal"));
		       },
		       error : function(request, status, error) {
		          alert("code:" + request.status + "\n" + "error:" + error);
		       }
		});
	});
	

	function canvas2dataurl(){
		var imgUrl_txt = canvas_txt.toDataURL();
		$.ajax({
		       url : "/addText/Save",
		       type : "POST",
		       data : {"imgUrl": imgUrl_txt},
		       async : false,
		       success : function(data) {
		    	   text_url = data; 
		       },
	    		error : function(request, status, error) {
	    			alert("code:" + request.status + "\n" + "error:" + error);
	    			loadingOff();
	    		}
		});
	}
	
	
</script>