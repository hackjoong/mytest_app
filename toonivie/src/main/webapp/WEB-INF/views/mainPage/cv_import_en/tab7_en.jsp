<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re">	
	<div class="menu-cont-title"><!-- mt20 -->
		<span class="fl" style="margin-top:9px;">Upload the platform,</span>
		<span class="fr help" title="도움말" onclick="javascript:tab7('en');" style="padding-bottom:10px;">
			<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
		</span>	
		<!-- button class="fr cp cut-img-add">PSD 파일  추가</button -->
	</div> 	
	<div class="w100 fl" id="tab7_1st">	
		<form id="upload_webtoon_movie1" action="/uploadWebtoon/upload" enctype="multipart/form-data">			
		<div class="w100 fl mt15 re apd-zone">
            	<!-- <input type="file" id="uploadfile" name="upfile" class='kinp upload-inp dn'>
				<button type="button" class="btn-cyan fl w100" id='upload-webtoon'>올리기</button> -->
				<div class="filebox" id="tab7_2nd">
						<input class="upload-name" value="Choose File" readonly="readonly">
						<label for="ex_filename">Search</label><input type="file" id="ex_filename" name="upfile" class='kinp upload-inp dn'>
					</div>            	
           	<div class="w100 fl tooni-title">
				<div class="tooni-head2">Webtoon information</div>
				<div class="tooni-cont2" style="background">
					<input type="text" name="id" value="${currentUser.getId() }" style="display: none;">
					<input type="text" name="genre" placeholder="Genre" id="tab7_3rd">
					<input type="text" name="title" placeholder="Title" id="tab7_4th">
					<input type="text" name="writer" placeholder="Writer" value="${sessionScope.currentUser.getNickname()}" id="tab7_5th">
					<textarea rows="8" cols="30" name="content" placeholder="content" id="tab7_6th"></textarea>
				</div>
			</div>
           	<div class="w100 fl mt15">
           		<button type="button" class="btn-cyan fl" id="tab7_7th" style="width:100%; border-radius:0;" onclick="upload_singlefile2()">Upload</button>           		
           	</div>
		</div>										
		</form>		
	</div>	
</div>
<style>
.filebox{
	width:100%;
}
.filebox input[type="file"] {
	position:absolute;
	width:1px;
	height:1px;
	padding:0;
	margin:-1px;
	overflow:hidden;
	clip:rect(0,0,0,0);
	border:0;
}
.filebox label {
	display:inline-block;
	/* padding:.2em .75em; */
	width: 55px;
	height: 25px;
	line-height: 25px;
	text-align: center;
	color:#fff;
	font-weight:normal;
	/* line-height:normal; */
	vertical-align:middle;
	cursor:pointer;
	border:none;
	/* border-radius:.25em; */
	margin-top:4px;
	background-color: #20beca;
}
/* named upload */
.filebox .upload-name {
	display:inline-block;
	height: 25px;
	line-height:normal;
	vertical-align:middle;
	background-color:#ffffff;
	border:1px solid #ebebeb;
	border-bottom-color:#e2e2e2;
	/* border-radius:.25em; */
	width:76.75%;
	color:#666;
	padding-left:2px;
}
</style>
<script>
var fileTarget = $('.filebox #ex_filename');

fileTarget.on('change', function() { // 값이 변경되면
	if (window.FileReader) { // modern browser
		var filename = $(this)[0].files[0].name;
	} else { // old IE
		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																			// 추출
	}

	// 추출한 파일명 삽입
	$(this).siblings('.upload-name').val(filename);
});


$("#upload-webtoon").click(function(){
	$("#uploadfile").click();
})
var wfilename = "";
var wfilename_origin = ""
var wUploaded = false; //psd file upload check
//Webtoon Movie - Single File Upload Code
//2017-04-11, shkwak
//2017-07-19, RE Use
function upload_singlefile2() {///upload_singlefile
		loadingOn();
		$('#upload_webtoon_movie1').ajaxForm({
			url: "/uploadWebtoon/upload?${_csrf.parameterName}=${_csrf.token}",
			method:"POST",
			enctype: "multipart/form-data", // 여기에 url과 enctype은 꼭 지정해주어야 하는 부분이며 multipart로 지정해주지 않으면 controller로 파일을 보낼 수 없음
			success: function(result){
				
				console.log(result); //.resultUrl);
				wfilename = result; //.resultUrl;
				alert(wfilename_origin + " 파일이 " + wfilename + " 파일명으로 서버에 저장되었습니다.");
				wUploaded = true;
				loadingOff();
			}
		});
		// 여기까지는 ajax와 같다. 하지만 아래의 submit명령을 추가하지 않으면 백날 실행해봤자 액션이 실행되지 않는다.
		$("#upload_webtoon_movie1").submit();
		
	
}
</script>