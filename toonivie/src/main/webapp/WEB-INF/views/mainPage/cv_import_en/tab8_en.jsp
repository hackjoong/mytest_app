<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 h100 fl re">			   				
	<div class="w100 fl scroll-pd mt10" id="tab8_1st">
		<div class="menu-cont-title">
			<span class="fl">Vdeo Converter</span>
			<span class="fr help" title="도움말" onclick="javascript:tab8('en');" style="padding-bottom:10px;">
				<img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;">
			</span>	
		</div>
		<form id="transformForm" action="/ffmpegConvert" enctype="multipart/form-data">
		<div class="w100 fl mt20">
			<!-- <div class="input_title">Input Fille</div> -->
			<!-- <div class="input_title">Imput File</div>
			<input type="file" id="inputFile" class="input_text" name="upfile" value="" title="압축 변환용 파일을 선택해 주세요!">    --> 	
			
<!-- 		<input class="upload-name" value="파일선택" readonly="readonly">
			<label class="re_label" for="inputFile">검색</label><input type="file" id="inputFile" name="upfile" class='input_text dn'>
			<span id="resolution_text" style="color:#646768">1920x1080</span> -->
			
			<!-- <div class="input_title">Input File</div>
			<input type="file" id="inputFile" class="input_text" name="upfile" value="" title="압축 변환용 파일을 선택해 주세요!">
			<span id="resolution_text" style="color:white"></span> -->
			
			<!-- <div class="input_title">Input File</div>
			<input type="file" id="inputFile" class="input_text" name="upfile" value="" title="압축 변환용 파일을 선택해 주세요!">
			<span id="resolution_text" style="color:white"></span>  -->
			
			<div class="input_title">Input File</div>
			<div class="filebox" id="tab8_2nd">
				<input class="upload-name inputFileName" value="Choose File" readonly="readonly">
				<label for="inputFile">Search</label><input type="file" id="inputFile" value="" name="upfile" class="input_text dn">
				<span id="resolution_text" style="color:#20beca"></span> 
			</div>
			
    	</div>
<!--     	<div class="w100 fl mt5"> 
          	<button class="resolution btn-ivory" type="button" id="360p" data-resolution="360" style="width:23%;" onclick="">360p</button>
          	<button class="resolution btn-gray" type="button" id="480p" data-resolution="480" style="width:23%;" onclick="">480p</button>  
          	<button class="resolution btn-gray" type="button" id="720p" data-resolution="720" style="width:23%;" onclick="">720p</button>  
          	<button class="resolution btn-gray" type="button" id="1080p" data-resolution="1080" style="width:23%;" onclick="">1080p</button>  
       	</div>
       	<div class="w100 fl mt5"> 
          	<button class="resolution btn-ivory" type="button" id="360p" data-resolution="360" style="width:23%; font-size: 0.85em; height: 25px;" onclick="">360p 보기</button>
          	<button class="resolution btn-gray" type="button" id="480p" data-resolution="480" style="width:23%; font-size: 0.85em; height: 25px;" onclick="">480p 보기</button>  
          	<button class="resolution btn-gray" type="button" id="720p" data-resolution="720" style="width:23%; font-size: 0.85em; height: 25px;" onclick="">720p 보기</button>  
          	<button class="resolution btn-gray" type="button" id="1080p" data-resolution="1080" style="width:23%; font-size: 0.85em; height: 25px;" onclick="">1080p 보기</button>  
       	</div> -->
       	
 	     <div class="w100 fl mt5" id="tab8_3rd"> 
          	<button class="btn-gray disabled resolution" type="button" id="360p" data-resolution="360" disabled="disabled" onclick="">360p</button>
          	<button class="btn-gray disabled resolution" type="button" id="480p" data-resolution="480" disabled="disabled" onclick="">480p</button>  
          	<button class="btn-gray disabled resolution" type="button" id="720p" data-resolution="720" disabled="disabled" onclick="">720p</button>  
          	<button class="btn-gray disabled resolution" type="button" id="1080p"data-resolution="1080" disabled="disabled" onclick="">1080p</button>  
       	</div>
       	
       	<!-- <div class="input_title mt10">display resolution</div>
      	<div class="w100"> 
          	<a class="dn download_btn btn-gray resolution_view" id="360p_download" href="" download>360p</a>
          	<a class="dn download_btn btn-gray resolution_view" id="480p_download" href="" download>480p</a>
          	<a class="dn download_btn btn-gray resolution_view" id="720p_download" href="" download>720p</a>
          	<a class="dn download_btn btn-gray resolution_view" id="1080p_download" href="" download>1080p</a>
       	</div> -->
       	
       	<%--비디오 높이 넓이를 위한 태그--%>
    	<video class='dn' id="video_dimensions" controls>
			<source src="" id="video_loader">
		</video>
		<div class="w100 fl mt20">
			<a class="more_option fr" style="color:#20beca" id="tab8_4th" >More Option</a>
		</div>
		<div class="w100 more_option_content">
			<div class="w100 fl mt10">
				<div class="input_title">Sampling Rate</div>
				<!-- input type="text" class="input_text" value="44100" -->    
				<select id="sampling_rate" class="input_text" name="sampling_rate">
	    			<option value="11025">11025</option>
	    			<option value="22050">22050</option>
	    			<option value="44100" selected="selected">44100</option>
	   				<option value="48000">48000</option>
				</select>
	    	</div>
	    	<div class="w100 fl mt20">
				<div class="input_title">Bit Rate</div>			   
				<select id="bit_rate" class="input_text" name="bit_rate">
	    			<option value="16">16</option>
	    			<option value="32">32</option>
	    			<option value="56">56</option>
	    			<option value="128">128</option>
	   				<option value="192">192</option>
	   				<option value="" selected="selected">default</option>  				
				</select>
	    	</div>
	    	<div class="w100 fl mt20">
				<div class="input_title">Output Format</div>			   
				<select id="output_format" class="input_text" name="output_format">
	    			<option value="avi">avi</option>
	    			<option value="flv">flv</option>
	    			<option value="mp4" selected="selected">mp4</option>
	   				<option value="*">기타</option>   				
				</select>
	    	</div>
	    	<div class="w100 fl mt20 dn">
				<div class="input_title">Output Size</div>			   
				<input type="text" class="input_text read_only" value="960x540" readonly>
	    	</div>
	    	<div class="w100 fl mt20">
				<div class="input_title">Output File</div>			   
				<input type="text" id="outputFile" class="input_text cursor_a" value="" title="출력 파일명 입니다." placeholder='default : "input_file_name".확장자' name="ouputfilename">
	    	</div>
		</div>
		
	
    	
    	</form>
    	<div class="w100 fl mt15">
           	<button type="button" class="btn-cyan fl tab8-ffmpeg" id="tab8_5th" style="width:100%; border-radius:0;" disabled>Vdeo Converter</button>           		
        </div>
        <div class="input_title mt30 display_view" id="tab8_6th">Display resolution view</div>
      	<div class="w100 mb10"> 
          	<a class="disabled download_btn btn-gray resolution_view" id="360p_download" onclick="view_Display(this)">360p</a>
          	<a class="disabled download_btn btn-gray resolution_view" id="480p_download" onclick="view_Display(this)">480p</a>
          	<a class="disabled download_btn btn-gray resolution_view" id="720p_download" onclick="view_Display(this)">720p</a>
          	<a class="disabled download_btn btn-gray resolution_view" id="1080p_download" onclick="view_Display(this)">1080p</a>
       	</div>
        
       
	</div>
	
</div>

<style>
.input_text {
	width: 100%;
    border: 0 solid #bbb;
    background: #fff;
    height: 25px;
    /*text-align: center;*/
    font-size: 1.1em;
    cursor: pointer;
}

.read_only {
	/*text-align: left;*/
    background: #bbb;
    cursor: no-drop;
}

.cursor_d {
    cursor: default;
}

.cursor_a {
    cursor: auto;
}

.input_title {
	width: 100%;
    float: left;
    /* border: 2px solid #323235;
    padding-top: 15px;
    padding-bottom: 15px;
    text-align: center; */
    color: #646768;
    font-size: 0.8em;
    margin-bottom: 10px;
    position: relative;
}
.input_title:before {content:''; position: absolute; top: 7px; right: 0; width: 66%; height: 9px; background-image: url(/resources/image/v2.0/tab_dashed.png); background-size: 100%; background-repeat: no-repeat;}

.upload-name {
	display: inline-block;
    height: 25px;
    line-height: normal;
    vertical-align: middle;
    background-color: #ffffff;
    border: 1px solid #ebebeb;
    border-bottom-color: #e2e2e2;
    /* border-radius: .25em; */
    width: 76.75%;
    color: #666;
    padding-left: 2px;
}
.re_label {
	display: inline-block;
    /* padding: .2em .75em; */
    width: 55px;
    height: 25px;
    line-height: 25px;
    text-align: center;
    color: #fff;
    font-weight: normal;
    /* line-height: normal; */
    vertical-align: middle;
    cursor: pointer;
    border: none;
    /* border-radius: .25em; */
    margin-top: 4px;
    background-color: #20beca;
}
</style>
<script>
/* $(".tab8-ffmpeg").click(function(){
	$("#transformForm").ajaxForm({
		url : "/ffmpegConvert",
		method:"POST",
		enctype: "multipart/form-data",
		async:false,
	    success : function(data) {
	    	console.log(data.videoUrl);
	    	console.log("tab8.jsp:135 - videoUrl : " + data.videoUrl);
	    	$("#moveVideoResult").remove();
	    	$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl + "'></video>");
	    	popupOn($(".video-modal"));
		},
	    error : function(request, status, error) {
	        alert("code:" + request.status + "\n" + "error:" + error);
	    }
	});
	$("#transformForm").submit();
});

var inputFileName = "";
$(document).ready(function() {
	$("#inputFile").change(function() {	
		inputFileName = $(this)[0].files[0].name;
		console.log($(this)[0].files[0].name);
		console.log($(this)[0].files[0].size);
		var imsiName = $(this)[0].files[0].name;
		$("#outputFile").attr("value", imsiName.substring(0, imsiName.indexOf(".")));
		
		console.log($("#sampling_rate").val());
		console.log($("#bit_rate").val());
		console.log($("#output_format").val());
		
		$(".tab8-ffmpeg").attr("disabled", false);
	});
}); */
$(".tab8-ffmpeg").click(function(){
	$("#transformForm").ajaxForm({
 		url : "/ffmpegConvert",
		method:"POST",
		enctype: "multipart/form-data",
		data: {
			"resolution" : $('.resolution.btn-cyan').data('resolution')	
		},
		async:false,
	    success : function(data) {
	    	if(data.resolution == 360){
	    		$('#360p_download').attr('data-url', data.videoUrl).addClass('btn-ivory').removeClass('disabled btn-gray');
	    		/* $('#360p_download').addClass('btn-cyan').removeClass('disabled btn-gray'); */
	    	}else if(data.resolution == 480){
	    		$('#480p_download').attr('data-url', data.videoUrl).addClass('btn-ivory').removeClass('disabled btn-gray');
	    		/* $('#480p_download').addClass('btn-cyan').removeClass('disabled btn-gray'); */
	    	}else if(data.resolution == 720){
	    		$('#720p_download').attr('data-url', data.videoUrl).addClass('btn-ivory').removeClass('disabled btn-gray');
	    		/* $('#720p_download').addClass('btn-cyan').removeClass('disabled btn-gray'); */
	    	}else if(data.resolution == 1080){
	    		$('#1080p_download').attr('data-url', data.videoUrl).addClass('btn-ivory').removeClass('disabled btn-gray');
	    		/* $('#1080p_download').addClass('btn-cyan').removeClass('disabled btn-gray'); */
	    	}
	    	console.log(data.videoUrl);
	    	console.log("tab8.jsp:135 - videoUrl : " + data.videoUrl);
	    	$("#moveVideoResult").siblings('a').remove();
	    	$("#moveVideoResult").remove();
	    	$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl + "'></video>");
	    	popupOn($(".video-modal"));
	    	$(".display_view").removeClass("dn");
		},
	    error : function(request, status, error) {
	        alert("code:" + request.status + "\n" + "error:" + error);
	    }
	});
	$("#transformForm").submit();
});

var inputFileName = "";
$(document).ready(function() {
	$("#inputFile").change(function() {	
		$('.resolution').attr('disabled',false);
		$('.resolution').removeClass('disabled');
		inputFileName = $(this)[0].files[0].name;
		$(".inputFileName").attr("value",inputFileName);
		/* alert(inputFileName); */
		console.log($(this)[0].files[0].name);
		console.log($(this)[0].files[0].size);
		//pre-load video to get Width n Height by KH 18-03-26
		var $source = $('#video_dimensions');
		$source[0].src = URL.createObjectURL($(this)[0].files[0]);
		console.log($source);
		var video = document.getElementById("video_dimensions");
 	    $("#video_dimensions").on("loadedmetadata", function () {
			distinct_input_video(video.videoHeight);
			//console.log(video.videoHeight);
 	    	$("#resolution_text").html("영상 크기 : "+video.videoWidth+"x"+video.videoHeight);
 	    	$('.download_btn').addClass('disabled');
	    }); 
		
		var imsiName = $(this)[0].files[0].name;
		$("#outputFile").attr("value", imsiName.substring(0, imsiName.indexOf(".")));
		
		console.log($("#sampling_rate").val());
		console.log($("#bit_rate").val());
		console.log($("#output_format").val());
		
		$(".tab8-ffmpeg").attr("disabled", false);
	});
	
	$(".resolution").click(function(){
		
		$(".resolution").addClass('btn-gray');
		$(".resolution").removeClass('btn-cyan');
		$(this).removeClass('btn-gray');
		$(this).addClass('btn-cyan');
	});
});

function distinct_input_video(height){
	$(".resolution").removeClass('btn-cyan');
	$(".resolution").addClass('btn-gray');
	if(height <= 360){
		$('#360p').removeClass('btn-gray');
		$('#360p').addClass('btn-cyan');
		/* $('#480p').addClass('disabled');
		$('#720p').addClass('disabled');
		$('#1080p').addClass('disabled'); */
	}else if(height <= 480){
		$('#480p').removeClass('btn-gray');
		$('#480p').addClass('btn-cyan');
		/* $('#720p').addClass('disabled');
		$('#1080p').addClass('disabled'); */
	}else if(height <= 720){
		$('#720p').removeClass('btn-gray');
		$('#720p').addClass('btn-cyan');
		/* $('#1080p').addClass('disabled'); */
	}else if(height <= 1080){
		$('#1080p').removeClass('btn-gray');
		$('#1080p').addClass('btn-cyan');
	}
}

function view_Display(pshtest){
	var viewId = $(pshtest).attr("id");
	var view_data = $(pshtest).data("url");
	/* alert(viewId); */
	var view_360p = $("#360p_download").attr("id");
	var view_480p = $("#480p_download").attr("id");
	var view_720p = $("#720p_download").attr("id");
	var view_1080p = $("#1080p_download").attr("id");
	
	if(viewId == view_360p){
		/* alert("성공360"); */
		$("#moveVideoResult").siblings('a').remove();
		$("#moveVideoResult").remove();
		$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + view_data + "'></video>");
		popupOn($(".video-modal"));
	}else if(viewId == view_480p){
		/* alert("성공480"); */
		$("#moveVideoResult").siblings('a').remove();
		$("#moveVideoResult").remove();
		$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + view_data + "'></video>");
		popupOn($(".video-modal"));
	}else if(viewId == view_720p){
		/* alert("성공720"); */
		$("#moveVideoResult").siblings('a').remove();
		$("#moveVideoResult").remove();
		$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + view_data + "'></video>");
		popupOn($(".video-modal"));
	}else if(viewId == view_1080p){
		/* alert("성공1080"); */
		$("#moveVideoResult").siblings('a').remove();
		$("#moveVideoResult").remove();
		$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='" + view_data + "'></video>");
		popupOn($(".video-modal"));
	}
}



$(".more_option").click(function(){
	$(".more_option_content").slideToggle();
});
</script>