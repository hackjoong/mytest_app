<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="w100 fl h100 re">
	<div class="w100 fl scroll-pd">
		<div class="menu-cont-title">
			<span class="fl">Image cut</span>
			<span class="fr help" title="help" onclick="tab9('en')"><img src="/resources/image/icon/question.png" style="width:17%; cursor:pointer; float: right;"></span>
		</div>
	</div>
	<div class="tab-pane" id="panel-333333">
		<form id="fileUploadWebtoonImg" action="/upload/uploadImg_wt" enctype="multipart/form-data"> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
		<div class="w90 fl ml15">				   				
			<div class="w100 fl re apd-zone mt10" id="tab9_1st">
				<div class='img-frame'>            	
	            	<img src='/resources/image/v2.0/webtoon_upload_en.png' class='upload-img upload-img-cut re_img_upload_preview'>
	            	<input type='file' name='wt_image' id='wt_image' class='kinp upload-inp' accept='image/gif, image/jpeg, image/png'>
	           	</div>
			</div>
			<!-- 추출된 웹툰 컷 출력 -->
	        <div class="w100 fl wt_cut_before" style="margin-top:60px" id="tab9_2nd">
	           	<button class=" fr btn-cyan disabled" type="button" id='wt_cut_save' style="width:100%;" >Image cut</button> 
     	    </div>
     	    <div class="menu-cont-title mt20">
				<span class="fl">Preview Image cut</span>
				<span class="fr" id='wt_cut_uploadCount'></span>
			</div>
			<div class="wtcut_box w100" id="tab9_3rd" style="overflow: auto; height: 445px; margin-top: 5px; display: inline-block; border:2px solid #20beca">
				<div class="imgPreview_">preview Image cut</div>
			</div>
  				<div class="w100 fl mt5" id="tab9_4th">
	           	<input class="w100" type="text" id="wt_cut_saveName" style="height: 30px; text-align: center; border:0px;" placeholder="Please enter a file name">
	         </div>
  				<div class="w100 fl mt5" id="tab9_5th">
	           	<button class="btn-cyan fr disabled" id="wt_cut_save_all" type="button" style="width:100%; height: 40px; border-radius:0;" onclick=''>Saving cut image</button> 
	         </div>
		</div>
		</form>
	</div>
</div>