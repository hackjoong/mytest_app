<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="Expires" content="-1">
<META http-equiv="Pragma" content="no-cache">
<META http-equiv="Cache-Control" content="No-Cache">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>TOONIVIE</title>

<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon_v2.1/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon_v2.1/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon_v2.1/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon_v2.1/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon_v2.1/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon_v2.1/favicon-72x72.png">

<link href="/resources/common/css/lib.css" rel="stylesheet">
<link href="/resources/common/css/uploadtoon_en.css" rel="stylesheet">
<link href="/resources/common/css/jquery-ui.min.css" rel='stylesheet'>
<link href="/resources/common/css/common.css" rel="stylesheet">
<link href="/resources/common/css/jquery.scrollbar.css" rel="stylesheet">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/jquery-ui.min.js"></script>
<script src="/resources/common/js/jquery.scrollbar.min.js"></script>
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/indexAjaxForm.js"></script>
<script src="/resources/common/js/templete.js"></script>
<script src="/resources/common/js/setCsrf.js"></script>
<script src="/resources/common/js/en/loading_en.js"></script>
<script src="/resources/common/js/en/webtoon_whole_en.js"></script>
<script src="/resources/common/js/jszip.js"></script>
<script src="/resources/common/js/FileSaver.js"></script>
<script src="/resources/common/js/ObjectDetection.js"></script>
<link rel="stylesheet" href="/resources/common/css/loading.css">
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- cropper add by KH 2018-02-22 -->
<link rel="stylesheet" href="/resources/common/css/cropper.css">
<script src="/resources/common/js/cropper.js"></script>
<script src="/resources/common/js/cropper_init.js"></script>
<!-- ROTATE  -->
<link href="/resources/common/css/RotateResize.css" rel="stylesheet">
<script src="/resources/common/js/jquery.ui.rotatable.min.js"></script>
<script src="/resources/common/js/en/colorTrans_en.js"></script>
<!-- <script src="/resources/common/js/rgbaToHex.js"></script> -->
<link rel="stylesheet" href="/resources/common/css/introjs.css">
<script src="/resources/common/js/intro.js"></script>
<script src="/resources/common/js/EffectIntro.js"></script>
</head>
<body>
<div class="popup-blackbg"></div>
<div class="heedo-popup">
		<!-- <div class="video-effect-1 dn">
		<video id="moveVideo" controls>
			
		</video>
	</div>
	<div class="video-effect-2 dn">
		<video id="rainVideo" controls>
			
		</video>
	</div> -->
	<!-- 비디오 모달 -->
	<div class="video-effect-modal effect-modal dn effect_modal_small">
		<div class="effect-header">
			<span class="effect-header-title">Webtoon movie resolution settings</span>
			<span class="effect-footer" style="margin-top:3px">(Made of native resolution if not selected.)</span>
		</div>
		<!-- <div class="effect-content">
			<input type="checkbox" class="resolution_value" value="360p">360p
			<input type="checkbox" class="resolution_value" value="480p">480p
			<input type="checkbox" class="resolution_value" value="720p">720p
			<input type="checkbox" class="resolution_value" value="1080p">1080p
		</div> -->
		<div class="effect-content">
			<div class="fl">
				<input type="checkbox" id="resolution_value1" class="resolution_value" value="360p">
				<label style="height: 20px; width: 20px; border:2px solid #ffffff;" for="resolution_value1"><spen></spen></label>
				<div class="fl ml20">360p</div>
			</div>
			<div class="fl ml25">
				<input type="checkbox" id="resolution_value2" class="resolution_value" value="480p">
				<label style="height: 20px; width: 20px; border:2px solid #ffffff; float: left;" for="resolution_value2"><spen></spen></label>
				<div class="fl ml20">480p</div>
			</div>
			<div class="fl ml25">
				<input type="checkbox" id="resolution_value3" class="resolution_value" value="720p">
				<label style="height: 20px; width: 20px; border:2px solid #ffffff;" for="resolution_value3"><spen></spen></label>
				<div class="fl ml20">720p</div>
			</div>
			<div class="fl ml25">
				<input type="checkbox" id="resolution_value4" class="resolution_value" value="1080p">
				<label style="height: 20px; width: 20px; border:2px solid #ffffff;" for="resolution_value4"><spen></spen></label>
				<div class="fl ml20">1080p</div>
			</div>
		</div>
		<!-- <input class="re_sceneNum" type="checkbox" id="chk1" onclick="checkboxThisScene(this.id);" checked="">
		<label style="position: absolute; height: 20px; width: 20px; margin-top:9px; border:2px solid #ffffff;" for="chk1"><spen></spen></label> -->
		<div class="effect-footer">
			<button type="button" class="btn-cyan make_video"
				style="width: 20%; height: 34px;" onclick="wt_movieMake()">Apply</button>
		</div>
	</div>
	<!-- 인물연출 모달 -->
	<div class="human-effect-modal effect-modal dn effect_modal_small">
		<div class="effect-header">
			<span class="effect-header-title">Set the time (seconds) of the production video.</span>
		</div>
		<div class="effect-content">
			<input type="text" class="effect-time">
		</div>
		<div class="effect-footer">
			<button type="button" class="btn-cyan go-start dn"
				style="width: 35%; height: 34px;">Apply</button>
			<button type="button" class="btn-cyan go-target dn"
				style="width: 35%; height: 34px;">Apply</button>
		</div>
	</div>
	
	<!-- 사물연출 모달 -->
		<div class="object-effect-modal dn effect_modal_small">
			<!-- effect-modal-2 -->
			<div class="effect-header">
				<span class="effect-header-title">Set the time (seconds) of the production video.</span>
			</div>
			<div class="effect-content">
				<input type="text" class="object-effect-time">
				<!-- effect-time2 -->
			</div>
			<div class="effect-footer">
				<button type="button" class="btn-cyan go-object"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- go-object -->
			</div>
		</div>
		

		<!-- 카메라연출 모달 -->
		<div class="camera-effect-modal dn effect_modal_small">
			<div class="effect-header">
				<span class="effect-header-title">Set the time (seconds) of the production video.</span>
			</div>
			<div class="effect-content">
				<input type="text" class="camera-effect-time">
				<!-- effect-time5 -->
			</div>
			<div class="effect-footer">
				<button type="button" class="btn-cyan go-camera"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- go-camera -->
			</div>
		</div>

		<!-- 장면연출, 탬플릿 모달 -->
		<div class="transform-effect-modal dn effect_modal_small">
			<div class="effect-header">
				<span class="effect-header-title">Set the time (seconds) of the production video.</span>
			</div>
			<div class="effect-content">
				<input type="text" class="transform-effect-time">
				<!-- effect-time4 -->
			</div>
			<div class="effect-footer">
				<button type="button" class="btn-cyan go-transform"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- go-transform -->
			</div>
		</div>
		
		<!-- 자막 모달  add by KH 2017-12-22-->
		<div class="subtitle-effect-modal dn effect_modal_small">
			<div class="effect-header">
				<span class="effect-header-title">Set text output time (seconds)</span>
			</div>
			<div class="effect-content">
				<input type="text" class="subtitle-effect-time">
				<!-- effect-time4 -->
			</div>
			<div class="effect-footer">
				<button type="button" class="btn-cyan insert-subtitle"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- insert-subtitle -->
			</div>
		</div>

				<!-- 컬러 팔레트 모달  add by KH 2018-03-05-->
		<div class="color-palette-modal dn effect_modal_small">
			<div class="effect-header">
				<span class="effect-header-title">Specify the number of colors to be extracted.</span>
			</div>
			<div class="effect-content">
				<input type="text" class="color-palette-count">
				<!-- effect-time4 -->
			</div>
			<div class="effect-footer">
				<button type="button" id="color_palette_get" class="btn-cyan"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- insert-subtitle -->
			</div>
		</div>

		<!-- 컬러 피커 모달  add by KH 2018-03-05-->
		<div class="color-picker-modal dn effect_modal_big" style="width: 800px !important;">
			<div class="effect-header">
				<canvas id="color_boxs_po" oncontextmenu="return false;" ></canvas>
				<!-- <img style="width: 170px; display: none;" src="/resources/image/v2.0/color_picker_img.png" alt="" id="color_base" /> -->
				
			</div>
			<div class="effect-content">
				<span class="effect-header-title" style="margin-bottom: 20px; display: block;">Choose the color you want to change.</span>
				<!-- <input type="color" id="color-picker" style="width: 80px;"> -->
				<label id="color_picker_preview" style="width:80px; height: 22px; background-color: black; vertical-align: middle; margin-bottom: -2px;"></label>
				<input type="text" id="color-picker-text" value="" style="width: 80px; border:1px solid #323235; text-align: center;">
				<!-- <span id="color-picker-text"></span> -->
				<!-- effect-time4 -->
			</div>
			<div class="effect-footer">
				<button type="button" id="color-picker-get" class="btn-cyan"
					style="width: 35%; height: 34px;">Apply</button>
				<!-- insert-subtitle -->
			</div>
		</div>
		<!-- 비디오 모달 -->
		<div class="video-modal dn"></div>

		<!-- PSD 이미지분리 진행바 추가 by shkwak, 2017-06-28 -->
		<!-- div class="effect-modal psd-progress-modal dn" style="width:700px;">		
		<div class="effect-header">
			<span class="effect-header-title">PSD 이미지분리 진행률</span>
		</div>
		<div class="effect-content">
			<div id="psd-progressbar" style="width:80%;"></div> 
		</div>			
	</div -->
	<!-- 캔버스 모달 -->
	<div id="canvas_madal" class="canvas-grapcut-modal effect-modal dn">
		<div class="effect-header"> 
			<span class="effect-header-title">check the Separate image</span>
		</div>		
		<div class="effect-content">
			<canvas class="canvas-before dn" id="canvas-after" style="both:clear" onmousemove="pointmove(event)" onmousedown="pointdown(event)" onmouseup="pointup(event)" oncontextmenu="return false;"></canvas>
		</div>
		<div class="effect-footer">
			<button class="btn-cyan fr" id="grabcut_withMask" type="button">Again Separate</button>
			<button class="btn-cyan fr" type="button" onclick="saveResultToList1()">Save</button>
		</div>
	</div>	
</div>	
   	<c:import url="/WEB-INF/views/header&footer/upload-header_en_2.jsp" />
   	

<script src='/resources/common/js/color_correction.js'></script>
<script src="/resources/common/js/en/grabcut_en.js"></script>
<script src="/resources/common/js/psdParse.js"></script>
<script src="/resources/common/js/dragndrop.js"></script>
<script src="/resources/common/js/moveObject.js"></script>
<script src="/resources/common/js/webdb.js"></script>

<!-- add by shkwak -->
<style>
.camera-effect-modal {
	width: 100%;
	padding: 25px;
	position: relative;
	float: left;
}

</style>

<script>


var savedBackImg = false; //배경 저장 여부 판단 bool 변수
//color_boxs
/* var colors =document.getElementById("color_boxs_po");
var ctx_colors = colors.getContext("2d");
var color_imgs= document.getElementById("color_base")
ctx_colors.drawImage(color_imgs,0,0,170,145); */



function test() {
	popupOn($(".psd-progress-modal"));
	$("#psd-progressbar").progressbar({
		value : 0
	});
}

/* modal Show 이벤트 */
function popupOn(e, data) {
	console.log("index.jsp:446 - popup, e : ↓, data : " + data);
	console.log(e);
	e.siblings().addClass("dn"); //$(".video-modal"), dn : dispaly none
	e.removeClass("dn");
	$(".popup-blackbg").show();
	$(".heedo-popup").show();
	$(".go-object").attr("data-obj", data); //설정 완료
}

/* modal Hide 이벤트 */
function popupOff(e) {
	e.addClass("dn");
	console.log("index.jsp:446 - popupOff, e : ↓");
	console.log(e);
	$(".popup-blackbg").hide();
	$(".heedo-popup").hide();
}



function clearRect_re(){
 	$(".wt_whole_img").hide();
	$("#canvas_bg").removeClass("dn"); 
	psd_c = document.getElementById("canvas_bg");
	psd_ctx = psd_c.getContext("2d");
	psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);
	context.clearRect(0, 0, canvas.width, canvas.height);
}

/* webDB 관련 : 초기화 */
//webdbInit();

/* grabcut 관련 */
//"웹툰 무비 만들기" 버튼 클릭 이벤트
var target_resolution = new Array();
$(".effect-cut-success-button").click(function() {
	popupOn($(".video-effect-modal"));	
});

function wt_movieMake(){
	try{
		target_resolution = $(".resolution_value:checked");
	}catch(err){
		target_resolution = '';
	}
	var count = 0;
	/* 기존 코드
			for(var i=1;i<50;i++){ //문제점 : 50이라는 임의의 수를 사용해야 함.
				if($(".videoList"+i).length>0){  //length : 1
					count++;
				}
			}
			console.log('count : ' + count);
			 */
			//대체 코드, modify by shkwak, 2017-06-16
			var j = 1;
			while ($(".videoList" + j++).length)
				count++;
			console.log('index.jsp:357 count : ' + count);

			var i = 1;
			var jsonString = "{ \"videoList\" : [ ";
			while ($(".videoList" + i).length) {
				if ($(".videoList" + i).data("use") == 'yes') {
					jsonString += "{ \"videoUrl\":\""
							+ $(".videoList" + i).data(
									"videourl") + "\",";
					jsonString += "\"videoWidth\":"
							+ $(".videoList" + i).data(
									"videowidth") + ","; //add by shkwak, 2017-06-19
					jsonString += "\"videoHeight\":"
							+ $(".videoList" + i).data(
									"videoheight") + ",";
					jsonString += "\"videoUse\":\""
							+ $(".videoList" + i).data("use")
							+ "\","; //add by shkwak, 2017-06-20, 27
					jsonString += "\"time\":"
							+ $(".videoList" + i).data(
									"effecttime") + ",";
					jsonString += "\"openEffect\":\"\",";
					jsonString += "\"closeEffect\":\"\"}";
				} else {
					console
							.log($(".videoList" + i)
									.data("use"));
				}
				console.log(i + ", " + count);
				if (i < count)
					jsonString += ","; //콤마(,) 찍는 조건식	
				i++;
			}
			jsonString += "],";
 			jsonString += "\"target_resolution\" : [ ";
			target_resolution.each(function(index){
				jsonString += "\""+this.value.replace("p","")+"\"";
				if(index+1 != target_resolution.length){
					jsonString += ",";
				}
			});
			jsonString +=  "]}"; 
			console.log("index.jsp:374 " + jsonString);
			console.log(jsonString);

			//실제 Scene 연결 처리 코드
			var data = {};
			data["data"] = jsonString;
			var csrfParameter = $(
					"meta[name='_csrf_parameter']").attr(
					"content");
			var csrfToken = $("meta[name='_csrf']").attr(
					"content");
			var csrfHeader = $("meta[name='_csrf_header']")
					.attr("content"); // THIS WAS ADDED
			var headers = {};
			headers[csrfHeader] = csrfToken;
			data[csrfParameter] = csrfToken;
			
			$.ajax({
				url : "/attach/attachVideo",
				dataType : "json",
				type : "POST",
				data : data,
				async : false,
				success : function(data) {
					console
							.log("index.jsp:386 videoUrl : "
									+ data.videoUrl.videoUrl);
					$('.resolution_value').prop("checked", false);
					$("#moveVideoResult").siblings('a').remove();
					$("#moveVideoResult").remove();
					console.log(data);
 					var resol_1080p = data.output_1080p == undefined? "dn" : "";
					var resol_720p = data.output_720p == undefined? "dn" : "";
					var resol_480p = data.output_480p == undefined? "dn" : "";
					var resol_360p = data.output_360p == undefined? "dn" : ""; 
					$(".video-modal")
							.append(
									"<video id=\"moveVideoResult\" controls><source src='" + data.videoUrl.videoUrl + "'></video>"+
									"<a class='"+resol_360p+"' href='"+data.output_360p+"' download>360p</a>"+
									"<a class='"+resol_480p+"' href='"+data.output_480p+"' download>480p</a>"+
									"<a class='"+resol_720p+"' href='"+data.output_720p+"' download>720p</a>"+
									"<a class='"+resol_1080p+"' href='"+data.output_1080p+"' download>1080p</a>");
					popupOn($(".video-modal"));
				},
				error : function(request, status, error) {
					alert("code:" + request.status
							+ "\n" + "error:" + error);
				}
			});
			$(".effect-cut-success-List").css('display','block');
}
	
function imsi() { //임시, 기존 $(".effect-cut-success-button").click(function() 코드
	var count=0;	
		for(var i=1;i<50;i++){
			if($(".videoList"+i).length>0){  //length : 1
				count++;
			}
		}

		var flag = 1;
		$(".hide-use-image").click(function() {
			$(".use-image").addClass("dn");
			$(".draggg").draggable({
				disabled : true
			});
		});
}
		
		$(document).ready(function() {
			var effectTop = $(".now-working").height() + 20;
			$(".left-menu-tabs-frame").scrollbar({})
			$(".now-working-content-scroll").scrollbar({})
			$(".effect-cut-content-scroll").scrollbar({});
			if ($(".now-working-content-item").legnth >= 4) {
				$(".if-scroll-padding-right").css({
					paddingRight : "15px"
				});
			} else {
				$(".if-scroll-padding-right").css({
					paddingRight : "0"
				});
			}
			$(".effect-cut").css({
				top : effectTop + "px"
			});
		});

var flag=1;
$(".hide-use-image").click(function(){
	$(".use-image").addClass("dn");
	$(".draggg").draggable({				
		disabled:true
	});
});

$(document).ready(function() {
	$("#background_img").on( 'scroll', function(){
		scroll_position = $('#background_img').scrollTop();
		//console.log($('#background_img').scrollTop());
		});
	$(".canvas-wrap").css({"width":$("body").width() - 82});
	var effectTop = $(".now-working").height() + 20;
	$(".left-menu-tabs-frame").scrollbar({})			
	$(".now-working-content-scroll").scrollbar({})
	$(".effect-cut-content-scroll").scrollbar({});
	if($(".now-working-content-item").legnth >= 4){
		$(".if-scroll-padding-right").css({paddingRight:"15px"});
	}else{
		$(".if-scroll-padding-right").css({paddingRight:"0"});
	}
	$(".effect-cut").css({top:effectTop+"px"});
	
	if($(".effect-cut-content-item").legnth >= 4){
		$(".if-scroll-padding-right").css({paddingRight:"15px"});
	}else{
		$(".if-scroll-padding-right").css({paddingRight:"0"});
	}
	
	//오른쪽 클릭 메뉴 비활성 by KH 2018-02-20
/* 	$(document).bind("contextmenu", function(e) {
	    return false;
	}); */
	
	//upper navi by KH 18-3-15
	$('.image_select_menu').click(function(){
		var menu_name = $(this).data('menu_name');
		if(menu_name == 'image_cut'){
			//image_cut
			$('.use-image-canvas-wrap').removeClass('dn');
			$('#background_img').removeClass('dn');
			$('.canvas-wrap').removeClass('dn').css('z-index','99');

			//psd_cut
			
			$('.second_saved_img').addClass('dn');
			//wt_cut
			$('.wt_whole_img').addClass('dn');
			/* $(".hide-this-content").css({
				transform : "rotate(0deg)"
			});
			$(".hide-this-content").parent().next(".hide-cont").css({
				maxHeight : "999px"
			}); */
			
			
		    
		    
		    $("#canvas-before").draggable({
		    	disabled : true,
		    });
		    $(".psd_Open-fg").addClass("disabled");	
		    $("#crop_Open_btn").addClass("disabled");	
		    $("#crop_Close_btn").addClass("disabled");	
		    $("#crop_btn").addClass("disabled");
		    
		

		    $(".psd_Layer_Preview").addClass("dn"); //레이어 이미지 미리보기 
			
			

		}else if(menu_name == 'psd_cut'){
			//image_cut
			/* $('.use-image-canvas-wrap').addClass('dn');
			$('#background_img').addClass('dn'); */
			
			$('.use-image-canvas-wrap').removeClass('dn');
			$('#background_img').removeClass('dn');
			//psd_cut
			
			//wt_cut
			$('.wt_whole_img').addClass('dn');
			/* $('.second_saved_img').removeClass('dn'); */
			
			/* $(".hide-this-content").css({
				transform : "rotate(0deg)"
			});
			$(".hide-this-content").parent().next(".hide-cont").css({
				maxHeight : "999px"
			}); */
			
			/* saved-image-item-wrapper second_saved_img */
			
			$('.second_saved_img').removeClass('dn');
			
			$(".psd_Layer_Preview").removeClass("dn"); //레이어 이미지 미리보기 
			
			
			
		}else if(menu_name == 'wt_cut'){
			//image_cut
			$('.use-image-canvas-wrap').addClass('dn');
			$('#background_img').addClass('dn');
			$('.canvas-wrap').addClass('dn');
			//psd_cut
			
			//wt_cut
			$('.wt_whole_img').removeClass('dn');
			/* $(".hide-this-content").css({
				transform : "rotate(180deg)"
			});
			$(".hide-this-content").parent().next(".hide-cont").css({
				maxHeight : "0"
			}); */
			
			$(".psd_Layer_Preview").addClass("dn"); //레이어 이미지 미리보기 
		}else if(menu_name == 'color_palette'){
			$('.tab_color_palette').removeClass('dn');
			$('.tab_color_correction').addClass('dn');
		}else if(menu_name == 'color_correction'){
			$('.tab_color_palette').addClass('dn');
			$('.tab_color_correction').removeClass('dn');
		}
	});
});
		

		var imgUrl;
		var downContents;
		var canvas = document.getElementById('canvas-before');
		var context = canvas.getContext('2d');
		var canvas_fg = document.getElementById('canvas-fg');
		var context_fg = canvas_fg.getContext('2d');
		var canvas_bg = document.getElementById('canvas-bg');
		var context_bg = canvas_bg.getContext('2d');
		
		var canvas_fg_bw = document.getElementById('canvas-fg-bw');
		var context_fg_bw = canvas_fg_bw.getContext('2d');
		var canvas_bg_bw = document.getElementById('canvas-bg-bw');
		var context_bg_bw = canvas_bg_bw.getContext('2d');
		
		var input_button = 0;
		var Gmode = 0;
		var filling_type = 0;
		var GmouseFlag = 0;
		var GmouseFlagBg = 0;
		var GmouseFlagFg = 0;

		function getPointDown(event) {
			var input_mouse = event.which;
			if(input_mouse == 1){
				if (Gmode == 0) {
					if(input_button == 0){
						context = canvas.getContext('2d');
						context_fg = canvas_fg.getContext('2d');
						context_bg = canvas_bg.getContext('2d');
						context.clearRect(0, 0, canvas.width, canvas.height);
						GbeX = event.offsetX;
						GbeY = event.offsetY;
						GmoX = GbeX;
						GmoY = GbeY;
						canvasFlag = 1;
					}else if(input_button == 2){
					}
				} else if (Gmode == 1) {
					if(input_button == 0){
						bgListX=new Array();
						bgListY=new Array();
						context.clearRect(0, 0, canvas.width, canvas.height);
						context.beginPath();
						GmouseFlagBg = 1;
					}else if(input_button == 2){
					}
				} else if (Gmode == 2) {
					GmouseFlagFg = 1;
				} else if (Gmode == 3) {
					GmouseFlagBg = 1;
					
			 		context_fg.lineWidth=$('#fg_width').val();
			 		context_fg_bw.lineWidth=$('#fg_width').val();
			 		context_bg.lineWidth=$('#bg_width').val();
			 		context_bg_bw.lineWidth=$('#bg_width').val();
			 		
					if(input_button == 0){
						context_fg.moveTo(event.offsetX, event.offsetY);
						context_fg.beginPath();
						
						context_fg_bw.moveTo(event.offsetX, event.offsetY);
						context_fg_bw.beginPath();
					}else if(input_button == 2){
						context_bg.moveTo(event.offsetX, event.offsetY);
						context_bg.beginPath(); 
						
						context_bg_bw.moveTo(event.offsetX, event.offsetY);
						context_bg_bw.beginPath(); 
					}
				}
			}else{
				init_color_picker();
			}
		}
		function getPointMove(event) {
			input_button = event.button;
			if (Gmode == 0) {
				if (canvasFlag == 1) {
					context.clearRect(0, 0, canvas.width, canvas.height);
					context.clearRect(GbeX, GbeY, GmoX - GbeX, GmoY - GbeY);
					context.beginPath();
					GmoX = event.offsetX;
					GmoY = event.offsetY;
					context.rect(GbeX, GbeY, GmoX - GbeX, GmoY - GbeY);
					context.stroke();
					//context.strokeRect(GbeX, GbeY, GmoX - GbeX, GmoY - GbeY);
				}
			} else if (Gmode == 1) {
				if (GmouseFlagBg == 1) {
					bgListX.push(Math.round(event.offsetX * img_resize_rate_w));
					bgListY.push(Math.round(event.offsetY * img_resize_rate_h));
					context.lineTo(event.offsetX, event.offsetY);
	    			context.stroke();
				}
			} else if (Gmode == 2) {
				if (GmouseFlagFg == 1) {
					fgListX.push(event.offsetX);
					fgListY.push(event.offsetY);
				}
			} else if (Gmode == 3) {
				if (GmouseFlagBg == 1){
					if(input_button == 0){
						context_fg.lineTo(event.offsetX, event.offsetY);
						//context_fg.lineWidth=5;
						context_fg.lineCap = 'round'
						context_fg.lineJoin = 'round'
						context_fg.strokeStyle = '#fc3425';
						context_fg.stroke();
						
						context_fg_bw.lineTo(event.offsetX, event.offsetY);
						//context_fg_bw.lineWidth=5;
						context_fg_bw.lineCap = 'round'
						context_fg_bw.lineJoin = 'round'
						context_fg_bw.strokeStyle = '#ffffff';
						context_fg_bw.stroke();
						
					}else if(input_button == 2){
						context_bg.lineTo(event.offsetX, event.offsetY);
						//context_bg.lineWidth=10;
						context_bg.lineCap = 'round'
						context_bg.lineJoin = 'round'
						context_bg.strokeStyle = '#4286f4';
						context_bg.stroke();

						context_bg_bw.lineTo(event.offsetX, event.offsetY);
						//context_bg_bw.lineWidth=10;
						context_bg_bw.lineCap = 'round'
						context_bg_bw.lineJoin = 'round'
						context_bg_bw.strokeStyle = '#ffffff';
						context_bg_bw.stroke();
					}
					 $('#canvas-fg').mouseleave(function (e) {
						 GmouseFlagBg = 0;
					        return;
					    });
					 $('#canvas-bg').mouseleave(function (e) {
						 GmouseFlagBg = 0;
					        return;
					    });
				}
			}
		}

		function getPointUp(event) {
			var input_mouse = event.which;
			if(input_mouse == 3){
				if(color_picker_init == 'true'){
					clicked_color_rgb = [];
					//var colorLabel = document.getElementById('grabcut_color_picker_pre');
					var colorLabel_re = document.getElementById('grabcut_color_picker');
					var x = event.offsetX;
					var y = event.offsetY;
					var imageData = context.getImageData(x, y, 1, 1);
					var pixels = imageData.data;
					var rgbaColor = 'rgba(' + pixels[0] + ',' + pixels[1] + ',' + pixels[2] + ',1)';
				 	//colorLabel.style.backgroundColor = rgbaColor;
					colorLabel_re.style.backgroundColor = rgbaColor; 
					clicked_color_rgb.push(pixels[0]);
					clicked_color_rgb.push(pixels[1]);
					clicked_color_rgb.push(pixels[2]);
				}
			}
			input_button = event.button;
			GmouseFlagBg == 0;
			GmouseFlagFg == 0;
			canvasFlag = 0;
			if (Gmode == 0) {
				if(input_button == 0){
					GafX = event.offsetX;
					GafY = event.offsetY;
					//context.strokeRect(GbeX, GbeY, GafX - GbeX, GafY - GbeY);
					if(ROI_status == 'true'){
						get_count();
					}else{
						grabcutExecute();
					}
					
				}else if(input_button == 2){
					
				}
			} else if (Gmode == 1) {
				if(input_button == 0){
					if (GmouseFlagBg == 1) {
						GmouseFlagBg=0;
						grabcutExecute();
					}
				}else if(input_button == 2){
					
				}
			} else if (Gmode == 2) {
				if (GmouseFlagFg == 1) {
					grabcutExecute();
				}
			} else if (Gmode == 3) {
				GmouseFlagBg=0;
				//context_fg.closePath();
				//context_bg.closePath();
				//grabcutExecute();
			}
		}
/* fg bg 붓 크기 by KH 2018-02-20*/
	 	$(document).on('input', "#fg_width", function () {
	 		context_fg.lineWidth=$('#fg_width').val();
	 		context_fg_bw.lineWidth=$('#fg_width').val();
	 		$('#fg_width_val').html($('#fg_width').val());
		});
	 	$(document).on('input', "#bg_width", function () {
	 		context_bg.lineWidth=$('#bg_width').val();
	 		context_bg_bw.lineWidth=$('#bg_width').val();
	 		$('#bg_width_val').html($('#bg_width').val());
		});
 

/* 이미지 fg bg 선택 by KH 2018-02-20*/
		function fg_bg_selection(){
			if(sailencyCut == 'true'){
				if($('#fg_bg').hasClass('btn-gray')){
					//활성화
					$('#fg_bg').html('FG-BG 컷 실행');
					$('#fg_bg').removeClass("btn-gray");
					$('#fg_bg').addClass('btn-cyan');
					Gmode = 3;
					//context_fg.beginPath();
					context_fg.clearRect(0, 0, canvas.width, canvas.height);
					context_fg_bw.clearRect(0, 0, canvas.width, canvas.height);
 
					//context_bg.beginPath();
					context_bg.clearRect(0, 0, canvas.width, canvas.height);
					context_bg_bw.clearRect(0, 0, canvas.width, canvas.height);
					
 					//context_fg_bw.beginPath(); 
					context_fg_bw.fillStyle = '#000000';
					context_fg_bw.fillRect(0, 0, canvas.width, canvas.height);
					context_fg_bw.fill();
					//context_bg_bw.beginPath(); 
					context_bg_bw.fillStyle = '#000000';
					context_bg_bw.fillRect(0, 0, canvas.width, canvas.height);
					context_bg_bw.fill();
					fg_mask = null;
					bg_mask = null;
				}else if($('#fg_bg').hasClass('btn-cyan')){
					//비활성
					$('#fg_bg').html('FG-BG 선택');
					$('#fg_bg').removeClass("btn-cyan");
					$('#fg_bg').addClass('btn-gray');
					
/* 					context_fg.beginPath(); 
 	 				context_fg.fillStyle = '#000000';
 					context_fg.fillRect(0, 0, canvas.width, canvas.height);
 					context_fg.fill(); */
 					
 					//context_fg.beginPath(); 
  					//context_fg.strokeStyle = '#ffffff';
					//context_fg.stroke();
					
					//백그라운드 rect 따로 맨뒤로 보낸는 방법 생각하기
					//context_bg.fillStyle = '#000000';
 					//context_bg.fillRect(0, 0, canvas.width, canvas.height);
 					
					//context_bg.strokeStyle = '#ffffff';
					//context_bg.stroke();  
					
/* 					fg_mask = canvas_fg.toDataURL();
					bg_mask = canvas_bg.toDataURL(); */
 					fg_mask = canvas_fg_bw.toDataURL();
					bg_mask = canvas_bg_bw.toDataURL();
					grabcutExecute();
					//$(".canvas-fg").addClass("dn");	
					//$(".canvas-bg").addClass("dn");	
					if($('#cutModeBtn1').hasClass('btn-cyan')){
						Gmode = 0;
					}else if($('#cutModeBtn2').hasClass('btn-cyan')){
						Gmode = 1;
					}
				}
				
			}
		}
		
/* 이미지 분리방식 선택 버튼 - 박스형, 자유형  by shkwak, 2017-10-30 */
		function setMode(modeInteger) {
			console.log("index.jsp:631 - setMode in!!");
			Gmode = modeInteger;
			$("#cutModeBtn1").removeClass('btn-gray').removeClass('btn-cyan');
			$("#cutModeBtn2").removeClass('btn-gray').removeClass('btn-cyan');
			if (Gmode == 0) {
				/* $("#cutModeBtn1").addClass('btn-cyan');
				$("#cutModeBtn2").addClass('btn-gray'); */
				
				$("#cutModeBtn1").attr("src","/resources/image/v2.0/cutModeBtn1_click.png");
				$("#cutModeBtn2").attr("src","/resources/image/v2.0/cutModeBtn2.png");
				$("#ObjectDetection").attr("src","/resources/image/v2.0/cutModeBtn3.png");
				
				//alert("박스형이 선택되었습니다.");
			} else if (Gmode == 1) {
				/* $("#cutModeBtn2").addClass('btn-cyan');
				$("#cutModeBtn1").addClass('btn-gray'); */
				
				$("#cutModeBtn1").attr("src","/resources/image/v2.0/cutModeBtn1.png");
				$("#cutModeBtn2").attr("src","/resources/image/v2.0/cutModeBtn2_click.png");
				$("#ObjectDetection").attr("src","/resources/image/v2.0/cutModeBtn3.png");
				//alert("자유형이 선택되었습니다.");
			}
			//alert(Gmode);
		}

/* 이미지 색상채우기 선택 by KH, 2017-12-21 */
		function set_filling_color(modeInteger) {
/* 			canvas = document.getElementById('canvas');
			ctx = canvas.getContext('2d'); */
			filling_type = modeInteger;
			$(".fill_color").each(function(){
				$(this).removeClass('btn-gray').removeClass('btn-cyan');
			})
			if (filling_type == 0) {
				/* $("#fill_black").addClass('btn-cyan');
				$("#fill_white").addClass('btn-gray');
				$("#fill_surrounding").addClass('btn-gray');
				$("#grabcut_color_picker").addClass('btn-gray'); */
				
				$("#fill_black").attr("src","/resources/image/v2.0/fill_black_click.png");
				$("#fill_white").attr("src","/resources/image/v2.0/fill_white.png");
				$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding.png");
				$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker.png").css("background-color","");
				$("#fill_auto").attr("src","/resources/image/v2.0/fill_auto.png");
				
				color_picker_init = 'false';
				//clicked_canvas.off('mousedown');
			} else if (filling_type == 1) {
				/* $("#fill_white").addClass('btn-cyan');
				$("#fill_black").addClass('btn-gray');
				$("#fill_surrounding").addClass('btn-gray');
				$("#grabcut_color_picker").addClass('btn-gray'); */
				
				$("#fill_black").attr("src","/resources/image/v2.0/fill_black.png");
				$("#fill_white").attr("src","/resources/image/v2.0/fill_white_click.png");
				$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding.png");
				$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker.png").css("background-color","");
				$("#fill_auto").attr("src","/resources/image/v2.0/fill_auto.png");
				
				color_picker_init = 'false';
				//clicked_canvas.off('mousedown');
			} else if (filling_type == 2){
				/* $("#fill_surrounding").addClass('btn-cyan');
				$("#fill_white").addClass('btn-gray');
				$("#fill_black").addClass('btn-gray');
				$("#grabcut_color_picker").addClass('btn-gray'); */
				
				$("#fill_black").attr("src","/resources/image/v2.0/fill_black.png");
				$("#fill_white").attr("src","/resources/image/v2.0/fill_white.png");
				$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding_click.png");
				$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker.png").css("background-color","");
				$("#fill_auto").attr("src","/resources/image/v2.0/fill_auto.png");
				clicked_canvas.off('mousedown');
				color_picker_init = 'false';
			} else if (filling_type == 3){
				//$("#fill_surrounding").addClass('btn-gray');
			    //$("#fill_white").addClass('btn-gray');
				//$("#fill_black").addClass('btn-gray');
				//$("#grabcut_color_picker").addClass('btn-cyan');
				//$("#grabcut_color_picker").removeClass('btn-gray')
				
				$("#fill_black").attr("src","/resources/image/v2.0/fill_black.png");
				$("#fill_white").attr("src","/resources/image/v2.0/fill_white.png");
				$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding.png");
				$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker_click.png");
				$("#fill_auto").attr("src","/resources/image/v2.0/fill_auto.png");
			} else if (filling_type == 4){
				//$("#fill_surrounding").addClass('btn-gray');
			    //$("#fill_white").addClass('btn-gray');
				//$("#fill_black").addClass('btn-gray');
				//$("#grabcut_color_picker").addClass('btn-cyan');
				//$("#grabcut_color_picker").removeClass('btn-gray')
				
				$("#fill_black").attr("src","/resources/image/v2.0/fill_black.png");
				$("#fill_white").attr("src","/resources/image/v2.0/fill_white.png");
				$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding.png");
				$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker.png").css("background-color","");
				$("#fill_auto").attr("src","/resources/image/v2.0/fill_auto_click.png");
			} 

		}

		$(function() {
			var wh = $(document).height() - 60;
			var th = wh / 11;
			var cbfw = $(window).width() - th;
			var sleft = 300 + th;
			$(".saved-image").css({
				left : "25%"
			}); //383px -> 400px -> 25%, shkwak, 2017-07-06 
			$(".canvas").css({
				height : 890
			});
			$(".octagon").css({
				height : wh
			});
			/* $(".left-menu2").css({
				left : 0
			}); */
			$(".tabs-son").css({
				width : 115
			});
			/* $(".tabs-son").css({
				height : 90
			}); */
			/* $(".tabs-son-close").css({
				width : 114
			}); */
			/* $(".left-menu-control").css({
				width : 114
			}); */
			$(".right-menu-inside").css({
				height : wh
			});
			$(".tabs-son").click(function() {
				if (!$(".left-menu-cont").is(":animated")) {
					$(".tabs-son").removeClass("active-left-tab");
					$(".scroll-element").css("display","none");
					$(this).addClass("active-left-tab")
					$(".left-menu-cont").hide();
					//탭 클릭시 draggable 활성여부
					/* $(".draggg").draggable({
						disabled : true
					}); */
					var activeTab = $(this).data("tabs");
					$("." + activeTab).fadeIn(function() {
						
						if ($(".left-menu-tabs-frame").children(".scroll-element").hasClass("scroll-scrolly_visible")) {
							$(".scroll-pd").css({
								"padding-left" : "15px",
								"padding-top" : "15px",
								"padding-bottom" : "15px",
								"padding-right" : "30px",
							});
						} else {
							$(".scroll-pd").css({
								"padding-left" : "15px",
								"padding-top" : "15px",
								"padding-bottom" : "15px",
								"padding-right" : "15px",
							});
						}
					});
				} else {
					return
				}
			});
		});

		$(".left-menu").click(function() {
			
			$(this).animate({
				left : "-300px"
			}, {
				queue : false,
				duration : 300
			});
			$(".left-menu2").delay(400).animate({
				left : 0
			}, {
				queue : true,
				duration : 300
			})
		});

		$(".tabs-son").click(function() {
			var tabs =  $(this).data("tabs");
			
			$(".left-menu-tabs-frame").stop().animate({
				width : "320px"
			});
		});

		$(".menu-cont-img-frame").click(function() {
			$(this).addClass("orange").siblings().removeClass("orange");
		});

		$(".tabs-son-close").click(function() {
				
			$(".left-menu-tabs-frame").stop().animate({
				width : "0"
			}, {
				queue : false,
				duration : 300
			});
			$(".tabs-son").removeClass("active-left-tab");
			/* $(this) */
		});

		$(".arrow").click(function() {
			if (!$(this).hasClass("gogogo")) {
				$(this).addClass("gogogo")
				$(".right-menu").animate({
					right : 0
				});
			} else {
				$(this).removeClass("gogogo")
				$(".right-menu").animate({
					right : "-380px"
				});
			}
		});

		$(".eft1").click(function() {
			$(".eft-1").removeClass("dn");
			$(".eft-2").addClass("dn");
		});

		$(".eft2").click(function() {
			$(".eft-2").removeClass("dn");
			$(".eft-1").addClass("dn");
		});

		$(document).on("change", ".upload-inp",	function() { //체크		
					readURL(this);
					if($(this).attr('name') == 'img'){
							$('#cut_img_ori').html('Image Separate');
						if ($(this).val() == "") {
							$(".upload-img-cut").attr("src", "/resources/image/toolicon_1.png");
							$(".cut-img").addClass("disabled").attr("disabled",	true); //$(".cut-img") : 이미지 따기 버튼
						} else {
							$(".cut-img").removeClass("disabled").attr("disabled", false);
							$(".cut-img-add").removeClass("disabled").attr("disabled", false);
							$(".color_change_btns").removeClass("disabled").attr("disabled", false);
						}
						if ($(".img-frame").length == 0) {
							$(".img-none").removeClass("dn");
						} else {
							$(".img-none").addClass("dn");
						}
					}else if($(this).attr('name') == 'psd'){
						
					}else if($(this).attr('name') == 'wt_image'){
						
						//init wt cut button
						$('#wt_cut_save').addClass('btn-cyan');
						$('#wt_cut_save').removeClass('btn-gray');
						$('#wt_cut_save').removeClass('disabled');
					}
				});
		function readURL(input) { //체크
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					if($(input).attr('id') == 'psd'){
						$(input).prev('img').css('display', 'none');
						$('#psd_file_name').html(input.files[0].name);
						$('#psd_file_name').attr('title', input.files[0].name);
					}else if($(input).attr('id') == 'img'){
						$(input).prev('img').attr('src', e.target.result);
					}else if($(input).attr('id') == 'wt_image'){
						$(input).prev('img').attr('src', e.target.result);
						$('#wt_cut_saveName').val((input.files[0].name).replace('.jpg', '').replace('.png', ''));
						//fileUploadWebtoonImg();
					}
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$(document).on("click", ".img-close-wrap", function() {
			if (confirm("이미지를 삭제하시겠습니까?") == true) {
				$(this).parents(".img-frame").remove();
				if ($(".img-frame").length == 0) {
					$(".img-none").removeClass("dn");
				} else {
					$(".img-none").addClass("dn");
				}
			} else {
				return false;
			}
		});

		$(".return-img").click(function() {
			$(".inpx").val("");
			$(".imgx").attr("src", "/resources/image/toolicon_1.png");
		});

		$(".popup-blackbg").click(function() {
			$(".popup-blackbg").hide();
			$(".heedo-popup").hide();
		});

		//Layer
		$(function() {
			var ww = $(window).width() - $(".layer-list").width();
			$(".layer-list").css({
				left : ww
			});
			$(".layer-list").draggable({
				containment : ".canvas",
				scroll : false,
				axis : "y"
			});
		})

		//CANVAS
		$(".go-edit").click(function() {
			var link = $(".orange").children("img").attr("src");
			var canvasWidth = $(".orange").data("width");
			var canvasHeight = $(".orange").data("height");
			if ($(".img-frame").length == 0) {
				alert("이미지를 업로드해주세요.");
			}
		});

		$('.go-next').click(function() {
			$(".canvas-before").addClass("dn");
			$(".canvas-footer").addClass("dn");
			$(".canvas-after").removeClass("dn");
			$(this).addClass("dn");
			$(".go-save").removeClass("dn");
		});

		$(".go-save").click(function() {
			$(this).addClass("dn");
			$(".go-back").removeClass("dn");
		});

		$(".go-back").click(function() {
			$(this).addClass("dn");
			$(".go-edit").removeClass("dn");
		});

		var imageObj = new Image();
		imageObj.onload = function() {
			context.drawImage(imageObj, 0, 0);
		};

		imageObj.src = '#';

		$(".hide-this-content").click(function() {
			if ($(this).hasClass('hide-saved-image-hide')) {
				$(this).removeClass('hide-saved-image-hide');
				$(this).css({
					transform : "rotate(0deg)"
				});
				$(this).parent().next(".hide-cont").css({
					maxHeight : "999px"
				});
			} else {
				$(this).addClass('hide-saved-image-hide');
				$(this).css({
					transform : "rotate(180deg)"
				});
				$(this).parent().next(".hide-cont").css({
					maxHeight : "0"
				});
			}
		});

		$(".kinp").change(function() {
			$(".canvas-wrap").css("z-index", "100");
		});	
		/*width auto by psh 2018-02-05*/
		function width_auto(){
			var image_count = $(".w_auto_slide").children().length;
			image_count = image_count * 94;
			$(".w_auto_slide").css('width', image_count+'px');
		}
		
		var selected_layer;
		/*cropper active by KH 2018-2-22*/
		$('#crop_btn').click(function(){
			var selected_layer_canvas = $('#'+selected_layer);
			if(psd_img_mode == 'true'){
				if(cropper_croped_status == 'true'){
					 //init ajax toblob 
						  selected_layer_canvas.cropper('getCroppedCanvas').toBlob(function (blob) {
						  var formData = new FormData();
						  //console.log(URL.createObjectURL(blob));
						  formData.append('imgFile', blob);
						  formData.append('shrinking_ratio', shrinking_ratio);
						  $.ajax('/uploadWebtoon/webtoon_upload_parts', {
						    method: "POST",
						    enctype: 'multipart/form-data',
						    data: formData,
						    processData: false,
						    contentType: false,
						    success: function (data) {
						      console.log('Upload success');
						      //cropped_image_append(name, "vimgSave");
						      cropped_image_append(selected_layer, data);
						      alert("저장");
						      $(".clxk").draggable({ 
									disabled : true,
							  });
						    },
						    error: function () {
						      console.log('Upload error');
						    }
						  });
					});
					/*  alert(selected_layer_canvas); */
					cropper_disable(selected_layer_canvas);
				}
				//active scroll, disable draggable
				var container = selected_layer_canvas.parent().parent();
				console.log(container);
				container.draggable('disable');
				container.css('height', '540px');
				container.css('overflow','auto');
				container.children('div').removeClass('h100');
				cropper_init(selected_layer_canvas);
			}
		});
		
		// psh 180316
		$('#crop_Open_btn').click(function(){
			$(this).removeClass('btn-gray').addClass('btn-cyan').addClass('disabled');
			$('#crop_Close_btn').addClass('btn-gray').removeClass('btn-cyan');
			
			$(".clxk").draggable({ 
				disabled : true,
			});
			
			var selected_layer_canvas = $('#'+selected_layer);
			if(psd_img_mode == 'true'){
				if(cropper_croped_status == 'true'){
					 //init ajax toblob 
						  selected_layer_canvas.cropper('getCroppedCanvas').toBlob(function (blob) {
						  var formData = new FormData();
						  //console.log(URL.createObjectURL(blob));
						  formData.append('imgFile', blob);
						  formData.append('shrinking_ratio', shrinking_ratio);
						  $.ajax('/uploadWebtoon/webtoon_upload_parts', {
						    method: "POST",
						    enctype: 'multipart/form-data',
						    data: formData,
						    processData: false,
						    contentType: false,
						    success: function (data) {
						      console.log('Upload success');
						      //cropped_image_append(name, "vimgSave");
						      cropped_image_append(selected_layer, data);
						      alert("저장");
						    },
						    error: function () {
						      console.log('Upload error');
						    }
						  });
					});
					/*  alert(selected_layer_canvas); */
					cropper_disable(selected_layer_canvas);
				}
				//active scroll, disable draggable
				var container = selected_layer_canvas.parent().parent();
				console.log(container);
				container.draggable('disable');
				container.css('height', '540px');
				container.css('overflow','auto');
				container.children('div').removeClass('h100');
				cropper_init(selected_layer_canvas);
			}
		});
		
 		/* 이미지 분리방식 선택 버튼 - 박스형, 자유형  by KH, 2018-02-27 */
		$('.psd_cut-img').click(function(){
			console.log("index.jsp:897 - psd setMode in!!");
			Gmode = $(this).data('gmode');
			$("#psd_cutModeBtn1").removeClass('btn-gray').removeClass('btn-cyan');
			$("#psd_cutModeBtn2").removeClass('btn-gray').removeClass('btn-cyan');
			if (Gmode == 5) {
				$("#psd_cutModeBtn1").addClass('btn-cyan');
				$("#psd_cutModeBtn2").addClass('btn-gray');
				//alert("박스형이 선택되었습니다.");
			} else if (Gmode == 4) {
				$("#psd_cutModeBtn2").addClass('btn-cyan');
				$("#psd_cutModeBtn1").addClass('btn-gray');
				//alert("자유형이 선택되었습니다.");
			}
			//alert(Gmode);
		}); 

		var blur_status = 'true';
		$('#grabcut_blur_status').click(function(){
			if(blur_status != 'true'){
				/* $('#grabcut_blur_status').addClass('btn-cyan');
				$('#grabcut_blur_status').removeClass('btn-gray'); */
				blur_status='true';
			}else {
				/* $('#grabcut_blur_status').addClass('btn-gray');
				$('#grabcut_blur_status').removeClass('btn-cyan'); */
				blur_status='false';
			}
		});
		
		
		
		/* $('.psd_pre').click(function(){
			var randomId = new Date().getTime();		
			psd_img_pre = $(this).data("image"); //선택한 자신의 이미지 경로
			psd_img_pre_height =  $(this).data("height"); //선택한 이미지의 높이
			 //alert(psd_img_pre);
			 //alert(psd_img_pre_height); 
			$(".psd_LPC_img").css("background","url('"+psd_img_pre+"?v="+randomId+"')");
			$(".psd_LPC_img").css("height",psd_img_pre_height);
		}); */
		
		$("#layered_img").on('click','.psd_pre', function(){
			var randomId = new Date().getTime();		
			psd_img_pre = $(this).data("image"); //선택한 자신의 이미지 경로
			psd_img_pre_height =  $(this).data("height"); //선택한 이미지의 높이
			/* alert(psd_img_pre); */
			/* alert(psd_img_pre_height); */
			$(".psd_LPC_img").css("background","url('"+psd_img_pre+"?v="+randomId+"')");
			$(".psd_LPC_img").css("height",psd_img_pre_height);
		});
		
		$('#color-picker-text').keyup(function(){
			var re_change_color =  $(this).val();
			/*alert(re_change_color);*/
			$("#color_picker_preview").css("background-color",re_change_color);
		});
		
		
		$('.re_abs_son').click(function(){
			/* alert($(this).data('tabs')); */
			var tab_num = $(this).data('tabs');
			/* alert(tab_num); */
			var select = tab_num.substring(3);
		    /* alert(select); */
			for(var i = 1; i < 10; i++){
				if(select == i){
					/* $(".tabb_"+i).css("background-image","url(/resources/image/v2.0/tabb_"+i+"_click.png)"); */
					/* $(".tabb_"+i).addClass('arrow_click'); */
					
				}else{
					/* $(".tabb_"+i).css("background-image","url(/resources/image/v2.0/tabb_"+i+".png)"); */
					/* $(".tabb_"+i).removeClass('arrow_click'); */
					
				}
			}
		});
		
		$('.video-modal').on('DOMNodeInserted','video', function (){
			$('#moveVideoResult').addClass('dn');
			setTimeout(function(){
				$('#moveVideoResult').removeClass('dn');
				var width = $('#moveVideoResult').width();
				if(width > 1280){
					$('#moveVideoResult').width("1000px");
					if(width > 1900){
						$('#moveVideoResult').width("1300px");
					}
				}
			},100);
		});
		
//grabcut mask setting by KH 180426
		
		//init mask canvas 
		var canvas_mask = document.getElementById('canvas-after');
		var context_mask = canvas_mask.getContext('2d');
		var clicked = false;
		
		function pointdown(event){
			input_button = event.which;
			clicked = true;
			if(input_button == 1){
				context_mask.moveTo(event.offsetX, event.offsetY);
				context_mask.beginPath();
				
				context_mask.moveTo(event.offsetX, event.offsetY);
				context_mask.beginPath();
			}else if(input_button == 3){
				context_mask.moveTo(event.offsetX, event.offsetY);
				context_mask.beginPath(); 
				
				context_mask.moveTo(event.offsetX, event.offsetY);
				context_mask.beginPath(); 
			}
		}
		function pointmove(event){
			if(clicked){
				if(input_button == 1){
					context_mask.lineTo(event.offsetX, event.offsetY);
					context_mask.lineWidth=10;
					context_mask.lineCap = 'round'
					context_mask.lineJoin = 'round'
					context_mask.strokeStyle = '#ffffff';
					context_mask.stroke();
					
				}else if(input_button == 3){
					context_mask.lineTo(event.offsetX, event.offsetY);
					context_mask.lineWidth=10;
					context_mask.lineCap = 'round'
					context_mask.lineJoin = 'round'
					context_mask.strokeStyle = '#000000';
					context_mask.stroke();
				}
			}
		}
		function pointup(event){
			clicked = false;
		}
		
		// effect gif change psh add 2018-04-26
		function change_gif(oj_n){
			var object_num = $(oj_n).data("ojgif");//사물
			var character_num = $(oj_n).data("ctgif");//캐릭터
			var camera_num = $(oj_n).data("cegif");//카메라연출
			var transform_num = $(oj_n).data("trgif");//장면전환
			var background_num = $(oj_n).data("bggif");//배경
			var template_num = $(oj_n).data("tpgif");//자동 효과 템플릿
			for(var i=1; i<99; i++){
				if(object_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/object-effect/object-effect-"+i+".gif)");
				}
				if(character_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/human-effect/human-effect-"+i+".gif)");
				}
				if(camera_num == i){
					//alert(camera_num);
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/camera-effect/Camera-effect-"+i+".gif)");
					
				}
				if(transform_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/transform-effect/transform-effect-"+i+".gif)");
				}
				if(background_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/background-effect/background-effect-"+i+".gif)");
				}
				if(template_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/template/template-"+i+".gif)");
				}
			}
		} 
		function change_jpg(oj_n){
			var object_num = $(oj_n).data("ojgif");//사물
			var character_num = $(oj_n).data("ctgif");//캐릭터
			var camera_num = $(oj_n).data("cegif");//카메라연출
			var transform_num = $(oj_n).data("trgif");//장면전환
			var background_num = $(oj_n).data("bggif");//배경
			var template_num = $(oj_n).data("tpgif");//자동 효과 템플릿
			for(var i=1; i<99; i++){
				if(object_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/object-effect/object-effect-"+i+".png)");
				}
				if(character_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/human-effect/human-effect-"+i+".png)");
				}
				if(camera_num == i){
					//alert(camera_num);
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/camera-effect/Camera-effect-"+i+".png)");
				}
				if(transform_num == i){
					//alert(camera_num);
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/transform-effect/transform-effect-"+i+".png)");
				}
				if(background_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/background-effect/background-effect-"+i+".png)");
				}
				if(template_num == i){
					$(oj_n).css("background-image","url(/resources/image/v2.0/effect/template/template-"+i+".png)");
				}
			}
		}
	</script>
</body>
</html>