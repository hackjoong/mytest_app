<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/jquery-ui.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/resources/common/js/form.js"></script>
<title>Insert title here</title>
</head>
<body>
	<form id="uploadExForm" action="/upload/ex" enctype="multipart/form-data"> 
	<input type="file" name="bigFile">aa
</form>
<button onclick="fileUploadEx()">업로드</button>
<script>
function fileUploadEx(){
	$('#uploadExForm').ajaxForm({
		url: '/upload/ex',
		method:"POST",
		enctype: "multipart/form-data",
		success: function(result){
			alert("success!");
		}
	});
	$("#uploadExForm").submit();
}
</script>
</body>
</html>