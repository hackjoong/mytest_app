<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>TOONIVIE</title>
<!-- favicon -->
<link rel="shortcut icon" href="/resources/image/favicon/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/resources/image/favicon/favicon-180x180.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/resources/image/favicon/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/resources/image/favicon/favicon-120x120.png">
<link rel="apple-touch-icon-precomposed" sizes="96x96" href="/resources/image/favicon/favicon-96x96.png">	
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/resources/image/favicon/favicon-72x72.png">

<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/loading.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<script src="/resources/common/js/loading.js"></script>
</head>
<style>
#list_paging{
	display: inline-block;
	text-align: center;
	width:100%;
	margin-top: 10px;
}
.pagenatoin-ul{
list-style: none;
}
.pagenation-ul>li {
display: inline;
padding: 2px 7px;
margin: 0;
font-size: 14px;
}
.nation_active{
	background-color: #2B314A;
	color: white;
}
.view_btns {
    color: #6AC599;
    border: none;
    border: 1px solid #6AC599;
    border-radius: 10px;
    background: rgba(0,0,0,0);
    float: right;
    transition: 0.5s;
    font-size:14px;
    padding:5px;
}
.re_toon_contents {width:25%; margin-bottom:15px;}
.re_toon_text {width: 95%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;}
@media (max-width:991px){
	.re_toon_contents {width:50%; margin-bottom: 0;}
}

</style>
<script>
$(function(){
	$('.view_btns').mouseover(function(){
		$(this).css({"color":"#fff","background-color":"#6AC599"})
	})
	$('.view_btns').mouseout(function(){
		$(this).css({"color":"#6AC599","background-color":"#fff"})
	})
	$('.kinds').click(function(){
		var a = $(this).attr('data-kind');
		if(a == 'origintoon') {
			$('.origin').addClass("active");
			$('.webmo').removeClass('active');
			$('.writerinfo_wonjak').removeClass("dn");
			$('.writerinfo_webmovie').addClass("dn");
		}
		else{
			$('.origin').removeClass("active");
			$('.webmo').addClass('active');
			$('.writerinfo_wonjak').addClass("dn");
			$('.writerinfo_webmovie').removeClass("dn");
		}
	})
})
</script>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
<div class="w100 fl">
	<div class="container pd-0">
		<div class="col-md-12 today pd-0">
			<div class="col-md-12 col-xs-12 today-wrap">
				<div class="col-md-2 col-xs-2 today-st pd-0 re">
					<span class="ab date-main"> <fmt:formatDate value="${date}"
							type="date" pattern="yyyy.MM.dd" />
					</span>
					<!-- <h1 class="ab" style="top:11%; left:13%;">오늘의<br/>웹툰</h1> -->
				</div>
				<div class="col-md-10 col-xs-10 today-imgt ">
					<c:import url="/WEB-INF/views/home/today/today_img_2.jsp" />
					<img src="/resources/image/leftbtn.png" class="ab lbtn"
						onClick="return slide('side',-1)"> <img
						src="/resources/image/rightbtn.png" class="ab rbtn"
						onClick="return slide('side',1)">
				</div>
			</div>
		</div>
		<div class="col-xs-12 genre pd-0">
			<div class="col-md-9 genre-wrap col-xs-12" style="padding:0; width:100%;">
				<ul class=" pd-0 genre-menu col-md-12">
					<li data-kind="webmovie" class="active kinds webmo col-md1 w50" style="font-size:15px; font-weight:bold;">웹툰무비</li>
					<li data-kind="origintoon" class="kinds origin col-md1 w50" style="font-size:15px; font-weight:bold;">원작웹툰</li>
					<li></li>
				</ul>
				<div class="w100 fl genre-cont">
					<div class="episode gen" id="myTable" style="">
						<h2 style="padding-left:15px;">작가소개</h2>
						<!-- 웹툰무비 클릭했을떄 -->
						<div class="writerinfo writerinfo_webmovie">
							<c:forEach items="${toonwriters }" var="writers">
								<div class="col-md-4 col-xs-6 toon_contents re_toon_contents">
									<div class="w100 fl conts contents">
										<div class="genre-cont-img cp re profile_writer" style="background-image: url('${writers.icon}');">
										</div>
										<div class="conts-cover w100" style="padding-bottom:16px;">
											<span class="searchTarget genre-cont-subtitle re_toon_text">닉네임 : ${writers.nickname }</span><br>
											<span class="searchTarget genre-cont-subtitle re_toon_text">대표작품 : ${writers.work }</span><br>
										</div>
										<div class="view_btn_wrap">
											<button class="view_btns" onclick="location.href='/webmovie2/${writers.tno}'">최신작품▶</button>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
						<!-- 원작웹툰 클릭했을떄 -->
						<div class="writerinfo writerinfo_wonjak dn">
							<c:forEach items="${oriwriters }" var="writers">
								<div class="col-md-4 col-xs-6 toon_contents re_toon_contents">
									<div class="w100 fl conts contents">
										<div class="genre-cont-img cp re profile_writer" style="background-image: url('${writers.icon}');">
										</div>
										<div class="conts-cover w100" style="padding-bottom:16px;">
											<span class="searchTarget genre-cont-subtitle re_toon_text">닉네임 : ${writers.nickname }</span><br>
											<span class="searchTarget genre-cont-subtitle re_toon_text">대표작품 : ${writers.work }</span><br>
										</div>
										<div class="view_btn_wrap">
											<button class="view_btns" onclick="location.href='/webtoon/${writers.tno}'">최신작품▶</button>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>				
						<!-- <div id="list_paging">
							 <ul class="pagenation-ul"> 
							 	<li class="cp"><a class=""><span><img src="/resources/image/icon/webmovieBtn/leftBtn.png" class="lbtn" style="width:3%;"></span></a></li> 
							 	<li class="cp"><a class=""><span><img src="/resources/image/icon/webmovieBtn/rightBtn.png" class="rbtn" style="width:3%;"></span></a></li> 
							 </ul> 
						</div> -->
					</div>
				</div>
			</div>
		</div>
		
</div>
</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
<style>
.profile_writer{
	background-size: cover;
	background-repeat: no-repeat;
}
</style>
</html>