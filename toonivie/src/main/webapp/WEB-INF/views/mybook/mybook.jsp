<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/resources/common/js/mypage.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/mypage.css">
<script src="/resources/common/js/form.js"></script>
<link rel="stylesheet" href="/resources/common/css/slidemyheader.css" >
<!-- add by ydh 180309 - header mybutton -->
<title>My books</title>
</head>
<body>

<c:if test="${currentUser == null }">
	<script>
		alert("로그인해주세요");
		location.href = "/login";
	</script>
</c:if>
<header class="mboo2">
	<div class="container">
		<img src="/resources/image/logo.png" alt="logo" class="logo m_re_logo logo_m" onclick="location.href='/'">
		<div id="header_info" style="margin-top:60px; width: auto;">
			<span class="fl" style="font-size: 17px; margin-top: 7px;"><b style="color: #516CB1;">${currentUser.getName()}</b>님</span>
			<!-- <button onclick="location.href='/logoutProcess.do'">로그아웃</button> -->
			<c:if test="${sessionScope.currentUser != null }">

						<!-- <button class="btn-login" style="float: right; margin-right:12%;">로그아웃</button> -->
						<ul class="fl">
							<li class="btn-login btn-my cp" style="border-radius:0; margin-right: 10%;"><span>MY</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">내 정보</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">내 서재</li>
									<li class="cp MYLIST" onclick="location.href='/logoutProcess.do'">로그아웃</li>
								<c:if test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
									<li class="cp MYLIST" onclick="location.href='/adminPage'">설정관리</li>
								</c:if>
								</ul>
							</li>
						</ul>
						<script>
						$(function(){
							$('.btn-my').mouseover(function(){
								$(this).find("ul").stop().slideDown();
							})
							$('.btn-my').mouseout(function(){
								$(this).find("ul").stop().slideUp();
							})
							$('.MYLIST').mouseover(function(){
								$(this).stop().css({"background-color":"#bbb","color":"#fff"});
								
							})
							$('.MYLIST').mouseout(function(){
								$(this).stop().css({"background-color":"#fff","color":"#767676"});
							})
						})
						</script>
					</c:if>
		</div>
	</div> 
</header>
<div id="admin_cont" style="height: auto;padding: 0;margin-bottom: 30px;">
	<div id="Context" class="fl" style="width: 100%;">
		<div class="fl w100 mybook-spot">
			<div class="inner" style="clear:both;">
				<img src="/resources/image/icon/myBooks.png" alt="logo" class="m_bookimg" style="width:70px;"><br/>
				<span class="id idInfo mboo3"><em>${currentUser.getName() }</em>님의 서재</span>
			</div>
		</div>
		<div class="inner mboo1 m_inner">
			<nav class="mybook_t">
			<ul class="nav nav-tabs tab-v2 num7">
				<li class="click1 active w25 m_nav_tabs_1"><a class="mboo5 m_mboo5" href="#">웹툰 무비</a></li>
				<!-- <li class="click2" style="background: none;"><a href="#">코믹스</a></li> -->
			</ul>
			</nav>
			<div class="wbox m_wbox" style="height: auto;">
<!--				<div class="pull-left" style="clear: both;">
						<select name="" id="toon_order" class="form-control mybook-select">
							<option value="/mypage/mybook/view_type/A/order/update"
								selected="selected">업데이트순</option>
							<option value="/mypage/mybook/view_type/A/order/title">작품제목순</option>
							<option value="/mypage/mybook/view_type/A/order/view">최신감상순</option>
						</select>
					</div>
-->
					<span class="order5text" style="width: 100%;">최근 본 6개만 보여줍니다.</span>
					<div class="pull-right m_pull_right">
						<div class="mybook-srch"style="position: relative;">
							<input type="text" name="search-title" id="srchVal" class="form-control" placeholder="작품명 검색">
							<button type="button" class="btn" style="position:absolute; top: 0; right: 0;">
								<img src="/resources/image/magnifier icon.png" id="serarchIcon" alt="search" onclick="myFunction()" style="width: 20px;">
							</button>
						</div>
					</div>
					
					<div class="mybook-empty" id="myTable">
						<ol class="lists re_mybook_empty" id="toon_list">
						<c:forEach var="myBooks" items="${BooksView }">
							<li class="li re_list_li">
								<div class="thumb m-b-5">
									<a class="mboo4 re_mboo4" href="/webmovie2/${myBooks.getTno() }">
										<img src="${myBooks.getThumbnail() }" class="thumbnail re_thumbnail"/>
									</a>
<!-- 									<span class="thumb-checked" style="z-index: 10">
										<input type="checkbox" name="toon_idx[]" id="toon_idx" value="4696">
									</span> -->
								</div>
								<div class="caption text-left">
                            		<span class="cate">${myBooks.getGenre() }</span>
                            		<strong class="tit searchTarget mboo6">${myBooks.getTitle() }</strong>
                       			</div>
							</li>
						</c:forEach>
						</ol>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>