<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style type="text/css">
.center {
	margin-top: 50px;
}

.modal-header {
	padding-bottom: 5px;
}

.modal-footer {
	padding: 0;
}

.modal-footer .btn-group button {
	height: 40px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	border: none;
	border-right: 1px solid #ddd;
}

.modal-footer .btn-group:last-child>button {
	border-right: 0;
}

.modal_user {
	margin-top: 40%;
}
</style>

<body>


	<!-- line modal -->
	<div class="modal fade" id="squarespaceModal" tabindex="-1"
		role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal_user">
				<div class="modal-header">
					<button type="button" class="close close_btn" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="lineModalLabel_memberinfo">회원정보수정</h3>
				</div>
				<div class="modal-body">

					<!-- content goes here -->
					<form name="form1" onsubmit="return submitCheck()"
						action="/members/modifyInfo" method="POST">
						<div class="form-group" style="width: 49%; float: left">
							<label for="modifyid">ID</label> <input type="text"
								class="form-control" id="modifyid" name="id"
								placeholder="ID" value="${currentUser.getId() }"
								readonly="readonly">
						</div>
						<div class="form-group" style="width: 49%; float: right;">
							<label for="modifyemail">이메일</label> <input
								type="email" class="form-control myemail" id="modifyemail"
								name="email" placeholder="이메일"
								value="${currentUser.getEmail() }" required="required">
						</div>
						<div class="form-group">
							<label for="modifyname">이름</label> <input type="text"
								class="form-control  mynameis" id="modifyname" name="name"
								placeholder="이름" value="${currentUser.getName() }">
						</div>
						<c:if
							test="${currentUser.getRole()=='ROLE_WRITER' }">
							<div class="form-group" style="width: 49%; float: left">
								<label for="modifynickname">닉네임</label> <input
									type="text" class="form-control mynick" id="modifynickname"
									name="nickname" placeholder="닉네임"
									value="${currentUser.getNickname() }">
							</div>
						</c:if>
						<c:if test="${currentUser.getRole()=='ROLE_WRITER' }">
							<div class="form-group" style="width: 49%; float: right;">
								<label for="modifydeapyo">대표 작품</label> <input
									type="text" class="form-control myjak" id="modifydeapyo"
									name="work" placeholder="대표작품"
									value="${currentUser.getWork() }">
							</div>
						</c:if>
						<c:if
							test="${currentUser.getRole()=='ROLE_COWORKER' }">
							<div class="form-group" style="width: 49%; float: left">
								<label for="exampleInputPassword1">담당 작가명(이름)</label> <input
									type="text" class="form-control mywriter" id="exampleInputPassword6"
									name="matching" placeholder="담당작가명(이름)"
									value="${currentUser.getMatching() }">
							</div>
						</c:if>
						<c:if test="${currentUser.getRole()=='ROLE_COWORKER' }">
							<div class="form-group" style="width: 49%; float: right;">
								<label for="exampleInputPassword1">전화번호</label> <input
									type="text" class="form-control mytel" id="coworkerphone"
									name="tel" placeholder="전화번호" value="${currentUser.getTel() }">
							</div>
						</c:if>
						<button type="submit" class="btn btn-default submit">수정</button>
					</form>

				</div>
				<div class="modal-footer">
					<div class="btn-group btn-group-justified" role="group"
						aria-label="group button">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default close_btn"
								data-dismiss="modal" role="button">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
function submitCheck() {
	var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
	var pwReg = /([0-9A-Za-z]{7,20})/g;
	var nameReg = /([가-힣A-Za-z]{2,4})/g;
	var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
	var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
	var idValue = $("[name = id]").val();
	var nameValue = $("[name = name]").val();
	var emailValue = $("[name = email]").val();
	var nicknameValue = $("[name = nickname]").val();
	var matchingValue = $("[name = matching]").val();
	var workValue = $("[name=work]").val();
	var telValue = $("[name=tel]").val();

	if (idReg.test(idValue) == false || idValue == "") {
		alert("아이디를 확인해주세요.");
		return false;
	} else if (emailReg.test(emailValue) == false || emailValue == "") {
		alert("이메일을 확인해주세요.");
		return false;
	} else if (nameReg.test(nameValue) == false || nameValue == "") {
		alert("이름을 확인해주세요.");
		return false;
	} else if(matchingValue == ""){
		alert("담당작가몀을 입력해주세요.");
	} else if (nicknameValue == "") {
		alert("닉네임을 확인해주세요.");
		return false;
	} else{
		$.ajax({
			url:'/members/modifyInfo',
			type:'POST',
			data:{
				'id':idValue,
				'name':nameValue,
				'email':emailValue,
				'nickname':nicknameValue,
				'work':workValue,
				'tel':telValue,
				'matching':matchingValue
			},
			success:function(data){
				if(data == 1){
					alert("회원님의 정보가 변경되었습니다.");
					location.href="/members/myPage";
				}
			},
			error: function(request, status, error) {
	            alert("code:" + request.status + "\n" + "error:" + error);
	        }	
		})
	}
}	


</script>

</body>
</html>