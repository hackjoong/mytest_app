<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style type="text/css">
.center {
    margin-top:50px;   
}

.modal-header {
	padding-bottom: 5px;
}

.modal-footer {
    	padding: 0;
	}
    
.modal-footer .btn-group button {
	height:40px;
	border-top-left-radius : 0;
	border-top-right-radius : 0;
	border: none;
	border-right: 1px solid #ddd;
}
	
.modal-footer .btn-group:last-child > button {
	border-right: 0;
}
.modal_pw{
width:340px;
margin-top:50%;
}
.idinfo{
	display: block;
    width: 100%;
    height: 34px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
}
@media (max-width:500px){
	.modal_pw{
	width:340px;
	margin-top:35%;
	}
}
</style>

<body>


<!-- line modal -->
<div class="modal fade" id="squarespaceModal_pwchange" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content modal_pw">
		<div class="modal-header">
			<button type="button" class="close close_btn" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">비밀번호 변경</h3>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form name="form1" action="/members/modifyPw" method="POST">
			<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}">
              <div class="form-group" style="width:100%; float: left">
                <label for="exampleInputEmail1">ID</label>
                <span class="idinfo">${currentUser.getId() }</span>
                <input type="hidden" value="${currentUser.getId() }" name="id"/>
              </div>
              <div class="form-group" style="width:49%; float: left;">
                <label for="newpw">새 비밀번호</label>
                <input type="password" class="form-control" id="newpw" name="pwd" minlength="8" maxlength="20" placeholder="비밀번호" required="required">
              </div>
              <div class="form-group" style="width:49%; float: right;">
                <label for="newpwconfirm">새 비밀번호 확인</label>
                <input type="password" class="form-control confirm_pwd" id="newpwconfirm" name="confirm_pw" minlength="8" maxlength="20" placeholder="비밀번호 확인" required="required">
              </div>
              <div class="check_pw_span" style="text-align: center; width:100%; margin: 0;"></div>
              <button type="button" class="btn btn-default modify_pw" style="clear:both;" onclick="pwSubmit()">수정</button>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button"  class="btn btn-default close_btn" data-dismiss="modal"  role="button">Close</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
<script>

$(function(){
	$(".confirm_pwd").keyup(function() {
		var pw = $("[name = pwd]").val();
		var re_pw = $(".confirm_pwd").val();
		if (pw != re_pw || pw == "" || re_pw == "") {
			$(".check_pw_span").css("color", "red");
			$(".check_pw_span").text("비밀번호가 일치하지 않습니다.");
		} else {
			$(".check_pw_span").css("color", "blue");
			$(".check_pw_span").text("비밀번호가 일치합니다.");
		}
	});

	$('.close_btn').click(function(){
		$('[name=pwd],[name=confirm_pw]').val('');
		$(".check_pw_span").text("");
	});
});

function pwSubmit(){
	var id = $('[name=id]').val();
	var pw = $("[name = pwd]").val();
	var confirm_pw = $("[name = confirm_pw]").val();
	
	if(pw==''||confirm_pw==''||pw==null||confirm_pw==null){
		alert("입력되지 않은 창이 있습니다. 입력해주세요.");
		return false;
	}
	else if(pw.length<8||confirm_pw.length<8){
		alert("8자 이상 20자로 해주세요!");
		return false;
	}
	
	else{
		$.ajax({
			url:'/members/modifyPw',
			type:'POST',
			data:{
				'id':id,
				'pwd':pw
			},
			success:function(data){
				if(data == 1){
					alert("비밀번호가 변경되었습니다.\n다시 로그인해 주세요.");
					location.href="/login";
				}
			},
			error: function(request, status, error) {
	            alert("code:" + request.status + "\n" + "error:" + error);
	        }	
		})
	}
}
</script>

</body>
</html>