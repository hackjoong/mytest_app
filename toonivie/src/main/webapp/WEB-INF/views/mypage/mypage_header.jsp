<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<link href="/resources/common/css/admin.css" rel="stylesheet">
<script src="/resources/common/js/form.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script src="/resources/common/js/mypage.js"></script>
<link rel="stylesheet" href="/resources/common/css/slidemyheader.css" >

<title>webtoon_AdminPage</title>
</head>
<script type="text/javascript">
$(document).ready(function() {
	$('li').css('background-color', "#fff");
	$(".menu>li").mouseover(function() {
		$(this).css({
			"background-color" : "#fff"
		});
	});
	$(".menu>li").mouseout(function() {
		$(this).stop().css({
			"color" : ""
		})
	});
	$('.menu>li>ul>li').mouseover(function() {
		$(this).stop().css("background-color", "#fff");
		$(this).stop().css({
			"color" : "#516CB1"
		})
	});
	$('.menu>li>ul>li').mouseout(function() {
		$(this).stop().css({
			"color" : ""
		})
	});
	var name = $('.name').text();

});
function myFunction(){
	var nameText = $(".usersname").text();
	var emailText = $(".usersemail").text();
	var writerText = $(".userswriter").text();
	var telText = $(".usersTel").text();
	var nickText = $(".usersnick").text();
	var jakText = $(".usersjak").text()
	$('.mynameis').val(nameText);
	$('.myemail').val(emailText);
	$('.myjak').val(jakText);
	$('.mywriter').val(writerText);
	$('.mytel').val(telText);
	$('.mynick').val(nickText);
}

//스크립트 정의
function fileCheck(obj){
	var pathpoint = obj.value.lastIndexOf('.');
	var filepoint = obj.value.substring(pathpoint+1, obj.length);
	var filetype = filepoint.toLowerCase();
	if(filetype=='jpg'||filetype=='gif'||filetype=='png'||filetype=='jpeg'){
		helo();
	}else{
		swal("이미지 파일만 선택할 수 있습니다.");
		var parentObj = obj.parentNode;
		var node = parentObj.replaceChild(obj.cloneNode(true),obj);
			return false;
	}
}



$(function(){
	$('.upload_toon').click(function(){
		$('.uploadToon-input').val('').removeAttr("readonly");
		$('.upload-name').prop("readonly",true);
	});

	var fileTarget = $('.filebox .upload-hidden');

	fileTarget.on('change', function() { // 값이 변경되면
		if (window.FileReader) { // modern browser
			var filename = $(this)[0].files[0].name;
		} else { // old IE
			var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
		}
		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename);
	});
	
	var fileTarget2 = $('.filebox2 .upload-hidden');

	fileTarget2.on('change', function() { // 값이 변경되면
		if (window.FileReader) { // modern browser
			var filename2 = $(this)[0].files[0].name;
		} else { // old IE
			var filename2 = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
		}
		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename2);
	});
});
function helo(){
	var uploadIcon = $('[name=uploadicon]').val();
	var name = $('.myfont>.name').text();
	
	if(uploadIcon==''||name==''){
		swal("파일을 업로드해주세요");
	}
	else{
		
		$('#upload_icon').ajaxForm({
            url: "/members/iconUpload",
            method: "POST",
            enctype: "multipart/form-data", // 여기에 url과 enctype은 꼭 지정해주어야 하는 부분이며 multipart로 지정해주지 않으면 controller로 파일을 보낼 수 없음
            success: function(data) {
            	if (data==1){
					location.href="/members/myPage";
				}
				else{
					location.href="/members/myPage";
				}

			},
			error : function(request, status, error) {
				swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "message:"
						+ request.responseText + "\n" + "error:"
						+ error,
					  type: 'error',
					});
			}

		});
$("#upload_icon").submit();
	}
	
}
</script>

<style>

.imginfo {
	background-color: #eeeeee;
	/* height: 300px; */
	margin: 0;
	padding-top:15px;
}

.profile_img{
	/* width: 20vh; */
	/* height:20vh; */
	/* width: 60%;
	height: 60% */
	width: 160px;
	height: 160px;
	border-radius: 100%;
	display: block;
	vertical-align: middle;
	border-radius: 100%;
}
.filebox2 label{
	width:85px; /*80px*/
}
.filebox2 label{
	position:relative;
    display: inline-block;
    padding: .3em 1.5em;
    font-size:14px;
    color: #fff;
    font-weight: normal;
    line-height: normal;
    vertical-align: middle;
    cursor: pointer;
    border: none;
    border-radius: .25em;
    background-color: #516CB1;
}
.filebox2 input[type="file"] {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
}
.filebox2 #dung{
	margin-top: 7px;
	padding:.4em 1.5em;
}

.myfont {
	font-size: 17px;
	text-align: center;
	margin-top: 8px;
}

.unicode {
	margin: 0;
	padding: 0;
	font-size: 14px;
}

.name {
	font-weight: bold;
}
#infoAllmodify{
	border: 1px solid #ccc; 
	border-top: 2px solid #666; 
	background: none; 
	padding: .3em .25em; 
	margin-right: 5px;
}
</style>
<script>
</script>


<c:import url="/WEB-INF/views/mypage/info/modifyinfo.jsp"/>
<c:import url="/WEB-INF/views/mypage/info/pwchange.jsp"/>
<c:import url="/WEB-INF/views/mypage/toon/uploadToon.jsp"/>


<c:if test="${currentUser == null }">
	<script>
		swal("로그인해주세요");
		location.href = "/login";
	</script>
</c:if>

<header class="moob7">
	<img src="/resources/image/logo.png" alt="logo" class="logo m_re_logo" onclick="location.href='/'">
	<div id="header_info">
		<span class="fl" style="font-size:17px; margin-top: 7px;"><b style="color: #516CB1;">${currentUser.getName()}</b>님</span>
		<!-- <button onclick="location.href='/logoutProcess.do'">로그아웃</button>	 -->
		<c:if test="${sessionScope.currentUser != null }">

						<!-- <button class="btn-login" style="float: right; margin-right:12%;">로그아웃</button> -->
						<ul class="fl">
							<li class="btn-login btn-my cp" style="border-radius:0; margin-right: 10%;"><span>MY</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">내 정보</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">내 서재</li>
									<li class="cp MYLIST" onclick="location.href='/logoutProcess.do'">로그아웃</li>
								<c:if test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
									<li class="cp MYLIST" onclick="location.href='/adminPage'">설정관리</li>
								</c:if>
								</ul>
							</li>
						</ul>
						<script>
						$(function(){
							$('.btn-my').mouseover(function(){
								$(this).find("ul").stop().slideDown();
							})
							$('.btn-my').mouseout(function(){
								$(this).find("ul").stop().slideUp();
							})
							$('.MYLIST').mouseover(function(){
								$(this).stop().css({"background-color":"#bbb","color":"#fff"});
								
							})
							$('.MYLIST').mouseout(function(){
								$(this).stop().css({"background-color":"#fff","color":"#767676"});
							})
						})
						</script>
					</c:if>
	</div>
</header>

<div id="admin_cont">
	<nav class="m_re_nav">
	<div class="imginfo">
	<c:if test="${sessionScope.currentUser.getIcon()!=null }">
		<c:forEach var="myIcon" items="${myInfo }" >
			<img  src="${myIcon.getIcon() }" class="profile_img">
		</c:forEach>
	</c:if>	
		<div class="filebox2 mt15" style="width:auto;"> 
		<form enctype="multipart/form-data" name="form_upload" id="upload_icon" action="/members/iconUpload" method="POST" >
			<label for="ex_file" style="float: none; margin: 0 auto; display: block; padding:5px; vertical-align: middle; text-align: center;">아이콘 변경</label>
			<input type="file" id="ex_file" name="upfile2" accept="image/jpeg,image/gif,image/png" onchange="javscript:fileCheck(this)">
			<c:forEach var="myIconvalue" items="${myInfo }" >
				<input type="hidden" name="iconValue" value="${myIconvalue.getIcon() }">
			</c:forEach>
		</form>
		</div>

		<p class="myfont" style="clear: both;">
			<span class="name" style="color: #516CB1;">${currentUser.getName() }</span>님의
			
			<a href="/members/myPage" style="color:#337ab7">마이페이지</a>
		</p>

	</div>
	<ul class="menu">

		<li class="m_menu_re_list mmrl_1"><span style="font-size: 19px;" onclick="mmrl_1()">회원 정보 관리</span>
		<span class="fr unicode" style="font-size: 16px;" onclick="mmrl_1()">&#x25BC;</span>
			<ul class="m_menu_re_list_hide mmrlh_1" style="width: 100%;">
				<li data-toggle="modal" data-target="#squarespaceModal_pwchange" data-backdrop="static" data-keyboard="false" style="padding: 15px 0 5px 0; font-size: 16px;">비밀번호 변경</li>		
				<li id="out" style="padding: 5px 0 15px 0; font-size: 16px;">회원
					탈퇴</li>

			</ul>
		</li>
		<c:if test="${currentUser.getRole()=='ROLE_WRITER' }">
			<li class="m_menu_re_list mmrl_2">
			<span style="font-size: 19px;" onclick="mmrl_2()">작품 관리</span>
			<span class="fr unicode" style="font-size: 16px;" onclick="mmrl_2()">&#x25BC;</span>
				<ul class="m_menu_re_list_hide mmrlh_2" style="width: 100%;">
					<li class="upload_toon" onclick="mySeries();" style="padding: 15px 0 5px 0; font-size: 16px;">시리즈 작품 등록</li>
					<li class="upload_toon" onclick="mywebMovie();" style="padding: 5px 0 5px 0; font-size: 16px;">웹툰무비 등록</li>
					<li class="upload_toon" onclick="mywonjak();" style="padding: 5px 0 15px 0; font-size: 16px;">원작 등록</li>
				</ul></li>
		</c:if>

	</ul>
	</nav>
	<div id="Context" class="m_re_context" style="width: 85%; float: right;">
		<div class="fl m50 re_ml25" style="width: 85%;">
			<div class="fl m_calendar_text_left" style="background-color: #516CB1; padding: 20px 50px; color: #fff;">
				<span class="w100" style="color: #fff; line-height: 15px;"> <fmt:formatDate
						value="${date}" type="date" pattern="yyyy.MM" /></span>
				<hr
					style="width: 40px; border-color: #2B385A; margin: 0 0 10px 18px" />
				<h2 style="line-height: 25px;">
					<fmt:formatDate value="${date}" type="date" pattern="dd. E" />
				</h2>
			</div>
			<div class="fl m_calendar_text" style="margin: 33px; font-size: 15px;">
				<span class="fl m_cd_hi" style="line-height: 20px;">안녕하세요! <span
					style='font-size: 20px; color: #2B385A;'>${currentUser.getName() }</span>
					님.
				</span><br /><br /> <span style="line-height: 0px;">TOONIVIE를 이용해주셔서 감사합니다.</span>
			</div>

		</div>
		<c:forEach var="Users" items="${myInfo }">
			<div class="fl m50 m_re_users" style="margin-top: 0; width: 40%; font-size: 15px; padding: 30px; border: 1px solid #ccc; box-shadow: 7px 7px 5px #aaa;">
				<h2 class="fl w100"
					style="float: left; border-bottom: 1px solid #ccc; line-height: 25px;">
					<span class="fl">기본 정보</span>
				</h2>
				<table class="table join-table fl" style="margin-top: 5px;">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>ID</span></th>
						<td class="td-join"><span>${Users.getId() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>이메일</span></th>
						<td class="td-join"><span class="usersemail">${Users.getEmail() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>이름</span></th>
						<td class="td-join"><span class="usersname">${Users.getName() }</span></td>
					</tr>
					<c:if test="${currentUser.getRole()=='ROLE_COWORKER' }">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>담당작가명(이름)</span></th>
						<td class="td-join"><span class="userswriter">${Users.getMatching() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>전화번호</span></th>
						<td class="td-join"><span class="usersTel">${Users.getTel() }</span></td>
					</tr>
					</c:if>
					<c:if test="${currentUser.getRole()=='ROLE_WRITER' }">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>닉네임</span></th>
						<td class="td-join"><span class="usersnick">${Users.getNickname() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>대표 작품</span></th>
						<td class="td-join"><span class="usersjak">${Users.getWork() }</span></td>
					</tr>
					</c:if>
				</table>
				<div class="fr">
					<button id="infoAllmodify" data-toggle="modal" data-target="#squarespaceModal" data-backdrop="static" data-keyboard="false" onclick="myFunction()">회원정보 수정</button>
				</div>
			</div>
		</c:forEach>

		<c:forEach var="Toons" items="${myToonInfo }">
			<div class="fl m50 m_re_users m_re_mb60"
				style="margin-top: 0; width: 40%; font-size: 15px; padding: 30px; border: 1px solid #ccc; box-shadow: 7px 7px 5px #aaa;">
				<h2 class="fl w100"
					style="float: left; border-bottom: 1px solid #ccc; line-height: 25px;">
					<span class="fl">작품 보기<h5 class="fr" style="margin-bottom: 0px;">(최신 작품 1개만 미리보기)</h5></span>
				</h2>
				<table class="table join-table fl"
					style="margin-top: 5px; width: 100%;">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작품명</span></th>
						<td class="td-join"><span>${Toons.getTitle() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작가</span></th>
						<td class="td-join"><span>${Toons.getWriter() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>장르</span></th>
						<td class="td-join"><span>${Toons.getGenre() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>내용</span></th>
						<td class="td-join" style=""><span
							style="text-overflow: ellipsis;; overflow: hidden; white-space: nowrap; /* width: 250px; display: inline-block; */">${Toons.getContent() }</span>
						</td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작성일</span></th>
						<td class="td-join"><span><fmt:formatDate
									value="${Toons.getWridate() }" type="date"
									pattern="yyyy.MM.dd hh:mm:ss(E)" /></span></td>
					</tr>
				</table>
				<div class="fr">
					<button class="view_project"
						style="border: 1px solid #ccc; border-top: 2px solid #666; background: none; padding: .3em .25em;">내작품 더보기</button>
				</div>
			</div>
		</c:forEach>
	</div>
</div>

<script>
$('.view_project').click(function() {
	$.ajax({
		type : "GET",
		url : "/members/toon/list",
		error : function() {
		},
		success : function(data) {
			$('#Context').empty();
			$('#Context').html(data);
		}
	});
});

$('#out').click(function() {
	$.ajax({
		type : 'get',
		url : '/members/dropOut',
		success : function(data) {
			$('#Context').empty();
			$('#Context').html(data);
		}
	});
});

$('#infoAllmodify').click(function() {
	$("#testModal").modal();
})

$('.upload_toon').click(function(){
	$('.uploadToon').val('').removeAttr('readonly');
})
function mywebMovie(){
	$.ajax({
		type : "GET",
		url : "/mypage/webmovie",
		error : function() {
			swal('통신실패!!');
		},
		success : function(data) {
			$('#Context').empty();
			$('#Context').html(data);
		}
	});
}
function mySeries(){
	$.ajax({
		type : "GET",
		url : "/mypage/series",
		error : function() {
			swal('통신실패!!');
		},
		success : function(data) {
			$('#Context').empty();
			$('#Context').html(data);
		}
	});
}
function mywonjak(){
	$.ajax({
		type : "GET",
		url : "/mypage/wonjak",
		error : function() {
			swal('통신실패!!');
		},
		success : function(data) {
			$('#Context').empty();
			$('#Context').html(data);
		}
	});
}
/* add psh 2018-02-12 */
function mmrl_1(){
	$(".mmrlh_1").slideToggle();
}
function mmrl_2(){
	$(".mmrlh_2").slideToggle();
}
</script>