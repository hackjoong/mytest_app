<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link href="/resources/common/css/admin.css" rel="stylesheet">
<!-- 원본 -->
<div style="width: 85%;" class="m_DO">
	<div class="fl re_dropOut" style="clear:both;width: 100%; margin: 50px 0 30px 0;">
		<div class="fl">
			<h2 class="fl bbgi1">회원 탈퇴</h2><br/>
			<div style="float: left; color: #777;">회원탈퇴를 신청하기 전에 안내사항을 꼭 확인바랍니다.</div>
		</div>
	</div>
	<!-- check1 -->
	<div class="fl re_dropOut" style="clear:both;width: 100%; margin: 50px 0 30px 0;">
		<div class="re_redcheck_rt" style="font-weight: bold; font-size: 20px;">
			<span class="fl re_redcheck" style="color:#f00;margin: 0;">&#10004;</span>
			<p class="fl m_re_check_fz1" style="margin:0; padding:0;">
				아이디(<span style="color: #526CB0">${currentUser.getId() }</span>)는 탈퇴할 경우 재사용 및 복구가 불가능합니다.
			</p>
		</div>
	 	<div style='clear:both;'>
			<p class="fl" style="margin:0; padding:0;">
				<span class="m_re_check_fz2" style="color:#f00;">탈퇴한 아이디는 본인,타인 모두 재사용 및 복구가 불가</span>
				하오니 신중하게 선택하시기 바랍니다.
			</p>
		</div>
		<div class="fl bbgi3" style="clear:both; margin-top:30px;">
			<input type="checkbox" class="yak yak1" id="yak1">위의 내용에 동의합니다.
		</div>
	</div>
	<!-- //check1 -->
	<!-- check2 -->
	<div class="fl re_dropOut" style="width:100%; margin: 50px 0 30px 0; font-size: 15px;">
		<div class="re_redcheck_rt" style="font-weight: bold; font-size: 20px;">
			<span class="fl re_redcheck" style="color:#f00;margin: 0;">&#10004;</span>
			<p class="fl m_re_check_fz1" style="margin:0; padding:0;">탈퇴 후에도 게시판형 서비스 등록한 게시물은 그대로 남아있습니다.</p>
		</div>
		<div style='clear: both;'>
			<p class="fl m_re_check_fz2" style="margin:0; padding:0;clear: both;">웹툰 댓글,답글, 웹툰 업로드 정보는 탈퇴시 자동 삭제되지 않고 그대로 남아있습니다.</p>
		</div>
		<div style='clear: both;'>
			<p class="fl m_re_check_fz2" style="margin:0; padding:0;clear: both;">삭제를 원하는 게시물이 있다면 <span  style="color:#f00;">반드시 탈퇴 전 비공개 처리하거나 삭제하시기 바랍니다.</span></p>
		</div>
		<p class="fl bbgi4" style="clear:both; margin-top:30px;"><input type="checkbox" class="yak yak2" id="yak2">위의 내용에 동의합니다.</p>
	</div>
	<!-- //check2 -->
	<!-- check3 -->
	<div class="fl re_dropOut" style="width:100%; margin: 50px 0 30px 0; font-size: 15px;">
		<p class="fl bbgi5" style="clear:both; margin-top:30px;"><input type="checkbox" id="all">전체 동의합니다.</p>
	</div>
	<!-- //check3 -->
	<div class="fl wrap_1 drout_ml mlil" style="margin-top:20px; width:100%; font-size: 15px;">
		<form action="/members/delete/${currentUser.getId() }">
			<input type="button" class="dropout m_dropout" value="회원탈퇴" style="border: 1px solid #ccc; border-top: 2px solid #666; background: none; padding: .6em .95em; margin-right: 5px;">
		</form>
	</div>
	
	
</div>

<!-- //원본 -->


<style>
.re_dropOut {width: 85%; margin: 50px 0 30px 50px;}
.re_redcheck_rt {position: relative;}
.re_redcheck {color:#f00;margin: 0; position: absolute; top: 0; left: -20px;}
.re_out {}


span{
margin:0;
}
</style>
<script>
$('.yak').click(function(){
	var yak1Check= $('#yak1').is(':checked');
	var yak2Check= $('#yak2').is(':checked');
	if(yak1Check==true && yak2Check==true){
		$('#all').prop('checked',true);
		$('.dropout').attr('type','submit');
	}
})


$('#all').click(function(){
	var allCheck= $('#all').is(':checked');
	//각각버튼 체크

	if(allCheck==true){
		$('.yak').prop('checked',true);
		$('.dropout').attr('type','submit');
	}
	else{
		$('.yak').prop('checked',false);
		$('.dropout').attr('type','button');
	} 
});



$(function(){
	$('.dropout').click(function(){
		if($('.dropout').prop("type")=="submit" ){
			if(confirm("정말로 하시겠습니까?")==true){
				alert("그동안 TOONIVIE서비스를 이용해주셔서 감사합니다.\n 보다 더 나은 서비스로 다시 찾아뵙겠습니다.");
			}
			else{
				return false;
			}
			
		}
		else{
			alert("동의 약관에 모두 체크해주세요.");
		}
	});
	
	
});
$('.yak').click(function(){
	if ($('.yak1').is(':checked') && $('.yak2').is(':checked')){
		$('#all').prop('checked',true);
	} else{
		$('#all').prop('checked',false);
	}
});
</script>

