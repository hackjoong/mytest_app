<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<!-- custom CSS -->
<link href="/resources/common/css/admin.css" rel="stylesheet">
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webtoonList.css">
<link rel="stylesheet" href="/resources/common/css/loading.css">
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/myseries.js"></script>
<script src="/resources/common/js/loading.js"></script>
<script src="/resources/common/js/setCsrf.js"></script>
<style type="text/css">
.table-striped-wonjak{
	/* display: block;
	height:600px; */
	overflow: auto;
}
.upload {margin-top: 0 !important;}
.striped-wonjak{
	display: block;
	height:auto;
	overflow: auto;
}
#wtoonwrap {
	width: 90%;
	margin-top: 60px;
	margin-left: 30px;
	height: 800px;
}
.choice3 {
    width: 80%;
    height: 33px;
    /* padding:15px 10px; */
    border-radius: 50px;
    background: none;
    color: #777;
    font-size: 12px;
    border: 1px solid #ccc;
    line-height: 0;
    outline: none;
}
</style>
<h2 class="re_admin_h2">작품 관리 - 시리즈</h2>
<div id="toonwrap" class="re_admin_wtoonwrap">
	<!-- 수정본 -->
	<div class="fl re_admin_serise">
	<div class="fl" style="width:100%;">
		<span class="fr" style="margin-top:9px; font-size:14px; color:#d3394c">※해당 시리즈를 클릭하면 수정할 수 있습니다.</span>
	</div>
		<div class="striped-wonjak" style="width:100%;">
		<table class="table table-striped table-striped-wonjak " style="width: 100%; font-size:14px;">
			<thead class="thead-dark">
				<tr>
					<th scope="col">종류</th>
					<th scope="col">제목</th>
					<th scope="col">장르</th>
					<th scope="col">작가</th>
					<th class="m_wonjak_col1_ah re_admin_content" scope="col">내용</th>
					<th class="m_wonjak_col2_ah" scope="col">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${mySeriesInfoAll }" var="SeriesList">
					<tr>
						<td id="mtd" class="dn idx_${SeriesList.getWmno()}">${SeriesList.getId() }</td>
						<td id="mtd" class="dn tth_${SeriesList.getWmno()}">${SeriesList.getWmthumbnail() }</td>
						<td id="mtd" class="dn tfile_${SeriesList.getWmno()}">${SeriesList.getWmfile() }</td>
						<td id="mtd" class="dn kinds_${SeriesList.getWmno()}">${SeriesList.getKinds() }</td>
						
						
						<td id="mtd" class="">
						<c:choose>
							<c:when test="${SeriesList.getKinds() =='webmovie'}">웹툰무비</c:when>
							<c:when test="${SeriesList.getKinds() =='origintoon'}">원작웹툰</c:when>
						</c:choose>
						</td>
						<td id="mtd" onclick="seriesUpdate(${SeriesList.getWmno()})" class="title_${SeriesList.getWmno()}">${SeriesList.getWmtitle() }</td>
						<td id="mtd" onclick="seriesUpdate(${SeriesList.getWmno()})" class="genre_${SeriesList.getWmno()}">${SeriesList.getWmgenre() }</td>
						<td id="mtd" onclick="seriesUpdate(${SeriesList.getWmno()})" class="writer_${SeriesList.getWmno()}">${SeriesList.getWmwriter()}</td>
						<td id="mtd" onclick="seriesUpdate(${SeriesList.getWmno()})" class="content_${SeriesList.getWmno()} re_admin_content"><span class="ellipsis" style="width: 150px;">${SeriesList.getWmcontent() }</span></td>
						<td id="mtd" class="choice_box"><input style="width: 100%;" type="button" value="삭제" class="choice"onclick="seriesDelete(${SeriesList.getWmno()})" /></td>
				</c:forEach>
			</tbody>
		</table>
		</div>
	</div>
	<!-- //수정본 -->
	<!-- 원본 -->
	<div class="w100 fl h100 re upload from m_upload_re admin_series_mb50">
		<div class="menu-cont-title">
			<span class="fl toonupload-text">시리즈 올리기</span>
		</div>
		<div class="w100 fl">
			<form id="upload_myseries" action="/uploadWebtoon/upload"
				enctype="multipart/form-data">
				<div class="w100 fl mt15 re apd-zone">
					<label for="ex_filename" class="cols-sm-2 control-label fl movie-text change-movie-text">썸네일</label>
					<div id="change_no">
						<input type="checkbox" id="checkbox" name="movieChanged" checked="checked" />변동 없음
					</div>
					<div class="filebox">
						<input class="upload-name m_re_fileog m_filego" value="파일선택" readonly="readonly" style="height: 44px;">
						<label for="mywebmovieFile" class="m_filego_uplo" style="margin: 0;" >업로드</label>
						<input type="file" name="upfile" id="mywebmovieFile" class="kinp upload-inp king2 upload-hidden" required="required" accept=".gif, .jpg, .png, .bmp">
					</div>
					<div class="w100 fl tooni-title">
						<div class="tooni-head2" style="margin:10px 0;">시리즈 정보입력</div>
						<div class="tooni-cont2">
							<input type="hidden" name="wmno" class="tooninfo" id="tooninfo_no" placeholder="번호">
							<input type="hidden" name="idx" id="idx" placeholder="ID">
							<div class="form-group fl w100">
								<label for="genre" class="cols-sm-2 control-label fl">종류</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<div class="w50 fl">
											<span><input type="radio" name="kinds" id="webmovie" value="webmovie" checked="checked">웹툰무비</span>
										</div>
										<div class="w50 fr">
											<span ><input type="radio" name="kinds" id="origintoon" value="origintoon">원작웹툰</span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="genre" class="cols-sm-2 control-label fl">장르</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="genre" class="tooninfo" id="tooninfo_genre" placeholder="장르" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">제목</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<input type="text" name="title" class="tooninfo" id="tooninfo_title" placeholder="제목" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">작가</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
									<c:if test="${currenstUser.getNickname() != null }">
										<input type="text" name="writer" class="tooninfo" id="tooninfo_writer"
											placeholder="작가" value="${currentUser.getNickname() }" readonly="readonly" required="required">
									</c:if>
									<c:if test="${currenstUser.getNickname() == null }">
										<input type="text" name="writer" class="tooninfo" id="tooninfo_writer"
											placeholder="작가" value="" required="required">
									</c:if>
										<input type="hidden" name="id" class="tooninfo" id="tooninfo_id"
											placeholder="아이디" value="${currentUser.getId() }" readonly="readonly" required="required">
									</div>
								</div>
							</div>
							<div class="form-group fl w100">
								<label for="username" class="cols-sm-2 control-label fl">내용</label>
								<div class="cols-sm-10">
									<div class="input-group w100 toonform">
										<textarea rows="7" cols="30" name="content" id="tooncontent"
											placeholder="내용"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl mt15">
						<button type="button" class="btn-orange fl submit_toon"
							onclick="mypage_upload_series()">업로드</button>
						<button type="reset" class="btn-orange fl cancel">취소</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- //원본 -->	
</div>