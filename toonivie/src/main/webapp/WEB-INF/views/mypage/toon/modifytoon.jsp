<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/loading.js"></script>
<link rel="stylesheet" href="/resources/common/css/loading.css">

<style type="text/css">
.center {
    margin-top:50px;   
}

.modal-header {
	padding-bottom: 5px;
}

.modal-footer {
    	padding: 0;
	}
    
.modal-footer .btn-group button {
	height:40px;
	border-top-left-radius : 0;
	border-top-right-radius : 0;
	border: none;
	border-right: 1px solid #ddd;
}
	
.modal-footer .btn-group:last-child > button {
	border-right: 0;
}
.modal_modifyToon{
	margin-top:35%;
}


.movie-text {
    width: 50%;
    text-align: left;
    float: left;
}

.filebox-modify {
    width: 100%;
}

.filebox-modify .upload-name-modify {
    display: inline-block;
    padding: .5em .75em;
    line-height: normal;
    vertical-align: middle;
    background-color: #f5f5f5;
    border: 1px solid #ebebeb;
    border-bottom-color: #e2e2e2;
    border-radius: .25em;
    width: 85%;
    color: #666;
    padding-left: 2px;
}

.filebox-modify label {
    display: inline-block;
    padding: .5em 1.5em;
    color: #fff;
    font-weight: normal;
    line-height: normal;
    vertical-align: middle;
    cursor: pointer;
    border: none;
    border-radius: .25em;
    background-color: #ff8c24;
    float: right;
}

.filebox-modify input[type="file"] {
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
}
</style>

<body>


<!-- line modal -->
<div class="modal fade" id="squarespaceModal_toon" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content modal_modifyToon">
		<div class="modal-header">
			<button type="button" class="close close_btn_toon" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel_info">웹툰 정보</h3>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form name="form1" id="modify_webtoon_movie" action="/members/toonChange" enctype="multipart/form-data" method="post">
			<input type="hidden" name="tno">
			<input type="hidden" name="id" value="${currentUser.getId() }">
			<input type="hidden" name="thumbnail">
			  <div class="form-group">
			  <label for="ex_filename" class="fl"><span class="fl">영상</span></label>
			  <div id="change_no" class="fr">
						<input type="checkbox" id="checkbox2" name="movieChanged" checked="checked" disabled="disabled"/>변동 없음
					</div>
					<div class="filebox-modify" style="clear:both; display: none;">
						<input type="file" name="upfile" id="ex_filename4" class="kinp upload-inp king2 upload-hidden-modify" style="display:none;">
						<input class="upload-name-modify fl" name="file_name" placeholder="파일선택">
						<label for="ex_filename4" class="btn-cyan">업로드</label>
					</div>
			  </div>
              <div class="form-group" style="width:49%; float: left;clear: both;">
                <label class="fl" for="exampleInputEmail1">제목</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="title" placeholder="title" required="required">
              </div>
              <div class="form-group" style="width:49%; float: right;">
                <label class="fl" for="exampleInputPassword1">작가</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="writer" placeholder="writer" required="required">
              </div>
              <div class="form-group">
                <label class="fl" for="exampleInputPassword1">장르</label>
                <input type="text" class="form-control" id="exampleInputPassword2" name="genre" placeholder="genre" required="required">
              </div>
              <div class="form-group">
                <label class="fl" for="exampleInputPassword1">내용</label>
                <textarea style="height:100px;" class="form-control" id="exampleInputPassword3" name="content" placeholder="content" required="required"></textarea>
              </div>
              <button type="button" onclick="modifyToon()" class="btn btn-default submit">작성</button>
              <button type="button" class="btn btn-default modify-toon">수정</button>
              <button type="button" class="btn btn-default delete_toon">삭제</button>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default close_btn_toon" data-dismiss="modal"  role="button">Close</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
<script>
$('#squarespaceModal_toon').on('hidden.bs.modal', function () {
    $("#checkbox2").prop("checked",true);
    $("#checkbox2").attr("disabled",true);

    $('.modify-toon').show();
	$('.submit').hide();
	$('.filebox-modify').hide();
})

$(function(){
	$('#checkbox2').click(function(){
		if($('#checkbox2').prop('checked')==false){
		    $('.filebox-modify').show();
		}
		else{
			$('.filebox-modify').hide();
		}
	})
	
	
	$('.submit').hide();
	$('.modify-toon').click(function(){
		$('#change_no').show();
		$(this).hide();
		$('.submit').show();
		$('.form-control').removeAttr('readonly');
		$('#checkbox2').removeAttr('disabled');
		$('#lineModalLabel_info').text("웹툰 수정");
		if($('#checkbox2').prop('checked')==false){
		    $('.filebox-modify').show();
		}
		else{
			$('.filebox-modify').hide();
		}
	});
	
	$('.delete_toon').click(function(){
		var s = confirm("삭제하시겠습니까?");
		if(s == true){
			var tno = $('[name=tno]').val();
			var file_info_name = $('[name=file_name]').val();
			var thumbnail_name = $('[name=thumbnail]').val();
			$.ajax({
				url:'/members/remove/'+tno,
				type:'post',
				data:{
					'file_name':file_info_name,
					'thumbnail':thumbnail_name
				},
				success : function(data) {

					if (data == 1) {
						swal("삭제되었습니다.");
						location.href="/members/myPage";
					}

				},
				error : function(data) {
				}
			})
		}
	})
	
	$('.close_btn_toon').click(function(){
	    $("#checkbox2").prop("checked",true);
	    $("#checkbox2").attr("disabled",true);
		$('#lineModalLabel_info').text("웹툰 정보");
		
	})
});

$(document).ready(function(){

	  $('.filebox-modify .upload-hidden-modify').on('change', function(){
	        if(window.FileReader){
	            var modifyFilename = $(this)[0].files[0].name;
	        } else {
	            var modifyFilename = $(this).val().split('/').pop().split('\\').pop();
	        }

	        $(this).siblings('.upload-name-modify').val(modifyFilename);
	    });
	}); 
function modifyToon(){
	
	var movieChanged = $("[name=movieChanged]").val();
	var tno = $("[name=tno]").val();
	var genre = $("[name=genre]").val();
	var title = $("[name=title]").val();
	var writer = $("[name=writer]").val();
	var upfile = $("[name=upfile]").val();
	var content = $("[name=content]").val();
	loadingOn();
	$("#modify_webtoon_movie").ajaxForm(
			{
				url : "/members/toonChange",
				type : "POST",
				enctype : "multipart/form-data",
				data:{
					'modiveChanged':movieChanged,
					'tno':tno,
					'genre':genre,
					'title':title,
					'writer':writer,
					'content':content,
					'upfile':upfile
				},
				success : function(data) {
					if (data==1){
						swal("변경되었습니다.");
						loadingOff();
						location.href="/members/myPage";
					}
					else{
						swal("변경 불가!!");
						loadingOff();
						location.href="/members/myPage";
					}

				},
				error : function(request, status, error) {
					swal({
						  title: 'Error!',
						  text: "code:" + request.status + "\n" + "error:" + error,
						  type: 'error',
						});
				}

			});
$("#modify_webtoon_movie").submit();
}
</script>
