<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>

<link href="/resources/common/css/admin.css" rel="stylesheet">
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<div class="fl tool5 m_tool5" style="clear:both;width: 80%; margin:50px 0px 30px 50px">
	<div class="fl">
		<h2 class="fl tool1" style="line-height: 20px;">작품 보기<span class="tool2" style="font-size:15px; color: #888; font-weight: normal;"><br class="m_br_db"><br class="m_br_db">(해당 웹툰 표를 클릭하면 상세정보가 뜹니다)</span>
		</h2>
	</div>
</div>
<div class="wrap fl" style=" width:97%; clear:both;">
	<!-- 원본 -->
	<div class="fl m50 m_tooList_box"
		style="margin-top: 5px; width: 90%; font-size: 15px; padding: 30px; border: 1px solid #ccc; box-shadow: 7px 7px 5px #aaa;">
		<div class="m_tooList_re" style="">
		<table class="table join-table fl" style="margin-top: 5px; width:100%; ">
			<tr class="join-row">
				<th class="th-join" scope="row" style="display:none"><span>작품번호</span></th>
				<th class="th-join" scope="row"><span>작품명</span></th>
				<th class="th-join m_toolist_700dn" scope="row"><span>작가</span></th>
				<th class="th-join m_toolist_700dn" scope="row"><span>장르</span></th>
				<th class="th-join m_toolist_700dn" scope="row"><span>내용</span></th>
				<th class="th-join" scope="row"><span>작성일</span></th>
			</tr>
			<c:forEach var="Toons" items="${myToonInfoAll }">
			<tr class="join-row click_row row_${Toons.getTno() }" data-toggle="modal" data-target="#squarespaceModal_toon" data-backdrop="static" data-keyboard="false">
				<td class="td-join click_tno" style="display:none"><span>${Toons.getTno() }</span></td>
				<td class="td-join click_file" style="display:none"><span>${Toons.getFile() }</span></td>
				<td class="td-join click_thumb" style="display:none"><span>${Toons.getThumbnail() }</span></td>
				<td class="td-join click_title" ><span>${Toons.getTitle() }</span></td>
				<td class="td-join click_writer m_toolist_700dn"><span>${Toons.getWriter() }</span></td>
				<td class="td-join click_genre m_toolist_700dn"><span>${Toons.getGenre() }</span></td>
				<td class="td-join click_content m_toolist_700dn"><span class="m_toonlist_td" style="text-overflow: ellipsis;; overflow: hidden;white-space: nowrap; display: block; width: 200px;">${Toons.getContent() }</span></td>
				<td class="td-join click_date"><span><fmt:formatDate value="${Toons.getWridate() }" type="date" pattern="yyyy.MM.dd hh:mm:ss(E)" /></span></td>
			</tr>
			</c:forEach>
		</table>
		</div>
	</div>
	<!-- //원본 -->
	<!-- 수정본  -->
	<%-- <div class="fl m50 ctt2 tool4"
		style="margin-top: 5px; width: 65%; font-size: 15px; padding: 30px; border: 1px solid #ccc; box-shadow: 7px 7px 5px #aaa;">
		<div style="overflow: auto;">
			<c:forEach var="Toons" items="${myToonInfoAll }">
			<table class="table join-table fl tool3" style="margin-top: 5px;" data-toggle="modal" data-target="#squarespaceModal_toon" data-backdrop="static" data-keyboard="false">
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작품명</span></th>
						<td class="td-join"><span>${Toons.getTitle() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작가</span></th>
						<td class="td-join"><span class="usersemail">${Toons.getWriter() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>장르</span></th>
						<td class="td-join"><span class="usersname">${Toons.getGenre() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>내용</span></th>
						<td class="td-join"><span style="text-overflow: ellipsis;; overflow: hidden; white-space: nowrap; width: 250px; display: block;">${Toons.getContent() }</span></td>
					</tr>
					<tr class="join-row">
						<th class="th-join" scope="row"><span>작성일</span></th>
						<td class="td-join"><span><fmt:formatDate
									value="${Toons.getWridate() }" type="date"
									pattern="yyyy.MM.dd hh:mm:ss(E)" /></span></td>
					</tr>
			</table>
			</c:forEach>
		</div>
	</div> --%>
	
	
	<!-- //수정본 -->
	<!-- <div class="fr m50" style="margin-top:5px width:30%; font-size: 15px padding 30px border: 1px solid #ccc; box-shadow: 7px 7px 5px #aaa;">
	<h1></h1>
	</div> -->
</div>
<c:import url="/WEB-INF/views/mypage/toon/modifytoon.jsp"/>
<script>
$('.click_row').click(function(){
	var tno = $(this).children('.click_tno').text();
	var title = $(this).children('.click_title').text();
	var file_name = $(this).children('.click_file').text();
	var writer = $(this).children('.click_writer').text();
	var genre = $(this).children('.click_genre').text();
	var content = $(this).children('.click_content').text();
	
	var file = $(this).children('.click_file').text();
	var thumb = $(this).children('.click_thumb').text();
	
	$('[name=tno]').val(tno);
	$('[name=title]').val(title);
	$('[name=file_name]').val(file_name);
	$('[name=writer]').val(writer);
	$('[name=genre]').val(genre);
	$('[name=content]').val(content);
	$('[name=file_name]').val(file);
	$('[name=thumbnail]').val(thumb);
	$('.form-control').attr('readonly',true);
	
});

</script>
	
