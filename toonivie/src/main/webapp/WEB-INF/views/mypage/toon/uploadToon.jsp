<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script src="/resources/common/js/loading.js"></script>
<link rel="stylesheet" href="/resources/common/css/loading.css">
<style type="text/css">
.center {
    margin-top:50px;   
}

.modal-header {
	padding-bottom: 5px;
}

.modal-footer {
    	padding: 0;
	}
    
.modal-footer .btn-group button {
	height:40px;
	border-top-left-radius : 0;
	border-top-right-radius : 0;
	border: none;
	border-right: 1px solid #ddd;
}
	
.modal-footer .btn-group:last-child > button {
	border-right: 0;
}
.modal_uploadtoon{
	margin-top:35%;
}


.movie-text {
    width: 50%;
    text-align: left;
    float: left;
}

.filebox {
    width: 100%;
}

.filebox .upload-name {
    display: inline-block;
    padding: .5em .75em;
    line-height: normal;
    vertical-align: middle;
    background-color: #f5f5f5;
    border: 1px solid #ebebeb;
    border-bottom-color: #e2e2e2;
    border-radius: .25em;
    width: 82%;
    color: #666;
    padding-left: 2px;
}

.filebox label {
    display: inline-block;
    padding: .5em 1.5em;
    color: #fff;
    font-weight: normal;
    line-height: normal;
    vertical-align: middle;
    cursor: pointer;
    border: none;
    border-radius: .25em;
    background-color: #ff8c24;
    float: right;
    /* width: 22%; */
}

.filebox input[type="file"] {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
}
</style>


<!-- line modal -->
<div class="modal fade" id="squarespaceModal_UPLOAD" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content modal_uploadtoon">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel lineModalLabel_upload">웹툰 등록</h3>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form name="form_upload" id="upload_webtoon_movie" action="/members/toonUpload" enctype="multipart/form-data" method="POST" >
			<input type="hidden" name="id" value="${currentUser.getId() }">
			  <div class="form-group">
			  <label for="ex_filename" class="fl"><span class="fl">영상</span></label>
					<div class="filebox" style="clear:both;">
						<input class="upload-name fl uploadToon-input" id="file" placeholder="파일선택" readonly="readonly" required="required">
						<label for="ex_filename" class="btn-cyan upload_btn re_ex_filename">업로드</label>
						<input type="file" name="upfile" id="ex_filename" class="uploadToon-input kinp upload-inp king2 upload-hidden">
					</div>
			  </div>
              <div class="form-group" style="width:49%; float: left;clear: both;">
                <label class="fl" for="title">제목</label>
                <input type="text" class="form-control uploadToon-input" id="title" name="title" placeholder="title" required="required">
              </div>
              <div class="form-group" style="width:49%; float: right;">
                <label class="fl" for="writer">작가</label>
                <input type="text" class="form-control" id="writer" value="${currentUser.getNickname() }" name="writer" placeholder="writer" readonly="readonly">
              </div>
              <div class="form-group">
                <label class="fl" for="genre">장르</label>
                <input type="text" class="form-control uploadToon-input" id="genre" name="genre" placeholder="genre" required="required">
              </div>
              <div class="form-group">
                <label class="fl" for="content">내용</label>
                <textarea style="height:100px;" class="form-control uploadToon6 uploadToon-input content_area" id="content" name="content" placeholder="content" required="required"></textarea>
              </div>
              <button type="button" class="btn btn-default tooninfo-submit">작성</button>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
<script>
$('.tooninfo-submit').click(function(){
	var upfile = $("[name=upfile]").val();
	var genre = $("[name=genre]").val();
	var title = $("[name=title]").val();
	var writer = $("[name=writer]").val();
	var content = $("[name=content]").val();
	if($('#title, #writer, #genre, #content').val()==''||$('#file').val()==''){
		alert("입력되지 않은 창이 있습니다.");
	}
	else if($('#title, #writer, #genre, #content').val()!=''||$('#file').val()==''){
		loadingOn();
		$("#upload_webtoon_movie").ajaxForm(
				{
					url : "/members/toonUpload",
					type : "POST",
					enctype : "multipart/form-data",
					data:{
						'upfile':upfile,
						'genre':genre,
						'title':title,
						'writer':writer,
						'content':content

					},
					success : function(data) {
						if (data==1){
							swal("웹툰이 추가되었습니다.");
							loadingOff();
							location.href="/members/myPage";
						}
						else{
							loadingOff();
							location.href="/members/myPage";
						}

					},
					error : function(request, status, error) {
						swal({
							  title: 'Error!',
							  text: "code:" + request.status + "\n" + "error:" + error,
							  type: 'error',
							})
					}

				});
	$("#upload_webtoon_movie").submit();
	
	}
});


$('.upload_btn').click(function(){
	$('.upload-name').val('');
	$('.upload-hidden').val('');
})
$(function(){
	
	var fileTarget = $('.filebox .upload-hidden');
	

	
	

	fileTarget.on('change', function() { // 값이 변경되면
		if (window.FileReader) { // modern browser
			var filename = $(this)[0].files[0].name;
		} else { // old IE
			var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																				// 추출
		}

		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename);
	});
	
	
});
</script>