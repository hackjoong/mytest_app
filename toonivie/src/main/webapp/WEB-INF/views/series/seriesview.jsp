﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>webmovie_show_2_kr</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<!-- add by yoo_dh0416 2018-02-07 -->
<link rel="stylesheet" href="/resources/common/jquery/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/resources/common/js/order.js"></script>

<c:set value="${sessionScope.currentUser.getId() }" var="userName"/>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0 mt20 re_seriesImg">
				<img src='${seriesInfo.getWmthumbnail()}' class="fl thumbnail_img re_thumbnail_img">
			<div class="content-wrap re_content_wrap">
				<h4 class="content-wrap-title" style="font-size:18px; font-weight: bold; display: inline-block;">${seriesInfo.getWmtitle() }</h4>
				<h4 style="font-size:14px; margin-bottom: 20px;">작가 : ${seriesInfo.getWmwriter() }</h4>
				<h4 style="font-size:14px;">장르 : ${seriesInfo.getWmgenre() }</h4>
				<h4 class="content-wrap-content" style="">내용 : ${seriesInfo.getWmcontent() }</h4>
			</div>
			</div>
			
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12 m_plr2p">
					<h2 class="pdl-15 m_p0">웹툰무비 보기</h2>
					<table class="lst_view">
						<tbody id="webmovie_1">
							<c:set var="i" value="1" />
							<c:forEach var="series" items="${seriesall }" >
							<c:choose>
								<c:when test="${address =='toon'}"><!-- if kinds wonjak  -->
								<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;">
									<td class="img re_m_img">
										<div class="cp m_img_bot" onclick="location.href='/webtoon/${series.getWtno() }'"style="width:100%; background: url('${series.getThumbnail() }'); background-repeat: no-repeat; background-position: center; background-size: contain; border: 1px solid #ccc;"></div>
									</td>
									<td class="tit re_tit_1">
										<div class="" style="display: inline-block;">${series.getWtname() }</div>
										<img src="/resources/image/icon/ico_toonup.png" class="dn updateimg_${i }">	
										<div class="sub_title sub_title_1 re_sub_title_1">${series.getWtcontent() }</div>
									</td>
									<td class="coin m_coin" onclick="location.href='/webtoon/${series.getWtno() }'">
										<div class="free_bt">보기</div>
									</td>
									<td class="w15 none m_num">
										<div class="date updated_1"><fmt:formatDate value="${series.getWtwridate() }" type="date" pattern="yyyy-MM-dd hh:mm" /></div>
									</td>
								</tr>
								</c:when>
								<c:otherwise>
									<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr"
									style="box-sizing: border-box; background-color: white;">
										<td class="img re_m_img">
											<div class="cp m_img_bot" onclick="location.href='/webmovie2/${series.getTno() }'"style="width:100%; background: url('${series.getThumbnail() }'); background-size:cover; background-position: center center; border: 1px solid #ccc;"></div>
										</td>
										<td class="tit re_tit_1">
											<div class="" style="display: inline-block;">${series.getTitle() }</div>
											<img src="/resources/image/icon/ico_toonup.png" class="dn updateimg_${i }">	
											<div class="sub_title sub_title_1 re_sub_title_1">${series.getContent() }</div>
										</td>
										<td class="coin m_coin" onclick="location.href='/webmovie2/${series.getTno() }'">
											<div class="free_bt">보기</div>
										</td>
										<td class="w15 none m_num">
											<div class="date updated_1"><fmt:formatDate value="${series.getWridate() }" type="date" pattern="yyyy-MM-dd hh:mm" /></div>
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
							<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<div class="add-toon tc" onclick="location.href='/cv/effect'"
						style="display: none;">
						<img src="/resources/image/upload icon.png" class="upload-con">
						<span>만화 올리기</span>
					</div>
					<div class="w100 fl rank-title">
						<c:choose>
							<c:when test="${address == 'toon'}">
								<h2>원작 순위 보기</h2>
								<div class="rank-type-won" style="width:142px;">	
									<span class="cp">조회순</span><span class="cp">&nbsp;&#124;&nbsp;댓글순</span><span class="cp">&nbsp;&#124;&nbsp;최신순</span>
								</div>
								<div class="rank-cont">
									<c:set var="i" value="1" />
									<c:forEach items="${toonsunwi}" var="wonjakAll">
										<span class="cp" id='toon${wonjakAll.getWtno()}'
										
										onclick="location.href='/webtoon/${wonjakAll.getWtno()}'"
										data-file='${wonjakAll.getPath()}'
										data-title='${wonjakAll.getWtname()}'
										data-genre='${wonjakAll.getWtgenre()}'
										data-writer='${wonjakAll.getWtwriter()}'
										data-content='${wonjakAll.getWtcontent()}'><b>${i}</b>
										${wonjakAll.getWtname()} </span>
									<c:set var="i" value="${i + 1}" />
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
							<h2>웹툰무비 순위</h2>
							<div class="rank-type" style="">							
								<span>&nbsp;&#124;&nbsp;최신순</span>
								<span>&nbsp;&#124;&nbsp;댓글순</span>
								<span>조회순</span>
							</div>
							<div class="rank-cont">
								<c:set var="i" value="1" />
								<c:forEach items="${toonsunwi}" var="toonivie">
									<span class="cp" id='toon${toonivie.getTno()}'
										
										onclick="location.href='/webmovie2/${toonivie.getTno()}'"
										data-file='${toonivie.getFile()}'
										data-title='${toonivie.getTitle()}'
										data-genre='${toonivie.getGenre()}'
										data-writer='${toonivie.getWriter()}'
										data-content='${toonivie.getContent()}'
										data-date='${toonivie.getWridate()}'><b>${i}</b>
										${toonivie.getTitle()} </span>
									<c:set var="i" value="${i + 1}" />
								</c:forEach>
							</div>
							</c:otherwise>
						</c:choose>
						
					</div>

				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>
</body>
<script>
$(function(){
	
	//현재날짜
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	
	if(month<10){
		month = "0" + month;
	}
	if(day<10){
		day = "0" + day;
	}
	var nowdate = d.getFullYear()+"-"+month+"-"+day;
	

	
	
	for(var i = 0; i<$('.ep_list_tr').length; i++){
		var wridateText= $('.ep_list_tr').eq(i).find('.updated_1').text();
		var wridate = wridateText.substring(0,10);
		
		if(nowdate == wridate){
			$('.ep_list_tr').eq(i).find('.updateimg_'+(i+1)).removeClass('dn');
		}
	}
	
	
	
})
</script>
<style>
.content-wrap{
display: inline-block; 
margin-left:20px; 
width:60%;
}
.content-wrap-content{
	font-size:14px; 
	width:70%; 
	float:left;
}

@media (max-width:1199px){
	.content-wrap-content{width:85%;}
}
@media (max-width:991px){
`	.thumbnail_img{margin-left:10px;}
	.content-wrap{width:70%;}
	.content-wrap-content{width:100%;}
}
@media (max-width:765px){
	.thumbnail_img{margin-left:10px;}
	.content-wrap{width:65%;}
	.content-wrap-content{width:100%;}
}
@media (max-width:650px){
	.content-wrap-title{margin-top:0;}
	.content-wrap{width:60%;}
	.content-wrap-content{width:98%;}
}
@media (max-width:600px){
	.content-wrap-title{margin-top:10px;}
	.content-wrap{width:55%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:555px){
	.content-wrap{width:55%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:545px){
	.content-wrap{width:54%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:500px){
	.content-wrap{width:54%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:491px){
	.content-wrap{width:52.8%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:455px){
	.content-wrap{width:100%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:416px){
	.content-wrap{width:100%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:400px){
	.content-wrap{width:100%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
@media (max-width:370px){
	.content-wrap{width:100%;}
	.content-wrap-content{width:90%;}
	.thumbnail_img{margin-left:10px;}
}
</style>
</html>