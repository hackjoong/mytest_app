<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원가입</title>
</head>
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- <script src="/resources/common/js/imgslide.js"></script>-->
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<style>
.auth{
	width:30%;
	background-color:#eee;
	margin-top: 55px;
	cursor: pointer;
}
#writer-box{
margin-left: 58px;
}

.auth>img{
	width:80%;
	margin: 0 auto;
	display: block;
	margin-top: 30px;
}
.auth_text{
	width:100%;
	height:100px;

	text-align: center;
	margin-top:30px
}
/* .auth_text:nth-child(odd){
	background-color:#49EBFF;
} */
.auth_text>span{
	clear:both;
	display: block;
	margin: 0 auto;
	vertical-align: middle;
	font-size: 25px;
	padding-top: 30px;
	color: #fff;
}
.auth-text-info{
width:100%;
margin-top: 45px;
text-align: center;
}
footer{
	margin-top:80px;
}

</style>
<script>
$(function(){
	$(".auth").eq(0).click(function(){
		location.href="/signup/signup2";
	});
	$(".auth").eq(1).click(function(){
		location.href="/signup/writer";
	});
	$(".auth").eq(2).click(function(){
		location.href="/signup/manager";
	});
})
</script>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
		<div class="fl auth-text-info">
		<h1>회원 유형
			<small class="w100 fl mt10" style="font-size: 20px; color: #999;">자신의 회원 유형을 선택해주세요</small>
		</h1>
		</div>
			<div class="fl auth re_auth re_a_1">
				<img src="/resources/image/authicon/user.png">
				<div class="auth_text m_auth_text" style="background-color:#ABC1C5"><span class="m_auth_text_span">일반회원</span></div>
			</div>
			<div class="fl auth ml5p re_auth re_a_2" id="writer-box">
				<img src="/resources/image/authicon/writer.png">
				<div class="auth_text m_auth_text" style="background-color:#D2551F"><span class="m_auth_text_span">작가</span></div>
			</div>
			<div class="fr auth re_auth re_a_3" id="writer-box2">
				<img src="/resources/image/authicon/coworker.png">
				<div class="auth_text m_auth_text" style="background-color:#2B3D79"><span class="m_auth_text_span">매니저</span></div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
</body>
</html>