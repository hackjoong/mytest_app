<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/jquery-ui.min.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>회원가입</title>
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="container wrap">
		<div class="col-md-12 join-wrap" style="margin-top:50px;">
			<h3>회원가입</h3>
			<div class="table-wrap">
			<form action="/signUp/inputSignUp" method="POST" id="signupform">
			   <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
			
				<table>
					<tr>
						<td>아이디</td>
						<td><input type="text" name="id" id="id" style=" width:30%;"><button type="button" id="checkbtn">중복검사</button></td>					
					</tr>
					<tr>
						<td>비밀번호</td>
						<td><input type="password" onkeyup="checkpwd()" name="pw" id="pw"></td>
					</tr>
					<tr>
						<td>비밀번호 재확인</td>
						<td><input type="password" onkeyup="checkpwd()" id="repw"><div id="checkpassword"></div></td>
					</tr>
					<tr>
						<td>이름</td>
						<td><input type="text" name="name" id="name"></td>
					</tr>
					<tr>
						<td>생년월일</td>
						<td>
							&nbsp;
							<select class="year" name="year" id="year">
								<c:forEach var="year" begin="1920" end="2016" step="1" >
									<option value="${year}">${year}</option>
								</c:forEach>
							</select> 년
							<select class="month" name="month" id="month">
								<c:forEach var="month" begin="1" end="12" step="1">
									<option value="${month}">${month}</option>
								</c:forEach>
							</select> 년
							<select class="day" name="day" id="day">
								<c:forEach var="day" begin="1" end="31" step="1">
									<option value="${day}">${day}</option>
								</c:forEach>
							</select> 일
													
						</td>
					</tr>
					<tr>
						<td>이메일</td>
						<td><input type="text" name="email" id="email"></td>
					</tr>
					<tr>
						<td>휴대전화 번호</td>
						<td><input type="text" name="phonenum" id="phonenum"></td>
					</tr>
				</table>
				</form>
				<input type="submit" id="subm" value="회원가입">
			</div>
		</div>
	</div>
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />	
</body>
<script>	
	$("#subm").click(function(){
		var id=document.getElementById("id");
		var pw=document.getElementById("pw");
		var repw=document.getElementById("repw");
		var birth=document.getElementById("year").value+"-"+document.getElementById("month").value+"-"+document.getElementById("day").value;
		var email=document.getElementById("email");
		var phonenum=document.getElementById("phonenum");
		var name=document.getElementById("name");
		if (id.readOnly == false) {
			alert("아이디 중복확인을 해주세요");
			return false;
		}
		else if(pw.value!=repw.value){
			alert("비밀번호를 다시 확인해 주세요");
			pw.value="";
			repw.value="";
			return false;
		}
		else if(id.value==""){
			alert("아이디를 입력하세요");
			return false;
		}
		else if(pw.value==""){
			alert("비밀번호를 입력하세요");
			return false;
		}
		else if(name.value==""){
			alert("이름을 입력하세요");
			return false;
		}
		
		else if(birth==""){
			alert("생일을 입력하세요");
			return false;
		}
		
		else if(phonenum.value==""){
			alert("전화번호를 입력하세요");
			return false;
		}
		document.getElementById("signupform").submit();
	});
	$("#checkbtn").click(function(){ //아이디 체크 메소드 중복 이슈!!
		var id=document.getElementById("id").value;
		
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content"); // THIS WAS ADDED
		//var pattern_id = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{3,11}$/;
		var reg_exp=new RegExp("^[a-zA-Z][a-zA-Z0-9][a-zA-Z0-9\_]{3,11}$","g"); //"^[a-zA-Z][a-zA-Z0-9][a-zA-Z0-9\_]{3,11}$","g");
		var match=reg_exp.exec(id);
		//if(match==null||id.length<4||id.length>12){ //"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
		if(id.length<4||id.length>12){
			alert("아이디는 영문숫자 혼용 혹은 영문, 첫글자는 영문, 4자이상 12자 이하이여야 합니다!");
			document.getElementById("id").value = "";
			return false;
		}
		var data = {};
		var headers = {};
		data[csrfParameter] = csrfToken;
		data["id"] = id;
		headers[csrfHeader] = csrfToken;
		$.ajax({
			url : "/signUp/checkId",
			dataType : "json",
			type : "POST",
			headers : headers,
			data : data,
			success : function(data) {
				if(data.result=="TRUE"){
					var q = confirm(id+"는 사용할 수 있는 아이디 입니다. 사용하시겠습니까?");
					if(q==true){
						document.getElementById("id").readOnly=true;
						$("#id").css("background" , "gray");	
					}
					else{
						$("#id").val("");
					}
				}
				else{
					$("#id").val("");
					alert("중복된 아이디 입니다.");
				}
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "error:" + error);
			}

		});
		
	});
	function checkpwd() {
		var pwd = document.getElementById("pw").value;
		var repwd = document.getElementById("repw").value;
		if (pwd == repwd) {
			document.getElementById("checkpassword").innerHTML = "<b>비밀번호가 일치합니다</b>"
		} else {
			document.getElementById("checkpassword").innerHTML = "<b>비밀번호가 일치하지않습니다</b>"
		}
	}
</script>
</html>