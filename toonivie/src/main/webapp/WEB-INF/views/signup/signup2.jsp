<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta id="_csrf_parameter" name="_csrf_parameter" />
<meta id="_csrf" name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta id="_csrf_header" name="_csrf_header"content="${_csrf.headerName}" />
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="/resources/common/bootstrap/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"> -->
<link rel="stylesheet" href="/resources/common/bootstrap/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<!-- <script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script> -->
<script src="/resources/common/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/common/js/checkId.js"></script>
<!-- Add by yoo_dh0416 171020 -->
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">

<title>로그인</title>
</head>
<style>
#id{
width: 72%;
}
#checkbtn {
	width: 26%;
	height: 42px;
	background: #f38900;
	border: 0;
	color: #fff;
}
</style>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl wrap logins re_duqor" style="margin-top:90px;">
		<div class="login-form psh pb150 re_bnone">
			<h3 class="login-title" style="margin-top: 0px;">회원가입</h3>
			<span class="login-subtitle m_service">TOONIVIE의 서비스를 이용할 수 있습니다.</span>
			<form class="" method="post" action="/signup/insertUser"
				onsubmit="return submitCheck()">
				<%-- <input type="text" name="id" class="login-id lg-input" placeholder="아이디">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="비밀번호">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<button class="submits">로그인</button> --%>

				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}">
				<div class="form-group w100">
					<label for="email" class="cols-sm-2 control-label fl">분류</label>
					<div class="cols-sm-10">
						<div class="input-group fl"style="clear:both;">
							<div style="float: left; font-size:0.9em"><input type="radio" value="ROLE_USER" name="role" checked="checked">일반 회원&nbsp;&nbsp;</div>
							<div style="float: left; font-size:0.9em"><input type="radio" value="ROLE_WRITER" name="role" disabled="disabled">작가 회원&nbsp;&nbsp;</div>
							<div style="float: left; font-size:0.9em"><input type="radio" value="ROLE_COWORKER" name="role" disabled="disabled">협력 회원</div>
						</div>
					</div>
				</div>
				<div class="form-group fl w100" style="margin-top: 20px;">
					<label for="name" class="cols-sm-2 control-label fl">아이디</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="id" id="id"
								placeholder="ID" />
							<input type="hidden" id="idChecked" size="30">
							<button type="button" id="checkbtn">중복검사</button>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label fl">이메일</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="email" id="email"
								placeholder="E-mail" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="username" class="cols-sm-2 control-label fl">이름</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="name" id="name"
								placeholder="Name" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="password" class="cols-sm-2 control-label fl">비밀번호</label>
					<span class="re_PW_redText" style="coler:red;color: red;font-weight: normal;font-size: 14px;float: right;">※ 비밀번호는 8자 이상 20자 이하로 입력!</span>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="pw"
								id="pw" minlength="8" maxlength="20" placeholder="Password" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="confirm" class="cols-sm-2 control-label fl">비밀번호
						확인</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw"
								name="confirm" id="confirm" minlength="8" maxlength="20" placeholder="Confirm Password"/>
						</div>
					</div>
					<div class="cols-sm-10">
						<span class="check_pw_span"></span>
					</div>
				</div>

				<!-- <button class="submits">회원가입</button> -->
				<!-- <div class="form-group "> -->
				<input type="submit" class="submits submit-btn" value="회원가입" />
				<!-- </div> -->
			</form>
			<div class="w100 fl joins">
				<!-- <button class="lg-input join-btn" onclick="location.href='/signup/signupPage'">회원가입</button> -->
				<div class="half-find2 m_login_fff"><a class="pt5 db" style="color:#333;" href="/login">로그인</a></div>
				<!-- <span class="half-find"><a href="/signup/findAccount">비밀번호 찾기</a></span> -->
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script>
		$(document).ready(function() {
			$("#id").focus();
		});

		$(".confirm_pw").keyup(function() {
			var pw = $("[name = pw]").val();
			var re_pw = $("[name=confirm]").val();
			if (pw != re_pw || pw == "" || re_pw == "") {
				$(".check_pw_span").css("color", "red");
				$(".check_pw_span").text("비밀번호가 일치하지 않습니다.");
			} else {
				$(".check_pw_span").css("color", "blue");
				$(".check_pw_span").text("비밀번호가 일치합니다.");
			}
		});


		function submitCheck() {
			var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
			var pwReg = /([0-9A-Za-z]{4,11})/g;
			var nameReg = /([가-힣A-Za-z]{2,4})/g;
			var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
			var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
			var idValue = $("[name = id]").val();
			var pwValue = $("[name = pw]").val();
			var nameValue = $("[name = name]").val();
			var emailValue = $("[name = email]").val();
			var confirmValue = $("[name = confirm]").val();

			if (idReg.test(idValue) == false || idValue == "") {
				alert("아이디를 확인해주세요.");
				return false;
			}else if($("#idChecked").val() != "idChecked"){
				alert("아이디 중복확인해주세요");
				return false;
			} else if (emailReg.test(emailValue) == false || emailValue == "") {
				alert("이메일을 확인해주세요.");
				return false;
			} else if (nameReg.test(nameValue) == false || nameValue == "") {
				alert("이름을 확인해주세요.");
				return false;
			} else if (pwValue != confirmValue || pwReg.test(pwValue) == false
					|| pwValue == "") {
				alert("비밀번호를 확인해주세요.");
				return false;
			} else if(pwValue.length<8 || confirmValue.length<8 || pwValue.length>20 || confirmValue.length>20){
				alert("비밀번호 길이가 맞지 않습니다. 다시 입력해주세요");
				return false;
			}
		}
	</script>
</body>
</html>