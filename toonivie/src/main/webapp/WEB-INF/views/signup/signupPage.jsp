<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>SignUp</title>	
    <script src="/resources/common/js/jquery-3.1.0.min.js"></script>
    <script src="/resources/common/js/jquery-ui.min.js"></script>    
    <link rel="stylesheet" href="/resources/common/css/common.css">
    <link rel="stylesheet" href="/resources/common/css/lib.css">
    <link href="/resources/common/css/signup.css" rel="stylesheet">
    
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
    <!-- 합쳐지고 최소화된 최신 CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- 부가적인 테마 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">   
    <!-- 글리피콘 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>    
    <script src="/resources/common/js/form.js"></script>    
</head>
<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="container">
		<div class="row main">
			<div class="panel-heading">
               <div class="panel-title text-center">
               		<h3 class="title">회원가입</h3>
               		<hr />
               	</div>
            </div> 
			<div class="main-login main-center">
				<form class="form-horizontal" method="post" action="/signup/insertUser">	
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">				
					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">아이디</label>
						<div class="cols-sm-10">
							<div class="input-group">									
								<input type="text" class="form-control" name="id" id="id"  placeholder="ID"/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">이메일</label>
						<div class="cols-sm-10">
							<div class="input-group">									
								<input type="text" class="form-control" name="email" id="email"  placeholder="E-mail"/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="username" class="cols-sm-2 control-label">이름</label>
						<div class="cols-sm-10">
							<div class="input-group">									
								<input type="text" class="form-control" name="name" id="name"  placeholder="Name"/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">비밀번호</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="password" class="form-control confirm_pw" name="pw" id="pw"  placeholder="Password"/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">비밀번호 확인</label>
						<div class="cols-sm-10">
							<div class="input-group">									
								<input type="password" class="form-control confirm_pw" name="confirm" id="confirm"  placeholder="Confirm Password"/>
							</div>
						</div>
						<div class="cols-sm-10">
							<span class="check_pw_span"></span>
						</div>
					</div>

					<div class="form-group ">
						<input type="submit" class="submit-btn" value="회원가입" onclick="submitCheck()"/>
					</div>
					<div class="login-register">
			            <a href='/login'>Login</a>
			         </div>
				</form>

			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script>
	$(".confirm_pw").keyup(function(){
		var pw = $("[name = pw]").val();
		var re_pw = $("[name=confirm]").val();
		if(pw!=re_pw||pw==""||re_pw==""){
			$(".check_pw_span").css("color","red");
			$(".check_pw_span").text("비밀번호가 일치하지 않습니다.");
		}else{
			$(".check_pw_span").css("color","blue");
			$(".check_pw_span").text("비밀번호가 일치합니다.");
		}
	})
		function submitCheck(){
			var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
			var pwReg = /([0-9A-Za-z]{4,11})/g;
			var nameReg = /([가-힣A-Za-z]{2,4})/g;
			var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
			var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
			var idValue = $("[name = id]").val();
			var pwValue = $("[name = pw]").val();
			var nameValue = $("[name = name]").val();
			var emailValue = $("[name = email]").val();
			var confirmValue = $("[name = confirm]").val();
			
			if(idReg.test(idValue) == false || idValue == ""){
				alert("아이디를 확인해주세요.");
				return false;
			}else if(emailReg.test(emailValue) == false || emailValue == ""){
				alert("이메일을 확인해주세요.");
				return false;
			}else if(nameReg.test(nameValue) == false || nameValue == ""){
				alert("이름을 확인해주세요.");
				return false;
			}else if(pwValue != confirmValue || pwReg.test(pwValue) == false || pwValue == ""){
				alert("비밀번호를 확인해주세요.");
				return false;
			}						
				
		}
	</script>
</body>
</html>