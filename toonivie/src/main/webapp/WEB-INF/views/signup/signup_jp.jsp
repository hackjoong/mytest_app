<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta id="_csrf_parameter" name="_csrf_parameter" />
<meta id="_csrf" name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta id="_csrf_header" name="_csrf_header"
	content="${_csrf.headerName}" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/joinlogin.css">
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>



<title>会員加入</title>
</head>
<style>
#id{
width: 87%;
}
#checkbtn {
	width: 13%;
	height: 34px;
	background: #f38900;
	border: 0;
	color: #fff;
	collspace:collspace;
}
</style>
<body>
	<c:import url="/WEB-INF/views/header&footer/jp/header_jp.jsp" />
	<div class="w100 fl wrap logins">
		<div class="login-form" style="top: 40%;">
			<h3 class="login-title" style="margin-top: 0px;">会員加入</h3>
			<span class="login-subtitle">TOONIVIEのサービスを使えます。</span>
			<form class="" method="post" action="/signup/insertUser_jp"
				onsubmit="return submitCheck()">
				<%-- <input type="text" name="id" class="login-id lg-input" placeholder="아이디">
			<input type="password" name="pw" class="login-pw lg-input" placeholder="비밀번호">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>				
			<button class="submits">로그인</button> --%>

				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}">
				<div class="form-group fl w100" style="margin-top: 20px;">
					<label for="name" class="cols-sm-2 control-label fl">ハンドルネーム</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="id" id="id"
								placeholder="ハンドルネーム" />
							<button type="button" id="checkbtn">重複検査</button>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label fl">メール</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="email" id="email"
								placeholder="メール" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="username" class="cols-sm-2 control-label fl">ネーム</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="text" class="form-control" name="name" id="name"
								placeholder="ネーム" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="password" class="cols-sm-2 control-label fl">パスワード</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw" name="pw"
								id="pw" placeholder="パスワード" />
						</div>
					</div>
				</div>

				<div class="form-group fl w100">
					<label for="confirm" class="cols-sm-2 control-label fl">パスワード チェック</label>
					<div class="cols-sm-10">
						<div class="input-group w100">
							<input type="password" class="form-control confirm_pw"
								name="confirm" id="confirm" placeholder="パスワード チェック" />
						</div>
					</div>
					<div class="cols-sm-10">
						<span class="check_pw_span"></span>
					</div>
				</div>

				<!-- <button class="submits">회원가입</button> -->
				<!-- <div class="form-group "> -->
				<input type="submit" class="submits submit-btn" value="会員加入"
					onclick="submitCheck()" />
				<!-- </div> -->
			</form>
			<div class="w100 fl joins">
				<!-- <button class="lg-input join-btn" onclick="location.href='/signup/signupPage'">회원가입</button> -->
				<span class="half-find2"><a href="/login_jp">ログイン</a></span>
				<!-- <span class="half-find"><a href="/signup/findAccount">비밀번호 찾기</a></span> -->
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script>
		$(document).ready(function() {
			$("#id").focus();
			$("#checkbtn").click(function() {
				var id = document.getElementById("id").value;

				var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
				var csrfToken = $("meta[name='_csrf']").attr("content");
				var csrfHeader = $("meta[name='_csrf_header']").attr("content"); // THIS WAS ADDED
				var reg_exp = new RegExp("^[a-zA-Z][a-zA-Z0-9]{3,11}$", "g");
				var match = reg_exp.exec(id);
				if (match == null || id.length < 4 || id.length > 12) {
					alert("IDは,英語,数字又は英語から成るものとし,最初の文字は英語又は12文字以上の長さで表記するものとする。");
					document.getElementById("id").value = "";
					return false;
				}
				var data = {};
				var headers = {};
				data[csrfParameter] = csrfToken;
				data["id"] = id;
				headers[csrfHeader] = csrfToken;
				$.ajax({
					url : "/signup/checkId",
					dataType : "json",
					type : "POST",
					headers : headers,
					data : data,
					success : function(data) {
						if (data.message == "TRUE") {
							var q = confirm(id+"は有効なIDです。 使いたいですか?");
							if (q == true) {
								document.getElementById("id").readOnly = true;
								$("#id").css("background", "gray");
								$('#id').css("color","#fff");
							} else {
								$("#id").val("");
							}
						} else {
							$("#id").val("");
							alert("重複ID。");
						}
					},
					error : function(request, status, error) {
						alert("code:" + request.status + "\n" + "error:" + error);
					}

				});

			});
		});

		$(".confirm_pw").keyup(function() {
			var pw = $("[name = pw]").val();
			var re_pw = $("[name=confirm]").val();
			if (pw != re_pw || pw == "" || re_pw == "") {
				$(".check_pw_span").css("color", "red");
				$(".check_pw_span").text("Passwords do not match.");
			} else {
				$(".check_pw_span").css("color", "blue");
				$(".check_pw_span").text("Password matches.");
			}
		});

		function submitCheck() {
			var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
			var pwReg = /([0-9A-Za-z]{4,11})/g;
			var nameReg = /([가-힣A-Za-z]{2,4})/g;
			var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
			var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
			var idValue = $("[name = id]").val();
			var pwValue = $("[name = pw]").val();
			var nameValue = $("[name = name]").val();
			var emailValue = $("[name = email]").val();
			var confirmValue = $("[name = confirm]").val();

			if (idReg.test(idValue) == false || idValue == "") {
				alert("IDを確認してください。");
				return false;
			} else if (emailReg.test(emailValue) == false || emailValue == "") {
				alert("メールを確認してください。");
				return false;
			} else if (nameReg.test(nameValue) == false || nameValue == "") {
				alert("名前を確認してください。");
				return false;
			} else if (pwValue != confirmValue || pwReg.test(pwValue) == false
					|| pwValue == "") {
				alert("暗証番号を確認してください。");
				return false;
			}
		}
	</script>
</body>
</html>