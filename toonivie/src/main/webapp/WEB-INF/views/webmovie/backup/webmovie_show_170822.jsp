<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css"> <!-- add by shkwak, 2017-06-07 -->

<script src="/resources/common/js/webmovie.js"></script> <!-- add by shkwak, 2017-06-07 -->

<body>
<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">				
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-8 col-xs-12 pd-0">						
						<video id="myVideo" controls src="" class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;" poster="" onclick="/*playPause();*/">
						</video>
						<!--video id="myVideo" controls src="/resources/cvMov/2017-04-24_1.mp4" class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;" poster="/resources/image/2017-04-24_1.mp4_000004590.jpg" onclick="/*playPause();*/">
						Your user agent does not support the HTML5 Video element. 
						</video-->
					</div>
					<div class="col-md-4 col-xs-12 pd-0" style="background-color:;">
						<div class="info">
						    <h2 class="title webmovie-title"></h2>
						    <p class="artist">
						    	<img class="thumbnail_new" src="/resources/image/thumnail_1.png" width="100px">
						    	작가 : <span class="artist-name"></span>						    	
						    </p>
						    <div class="info-btns" style="display:none;">
						        <div class="info-btn-wrap"><button id="info-btn-subscription" class="info-btn-subscription btn-subscription" data-subscription="false"><span class="info-text-false"><i class="icon"></i><span>구독하기</span></span><span class="info-text-true"><i class="icon"></i><span>구독중</span></span><span class="info-text-truehover"><i class="icon"></i><span>구독취소</span></span></button>
						            <div class="coach-tooltip is-hide"><span>최신화 놓치기 싫다면<br>지금 구독하세요.</span><button class="coach-tooltip-close"><i class="icon"></i><span class="a11y">닫기</span></button></div>
						        </div><a class="btn-first-episode" href="#" style="display: inline;">처음부터</a></div>
						    <div class="genre_new">장르 : <span class="genre-name"></span></div>
						    <div class="publisher"></div>
						    <div class="badge-list">HD</div>
						    <div class="summary" style="padding: 15px 0;"></div>							
						</div>					
					</div>					
				</div>				
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<h2 class="pdl-15">웹툰무비 보기</h2>				
					<table class="lst_view">				
					<tbody id="webmovie_1">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show(1);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2017-04-24</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/one_sided_love_1.png" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">1화</div>
					        	<div class="sub_title sub_title_1">에피소드 1</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/one_sided_love_2.png" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">2화</div>
					        	<div class="sub_title sub_title_2">에피소드 2</div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>								
					</tbody>
					<tbody id="webmovie_2" style="display: none;">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show(2);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2016-11-02</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/horizon_1.jpg" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">1화</div>
					        	<div class="sub_title sub_title_1">소년과 소녀</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/horizon_2.jpg" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">2화</div>
					        	<div class="sub_title sub_title_2"> </div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>								
					</tbody>
					<tbody id="webmovie_3" style="display: none;">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show(3);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2016-11-02</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/three_kingdoms_1_2.jpg" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">1화</div>
					        	<div class="sub_title sub_title_1">에피소드 1</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/three_kingdoms_1_2.jpg" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">2화</div>
					        	<div class="sub_title sub_title_2">에피소드 2</div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">보기</div>
					        </td>
					    </tr>								
					</tbody>
					</table>
					
					<ul class="genre-menu col-md-12">	
					</ul>
					
					<div class="w100 fl genre-cont" style="display:none;">
						<div class="episode gen">
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(1);" style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>									
									<div class="conts-cover"><span class="genre-cont-subtitle">드라마</span><br>
									<span class="genre-cont-title">짝사랑의 추억</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(2);" style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">스릴러</span><br>
									<span class="genre-cont-title">수평선</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(3);" style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">액션</span><br>
									<span class="genre-cont-title">삼국지</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(4);" style="background-image: url('/resources/image/엽기적인 그녀.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">드라마</span><br>
									<span class="genre-cont-title">엽기적인 그녀</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover"><span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>					
						</div>													
					</div>
					<div class="w100 fl best-toon" style="display:none;">						
						<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br>
										<span class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>				
					<div class="col-md-3 rank col-xs-12">
						<div class="add-toon tc" onclick="location.href='/cv/effect'"  style="display:none;">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>만화 올리기</span>
						</div>
						<div class="w100 fl rank-title">
							<!--div class="rank-head">
								실시간 웹툰 순위
							</div-->
							<h2>웹툰 순위 보기</h2>
							<div class="rank-cont">
								<span class="cp"  style="font-size: 1.25em" onclick="webmovie_show(1);"><b>1</b> 짝사랑의 추억</span>
								<span class="cp"  style="font-size: 1.25em" onclick="webmovie_show(2);"><b>2</b> 수평선</span>
								<span class="cp"  style="font-size: 1.25em" onclick="webmovie_show(3);"><b>3</b> 삼국지</span>
								<span class="cp"  style="font-size: 1.25em" onclick="webmovie_show(4);"><b>4</b> 엽기적인 그녀</span>
								<span><b>5</b> ...</span>
								<!--span><b>6</b> ...</span>
								<span><b>7</b> ...</span>
								<span><b>8</b> ...</span>
								<span><b>9</b> ...</span>
								<span><b>10</b> ...</span-->								
							</div>
						</div>
						<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>								
							</li>
						</div-->
					</div>
				</div>
			</div>			
		</div>
		
<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script src="/resources/common/js/js.js"></script>
<script>
var myVideo = document.getElementById("myVideo");

console.log('param.id : ' + ${param.id});
webmovie_show(${param.id});
</script>

</body>
</html>