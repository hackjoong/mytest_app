<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>webmovie_show_ch</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<!-- add by shkwak, 2017-06-07 -->
<script src="/resources/common/js/webmovie.js"></script>
<!-- add by shkwak, 2017-06-07 -->

<body>
	<c:import url="/WEB-INF/views/header&footer/ch/header_ch.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-8 col-xs-12 pd-0">
						<video id="myVideo" controls src=""
							class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;"
							poster="" onclick="/*playPause();*/"> Your user agent
							does not support the HTML5 Video element.
						</video>
					</div>
					<div class="col-md-4 col-xs-12 pd-0" style="background-color:;">
						<div class="info">
							<h2 class="title webmovie-title"></h2>
							<p class="artist">
								<img class="thumbnail_new" src="/resources/image/thumnail_1.png"
									width="100px"> 作家 : <span class="artist-name"></span>
							</p>
							<div class="info-btns" style="display: none;">
								<div class="info-btn-wrap">
									<button id="info-btn-subscription"
										class="info-btn-subscription btn-subscription"
										data-subscription="false">
										<span class="info-text-false"><i class="icon"></i><span>구독하기</span></span><span
											class="info-text-true"><i class="icon"></i><span>구독중</span></span><span
											class="info-text-truehover"><i class="icon"></i><span>구독취소</span></span>
									</button>
									<div class="coach-tooltip is-hide">
										<span>如果你不想失去的最新版本,<br>现在订阅吧。
										</span>
										<button class="coach-tooltip-close">
											<i class="icon"></i><span class="a11y">닫기</span>
										</button>
									</div>
								</div>
								<a class="btn-first-episode" href="#" style="display: inline;">从一开始</a>
							</div>
							<div class="genre_new">
								内容 : <span class="genre-name"></span>
							</div>
							<div class="publisher"></div>
							<div class="badge-list">HD</div>
							<div class="summary" style="padding: 15px 0;"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<h2 class="pdl-15">漫画看电影吧</h2>
					<table class="lst_view">
						<tbody id="webmovie_1">
							<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show(1);">
								<td class="w150 none">
									<div class="num">01</div>
									<div class="date updated_1">2017-04-24</div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/one_sided_love_1.png" width="225"
										height="90" alt="" class="thumbnail_1">
								</a></td>
								<td class="tit">
									<div class="">第一集</div>
									<div class="sub_title sub_title_1">插曲 1</div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
							<tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show('준비중');">
								<td class="w150 check_on">
									<div class="num">02</div>
									<div class="date"></div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/one_sided_love_2.png" width="225"
										height="90" alt="" class="thumbnail_2">
								</a></td>
								<td class="tit">
									<div class="">第二集</div>
									<div class="sub_title sub_title_2">插曲 2</div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
						</tbody>
						<tbody id="webmovie_2" style="display: none;">
							<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show(2);">
								<td class="w150 none">
									<div class="num">01</div>
									<div class="date updated_1">2016-11-02</div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/horizon_1.jpg" width="225" height="90"
										alt="" class="thumbnail_1">
								</a></td>
								<td class="tit">
									<div class="">第一集</div>
									<div class="sub_title sub_title_1">男孩和女孩</div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
							<tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show('준비중');">
								<td class="w150 check_on">
									<div class="num">02</div>
									<div class="date"></div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/horizon_2.jpg" width="225" height="90"
										alt="" class="thumbnail_2">
								</a></td>
								<td class="tit">
									<div class="">第二集</div>
									<div class="sub_title sub_title_2"></div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
						</tbody>
						<tbody id="webmovie_3" style="display: none;">
							<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show(3);">
								<td class="w150 none">
									<div class="num">01</div>
									<div class="date updated_1">2016-11-02</div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/three_kingdoms_1_2.jpg" width="225"
										height="90" alt="" class="thumbnail_1">
								</a></td>
								<td class="tit">
									<div class="">第一集</div>
									<div class="sub_title sub_title_1">插曲 1</div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
							<tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show('준비중');">
								<td class="w150 check_on">
									<div class="num">02</div>
									<div class="date"></div>
								</td>
								<td class="img"><a class="tmb" href="javascript:;"> <img
										src="/resources/image/three_kingdoms_1_2.jpg" width="225"
										height="90" alt="" class="thumbnail_2">
								</a></td>
								<td class="tit">
									<div class="">第二集</div>
									<div class="sub_title sub_title_2">插曲 2</div>
								</td>
								<td class="coin">
									<div class="free_bt">视图</div>
								</td>
							</tr>
						</tbody>
					</table>

					<ul class="genre-menu col-md-12">
					</ul>

					<div class="w100 fl genre-cont" style="display: none;">
						<div class="episode gen">
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_ch(1);"
										style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">电视剧</span><br> <span
											class="genre-cont-title">暗恋的回忆</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_ch(2);"
										style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">惊悚</span><br> <span
											class="genre-cont-title">水平線</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_ch(3);"
										style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">行动</span><br> <span
											class="genre-cont-title">三国志</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_ch(4);"
										style="background-image: url('/resources/image/엽기적인 그녀.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">电视剧</span><br> <span
											class="genre-cont-title">我的野蛮女友</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">流派</span><br> <span
											class="genre-cont-title">头衔</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">流派</span><br> <span
											class="genre-cont-title">头衔</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl best-toon" style="display: none;">
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">流派</span><br> <span
										class="genre-cont-title">头衔</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">流派</span><br> <span
										class="genre-cont-title">头衔</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">流派</span><br> <span
										class="genre-cont-title">头衔</span><br>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<div class="add-toon tc" onclick="location.href='/cv/effect'"
						style="display: none;">
						<img src="/resources/image/upload icon.png" class="upload-con">
						<span>漫画创作</span>
					</div>
					<div class="w100 fl rank-title">
						<!--div class="rank-head">
								실시간 웹툰 순위
							</div-->
						<h3>实时网络漫画排行榜</h3>
						<div class="rank-type" style="">
							<span>&nbsp;&#124;&nbsp;最新</span> <span>&nbsp;&#124;&nbsp;帖子</span>
							<span>查询</span>
						</div>
						<div class="rank-cont">
							<span class="cp"
								onclick="webmovie_show_ch(1);"><b>1</b> 暗恋的回忆</span> <span
								class="cp"
								onclick="webmovie_show_ch(2);"><b>2</b> 水平線</span> <span
								class="cp"
								onclick="webmovie_show_ch(3);"><b>3</b> 三国志</span> <span
								class="cp"
								onclick="webmovie_show_ch(4);"><b>4</b> 我的野蛮女友</span>
							<c:set var="i" value="5" />
							<c:forEach items="${toon}" var="toonivie">
								<span class="cp" id='toon${toonivie.getTno()}'									
									onclick="location.href='/webmovie2_ch/${toonivie.getTno()}'"
									data-file='${toonivie.getFile()}'
									data-title='${toonivie.getTitle()}'
									data-genre='${toonivie.getGenre()}'
									data-writer='${toonivie.getWriter()}'
									data-content='${toonivie.getContent()}'
									data-date='${toonivie.getWridate()}'><b>${i}</b>
									${toonivie.getTitle()} </span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</div>
					</div>
					<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>								
							</li>
						</div-->
				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
<script src="/resources/common/js/js.js"></script>
<script>
$('.rank-type').find('span').eq(0).click(function(){ //조회순 클릭
	var data = {};
	$.ajax({
		url : "/order/lastest",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 暗恋的回忆</a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 水平線</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 三国志</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 我的野蛮女友</span>');
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+5) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(1).click(function(){
	var data = {};
	$.ajax({
		url : "/order/commentOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 暗恋的回忆</a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 水平線</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 三国志</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 我的野蛮女友</span>');
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+5) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(2).click(function(){
	var data = {};
	$.ajax({
		url : "/order/countOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 暗恋的回忆</a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 水平線</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 三国志</span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 我的野蛮女友</span>');
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+5) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

var myVideo = document.getElementById("myVideo");

console.log('param.id : ' + ${param.id});
webmovie_show_ch(${param.id});

function webmovie_show_ch(id) {	
	console.log(myVideo.src, id);	
	switch(id) {
    case 1: //'짝사랑의 추억':
        myVideo.src = "/resources/cvMov/2017-04-24_1.mp4";
        myVideo.poster = "/resources/image/2017-04-24_1.mp4_000004590.jpg";
        $(".webmovie-title").html("暗恋的回忆");
        $(".thumbnail_new").attr('src',"/resources/image/thumnail_1.png");
		$(".artist-name").html("无线电听众的故事");
        $(".genre-name").html("电视剧  / 插曲"); 
        $(".summary").html("SBS 컬투쇼의 베스트 사연을 웹툰으로 제작한 영상으로 한 여성의 짝사랑에 대한 에피소드를 소개하고 있다.");
        $("#webmovie_1").css('display','table-row-group');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','none');
        break;
    case 2: //'수평선':
        myVideo.src = "/resources/cvMov/2016-11-02_1.mp4";
        myVideo.poster = "/resources/image/2016-11-02_1.mp4_000003956.jpg";
        $(".webmovie-title").html("水平線");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_2_1.jpg");
        $(".artist-name").html("Jung Ji-hoon");
        $(".genre-name").html("惊悚  / 神秘");
        $(".summary").html("세상이 멸망한 어느 날, 부모를 잃은 소년, 소녀가 영원히 함께 앞으로 걸어갈 수 있다는 한 가지 희망만으로 살아가는데... 그런 희망조차 빼앗으려는 망가진 어른들의 등장!");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','table-row-group');
        $("#webmovie_3").css('display','none');
        break;
    case 3: //'삼국지':
    	myVideo.src = "/resources/cvMov/2016-11-02_2.mp4";
    	myVideo.poster = "/resources/image/2016-11-02_2.mp4_000010407.jpg";
    	$(".webmovie-title").html("三国志");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_3.jpg");
    	$(".artist-name").html("Park Bong Sung");
        $(".genre-name").html("史剧  / 行动");
        $(".summary").html("작가 박봉성은 단순희 삼국지에 그림을 추가해 만화삼국지를 펴낸것이 아니라, 방대한 자료수집과 중국현지 취재를 통해 나관중도 간과했던 새로운 사실을 밝혀내고, 시대상황을 소상하게 설명하고 있다.");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','table-row-group');
    	break;
    case 5: //'TEST':
    	myVideo.src = "/resources/cvMov/2016-11-02_2.mp4";
    	myVideo.poster = "/resources/image/2016-11-02_2.mp4_000010407.jpg";
    	$(".webmovie-title").html("");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_3.jpg");
    	$(".artist-name").html("");
        $(".genre-name").html("");
        $(".summary").html("");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','table-row-group');
    	break;

    default:
        alert("正在准备。");
	}
}
</script>
</body>
</html>