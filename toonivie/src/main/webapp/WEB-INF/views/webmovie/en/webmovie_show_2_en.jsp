﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>webmovie_show_2_en</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<!-- add by yoo_dh0416 2018-02-07 -->
<link rel="stylesheet" href="/resources/common/css/commentList.css">
<script src="/resources/common/js/en/comment.js"></script>
<link rel="stylesheet" href="/resources/common/jquery/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<body>
	<c:import url="/WEB-INF/views/header&footer/en/header_en.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-8 col-xs-12 pd-0">
						<video id="myVideo" controls src="${toonInfor.getFile() }"
							class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;"
							poster="" onclick="/*playPause();*/"> Your user agent
							does not support the HTML5 Video element.</video>
						<!--video id="myVideo" controls src="/resources/cvMov/2017-04-24_1.mp4" class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;" poster="/resources/image/2017-04-24_1.mp4_000004590.jpg" onclick="/*playPause();*/">
						Your user agent does not support the HTML5 Video element. 
						</video-->
					</div>

					<div class="col-md-4 col-xs-12 pd-0" style="background-color:;">
						<div class="info">
							<span style="float: right; margin-top:20px; margin-right:5px;">View : ${toonInfor.getViewCount() }</span>
							<h2 class="title webmovie-title" style="">${toonInfor.getTitle() }</h2>
							
							<p class="artist">
								<c:set value="${profile }" var="profiles" />
								<c:choose>
									<c:when test="${profiles != null }">
										<img class="thumbnail_new" src="${profiles }"
											width="100px">
									</c:when>
									<c:otherwise>
										<img class="thumbnail_new" src="/resources/profile/default/profile.jpg"
											width="100px">
									</c:otherwise>
								</c:choose>
								Writer : <span class="artist-name">${toonInfor.getWriter() }</span>
									
							</p>
							<div class="info-btns" style="display: none;">
								<div class="info-btn-wrap">
									<button id="info-btn-subscription"
										class="info-btn-subscription btn-subscription"
										data-subscription="false">
										<span class="info-text-false"><i class="icon"></i><span>구독하기</span></span><span
											class="info-text-true"><i class="icon"></i><span>구독중</span></span><span
											class="info-text-truehover"><i class="icon"></i><span>구독취소</span></span>
									</button>
									<div class="coach-tooltip is-hide">
										<span>최신화 놓치기 싫다면<br>지금 구독하세요.
										</span>
										<button class="coach-tooltip-close">
											<i class="icon"></i><span class="a11y">닫기</span>
										</button>
									</div>
								</div>
								<a class="btn-first-episode" href="#" style="display: inline;">처음부터</a>
							</div>
							<div class="genre_new">
								Genre : <span class="genre-name">${toonInfor.getGenre() }</span>
							</div>
							<div class="publisher"></div>
							<div class="badge-list">HD</div>
							<div class="summary" style="height:275px;padding: 15px 0; overflow: auto;">${toonInfor.getContent() }</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<h2 class="pdl-15">Watch the Webtoons Movie</h2>				
					<table class="lst_view">				
					<tbody id="webmovie_1">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en(1);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2017-04-24</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/one_sided_love_1.png" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The first episode</div>
					        	<div class="sub_title sub_title_1">epsode 1</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/one_sided_love_2.png" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The second episode</div>
					        	<div class="sub_title sub_title_2">epsode 2</div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>								
					</tbody>
					<tbody id="webmovie_2" style="display: none;">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en(2);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2016-11-02</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/horizon_1.jpg" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The first episode</div>
					        	<div class="sub_title sub_title_1">Boys and Girls</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/horizon_2.jpg" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The second episode</div>
					        	<div class="sub_title sub_title_2"> </div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>								
					</tbody>
					<tbody id="webmovie_3" style="display: none;">
					    <tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en(3);">
					        <td class="w150 none">
					            <div class="num">01</div>
					            <div class="date updated_1">2016-11-02</div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">              
					                <img src="/resources/image/three_kingdoms_1_2.jpg" width="225" height="90" alt="" class="thumbnail_1">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The first episode</div>
					        	<div class="sub_title sub_title_1">episode 1</div>
					        </td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>
					    <tr data-ep_no="002" data-p="3" data-rp="2" class="ep_list_tr" style="box-sizing: border-box; background-color: white;" onclick="webmovie_show_en('준비중');">
					        <td class="w150 check_on">
					            <div class="num">02</div>
					            <div class="date"> </div>
					        </td>
					        <td class="img">
					            <a class="tmb" href="javascript:;">
					                <img src="/resources/image/three_kingdoms_1_2.jpg" width="225" height="90" alt="" class="thumbnail_2">
					            </a>
					        </td>
					        <td class="tit">
					        	<div class="">The second episode</div>
					        	<div class="sub_title sub_title_2">episode 2</div>	
							</td>
					        <td class="coin">
					            <div class="free_bt">View</div>
					        </td>
					    </tr>								
					</tbody>
					</table>
					<!--  댓글 start-->
					<div id="dialog" style="display:none;">
					<h5>Please enter your membership password in case of a member.<br/><span style="color:red; line-height:30px;">※Case sensitive.</span></h5>
					<form>
						<input type="password" placeholder="Password" class="form-control" style="width:70%;height:32px; font-size:14px;float: left;" required="required">
						<button type="button"class="btn pwsubmit" style="font-size:14px;float: right;">Submit</button>
					</form>
					</div>
					<input type="hidden" name="idnick" value="${currentUser.getId() }">
					<input type="hidden" name="pwnick" value="${currentUser.getNohashpw() }">
					<form name="form1">
						<div class="panel-body">
							<div class="clearfix">
								<div class="pull-left">
									<h3>Comment<small class="text-12 text-red"> (<span id="total_cnt"><c:out value="${countComment }"></c:out></span>)	</small></h3>
									
						
								</div>
								<div class="pull-right">
									<small class="text-muted"> <span id="comment_char_cnt">0</span>/200
									</small>
								</div>
							</div>
							<div class="row row-sm">
								<div class="col-xs-10 comment_wrap" style="margin-bottom:5px;">
									<input type="hidden" name="tno" value="${toonInfor.getTno() }">
									
									<input type="text" name="nickname" id="id" class="comment_id" placeholder="ID" value="${currentUser.getId() }" autocomplete=off style="width:17%;">
									<input type="password" name="pw" id="comment_pwd" class="comment_pw" placeholder="Password" value="${currentUser.getNohashpw() }" autocomplete=off>
								</div>
								<div class="col-xs-10">						
									<textarea name="comment" id="comment"
										class="form-control"
										placeholder="Non-subject comments, spoilers, or malicious comments can be deleted without warning."
										onkeydown="if($(this).val().length > 200) { alert('You can register the contents of less than 200 characters.'); return false; } else { $('#comment_char_cnt').html($(this).val().length); }"
										onkeyup="if($(this).val().length > 200) { alert('You can register the contents of less than 200 characters.'); return false; } else { $('#comment_char_cnt').html($(this).val().length);  }"
										maxlength="200"></textarea>
								</div>
								<div class="col-xs-2">
									<button type="button" class="btn btn-block btn-dgray btn-comment">submit</button>
								</div>
							</div>
						</div>
					</form>
					<ul class="comments-list" id="comment_list">
					</ul>
					<!-- 댓글 end -->

					<ul class="genre-menu col-md-12">
					</ul>

					<div class="w100 fl genre-cont" style="display: none;">
						<div class="episode gen">
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_en(1);"
										style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">드라마</span><br> <span
											class="genre-cont-title">짝사랑의 추억</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_en(2);"
										style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">스릴러</span><br> <span
											class="genre-cont-title">수평선</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_en(3);"
										style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">액션</span><br> <span
											class="genre-cont-title">삼국지</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show_en(4);"
										style="background-image: url('/resources/image/엽기적인 그녀.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">드라마</span><br> <span
											class="genre-cont-title">엽기적인 그녀</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl best-toon" style="display: none;">
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<div class="add-toon tc" onclick="location.href='/cv/effect'"
						style="display: none;">
						<img src="/resources/image/upload icon.png" class="upload-con">
						<span>만화 올리기</span>
					</div>
					<div class="w100 fl rank-title">
						<!--div class="rank-head">
								실시간 웹툰 순위
							</div-->
						<h2>Toonivie Ranking</h2>
						<div class="rank-type" style="">							
							<span>&nbsp;&#124;&nbsp;Latest</span>
							<span>&nbsp;&#124;&nbsp;Comment</span>
							<span>View</span>
						</div>
						<div class="rank-cont">
							<!-- <span class="cp" onclick="webmovie_show_en(1);"><b>1</b> Memories of one-sided love</span> 
							<span class="cp" onclick="webmovie_show_en(2);"><b>2</b> Horizon</span>
							<span class="cp" onclick="webmovie_show_en(3);"><b>3</b> The Three Kingdoms</span>
							<span class="cp" onclick="webmovie_show_en(4);"><b>4</b> My Sassy Girl</span> -->
							<!-- <span class="cp"   onclick="webmovie_show(5);"><b>5</b> test</span> -->
							<c:set var="i" value="1" />
							<c:forEach items="${toonsunwi}" var="toonivie">
								<span class="cp" id='toon${toonivie.getTno()}'
									
									onclick="location.href='/webmovie2_en/${toonivie.getTno()}'"
									data-file='${toonivie.getFile()}'
									data-title='${toonivie.getTitle()}'
									data-genre='${toonivie.getGenre()}'
									data-writer='${toonivie.getWriter()}'
									data-content='${toonivie.getContent()}'
									data-date='${toonivie.getWridate()}'><b>${i}</b>
									${toonivie.getTitle()} </span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</div>
					</div>
					<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>								
							</li>
						</div-->
				</div>
				
				
				<!-- 			<span>웹툰 정보리스트</span>
	    <div id="listReply" style="width:100%; height:200px; overflow: auto; overflow: x-hidden; margin-top: 5px;">
	    <table class="table1 board-title" style="width:95.8%; margin-top: 10px; float: left; border:1px solid #000">
                 <tr style="border:1px solid #000; text-align: center">
                    	<td>파일</td>
                    	<td>장르</td>
                    	<td>제목</td>
                    	<td>작가</td>
                    	<td>내용</td>
                    	<td>등록 날짜</td>
                	</tr>     
                	           
        			<c:forEach items="${toon}" var="toonivie"> 
				       	<tr style="cursor: pointer; text-align: center">
				       		<td><span style="font-size:15px">${toonivie.getFile()}</span>
				           	<td><span style="font-size:15px">${toonivie.getGenre()}</span>
				           	<td><span style="font-size:15px">${toonivie.getTitle()}</span>
				           	<td><span style="font-size:15px">${toonivie.getWriter()}</span>
				           	<td><span style="font-size:15px">${toonivie.getContent()}</span>
				           	<td><span style="font-size:15px"><fmt:formatDate type ="date" value="${toonivie.getWridate()}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
				           	</td>
				       	</tr>
				    </c:forEach> 
            </table>-->

			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>
<script>
var myVideo = document.getElementById("myVideo");




function webmovie_show_en(id) {	
	console.log(myVideo.src, id);
	
	switch(id) {
    case 1: //'짝사랑의 추억':
        myVideo.src = "/resources/cvMov/2017-04-24_1.mp4";
        myVideo.poster = "/resources/image/2017-04-24_1.mp4_000004590.jpg";
        $(".webmovie-title").html("Memories of unrequited love");
        $(".thumbnail_new").attr('src',"/resources/image/thumnail_1.png");
		$(".artist-name").html("Radio audience's story");
        $(".genre-name").html("Drama / episode"); 
        $(".summary").html("SBS 컬투쇼의 베스트 사연을 웹툰으로 제작한 영상으로 한 여성의 짝사랑에 대한 에피소드를 소개하고 있다.");
        $("#webmovie_1").css('display','table-row-group');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','none');
        break;
    case 2: //'수평선':
        myVideo.src = "/resources/cvMov/2016-11-02_1.mp4";
        myVideo.poster = "/resources/image/2016-11-02_1.mp4_000003956.jpg";
        $(".webmovie-title").html("Horizon");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_2_1.jpg");
        $(".artist-name").html("Jung Ji-hoon");
        $(".genre-name").html("A thriller / mystery");
        $(".summary").html("세상이 멸망한 어느 날, 부모를 잃은 소년, 소녀가 영원히 함께 앞으로 걸어갈 수 있다는 한 가지 희망만으로 살아가는데... 그런 희망조차 빼앗으려는 망가진 어른들의 등장!");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','table-row-group');
        $("#webmovie_3").css('display','none');
        break;
    case 3: //'삼국지':
    	myVideo.src = "/resources/cvMov/2016-11-02_2.mp4";
    	myVideo.poster = "/resources/image/2016-11-02_2.mp4_000010407.jpg";
    	$(".webmovie-title").html("The Romance of the Three Kingdoms");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_3.jpg");
    	$(".artist-name").html("Park Bong Sung");
        $(".genre-name").html("the historical drama / Action");
        $(".summary").html("작가 박봉성은 단순희 삼국지에 그림을 추가해 만화삼국지를 펴낸것이 아니라, 방대한 자료수집과 중국현지 취재를 통해 나관중도 간과했던 새로운 사실을 밝혀내고, 시대상황을 소상하게 설명하고 있다.");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','table-row-group');
    	break;
    case 5: //'TEST':
    	myVideo.src = "/resources/cvMov/2016-11-02_2.mp4";
    	myVideo.poster = "/resources/image/2016-11-02_2.mp4_000010407.jpg";
    	$(".webmovie-title").html("${title}");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_3.jpg");
    	$(".artist-name").html("");
        $(".genre-name").html("");
        $(".summary").html("");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','table-row-group');
    	break;

    default:
        alert("Preparing for the Webtoons movie.");
	}
}



$('.rank-type').find('span').eq(0).click(function(){
	var data = {};

	$.ajax({
		url : "/order/lastest",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>');
			 */$.each(data, function(idx, val) {
				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
})
$('.rank-type').find('span').eq(1).click(function(){
	var data = {};

	$.ajax({
		url : "/order/commentOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>');
			 */$.each(data, function(idx, val) {
				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
})
$('.rank-type').find('span').eq(2).click(function(){
	var data = {};

	$.ajax({
		url : "/order/countOrder",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			/* $('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=1" style="text-decoration:none; color:#000;"><b>1</b> Memories of one-sided love </a></span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=2" style="text-decoration:none; color:#000;"><b>2</b> Horizon </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=3" style="text-decoration:none; color:#000;"><b>3</b> The Three Kingdoms </span>');
			$('.rank-cont').append('<span class="cp"><a href="/webmovie_en?id=4" style="text-decoration:none; color:#000;"><b>4</b> My Sassy Girl </span>');
			 */$.each(data, function(idx, val) {
				
				$('.rank-cont').append('<span class="cp"><a href="/webmovie2_en/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
})
</script>



</body>
</html>