﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<link rel="stylesheet" href="/resources/common/css/comment.css">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/webmovie.js"></script>
<script src="/resources/common/js/commentList.js"></script>

<div
	style="border-bottom: 1px solid #ccc; margin-top: 30px; padding-bottom: 5px; width: 100%; float: left;">
	<h4 style="float: left; width: 15%;">댓글 목록</h4>
	<div class="countComment"
		style="margin-top: 12px; width: 10%; float: right;">
		댓글 : <span><c:out value="${countComment }"></c:out></span>
	</div>
</div>

<div class="commentList" style="width: 100%;">
	<textarea rows="1" cols="80"></textarea>

</div>
