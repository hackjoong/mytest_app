﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>webmovie_show_2_kr</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<!-- add by yoo_dh0416 2018-02-07 -->
<link rel="stylesheet" href="/resources/common/css/commentList.css">
<script src="/resources/common/js/comment.js"></script>
<!-- add by yoo_dh0416, video keypress control js -->
<script src="/resources/common/js/playvideo.js"></script>
<link rel="stylesheet" href="/resources/common/jquery/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>
	

function resize(obj) {
			obj.style.height = "1px";
			obj.style.height = (1+obj.scrollHeight)+"px";
}

</script>
<c:set value="${sessionScope.currentUser.getId() }" var="userName"/>
<script>

$(function(){
	//user check
var user = '<c:out value="${userName}"/>';
var os = $('#id').val();
	if(user==null||user==''){
		$('.comment_pw').show();
		$('[name=pwd]').show();
		
	}
	else if(user == os){
		$('.comment_pw').hide();
		$('[name=pwd]').hide();

	}
	//user check end
})

</script>
<script>
$(document).ready(function(){
	$('.rank-type').find('span').eq(0).click(function(){
		var data ={};
		$.ajax({
			url:'/order/lastest',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type').find('span').eq(1).click(function(){
		var data ={};
		$.ajax({
			url:'/order/commentOrder',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type').find('span').eq(2).click(function(){
		var data ={};
		$.ajax({
			url:'/order/countOrder',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});
});
</script>
<style>
.next_menu_prev{
	border: 0;
	background-color: #fff;
}
</style>
<body>
	<c:import url="/WEB-INF/views/header&footer/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-8 col-xs-12 pd-0">
						<video id="myVideo" controls  controlsList="nodownload" src="${toonInfor.getFile() }" class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;" poster=""> Your user agent does not support the HTML5 Video element.</video>
						<!--video id="myVideo" controls src="/resources/cvMov/2017-04-24_1.mp4" class="col-md-12 col-xs-12 pd-0 h-auto" height="438.75px;" poster="/resources/image/2017-04-24_1.mp4_000004590.jpg" onclick="/*playPause();*/">
						Your user agent does not support the HTML5 Video element. 
						</video-->
					</div>

					<div class="col-md-4 col-xs-12 pd-0 m_plr2p" style="background-color:;">
						<div class="info m_info">
							<span style="float: right; margin-top:20px; margin-right:5px;">조회 : ${toonInfor.getViewCount()+1 }</span>
							<h2 class="title webmovie-title" style="">${toonInfor.getTitle() }</h2>
							
							<p class="artist">
								<c:set value="${profile }" var="profiles" />
								<c:choose>
									<c:when test="${profiles != null }">
										<%-- <img class="thumbnail_new" src="${profiles }"
											width="100px"> --%>
									</c:when>
									<c:otherwise>
										<img class="thumbnail_new" src="/resources/profile/default/profile.jpg"
											width="100px">
									</c:otherwise>
								</c:choose>
								작가 : <span class="artist-name">${toonInfor.getWriter() }</span><br/>
								등록일 : <span class="artist-name"><fmt:formatDate type ="date" value="${toonInfor.getWridate()}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
									
							</p>
							<div class="info-btns" style="display: none;">
								<div class="info-btn-wrap">
									<button id="info-btn-subscription"
										class="info-btn-subscription btn-subscription"
										data-subscription="false">
										<span class="info-text-false"><i class="icon"></i><span>구독하기</span></span><span
											class="info-text-true"><i class="icon"></i><span>구독중</span></span><span
											class="info-text-truehover"><i class="icon"></i><span>구독취소</span></span>
									</button>
									<div class="coach-tooltip is-hide">
										<span>최신화 놓치기 싫다면<br>지금 구독하세요.
										</span>
										<button class="coach-tooltip-close">
											<i class="icon"></i><span class="a11y">닫기</span>
										</button>
									</div>
								</div>
								<a class="btn-first-episode" href="#" style="display: inline;">처음부터</a>
							</div>
							<div class="genre_new">
								장르 : <span class="genre-name">${toonInfor.getGenre() }</span>
							</div>
							<div class="publisher"></div>
							<div class="badge-list">HD</div>
							<div class="summary m_summary">${toonInfor.getContent() }</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12 m_plr2p">
					<!-- <h2 class="pdl-15 m_p0">웹툰무비 보기</h2>
					<table class="lst_view">
						<tbody id="webmovie_1">
							<tr data-ep_no="001" data-p="0" data-rp="0" class="ep_list_tr"
								style="box-sizing: border-box; background-color: white;"
								onclick="webmovie_show(1);">
								<td class="w15 none m_num">
									<div class="num">01</div>
									<div class="date updated_1">2017-04-24</div>
								</td>
								<td class="img m_img">
									<a class="tmb" href="javascript:;">
										<img src="/resources/image/poster_mini.jpg" alt="" class="thumbnail_1">
									</a>
								</td>
								<td class="tit m_tit">
									<div class="">1화</div>
									<div class="sub_title sub_title_1">에피소드 1</div>
								</td>
								<td class="coin m_coin">
									<div class="free_bt">보기</div>
								</td>
							</tr>
						</tbody>
					</table> -->
					<c:if test="${wmno != 0 }">
					<table class="lst_view">
						<tbody id="webmovie_1">
							<tr>
								<td class="w15" style="border: 0;">
									<c:choose>
										<c:when test="${prevView.tno !=null }">
											<button class="next_menu_prev" onclick="location.href='/webmovie2/${prevView.tno}'">
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/leftBtn.png'>
												<span>이전화</span>
											</button>
										</c:when>
										<c:otherwise>
											<button disabled="disabled" class="next_menu_prev" style="color:#ccc;">
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/disleftBtn.png'>
												<span>이전화</span>
											</button>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="w15" style="border: 0;">
									<button class="next_menu_prev" onclick="location.href='/series/movie/${wmno}'">
										<img class="re_wmno_img" src='/resources/image/icon/webmovieBtn/menu.png'>
									</button>
								</td>
								<td class="w15" style="border: 0;">
									<c:choose>
										<c:when test="${nextView.tno !=null }">
											
											<button class="next_menu_prev" onclick="location.href='/webmovie2/${nextView.tno}'">
												<span>다음화</span>
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/rightBtn.png'>
											</button>
										</c:when>
										<c:otherwise>
											
											<button disabled="disabled" class="next_menu_prev" style="color:#ccc;">
												<span>다음화</span>
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/disrightBtn.png'>
											</button>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</tbody>
					</table>
					</c:if>
					<!--  댓글 start-->
					<div id="dialog" style="display:none;">
					<h5>회원의 경우 회원 비밀번호를 입력하세요.<br/><span style="color:red; line-height:30px;">※대 소문자 구분.</span></h5>
					<form>
						<input type="password" placeholder="비밀번호" class="form-control" style="width:70%;height:32px; font-size:14px;float: left;" required="required">
						<button type="button"class="btn pwsubmit" style="font-size:14px;float: right;">작성</button>
					</form>
					</div>
					<input type="hidden" name="idnick" value="${currentUser.getId() }">
					<input type="hidden" name="pwnick" value="${currentUser.getPw() }">
					<form name="form1">
						<div class="panel-body re_panel-body">
							<!-- 원본  -->
							<%-- <div class="clearfix">
								<div class="pull-left">
									<h3>댓글<small class="text-12 text-red"> (<span id="total_cnt"><c:out value="${countComment }"></c:out></span>)	</small></h3>
									
						
								</div>
								<div class="pull-right">
									<small class="text-muted"> <span id="comment_char_cnt">0</span>/200</small>
								</div>
							</div> --%>
							<!-- //원본  -->
							<!-- 수정본  -->
							<div class="clearfix">
								<h3 class="fl comment_h3">댓글<small class="text-12 text-red"> (<span id="total_cnt"><c:out value="${countComment }"></c:out></span>)	</small></h3>
								<small class="text-muted re_text-muted"><span id="comment_char_cnt">0</span>/200</small>
							</div>
							<!-- //수정본  -->
							<div class="row row-sm">
								<div class="col-xs-10 comment_wrap" style="margin-bottom:5px;">
									<input type="hidden" name="tno" value="${toonInfor.getTno() }">
									<input type="hidden"class="user_id"value="${currentUser.getId() }">
									<input type="text" name="nickname" id="id" class="comment_id m_comment_id" placeholder="ID" value="${currentUser.getId() }" autocomplete=off style="width:17%;">
									<input type="password" name="pw" id="comment_pwd" class="comment_pw m_comment_pw" placeholder="Password" value="${currentUser.getPw() }" autocomplete=off>
								</div>
								<div class="col-xs-10 m_comment">						
									<textarea name="comment" id="comment"
										class="form-control m_comment_box nofocused"
										placeholder="주제와 무관한 댓글이나 스포일러, 악플은 경고 조치 없이 삭제될 수 있습니다."
										onkeydown="if($(this).val().length > 200) { alert('200자 이내의 내용만 등록 가능합니다.'); return false; } else { $('#comment_char_cnt').html($(this).val().length); }"
										onkeyup="if($(this).val().length > 200) { alert('200자 이내의 내용만 등록 가능합니다.'); return false; } else { $('#comment_char_cnt').html($(this).val().length);  }"
										maxlength="200"></textarea>
								</div>
								<div class="col-xs-2 m_comment_ok">
									<button type="button" class="btn btn-block btn-dgray btn-comment-movie m_comment_ok_box" id="btn-comment-movie">등록</button>
								</div>
							</div>
						</div>
					</form>
					<ul class="comments-list" id="comment_list">
					</ul>
					<!-- 댓글 end -->

					<ul class="genre-menu col-md-12">
					</ul>

					<div class="w100 fl genre-cont" style="display: none;">
						<div class="episode gen">
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(1);"
										style="background-image: url('/resources/image/one_sided_love.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">드라마</span><br> <span
											class="genre-cont-title">짝사랑의 추억</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(2);"
										style="background-image: url('/resources/image/horizon.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">스릴러</span><br> <span
											class="genre-cont-title">수평선</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(3);"
										style="background-image: url('/resources/image/three_kingdoms.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">액션</span><br> <span
											class="genre-cont-title">삼국지</span><br>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img" onclick="webmovie_show(4);"
										style="background-image: url('/resources/image/엽기적인 그녀.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">드라마</span><br> <span
											class="genre-cont-title">엽기적인 그녀</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="w100 fl conts">
									<div class="genre-cont-img"
										style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목</span><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w100 fl best-toon" style="display: none;">
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="w100 fl conts">
								<div class="genre-cont-img"
									style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br> <span
										class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<div class="add-toon tc" onclick="location.href='/cv/effect'"
						style="display: none;">
						<img src="/resources/image/upload icon.png" class="upload-con">
						<span>만화 올리기</span>
					</div>
					<div class="w100 fl rank-title">
						<!--div class="rank-head">
								실시간 웹툰 순위
							</div-->
						<h2>웹툰무비 순위</h2>
						<div class="rank-type" style="">							
							<span>&nbsp;&#124;&nbsp;최신순</span>
							<span>&nbsp;&#124;&nbsp;댓글순</span>
							<span>조회순</span>
						</div>
						<div class="rank-cont">
							<!-- <span class="cp"
								onclick="webmovie_show(1);"><b>1</b> 짝사랑의 추억</span> <span
								class="cp"  onclick="webmovie_show(2);"><b>2</b>
								수평선</span> <span class="cp" 
								onclick="webmovie_show(3);"><b>3</b> 삼국지</span> <span class="cp"
								 onclick="webmovie_show(4);"><b>4</b>
								엽기적인 그녀</span> -->
							<!-- <span class="cp"   onclick="webmovie_show(5);"><b>5</b> test</span> -->
							<c:set var="i" value="1" />
							<c:forEach items="${toonsunwi}" var="toonivie">
								<span class="cp" id='toon${toonivie.getTno()}'
									
									onclick="location.href='/webmovie2/${toonivie.getTno()}'"
									data-file='${toonivie.getFile()}'
									data-title='${toonivie.getTitle()}'
									data-genre='${toonivie.getGenre()}'
									data-writer='${toonivie.getWriter()}'
									data-content='${toonivie.getContent()}'
									data-date='${toonivie.getWridate()}'><b>${i}</b>
									${toonivie.getTitle()} </span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>
						</div>
					</div>
					<!--div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>								
							</li>
						</div-->
				</div>
				
				
				<!-- 			<span>웹툰 정보리스트</span>
	    <div id="listReply" style="width:100%; height:200px; overflow: auto; overflow: x-hidden; margin-top: 5px;">
	    <table class="table1 board-title" style="width:95.8%; margin-top: 10px; float: left; border:1px solid #000">
                 <tr style="border:1px solid #000; text-align: center">
                    	<td>파일</td>
                    	<td>장르</td>
                    	<td>제목</td>
                    	<td>작가</td>
                    	<td>내용</td>
                    	<td>등록 날짜</td>
                	</tr>     
                	           
        			<c:forEach items="${toon}" var="toonivie"> 
				       	<tr style="cursor: pointer; text-align: center">
				       		<td><span style="font-size:15px">${toonivie.getFile()}</span>
				           	<td><span style="font-size:15px">${toonivie.getGenre()}</span>
				           	<td><span style="font-size:15px">${toonivie.getTitle()}</span>
				           	<td><span style="font-size:15px">${toonivie.getWriter()}</span>
				           	<td><span style="font-size:15px">${toonivie.getContent()}</span>
				           	<td><span style="font-size:15px"><fmt:formatDate type ="date" value="${toonivie.getWridate()}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
				           	</td>
				       	</tr>
				    </c:forEach> 
            </table>-->

			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>

</body>
</html>