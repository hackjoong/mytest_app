<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="/resources/common/css/slidemyheader.css">
<!-- add by ydh 180309 - header mybutton -->
<style>
input::-webkit-input-placeholder {
	color: #ccc;
}
</style>
<script>
	$(function() {
		var search = document.getElementById("search_input");
		var userInfo = document.getElementById("userInfo");
		var s = location.pathname;
		if (s == "/webtoon_wonjak" || s == "/webtoon_wonjak/searchItem") {
			search.style.display = "";
		} else {
			search.style.display = "none";
		}
	});
	$(function() {
		$('.language>a').click(function() {
			swal("서비스 준비중입니다.");
			return false;
		})
	})

	function myFunction() {
		$('#header-search').submit();
	}
	function listarticle(pageNum) {
		$("#pageNum").val(pageNum);
		$('#header-search').submit();
	}
</script>
<header class="pt30 m_pt10">
	<div class="container pd-0">
		<div class="language dn"
			style="float: right; margin-bottom: 10px; margin-right: 15px;">
			<a href="/index_ch"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/ChinaIcon.png">中国語</a>&nbsp;&nbsp; <a
				href="/index_jp"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/JapanIcon.png">日本語</a>&nbsp;&nbsp; <a
				href="/index_en"
				style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/USAIcon.png">English</a>&nbsp;&nbsp; <a
				href="/" style="font-size: 10px; font-weight: bold; color: #777;"><img
				src="/resources/image/icon/KoreaIcon.png">한국어</a>
		</div>
		<div class="col-md-12 pd-0 header-pd">
			<div class="col-md-8 col-sm-6 col-xs-5 pd-0 header-logo">
				<img src="/resources/image/logo.png" alt="looking" class="logo"
					onclick="location.href='/'">
			</div>

			<div class="col-md-4 col-sm-6 col-xs-7 pd-0 header-search-login">
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_USER' }">
					<font class="userInfo" id="userInfo"
						style="float: right; position: absolute; right: 12%; margin: 10px 0; font-size: 15px;"><span
						class="nullcheck">${sessionScope.currentUser.getName()}</span>님
						환영합니다. </font>

				</c:if>
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_ADMIN' }">
					<font class="userInfo" id="userInfo"
						style="float: right; position: absolute; right: 12%; margin: 10px 0; font-size: 15px;"><span
						class="nullcheck">${sessionScope.currentUser.getName()}</span>
						관리자님 환영합니다. </font>

				</c:if>
				<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_WRITER' }">
					<font class="userInfo" id="userInfo"
						style="float: right; position: absolute; right: 12%; margin: 10px 0; font-size: 15px;"><span
						class="nullcheck">${sessionScope.currentUser.getName()}</span> 작가님
						환영합니다. </font>

				</c:if>
				<c:if
					test="${ sessionScope.currentUser.getRole()=='ROLE_COWORKER' }">
					<font class="userInfo" id="userInfo"
						style="float: right; position: absolute; right: 12%; margin: 10px 0; font-size: 15px;"><span
						class="nullcheck">${sessionScope.currentUser.getName()}</span>
						매니저님 환영합니다. </font>

				</c:if>
				<div class="w100 fl search-header">

					<div class="search" id="search_input">
						<form id="header-search" action="/webtoon_wonjak/searchItem">
							<input type="hidden" id="pageNum" name="pageNum" value="" />
							<c:choose>
								<c:when test="${search_data!=null }">
									<input type="text" id="searchValue" name="searchValue"
										class="search_text" placeholder="제목, 작가"
										value="${search_data }" style="font-size: 16px;">
								</c:when>
								<c:otherwise>
									<input type="text" id="searchValue" name="searchValue"
										class="search_text" placeholder="제목, 작가"
										style="font-size: 16px;">
								</c:otherwise>
							</c:choose>

							<!-- <img src="/resources/image/magnifier icon.png" alt="search"
								onclick="myFunction()"> -->
						</form>
					</div>
					<c:if test="${sessionScope.currentUser != null }">

						<!-- <button class="btn-login" style="float: right; margin-right:12%;">로그아웃</button> -->
						<ul>
							<li class="btn-login btn-my cp"
								style="border-radius: 0; margin-right: 10%;"><span>MY</span>
								<ul class="myul" style="z-index: 5;">
									<li class="cp MYLIST" onclick="location.href='/members/myPage'">내
										정보</li>
									<li class="cp MYLIST" onclick="location.href='/myBooks'">내
										서재</li>
									<li class="cp MYLIST"
										onclick="location.href='/logoutProcess.do'">로그아웃</li>
									<c:if
										test="${sessionScope.currentUser.getRole()=='ROLE_ADMIN'}">
										<li class="cp MYLIST" onclick="location.href='/adminPage'">설정관리</li>
									</c:if>
								</ul></li>
						</ul>
						<script>
							$(function() {
								$('.btn-my').mouseover(function() {
									$(this).find("ul").stop().slideDown();
								})
								$('.btn-my').mouseout(function() {
									$(this).find("ul").stop().slideUp();
								})
								$('.MYLIST').mouseover(function() {
									$(this).stop().css({
										"background-color" : "#bbb",
										"color" : "#fff"
									});

								})
								$('.MYLIST').mouseout(function() {
									$(this).stop().css({
										"background-color" : "#fff",
										"color" : "#767676"
									});
								})
							})
						</script>
					</c:if>
					<c:if test="${sessionScope.currentUser == null }">
						<button class="btn-login" style="float: right; margin-right: 12%;"
							onclick="location.href='/login'">로그인</button>
					</c:if>
				</div>
			</div>
			<div class="w100 fl today-header mt20">
				<ul>
					<!-- <li>TOONIVIE</li>
					<li>신작</li>
					<li>장르</li>
					<li>랭킹</li>
					<li class="cp" onclick="location.href='/'">웹툰무비</li> -->
					<li class="cp" onclick="location.href='/menu/writer'">작가</li>
					<li>완결</li>
					<li>연재</li>
					<li class="cp" onclick="location.href='/webtoon_wonjak'">원작</li>
					<li class="cp" onclick="location.href='/movietoon'">웹툰무비</li>
				</ul>
			</div>
		</div>
	</div>
</header>