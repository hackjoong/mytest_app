<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>webmovie_show_2_kr</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/resources/common/js/webtooncomment.js"></script>
<!-- Custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" href="/resources/common/css/webmovie.css">
<!-- add by shkwak, 2017-06-07 -->

<link rel="stylesheet" href="/resources/common/css/commentList.css">
<!-- add by yoo_dh0416 2017-10-12 -->

<!-- <script src="/resources/common/js/webmovie.js"></script> -->
<!-- add by shkwak, 2017-06-07 -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/resources/common/js/jquery-ui.js"></script>
<c:set value="${sessionScope.currentUser.getId() }" var="userName"/>
<script>
function resize(obj) {
	obj.style.height = "1px";
	obj.style.height = (1+obj.scrollHeight)+"px";
}

//유저 체크
$(function(){
var user = '<c:out value="${userName}"/>';
var os = $('#id').val();
	if(user==null||user==''){
		$('.comment_pw').show();
		$('.dap_pw').show();
		$('.col-xs-2>button').addClass('btn-comment').removeClass('btn-comment-user');
		

	}
	else if(user == os){
		$('.comment_pw').hide();
		$('.col-xs-2>button').removeClass('btn-comment').addClass('btn-comment-user');
	}
});

$(document).ready(function(){
	$('.btn-dgray').css('outline','none');
	$('.rank-type').find('span').eq(0).click(function(){
		var data ={};
		console.log("asd");
		$.ajax({
			url:'/order/lastest_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});

	$('.rank-type').find('span').eq(1).click(function(){
		var data ={};
		$.ajax({
			url:'/order/commentOrder_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type').find('span').eq(2).click(function(){
		var data ={};
		$.ajax({
			url:'/order/countOrder_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				/* $('.rank-cont').append('<span class="cp"><a href="/webmovie?id=1" style="text-decoration:none; color:#000;"><b>1</b> 짝사랑의 추억  </a></span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=2" style="text-decoration:none; color:#000;"><b>2</b> 수평선   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=3" style="text-decoration:none; color:#000;"><b>3</b> 삼국지   </span>');
				$('.rank-cont').append('<span class="cp"><a href="/webmovie?id=4" style="text-decoration:none; color:#000;"><b>4</b> 엽기적인 그녀   </span>'); */
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});	 
				console.log(data);
				$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});
});
$(document).ready(function(){	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 300) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},500);
		return false;
	});	
});
</script> 
<style>
.next_menu_prev{border: 0;	background-color: #fff;}

.scrollToTop{width:60px; height:60px; padding-top:15px; text-align:center; background: #444; font-weight: bold; color: #fff; text-decoration: none; position:fixed; top:82%; float:right; display:none; right:20%; border-radius: 100%; cursor: pointer; z-index: 9999;}
.scrollToTop:hover{text-decoration:none; color:#fff;}
.scrollToBot{width:60px; height:60px; padding-top:15px; text-align:center; background: #444; font-weight: bold; color: #fff; text-decoration: none; position:fixed; top:90%; float:right; display:none; right:20%; border-radius: 100%; cursor: pointer; z-index: 9999;}
.scrollToBot:hover{text-decoration:none; color:#fff;}
</style>
<body>
	<c:import url="/WEB-INF/views/wonjak/header.jsp" />
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
				<div class="scrollToTop">Top</div>
				<!-- <div class="scrollToBot">Bot</div> -->
					<div class="col-md-8 col-xs-12 pd-0 block">
							<span class="path" style="display:none;">${selToon.getPath() }</span>
							<script>
							var foo = new Array();
							var s = $(".path").text();
							var foo = s.split(',');
							for(i = 0; i<foo.length; i++){
								$(".block").append("<img src='/resources/webtoon/"+foo[i]+"' style='width:100%;' >");
								
							}
							</script>
					
					</div>
					<div class="col-md-4 col-xs-12 pd-0 m_plr2p">
						<div class="info m_info">
							<span style="float: right; margin-top:20px; margin-right:5px;">조회 : ${selToon.getWtviewCount()+1 }</span>
							<h2 class="title webmovie-title" style="">${selToon.getWtname() }</h2>
							
							<p class="artist">
							<c:set value="${profile }" var="profiles" />
							<c:choose>
								<c:when test="${profiles != null }">
									<img class="thumbnail_new" src="${profiles }"
										width="100px">
								</c:when>
								<c:otherwise>
									<img class="thumbnail_new" src="/resources/profile/default/profile.jpg"
										width="100px">
								</c:otherwise>
							</c:choose>
									 작가 : <span class="artist-name">${selToon.getWtwriter() }</span><br/>
									등록일 : <span class="artist-name"><fmt:formatDate type ="date" value="${selToon.getWtwridate() }" pattern="yyyy-MM-dd HH:mm:ss" /></span>
									
							</p>
							<div class="genre_new">
								장르 : <span class="genre-name">${selToon.getWtgenre() }</span>
							</div>
							<div class="publisher"></div>
							<div class="badge-list">HD</div>
							<div class="summary" style="height:auto;padding: 15px 0; overflow: auto;">${selToon.getWtcontent() }</div>
						</div>
					</div>
					
					<div class="col-md-4 rank bn991">
						<div class="add-toon tc" onclick="location.href='/cv/effect'"
							style="display: none;">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>만화 올리기</span>
						</div>
						<div class="w100 fl rank-title">
							<h2>원작 순위 보기</h2>
							<div class="rank-type">							
								<span class="cp">&nbsp;&#124;&nbsp;최신순</span>
								<span class="cp">&nbsp;&#124;&nbsp;댓글순</span>
								<span class="cp">조회순</span>
							</div>
							<div class="rank-cont">
								<c:set var="i" value="1" />
								<c:forEach items="${wonjaksunwi}" var="wonjakAll">
									<span class="cp" id='toon${wonjakAll.getWtno()}'
										
										onclick="location.href='/webtoon/${wonjakAll.getWtno()}'"
										data-file='${wonjakAll.getPath()}'
										data-title='${wonjakAll.getWtname()}'
										data-genre='${wonjakAll.getWtgenre()}'
										data-writer='${wonjakAll.getWtwriter()}'
										data-content='${wonjakAll.getWtcontent()}'><b>${i}</b>
										${wonjakAll.getWtname()} </span>
									<c:set var="i" value="${i + 1}" />
								</c:forEach>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12 m_plr2p">
					<!--  댓글 start-->
					<div id="dialog" style="display:none;">
					<h5>회원의 경우 회원 비밀번호를 입력하세요.<br/><span style="color:red; line-height:30px;">※대 소문자 구분.</span></h5>
					<form>
						<input type="password" placeholder="비밀번호" class="form-control" style="width:70%;height:32px; font-size:14px;float: left;" required="required">
						<button type="button"class="btn pwsubmit" style="font-size:14px;float: right;">작성</button>
					</form>
					</div>
					
					
					
					
					
					
					
					
					<c:if test="${wmno != 0 }">
					<table class="lst_view">
						<tbody id="webmovie_1">
							<tr>
								<td class="w15" style="border: 0;">
									<c:choose>
										<c:when test="${prevView.wtno !=null }">
											<button class="next_menu_prev" onclick="location.href='/webtoon/${prevView.wtno}'">
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/leftBtn.png'>
												<span>이전화</span>
											</button>
										</c:when>
										<c:otherwise>
											<button disabled="disabled" class="next_menu_prev" style="color:#ccc;">
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/disleftBtn.png'>
												<span>이전화</span>
											</button>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="w15" style="border: 0;">
									<button class="next_menu_prev" onclick="location.href='/series/toon/${wmno}'">
										<img class="re_wmno_img" src='/resources/image/icon/webmovieBtn/menu.png'>
									</button>
								</td>
								<td class="w15" style="border: 0;">
									<c:choose>
										<c:when test="${nextView.wtno !=null }">
											<button class="next_menu_prev" onclick="location.href='/webtoon/${nextView.wtno}'">
												<span>다음화</span>
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/rightBtn.png'>
											</button>
										</c:when>
										<c:otherwise>
											<button disabled="disabled" class="next_menu_prev" style="color:#ccc;">
												<span>다음화</span>
												<img class="re_wprev_img" src='/resources/image/icon/webmovieBtn/disrightBtn.png'>
											</button>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</tbody>
					</table>
					</c:if>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<input type="hidden" name="idnick" value="${currentUser.getId() }">
					<input type="hidden" name="pwnick" value="${currentUser.getPw() }">
					<form name="form1">
						<div class="panel-body re_panel-body re_webtoon_comment">
							<!-- 원본 -->
							<%-- <div class="clearfix">
								<div class="pull-left">
									<h3>댓글<small class="text-12 text-red"> (<span id="total_cnt"><c:out value="${countComment }"></c:out></span>)	</small></h3>
						
								</div>
								<div class="pull-right">
									<small class="text-muted"> <span id="comment_char_cnt">0</span>/200
									</small>
								</div>
							</div> --%>
							<!-- //원본 -->
							<!-- 수정본 -->
							<div class="clearfix">
								<h3 class="fl comment_h3">댓글<small class="text-12 text-red"> (<span id="total_cnt"><c:out value="${countComment }"></c:out></span>)	</small></h3>
								<small class="text-muted re_text-muted"> <span id="comment_char_cnt">0</span>/200</small>
							</div>
							<!-- //수정본 -->
							<div class="row row-sm">
								<div class="col-xs-10" style="margin-bottom:5px;">
									<input type="hidden" name="wtno" value="${selToon.getWtno() }">
									<input type="hidden"class="user_id" value="${currentUser.getId() }">
									<input type="text" name="nickname" id="id" class="comment_id m_comment_id" placeholder="ID" value="${currentUser.getId() }" autocomplete=off>
									<input type="password" name="pw" id="pwd" class="comment_pw m_comment_pw" placeholder="Password" value="${currentUser.getPw() }" autocomplete=off>
								</div>
								<div class="col-xs-10 m_comment">
									<textarea name="comment"
										class="form-control m_comment_box"
										placeholder="주제와 무관한 댓글이나 스포일러, 악플은 경고 조치 없이 삭제될 수 있습니다."
										onkeydown="if($(this).val().length > 200) { alert('200자 이내의 내용만 등록 가능합니다.'); return false; } else { $('#comment_char_cnt').html($(this).val().length); }"
										onkeyup="if($(this).val().length > 200) { alert('200자 이내의 내용만 등록 가능합니다.'); return false; } else { $('#comment_char_cnt').html($(this).val().length);  }"
										maxlength="200"></textarea>
								</div>
								<div class="col-xs-2 m_comment_ok">
									<button type="button" class="btn btn-block btn-dgray btn-comment m_comment_ok_box" id="btn-comment-wonjak">등록</button>
								</div>
							</div>
						</div>
					</form>
					<ul class="comments-list re_webtoon_comment_bot" id="comment_list">
					</ul>
					<!-- 댓글 end -->

					<ul class="genre-menu col-md-12">
					</ul>
					<!-- 순위 보기 -->
					<div class="col-md-3 rank col-xs-12 re_webtoon_list m_WTL">
						<div class="add-toon tc" onclick="location.href='/cv/effect'"
							style="display: none;">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>만화 올리기</span>
						</div>
						<div class="w100 fl rank-title">
							<h2>원작 순위 보기</h2>
							<div class="rank-type">							
								<span>&nbsp;&#124;&nbsp;최신순</span>
								<span>&nbsp;&#124;&nbsp;댓글순</span>
								<span>조회순</span>
							</div>
							<div class="rank-cont">
								<c:set var="i" value="1" />
								<c:forEach items="${wonjaksunwi}" var="wonjakAll">
									<span class="cp" id='toon${wonjakAll.getWtno()}'
										
										onclick="location.href='/webtoon/${wonjakAll.getWtno()}'"
										data-file='${wonjakAll.getPath()}'
										data-title='${wonjakAll.getWtname()}'
										data-genre='${wonjakAll.getWtgenre()}'
										data-writer='${wonjakAll.getWtwriter()}'
										data-content='${wonjakAll.getWtcontent()}'><b>${i}</b>
										${wonjakAll.getWtname()} </span>
									<c:set var="i" value="${i + 1}" />
								</c:forEach>
							</div>
						</div>
					</div>
					<!-- 순위 보기 end -->
				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>
</body>
</html>