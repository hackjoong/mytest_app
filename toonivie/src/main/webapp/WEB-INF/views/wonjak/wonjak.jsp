<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="_csrf_parameter" />
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<link rel="stylesheet" type="text/css" href="/resources/common/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/resources/common/css/component.css" />
<link rel="stylesheet" href="/resources/common/jquery/jquery-ui.css">
<!-- js -->
<script src="/resources/common/js/form.js"></script>
<script src="/resources/common/js/custom-file-input.js"></script>
<script src="/resources/common/js/jquery-ui.js"></script>
<style type="text/css">
.table-striped-wonjak{
	display: block;
	height:auto;
	overflow: auto;
}

#wtoonwrap {
	width: 90%;
	margin-top: 60px;
	margin-left: 30px;
	height: 800px;
}
.choice3 {
    width: 80%;
    padding:15px 10px;
    border-radius: 50px;
    background: none;
    color: #777;
    font-size: 12px;
    border: 1px solid #ccc;
    line-height: 0;
    outline: none;
}
.wonjak_dialog_btn {background: #ff8c24;}
@media (max-width:500px){
	.m_dialog {width: 96% !important;}
	.re_admin_thumbnail1 {width: 29%; text-align: center;}
}

</style>
<c:set value="${countwonjak}" var="Allwonjak"/>
<div id="dialog" style="display:none;">
		<form id="modify_webtoon_info" enctype="multipart/form-data" method="POST" >
		<span class="wtno dn"></span>
		<div class="form-group" style="width: 100%; float: left; clear: both; font-size:15px;">
			<label class="fl" for="title">썸네일</label>
			<div class="filebox" style="clear:both;">
				<input class="upload-name" placeholder="파일선택" readonly="readonly" style="width:85.5%;float: left;">
				<label class="re_admin_thumbnail1" for="thumbnail1" style="padding:8.5px; margin: 0; float: right;">업로드</label>
				<input type="file" id="thumbnail1" name="thumbnail" class='kinp upload-inp dn'>
			</div> 
		</div>
		<div class="form-group" style="width: 49%; float: left; clear: both;">
			<label class="fl" for="title">제목</label> <input type="text"
				class="form-control uploadToon-input" id="modtitle" name="title"
				placeholder="title" required="required">
		</div>
		<div class="form-group" style="width: 49%; float: right;">
			<label class="fl" for="writer">작가</label> <input type="text"
				class="form-control" id="modwriter" name="writer" placeholder="writer">
		</div>
		<div class="form-group" style="width: 49%; float: left;">
			<label class="fl" for="genre">장르</label> <input type="text"
				class="form-control uploadToon-input" id="modgenre" name="genre"
				placeholder="genre" required="required">
		</div>
		<div class="form-group" style="width: 49%; float: right;">
			<label class="fl" for="genre" style="width:100%;"><span class="fl" style="margin-top: 0;">시리즈 작품</span></label>
				<select style="background: #fff;" name="modserieswonjak" class="modserieswonjak re_admin_modserieswonjak">
				<option value="0" selected>없음</option>
				<c:forEach items="${mySeriesInfowonjak }" var="mywonjakSeries">
					<option value="${mywonjakSeries.getWmno() }" >${mywonjakSeries.getWmtitle() }</option>
				
				</c:forEach>
			</select>
		</div>
		<div class="form-group">
			<label class="fl" for="content">내용</label>
			<textarea style="height: 100px;"
				class="form-control uploadToon6 uploadToon-input content_area"
				id="modcontent" name="content" placeholder="content"
				required="required" maxlength="500"></textarea>
		</div>
		<button style="background: #ff8c24; border:0; color:#fff; font-size: 15px;" type="button" class="btn btn-default tooninfo-submit"
			onclick="fileModify()">수정</button>
			<input type="checkbox" class="check_free" name="check_free" value="true" />공개 여부
	</form>
</div>
<h2 class="re_admin_h2">작품 관리 - 원작 웹툰</h2>					
<div id="wtoonwrap" class="re_admin_wtoonwrap">
	<div class="fl re_admin_wonjakbox">
		<div class="fl" style="width:100%;">
			<span class="fl">원작의 총 개수는 <c:out value="${Allwonjak}"/>개 입니다</span>
			<span class="fr" style="margin-top:9px; font-size:14px; color:#d3394c">※해당 웹툰을 클릭하면 수정할 수 있습니다.</span>
		</div>
		<table class="table table-striped table-striped-wonjak " style="width: 98.5%; font-size:14px;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="14%">제목</th>
					<th scope="col" width="14%">장르</th>
					<th scope="col" width="14%">작성자</th>
					<th scope="col" width="20%" class="re_admin_wtcontent">내용</th>
					<th scope="col" width="10%" class="re_admin_wtcontent">공개 여부</th>
					<th scope="col" width="11%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${wonjak }">
					<tr class="cp">
						<td scope="row" class="wmno_${item.getWtno() } dn">${item.getWmno() }</td>
						<td scope="row" class="wtthumb_${item.getWtno() } dn" onclick="wtload(${item.getWtno()})">${item.getThumbnail() }</td>
						<td scope="row" class="wtname_${item.getWtno() }" onclick="wtload(${item.getWtno()})">${item.getWtname() }</td>
						<td scope="row" class="wtgenre_${item.getWtno() }" onclick="wtload(${item.getWtno()})">${item.getWtgenre()}</td>
						<td scope="row" class="wtwriter_${item.getWtno() }" onclick="wtload(${item.getWtno()})">${item.getWtwriter() }</td>
						<td scope="row" class="wtcontent_${item.getWtno() } re_admin_wtcontent" onclick="wtload(${item.getWtno()})"><span>${item.getWtcontent() }</span></td>
						<td scope="row" class="free_open_${item.getWtno() }" onclick="wtload(${item.getWtno()})" data-open="${item.getFree_open()}"><span>
						<c:if test="${item.getFree_open() eq true}">
  						 	 O
						</c:if>
						<c:if test="${item.getFree_open() eq false}">
  						 	 X
						</c:if>
						</span></td>
						<td scope="row"><button class="choice3 re_admin_choice3" onclick="wonjakDel(${item.getWtno()})">삭제</button></td>
						<td scope="row" class="dn"><input type="text" name="path_${item.getWtno()}" value="${item.getPath() }"></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<form id="upload_webtoon_info" class="re_admin_webtoon_upload" action="/webtoon_upload2/webtoon_upload"
enctype="multipart/form-data" style="width: 37%; float: right;">
		<div class="form-group imgFile_box">
			<label for="ex_filename" class="fl"><span class="fl">이미지파일</span></label>
			<div class="box" style="clear:both;">
				<input type="file" name="file-2[]" id="fileUp" class="inputfile inputfile-2 uploadToon-input kinp upload-inp king2 upload-hidden" style="display: none;" multiple data-multiple-caption="{count} files selected">
				<label for="fileUp" style="width:100%;max-width:100%"> 
				<svg xmlns="http://www.w3.org/2000/svg"	width="20" height="17" viewBox="0 0 20 17">
						<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
				</svg>
				<span>Choose a file…</span>
				</label>
			</div>
		</div>
		<div class="form-group" style="width: 100%; float: left; clear: both;">
			<label class="fl" for="title">썸네일</label>
			<div class="filebox" style="clear:both;">
						<input class="upload-name re_admin_upload_name" value="파일선택" readonly="readonly">
						<label class="re_admin_mywebmovieFile" for="thumbnail">업로드</label>
						<input type="file" id="thumbnail" name="thumbnail" class='kinp upload-inp dn'>
					</div> 
		</div>
		<div class="form-group" style="width: 49%; float: left; clear: both;">
			<label class="fl" for="title">제목</label> <input type="text"
				class="form-control uploadToon-input" id="title" class="wonjaktitle" name="title"
				placeholder="title" required="required">
		</div>
		<div class="form-group" style="width: 49%; float: right;">
			<label class="fl" for="writer">작가</label> <input type="text"
				class="form-control" id="writer" name="writer" value="${sessionScope.currentUser.getNickname() }" placeholder="writer">
		</div>
		<div class="form-group" style="width: 49%; float: left;">
			<label class="fl" for="genre">장르</label>
			<input type="text"
				class="form-control uploadToon-input" id="genre" name="genre"
				placeholder="genre" required="required">
		</div>
		<div class="form-group" style="width: 49%; float: right;">
			<label class="fl" for="genre">시리즈 작품</label>
				<select class="form-control uploadToon-input" name="serieswonjak">
				<option value="0" selected>없음</option>
				<c:forEach items="${mySeriesInfowonjak }" var="mywonjakSeries">
					<option value="${mywonjakSeries.getWmno() }" >${mywonjakSeries.getWmtitle() }</option>
				</c:forEach>
			</select>
		</div>
		<div class="form-group">
			<label class="fl" for="content">내용</label>
			<textarea style="height: 100px;"
				class="form-control uploadToon6 uploadToon-input content_area"
				id="content" name="content" placeholder="content"
				required="required" maxlength="500"></textarea>
		</div>
		<button type="button" class="btn btn-default tooninfo-submit"
			onclick="fileSubmit3()">작성</button>
			<input type="checkbox" class="check_free" name="check_free" value="true" />공개 여부
	</form>
</div>

<script>
var fileTarget = $('.filebox #thumbnail');

fileTarget.on('change', function() { // 값이 변경되면
	$(this).siblings('.upload-name').val('');
	if (window.FileReader) { // modern browser
		var filename = $(this)[0].files[0].name;
	} else { // old IE
		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																			// 추출
	}

	// 추출한 파일명 삽입
	$(this).siblings('.upload-name').val(filename);
});

//수정 파일업로드
var fileTarget1 = $('.filebox #thumbnail1');

fileTarget1.on('change', function() { // 값이 변경되면
	$(this).siblings('.upload-name').val('');
	if (window.FileReader) { // modern browser
		var filename = $(this)[0].files[0].name;
	} else { // old IE
		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																			// 추출
	}

	// 추출한 파일명 삽입
	$(this).siblings('.upload-name').val(filename);
});

function wtload(wtno){
	$('#dialog').dialog({
		dialogClass: 'm_dialog',
		title : '원본웹툰 정보수정 ',
		modal : true,
		width : '500',
		height : '500', 
		position:{
			my:"center",
			at:"center center"
		},
		open : function(event, ui) {
			$('#dialog #title,#dialog #genre,#dialog #writer,#dialog #content').css('font-size','14px');
			var wtname = $('.wtname_'+wtno).text();
			var wtgenre = $('.wtgenre_'+wtno).text();
			var wtwriter = $('.wtwriter_'+wtno).text();
			var wtcontent = $('.wtcontent_'+wtno).text();
			var wtthumb = $('.wtthumb_'+wtno).text();
			var check_free_result = $('.free_open_'+wtno).data('open');
			if(check_free_result == "false"){
				check_free_result = false;
			}
			$('.check_free').prop('checked', check_free_result);
			$('#dialog #modtitle').val(wtname);
			$('#dialog #modgenre').val(wtgenre);
			$('#dialog #modwriter').val(wtwriter);
			$('#dialog #modcontent').val(wtcontent);
			$('.wtno').text(wtno);
			$('#dialog .upload-name').val(wtthumb.substring(29,wtthumb.length));
			
			//select box check
			var wmno = $('.wmno_'+wtno).text();
			var length = $('.modserieswonjak').children().length;
			for(var i = 0; i<length; i++){
				if($('.modserieswonjak').children().eq(i).val()==wmno){
					$('.modserieswonjak').children().eq(i).prop('selected',true);
					break;
				}
			}
		},
		close:function(event,ui){
			$('#dialog #modtitle').val('');
			$('#dialog #modgenre').val('');
			$('#dialog #modwriter').val('');
			$('#dialog #modcontent').val('');
			$('#dialog .upload-name').val('');
			$('.modserieswonjak').children().eq(0).prop('selected',true);// no selected
		}
	});
}
function fileModify(){
	var s = confirm("수정하시겠습니까?");
	if(s == true){
		var formData = new FormData();
		var title = $('#dialog #modtitle').val();
		var genre = $('#dialog #modgenre').val();
		var writer = $('#dialog #modwriter').val();
		var content = $('#dialog #modcontent').val();
		var wtno = $('.wtno').text();
		var thumbnail = $('#dialog #thumbnail1').val();
		var beforethumb = $('.wtthumb_'+wtno).text();
		var modseries = $('.modserieswonjak').val();
		var check_free = $('.check_free').is(":checked");
		formData.append('thumbnail',$('#dialog #thumbnail1')[0].files[0]);
		formData.append('title',title);
		formData.append('writer',writer);
		formData.append('genre',genre);
		formData.append('content',content);
		formData.append('wtno',wtno);
		formData.append('beforethumb',beforethumb);
		formData.append('modseries',modseries);
		formData.append('check_free',check_free);
		
		console.log(formData);		
		$.ajax({
			url:'/webtoon_upload2/webtoon_update/'+wtno,
			type:'POST',
			data:formData,
			contentType: false,
			processData: false,
			success:function(data){
				$('#dialog').dialog("close");
				if(data==1)
					alert('수정되었습니다.');
					home();
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		})
	}
}


function fileSubmit3() {
	var title = $('#upload_webtoon_info #title').val();
	var writer = $('#upload_webtoon_info #writer').val();
	var genre = $('#upload_webtoon_info #genre').val();
	var contents = $('#upload_webtoon_info #content').val();
	var thumbnail = $('#upload_webtoon_info #thumbnail').val();
	var serieswonjak = $('[name=serieswonjak]').val();
	var check_free = $('.check_free').is(":checked");
	if(thumbnail == ''){
		alert("썸네일을 첨부해주세요.");
	}
	else if(title==''){
		alert("제목을 입력해주세요.");
	}
	else if(writer==''){
		alert("작가를 입력해주세요.");
	}
	else if(genre==''){
		alert("장르를 입력해주세요.");
	}
	else if(contents==''){
		alert("내용을 입력해주세요.");
	}

	else{
		var s = confirm("등록하시겠습니까?");
		if(s== true){
			var formData = new FormData();
			$.each($('#fileUp')[0].files, function(i, file) {
				formData.append('file-' + i, file);
				console.log('file-' + i, file);
			});
			formData.append('thumbnail',$('#upload_webtoon_info #thumbnail')[0].files[0]);
			formData.append('title',title);
			formData.append('writer',writer);
			formData.append('genre',genre);
			formData.append('content',contents);
			formData.append('serieswonjak',serieswonjak);
			formData.append('check_free',check_free);
			$.ajax({
				type : "POST",
				url : "/webtoon_upload2/webtoon_upload?${_csrf.parameterName}=${_csrf.token}",
				dataType : "text",
				data : formData,
				processData : false,
				contentType : false,
				success : function(result) {
					alert("업로드 완료");
					console.log(result); //.resultUrl);
					home();
				},
				error : function(){
					alert("파일이 없거나 빈 값이 있습니다.");
				}
			});
		}
	}	
}

function home(){
	$.ajax({
		type : "GET",
		url : "/wonjak",
		error : function() {
			alert('페이지를 불러오지 못했습니다.');

		},
		success : function(data) {
			$('#Context').html(data);

		}

	});
}

function wonjakDel(wtno){
	var s = confirm("삭제하시겠습니까?");
	if(s==true){
		var path_img = $('[name=path_'+wtno+']').val();
		$.ajax({
			url:'/webtoon_upload2/webtoon_delete/'+wtno,
			type:'POST',
			data:{
				'path':path_img,
				'wtno':wtno
			},
			success:function(data){
				if(data==1)
					alert('삭제되었습니다.');
					home();
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		})
	}
	
}
</script>