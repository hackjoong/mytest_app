<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<c:set var="date" value="<%=new Date()%>" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no" />
<title>TOONIVIE</title>
</head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!-- <script src="/resources/common/js/imgslide.js"></script> --> 
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- custom CSS -->
<link rel="stylesheet" href="/resources/common/css/common.css">
<link rel="stylesheet" href="/resources/common/css/loading.css">
<link rel="stylesheet" href="/resources/common/css/lib.css">
<script src="/resources/common/js/loading.js"></script>

<script>

</script>
<style>


.modal_tooninfo{
	position: absolute;
	background-color:rgba(46,46,46,0.5);
	color:#fff;
	font-size:15px;
	width: 249.3px; 
	height:175.5px; 
	padding:25px;
}
.contents{
	margin-top: -200px;
}
.ellipsis{
	width:200px;
	display: -webkit-box;
	text-overflow: ellipsis;
	overflow: hidden;
	-webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
	margin-top:10px;
	line-height: 22px;
	
}
</style>
<body>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script>



///스페이스바 클릭 시, Video - Play, Pause 전환
///2017-05-30, shkwak 
window.addEventListener("keypress", function (evt) {
  var ENTER = 13;
  if (evt.which === ENTER) {
      myFunction();
      evt.preventDefault();
  }
});

 $(function(){
		$(".loading-block .loading-element").css("transform","translate(50%,50%)");
	$(".modal_tooninfo").hide();
	$('.conts').mouseover(function(){
		$(this).find(".modal_tooninfo").stop().fadeIn(250);
	});
	$('.conts').mouseout(function(){
		$(this).find(".modal_tooninfo").stop().fadeOut(250);
	});
});


</script>
<style>
#list_paging{
	display: inline-block;
	text-align: center;
	width:100%;
	margin-top: 10px;
}
.pagenatoin-ul{
list-style: none;
}
.pagenation-ul>li {
display: inline;
padding: 2px 7px;
margin: 0;
font-size: 14px;
}
.nation_active{
	background-color: #2B314A;
	color: white;
}

</style>	
	<c:import url="/WEB-INF/views/wonjak/header.jsp" />
<!-- 	<form id="asdf" action="/webtoon_wonjak">
		<input type="hidden" id="pageNum" name="pageNum" value=""/>
	</form> -->
	<div class="w100 fl">
		<div class="container pd-0">
			<div class="col-md-12 today pd-0">
				<div class="col-md-12 col-xs-12 today-wrap">
					<div class="col-md-2 col-xs-2 today-st pd-0 re">
						<span class="ab date-main"> <fmt:formatDate value="${date}"
								type="date" pattern="yyyy.MM.dd" />
						</span>
						<!-- <h1 class="ab" style="top:11%; left:13%;">오늘의<br/>웹툰</h1> -->
					</div>
					<div class="col-md-10 col-xs-10 today-imgt ">
						<c:import url="/WEB-INF/views/home/today/today_img_2.jsp" />
						<img src="/resources/image/leftbtn.png" class="ab lbtn"
							onClick="return slide('side',-1)"> <img
							src="/resources/image/rightbtn.png" class="ab rbtn"
							onClick="return slide('side',1)">
					</div>
				</div>
			</div>
			<div class="col-xs-12 genre pd-0">
				<div class="col-md-9 genre-wrap col-xs-12">
					<!-- <ul class="genre-menu col-md-12">
						<li data-genre="episode" class="active col-md-1">에피소드</li>
						<li data-genre="story" class="col-md-1">스토리</li>
						<li data-genre="daily" class="col-md-1">일상</li>
						<li data-genre="gag" class="col-md-1">개그</li>
						<li data-genre="fantasy" class="col-md-1">판타지</li>
						<li data-genre="action" class="col-md-1">액션</li>
						<li data-genre="omnibus" class="col-md-1">옴니버스</li>
						<li data-genre="drama" class="col-md-1">드라마</li>
						<li data-genre="pure" class="col-md-1">순정</li>
						<li data-genre="thriller" class="col-md-1">스릴러</li>
						<li data-genre="history" class="col-md-1">시대극</li>
						<li data-genre="sports" class="col-md-1">스포츠</li>
					</ul> -->
					<div class="w100 fl genre-cont">
						<div class="episode gen" id="myTable">
							<c:forEach var="itemwonjak" items="${newwonjak }" >
							<c:choose>
								<c:when test="${itemwonjak.kinds == 'origintoon' }">
									<div class="col-md-4 col-xs-6 toon_contents">
										<div class="w100 fl conts contents">
											<div class="genre-cont-img cp wonjak-img"
												onclick="location.href='/series/toon/${itemwonjak.wmno}'"
												style="background-image: url('${itemwonjak.wmthumbnail}'); background-size: contain; background-repeat: no-repeat;"></div>
											<div class="conts-cover re_conts_cover">
												<span class="searchTarget genre-cont-subtitle">${itemwonjak.wmgenre }</span><br>
												<div class="searchTarget genre-cont-title re_searchTarget">${itemwonjak.wmtitle }</div><br>
											</div>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<div class="col-md-4 col-xs-6 toon_contents">
										<div class="w100 fl conts contents">
											<div class="genre-cont-img cp wonjak-img"
												onclick="location.href='/webtoon/${itemwonjak.wmno}'"
												style="background-image: url('${itemwonjak.wmthumbnail}'); background-size: contain; background-repeat: no-repeat;"></div>
											<div class="conts-cover re_conts_cover">
												<span class="searchTarget genre-cont-subtitle">${itemwonjak.wmgenre }</span><br>
												<div class="searchTarget genre-cont-title re_searchTarget">${itemwonjak.wmtitle }</div><br>
											</div>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
								
							</c:forEach>
						<div id="list_paging">
							${nation.getNavigator()}
						</div>
						</div>
						
						<div class="story gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">story</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="daily gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">daily</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="gag gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">gag</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="fantasy gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">fantasy</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="action gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="omnibus gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">omnibus</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="drama gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">drama</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
						</div>
						<div class="pure gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">pure</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="thriller gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="history gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>

									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						</div>
						<div class="sports gen">
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-6">
								<div class="w100 fl conts">
									<div class="genre-cont-img"></div>
									<div class="conts-cover">
										<span class="genre-cont-subtitle">장르</span><br> <span
											class="genre-cont-title">제목입니당</span><br>
									</div>
								</div>
							</div>
						<div id="list_paging" style="clear: both;">
							${nation.getNavigator()}
						</div>
						</div>
					</div>
					<div class="w100 fl best-toon">
						<!--div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-6">
							<div class="w100 fl conts">
								<div class="genre-cont-img" style="background-image: url('/resources/image/no-image.jpg'); background-size: cover;"></div>
								<div class="conts-cover">
									<span class="genre-cont-subtitle">장르</span><br>
									<span class="genre-cont-title">제목</span><br>
								</div>
							</div>
						</div-->
					</div>
				</div>
				<div class="col-md-3 rank col-xs-12">
					<c:if test="${ sessionScope.currentUser.getRole()=='ROLE_WRITER' || sessionScope.currentUser.getRole() == 'ROLE_ADMIN' }">
						<div class="add-toon tc" onclick="location.href='/cv/effect'">
							<img src="/resources/image/upload icon.png" class="upload-con">
							<span>투니비 저작툴</span>
						</div>
					</c:if>
					<div class="w100 fl rank-title">
						<!-- <div class="rank-head" style="">투니비 웹툰 순위</div> -->
						<h2>투니비 원작 순위</h2>
						<div class="rank-type">							
							<span>&nbsp;&#124;&nbsp;최신순</span>
							<span>&nbsp;&#124;&nbsp;댓글순</span>
							<span>조회순</span>
						</div>
						<div class="rank-cont">
							<!-- <span class="cp" onclick="location.href='/webmovie?id=1'"><b>1</b> 짝사랑의 추억</span>
							<span class="cp" onclick="location.href='/webmovie?id=2'"><b>2</b> 수평선</span>
							<span class="cp" onclick="location.href='/webmovie?id=3'"><b>3</b> 삼국지</span> 
							<span class="cp" onclick="location.href='/webmovie?id=4'"><b>4</b> 엽기적인 그녀</span> -->
							<c:set var="i" value="1" />
							<c:forEach items="${wonjaksunwi}" var="wonjak">
								<span class="cp" onclick="location.href='/webtoon/${wonjak.getWtno()}'"><b>${i}</b> ${wonjak.getWtname()}</span>
								<c:set var="i" value="${i + 1}" />
							</c:forEach>							
						</div>
						
					</div>
					<!-- <div class="w100 fl notice-title">
							<div class="notice-head">
								공지사항
							</div>
							<ul class="notice-cont">
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
								<li>앙녕 제목입니당</li>
							</ul>
					</div> -->
					<%-- <c:if test="${sessionScope.currentUser != null }">
						<form method="post" action="/toonivie/insertTooninfo">
							<div class="w100 fl tooni-title">
								<div class="tooni-head">
									웹툰 입력
								</div>
								<div class="tooni-cont">
								<input type="text" name="genre" placeholder="장르">
								<input type="text" name="title" placeholder="제목">
								<input type="text" name="writer" placeholder="작가">
								<textarea rows="8" cols="30" name="content" placeholder="내용"></textarea>
								<input type="submit" value="작성" class="tooni-submit"/>
								</div>
							</div>							
						</form>
					</c:if> --%>

				</div>
			</div>
		</div>
	</div>
	<c:import url="/WEB-INF/views/header&footer/footer.jsp" />
	<script src="/resources/common/js/js.js"></script>

</body>
<script>
$(function(){
$('.rank-type').find('span').eq(0).click(function(){ //최신순 클릭 이벤트
	var data = {};
	$.ajax({
		url : "/order/lastest_won",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
			});	 
			console.log(data);
			$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(1).click(function(){ //댓글순 클릭 이벤트
	var data = {};
	$.ajax({
		url : "/order/commentOrder_won",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
			});	
			console.log(data);
			$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});

$('.rank-type').find('span').eq(2).click(function(){ //조회순 클릭 이벤트
	var data = {};
	$.ajax({
		url : "/order/countOrder_won",
		type:'post',
		data: data,
		async: false,
		success: function(data) {	
			$('.rank-cont').empty();
			$.each(data, function(idx, val) {				
				$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
			});	
			console.log(data);
			$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
			$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
			$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
		},
		error : function(request, status, error) {
			alert("code:" + request.status
					+ "\n" + "error:" + error);	
		}
	});
});
})
</script>
</html>