function tab9(lang){//이미지 컷 탭
	if(lang=="kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");

		$("#tab9_1st").attr({"data-step":"1","data-intro":"원본 웹툰 이미지를 불러옵니다."});
		$("#wt_cut_save").attr({"data-step":"2","data-intro":"서버에 웹툰 이미지를 업로드 하고, 작업영역에 이미지가 표시됩니다."});
		if($("#wt_whole_cropArea").attr('src') == undefined){
			$("#wt_whole_cropArea").css("width","960px").css("height","540px");
			$("#wt_whole_img").removeClass("dn");
			$("#canvas_width_re").addClass("dn");
			$("#background_img").addClass("dn");
		}
		$("#wt_whole_img").attr({"data-step":"3","data-intro":"작업영역에서 이미지를 컷 합니다."});
		$("#tab9_3rd").attr({"data-step":"4","data-intro":"컷 단위로 분리된 이미지 컷이 표시되고, 삭제가 가능합니다."});
		$("#tab9_4th").attr({"data-step":"5","data-intro":"컷 단위로 분리된 이미지를 다운로드하기 위한 파일명을 입력합니다."});
		$("#tab9_5th").attr({"data-step":"6","data-intro":"컷 단위로 분리된 모든 이미지를 다운로드합니다."});
	}
	else if(lang="en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab9_1st").attr({"data-step":"1","data-intro":"Add a webtoon image."});
		$("#tab9_2nd").attr({"data-step":"2","data-intro":"Upload the Webtoon image to the server, and cut-off splitter is enabled."});
		$("#tab9_3rd").attr({"data-step":"3","data-intro":"The image cut is displayed in cut units and can be deleted."});
		$("#tab9_4th").attr({"data-step":"4","data-intro":"Enter the file name for downloading the separated images in cut units."});
		$("#tab9_5th").attr({"data-step":"5","data-intro":"Download all images separated by cuts."});
		
	}
	introJs().start();
}

function tab1_img(lang){ //이미지분리 탭 - img
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#wt_whole_img").addClass("dn");
		$("#tab1_1st").attr({"data-step":"1","data-intro":"분리할 이미지를 추가합니다."});
		$("#tab1_2nd").attr({"data-step":"2","data-intro":"서버에 분리할 이미지를 업로드 하고, 작업영역에 이미지가 표시됩니다."});
		$("#tab1_3rd").attr({"data-step":"3","data-intro":"배경색상을 어떤색으로 채울지 지정합니다."});
		$("#tab1_4th").attr({"data-step":"4","data-intro":"객체를 분리하기 위해 종류를 선택합니다."});
		if(fileUploadOpertFlag == false){
			$("#canvas_width_re").css("width","960px").css("height","540px");
			$("#canvas_width_re").removeClass("dn");			
		}
		if($("#canvas_width_re").parents(".canvas-wrap").hasClass("dn")){
			$("#canvas_bg").attr({"data-step":"5","data-intro":"작업영역에서 분리할 영역을 지정합니다."});
		}else{
			$("#canvas_width_re").attr({"data-step":"5","data-intro":"작업영역에서 분리할 영역을 지정합니다."});
		}
		$("#tab1_5th").attr({"data-step":"6","data-intro":"불러온 이미지의 대표 색상을 추출하고 원하는 색상으로 변경합니다.(옵션)"});
		$("#tab1_6th").attr({"data-step":"7","data-intro":"RGB를 기준으로 색상을 보정합니다.(옵션)"});
		$("#tab1_7nd").attr({"data-step":"8","data-intro":"배경을 저장하여 이미지 분리를 마무리 합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab1_1st").attr({"data-step":"1","data-intro":"Add image to detach."});
		$("#tab1_2nd").attr({"data-step":"2","data-intro":"Select the kind of object you want to separate."});
		$("#tab1_3rd").attr({"data-step":"3","data-intro":"Specifies the color that will populate the background color."});
		$("#tab1_4th").attr({"data-step":"4","data-intro":"Detach the object and save the remaining background."});
		$("#tab1_5th").attr({"data-step":"5","data-intro":"You change the color by extracting colors from imported images and extracting representative colors."});
		$("#tab1_6th").attr({"data-step":"6","data-intro":"Correct the color."});
		
	}
	introJs().start();
}

function tab1_psd(lang){ //이미지분리 탭 -psd
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab1_psd_1st").attr({"data-step":"1","data-intro":"분리할 PSD 파일을 추가합니다."});
		$("#psd_file_name").attr({"data-step":"2","data-intro":"업로드하면 이름이 나옵니다."});
		$("#tab1_psd_3rd").attr({"data-step":"3","data-intro":"레이어를 분리합니다."});
		$("#tab1_psd_4th").attr({"data-step":"4","data-intro":"PSD 배경을 지정합니다."});
		$("#tab1_psd_5th").attr({"data-step":"5","data-intro":"레이어 이미지 분리를 선택합니다."});
		$("#tab1_psd_6th").attr({"data-step":"6","data-intro":"분리할 이미지를 저장합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab1_psd_1st").attr({"data-step":"1","data-intro":"Add PSD file to detach."});
		$("#psd_file_name").attr({"data-step":"2","data-intro":"Upload to get a name."});
		$("#tab1_psd_3rd").attr({"data-step":"3","data-intro":"Detach layers."});
		$("#tab1_psd_4th").attr({"data-step":"4","data-intro":"Specify the PSD background."});
		$("#tab1_psd_5th").attr({"data-step":"5","data-intro":"Select Layer Image Isolation."});
		$("#tab1_psd_6th").attr({"data-step":"6","data-intro":"Save the image you want to detach."});
		
	}
	introJs().start();
}

function tab2(lang){//연출 탭
	if(lang=="kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab2_1st").attr({"data-step":"1","data-intro":"인물, 사물, 배경, 장면(신) 등의 연출이 있고, 2가지의 연출방식이 존재합니다."});
		$("#tab2_2nd").attr({"data-step":"2","data-intro":" 첫 번째 연출방식 예시입니다. 효과를 선택합니다."});
		$(".heedo-popup").attr({"data-step":"3","data-intro":"연출 시간(초)을 설정합니다."});
		$("#wrap1").attr({"data-step":"4","data-intro":"분리된 이미지를 작업영역으로 드래그하여 내려놓습니다."});
		$("#background_img").attr({"data-step":"5","data-intro":"드래그한 분리된 이미지를 원하는 위치에 놓습니다."});
		$(".direction1").find(".result-btn-1").attr({"data-step":"6","data-intro":"시작과 종료 위치를 지정하고 완료 버튼을 누릅니다."});		
		$("#multiapply").attr({"data-step":"7","data-intro":"효과 적용하기 버튼을 눌러 연출 효과를 적용합니다."});
	}
	else if(lang="en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab2_1st").attr({"data-step":"1","data-intro":"It's character production."});
		$("#tab2_2nd").attr({"data-step":"2","data-intro":"It's a production of things."});
		$("#tab2_3rd").attr({"data-step":"3","data-intro":"It's a background production."});
		$("#tab2_4th").attr({"data-step":"4","data-intro":"It's a production of a scene."});
	}
	
	introJs().setOption('doneLabel', 'Done').start();

}
function tab2_2(lang){//연출 탭
	if(lang=="kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");	
		$("#tab22_3rd").attr({"data-step":"1","data-intro":" 두 번째 연출방식 예시입니다. 효과를 선택합니다."});
		$(".heedo-popup").attr({"data-step":"2","data-intro":"연출 시간(초)을 설정합니다."});
		$("#wrap1").attr({"data-step":"3","data-intro":"분리된 이미지를 작업영역으로 드래그하여 내려놓습니다."});
		$("#background_img").attr({"data-step":"4","data-intro":"드래그한 분리된 이미지를 원하는 위치에 놓습니다."});	
		$("#multiapply").attr({"data-step":"5","data-intro":"효과 적용하기 버튼을 눌러 연출 효과를 적용합니다."});
	}
	else if(lang="en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab2_1st").attr({"data-step":"1","data-intro":"It's character production."});
		$("#tab2_2nd").attr({"data-step":"2","data-intro":"It's a production of things."});
		$("#tab2_3rd").attr({"data-step":"3","data-intro":"It's a background production."});
		$("#tab2_4th").attr({"data-step":"4","data-intro":"It's a production of a scene."});
	}
	introJs().setOption('doneLabel', 'Done').start();
}
function tab3(lang){ //카메라연출 탭
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab3_1st").attr({"data-step":"1","data-intro":"카메라 연출을 적용합니다."});
		$("#wrap1").attr({"data-step":"2","data-intro":"분리된 이미지를 작업영역으로 드래그하여 내려놓습니다."});
		$("#background_img").attr({"data-step":"3","data-intro":"내려 놓은 분리된 이미지를 원하는 위치에 놓습니다."});
		$("#Zoom-out").attr({"data-step":"4","data-intro":"카메라 연출을 선택합니다."});	
		$(".heedo-popup").attr({"data-step":"5","data-intro":"카메라 연출 영상의 시간을 설정합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab3_1st").attr({"data-step":"1","data-intro":"This is a camera director."});
		
	}
	introJs().start();
}
function tab4(lang){ //자동효과 탬플릿 탭
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab4_1st").attr({"data-step":"1","data-intro":"장면에 템플릿을 적용합니다."});
		$("#Flower").attr({"data-step":"2","data-intro":"템플릿을 선택합니다."});	
		$(".heedo-popup").attr({"data-step":"3","data-intro":"연출 영상의 시간을 설정합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab4_1st").attr({"data-step":"1","data-intro":"Applies the template to the scene."});
		
	}
	introJs().start();
}
function tab5(lang){ //텍스트 탭
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab5_1st").attr({"data-step":"1","data-intro":"말풍선 효과 탭입니다."});
		$("#tab5_2nd").attr({"data-step":"2","data-intro":"텍스트를 입력합니다."});
		$("#subtitle_color").attr({"data-step":"3","data-intro":"텍스트 색상을 선택합니다."});
		$("#subtitle_font").attr({"data-step":"4","data-intro":"텍스트 폰트를 선택합니다."});
		$("#subtitle_size").attr({"data-step":"5","data-intro":"텍스트 크기를 선택합니다."});
		$("#tab5_6th").attr({"data-step":"6","data-intro":"텍스트를 삽입합니다."});
		$("#background_img").attr({"data-step":"7","data-intro":"텍스트를 원하는 위치에 놓습니다."});	
		$("#tab5_7th").attr({"data-step":"8","data-intro":"텍스트 연출을 완료합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab5_1st").attr({"data-step":"1","data-intro":"It's the speech bubble effect tab."});
		$("#tab5_2nd").attr({"data-step":"2","data-intro":"Type the text."});
		$("#subtitle_color").attr({"data-step":"3","data-intro":"Select a text color."});
		$("#subtitle_font").attr({"data-step":"4","data-intro":"Select a text font."});
		$("#subtitle_size").attr({"data-step":"5","data-intro":"Select a text size."});
		$("#tab5_6th").attr({"data-step":"6","data-intro":"Insert the text."});
		$("#tab5_7th").attr({"data-step":"7","data-intro":"Complete the text presentation."});
		
	}
	introJs().start();
}
function tab7(lang){//플랫폼 업로드 탭
	if(lang == "kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab7_1st").attr({"data-step":"1","data-intro":"투니비 플랫폼에 웹툰을 업로드 할 수 있습니다."});
		$("#tab7_2nd").attr({"data-step":"2","data-intro":"업로드할 파일을 선택합니다."});
		$("#tab7_3rd").attr({"data-step":"3","data-intro":"장르를 입력합니다."});
		$("#tab7_4th").attr({"data-step":"4","data-intro":"제목을 입력합니다."});
		$("#tab7_5th").attr({"data-step":"5","data-intro":"작가명을 입력합니다(로그인한 경우는 제외)"});
		$("#tab7_6th").attr({"data-step":"6","data-intro":"내용을 입력합니다."});
		$("#tab7_7th").attr({"data-step":"7","data-intro":"업로드합니다."});
	}
	else if(lang == "en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab7_1st").attr({"data-step":"1","data-intro":"You can upload webcomics to the TOONIVIE platform."});
		$("#tab7_2nd").attr({"data-step":"2","data-intro":"Select the file to upload."});
		$("#tab7_3rd").attr({"data-step":"3","data-intro":"Enter a genre."});
		$("#tab7_4th").attr({"data-step":"4","data-intro":"Enter the webcomics title."});
		$("#tab7_5th").attr({"data-step":"5","data-intro":"Type a name (unless you are logged in)."});
		$("#tab7_6th").attr({"data-step":"6","data-intro":"Enter the webcomics content."});
		$("#tab7_7th").attr({"data-step":"7","data-intro":"Upload."});		
	}
	introJs().start();
}
function tab8(lang){//압축변환 탭
	if(lang=="kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab8_1st").attr({"data-step":"1","data-intro":"영상을 압축하고 변환합니다."});
		$("#tab8_2nd").attr({"data-step":"2","data-intro":"변환할 파일을 선택합니다."});
		$("#tab8_3rd").attr({"data-step":"3","data-intro":"변환할 해상도를 선택합니다."});
		$("#tab8_4th").attr({"data-step":"4","data-intro":"추가로 변환할 내용을 선택합니다."});
		$("#tab8_5th").attr({"data-step":"5","data-intro":"변환합니다."});
		$("#tab8_6th").attr({"data-step":"6","data-intro":"변환된 영상을 다시보려면 변환한 해상도를 선택해주세요."});
	}
	else if(lang="en"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#tab8_1st").attr({"data-step":"1","data-intro":"Compresses and converts the videos."});
		$("#tab8_2nd").attr({"data-step":"2","data-intro":"Select the file to convert."});
		$("#tab8_3rd").attr({"data-step":"3","data-intro":"Select the resolution you want to convert."});
		$("#tab8_4th").attr({"data-step":"4","data-intro":"Select Additional Conversions."});
		$("#tab8_5th").attr({"data-step":"5","data-intro":"Convert."});
		$("#tab8_6th").attr({"data-step":"6","data-intro":"Select a converted resolution to view the converted image again."});
		
	}
	introJs().start();
}

function audionsubtitle(lang){//압축변환 탭
	if(lang=="kr"){
		$("*").removeAttr("data-step").removeAttr("data-intro");
		$("#videoInput").attr({"data-step":"1","data-intro":"저작툴에서 생성한 웹툰무비 영상을 추가합니다."});
		$("#txt-title").attr({"data-step":"2","data-intro":"자막 추가하기 "});
		$("#smiInputData0").find(".audio-time-txt").find(".fl").eq(0).attr({"data-step":"3","data-intro":"영상에 보여 질 자막의 시작/끝 시간을 설정합니다."});
		$("#smiInputData0").find(".audio-time-txt").find(".fr").eq(0).attr({"data-step":"4","data-intro":"자막의 언어를 설정합니다."});
		var lang = $(".translate-active").attr("data-lang");
		$("#smiInputData0").find(".audio-txt-"+lang).attr({"data-step":"5","data-intro":"영상에 보여 질 자막 내용을 입력합니다."});
		$("#smiInputData0").find(".txt-cont-save-"+lang).attr({"data-step":"6","data-intro":"자막의 시작/끝 시간, 자막 언어, 자막 내용을 저장합니다."});
		$("#append-btn1").attr({"data-step":"7","data-intro":"자막을 추가할 수 있는 폼을 제공합니다."});
		
		$("#audio-title").attr({"data-step":"8","data-intro":"더빙 추가하기"});
		$("#audioInputData0").find(".audio-time-txt").find(".load-btn").attr({"data-step":"9","data-intro":"더빙할 음원파일을 불러옵니다."});
		$("#audioInputData0").find(".audio-time-txt").find(".fl").eq(0).attr({"data-step":"10","data-intro":"웹툰무비 영상에 추가할 더빙의 시작/끝 시간을 설정하고 시간을 적용합니다."});
		$("#waveform0").attr({"data-step":"11","data-intro":"음원파일을 불러와 더빙할 구간을 설정합니다."});
		$("#audioInputData0").find(".audio-cont-save").attr({"data-step":"12","data-intro":"더빙의 시작/끝 시간, 더빙 구간을 저장합니다."});
		$("#append-btn2").attr({"data-step":"13","data-intro":"더빙을 추가할 수 있는 폼을 제공합니다."});
		$("#save").attr({"data-step":"14","data-intro":"최종적으로 자막(smi)파일과 더빙을 적용한 웹툰무비 영상을 저장합니다."});		
	}else if(lang="en"){
	}
	introJs().start();
}
