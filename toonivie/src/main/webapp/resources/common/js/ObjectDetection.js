$(document).ready(function(){
	
	$('#ObjectDetection').click(function(){
		swal({
			title: "객체인지를 사용하시겠습니까?",
			icon: "warning",
			buttons: true,
			showCancelButton: true,
			dangerMode: true,
			}).then((result) => {
				if (result.value) {	 
					if(clicked_color_rgb == ''){
						clicked_color_rgb.push(1);
					}
					var data = {};
					data["imgUrl"]=grabcutUrl;
				    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
				    var csrfToken = $("meta[name='_csrf']").attr("content");
				    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
				    var headers = {};
			        headers[csrfHeader] = csrfToken;
			        data[csrfParameter] = csrfToken;
			        data["filling_type"]=filling_type;
			        data['clicked_color_rgb']=clicked_color_rgb;
			        data["blur_status"]=blur_status;
			        $.ajax({
			    		url : "/ObjectDetection",
			    		dataType : "json",
			    		type : "POST",
			    		async: false,
			    		data : data,
			    		beforeSend:function(){
			    			loadingOn();
			    			$("#cutModeBtn1").attr("src","/resources/image/v2.0/cutModeBtn1.png");
							$("#cutModeBtn2").attr("src","/resources/image/v2.0/cutModeBtn2.png");
							$("#ObjectDetection").attr("src","/resources/image/v2.0/cutModeBtn3_click.png");
			    		},
			    		success : function(data) {
			    			var common_data = data[data.length-1];
			    			setTimeout(function () {
			    				var randomId = new Date().getTime();
			    				$.each(data, function (index, item){
			        				resultWidth=item.width;
			        				resultHeight=item.height;
			        				resultImg="";
			        				resultImg=item.resultImg;
			        				backImg=common_data.backGroundFile;
			        				grabcutUrl=common_data.backGroundFile;
			        				backGroundWidth=common_data.backGroundWidth;
			        				backGroundHeight=common_data.backGroundHeight;
			        				$(".dragDiv"+listNum+"").attr("title",item.Object_type);
			        				saveResultToList1();
			    				});
			    				loadingOff();
			    			},1000);
			    		},
			    		error : function(request, status, error) {
			    			swal({
								  title: 'Error!',
								  text: "code:" + request.status + "\n" + "error:" + error,
								  type: 'error',
								})
			    			loadingOff();
			    		}
			        });
				}else{
					return;
				}
			});
		
	});
});