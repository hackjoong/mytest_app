/*!
 * webmovie.js v0.1.0 (https://www.toonivie.com/webmovie)
 * Copyright 2017 Ideaconcert
 * Licensed under *****
 * Author : shkwak
 * Date : 2017-06-07(Wed)
 */

function webmovie_show(id) {	
	console.log(myVideo.src, id);
	
	switch(id) {
    case 1: //'짝사랑의 추억':
        myVideo.src = "/resources/cvMov/2017-04-24_1.mp4";
        myVideo.poster = "/resources/image/2017-04-24_1.mp4_000004590.jpg";
        $(".webmovie-title").html("짝사랑의 추억");
        $(".thumbnail_new").attr('src',"/resources/image/thumnail_1.png");
		$(".artist-name").html("컬투쇼 청취자 사연");
        $(".genre-name").html("드라마/에피소드"); 
        $(".summary").html("SBS 컬투쇼의 베스트 사연을 웹툰으로 제작한 영상으로 한 여성의 짝사랑에 대한 에피소드를 소개하고 있다.");
        $("#webmovie_1").css('display','table-row-group');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','none');
        break;
    case 2: //'수평선':
        myVideo.src = "/resources/cvMov/2016-11-02_1.mp4";
        myVideo.poster = "/resources/image/2016-11-02_1.mp4_000003956.jpg";
        $(".webmovie-title").html("수평선");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_2_1.jpg");
        $(".artist-name").html("정지훈");
        $(".genre-name").html("스릴러/미스터리");
        $(".summary").html("세상이 멸망한 어느 날, 부모를 잃은 소년, 소녀가 영원히 함께 앞으로 걸어갈 수 있다는 한 가지 희망만으로 살아가는데... 그런 희망조차 빼앗으려는 망가진 어른들의 등장!");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','table-row-group');
        $("#webmovie_3").css('display','none');
        break;
    case 3: //'삼국지':
    	myVideo.src = "/resources/cvMov/2016-11-02_2.mp4";
    	myVideo.poster = "/resources/image/2016-11-02_2.mp4_000010407.jpg";
    	$(".webmovie-title").html("삼국지");
		$(".thumbnail_new").attr('src',"/resources/image/thumnail_3.jpg");
    	$(".artist-name").html("박봉성");
        $(".genre-name").html("시대극/액션");
        $(".summary").html("작가 박봉성은 단순희 삼국지에 그림을 추가해 만화삼국지를 펴낸것이 아니라, 방대한 자료수집과 중국현지 취재를 통해 나관중도 간과했던 새로운 사실을 밝혀내고, 시대상황을 소상하게 설명하고 있다.");
        $("#webmovie_1").css('display','none');
        $("#webmovie_2").css('display','none');
        $("#webmovie_3").css('display','table-row-group');
    	break;
    default:
        alert("웹툰무비 준비 중입니다.");
	} 
}
/*
//스페이스바 클릭 시, Video : Play/Pause 전환
//2017-05-30, shkwak 
window.addEventListener("keypress", function (evt) {
    var SPACEBAR = 32;
	
	//console.log(evt.which);
    if (evt.which === SPACEBAR) {
        playPause();
        evt.preventDefault();
    }	
});

function playPause()
{ 
	if (myVideo.paused)	myVideo.play(); 
	else myVideo.pause(); 
}

///방향키 클릭 시(Left/Right), Video : currentTime 변경
///2017-06-07, shkwak
window.addEventListener("keydown", function (evt) {
	var LeftArrow = 37;
	var RightArrow = 39;

	//console.log(evt.keyCode);
	if (evt.keyCode === LeftArrow) {
        myVideo.currentTime -= 1;
        evt.preventDefault();
    } else if (evt.keyCode === RightArrow) {
		myVideo.currentTime += 1;
        evt.preventDefault();
    }
});

///방향키 클릭 시(Up/Down), Video : Volume 조절
///2017-06-07, shkwak
window.addEventListener("keydown", function (evt) {
	var UpArrow = 38;
	var DownArrow = 40;

	//console.log(evt.keyCode);
	if (evt.keyCode === UpArrow) {
        myVideo.volume += 0.1;		
        evt.preventDefault();
    } else if (evt.keyCode === DownArrow) {
		myVideo.volume -= 0.1;
        evt.preventDefault();
    }
	console.log(myVideo.volume*100);
});
*/
/*
<div id="curTime"> ...</div>
<div id="vRemaining"> ...</div>

//display the current and remaining times
myVideo.addEventListener("timeupdate", function () {
  //Current time  
  var vTime = myVideo.currentTime;
  var vLength = myVideo.duration.toFixed(1);
  document.getElementById("curTime").textContent = vTime.toFixed(1);
  document.getElementById("vRemaining").textContent = (vLength - vTime).toFixed(1);
}, false);
*/

/*
<span id="test1"></span>
<span id="test2"></span>
<span id="test3"></span>
<span ><button onclick="test44width()">버튼</button></span>

//display the device width
$("#test1").html(screen.width);				//화면 해상도
$("#test2").html(document.body.scrollWidth);//document 전체 넓이
$("#test3").html(innerWidth);				//window 넓이 : ok

function test44width() {
	$("#test1").html(screen.width);
	$("#test2").html(document.body.scrollWidth);
	$("#test3").html(innerWidth);
}
*/