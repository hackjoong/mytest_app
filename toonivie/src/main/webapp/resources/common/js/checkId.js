$(function() {
	$("#checkbtn").click(function() {
		var id = document.getElementById("id").value;
		var d = $(".idChecked");
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content"); // THIS WAS ADDED
		var reg_exp = new RegExp("^[a-zA-Z][a-zA-Z0-9]{3,11}$", "g"); //특수문자(_) 들어갈수 있게 수정 필요!! 2018-01-17 by shkwak
		var match = reg_exp.exec(id);
		if (match == null || id.length < 4 || id.length > 12) {
			alert("아이디는 영문숫자 혼용 혹은 영문, 첫글자는 영문, 4자이상 12자 이하이여야 합니다!!");
			document.getElementById("id").value = "";
			return false;
		}
		var data = {};
		var headers = {};
		data[csrfParameter] = csrfToken;
		data["id"] = id;
		headers[csrfHeader] = csrfToken;
		$.ajax({
			url : "/signup/checkId",
			dataType : "json",
			type : "POST",
			headers : headers,
			data : data,
			success : function(data) {
				if (data.message == "TRUE") {
					var q = confirm(id + "는 사용할 수 있는 아이디 입니다. 사용하시겠습니까?");
					if (q == true) {
						document.getElementById("id").readOnly = true;
						$("#id").css("background", "gray");
						$('#id').css("color","#fff");
						$('#idChecked').val("idChecked");
					} else {
						$("#id").val("");
					}
				} else {
					$("#id").val("");
					alert("중복된 아이디 입니다.");
				}
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "error:" + error);
			}

		});

	});
	
	
	$("#nickcheckbtn").click(function() {
		var nickname = document.getElementById("nickname").value;
		var d = $(".idChecked");
		var nameReg = /([가-힣A-Za-z]{2,4})/g;

		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content"); // THIS WAS ADDED
		if (nameReg.test(nickname)==false || nickname.length < 2 || nickname.length > 12) {
			alert("닉네임은 한글, 영문이거나 한글+숫자, 영문+숫자 혹은 2자이상 12자 이하이여야 합니다.");
			document.getElementById("nickname").value = "";
			return false;
		}
		var data = {};
		var headers = {};
		data[csrfParameter] = csrfToken;
		data["nickname"] = nickname;
		headers[csrfHeader] = csrfToken;
		$.ajax({
			url : "/signup/checkNickname",
			dataType : "json",
			type : "POST",
			headers : headers,
			data : data,
			success : function(data) {
				if (data.message == "TRUE") {
					var q = confirm(nickname + "는 사용할 수 있는 닉네임 입니다. 사용하시겠습니까?");
					if (q == true) {
						document.getElementById("nickname").readOnly = true;
						$("#nickname").css("background", "gray");
						$('#nickname').css("color","#fff");
						$('#nickChecked').val("nickChecked");
					} else {
						$("#nickname").val("");
					}
				} else {
					$("#nickname").val("");
					alert("중복된 아이디 입니다.");
				}
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "error:" + error);
			}

		});

	});
})

