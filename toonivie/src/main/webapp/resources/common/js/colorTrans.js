var color_change_status = 'true';
var selected_color =null;
var ROI_status = 'false';
$( document ).ready(function() {
	$('#color_palette_get').click(function(){
		if(11 > $(".color-palette-count").val() > 0){
			if(ROI_status == 'true' || ROI_status == 'using'){
				init_get_palette_roi($(".color-palette-count").val());
			}else{
				init_get_palette($(".color-palette-count").val());
			}
		}else {
			swal('1 ~ 10개 까지만 적용 가능합니다.');
		}
		popupOff($(".color-palette-modal"));
	});
	$('#change_color_trans').click(function(){
		if(color_change_status == 'true'){
			$(this).addClass('btn-gray');
			$(this).removeClass('btn-cyan');
			$(".tab1_btn_color").removeClass('disabled').removeClass('btn-gray').addClass('btn-cyan');
			init_color_change();
			color_change_status = 'false';
		}else if(ROI_status == 'using'){
			$(this).addClass('btn-gray');
			$(this).removeClass('btn-cyan');
			$(".tab1_btn_color").removeClass('disabled').removeClass('btn-gray').addClass('btn-cyan');
			init_color_change_roi();
			context.rect(GbeX, GbeY, GmoX - GbeX, GmoY - GbeY);
			context.stroke();
		}
	});
	$('.color_box').on('click','.select_color_change' ,function() {
		var data = 'null';
		selected_color = $(this);
		$("#change_color_trans").removeClass('btn-gray');
		$("#change_color_trans").addClass('btn-ivory');
		if(ROI_status != 'using'){
			color_change_status = 'true';
		}
		
		popupOn($('.color-picker-modal'), data);
		$('#color-picker-text').val(selected_color.data('color-before')); //selected palate color 2 color picker color
		$('#color_picker_preview').css("background-color", selected_color.data('color-before'));
		colorpicker_trans("color_boxs_po","/resources/image/v2.0/color_picker_img.png","color_picker_preview");
	    var colorText = document.getElementById("color-picker-text");
	    
	    $('#color-picker').val(colorText.value);
	    colorText.onchange = function() {
	    	$('#color-picker').val(colorText.value);
	    }
		
	});
	$('#color-picker-get').click(function(){
		selected_color.data('color-after', $('#color-picker-text').val());
		selected_color.children("div").css('background',$('#color-picker-text').val()).css('display','block');
		selected_color.children("img").css('display','block');
		$(".tab1_btn_color").addClass('disabled').removeClass('btn-cyan').addClass('btn-gray');
		popupOff($('.color-picker-modal'));
	});
	
	var img_url = '';
	$('#grabcut_color_picker').click(function(){
		color_picker_init = 'true';
		swal('오른쪽 버튼으로 색상을 뽑아주세요');
		set_filling_color('3');
		colorpicker("canvas-fg", img_url);
	});
	$('.color_box').on('mousemove','.select_color_change' ,function() {
		$('.select_color_change').draggable({
			revert:"invalid",
			start:function(){
				$(this).css('z-index', '1000');
			},
			stop:function(){
				$('color_box_me').css('z-index', '100');
			}
		});
		$('.select_color_change').droppable({
		    drop: function(event,ui) {
		    	init_color_merge(ui.draggable, $(this));
		        ui.draggable.fadeOut(function () {
		            ui.draggable.remove();
		          });   
		        }
		});
	});

});
function ROI_setting(){
	if(ROI_status == 'using'){
		ROI_status = 'false';
		$('#ROI_select').html('ROI 설정');
		$('#ROI_select').removeClass('disabled');
		GbeX = 0;
		GbeY = 0;
		GafX = 0;
		GafY = 0;
		init_color_picker();
		return;
	}
	swal('ROI 영역을 설정해주세요');
	color_change_status = 'false';
	ROI_status = 'true';
	Gmode=0;
	$('#ROI_select').html('ROI 설정 중');
	$('#cutModeBtn2').removeClass('btn-cyan');
	$('#cutModeBtn1').removeClass('btn-gray');
	$('#ROI_select').addClass('disabled');
}

var color_picker_init= "false";
function put_color(color_list){
	$('.color_box_text').addClass("dn");
	$('.color_box_me').remove();
	var color_width = 100/color_list.length+'%' ;
	for(var i=0;i<color_list.length;i++){
		var append_str = "<div class='color_box_me select_color_change' id='palette"+(i+1)+"' style='background:"+color_list[i]+";width:"+color_width+"' data-color-before='"+color_list[i]+"' data-color-after=''>"+
						 	"<img class='color_box_me_arrow color_box_me_change"+(i+1)+"' src='/resources/image/bot_arrow.png'>" + 
						 	"<div class='color_box_me_after color_box_me_change"+(i+1)+"'></div>" +
						 "</div>";
		$('.color_box').append(append_str);
		
	}
}

var color_filename;
function get_count(){
		var data = 'null';
		$(".color-palette-count").attr("value","5");
		popupOn($(".color-palette-modal"), data);
}

var color_list = new Array();
var color_list_bf = new Array();
var color_list_af = new Array();

function init_color_change(){
	color_info = new Object();
	color_info.filepath = color_filename;
	color_list = new Array();
	color_list_bf = new Array();
	color_list_af = new Array();

	$('.color_box_me').each(function(){
		//palette_color
		color_list.push($(this).data('color-before'));
		if($(this).data('color-after') != ''){
			color_list_bf.push($(this).data('color-before'));
			color_list_af.push($(this).data('color-after'));
		}
	});
	color_info.color_list = color_list;
	color_info.color_list_bf = color_list_bf;
	color_info.color_list_af = color_list_af;
	var info = JSON.stringify(color_info);
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};

	headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
		url : "/color_trans/color_change",
		type : "POST",
		async: false,
		data : {
			'info' : info
		},
		success : function(data) {
			var randomId = new Date().getTime();
			img_url = "/resources/cvimgs/"+color_filename+"?v="+randomId;
			init_color_picker();
			$("#canvas-before").css("background-image","url('"+color_filename+"?v="+randomId+"')");
			init_get_palette(color_list.length);
		},
		error : function(request, status, error) {
			swal("code:" + request.status + "\n" + "error:" + error);
			loadingOff();
		}
	});
}

function init_color_change_roi(){
	color_info = new Object();
	color_info.filepath = color_filename;
	color_list = new Array();
	color_list_bf = new Array();
	color_list_af = new Array();
	//get color_list
	$('.color_box_me').each(function(){
		//palette_color
		color_list.push($(this).data('color-before'));
		if($(this).data('color-after') != ''){
			//put before after color data
			color_list_bf.push($(this).data('color-before'));
			color_list_af.push($(this).data('color-after'));
		}
	});
	color_info.color_list = color_list;
	color_info.color_list_bf = color_list_bf;
	color_info.color_list_af = color_list_af;
	color_info.GbeX = Math.round(GbeX * img_resize_rate_w);
	color_info.GbeY = Math.round(GbeY * img_resize_rate_h);
	color_info.GafX = Math.round(GafX * img_resize_rate_w);
	color_info.GafY = Math.round(GafY * img_resize_rate_h);
	color_info.inoutFlag = $('#roi_negative').prop('checked');
	var info = JSON.stringify(color_info);

	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};

	headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
	$.ajax({
		url : "/color_trans/color_change_roi",
		type : "POST",
		async: false,
		data : {
			'info' : info
		},
		beforeSend:function(){
		},
		success : function(data) {
			var randomId = new Date().getTime();
			img_url = "/resources/cvimgs/"+color_filename+"?v="+randomId;
			$("#canvas-before").css("background-image","url('/resources/cvimgs/"+color_filename+"?v="+randomId+"')");
			init_get_palette_roi(color_list.length);
		},
		error : function(request, status, error) {
			swal("code:" + request.status + "\n" + "error:" + error);
			loadingOff();
		},complete : function() {
			//rew rect by KH 18-03-20
		    context.rect(GbeX, GbeY, GmoX - GbeX, GmoY - GbeY);
			context.stroke();
	    }

	});
}

function init_get_palette_roi(color_count){

	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
	headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    
	$.ajax({
		url : "/color_trans/get_color_palette_roi",
		type : "POST",
		async: false,
		data : {
			'filename' : color_filename,
			'color_count' : color_count,
			'GbeX' : Math.round(GbeX * img_resize_rate_w),
			'GbeY' : Math.round(GbeY * img_resize_rate_h),
			'GafX' : Math.round(GafX * img_resize_rate_w),
			'GafY' : Math.round(GafY * img_resize_rate_h),
			'inoutFlag' : inoutFlag = $('#roi_negative').prop('checked')
		},
		beforeSend:function(){
//			loadingOn();
		},
		success : function(data) {
			put_color(data);
			ROI_status = 'using';
			$('#ROI_select').html('ROI 설정 해제');
			$('#ROI_select').removeClass('disabled');
		},
		error : function(request, status, error) {
			swal("code:" + request.status + "\n" + "error:" + error);
			loadingOff();
		}
	});
}


function init_get_palette(color_count){
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};

	headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    
	$.ajax({
		url : "/color_trans/get_color_palette",
		type : "POST",
		async: false,
		data : {
			'filename' : color_filename,
			'color_count' : color_count
		},
		beforeSend:function(){
		},
		success : function(data) {
			put_color(data);
		},
		error : function(request, status, error) {
			swal("code:" + request.status + "\n" + "error:" + error);
			loadingOff();
		}
	});
}

//color picker init by KH 18-03-19
	var clicked_canvas=null;
	var clicked_color_rgb= new Array();
	function colorpicker(canvas_me, img_url){
		clicked_color_rgb = [];
		//오른쪽 클릭 메뉴 비활성 by KH 2018-02-20
		var colorLabel = document.getElementById('grabcut_color_picker_pre');
		var canvas_img = null;
		var img_context = null;
		canvas_img = canvas;
		img_context = context;
		var image = new Image();
		image.src = img_url;
		$(image).on('load' , function() {
		    img_context.drawImage(image, 0, 0);
		});
		clicked_canvas = $('#canvas-before');
	}
	var clicked_canvas_trans=null;
	var clicked_color_rgb_trans= new Array();
	function colorpicker_trans(canvas_me, img_url, colorLabel_id){
		clicked_color_rgb_trans = [];
		var colorLabel = document.getElementById(colorLabel_id);
		var colorText = document.getElementById("color-picker-text");
		var canvas_img = null;
		var img_context = null;
		canvas_img = document.getElementById(canvas_me);
		img_context = canvas_img.getContext('2d');
		var image = new Image();
		image.src = img_url;
		$(image).on('load' , function() {
		    img_context.drawImage(image, 0, 0, 170, 145);
		});
		clicked_canvas_trans = $('#'+canvas_me);
		$(canvas_img).mousedown(function(e) {
			var input_button = e.which;
			if(input_button == 1){
				var x = e.offsetX;
				var y = e.offsetY;
				var imageData = img_context.getImageData(x, y, 1, 1);
				var pixels = imageData.data;
				var rgbaColor = 'rgb(' + pixels[0] + ',' + pixels[1] + ',' + pixels[2] +')';
				colorLabel.style.backgroundColor = rgbaColor;
				colorText.value = rgb2hex(rgbaColor);
				
				clicked_color_rgb_trans.push(pixels[0]);
				clicked_color_rgb_trans.push(pixels[1]);
				clicked_color_rgb_trans.push(pixels[2]);
			}
		});
	}
	function init_color_picker(){
		var image = new Image();
		image.src = img_url;
		$(image).on('load' , function() {
			context.drawImage(image, 0, 0, fixWidth, fixHeight);
		});
	}
	
	function rgb2hex(rgb){
		 rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
		 return (rgb && rgb.length === 4) ? "#" +
		  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
		  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
		  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
		}
	
//색상 병합
	function init_color_merge(parent, child){
		color_info = new Object();
		color_info.filepath = color_filename;
		color_list = new Array();
		color_list_bf = new Array();
		color_list_af = new Array();
		
		$('.color_box_me').each(function(){
			color_list.push($(this).data('color-before'));
		});
		color_list_bf.push($(parent).data('color-before'));
		color_list_af.push($(child).data('color-before'));
		color_info.color_list = color_list;
		color_info.color_list_bf = color_list_bf;
		color_info.color_list_af = color_list_af;
		var info = JSON.stringify(color_info);
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	    var csrfToken = $("meta[name='_csrf']").attr("content");
	    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
	    var headers = {};

		headers[csrfHeader] = csrfToken;
	    data[csrfParameter] = csrfToken;
		$.ajax({
			url : "/color_trans/color_merge",
			type : "POST",
			async: false,
			data : {
				'info' : info
			},
			beforeSend:function(){
				loadingOn();
			},
			success : function(data) {
				setTimeout(function () {
					put_color(data);
					loadingOff();
				},1000);
			},
			error : function(request, status, error) {
				swal("code:" + request.status + "\n" + "error:" + error);
				loadingOff();
			}
		});
	}
	