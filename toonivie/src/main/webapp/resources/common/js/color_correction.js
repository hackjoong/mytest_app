 $( function() {
    function hexFromRGB(r, g, b) {
      var hex = [
        r.toString( 16 ),
        g.toString( 16 ),
        b.toString( 16 )
      ];
      $.each( hex, function( nr, val ) {
        if ( val.length === 1 ) {
          hex[ nr ] = "0" + val;
        }
      });
      return hex.join( "" ).toUpperCase();
    }
    function refreshSwatch() {
      var red = $( "#red" ).slider( "value" ),
        green = $( "#green" ).slider( "value" ),
        blue = $( "#blue" ).slider( "value" ),
        hex = hexFromRGB( red, green, blue );
      $( "#swatch" ).css( "background-color", "#" + hex );
    }
 
    $( "#red, #green, #blue" ).slider({
      orientation: "horizontal",
      range: "min",
      max: 255,
      value: 127,
      slide: refreshSwatch,
      change: refreshSwatch
    });
    $( "#red" ).slider( "value", 255 );
    $( "#green" ).slider( "value", 255 );
    $( "#blue" ).slider( "value", 255 );
  } );
 
 $( function() {
    function hexFromRGB_psd(r, g, b) {
	      var hex = [
	        r.toString( 16 ),
	        g.toString( 16 ),
	        b.toString( 16 )
	      ];
	      $.each( hex, function( nr, val ) {
	        if ( val.length === 1 ) {
	          hex[ nr ] = "0" + val;
	        }
	      });
	      return hex.join( "" ).toUpperCase();
    }
	 function refreshSwatch_psd() {
	     var red = $( "#red_psd" ).slider( "value" ),
	       green = $( "#green_psd" ).slider( "value" ),
	       blue = $( "#blue_psd" ).slider( "value" ),
	       hex = hexFromRGB_psd( red, green, blue );
	     $( "#swatch" ).css( "background-color", "#" + hex );
	 }
	 $( "#red_psd, #green_psd, #blue_psd" ).slider({
	     orientation: "horizontal",
	     range: "min",
	     max: 255,
	     value: 127,
	     slide: refreshSwatch_psd,
	     change: refreshSwatch_psd
	   });
	   $( "#red_psd" ).slider( "value", 255 );
	   $( "#green_psd" ).slider( "value", 255 );
	   $( "#blue_psd" ).slider( "value", 255 );
	 } );