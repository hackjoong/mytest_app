var cropper_croped_status = 'false';
function cropper_init(canvas_id){
	cropper_croped_status = 'true';
	canvas_id.cropper('destroy');

	canvas_id.cropper({
		  viewMode: 2,
		  //aspectRatio: 16 / 9,
		  zoomable: false,
		  checkOrientation: false,
		  dragMode: 'crop',
		  ready: function () {
			  $(this).cropper('clear');
		  },
		  crop: function(e) {
		    // Output the result data for cropping image.
		  }
		  
	});
}


var scroll_position = 0;
var psd_layer_cut_imgurl;
var psd_layer_cut_width;
var psd_layer_cut_height;

function auto_crop(canvas_id){
	if(psd_img_mode == 'true'){
		
	}else{
		$('#background_img').children().removeClass('h100');
		canvas_id.cropper({
			  viewMode: 3,
			  //aspectRatio: 16 / 9,
			  zoomable: false,
			  //checkOrientation: false,
			  dragMode: 'none',
			  //autoCrop: true,
			  ready: function () {
				var container_wh = $('.canvas-codeBG').cropper('getContainerData');
				  $(this).cropper('setCropBoxData',{
					left:0,
					top:scroll_position,
					width: container_wh.width-1,
					height: 600
			  });
						 //init ajax toblob 
						canvas_id.cropper('getCroppedCanvas').toBlob(function (blob) {	
						  var formData = new FormData();
						  formData.append('imgFile', blob);
						  formData.append('shrinking_ratio_w', shrinking_ratio_w);
						  formData.append('shrinking_ratio_h', shrinking_ratio_h);
						  $.ajax('/uploadWebtoon/webtoon_upload_bg', {
						    method: "POST",
						    enctype: 'multipart/form-data',
						    data: formData,
						    processData: false,
						    contentType: false,
						    async: false,
						    success: function (data) {
						      cropper_disable(canvas_id);
						      $('#background_img').children().addClass('h100');
						      psd_layer_cut_imgurl = data.filepath;
						      psd_layer_cut_width = data.width;
						      psd_layer_cut_height = data.height;
						    },
						    error: function () {
						    }
						  });
						});
		    },
			  crop: function(e) {
			    // Output the result data for cropping image. 
			  }
		});
		
	}
}

function cropper_disable(canvas_id){

	try{
		canvas_id.cropper('destroy');
	}catch(err){
	}
	cropper_croped_status = false;
}

var numb_children = $('#layered_img').children().length + 1;

/*open cropped image by project_name and append by KH 2018-02-22*/
function cropped_image_append(selected_layer, value){
	var randomId = new Date().getTime();
	var numb_children_sub = $("#layered_img_cropped").find('.' + selected_layer).length + 1;
	var image_psd_sub_num = selected_layer;
	var image_psd_sub_num_count = image_psd_sub_num.substring(5, 7);
	//layered_img_cropped
	var append_str =
				"<!-- 상단 분리된 이미지 영역 -->"+
				"<div class='clxk saved-image-item draggg append_dragDiv"+numb_children+" "+selected_layer+"  sii_px di appended' data-target='wrap_cropped"+numb_children+"' data-index='"+numb_children+"' data-selection='false' data-cropper_cropped='true'>"+
				"	<div class='saved-image-item-image re append_itemNum"+numb_children+"' style='background-image: url()'></div>"+
				"	<div class='w100 fl'>"+
				"		<span class='w100 fl tc re_saved_name'>Image "+image_psd_sub_num_count+"-"+numb_children_sub+" </span>"+
				"	</div>"+
				"</div>";
	
	$('#layered_img_cropped').append(append_str);
	$(".append_itemNum"+numb_children+"").css("background-image","url('')");
	$(".append_itemNum"+numb_children+"").css("background-image","url('"+value.filepath+"?v="+randomId+"')");
	$(".append_itemNum"+numb_children+"").attr("data-url",""+value.filepath);
	$(".append_dragDiv"+numb_children+"").attr("data-width",value.width);
	$(".append_dragDiv"+numb_children+"").attr("data-height",value.height);
	$(".append_dragDiv"+numb_children+"").attr("data-image",""+value.filepath);
	numb_children++;
	/*append_dragDiv*/
	
	$(".draggg").draggable({ //.append_dragDiv1
		disabled : false,
		helper : "clone",
		revert : "true",
		opacity : "0.5",
	});
	
   
	
}