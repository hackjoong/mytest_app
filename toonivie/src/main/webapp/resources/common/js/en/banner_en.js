$("#banner-info").change(function() {
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            $('.p>h4').hide();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(function() {
    var fileTarget = $('.bannerbox .upload-hidden');

    fileTarget.on('change',
        function() { // 값이 변경되면
            if (window.FileReader) { // modern browser
                var filename = $(this)[0].files[0].name;
            } else { // old IE
                var filename = $(this).val().split('/').pop().split(
                    '\\').pop(); // 파일명만 추출
            }

            // 추출한 파일명 삽입
            $(this).siblings('.upload-baner').val(filename);
            var fileNm = $(".upload-baner").val();

            if (fileNm != "") {

                var ext = fileNm.slice(fileNm.lastIndexOf(".") + 1)
                    .toLowerCase();

                if (!(ext == "gif" || ext == "jpg" || ext == "png")) {
                    alert("Only files (.jpg, .png, .gif ) an be uploaded.");
                    $(this).siblings('.upload-baner').val('');
                    return false;
                }

            }
        });
});

var wfilename = "";
var wfilename_origin = ""
var wUploaded = false; //psd file upload check
//Webtoon Movie - Single File Upload Code
//2017-04-11, shkwak
//2017-07-19, RE Use
function upload_singlefile_1() { ///upload_singlefile
    if ($('.upload-baner').val() == '') {
        alert('Please select a file.');
        return false;
    } else {
        $('#upload_banner')
            .ajaxForm({
                url: "/banner/upload?${_csrf.parameterName}=${_csrf.token}",
                method: "POST",
                enctype: "multipart/form-data", // 여기에 url과 enctype은 꼭 지정해주어야 하는 부분이며 multipart로 지정해주지 않으면 controller로 파일을 보낼 수 없음
                success: function(result) {
                    console.log(result); //.resultUrl);
                    wfilename = result; //.resultUrl;
                    alert("The "+wfilename_origin + " file was saved to the server with the " + wfilename
							+ " file name.");
                    wUploaded = true;
                    bannerList();

                }
            });
    }
    // 여기까지는 ajax와 같다. 하지만 아래의 submit명령을 추가하지 않으면 백날 실행해봤자 액션이 실행되지 않는다.
    $("#upload_banner").submit();

}









function bDelete(sno) {
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = {};
    var headers = {};
    data[csrfParameter] = csrfToken;
    headers[csrfHeader] = csrfToken;
    var banner_img = $('#B_FILE_'+sno).val();
    console.log(banner_img);
    

    var true_delete = confirm("Are you sure you want to delete it?");
    if (true_delete == true) {
        $.ajax({
            url: '/banner/delete/' + sno,
            type: 'post',
            data:{
            	'banner':banner_img
            },
            success: function(data) {

                if (data == 1) {
                    alert("Deleted.");
                    bannerList();
                }

            },
            error: function(data) {
                console.log(data);
            }
        })
    } else {
        return false;
    }
}

$('.resetImg').click(function() {
    $('#blah').removeAttr('src');
    $('.p>h4').show();
})

