var grabcutUrl;
var GmoX=0;
var GmoY=0;
var GbeX=0;
var GbeY=0;
var GafX=0;
var GafY=0;
var canvasFlag=0;
var bgListX=new Array();
var bgListY=new Array();
var fgListX=new Array();
var fgListY=new Array();
var rgb_ori = [];
var sailencyCut = 'false';
var fg_mask;
var bg_mask;
var img_resize_rate_h;
var img_resize_rate_w;
function fileUploadGrabcutImg(){ //이미지 따기 클릭 이벤트
	$('#uploadGrabcutImgFrom').ajaxForm({
		url : '/upload/uploadImg',
		method : "POST",
		enctype : "multipart/form-data",
		async : false,
		success : function(result){		
			$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
//			$(".wrap10").addClass("dn");
			$(".wrap_BG").addClass("dn");
			$(".saved-image-item").each(function(){
				$(this).attr("data-image","");
				$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			});												
			$(".save-bg").attr("data-image","");
			$(".canvas-append-zone").css("z-index","99");
			$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			listNum=1;
			$("#canvas-after").css("background-image","url('')");
			$("#canvas-before").data("img_url","/resources/cvimgs/"+result.resultUrl);
//			$("#canvas-before").attr("width",result.width);
//			$("#canvas-before").attr("height",result.height);
			//calculate resize img ratio by KH 180409
			$("#canvas-before").attr("width",960);
			$("#canvas-before").attr("height",540);
			img_resize_rate_w = result.width / 960;
			img_resize_rate_h = result.height / 540;
			
			$("#canvas-before").css("background-size","960px 540px");
			$(".canvas-before").removeClass("dn");			
			//$("#canvas-before").css("background-image","url("+"/resources/cvimgs/"+result.resultUrl+")");
			img_url = "/resources/cvimgs/"+result.resultUrl;
			init_color_picker();
			
			$(".second_saved_img").addClass("dn");
			//이미지분리종류
			$("#cutModeBtn1").attr("src","/resources/image/v2.0/cutModeBtn1_click.png");
			$("#cutModeBtn2").attr("src","/resources/image/v2.0/cutModeBtn2.png");
			//배경색상채우기
			$("#fill_black").attr("src","/resources/image/v2.0/fill_black_click.png");
			$("#fill_white").attr("src","/resources/image/v2.0/fill_white.png");
			$("#fill_surrounding").attr("src","/resources/image/v2.0/fill_surrounding.png");
			$("#grabcut_color_picker").attr("src","/resources/image/v2.0/grabcut_color_picker.png");
			
			//fg_bg 마스킹 init by KH 2018-02-20
			$("#canvas-fg").attr("width",result.width);
			$("#canvas-fg").attr("height",result.height);
			$("#canvas-bg").attr("width",result.width);
			$("#canvas-bg").attr("height",result.height);
			$("#canvas-fg-bw").attr("width",result.width);
			$("#canvas-fg-bw").attr("height",result.height);
			$("#canvas-bg-bw").attr("width",result.width);
			$("#canvas-bg-bw").attr("height",result.height);
			$(".canvas-fg").removeClass("dn");	
			$(".canvas-bg").removeClass("dn");	
			
			//get color palette by KH 2018-03-05
			color_filename = result.resultUrl;
			put_color(result.color_palette);
			
			//ROI init by KH 18-03-12
			$('#ROI_select').removeClass('disabled');
			if(ROI_status == 'true' || ROI_status == 'using'){
				ROI_setting(); //ROI reset by KH 18-03-19
			}
			
			$(".go-edit").addClass("dn")
			$(".go-next").removeClass("dn");
			$(".canvas-wrap").removeClass("dn");			
			grabcutUrl=result.resultUrl;
			//sailencycut 여부 확인 by KH 2018-02-12
/*			var go_saliency = confirm("사일런시 컷을 하시겠습니까?");
			if(go_saliency == true){
				saliencyCut = 'true';
			}else{
				saliencyCut = 'false'; 
			}*/

			/*psd_c = document.getElementById("canvas_bg");
		    psd_ctx = psd_c.getContext("2d");
		    psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);*/
		    $(".second_saved_img").addClass("dn");
		    $("#layered_img_cropped").children().remove(); // 튀나온 박스 썸네일
		    $(".cropped_remove").remove(); // 드랍된 분리된 이미지
		    
		    $(".use-image-canvas-wrap").css("top",'25%');
		    
		    
		    alert("select an area to edit");
			$(".canvas-append-zone").removeClass('dn');
			psd_img_mode='false';
			itemList_Length=9; //9로 지정하면 안됨! 수정 필요 by shkwak, 2018-03-13

			$('#cut_img_ori').html('이미지 다시 따기');
			$('.saved-image-item').attr('title', '');
			//data값 초기화
			$('.saved-image-item').data('image','');
			set_filling_color(0);
			setMode(0);
			
			//psh add 20180409 이미지 컷 초기화
			cropper_croped_status_wt = 'false'; //크롭퍼 종료
			$(".re_img_upload_preview").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
			$("#wt_cut_save").text('Image Separate').addClass('disabled'); //이미지 분리 버튼
			$("#wt_whole_cropArea").cropper('destroy').removeAttr('src'); //크롭퍼 비활성화
			$('#wt_cut_saveName').val("");	
			$(".imgPreview_").show();
			$('#wt_cut_uploadCount').text("");
			$(".wtcut_box").children(".wt_cut_appendImg").remove();
			$("#wt_cut_save_all").addClass('disabled');
			/*$(".side_img_cut_preview_box").remove();*/
			//psh add 20180409 psd 초기화
			$("#canvas_bg").data("image","").css("background-image","url('')").addClass("dn"); //스크롤되는 배경
			$(".psd_LPC_img").css("background",""); //미리보기
			$(".psd_LPC_img").css("height","0");
			$("#upload_psd").attr("src","/resources/image/toolicon_1.png"); // psd 추가 이미지
			
			//for mask grabcut by KH 180426
			ori_background = result.resultUrl;

		}
	});
	$("#uploadGrabcutImgFrom").submit();
	getColorValueAvr();
	savedBackImg = false; //배경 저장 여부 초기화, add by shkak, 2017-11-09
}





function grabcutExecute(){ //매우 중요	
	bgListX.push(1);
    bgListY.push(1);
    fgListX.push(1);
    fgListY.push(1);
    clicked_color_rgb.push(1);
    var randomId = new Date().getTime();
    var data = {};
    data["GbeX"] = Math.round(GbeX * img_resize_rate_w);
    data["GbeY"] = Math.round(GbeY * img_resize_rate_h);
    data["GafX"] = Math.round(GafX * img_resize_rate_w);
    data["GafY"] = Math.round(GafY * img_resize_rate_h);
    data["bgListX"] = bgListX;
    data["bgListY"] = bgListY;
    data["fgListX"] = fgListX;
    data["fgListY"] = fgListY;
    data["Gmode"]=Gmode;
    data["imgUrl"]=grabcutUrl;
    data["filling_type"]=filling_type; //add by KH, 2017-12-21
    data['clicked_color_rgb']=clicked_color_rgb; //add by KH, 2018-03-16
    data["fg_mask"]=fg_mask;
    data["bg_mask"]=bg_mask;
    data["blur_status"]=blur_status;
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};

    console.log(data);
   /* {GbeX: 297, GbeY: 224, GafX: 507, GafY: 434, bgListX: Array(5), …}
    GafX    :    507
    GafY    :    434
    GbeX    :    297
    GbeY    :    224
    Gmode   :    0
    bgListX :    (5) [1, 1, 1, 1, 1]
    bgListY :    (5) [1, 1, 1, 1, 1]
    fgListX :    (5) [1, 1, 1, 1, 1]
    fgListY :    (5) [1, 1, 1, 1, 1]
    imgUrl  :    "20171113163913.jpg"*/
    /*if(sailencyCut == 'true'){
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
        
    	$.ajax({
    		url : "/sailencyCut/sailencyCutExe",
    		dataType : "json",
    		type : "POST",
    		async: false,
    		data : data,
    		beforeSend:function(){
    			loadingOn();
    		},
    		success : function(data) {
    			setTimeout(function () {
//    				sailencyCut = 'false';
    				popupOn($(".canvas-grapcut-modal"));
    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",data.width);
    				$("#canvas-after").attr("height",data.height);
    				$(".canvas-footer").removeClass("dn");
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultImg="";
    				resultImg=data.resultImg;
    				//alert(resultHeight+","+resultWidth);
    				backImg=data.backGroundFile;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				loadingOff();
    			},1000);
    		},
    		error : function(request, status, error) {
    			alert("code:" + request.status + "\n" + "error:" + error);
    			loadingOff();
    		}
    	});
    }else{*/
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
    	$.ajax({
    		url : "/grabcut/grabcutExe",
    		dataType : "json",
    		type : "POST",
    		async:false,
    		data : data,
    		beforeSend:function(){
    			loadingOn();
    		},
    		success : function(data) {
    			//get color palette by KH 2018-03-20
    			color_filename = data.backGroundFile.replace('/resources/cvimgs/','');
    			//color picker change background by KH 18-03-19
    			img_url = data.backGroundFile;
    			init_color_picker();
    			if(ROI_status == 'true' || ROI_status == 'using'){
    				ROI_setting(); //ROI reset by KH 18-03-19
    			}
    			setTimeout(function () {
    				popupOn($(".canvas-grapcut-modal"));
    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",data.width);
    				$("#canvas-after").attr("height",data.height);
    				$(".canvas-footer").removeClass("dn");
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultImg="";
    				resultImg=data.resultImg;
    				//alert(resultHeight+","+resultWidth);
    				backImg=data.backGroundFile;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				loadingOff();
    			},1000);
    		},
    		error : function(request, status, error) {
    			alert("code:" + request.status + "\n" + "error:" + error);
    			loadingOff();
    		}
    	});
//    }
    
}

var resultWidth;
var resultHeight;
var backGroundWidth;
var backGroundHeight;
var resultImg;
var backImg;
var listNum=1;

var fixed_width=1040;
var fixed_height=0;
var bg_ratio=0;
var shrinking_ratio=0;
var itemList_Length=9;
function makeTopLayers(data){ //for PSD 이미지분리 결과 DP
	var itemList = data.result;
	$(".saved-image-item").each(function(){
		$(this).attr("data-image","");
		$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
	});	
	$(".save-bg").attr("data-image","");
	$(".canvas-append-zone").css("z-index","99");
	$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
	listNum=1;
	
	itemList_Length = itemList.length;
	$('#layered_img').children('.appended').remove();
	//$('.canvas-codeBG').remove();
	if(itemList_Length>9){
		var add_length = Math.abs(9-itemList_Length);
		var numb = 10;
		for(var i=0;i<add_length;i++){
			var append_str =
				"<!-- 상단 분리된 이미지 영역 -->"+
				"<div class='psd_pre clxk saved-image-item draggg dragDiv"+numb+" img_selection sii_px di appended' data-target='wrap"+numb+"' data-index='"+numb+"' data-selection='false' title='"+data.layer_name[numb-1]+"'>"+
				"	<div class='saved-image-item-image re itemNum"+numb+"' style='background-image: url()'></div>"+
				"	<div class='w100 fl'>"+
				"		<span class='w100 fl tc' >"+data.layer_name[numb]+"</span>"+
				"	</div>"+
				"</div>";
			$('#layered_img').append(append_str);
			width_auto();
			numb++;
			 
		}
		
	}
	$.each(itemList,function(index,value){
		var randomId = new Date().getTime();
		//$("#psd-progressbar").progressbar({
		//      value: 10*index
		//});
		$(".itemNum"+listNum+"").css("background-image","url('')");
		$(".itemNum"+listNum+"").css("background-image","url('/"+value.imgUrl+"?v="+randomId+"')");
		$(".itemNum"+listNum+"").next().children().html(data.layer_name[index]);
		$(".dragDiv"+listNum+"").attr('title',data.layer_name[index]);
//
		$(".itemNum"+listNum+"").data("url","/"+value.imgUrl);
		$(".dragDiv"+listNum+"").data("width",value.width);
		$(".dragDiv"+listNum+"").data("height",value.height);
		$(".dragDiv"+listNum+"").data("image","/"+value.imgUrl);
/*		$(".itemNum"+listNum+"").attr("data-url","/"+value.imgUrl);
		$(".dragDiv"+listNum+"").attr("data-width",value.width);
		$(".dragDiv"+listNum+"").attr("data-height",value.height);
		$(".dragDiv"+listNum+"").attr("data-image","/"+value.imgUrl);*/
		listNum++;
	});
	
	//popupOff($(".psd-progress-modal"));	
}

/* 배경 저장, PSD 배경 저장 클릭 이벤트 */
$(".cut-img-add").click(function(){
	if($(this).attr('id')=="psd_bg_selection"){
		alert("배경으로 지정할 이미지를 선택 해주세요.");
		$('.img_selection').data('selection', 'true');
		$(".canvas-append-zone").removeClass('dn');
		/*$("#background_img").removeClass("dn");
		$("#second_saved_img").removeClass("dn");*/
		$(".wt_whole_img").hide();
		//psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);
		psd_c = document.getElementById("canvas_bg");
	    psd_ctx = psd_c.getContext("2d");
	    psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);
	}else{
		if($('.saved-image-item').data('image') == ''){
			//save-bg as object by KH 180419
			if(filling_type == 2){
				//alert('You can not set the background color to Fill Color.');
				return;
			}else{
				save_bg_as_object();
			}
		}
			$(".canvas-wrap").addClass("dn");
	//	$(".dragDiv10").attr('data-image',backImg);
			$(".dragDiv_BG").attr('data-image',backImg);
			$(".save-bg").css("background-image","url('"+backImg+"')");
	//	$(".wrap10").removeClass("dn");
			$(".wrap_BG").removeClass("dn");
/*			$(".use-image-canvas-wrap").css("width",backGroundWidth);
			$(".use-image-canvas-wrap").css("height",backGroundHeight);*/
			$(".use-image-canvas-wrap").css("width",960);
			$(".use-image-canvas-wrap").css("height",540);
			/*	$(".canvas-code10").css("width",backGroundWidth);
		$(".canvas-code10").css("height",backGroundHeight);
		$(".canvas-code10").css("background-image","url('"+backImg+"')");
		$(".canvas-code10").attr("data-image",backImg);*/
/*			$(".canvas-codeBG").css("width",backGroundWidth);
			$(".canvas-codeBG").css("height",backGroundHeight);*/
			$(".canvas-codeBG").css("width",960);
			$(".canvas-codeBG").css("height",540);
			$(".canvas-codeBG").css("background-image","url('"+backImg+"')");
			$(".canvas-codeBG").attr("data-image",backImg);
			savedBackImg = true;
	}
	
});

/*PSD 배경 저장 클릭 이벤트 (선택) 
$(".img_selection").click(function(){
	var me = $(this);
	if($('.img_selection').data('selection')=='true'){
		backImg = me.data('image');
		backGroundWidth = me.data('width');
		backGroundHeight = me.data('height');
		
		shrinking_ratio=fixed_width/backGroundWidth;
		bg_ratio=backGroundWidth/backGroundHeight;
		backGroundHeight=fixed_width/bg_ratio;
		backGroundWidth=fixed_width;
			
		$(".canvas-wrap").addClass("dn");
		$(".dragDiv_BG").attr('data-image',backImg);
		$(".save-bg").css("background-image","url('"+backImg+"')");
		$(".wrap_BG").removeClass("dn");
		
		$(".use-image-canvas-wrap").css("width",backGroundWidth);
		$(".use-image-canvas-wrap").css("height",backGroundHeight);
		$(".canvas-codeBG").css("width",backGroundWidth);
		$(".canvas-codeBG").css("height",backGroundHeight);
		$(".canvas-codeBG").css("background-image","url('"+backImg+"')");
		$(".canvas-codeBG").attr("data-image",backImg);
		savedBackImg = true;
		$('.img_selection').data('selection', 'false');
	}
});*/
var psd_c;
var psd_ctx;
$('#layered_img').on('click', '.img_selection',function() {
	var me = $(this);
	if($('.img_selection').data('selection')=='true'){
		backImg = me.data('image');
		backGroundWidth = me.data('width');
		backGroundHeight = me.data('height');
		console.log("grabcut.js: 324 img_selection");
		shrinking_ratio=fixed_width/backGroundWidth;
		bg_ratio=backGroundWidth/backGroundHeight;
		backGroundHeight=Math.round(fixed_width/bg_ratio);
		backGroundWidth=fixed_width;
		//웹툰무비 가로해상도 유지 CANVAS 감싸고 있는 DIV 태그 화면 맞춤
		//Resolution fixed by KH 2018-02-26
		//backGroundHeight='540';
			
		$(".canvas-wrap").addClass("dn");
		$(".dragDiv_BG").attr('data-image',backImg);
		$(".save-bg").css("background-image","url('"+backImg+"')");
		$(".wrap_BG").removeClass("dn");

/*		//for setting color by KH 2018-02-08
		grabcutUrl = backImg;
		getColorValueAvr();*/
		
/*		$(".use-image-canvas-wrap").css("width",backGroundWidth);
//		$(".use-image-canvas-wrap").css("height",backGroundHeight);
		$(".use-image-canvas-wrap").css("height",690);
		$(".use-image-canvas-wrap").css('overflow','scroll');*/
		
		$(".use-image-canvas-wrap").css("top",'33%');
		$(".second_saved_img").removeClass("dn");
		$(".use-image-canvas-header").remove();
		
		$("#background_img").css("width",backGroundWidth);
//		$("#background_img").css("height",backGroundHeight);
		$("#background_img").css("height",540);
		//problem with overflow by KH 2018-02-27
		$("#background_img").css('overflow','scroll');
		
		$(".canvas-codeBG").css("width",backGroundWidth);
		$(".canvas-codeBG").css("height",backGroundHeight);
		//$(".canvas-codeBG").css("background-image","url('"+backImg+"')");
		$(".canvas-codeBG").attr("image",backImg);
		savedBackImg = true;
		
/*	    psd_c = document.getElementById("canvas_bg");
	    psd_ctx = psd_c.getContext("2d");*/
	    
/*	    var c = document.getElementById("canvas_bg");
	    var ctx = c.getContext("2d");
*/	    var image = new Image();  
	    image.onload = drawImageActualSize; // draw when image has loaded
	    image.src = backImg;

		function drawImageActualSize() {
			/*			psd_c.width = backGroundWidth;
			psd_c.height = backGroundHeight;
			//ctx.drawImage(this, 0, 0);
			psd_ctx.drawImage(this, 0, 0, backGroundWidth, backGroundHeight);
*/			canvas.width = backGroundWidth;
			canvas.height = backGroundHeight;
		   //ctx.drawImage(this, 0, 0);
			context.drawImage(this, 0, 0, 960, 540);
/*		   c.width = backGroundWidth;
		   c.height = backGroundHeight;
		   //ctx.drawImage(this, 0, 0);
		   ctx.drawImage(this, 0, 0, backGroundWidth, backGroundHeight);
*/		}
		btop_start_position = $(".canvas-codeBG").offset().top;
		$('.img_selection').data('selection', 'false');
	}
});

function saveResultToList1(){
	var randomId = new Date().getTime();
	$(".itemNum"+listNum+"").css("background-image","url('')");
	$(".itemNum"+listNum+"").css("background-image","url('"+resultImg+"?v="+randomId+"')");
	$("#canvas-before").css("background-image","url('')");
	$("#canvas-before").css("background-image","url('"+backImg+"?v="+randomId+"')");
/*	$(".itemNum"+listNum+"").attr("data-url",resultImg);
	$(".dragDiv"+listNum+"").attr("data-width",resultWidth);
	$(".dragDiv"+listNum+"").attr("data-height",resultHeight);
	$(".dragDiv"+listNum+"").attr("data-image",resultImg);*/
	$(".itemNum"+listNum+"").data("url",resultImg);
	$(".dragDiv"+listNum+"").data("width",resultWidth);
	$(".dragDiv"+listNum+"").data("height",resultHeight);
	$(".dragDiv"+listNum+"").data("image",resultImg);
	context.clearRect(0,0,1000,1000);
	listNum++;
	popupOff($(".canvas-grapcut-modal"));
	
	
}

function setMode(modeInteger){
	Gmode=modeInteger;
	alert(mode);
//	if(Gmode==0){
//	    $.ajax({
//	        url : "/grabcut/init",
//	        dataType : "json",
//	        type : "POST",
//	        success : function(data) {
//	        },
//	        error : function(request, status, error) {
//	            alert("code:" + request.status + "\n" + "error:" + error);
//	        }
//	    });
//	}
}


/* 색상 보정(RGB) */
function getColorValueAvr(){
	var data={};
	data["imgUrl"]=grabcutUrl;
	data["psd_img_mode"]=psd_img_mode;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    console.log(data);
	$.ajax({
        url : "/changeColor/getColorAvg",
        dataType : "json",
        type : "POST",
        data : data,
        success : function(data) {
        	//alert("red : "+data.red+" green : "+data.green+" blue : "+data.blue);
        	if(psd_img_mode == 'true'){
            	$( "#red_psd" ).slider( "value", data.red );
                $( "#green_psd" ).slider( "value", data.green );
                $( "#blue_psd" ).slider( "value", data.blue );
                if(psd_color_init != "true"){
                	//적용된 이미지 변경
                	$('.canvas-code'+image_data_index).css('background-image', "url('"+grabcutUrl+"')");
                	$('.canvas-code'+image_data_index).data('image', "/resources/psdimgs/"+data.resultImg_Name);
                	//상단 메뉴 이미지 변경
                	$('.itemNum'+image_data_index).css('background-image', "url('"+grabcutUrl+"')");
                	$('.itemNum'+image_data_index).data('url', +grabcutUrl);
                	$('.dragDiv'+image_data_index).data('image', grabcutUrl);
                }
        	}else{
            	$( "#red" ).slider( "value", data.red );
                $( "#green" ).slider( "value", data.green );
                $( "#blue" ).slider( "value", data.blue );
            	$('#go_back').removeClass('disabled');//add by KH 2017-12-18
            	$('#go_back').prop("disabled", false);//add by KH 2017-12-18
            	$('#canvas-before').css('background-image', "url(/resources/cvimgs/"+grabcutUrl+")");
        	}
            rgb_ori.push(data.red);//add by KH 2017-12-18
            rgb_ori.push(data.green);//add by KH 2017-12-18
            rgb_ori.push(data.blue);//add by KH 2017-12-18

        	
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    });
}
//'save_yn', 'rgb_ori' add by KH 2017-12-18
function changeColorExe(save_yn, rgb_ori1){
	var data={};
	if(psd_img_mode == 'true'){
		data["red"]=$( "#red_psd" ).slider("value");
		data["green"]=$( "#green_psd" ).slider("value");
		data["blue"]=$( "#blue_psd" ).slider("value");
	}else{
		if(rgb_ori1 != null){
			/*		data["red"]= rgb_ori1[0];
			data["green"]= rgb_ori1[1];
			data["blue"]= rgb_ori1[2];
	    	$( "#red" ).slider( "value", rgb_ori1[0] );
	        $( "#green" ).slider( "value",rgb_ori1[1] );
	        $( "#blue" ).slider( "value", rgb_ori1[2] );
			 */	}else{
				 data["red"]=$( "#red" ).slider("value");
				 data["green"]=$( "#green" ).slider("value");
				 data["blue"]=$( "#blue" ).slider("value");
			 }
	}
	
//	alert($( "#red" ).slider("value"));
	
	/*data["red"]=$( "#red" ).slider("value");
	data["green"]=$( "#green" ).slider("value");
	data["blue"]=$( "#blue" ).slider("value");*/
	data["imgUrl"]=grabcutUrl;
	data["save_yn"]=save_yn;
	
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    if(save_yn){//add by KH 2017-12-18
    	$('#go_back').addClass('disabled');
    	$('#go_back').prop("disabled", true);
    }else{
    	context.clearRect(0, 0, canvas.width, canvas.height);
    }
    
    
	$.ajax({
        url : "/changeColor/changeExe",
        dataType : "json",
        type : "POST",
        data : data,
        success : function(data) {
        	var randomId = new Date().getTime();
        	if(psd_img_mode == 'true'){
        		//적용된 이미지 변경
        		$('.canvas-code'+image_data_index).css('background-image', "url('/resources/psdimgs/new_"+data.resultImg_Name+"?v="+randomId+"')");
        		$('.canvas-code'+image_data_index).data('image', "/resources/psdimgs/new_"+data.resultImg_Name);
        		//상단 메뉴 이미지 변경
        		$('.itemNum'+image_data_index).css('background-image', "url('/resources/psdimgs/new_"+data.resultImg_Name+"?v="+randomId+"')");
        		$('.itemNum'+image_data_index).data('url', "/resources/psdimgs/new_"+data.resultImg_Name);
        		$('.dragDiv'+image_data_index).data('image', "/resources/psdimgs/new_"+data.resultImg_Name);
        	}else{
        		$("#canvas-before").css("background-image","url('')");
        		$("#canvas-before").css("background-image","url('"+data.resultImg+"?v="+randomId+"')");      
        		console.log(data.resultImg);
        	}
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    });
}
var psd_color_change = 'false';
var psd_color_init = 'false;'
var image_data_index = null;
var return_color = 'false';
var image_url;
$('#change_psd_color').click(function() {
	if(psd_color_change == 'true'){
		psd_color_init = 'false';
		psd_color_change = 'false';
		changeColorExe('psd');
//		$('.canvas-code'+image_data_index).css('background-image', "url(resources/psdimgs/new_"+image_url+".png)");
	}else{
		alert('적용할 이미지를 선택해 주세요');
		psd_color_init = 'true';
	}
});

$('#layered_img').on('click', '.saved-image-item',function() { 
	image_data_index = $(this).data('index');
	grabcutUrl = $(this).data('image');
	if(psd_color_init == 'true'){
		getColorValueAvr();
		alert('RGB 색상 설정후 적용 버튼을 눌러주세요');
		psd_color_change = 'true';
	}else if(return_color == 'true'){
		grabcutUrl = $(this).data('image');
		grabcutUrl = grabcutUrl.replace('new_','');
		getColorValueAvr();
	}
});

$('#return_psd_color').click(function(){
	alert('적용할 이미지를 선택해 주세요');
	return_color = 'true';
});



function save_bg_as_object(){
    clicked_color_rgb.push(1);
    var randomId = new Date().getTime();
    var data = {};
    data["imgUrl"]=grabcutUrl;
    data["filling_type"]=filling_type; 
    data['clicked_color_rgb']=clicked_color_rgb;
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};

    console.log(data);
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
    	$.ajax({
    		url : "/grabcut/saveBGasOBJ",
    		dataType : "json",
    		type : "POST",
    		async:false,
    		data : data,
    		beforeSend:function(){
    			loadingOn();
    		},
    		success : function(data) {
    			setTimeout(function () {
    				popupOn($(".canvas-grapcut-modal"));
    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",data.width);
    				$("#canvas-after").attr("height",data.height);
    				$(".canvas-footer").removeClass("dn");
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultImg="";
    				resultImg=data.resultImg;
    				//alert(resultHeight+","+resultWidth);
    				backImg=data.backGroundFile;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				$(".canvas-wrap").addClass("dn");
    				
					$(".canvas-wrap").addClass("dn");
					$(".dragDiv_BG").attr('data-image',backImg);
					$(".save-bg").css("background-image","url('"+backImg+"')");
					$(".wrap_BG").removeClass("dn");
					$(".use-image-canvas-wrap").css("width",960);
					$(".use-image-canvas-wrap").css("height",540);
					$(".canvas-codeBG").css("width",960);
					$(".canvas-codeBG").css("height",540);
					$(".canvas-codeBG").css("background-image","url('"+backImg+"')");
					$(".canvas-codeBG").attr("data-image",backImg);
					savedBackImg = true;
					loadingOff();
			},1000);
    		},
    		error : function(request, status, error) {
    			alert("code:" + request.status + "\n" + "error:" + error);
    			loadingOff();
    		}
    	});
}