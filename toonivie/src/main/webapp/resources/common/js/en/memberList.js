$(document).ready(function() {
    $("#id").focus();
    
    $('.pwform').hide();
    

	$('.checkbox_pw').click(function(){
		if($('.checkbox_pw').prop('checked')==false){
		    $('.pwform').show();
		}
		else{
			$('.pwform').hide();
		}
	})
    
});

$(".confirm_pw").keyup(function() {
    var pw = $("[name = pw]").val();
    var re_pw = $("[name=confirm]").val();
    if (pw != re_pw || pw == "" || re_pw == "") {
        $(".check_pw_span").css('color','red').css('width','195px').css('float','left');
        $(".check_pw_span").text("Passwords do not match.");
    } else {
        $(".check_pw_span").css("color", "blue").css('width','150px').css('float','left');
        $(".check_pw_span").text("Passwords match.");
    }
});

function modify_member() {
    var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
    var pwReg = /([0-9A-Za-z]{4,11})/g;
    var nameReg = /([가-힣A-Za-z]{2,4})/g;
    var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
    var idValue = $("[name = id]").val();
    var pwValue = $("[name = pw]").val();
    var nameValue = $("[name = name]").val();
    var emailValue = $("[name = email]").val();
    var confirmValue = $("[name = confirm]").val();
 
    if($('.checkbox_pw').prop('checked')==false){
    	if ( idValue == "") {
            alert("Please check your ID.");
            return false;
        } else if (emailReg.test(emailValue) == false || emailValue == "") {
            alert("Please check your e-mail.");
            return false;
        } else if (nameReg.test(nameValue) == false || nameValue == "") {
            alert("Please check your name.");
            return false;
        } else if (pwValue != confirmValue || pwReg.test(pwValue) == false ||
            pwValue == "") {
            alert("Please check your password.");
            return false;
        } else {
            update();
        }
	}
	else{
		 if (idValue == "") {
		        alert("Please check your ID.");
		        return false;
		    } else if (emailReg.test(emailValue) == false || emailValue == "") {
		        alert("Please check your e-mail.");
		        return false;
		    } else if (nameReg.test(nameValue) == false || nameValue == "") {
		        alert("Please check your name.");
		        return false;
		    } else {
		        update();
		    }
	}
   
}

var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

function mDelete(userSeq) {
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = {};
    var headers = {};
    data[csrfParameter] = csrfToken;
    headers[csrfHeader] = csrfToken;

    var member_delete = confirm("Are you sure you want to delete it?");
    if (member_delete == true) {
        $.ajax({
            url: '/member/delete/' + userSeq,
            type: 'post',
            success: function(data) {

                if (data == 1) {
                    alert("Deleted.");
                    home();
                }

            },
            error: function(data) {
                console.log(data);
            }
        })
    }
    else{
    	return false;
    	home();
    }
}

function memberInfo_en(userSeq) {
    console.log(userSeq);
    $('.info-modify').css('display', 'none');
    $('.logins').css('display', 'block');
    var a = $('.id_' + userSeq).text();
    $("[name=id]").val(a);
    var b = $('.name_' + userSeq).text();
    $("[name=name]").val(b);
    var c = $('.email_' + userSeq).text();
    $("[name=email]").val(c);
    $("[name=userSeq2]").val(userSeq);
    var d = $('.auth_'+userSeq).text();
    var fu = $.trim(d);
 
    
    if(fu=="User"){
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_user_en').attr("checked","checked");

    }else if(fu=="Admin"){
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_admin_en').attr("checked","checked");
    }

}

function close_info(userSeq) {
    var a = confirm("Are you sure you want to cancel the contents?");
    if (a == true) {
        $('.logins').css("display", "none");
        $('.info-modify').css('display', 'block');
    }
}



function update() {
    var id = $("[name=id]").val();
    var name = $("[name=name]").val();
    var email = $("[name=email]").val();
    var pw = $("[name=pw]").val();
    var no = $("[name=userSeq2]").val();
    var check = $('input:checkbox[name="noChangedPW_en"]').is(":checked");
    var auth = $(':radio[name="auth"]:checked').val();
    //console.log(id+","+name+","+email+","+pw+","+no+","+check+","+auth);

    $.ajax({
        url: "/member/update_en",
        type: "POST",
        data: {
            'id': id,
            'name': name,
            'email': email,
            'pw': pw,
            'no': no,
            'auth': auth,
            'noChangedPW_en':check
        },
        dataType: 'json',
        success: function(data) {
            if (data == 1) {
                alert("It has been modified.");
                home();
            }
        },
        error: function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    });


}