$(document).ready(function() {
	var fileTarget = $('.filebox .upload-hidden');
	$('#change_no').hide();

	$('#checkbox').click(function(){
		if($('#checkbox').prop('checked')==false){
		    $('.filebox').show();
		}
		else{
			$('.filebox').hide();
		}
	})
	

	
	

	fileTarget.on('change', function() { // 값이 변경되면
		if (window.FileReader) { // modern browser
			var filename = $(this)[0].files[0].name;
		} else { // old IE
			var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																				// 추출
		}

		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename);
	});

	// 업데이트시 파일 변경 안할떄
	$('.no-change').css('display', 'none');
});

var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	/* $(".wtr").hide(); */

});

function wDelete(tno) {
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var data = {};
	var headers = {};
	data[csrfParameter] = csrfToken;
	headers[csrfHeader] = csrfToken;

	var webtoon_delete = confirm("Are you sure you want to delete it?");
	if (webtoon_delete == true) {

		$.ajax({
			url : '/webtoon/delete/' + tno,
			type : 'post',
			success : function(data) {

				if (data == 1) {
					alert("Deleted.");
					toonList();
				}

			},
			error : function(data) {
				console.log(data);
			}
		})
	} else {
		return false;
	}
}

function wUpdate(tno) {
	$("#upload_webtoon_movie").attr({
		id : "upload_webtoon_movie3",
		action : "/uploadWebtoon/update"
	});

	if($('#checkbox').is(':checked')){
	    $('.filebox').hide();
	}


	$('#change_no').show();
	var a = $('.genre_' + tno).text();
	$("[name=genre]").val($.trim(a));
	var b = $('.title_' + tno).text();
	$("[name=title]").val($.trim(b));
	var c = $('.writer_' + tno).text();
	$("[name=writer]").val($.trim(c));
	var d = $('.content_' + tno).text();
	$.trim($("[name=content]").val($.trim(d)));
	var e = $('.file_' + tno).text();
	var f = e.substring(23);
	$('.upload-name').attr({
		"value" : $.trim(f)
	});

	$("[name=tno]").val(tno);
	$(".submit_toon").text("Modify").attr("onclick", "update()");
	$("span.fl").text("Modify Webtoon Movie");
	$(".cancel").attr({
		type : "button",
		onclick : "cancel()"
	});
	$("#ex_filename").attr("name", "upfile3");

}

function cancel() {
	var a = confirm("Are you sure you want to cancel the contents?");
	if (a == true) {
		$("#upload_webtoon_movie3").attr({
			id : "upload_webtoon_movie",
			action : "/uploadWebtoon/upload"
		});
		$("[name=genre]").val("");
		$("[name=title]").val("");
		$("[name=writer]").val("");
		$("[name=content]").val("");
		$("[name=tno]").val("");
		$(".upload-name").attr({
			"value" : "Select File"
		});
		$(".submit_toon").text("Upload").attr("onclick", "upload_singlefile1()");
		$("span.fl").text("Register a webtoon movie");
		$(".cancel").attr("type", "reset");
		$(".cancel").removeAttr("onclick");

		$("#ex_filename").attr("name", "upfile");
		$('#change_no').hide();
		$('.filebox').show();
	}
}

function update() {

	loadingOn();
	$("#upload_webtoon_movie3")
			.ajaxForm(
					{
						url : "/uploadWebtoon/update?${_csrf.parameterName}=${_csrf.token}",
						type : "POST",
						enctype : "multipart/form-data",

						success : function() {
							alert("It has been modified.");
							loadingOff();
							toonList();

						},
						error : function(request, status, error) {
							alert("code:" + request.status + "\n" + "message:"
									+ request.responseText + "\n" + "error:"
									+ error);
							loadingOff();
							toonList();
						}

					});
	$("#upload_webtoon_movie3").submit();

}

var wfilename = "";
var wfilename_origin = ""
var wUploaded = false; // psd file upload check
// Webtoon Movie - Single File Upload Code
// 2017-04-11, shkwak
// 2017-07-19, RE Use
function upload_singlefile1() { // /upload_singlefile
	$('#upload_webtoon_movie')
			.ajaxForm(
					{
						url : "/uploadWebtoon/upload?${_csrf.parameterName}=${_csrf.token}",
						method : "POST",
						enctype : "multipart/form-data", // 여기에 url과 enctype은
															// 꼭 지정해주어야 하는 부분이며
															// multipart로 지정해주지
															// 않으면 controller로
															// 파일을 보낼 수 없음
						success : function(result) {
							console.log(result); // .resultUrl);
							wfilename = result; // .resultUrl;
							alert("The "+wfilename_origin + " file was saved to the server with the " + wfilename
									+ " file name.");
							wUploaded = true;
							toonList();

						},
						error : function(request, status, error) {
							alert("code:" + request.status + "\n" + "message:"
									+ request.responseText + "\n" + "error:"
									+ error);
						}

					});
	// 여기까지는 ajax와 같다. 하지만 아래의 submit명령을 추가하지 않으면 백날 실행해봤자 액션이 실행되지 않는다.
	$("#upload_webtoon_movie").submit();

}

function toonList() {
	$.ajax({
		type : "GET",
		url : "/webtoon/list_en",
		dataType : "text",
		error : function() {
			alert('Failed to retrieve page.');
		},
		success : function(data) {
			$('#Context').html(data);
		}

	});
}