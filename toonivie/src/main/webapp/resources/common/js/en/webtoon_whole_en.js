$(document).ready(function() {
	$('#wt_cut_save').click(function(){
		if(cropper_croped_status_wt == 'true'){
			/*		$('#wt_whole_cropArea').cropper('getCroppedCanvas').toDataURL(function (dataUrl) {	
			alert();
			  var append_str = "<img class='' src='"+dataUrl+"'>";
			  $('.wtcut_box').append(append_str);
		});*/

		var append_idx=$('.wt_cut_appendImg').length+1;
		var img_previw_title_num = $(".side_img_cut_preview").children().length+1;
		/*alert(#''+append_idx+');
*/		$(".imgPreview_").hide();
		var img_dataurl = $('#wt_whole_cropArea').cropper('getCroppedCanvas').toDataURL('image/jpeg');
		var append_str = "<div class='wt_cut_appendImg mt5' style='position: relative;' data-idx='"+append_idx+"'>" + 
						 	"<img style='width: 100%' class='' src='"+img_dataurl+"'>" +
						 	/*"<div style='position: absolute; right:5px; top:0; cursor: pointer;' type='button' onclick='wt_cut_delete(this)'>X</div>" +*/
						 	"<img style='position: absolute; right:2px; top:2px; width:20px; cursor: pointer;' src='/resources/image/v2.0/wt_cut_delete.png' onclick='wt_cut_delete(this)'>" +
						 "</div>";
		
		var append_side_previw = "<div class='side_img_cut_preview_box' data-idx='"+append_idx+"'>" + 
								 	"<div class='img_previw_title'>#"+img_previw_title_num+"</div>" +
								 	"<img class='img_previw_imgs' src='"+img_dataurl+"'>" +
								 "</div>";
		
		$('.wtcut_box').append(append_str);
		$('.side_img_cut_preview').append(append_side_previw);
		/*alert(img_previw_title_num);*/
		images.push(img_dataurl);
		$('#wt_cut_uploadCount').html("Image count : " +append_idx);
		wt_cropper_status($('#wt_whole_cropArea'));
		
		///psh add 이미지컷 캔버스 초기화
		$(".use-image-canvas").data("image","").css("background-image","url('')"); // 캔버스에 딴 이미지 올린것
		$("#canvas-before").addClass("dn");
		$("#canvas-bg").data("image","").css("background-image","url('')");
		$(".canvas-fg").addClass("dn");	
		$(".canvas-bg").addClass("dn"); 
		$("#background_img").addClass("dn");
		
		$('.wt_whole_img').on('cropend', '#wt_whole_cropArea', function(){
			wt_cropper_status($('#wt_whole_cropArea'));
			/*		var crop_wh = $('#wt_whole_cropArea').cropper('getData');
			if(crop_wh.width < viewport_height){
				console.log(crop_wh);
				alert("분리된 컷이 화면 보다 작을 경우 화질 저하가 생길 수 있습니다.");
			}*/
		});
		}else if(cropper_croped_status_wt == 'false'){
			fileUploadWebtoonImg();
		}

	});
	$('#wt_cut_save_all').click(function(){
		createArchive(images);
	});
});

function wt_cut_delete(me){
//	var i = 1;
	var count=0;
	$('.wt_cut_appendImg').each(function(idx){
		$(this).data('idx', idx);
		count = idx;
		/*var cccceee = $(this).data('idx', );
		alert(cccceee);*/
		/*$(".side_img_cut_preview_box").data('idx', idx).remove();*/
		
	});
   
	/*var delete_num = $(me).parent(".wt_cut_appendImg").data('idx');*/
	var delete_num = $(me).parent(".wt_cut_appendImg").attr('data-idx');
	/*alert(delete_num);*/
	/*var delete_num_add = delete_num+1;*/
	$(".side_img_cut_preview_box[data-idx='"+delete_num+"']").addClass("demo");
	$(".side_img_cut_preview .demo").remove();
	/*$(".side_img_cut_preview_box[data-idx='"+delete_num_add+"']").remove();*/
	//$(".side_img_cut_preview_box").data('idx', count).remove();
	/*$(".side_img_cut_preview_box").eq(delete_num).remove();*/
	
	/*$(".side_img_cut_preview_box").data("idx",delete_num_add).remove();*/
	/*$(".side_img_cut_preview_box").data("idx",delete_num_add).addClass("demo");*/
	
	/*$('#wt_cut_uploadCount').html("총 "+count+"개");*/
	$('#wt_cut_uploadCount').html("Image count : " +count);
	$(me).parent().remove();
	images.splice(1-$('.wt_cut_appendImg').data('idx'), 1);
	wt_cropper_status($('#wt_whole_cropArea'));
	
	
	$(".img_previw_title").each(function(idx){
		var previw_title = "#" + (idx+1);
		$(this).text("");
		$(this).text(previw_title);
	});
}

var images = [];
function createArchive(images){
	  if($('#wt_cut_saveName').val() == ''){
		  alert('전체 파일명을 입력해주세요.');
		  $('#wt_cut_saveName').focus();
		  return;
	  }
	  var zip = new JSZip(); //jszip
	  var img = zip.folder($('#wt_cut_saveName').val());

	  for (var i=0; i<images.length; i++) {
		  var commaIdx = images[i].indexOf(",");
//		  img.file($('.wt_cut_saveName').children()[i].val()+".jpg", images[i].slice(commaIdx + 1), {base64: true});
		  img.file($('#wt_cut_saveName').val()+"_"+(i+1)+".jpg", images[i].slice(commaIdx + 1), {base64: true});
//		  img.file(i+".jpg", images[i], {base64: true});
//		  console.log("!!"+images[i].slice(commaIdx + 1));
	  }
      zip.generateAsync({type:"blob"}).then(function(file){
    	  saveAs(file, $('#wt_cut_saveName').val()+".zip"); //filesaver
      });

	}
function downloadURI(uri, name) {
	  var link = document.createElement("a");
	  link.download = name;
	  link.href = uri;
	  document.body.appendChild(link);
	  link.click();
	  document.body.removeChild(link);
	  delete link;
	}

function fileUploadWebtoonImg(){ //이미지 따기 클릭 이벤트
	$('#fileUploadWebtoonImg').ajaxForm({
		url : '/upload/uploadImg_wt',
		method : "POST",
		enctype : "multipart/form-data",
		async : false,
		beforeSend:function(){
			loadingOn();
		},
		success : function(result){		
			setTimeout(function () {
				alert('Select image separate  area');
				$("#wt_cut_save").text('Cut select area image');
				$(".wt_whole_img").show();
/*				$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
	//			$(".wrap10").addClass("dn");
				$(".wrap_BG").addClass("dn");
				$(".saved-image-item").each(function(){
					$(this).attr("data-image","");
					$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
				});		*/
				$('.wt_whole_img').removeClass('dn');
				$(".side_img_cut_preview_box").data("idx","").attr("data-idx","");
				$('#wt_whole_cropArea').attr('src', "/resources/cvimgs/wt_img/"+result.resultUrl);
				$(".wt_whole_img").css("width", '1047px'); //우선 고정
				//$("#wt_whole_cropArea").css("height", result.height);
				cropper_init_wt($('#wt_whole_cropArea'));
				
				//psh add 180409 이미지 분리 초기화
				imgcut_Imgdiv();

				//psh add 20180409 psd 초기화
				imgcut_psdreset();
				
				loadingOff();
			},1000);
		}
	});
	$("#fileUploadWebtoonImg").submit();
}

var cropper_croped_status_wt = 'false';
function cropper_init_wt(canvas_id){
	cropper_croped_status_wt = 'true';
	canvas_id.cropper('destroy');
	canvas_id.cropper({
		  viewMode: 3,
		  aspectRatio: 16 / 9,
		  zoomable: false,
		  checkOrientation: false,
		  dragMode: 'crop',
		  //autoCrop: true,
		  ready: function () {
			  $(this).cropper('clear');
			  wt_cropper_status($(this));
/*			  var container_wh = $(this).cropper('getContainerData');
			  $(this).cropper('setCropBoxData',{
				left:0,
				top:0,
				width: container_wh.width
				//height: 540
			  });*/
		  },
		  crop: function(e) {
		    // Output the result data for cropping image.
			
			console.log(":::");
		    console.log(e.x);
		    console.log(e.y);
		    console.log(e.width);
		    console.log(e.height);
		    console.log(e.rotate);
		    console.log(e.scaleX);
		    console.log(e.scaleY);
		  }
	});
	
/*	var container_wh = canvas_id.cropper('getContainerData');
	canvas_id.cropper('setCropBoxData',{
		  left: 0,
		  top: 0,
	      width: container_wh.width-1,
		  height: container_wh.height/2,
		});*/
}
function wt_cropper_status(canvas_id){
/*	if(!($('#wt_whole_cropArea').cropper('getCropBoxData').left == undefined)){
		$('#wt_cut_save').addClass('btn-orange');
		$('#wt_cut_save').removeClass('btn-gray');
		$('#wt_cut_save').removeClass('disabled');
	}*/
	if($(".wt_cut_appendImg").length != 0){
		$('#wt_cut_save_all').addClass('btn-orange');
		$('#wt_cut_save_all').removeClass('btn-gray');
		$('#wt_cut_save_all').removeClass('disabled');
	}else{
		$('#wt_cut_save_all').removeClass('btn-orange');
		$('#wt_cut_save_all').addClass('btn-gray');
		$('#wt_cut_save_all').addClass('disabled');
	}
}

function imgcut_Imgdiv(){
	$(".saved-image-item").data("image","");
	$(".canvas-before").data("image","").css("background-image","url('')");
	$(".saved-image-item-image").data("image","").css("background-image","url('')");
	$(".dragDiv_BG").data("image","").css("background-image","url('')");
	$(".itemNumBG").data("image","").css("background-image","url('')"); // psd분리 bg
	$(".canvas-codeBG").data("image","").css("background-image","url('')"); // psd분리 캔퍼스 bg
	$(".use-image-canvas").data("image","").css("background-image","url('')"); // 캔버스에 딴 이미지 올린것
	$("#canvas-bg").data("image","").css("background-image","url('')");
	$(".canvas-fg").addClass("dn");	
	$(".canvas-bg").addClass("dn"); 
	$("#background_img").addClass("dn");
	$(".re_upload_img_cut").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
	$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
	$(".color_box_text").removeClass("dn");
	$(".color_box_me").remove();
	$("#canvas-before").addClass("dn");
	$(".imgtab_click").css("z-index","101");
	//이미지 분리 종류
	$("#cutModeBtn1").attr({"disabled":"disabled","src":"/resources/image/v2.0/cutModeBtn1.png"}).addClass("disabled"); 
	$("#cutModeBtn2").attr({"disabled":"disabled","src":"/resources/image/v2.0/cutModeBtn2.png"}).addClass("disabled");
	//배경 색상 채우기
	$("#fill_black").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_black.png"}).addClass("disabled");
	$("#fill_white").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_white.png"}).addClass("disabled");
	$("#fill_surrounding").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_surrounding.png"}).addClass("disabled");
	$("#grabcut_color_picker").attr({"disabled":"disabled","src":"/resources/image/v2.0/grabcut_color_picker.png","background-color":"#fff"}).addClass("disabled");
	//버튼 비활성화
	$("#cut_img_ori").attr("disabled","disabled").addClass("disabled"); 
	$(".cut-img-add").attr("disabled","disabled").addClass("disabled"); 
	$(".color_change_btns").attr("disabled","disabled").addClass("disabled"); 
	$("#ROI_select").attr("disabled","disabled").addClass("disabled"); 
	
	
}
function imgcut_psdreset(){
	$("#canvas_bg").data("image","").css("background-image","url('')");
	$(".psd_Layer_Preview").addClass("dn"); //레이어 미리보기
	$(".psd_LPC_img").css("background","");
	$(".psd_LPC_img").css("height","0");
	$(".second_saved_img").addClass("dn");
	$("#upload_psd").attr("src","/resources/image/toolicon_1.png"); 
}