var fileTarget = $('.filebox-modify>input.upload-hidden-modify');
$('#checkbox').click(function() {
	if ($('#checkbox').prop('checked') == false) {
		$('.filebox-modify').show();
	} else {
		$('.filebox-modify').hide();
	}
})
fileTarget.on('change', function() { // 값이 변경되면
	if (window.FileReader) { // modern browser
		var filename = $(this)[0].files[0].name;
	} else { // old IE
		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
		// 추출
		alert(filename);
	}

	// 추출한 파일명 삽입
	$(this).siblings('.upload-name-modify').val(filename);
});

// 업데이트시 파일 변경 안할떄
$('.no-change').css('display', 'none');