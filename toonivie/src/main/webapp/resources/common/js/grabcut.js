var grabcutUrl;
var beforeGrabCutUrl; 
var historyGrabCut = new Array();
var GmoX=0;
var GmoY=0;
var GbeX=0;
var GbeY=0;
var GafX=0;
var GafY=0;
var canvasFlag=0;
var bgListX=new Array();
var bgListY=new Array();
var fgListX=new Array();
var fgListY=new Array();

var gcBgdX=new Array();
var gcBgdY=new Array();

var gcFgdX=new Array();
var gcFgdY=new Array();

var gcprBgdX=new Array();
var gcprBgdY=new Array();

var gcprFgdX=new Array();
var gcprFgdY=new Array();


var rgb_ori = [];
var saliencyCut = 'false';
var fg_mask;
var bg_mask;
var grabcut_mask;
var ori_background;
var img_resize_rate_h;
var img_resize_rate_w;
var fileUploadOpertFlag = false; 
var grabCutExecuteOpertFlag = false; 
var fixWidth = 960; 
var fixHeight = 540;
var blurSigmaXV = 0;

function fileUploadGrabcutImg(){ 
	//이미지 업로드 클릭 이벤
	fileUploadOpertFlag = false;
	historyGrabCut = new Array();
	gcDataReSet();
	$('#uploadGrabcutImgFrom').ajaxForm({
		url : '/upload/uploadImg',
		method : "POST",
		enctype : "multipart/form-data",
		async : false,
		beforeSend:function(){
			loadingOn();
		},
		success : function(result){		
			fixHeight = ((result.height*fixWidth)/result.width);
			if(fixHeight < 550){
				$("#canvas_width_re").css("height",fixHeight+10);
			}else{
				$("#canvas_width_re").removeAttr("style");
			}
			$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9, .wrap11").remove();
			$(".wrap_BG").addClass("dn");
			$(".saved-image-item").each(function(index){
				$(this).attr("data-image","");
				$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
				if(index<9){
					$(".dragDiv"+(index+1)).addClass("disabled");					
				}
			});								
			$(".glyphicon-pushpin").addClass("dn");
			$(".save-bg").attr("data-image","");
			$(".canvas-append-zone").css("z-index","99");
			$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			listNum=1;
			$("#canvas-after").css("background-image","url('')");
			$("#canvas-after").attr("width",fixWidth); 
			$("#canvas-after").attr("height",fixHeight);		
			img_resize_rate_w = result.width / fixWidth;
			img_resize_rate_h = result.height / fixHeight;
			$("#canvas-before").data("img_url",result.resultUrl);
			$("#canvas-before").attr("width",fixWidth);
			$("#canvas-before").attr("height",fixHeight);			
			$("#canvas-before").css("background-size",fixWidth+"px "+fixHeight+"px");
			
			img_url = result.resultUrl;
			beforeGrabCutUrl = result.resultUrl;
			
			$(".canvas-before").removeClass("dn");			
			$("#canvas-before").parent().removeClass("dn");			
			$(".second_saved_img").addClass("dn");
			
			//fg_bg 마스킹 init by KH 2018-02-20
			$("#canvas-fg").attr("width",fixWidth);
			$("#canvas-fg").attr("height",fixHeight);
			$("#canvas-bg").attr("width",fixWidth);
			$("#canvas-bg").attr("height",fixHeight);
			$("#canvas-fg-bw").attr("width",fixWidth);
			$("#canvas-fg-bw").attr("height",fixHeight);
			$("#canvas-bg-bw").attr("width",fixWidth);
			$("#canvas-bg-bw").attr("height",fixHeight);
			$(".canvas-fg, .canvas-bg").removeClass("dn");
			
			
			color_filename = result.filename;
			put_color(result.color_palette);
			
			
			$('#ROI_select').removeClass('disabled');
			if(ROI_status == 'true' || ROI_status == 'using'){
				ROI_setting();
			}
			
			$(".go-edit").addClass("dn")
			$(".go-next").removeClass("dn");
			$(".canvas-wrap").removeClass("dn");			
			grabcutUrl = img_url;
			psd_c = document.getElementById("canvas_bg");
		    psd_ctx = psd_c.getContext("2d");
		    psd_ctx.clearRect(0, 0, psd_c.width, psd_c.height);
		    $(".second_saved_img").addClass("dn");
		    $("#layered_img_cropped").children().remove();
		    $(".cropped_remove").remove();
		    
		    $(".use-image-canvas-wrap").css("top",'25%');

			$(".canvas-append-zone").removeClass('dn');
			psd_img_mode='false';
			itemList_Length=9;

			$('#cut_img_ori').html('이미지 다시 따기'); //이미지 재 업로드 -> 이미지 다시 따기
			$('.saved-image-item').attr('title', '');
			$('.saved-image-item').data('image','');

			cropper_croped_status_wt = 'false';
			
			$("#wt_image_view").data("image","").attr("src","/resources/image/toolicon_1.png");
			$("#wt_image").val(''); 
			$("#wt_image_name").text("");
			$(".wt_whole_img").addClass("dn");
			$("#wt_cut_save").text('이미지 업로드').addClass('disabled');
			$("#wt_whole_cropArea").cropper('destroy').removeAttr('src').removeAttr("style");
			$('#wt_cut_saveName').val("");	
			$(".imgPreview_").show();
			$('#wt_cut_uploadCount').text("");
			$(".wtcut_box").children(".wt_cut_appendImg").remove();
			$(".side_img_cut_preview").children(".side_img_cut_preview_box").remove();
			$("#transImgName").addClass("disabled");
			$("#wt_cut_save_all").addClass('disabled');

			$("#canvas_bg").removeAttr("data-image").removeAttr("style").addClass("dn");
			$(".psd_LPC_img").css("background","");
			$(".psd_LPC_img").css("height","0");
			$("#upload_psd").attr("src","/resources/image/toolicon_1.png");
			ori_background = result.filename;
			//초기화
			canvas_index = [];

		    swal('분리할 영역을 선택해주세요.');
		}
	});
	$("#uploadGrabcutImgFrom").submit();
	fileUploadOpertFlag = true;
	imageCutBtnSet();
	getColorValueAvr(false);
	savedBackImg = false;
	leftTabSet();
}

function grabcutExecute(){ //매우 중요	
	grabCutExecuteOpertFlag = false;
	loadingOn()
	bgListX.push(1);
    bgListY.push(1);
    fgListX.push(1);
    fgListY.push(1);
    
    clicked_color_rgb.push(1);
    var randomId = new Date().getTime();
    var data = {};
    data["GbeX"] = Math.round(GbeX * img_resize_rate_w);
    data["GbeY"] = Math.round(GbeY * img_resize_rate_h);
    data["GafX"] = Math.round(GafX * img_resize_rate_w);
    data["GafY"] = Math.round(GafY * img_resize_rate_h);
    data["bgListX"] = bgListX;
    data["bgListY"] = bgListY;
    data["fgListX"] = fgListX;
    data["fgListY"] = fgListY;
    
    data["gcBgdX"] = gcBgdX;
    data["gcBgdY"] = gcBgdY;
    
    data["gcFgdX"] = gcFgdX;
    data["gcFgdY"] = gcFgdY;
    
    data["gcprBgdX"] = gcprBgdX;
    data["gcprBgdY"] = gcprBgdY;
    
    data["gcprFgdX"] = gcprFgdX;
    data["gcprFgdY"] = gcprFgdY;
    
    data["Gmode"]=Gmode;
    data["imgUrl"]=grabcutUrl;
    beforeGrabCutUrl = grabcutUrl;
    data["filling_type"]=filling_type;
    data['clicked_color_rgb']=clicked_color_rgb;
    data["fg_mask"]=fg_mask;
    data["bg_mask"]=bg_mask;
    data["blur_status"]=blur_status;
    data["blurSigmaXV"] = blurSigmaXV;
    data["iterate"]="5";
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var headers = {};

    if(SaliencyCut_status == 'true'){
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
    	$.ajax({
    		url : "/sailencyCut/sailencyCutExe",
    		dataType : "json",
    		type : "POST",
    		async: true,
    		data : data,
    		success : function(data) {
    			setTimeout(function () {

    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",data.width);
    				$("#canvas-after").attr("height",data.height);
    				popupOn($(".canvas-grapcut-modal"));
    				$(".canvas-footer").removeClass("dn");
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultImg="";
    				resultImg=data.resultImg;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				grabCutExecuteOpertFlag = true;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				loadingOff();
    			},1000);
    		},
    		error : function(request, status, error) {
    			swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					})
    			loadingOff();
    		}
    	});
    }else{
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
    	$.ajax({
    		url : "/grabcut/grabcutExe",
    		dataType : "json",
    		type : "POST",
    		async: true,
    		data : data,
    		success : function(data) {
    			color_filename = data.backGroundFile.replace('/resources/cvimgs/','');
    			img_url = data.backGroundFile;
    			//init_color_picker();
    			if(ROI_status == 'true' || ROI_status == 'using'){
    				ROI_setting(); 
    			}
    			setTimeout(function () {
    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",data.width);
    				$("#canvas-after").attr("height",data.height);
    				popupOn($(".canvas-grapcut-modal"));
    				grabcutResultView(data);
    				$(".canvas-footer").removeClass("dn");
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultbeX=data.beX;
    				resultbeY=data.beY;
    				resultImg="";
    				resultImg=data.resultImg;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				grabCutExecuteOpertFlag = true;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				loadingOff();
    			},1000);
    		},
    		error : function(request, status, error) {
    			swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					})
    		}
    	});
    }
}

function grabcutResultView(data){
	$('.side_img_cut_preview').children().remove();
	var grabcutResultView = "<div><img style='width:310px; height:304px;' src='"+data.resultImg+"'/></div>";
	$('.side_img_cut_preview').append(grabcutResultView);
}

var resultWidth;
var resultHeight;
var resultbeX;
var resultbeY;
var backGroundWidth;
var backGroundHeight;
var resultImg;
var listNum=1;

var fixed_width=1040;
var fixed_height=0;
var bg_ratio=0;
var shrinking_ratio_w=0;
var shrinking_ratio_h=0;
var itemList_Length=9;


function makeTopLayers(data){
	var itemList = data.result;
	$(".saved-image-item").each(function(){
		$(this).attr("data-image","");
		$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
	});	
	$(".save-bg").attr("data-image","");
	$(".canvas-append-zone").css("z-index","99");
	$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
	listNum=1;
	
	itemList_Length = itemList.length;
	obtest = data.result;
	$('#layered_img').children('.appended').remove();
	if(itemList_Length>9){
		var add_length = Math.abs(9-itemList_Length);
		var numb = 10;
		for(var i=0;i<add_length;i++){
			var append_str =
				"<!-- 상단 분리된 이미지 영역 -->"+
				"<div class='psd_pre clxk saved-image-item draggg dragDiv"+numb+" img_selection sii_px di appended' data-target='wrap"+numb+"' data-index='"+numb+"' data-url='"+ itemList[numb-1].layer_name.value +"' data-selection='true' title='"+itemList[numb-1].layer_name.value+"'>"+
				"	<div class='saved-image-item-image re itemNum"+numb+"' style='background-image: url()'></div>"+
				"	<div class='w100 fl'>"+
				"		<span class='w100 fl tc' >"+itemList[numb-1].layer_name.value+"</span>"+
				"	</div>"+
				"</div>";
			$('#layered_img').append(append_str);
			width_auto();
			numb++;
		}
		
	}
	$(".saved-image-item").addClass("disabled");
	$.each(itemList,function(index,value){
		var randomId = new Date().getTime();
		$(".itemNum"+listNum+"").css("background-image","url('')");
		$(".itemNum"+listNum+"").css("background-image","url('/"+value.imgUrl+"?v="+randomId+"')");
		$(".itemNum"+listNum+"").attr("data-url", value.imgUrl);
		$(".itemNum"+listNum+"").next().children().html(value.layer_name.value);
		$(".dragDiv"+listNum+"").attr('title',value.layer_name.value);
//
		//$(".itemNum"+listNum+"").data("url","/"+value.imgUrl+"?v="+randomId);
		$(".itemNum"+listNum+"").data("url","/"+value.imgUrl);
		$(".dragDiv"+listNum+"").data("width",value.width);
		$(".dragDiv"+listNum+"").data("height",value.height);
		$(".dragDiv"+listNum+"").data("image","/"+value.imgUrl+"?v="+randomId);
		if(listNum<10){
			saveImgItemImgAtvChk(listNum,"d");
		}		
		listNum++;
	});
}

/* 배경 저장, PSD 배경 저장 클릭 이벤트 */
$(".cut-img-add").click(function(){
	if(fileUploadOpertFlag == true){
			if(listNum == 1){
					if(filling_type == 2){
						swal('주변색 채우기는 배경색으로 설정하실수 없습니다.');
						return;
					}else{
						save_bg_as_object();
					}
			}
			if(historyGrabCut.length > 0){
				$(".canvas-wrap").addClass("dn");
				$(".dragDiv_BG").attr('data-image',grabcutUrl);
				$(".save-bg").css("background-image","url("+grabcutUrl+")");
				$(".wrap_BG").removeClass("dn");
				$(".canvas-codeBG").attr("width",fixWidth);
				$(".canvas-codeBG").attr("height",fixHeight);
				$(".canvas-codeBG").css("background-image","url("+grabcutUrl+")");
				$(".canvas-codeBG").css("background-size",fixWidth+"px "+fixHeight+"px");
				$(".canvas-codeBG").attr("data-image",grabcutUrl);
				$(".re_saved_Bg").removeClass("disabled");
				$('.side_img_cut_preview').children().remove();
				saveImgItemImgAtvChk(listNum, "d");
				savedBackImg = true;
				leftTabSet();
				imageCutBtnSet();	
				savedBackImg = false;
			}	
		
	}else{
		swal('이미지 업로드를 먼저 실행 해주세요.');
		return false;
	}
});

var psd_c;
var psd_ctx;
$('#layered_img').on('click', '.img_selection',function() {
	if(psd_img_mode != 'true'){
		var me = $(this);
		if($('.img_selection').data('selection')=='true'){
			
		grabcutUrl = me.data('image');
		backGroundWidth = me.data('width');
		backGroundHeight = me.data('height');
		shrinking_ratio_w = fixed_width/backGroundWidth;
		shrinking_ratio_h = fixed_height/backGroundHeihg;
		//shrinking_ratio = 1;
		backGroundHeight=backGroundHeight * shrinking_ratio;
		backGroundWidth=fixed_width;
		$(".canvas-wrap").addClass("dn");
		$(".dragDiv_BG").attr('data-image',grabcutUrl);
		$(".save-bg").css("background-image","url('"+grabcutUrl+"')");
		$(".wrap_BG").removeClass("dn");
		$(".use-image-canvas-wrap").css("top",'33%');
		$(".second_saved_img").removeClass("dn");
		$(".use-image-canvas-header").remove();
			
		$("#background_img").css("width",backGroundWidth);
		$("#background_img").css("height",fixHeight);
		$("#background_img").css('overflow','scroll');
			
		$(".canvas-codeBG").css("width",backGroundWidth);
		$(".canvas-codeBG").css("height",backGroundHeight);
		$(".canvas-codeBG").attr("image",grabcutUrl);
			savedBackImg = true;
			
		psd_c = document.getElementById("canvas_bg");
		psd_ctx = psd_c.getContext("2d");
		    
		var image = new Image();  
		image.onload = drawImageActualSize;
		image.src = grabcutUrl;

		function drawImageActualSize() {
						psd_c.width = backGroundWidth;	
		psd_c.height = backGroundHeight;
		psd_ctx.drawImage(this, 0, 0, backGroundWidth, backGroundHeight);
		canvas.width = backGroundWidth;
		canvas.height = backGroundHeight;
		context.drawImage(this, 0, 0, fixWidth, fixHeight);
		}
			btop_start_position = $(".canvas-codeBG").offset().top;
		$('.img_selection').data('selection', 'false');
		}
	}
});

function saveResultToList1(){
	var randomId = new Date().getTime();
	$(".itemNum"+listNum+"").css("background-image","url('')");
	$(".itemNum"+listNum+"").css("background-image","url('"+resultImg+"?v="+randomId+"')");
	$("#canvas-before").css("background-image","url('')");
	$("#canvas-before").css("background-image","url('"+grabcutUrl+"?v="+randomId+"')");
	$(".itemNum"+listNum+"").data("url",resultImg);
	$(".itemNum"+listNum+"").attr('data-url',resultImg);
	$(".dragDiv"+listNum+"").data("width",resultWidth);
	$(".dragDiv"+listNum+"").data("height",resultHeight);
	$(".dragDiv"+listNum+"").data("image",resultImg);
	context.clearRect(0, 0, canvas.width, canvas.height);
	grabCutExecuteOpertFlag = false;
	leftTabSet();
	imageCutBtnSet();
	savedBackImg = false;
	saveImgItemImgAtvChk(listNum,"d");
	historyGrabCut[listNum-1] = {"itemUrl":resultImg, "beforeGrabCutUrl":beforeGrabCutUrl, "grabcutUrl":grabcutUrl};
	if(listNum == historyGrabCut.length){
		$('#tool_forward').addClass("disabled");
	}
	listNum++;	
	$("#tool_back").removeClass("disabled");
	gcDataReSet();
	popupOff($(".canvas-grapcut-modal"));	
}


function imageCutBtnSet(){
	if(fileUploadOpertFlag == true && savedBackImg == false ){
		set_filling_color(0);
		setMode(0);
		$(".cut-img").removeClass("disabled").attr("disabled", false);
		$(".re_saved_Bg").addClass("disabled");
		if(fileUploadOpertFlag == true){
			$(".cut-img-add").removeClass("disabled").attr("disabled", false);			
		}
		$(".color_change_btns").removeClass("disabled").attr("disabled", false);	
		$("#multiapply").addClass("disabled");
		$(".effect-cut-success-button").addClass("disabled");
	}else{
		set_filling_color(6);
		setMode(3);
		$(".cut-img").addClass("disabled").attr("disabled", true);
		$(".cut-img-add").addClass("disabled").attr("disabled", true);
		$(".re_saved_Bg").removeClass("disabled");
		$('.side_img_cut_preview').children().remove(); 
		$(".color_change_btns").addClass("disabled").attr("disabled", true);
		$("#cut_img_ori").removeClass("disabled").attr("disabled", false);	
		$("#tool_back, #tool_forward").addClass("disabled");
		$("#multiapply").removeClass("disabled");
		
	}
}

/* 색상 보정(RGB) */

function getColorValueAvr(initl_f){
	var data={};
	if(initl_f == false){
		data["imgUrl"] = grabcutUrl;		
	}else{
		data["imgUrl"] = "/resources/cvimgs/Original_img.png";
	}
	data["psd_img_mode"]=psd_img_mode;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    $('#red').trigger('slide'); 
	$.ajax({
        url : "/changeColor/getColorAvg",
        dataType : "json",
        type : "POST",
        data : data,
        success : function(data) {      
        	var randomId = new Date().getTime();
        	context.clearRect(0, 0, canvas.width, canvas.height);
        	if(psd_img_mode == 'true'){
            	$( "#red_psd" ).slider( "value", data.red );
                $( "#green_psd" ).slider( "value", data.green );
                $( "#blue_psd" ).slider( "value", data.blue );
                if(psd_color_init != "true"){
                	$('.canvas-code'+image_data_index).css('background-image',"url('"+grabcutUrl+"?v="+randomId+"')");
                	$('.canvas-code'+image_data_index).data('image', "/resources/psdimgs/"+data.resultImg_Name);
                	$('.itemNum'+image_data_index).css('background-image',"url('"+grabcutUrl+"?v="+randomId+"')");
                	$('.itemNum'+image_data_index).data('url', +grabcutUrl);
                	$('.dragDiv'+image_data_index).data('image', grabcutUrl);
                }
        	}else{
            	$( "#red" ).slider( "value", data.red );
                $( "#green" ).slider( "value", data.green );
                $( "#blue" ).slider( "value", data.blue );
            	$("#canvas-before").css("background-image","url("+grabcutUrl+"?v="+randomId+")");
        	}
        	
            rgb_ori.push(data.red);
            rgb_ori.push(data.green);
            rgb_ori.push(data.blue);
            if(initl_f == true){
                changeColorExe(true);
            }
        	loadingOff();
        },
        error : function(request, status, error) {
        	swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})
        }
    });
}

function changeColorExe(save_yn, rgb_ori1){
	
	var data={};
	if(psd_img_mode == 'true'){
		data["red"]=$( "#red_psd" ).slider("value");
		data["green"]=$( "#green_psd" ).slider("value");
		data["blue"]=$( "#blue_psd" ).slider("value");
	}else{
		if(rgb_ori1 != null){
		}else{
			data["red"]=$( "#red" ).slider("value");
			data["green"]=$( "#green" ).slider("value");
			data["blue"]=$( "#blue" ).slider("value");
		}
	}
	data["imgUrl"] = grabcutUrl;
	data["save_yn"] = save_yn;
	
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    loadingOn();
	$.ajax({
        url : "/changeColor/changeExe",
        dataType : "json",
        type : "POST",
        data : data,
        async : false,
        success : function(data) {
        	var randomId = new Date().getTime();
        	if(psd_img_mode == 'true'){
        		//적용된 이미지 변경
        		$('.canvas-code'+image_data_index).css('background-image', "url('/resources/psdimgs/new_"+data.resultImg_Name+"?v="+randomId+"')");
        		$('.canvas-code'+image_data_index).data('image', "/resources/psdimgs/new_"+data.resultImg_Name);
        		//상단 메뉴 이미지 변경
        		$('.itemNum'+image_data_index).css('background-image', "url('/resources/psdimgs/new_"+data.resultImg_Name+"?v="+randomId+"')");
        		$('.itemNum'+image_data_index).data('url', "/resources/psdimgs/new_"+data.resultImg_Name);
        		$('.dragDiv'+image_data_index).data('image', "/resources/psdimgs/new_"+data.resultImg_Name);
        	}else{
        		$("#canvas-before").css("background-image","url('')");
        		$("#canvas-before").css("background-image","url('"+data.resultImg+"?v="+randomId+"')");
        	}
        	loadingOff();
        },
        error : function(request, status, error) {
        	swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				});
        }
    });
}

var psd_color_change = 'false';
var psd_color_init = 'false;'
var image_data_index = null;
var return_color = 'false';
var image_url;
$('#change_psd_color').click(function() {
	if(psd_color_change == 'true'){
		psd_color_init = 'false';
		psd_color_change = 'false';
		changeColorExe('psd');
	}else{
		swal('적용할 이미지를 선택해 주세요');
		psd_color_init = 'true';
	}
});

$('#layered_img').on('click', '.saved-image-item',function() { 
	image_data_index = $(this).data('index');
	grabcutUrl = $(this).data('image');
	if(psd_color_init == 'true'){
		getColorValueAvr(false);
		swal('RGB 색상 설정후 적용 버튼을 눌러주세요.');
		psd_color_change = 'true';
	}else if(return_color == 'true'){
		grabcutUrl = $(this).data('image');
		grabcutUrl = grabcutUrl.replace('new_','');
		getColorValueAvr(false);
	}
});

$('#red, #green, #blue').on('slide', function(event, ui) {
	if(fileUploadOpertFlag == true){
		var cur_red = rgb_ori[0];    var cur_green = rgb_ori[1];    var cur_blue = rgb_ori[2];
	    $('#preview_btn, #applc_btn').removeClass('disabled');
	    if( ui != undefined){
	    	if( event.target.id =="red" ){
		    	cur_red = ui.value;
		    }else if( event.target.id =="green" ){
		    	cur_green = ui.value;
		    }else if( event.target.id =="blue" ){
		    	cur_blue = ui.value;
		    }    
	    }	    
		if( cur_red == rgb_ori[0] && cur_green == rgb_ori[1] && cur_blue == rgb_ori[2] ){
			$('#go_back, #preview_btn, #applc_btn').addClass('disabled');
		}else{
			$('#go_back, #preview_btn, #applc_btn').removeClass('disabled');
		}
	}    	
});

function save_bg_as_object(){
    clicked_color_rgb.push(1);
    var randomId = new Date().getTime();
    var data = {};
    data["imgUrl"]=grabcutUrl;
    beforeGrabCutUrl = grabcutUrl;
    data["filling_type"]=filling_type; 
    data['clicked_color_rgb']=clicked_color_rgb;
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var headers = {};
        headers[csrfHeader] = csrfToken;
        data[csrfParameter] = csrfToken;
    	$.ajax({
    		url : "/grabcut/saveBGasOBJ",
    		dataType : "json",
    		type : "POST",
    		async:false,
    		data : data,
    		beforeSend:function(){
    			loadingOn();
    		},
    		success : function(data) {
    			setTimeout(function () {
    				
    				$("#canvas-after").css({"background-image":"url('')"});
    				$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
    				$("#canvas-after").attr("width",fixWidth);
    				$("#canvas-after").attr("height",fixHeight);
    				$("#canvas-after").css("background-size",fixWidth+"px "+fixHeight+"px");
    				$(".canvas-footer").removeClass("dn");
    				popupOn($(".canvas-grapcut-modal"));
    				
    				resultWidth=data.width;
    				resultHeight=data.height;
    				resultImg="";
    				resultImg=data.resultImg;
    				grabcutUrl=data.backGroundFile;
    				backGroundWidth=data.backGroundWidth;
    				backGroundHeight=data.backGroundHeight;
    				if(Gmode==1){
    					bgListX=new Array();
    					bgListY=new Array();
    				}
    				$(".canvas-wrap").addClass("dn");
					$(".dragDiv_BG").attr('data-image',grabcutUrl);
					$(".save-bg").css("background-image","url('"+grabcutUrl+"')");
					$(".wrap_BG").removeClass("dn");
					$(".use-image-canvas-wrap").css("width",fixWidth);
					$(".use-image-canvas-wrap").css("height",fixHeight);
					$(".canvas-codeBG").css("width",960);
					$(".canvas-codeBG").css("height",540);
					$(".canvas-codeBG").css("background-image","url('"+grabcutUrl+"')");
					$(".canvas-codeBG").attr("data-image",grabcutUrl);
					savedBackImg = true;
					loadingOff();
			},1000);
    		},
    		error : function(request, status, error) {
    			swal({
					  title: 'Error!',
					  text: "code:" + request.status + "\n" + "error:" + error,
					  type: 'error',
					})
    			loadingOff();
    		}, complete : function(data){
    		}
    	});
}
function leftTabSet(){
	if(savedBackImg == false){
		for(var i=2; i<7; i++){
			$('.tabs').find('.tabb_'+i).addClass('disabled');	
		}
	}else{
		$('.tabs').find('.tabs-son').removeClass('disabled');
	}
}

function gcDataReSet(){
	gcBgdX=new Array();
	gcBgdY=new Array();
	gcFgdX=new Array();
	gcFgdY=new Array();
	gcprBgdX=new Array();
	gcprBgdY=new Array();
	gcprFgdX=new Array();
	gcprFgdY=new Array();
	context_fg.clearRect(0, 0, canvas_fg.width, canvas_fg.height);
}

function saveImgItemImgAtvChk(itemNm,fixFlag){
	var saveImgBgUrl = $('#layered_img').find('.itemNum'+itemNm).attr('data-url');
	var disableFlag = $('#layered_img').find('.dragDiv'+itemNm).hasClass('disabled');
	var dnFlag = $('#layered_img').find('.dragDiv'+itemNm).find('.glyphicon-pushpin').hasClass('dn');
	if(saveImgBgUrl != ""){
		switch (fixFlag) {
			case "d":
				if(disableFlag){
					$('#layered_img').find('.dragDiv'+itemNm).removeClass('disabled');
				}else{
					$('#layered_img').find('.dragDiv'+itemNm).addClass('disabled');
				}
				break;
			case "g":
				if(dnFlag){
					$('#layered_img').find('.dragDiv'+itemNm).find('.glyphicon-pushpin').removeClass('dn');
				}else{
					$('#layered_img').find('.dragDiv'+itemNm).find('.glyphicon-pushpin').addClass('dn');
				}		    
		    break;
		    default:
		    break;
		}		
	}		
}