var grabcutUrl;
var GmoX=0;
var GmoY=0;
var GbeX=0;
var GbeY=0;
var GafX=0;
var GafY=0;
var canvasFlag=0;
var bgListX=new Array();
var bgListY=new Array();
var fgListX=new Array();
var fgListY=new Array();

function fileUploadGrabcutImg(){
	$('#uploadGrabcutImgFrom').ajaxForm({
		url: '/upload/uploadImg',
		method:"POST",
		enctype: "multipart/form-data",
		async:false,
		success: function(result){		
			$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
//			$(".wrap10").addClass("dn");
			$(".wrap_BG").addClass("dn");
			$(".saved-image-item").each(function(){
				$(this).attr("data-image","");
				$(this).find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			})									
			$(".save-bg").attr("data-image","");
			$(".canvas-append-zone").css("z-index","99");
			$(".save-bg").find(".saved-image-item-image").attr("data-url","").css("background-image","url('')");
			listNum=1;
			$("#canvas-before").css("background-image","url("+"/resources/cvimgs/"+result.resultUrl+")");
			$("#canvas-after").css("background-image","url('')");
			$("#canvas-before").attr("width",result.width);
			$("#canvas-before").attr("height",result.height);
			$(".canvas-before").removeClass("dn");			
			$(".go-edit").addClass("dn")
			$(".go-next").removeClass("dn");
			$(".canvas-wrap").removeClass("dn");			
			grabcutUrl=result.resultUrl;
			alert("Please select an area to edit.");
		}
	});
	$("#uploadGrabcutImgFrom").submit();
	getColorValueAvr();
	
}
function getColorValueAvr(){
	var data={};
	data["imgUrl"]=grabcutUrl;
	$.ajax({
        url : "/changeColor/getColorAvg",
        dataType : "json",
        type : "GET",
        data : data,
        success : function(data) {
        	//alert("red : "+data.red+" green : "+data.green+" blue : "+data.blue);
        	$( "#red" ).slider( "value", data.red );
            $( "#green" ).slider( "value", data.green );
            $( "#blue" ).slider( "value", data.blue );
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }

    });
	
}
function grabcutExecute(){ //매우 중요
	//alert("before");
	bgListX.push(1);
    bgListY.push(1);
    fgListX.push(1);
    fgListY.push(1);
    var randomId = new Date().getTime();
    var data = {};
    data["GbeX"] = GbeX;
    data["GbeY"] = GbeY;
    data["GafX"] = GafX;
    data["GafY"] = GafY;
    data["bgListX"] = bgListX;
    data["bgListY"] = bgListY;
    data["fgListX"] = fgListX;
    data["fgListY"] = fgListY;
    data["Gmode"]=Gmode;
    data["imgUrl"]=grabcutUrl;
    console.log(data);
    $.ajax({
        url : "/grabcut/grabcutExe",
        dataType : "json",
        type : "POST",
        async:false,
        data : data,
        success : function(data) {
        	//alert("after");
        	$("#canvas-after").css({"background-image":"url('')"});
			$("#canvas-after").css({"background-image":"url("+data.resultImg+"?v="+randomId+")"});
			$("#canvas-after").attr("width",data.width);
			$("#canvas-after").attr("height",data.height);
			$(".canvas-footer").removeClass("dn");
			resultWidth=data.width;
			resultHeight=data.height;
			resultImg="";
			resultImg=data.resultImg;
			//alert(resultHeight+","+resultWidth);
			backImg=data.backGroundFile;
			grabcutUrl=data.backGroundFile;
			backGroundWidth=data.backGroundWidth;
			backGroundHeight = data.backGroundHeight;
			if(Gmode==1){
				bgListX=new Array();
				bgListY=new Array();
			}
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    });
}

var resultWidth;
var resultHeight;
var backGroundWidth;
var backGroundHeight;
var resultImg;
var backImg;
var listNum=1;
function makeTopLayers(data){ //for PSD 이미지분리 결과 DP
	var itemList = data.result;
	$.each(itemList,function(index,value){
		var randomId = new Date().getTime();
		//$("#psd-progressbar").progressbar({
		//      value: 10*index
		//});
		$(".itemNum"+listNum+"").css("background-image","url('')");
		$(".itemNum"+listNum+"").css("background-image","url('/"+value.imgUrl+"?v="+randomId+"')");
/*		$(".itemNum"+listNum+"").attr("data-url",value.imgUrl);
		$(".dragDiv"+listNum+"").attr("data-width",value.width);
		$(".dragDiv"+listNum+"").attr("data-height",value.width);
		$(".dragDiv"+listNum+"").attr("data-image",value.imgUrl);*/
		$(".itemNum"+listNum+"").data("url",value.imgUrl);
		$(".dragDiv"+listNum+"").data("width",value.width);
		$(".dragDiv"+listNum+"").data("height",value.width);
		$(".dragDiv"+listNum+"").data("image",value.imgUrl);
		console.log(index);
		listNum++
	});
	//popupOff($(".psd-progress-modal"));	
}

$(".cut-img-add").click(function(){
	$(".canvas-wrap").addClass("dn");
//	$(".dragDiv10").attr('data-image',backImg);
	$(".dragDiv_BG").attr('data-image',backImg);
	$(".save-bg").css("background-image","url('"+backImg+"')");
//	$(".wrap10").removeClass("dn");
	$(".wrap_BG").removeClass("dn");
	
	$(".use-image-canvas-wrap").css("width",backGroundWidth);
	$(".use-image-canvas-wrap").css("height",backGroundHeight);
//	$(".canvas-code10").css("width",backGroundWidth);
//	$(".canvas-code10").css("height",backGroundHeight);
//	$(".canvas-code10").css("background-image","url('"+backImg+"')");
//	$(".canvas-code10").attr("data-image",backImg);
	$(".canvas-codeBG").css("width",backGroundWidth);
	$(".canvas-codeBG").css("height",backGroundHeight);
	$(".canvas-codeBG").css("background-image","url('"+backImg+"')");
	$(".canvas-codeBG").attr("data-image",backImg);
});

function saveResultToList1(){
	var randomId = new Date().getTime();
	$(".itemNum"+listNum+"").css("background-image","url('')");
	$(".itemNum"+listNum+"").css("background-image","url('"+resultImg+"?v="+randomId+"')");
	$("#canvas-before").css("background-image","url('')");
	$("#canvas-before").css("background-image","url('"+backImg+"?v="+randomId+"')");
	$(".itemNum"+listNum+"").attr("data-url",resultImg);
	$(".dragDiv"+listNum+"").attr("data-width",resultWidth);
	$(".dragDiv"+listNum+"").attr("data-height",resultHeight);
	$(".dragDiv"+listNum+"").attr("data-image",resultImg);
	context.clearRect(0,0,1000,1000);
	listNum++;
}

function setMode(modeInteger){
	Gmode=modeInteger;
	alert(mode);
	if(Gmode==0){
	    $.ajax({
	        url : "/grabcut/init",
	        dataType : "json",
	        type : "POST",
	        success : function(data) {
	        },
	        error : function(request, status, error) {
	            alert("code:" + request.status + "\n" + "error:" + error);
	        }
	    });
	}
}

function changeColorExe(){
	var data={};
	//alert($( "#red" ).slider("value"));
	data["red"]=$( "#red" ).slider("value");
	data["green"]=$( "#green" ).slider("value");
	data["blue"]=$( "#blue" ).slider("value");
	data["imgUrl"]=grabcutUrl;
	$.ajax({
        url : "/changeColor/changeExe",
        dataType : "json",
        type : "POST",
        data : data,
        success : function(data) {
        	var randomId = new Date().getTime();
        	$("#canvas-before").css("background-image","url('')");
        	$("#canvas-before").css("background-image","url('"+data.resultImg+"?v="+randomId+"')");        	
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
        }
    });
}