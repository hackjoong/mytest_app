$(function() {
	commentList();
	$('.btn-comment').click(function() {
		var id = $('[name=nickname]').val();
		var pw = $('#comment_pwd').val();
		var comment = $('[name=comment]').val();
	
		if (id == '' || id == null) {
			swal("ハンドルネームを入力してください!");
			return false;
		} else if(pw=='' || pw == null ){
			swal('暗証番号を入力してください!');
			return false;
		} else if(comment=='' || comment == null){
			swal('書き込みを入力してください!');
			return false;
		} else {
			var insertData = $('[name=form1]').serialize(); // commentInsertForm의
															// 내용을가져옴
			commentInsert(insertData); // Insert 함수호출(아래)
		}
	})
	$('.btn-comment-user').click(function() {
		var id = $('[name=nickname]').val();
		var comment = $('[name=comment]').val();
		if (id == ''|| id == null) {
			swal("IDを入力してください!");
			return false;
		} else if(comment=='' || comment == null){
			swal("書き込みを入力してください!");
			return false;
		} else {
			var insertData = $('[name=form1]').serialize(); // commentInsertForm의
															// 내용을가져옴
			commentInsert(insertData); // Insert 함수호출(아래)
		}
	})
	$('#dialog').find('form>input').keypress(function(e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			swal("ボタンを直接クリックしてください。");
			return false;
		}
	});

});

function commentInsert(insertData) {
	var tno = $('[name=tno]').val();
	$.ajax({
		url : '/comment/insert',
		type : 'post',
		async : true,
		data : insertData,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				var pwnick = $('[name=pwnick]').val();
				$('[name=pw]').val(pwnick);
				var idnick = $('[name=idnick]').val();
				$('#comment').val('');
				$('[name=nickname]').val(idnick);
				var total_cnt = $('#total_cnt').text();
				$('#total_cnt').text(parseInt(total_cnt) + 1);
				$('#comment_char_cnt').html($('#comment').val().length);
				/* location.href="/webmovie2/"+tno; */
				commentList();
			}
		}
	});
}


function fncLPAD(num) {
	if (num < 10)
		return '0' + num;
	else
		return num;
}

// 댓글 시작 start
function commentList() {
    var tno = $('[name=tno]').val();
    $.ajax({
    	type   : 'get',
        url    : '/comment/list',
        async  : true,
        data   : {
            'tno': tno
        },
        success: function (data) {
            var a = '';
            $.each(data, function (key, comment) {
                var idnick = $('[name=idnick]').val();
                var pwnick = $('[name=pwnick]').val();
                var now = new Date(comment.cdate);
                
                var comment_date = now.getFullYear() + '-' + fncLPAD(now.getMonth() + 1) + '-' +
                        fncLPAD(now.getDate()) + '&nbsp;' + fncLPAD(now.getHours()) + ':' +
                        fncLPAD(now.getMinutes()) + ':' + fncLPAD(now.getSeconds());

                a += '<li class="li_' + comment.cno + ' commentLi" style="clear:both;">';
                a += '<strong class="name commentPw_' + comment.cno + '" id="commentPw" style="displ' +
                        'ay:none;">' + comment.pw + '</strong>';
                a += '<strong class="name name_'+comment.cno+'">' + comment.nickname + '</strong>';
                a += '<span class="comment comment_'+comment.cno+'">' + comment.comment + '</span>';
                a += '<span class="comment-info">';
                a += '<time>' + comment_date + ' | ';
                a += '<a class="show_' + comment.cno + ' dapForm cp" onclick="show_dap(' +
                        comment.cno + ');">返事</a><b style="color:#f00;">(<span class="dapcount_'+comment.cno+'">'+comment.dapCount+'</span>)</b> | ';
                a += '<a class="cp" id="comodify_'+comment.nickname+'" onclick="commentModify(' + comment.cno + ',' + comment.tno +')">修整</a> | ';
                a += '<a class="cp" id="codelete_'+comment.nickname+'" onclick="myFunction(' + comment.cno + ',' + comment.tno +')">削除</a>';
                a += '</time> ';
                a += '</span>';
                a += '<div class="dap_' + comment.cno + ' dap" style="margin-top:30px; display:none; width:95%;float:right;' +
                        '">';
                a += '<form name="dapForm_' + comment.cno + '">';
                a += '<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" clas' +
                        's="datinput" /> ';
                a += '<input type="hidden" name="cno" class="comment_id" value="' + comment.cno + '">';
                a += '<input type="text" name="nickname" class="dap_name dapnick_'+comment.cno+' comment_id" placeholder="ID" value="' +
                        idnick + '" autocomplete="off" style="margin-bottom:5px; width:14%; margin-righ' +
                        't:10px;">';
                a += '<input type="password" name="pwd" class="dap_pw dappwd_'+comment.cno+' comment_pw" placeholder="Password" value="'+pwnick+'" autocomplete="off" style="margin-bottom:5px; width:14%; margin-righ' +
                't:10px;">';
                a += '<div class="mitjul">';
                a += '<textarea style="float:left; height:34px; width:84%; overflow:hidden;" rows="1" class="form-control form-d' +
                        'ap daptext_'+comment.cno+'" name="content" autocomplete="off" maxlength="10000" placeholder="返事">';
                a += '</textarea>';
                a += '<button type="button" style="float:right; width:14.1%; height:34px; margin-right:2px;" onclick="dapInsert(' + comment.cno +
                        ');" class="btn btn-block btn-dgray btn-dap">作成</button>';
                a += '</div>';
                a += '<div class="dapList_' + comment.cno + '" style="clear:both;width:100%;float:ri' +
                        'ght;margin-top:10px; padding-bottom:10px;">';
                a += '</form>';
                a += '</div>';
                a += '</li>';

            });
            $('.comments-list').html(a);

        },
        
    })
}

// 댓글 수정 start
function commentModify(cno, tno) {
	var s = confirm("修正しますか?");
	if (s == true) {
		$('#dialog').dialog({
			title : '書き込み秘密番号を入力してください。',
			modal : true,
			width : '300',
			height : '200',
			position:{
				my:"center",
				at:"center center"
			},
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","pwCheckComment("+cno+","+tno+")");
			},
			close:function(event,ui){
				$('#dialog>form>input').val('');
				$('.pwsubmit').removeAttr("onclick");
			}
		});
	}
}
//댓글수정비밀번호 검사
function pwCheckComment(cno,tno,pw){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/comment/checkPw',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'cno':cno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('.pwsubmit').removeAttr("onclick");
       		$('#dialog').dialog("close");
       		commentUpdate(cno,tno); 
       	 }
       	 else if (data == 0) {
                swal("暗証番号が一致しません。");
                $('#dialog>form>input').val('').focus();
                return false;
            }

        },
        error: function(data) {
        }
    })
}
// 댓글 수정 취소
function cancel() {
	var s = confirm("修正をキャンセルしますか?");
	if (s == true) {
		$('.pwsubmit').removeAttr("onclick");
		$('#dialog>form>input').val('');
		commentList();
	}
}

// 댓글 수정(내용출력을 input폼으로 변경)
function commentUpdate(cno,tno) {
	var a = '';
	var name = $('.name_'+cno).text();
	var comment = $('.comment_'+cno).text();

	a += '<div class="row row-sm">';
	a += '<div class="col-xs-10" style="margin-bottom:5px; width:100%;">';
	a += '<input type="hidden" name="tno" value="'+tno+'">'
	a += '<input type="text" name="nickname" id="id" class="comment_id" placeholder="ID" style="width:14%" value="'+name+'" autocomplete="off" readonly="readonly">';
	a += '</div>	<div class="col-xs-10">';
	a += '<textarea name="comment_'
			+ cno
			+ '" id="comment" class="form-control" placeholder="主題と無関係なコメントやスポイラー、悪質コメントは警告措置なしに削除されことができます。" maxlength="200">'+comment+'</textarea>';
	a += '</div>';
	a += '<div class="col-xs-2">';
	a += '<button type="button" class="btn btn-block btn-dgray" onclick="commentUpdateProc('
			+ cno
			+ ')" style="background-color:#5CB75C;width:50%;border-radius:0;height:55px;float:left;">修整</button>';
	a += '<button type="button" class="btn btn-block btn-dgray" onclick="cancel()" style="background-color:#D85450; width:50%; border-radius:0;height:55px;float:right; margin-top:0;">取消</button>';
	a += '</div>';
	a += '</div>';
	$('.li_' + cno).html(a);
}
// 댓글 수정
function commentUpdateProc(cno) {
	var updateContent = $('[name=comment_' + cno + ']').val();

	$.ajax({
		url : '/comment/update',
		type : 'post',
		data : {
			'comment' : updateContent,
			'cno' : cno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				swal("修正されました。");
				commentList(); // 댓글 수정후 목록 출력
		}
	});
}

// 댓글 삭제 start
function myFunction(cno, tno) {
	var s = confirm("書き込みを削除しますか?");
	if (s == true) {
		$('#dialog').dialog({
			title : '書き込み秘密番号を入力してください。',
			modal : true,
			width : '300',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","pwCheckcoDel("+cno+","+tno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').removeAttr("onclick");
				$('#dialog>form>input').val('');
			}
		});
	}
}
//댓글삭제 비번확인
function pwCheckcoDel(cno,tno){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/comment/checkPw',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'cno':cno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('.pwsubmit').removeAttr("onclick");
       		$('#dialog').dialog("close");
       		commentDelete(cno,tno); 
       	 }
       	 else if (data == 0) {
                swal("暗証番号が一致しません。");
                $('#dialog>form>input').val('').focus();
                return false;
            }

        },
        error: function(data) {
        }
    })
	
}
//실제 삭제되는 ajax 함수
function commentDelete(cno, tno) {
	$.ajax({
		url : '/comment/delete?cno=' + cno + "&tno=" + tno,
		type : 'post',
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				swal("削除されました。");
				var total_cnt = $('#total_cnt').text();
				$('#total_cnt').text(parseInt(total_cnt) - 1);
				commentList(); // 댓글 삭제후 목록 출력
		}
	});
}
// 댓글삭제 end

// 답글 입력
function dapInsert(cno) {
	var dapId = $('.dapnick_'+cno).val();
	var pwd = $('.dappwd_'+cno).val();
	var content = $('.daptext_'+cno).val();
	if (dapId == '' || dapId == null) {
		swal("ハンドルネームを入力してください!");
		return false;
	} else if(pwd == '' || pwd == null ){
		swal('暗証番号を入力してください!');
		return false;
	} else if(content == '' || content == null){
		swal('返事を入力してください!');
		return false;
	} else {
		var dapForm = $('[name=dapForm_' + cno + ']').serialize();
		dapInput(dapForm, cno);
	}

}
//답글 입력 ajax
function dapInput(dapForm, cno) {
	$.ajax({
		url : '/dap/insert?cno=' + cno,
		type : 'post',
		async : true,
		data : dapForm,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				var idnick = $('[name=idnick]').val();
				$('[name=nickname]').val(idnick);
				$('[name=content]').val('');
				var pwnick = $('[name=pwnick]').val();
				$('[name=pwd]').val(pwnick);
				var total_cnt = $('.dapcount_'+cno).text();
				$('.dapcount_'+cno).text(parseInt(total_cnt) + 1);
			}
			dapList(cno);
		}
	});
}
// 답글 목록
function dapList(cno) {
	$.ajax({
		url : '/dap/list',
		type : 'get',
		async : true,
		data : {
			'cno' : cno
		},
		success : function(data) {
			var a = '';
			$.each(data, function(key, value) {
				var now = new Date(value.ddate);
				var dap_date = now.getFullYear() + '-'
						+ fncLPAD(now.getMonth() + 1) + '-'
						+ fncLPAD(now.getDate()) + '&nbsp;'
						+ fncLPAD(now.getHours()) + ':'
						+ fncLPAD(now.getMinutes()) + ':'
						+ fncLPAD(now.getSeconds());
				a += '<li class="dapli_' + value.dno + ' dapli" style="width:100%;float:right; padding:10px 0;">';
				a += '<strong class="name dapPw_' + value.dno + '" id="commentPw" style="display:none;">' + value.pwd + '</strong>';
				a += '<strong class="name dwriter_'+value.dno+'">' + value.dwriter + '</strong>';
				a += '<span class="comment dap_comment_'+value.dno+'">' + value.content + '</span>';
				a += '<span class="comment-info">';
				a += '<time>' + dap_date + ' | ';
				a += '<a class="cp" onclick="dapModify(' + value.dno + ',' + value.cno +')">修整</a> | ';
				a += '<a class="cp" onclick="dapDeleteconfirm(' + value.dno + ',' + value.cno +')">削除</a></time> ';
				a += '</span></li>';
			});
			$(".dapList_" + cno).html(a);
		},
	});
}


//답글 수정 start
function dapModify(dno, cno) {
	var s = confirm("修正しますか?");
	if (s == true) {
		$('#dialog').dialog({
			title : '返事の暗証番号を入力してください。',
			modal : true,
			width : '300',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","dapSujung("+dno+","+cno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').removeAttr("onclick");
				$('#dialog>form>input').val('');
			}
		});
	}
}
//답글수정 비번확인
function dapSujung(dno,cno){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/dap/checkPw',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'dno':dno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('#dialog').dialog("close");
    		$('.pwsubmit').removeAttr("onclick");
    		$('#dialog>form>input').val('');
    		dapUpdate(dno,cno);
    		
       	 }
       	 else if (data == 0) {
                swal("暗証番号が一致しません。");
                $('#dialog>form>input').val('').focus();
                return false;
            }

        },
        error: function(data) {
        }
    })
}

// 답글 수정 취소
function cancel_dap(cno) {
	var s = confirm("返事修正をキャンセルしますか");
	if (s == true) {
		$('.pwsubmit').removeAttr("onclick");
		dapList(cno);
	}
}

// 답글 수정(내용출력을 input폼으로 변경)
function dapUpdate(dno,cno) {
	var a = '';
	var id=$('.dwriter_'+dno).text();
	var dap_comment = $('.dap_comment_'+dno).text();

	a += '<div class="row row-sm">';
	a += '<div class="col-xs-10" style="margin-bottom:5px; width:100%;">';
	a += '<input type="hidden" name="dno" value="'+dno+'">'
	a += '<input type="text" name="nickname_'+dno+'" id="id" class="comment_id" placeholder="ID" style="width:15.5%" value="'+id+'" autocomplete="off" readonly="readonly">';
	a += '</div>	<div class="col-xs-10" style="width:82.4%; padding: 0 0 0 15px;">';
	a += '<textarea name="dap_'	+ dno + '" id="comment" class="form-control" placeholder="主題と無関係なコメントやスポイラー、悪質コメントは警告措置なしに削除されことができます。" maxlength="200" style="height:34px; border-radius:0;width:98%;">'+dap_comment+'</textarea>';
	a += '</div>';
	a += '<div class="col-xs-2" style="padding:0; float:right; margin-right:16px; width:13.7%;">';
	a += '<input type="button" class="btn btn-block btn-dgray" onclick="dapUpdateProc('+ dno +','+cno+ ')" style="background-color:#5CB75C;width:50%;border-radius:0;height:34px;float:left; font-size:13px;" value="修整">';
	a += '<button type="button" class="btn btn-block btn-dgray" onclick="cancel_dap('+cno+')" style="background-color:#D85450; width:50%; border-radius:0;height:34px;float:right; margin-top:0; font-size:13px;">取消</button>';
	a += '</div>';
	a += '</div>';
	$('.dapli_' + dno).html(a);
}
//답글 수정
function dapUpdateProc(dno, cno) {
	var updateContent = $('[name=dap_' + dno + ']').val();
	var updateNick = $('[name=nickname_'+dno+']').val();
	
	
	$.ajax({
		url : '/dap/update',
		type : 'post',
		data : {
			'content' : updateContent,
			'dwriter' : updateNick,
			'dno' : dno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				swal("修正されました。");
				dapList(cno); // 댓글 수정후 목록 출력
		}
	});
}



//답글 삭제 start
function dapDeleteconfirm(dno, cno) {
	
	var s = confirm("削除しますか?");
	if (s == true) {
		
		$('#dialog').dialog({
			title : '返事の暗証番号を入力してください。',
			modal : true,
			width : '300',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","dapDeletePw("+dno+","+cno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').attr("onclick");
				$('#dialog>form>input').val('');
			}
		});
	}
}

//답글삭제 비번확인
function dapDeletePw(dno,cno){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/dap/checkPw',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'dno':dno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
    		$('[name=pwd]').val('');
    		$('#dialog>form>input').val('');
    		$('.pwsubmit').attr("onclick");
    		$('#dialog').dialog("close");
    		dapDelete(dno, cno);
       	 }
       	 else if (data == 0) {
       		swal("暗証番号が一致しません。");
    		$('#dialog>form>input').val('').focus();
            return false;
            }

        },
        error: function(data) {
        }
    });

}

//답글 삭제
function dapDelete(dno, cno) {

	$.ajax({
		url : '/dap/delete/' + dno,
		type : 'post',
		data : {
			cno:cno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				swal("削除されました。");
				var total_cnt = $('.dapcount_'+cno).text();
				$('.dapcount_'+cno).text(parseInt(total_cnt) - 1);
				dapList(cno); // 댓글 삭제후 목록 출력
		}
	});
}

// 답글삭제 end

// 답글 리스트와 Form Show Hide 함수 시작
function show_dap(cno) {
	$(".dap_" + cno).not(".dapForm").css("display", "block");
	$(".show_" + cno).text("閉じる").attr("onclick", 'hide_dap(' + cno + ')').attr(
			"class", "hide_" + cno);
	$('.li_' + cno).css("height", "220px");

	if ($(".dapList_" + cno).is(':empty')) {
		dapList(cno);
	}
	if($('.nullcheck').text()!=''){
		$('.dap_pw').css('display','none');
	}
	else if($('.nullcheck').text()==''){
		$('.dap_pw').css('display','block');
	}


	
	$(".dapList_" + cno).show();
}
function hide_dap(cno) {
	$(".dap_" + cno).css("display", "none");
	$(".hide_" + cno).text("返事").attr("onclick", 'show_dap(' + cno + ')').attr(
			"class", "show_" + cno);
	$(".dapList_" + cno).hide();
	$('.li_' + cno).css("height", "");
}
// 답글 리스트와 Form Show Hide 함수 끝

