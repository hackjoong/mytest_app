function fncLPAD(num) {
	if (num < 10)
		return '0' + num;
	else
		return num;
}


var bno = $('#tno').val(); // 게시글 번호

$('#submit').click(function() { // 댓글 등록 버튼 클릭시
	var insertData = $('[name=form1]').serialize(); // commentInsertForm의 내용을가져옴
	commentInsert(insertData); // Insert 함수호출(아래)
});

// 댓글 목록
function commentList() {
	$.ajax({
				url : '/comment/list',
				type : 'get',
				async : true,
				data : {
					'tno' : bno
				},
				success : function(data,key, comment) {
					var a = '';
					var as = new Date(comment.cdate);
					$.each(data,function(key, comment) {
										var now = new Date(comment.cdate);
										var comment_date = now.getFullYear()
										+ '-' + fncLPAD(now.getMonth()+1)
										+ '-' + fncLPAD(now.getDate())
										+ '&nbsp;'
										+ fncLPAD(now.getHours()) + ':'
										+ fncLPAD(now.getMinutes());
										
										a += '<div class="commentArea" style="padding:5px;border-bottom:1px solid darkgray; margin-bottom:12px; font-size:13px;">';
										
										a += '<div class="commentInfo'
												+ comment.cno
												+ '" style=" font-weight:bold;">作成者: '

												+ comment.nickname;

										a += '<div class="cp" style="float:right; font-weight:normal;">';
										a += '<a  onclick="commentUpdate('
												+ comment.cno + ',\''
												+ comment.pw + '\');"> 修整 </a>|';
										a += '<a  onclick="commentDelete('
												+ comment.cno+","+comment.tno
												+ ');"> 削除 </a> </div>';
										a += '</div>'
										a += '<div class="commentContent'
												+ comment.cno
												+ '"> <div>'
												+ comment.comment + '</div>';
										a += '<span style="color:#999; font-weight:normal">'
												+ comment_date + '</span>';
										a += '</div><a class="show_'+ comment.cno+' dapgul" style="cursor:pointer;"onclick="show('
												+ comment.cno + ')">応答</a>';
										a += '<div class="hello_'
												+ comment.cno
												+ ' hello" style="display:none;"><form name="dapgul_'+comment.cno+'" id="dap"  style="margin-bottom:10px; >';
										a += '<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" class="datinput" /> ';
										a += '<input type="hidden" name="cno" id="id" value="'+comment.cno+'">';
										
										a += '<input type="text" id="nickname" class="datinput" name="dwriter" style="border:0; border-bottom:1px solid #ccc; margin-top:20px;" placeholder="名前">';
										a += '<textarea id="textarea_dap" name="content" rows="1"';
										a += 'cols="83" autocomplete="off" maxlength="10000" placeholder="応答">';
										a += '</textarea>';
										a += '<input type="button" class="submit datinput dapsubmit" id="dap" name="dap" onclick="dapInsert('+comment.cno+');" value="作成">';
										a += '</form>';
										a += '<div class="dapList_'+comment.cno+'" style="clear:both;width:100%;float:right;margin-top:30px;">';
										a += '';
										a += '</div>';
										a += '</div>';
										a += '</div>';

									});

					$(".commentList").html(a);
				}
			});
	return false;
}

function dapInsert(cno) {
	var dap_data = $('[name=dapgul_'+cno+']').serialize();
	dapInput(dap_data, cno);
}

function dapInput(dap_data, cno) {
	$.ajax({
		url : '/dap/insert?cno='+cno,
		type : 'post',
		async : true,
		data : dap_data,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				$('[name=content]').val('');
				$('[name=dwriter]').val('');
			}
			dapList(cno);
			
		}
	});
}

// 답글 목록
function dapList(cno) {
	$.ajax({
				url : '/dap/list',
				type : 'get',
				async : true,
				data : {
					'cno' : cno
				},
				success : function(data) {
					var a = '';
					$
							.each(
									data,
									function(key, value) {
										var now = new Date(value.ddate);
										var dap_date = fncLPAD(now.getMonth()) + ',&nbsp;'
													+ fncLPAD((now.getDate())+1) + ',&nbsp;'
													+ now.getFullYear() + ',&nbsp;'
													+ fncLPAD(now.getHours()) + ':'
													+ fncLPAD(now.getMinutes());
										a += '<div style=" clear:both;width:95%;float:right;"><div class="commentArea" style="border-bottom:1px solid darkgray; margin-bottom: 15px;">';
										a += '<div class="commentInfo'
												+ value.dno
												+ '" style=" font-weight:bold;">作成者: '

												+value.dwriter;
										a += '<div class="cp" style="float:right; font-weight:normal"><a onclick="dapUpdate('
												+ value.dno
												+ ',\''
												+ value.content + '\','
												+ value.cno
												+ ');"> 修整 </a>|';
										a += '<a onclick="dapDelete('
												+ value.dno + "," + value.cno
												+ ');"> 削除 </a></div></div>';
										a += '<div class="dapContent'
												+ value.dno + '"> <div>'
												+ value.content + '</div>';
										a += '<span style="color:#999; font-weight:normal">'
												+ dap_date + '</span>';
										a += '</div></div></div>';
									});

					$(".dapList_"+cno).html(a);
				},
				/*
				 * error : function(request, status, error, data) {
				 * alert("code:" + request.status + "\n" + "message:" +
				 * request.responseText + "\n" + "error:" + error); }
				 */
			});
}
// 답글 수정
function dapUpdateProc(dno, cno) {
	var updateContent = $('[name=dap_' + dno + ']').val();
	
	$.ajax({
		url : '/dap/update',
		type : 'post',
		data : {
			'content' : updateContent,
			'dno' : dno,
			'cno' : cno
		},
		success : function(data) {
			if (data == 1)
				dapList(cno); // 댓글 수정후 목록 출력
		}
	});
}

// 댓글 수정(내용출력을 input폼으로 변경)
function dapUpdate(dno, content, cno) {
	var a = '';
	a += '<div class="input-group">';
	a += '<input type="text"  id="textarea_comment" class="datinput" style="width:97%; margin-bottom:1px;" name="dap_'
			+ dno + '"/>';
	a += '<span class="input-group-btn"><input type="button" class="submit datinput" value="修整" style="margin-bottom:5px;" onclick="dapUpdateProc('
			+ dno + "," + cno + ');"></span>';
	a += '</div>';

	$('.dapContent' + dno).html(a);
}

// 답글 삭제
function dapDelete(dno, cno) {

	$.ajax({
		url : '/dap/delete/' + dno,
		type : 'post',
		success : function(data) {
			if (data == 1)
				dapList(cno); // 댓글 삭제후 목록 출력
		}
	});
}

// 댓글 등록
function commentInsert(insertData) {
	$.ajax({
		url : '/comment/insert',
		type : 'post',
		async : true,
		data : insertData,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				$('[name=pw]').val('');
				$('[name=comment]').val('');
				var s = $('.countComment>span').text();
				$('.countComment>span').text(parseInt(s)+1);
				commentList();
			}
		}
	});
}

// 댓글 수정(내용출력을 input폼으로 변경)
function commentUpdate(cno, comment) {
	var a = '';

	a += '<div class="input-group">';
	a += '<input type="text"  id="textarea_comment" class="datinput" style="width:99%; margin-bottom:1px;" name="comment_'
			+ cno + '"/>';
	a += '<span class="input-group-btn"><input type="button" class="submit datinput" value="修整" style="margin-bottom:5px;" onclick="commentUpdateProc('
			+ cno + ');"></span>';
	a += '</div>';

	$('.commentContent' + cno).html(a);
}

// 댓글 수정
function commentUpdateProc(cno) {
	var updateContent = $('[name=comment_' + cno + ']').val();

	$.ajax({
		url : '/comment/update',
		type : 'post',
		data : {
			'comment' : updateContent,
			'cno' : cno
		},
		success : function(data) {
			if (data == 1)
				commentList(bno); // 댓글 수정후 목록 출력
		}
	});
}

// 댓글 삭제
function commentDelete(cno, tno) {

	$.ajax({
		url : '/comment/delete?cno=' + cno + "&tno=" + tno,
		type : 'post',
		success : function(data) {
			if (data == 1)
				var s = $('.countComment>span').text();
			$('.countComment>span').text(parseInt(s)-1);
				commentList(bno); // 댓글 삭제후 목록 출력
		}
	});
}

function show(cno) {	
	$(".hello_" + cno).css("display", "block");
	$(".show_"+cno).text("閉じる").attr("onclick",'hide('+cno+')').attr("class","hide_"+cno);
	
	if ($(".dapList_"+cno).is(':empty')) {
		dapList(cno);		
	}
	$(".dapList_"+cno).show();
}

function hide(cno){
	// commentList();
	$(".hello_" + cno).css("display", "none");
	$(".hide_"+cno).text("応答").attr("onclick",'show('+cno+')').attr("class","show_"+cno);
	/*
	 * $(".dapgul").not(".show_"+cno).text("답글달기").attr("onclick",'show('+cno+')').attr("class","show"+cno);
	 * $(".hello").not(".hello_" + cno).css("display", "block");
	 */
	$(".dapList_"+cno).hide();
}

$(document).ready(function() {
	commentList();

})
