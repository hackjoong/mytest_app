$(function() {
	$(".gen").hide();
	$(".gen:first").show();
	$(".genre-menu li").click(function() {
		if (!$(this).is(":animated")) {
			$(".genre-menu li").removeClass("active").css("color", "#333");
			// $(this).addClass("active").css({"color": "darkred","font-weight":
			// "bolder"});
			$(this).addClass("active").css("color", "darkred");
			$(".gen").hide()
			var activeTab = $(this).data("genre");
			$("." + activeTab).fadeIn()
		} else {
			return;
		}
	});
});