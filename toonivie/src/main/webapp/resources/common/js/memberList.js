$(document).ready(function() {
    $("#id").focus();
    
    $('.pwform').hide();
    

	$('.checkbox_pw').click(function(){
		if($('.checkbox_pw').prop('checked')==false){
		    $('.pwform').show();
		}
		else{
			$('.pwform').hide();
		}
	})
    
});

$(".confirm_pw").keyup(function() {
    var pw = $("[name = pw]").val();
    var re_pw = $("[name=confirm]").val();
    if (pw != re_pw || pw == "" || re_pw == "") {
        $(".check_pw_span").css("color", "red");
        $(".check_pw_span").text("비밀번호가 일치하지 않습니다.");
    } else {
        $(".check_pw_span").css("color", "blue");
        $(".check_pw_span").text("비밀번호가 일치합니다.");
    }
});

function modify_member() {
    var idReg = /([0-9a-zA-Z]{3,11})\w+/g;
    var pwReg = /([0-9A-Za-z]{4,11})/g;
    var nameReg = /([가-힣A-Za-z]{2,4})/g;
    var nameKeyupReg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    var emailReg = /[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}/g
    var idValue = $("[name = id]").val();
    var pwValue = $("[name = pw]").val();
    var nameValue = $("[name = name]").val();
    var emailValue = $("[name = email]").val();
    var confirmValue = $("[name = confirm]").val();
 
    if($('.checkbox_pw').prop('checked')==false){
    	if ( idValue == "") {
            swal("아이디를 확인해주세요.");
            return false;
        } else if (emailReg.test(emailValue) == false || emailValue == "") {
            swal("이메일을 확인해주세요.");
            return false;
        } else if (nameReg.test(nameValue) == false || nameValue == "") {
            swal("이름을 확인해주세요.");
            return false;
        } else if (pwValue != confirmValue || pwReg.test(pwValue) == false ||
            pwValue == "") {
            swal("비밀번호를 확인해주세요.");
            return false;
        } else {
            update();
        }
	}
	else{
		 if (idValue == "") {
		        swal("아이디를 확인해주세요.");
		        return false;
		    } else if (emailReg.test(emailValue) == false || emailValue == "") {
		        swal("이메일을 확인해주세요.");
		        return false;
		    } else if (nameReg.test(nameValue) == false || nameValue == "") {
		        swal("이름을 확인해주세요.");
		        return false;
		    } else {
		        update();
		    }
	}
   
}

var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

function mDelete(userSeq) {
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = {};
    var headers = {};
    data[csrfParameter] = csrfToken;
    headers[csrfHeader] = csrfToken;
    var id= $('.id_'+userSeq).text();
    var member_delete = confirm("삭제하시겠습니까?");
    if (member_delete == true) {
        $.ajax({
            url: '/member/delete/' + userSeq,
            type: 'post',
            data:{
            	'id':id
            },
            success: function(data) {

                if (data == 1) {
                    swal("삭제되었습니다.");
                    home();
                }

            },
            error: function(data) {
            }
        })
    }
    else{
    	return false;
    	home();
    }
}

function memberInfo(userSeq) {
    var d = $('.auth_'+userSeq).text();
  var fu = $.trim(d);
   
    if(fu=="사용자"){
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_user').prop("checked",true);

    }else if(fu=="관리자"){
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_admin').prop("checked",true);
    }
    else if(fu=="작가"){
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_writer').prop("checked",true);
    }
    else{
    	$('[name=auth]').removeAttr("checked");
    	$('.auth_manager').prop("checked",true);
    }
 
    $('.info-modify').css('display', 'none');
    $('.logins').css('display', 'block');
    var a = $('.id_' + userSeq).text();
    $("[name=id]").val(a);
    var b = $('.name_' + userSeq).text();
    $("[name=name]").val(b);
    var c = $('.email_' + userSeq).text();
    $("[name=email]").val(c);
    $("[name=userSeq2]").val(userSeq);
    
 

}

function close_info(userSeq) {
    var a = confirm("작성중인 내용을 취소하시겠습니까?");
    if (a == true) {
        $('.logins').css("display", "none");
        $('.info-modify').css('display', 'block');
    }
}



function update() {
    var id = $("[name=id]").val();
    var name = $("[name=name]").val();
    var email = $("[name=email]").val();
    var pw = $("[name=pw]").val();
    var no = $("[name=userSeq2]").val();
    var check = $('input:checkbox[name="changedPW_kr"]').is(":checked");
    var auth = $(':radio[name="auth"]:checked').val();

    $.ajax({
        url: "/member/update",
        type: "POST",
        data: {
            'id': id,
            'name': name,
            'email': email,
            'pw': pw,
            'no': no,
            'auth': auth,
            'noChangedPW':check
        },
        dataType: 'json',
        success: function(data) {
            if (data == 1) {
                swal("수정되었습니다.");
                home();
            }
        },
        error: function(request, status, error) {
        	swal({
				  title: 'Error!',
				  text: "code:" + request.status + "\n" + "error:" + error,
				  type: 'error',
				})
        }
    });
}
function home(){
	$.ajax({
		type : "GET",
		url : "/member/list",
		error : function() {
			swal('페이지를 불러오지 못했습니다.');
		},
		success : function(data) {
			$('#Context').html(data);
		}

	});
}
