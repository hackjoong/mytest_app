function fileUploadMoveObjectImg(){
	$('#moveObjectForm').ajaxForm({
		url: '/moveObject/uploadImg',
		method:"POST",
		enctype: "multipart/form-data",
		success: function(result){
			moveObject(result.foreUrl,result.backUrl);
		}
	});
	$("#moveObjectForm").submit();
}
function moveObject(data){
	var dataSend = {};
	dataSend["str"] = data;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    $.ajax({
       url : "/moveObject/moveExe",
       dataType : "json",
       type : "POST",
       data : dataSend,
       async:false,
       success : function(data) {
    	   insertVideoTab(data);    	  
    	   $("#moveVideoResult").siblings('a').remove();
    	   $("#moveVideoResult").remove();
    	   $(".video-modal").append("" +
                   "<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
    	   popupOn($(".video-modal"));
       },

       error : function(request, status, error) {
          swal({
			  title: 'Error!',
			  text: "code:" + request.status + "\n" + "error:" + error,
			  type: 'error',
			})
       }
    });
}

function moveObject_new(data){
	var dataSend = {};
	dataSend["str"] = data;
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");  // THIS WAS ADDED
    var headers = {};
    headers[csrfHeader] = csrfToken;
    data[csrfParameter] = csrfToken;
    $.ajax({
       url : "/MultiApplyObject/getApply_Move",
       dataType : "json",
       type : "POST",
       data : dataSend,
       async:false,
       success : function(data) {
    	   insertVideoTab(data);    	  
    	   $("#moveVideoResult").siblings('a').remove();
    	   $("#moveVideoResult").remove();
    	   $(".video-modal").append("" +
                   "<video id=\"moveVideoResult\" controls><source src='"+data.videoUrl+"'></video>");
    	   popupOn($(".video-modal"));
       },

       error : function(request, status, error) {
          swal({
			  title: 'Error!',
			  text: "code:" + request.status + "\n" + "error:" + error,
			  type: 'error',
			})
       }
    });
}

//add psh 20180312
var titleName = null;
$(".effect_titleName").click(function(){
	titleName = $(this).attr('title');
});

var sceneNum=1;
/* 장면연출 항목 생성 */
function insertVideoTab(data){ //checked, modify by shkwak, 2017-06-19
	$(".effect-cut-success-button").removeClass("disabled");	
	var effectTime = data.effectTime;
	var thumbNailUrl = data.thumbNailUrl;  
	
	appendString ="";
	// add psh 20180312
	appendString +=	"<div class='sceneListSortable effect-cut-content-item videoList"+sceneNum+"' data-videoUrl='"+data.videoUrl+"' data-videowidth='"+data.videoWidth+"' data-videoheight='"+data.videoHeight+"' data-effectTime='"+effectTime+"' data-use='yes'>"+
						"<input class='re_sceneNum' type='checkbox' id='chk"+sceneNum+"' onclick='checkboxThisScene(this.id);' checked>" +
						"<label style='position: absolute; height: 20px; width: 20px; margin-top:9px; border:2px solid #ffffff;' for='chk"+sceneNum+"'><spen></spen></label>" +
						"<div class='effect-cut-sumnail' onclick='showThisScene("+sceneNum+");' style=\"background-image:url('"+thumbNailUrl+"');background-size:cover; margin-left: 32px;\"></div>"+
						"<div class='fl' style='display: inline; width: 115px; text-align: center; margin-top:5px;'>"+
							"<div style='width: 100%; font-size: 12px;'>Scene "+sceneNum+"</div>" +
							"<div style='font-size: 9px; width:90%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;'>'"+titleName+"'</div>" +
						"</div>"+
						"<div class='sortable_content fr' style='margin-top:12px;'>"+
							"<input type='text' class='effect-cut-time mr10 tc' value='"+effectTime+"s' readOnly style='width: 45px;'>"+
							"<span class='fl cp'><img class='' style='width: 15px; margin-top:5px;' src='/resources/image/v2.0/sceneChange.png' alt='sceneListChange' /></span>"+
						"</div>"+
					"</div>";
	
	$(".if-scroll-padding-right").append(appendString);
	sceneNum++;
	
	$( ".if-scroll-padding-right" ).sortable();
}

///연출 장면 UI 개선, 체크박서, 삭제(X) 함수 추가
function removeThisScene(id) {
	$(".videoList"+id).hide();
	$(".videoList"+id).data("use","no"); //videoList의 사용유무 표시	
}
//add by shkwak, 2017-06-20
function checkboxThisScene(id) { //id:chk1
	var id = id.substr(3,id.length); //1
	if($("#chk"+id).is(":checked")){
		$(".videoList"+id).data("use","yes");		
    }else{
    	$(".videoList"+id).data("use","no");
    }
}

function showThisScene(id) {
	var videoUrl = $(".videoList"+id).data("videourl");
	$("#moveVideoResult").remove();
	$(".video-modal").append("<video id=\"moveVideoResult\" controls><source src='"+videoUrl+"'></video>");
	popupOn($(".video-modal"));
}

///연출 장면 저장 및 불러오기 함수 추가
//shkwak, 2017-06-20
$(".effect-cut-save").click(function(){
	var title = "scene1"; // + createFileUtils.getToday(1);
	var contents = $('.sceneList').html();
	html5rocks.webdb.updateTodo(contents); //webdb update
	swal("장면연출 항목이 저장되었습니다.");
	
});

$(".effect-cut-load").click(function(){
	html5rocks.webdb.selectTodo(1); //id=1
});

//=======================================================================================
//장면효과 JavaScript Code 정리 by shkwak, 2017-07-06
//=======================================================================================
