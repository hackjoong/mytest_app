$(document).ready(function() {
	var fileTarget = $('.filebox #mywebmovieFile');
	$('#change_no').hide();

	$('#checkbox').click(function(){
		if($('#checkbox').prop('checked')==false){
		    $('.filebox').show();
		}
		else{
			$('.filebox').hide();
		}
	})

	

	fileTarget.on('change', function() { // 값이 변경되면
		$(this).siblings('.upload-name').val('');
		if (window.FileReader) { // modern browser
			var filename = $(this)[0].files[0].name;
		} else { // old IE
			var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																				// 추출
		}
	
		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename);
	});
	
	//radio button click(webmovie and origintoon)
	$('input:radio[name=kinds]').click(function(){
		if($(this).val() == 'webmovie'){
			$('.change-movie-text').text("영상");
		}
		else if($(this).val() == 'origintoon'){
			$('.change-movie-text').text("썸네일");
		}
		
	});

});

var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	/* $(".wtr").hide(); */

});


function seriesDelete(wmno) {
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var data = {};
	var headers = {};
	data[csrfParameter] = csrfToken;
	headers[csrfHeader] = csrfToken;
	var thumbnail = $('.tth_'+wmno).text();
	var file = $('.tfile_'+wmno).text();

	var webtoon_delete = confirm("삭제하시겠습니까?");
	if (webtoon_delete == true) {

		$.ajax({
			url : '/series/delete/' + wmno,
			type : 'post',
			data:{
				'thumbnail':thumbnail,
				'file':file
			},
			success : function(data) {

				if (data == 1) {
					swal("삭제되었습니다.");
					home();
				}

			},
			error : function(data) {
			}
		})
	} else {
		return false;
	}
}
function seriesUpdate(wmno) {
	$("#upload_myseries").attr({
		id : "update_myseries",
		action : "/series/update"
	});

	if($('#checkbox').is(':checked')){
	    $('.filebox').hide();
	}


	$('#change_no').show();
	var a = $('.genre_' + wmno).text();
	$("[name=genre]").val($.trim(a));
	var b = $('.title_' + wmno).text();
	$("[name=title]").val($.trim(b));
	var c = $('.writer_' + wmno).text();
	$("[name=writer]").val($.trim(c));
	var d = $('.content_' + wmno).text();
	$.trim($("[name=content]").val($.trim(d)));
	var e = $('.tfile_' + wmno).text();
	var f = e.substring(30);
	var as = $('.idx_' + wmno).text();
	$("[name=idx]").val($.trim(as));
	$('.upload-name').attr({
		"value" : $.trim(f)
	});

	$("[name=wmno]").val(wmno);
	$(".submit_toon").text("수정").attr("onclick", "seriesFormupdate()");
	$("span.fl").text("시리즈 수정");
	$(".cancel").attr({
		type : "button",
		onclick : "cancel()"
	});
	$("#mywebmovieFile").attr("name", "upfile3");
}


function cancel() {
	var a = confirm("작성중인 내용을 지우고 취소하시겠습니까?");
	if (a == true) {
		$("#update_myseries").attr({
			id : "upload_myseries",
			action : "/series/upload"
		});
		$("[name=genre]").val("");
		$("[name=title]").val("");
		$("[name=writer]").val("");
		$("[name=content]").val("");
		$("[name=wmno]").val("");
		$(".upload-name").attr({
			"value" : "파일선택"
		});
		$(".submit_toon").text("업로드").attr("onclick", "mypage_upload_series()");
		$("span.fl").text("시리즈 올리기");
		$('.change-movie-text').text("썸네일");
		$(".cancel").attr("type", "reset");
		$(".cancel").removeAttr("onclick");
		$('[name=kinds]').prop('checked',false);

		$("#mywebmovieFile").attr("name", "upfile");
		$('#change_no').hide();
		$('.filebox').show();
		$('#webmovie').prop("selected",true);
	}
}


function seriesFormupdate() {
	loadingOn();
	$("#update_myseries")
			.ajaxForm(
					{
						url : "/series/update?${_csrf.parameterName}=${_csrf.token}",
						type : "POST",
						enctype : "multipart/form-data",

						success : function(result) {
							if(result==1){
								swal("수정되었습니다.");
								loadingOff();
								home();
							}
							else{
								swal("에러.");
								loadingOff();
								home();
							}
						},
						error : function(request, status, error) {
							swal({
								  title: 'Error!',
								  text: "code:" + request.status + "\n" + "message:"
									+ request.responseText + "\n" + "error:"
									+ error,
								  type: 'error',
								})
							loadingOff();
							home();
						}

					});
	$("#update_myseries").submit();

}


var wfilename = "";
var wfilename_origin = ""
var wUploaded = false; // psd file upload check
//series upload
function mypage_upload_series() { // /upload_singlefile
	loadingOn();
	$('#upload_myseries')
	.ajaxForm({
				url : "/series/upload?${_csrf.parameterName}=${_csrf.token}",
				method : "POST",
				enctype : "multipart/form-data", // 여기에 url과 enctype은
													// 꼭 지정해주어야 하는 부분이며
													// multipart로 지정해주지
													// 않으면 controller로
													// 파일을 보낼 수 없음
				success : function(result) {
					wfilename = result; // .resultUrl;
					swal(wfilename_origin + " 파일이 " + wfilename
							+ " 파일명으로 서버에 저장되었습니다");
					wUploaded = true;
					loadingOff();
					//home();


				},
				error : function(request, status, error) {
					swal({
						  title: 'Error!',
						  text: "code:" + request.status + "\n" + "message:"
							+ request.responseText + "\n" + "error:"
							+ error,
						  type: 'error',
						});
					loadingOff();
					home();

				}

			});
	// 여기까지는 ajax와 같다. 하지만 아래의 submit명령을 추가하지 않으면 백날 실행해봤자 액션이 실행되지 않는다.
	$("#upload_myseries").submit();

}
function home() {
	var link = document.location.pathname;
	var url;
	if(link == "/adminPage"){
		url = "/admin/series";
	}
	else{
		url = "/mypage/series";
	}
	$.ajax({
		type : "GET",
		url : url,
		dataType : "text",
		error : function() {
			swal('페이지를 불러오지 못했습니다.');
		},
		success : function(data) {
			$('#Context').html(data);
		}

	});
}