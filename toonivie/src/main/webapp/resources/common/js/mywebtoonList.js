
$(document).ready(function() {
	var fileTarget = $('.filebox #mywebmovieFile');
	$('#change_no').hide();

	$('#checkbox').click(function(){
		if($('#checkbox').prop('checked')==false){
		    $('.filebox').show();
		}
		else{
			$('.filebox').hide();
		}
	})

	

	fileTarget.on('change', function() { // 값이 변경되면
		$(this).siblings('.upload-name').val('');
		if (window.FileReader) { // modern browser
			var filename = $(this)[0].files[0].name;
		} else { // old IE
			var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만
																				// 추출
		}
	
		// 추출한 파일명 삽입
		$(this).siblings('.upload-name').val(filename);
	});
});





var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(function() {
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	/* $(".wtr").hide(); */

});

function wDelete(tno) {
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var data = {};
	var headers = {};
	data[csrfParameter] = csrfToken;
	headers[csrfHeader] = csrfToken;
	var thumbnail = $('.tth_'+tno).val();
	var file = $('.tfile_'+tno).val();

	var webtoon_delete = confirm("삭제하시겠습니까?");
	if (webtoon_delete == true) {

		$.ajax({
			url : '/webtoon/delete/' + tno,
			type : 'post',
			data:{
				'thumbnail':thumbnail,
				'file':file
			},
			success : function(data) {

				if (data == 1) {
					swal("삭제되었습니다.");
					toonList();
				}

			},
			error : function(data) {
			}
		})
	} else {
		return false;
	}
}

function wUpdate(tno) {
	$("#upload_mywebtoon_movie").attr({
		id : "upload_webtoon_movie3",
		action : "/uploadWebtoon/update"
	});

	if($('#checkbox').is(':checked')){
	    $('.filebox').hide();
	}
	
	
	$('#change_no').show();
	var a = $('.genre_' + tno).text();
	$("[name=genre]").val($.trim(a));
	var b = $('.title_' + tno).text();
	$("[name=title]").val($.trim(b));
	var c = $('.writer_' + tno).text();
	$("[name=writer]").val($.trim(c));
	var d = $('.content_' + tno).text();
	$.trim($("[name=content]").val($.trim(d)));
	var e = $('.tfile_' + tno).text();
	var f = e.substring(23);
	var as = $('.idx_' + tno).text();
	$("[name=idx]").val($.trim(as));
	$('.upload-name').attr({
		"value" : $.trim(f)
	});
	var g =	$('.free_open_'+tno).data('open');
	if(g == false){
		g = false;
	}
	$('#check_free').prop('checked', g);
	
	var wmno = $('.wmno_'+tno).text();
	var length = $('[name=seriestitle]').children().length;
	for(var i = 0; i<length; i++){
		if($('[name=seriestitle]').children().eq(i).val()==wmno){
			$('[name=seriestitle]').children().eq(i).prop('selected',true);
			break;
		}
	}
	

	$("[name=tno]").val(tno);
	$(".submit_toon").text("수정").attr("onclick", "update()");
	$("span.fl").text("웹툰무비 수정");
	$(".cancel").attr({
		type : "button",
		onclick : "cancel()"
	});
	$("#mywebmovieFile").attr("name", "upfile3");

}

function cancel() {
	var a = confirm("작성중인 내용을 지우고 취소하시겠습니까?");
	if (a == true) {
		$("#upload_webtoon_movie3").attr({
			id : "upload_mywebtoon_movie",
			action : "/uploadWebtoon/upload"
		});
		$("[name=genre]").val("");
		$("[name=title]").val("");
		$("[name=writer]").val("");
		$("[name=content]").val("");
		$("[name=tno]").val("");
		$(".upload-name").attr({
			"value" : "파일선택"
		});
		$(".submit_toon").text("업로드").attr("onclick", "mypage_upload_moviefile1()");
		$("span.fl").text("웹툰무비 업로드");
		$(".cancel").attr("type", "reset");
		$(".cancel").removeAttr("onclick");

		$("#mywebmovieFile").attr("name", "upfile");
		$('#change_no').hide();
		$('.filebox').show();
	}
}

function update() {

	loadingOn();
	$("#upload_webtoon_movie3")
			.ajaxForm(
					{
						url : "/uploadWebtoon/update?${_csrf.parameterName}=${_csrf.token}",
						type : "POST",
						enctype : "multipart/form-data",

						success : function() {
							swal("수정되었습니다.");
							loadingOff();
							toonList();

						},
						error : function(request, status, error) {
							swal({
								  title: 'Error!',
								  text: "code:" + request.status + "\n" + "message:"
									+ request.responseText + "\n" + "error:"
									+ error,
								  type: 'error',
								})
							loadingOff();
							toonList();
						}

					});
	$("#upload_webtoon_movie3").submit();

}

var wfilename = "";
var wfilename_origin = ""
var wUploaded = false; // psd file upload check
// Webtoon Movie - Single File Upload Code
// 2017-04-11, shkwak
// 2017-07-19, RE Use
function mypage_upload_moviefile1() { // /upload_singlefile
	
	loadingOn();
	$('#upload_mywebtoon_movie')
	.ajaxForm({
				url : "/uploadWebtoon/upload?${_csrf.parameterName}=${_csrf.token}",
				method : "POST",
				enctype : "multipart/form-data", 
				success : function(result) {
					wfilename = result; // .resultUrl;
					swal(wfilename_origin + " 파일이 " + wfilename
							+ " 파일명으로 서버에 저장되었습니다");
					wUploaded = true;
					loadingOff();
					//toonList();


				},
				error : function(request, status, error) {
					swal({
						  title: 'Error!',
						  text: "code:" + request.status + "\n" + "message:"
							+ request.responseText + "\n" + "error:"
							+ error,
						  type: 'error',
						})
					loadingOff();
					toonList();

				}

			});
	$("#upload_mywebtoon_movie").submit();

}


function toonList() {
	$.ajax({
		type : "GET",
		url : "/mypage/webmovie",
		dataType : "text",
		error : function() {
			swal('페이지를 불러오지 못했습니다.');
		},
		success : function(data) {
			$('#Context').html(data);
		}

	});
}