$(document).ready(function(){
	$('.rank-type').find('span').eq(0).click(function(){
		var data ={};
		$.ajax({
			url:'/order/lastest',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});
				$('.rank-type').find('span').eq(0).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type').find('span').eq(1).click(function(){
		var data ={};
		$.ajax({
			url:'/order/commentOrder',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});	 
				$('.rank-type').find('span').eq(1).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type').find('span').eq(2).click(function(){
		var data ={};
		$.ajax({
			url:'/order/countOrder',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webmovie2/'+val.tno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.title + ' </a></div>');
				});	 
				$('.rank-type').find('span').eq(2).addClass('fontcolor-red');
				$('.rank-type').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type').find('span').eq(0).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});
	
	
	//wonjak ranking
	
	$('.rank-type-won').find('span').eq(0).click(function(){
		var data ={};
		$.ajax({
			url:'/order/countOrder_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});	 
				$('.rank-type-won').find('span').eq(0).addClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});

	$('.rank-type-won').find('span').eq(1).click(function(){
		var data ={};
		$.ajax({
			url:'/order/commentOrder_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});
				$('.rank-type-won').find('span').eq(1).addClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(0).removeClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(2).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});


	$('.rank-type-won').find('span').eq(2).click(function(){
		var data ={};
		$.ajax({
			url:'/order/lastest_won',
			type:'POST',
			data:data,
			success: function(data) {	
				$('.rank-cont').empty();
				$.each(data, function(idx, val) {
					
					$('.rank-cont').append('<span class="cp"><a href="/webtoon/'+val.wtno+'" style="text-decoration:none; color:#000;"><b>' + (idx+1) + "</b> " + val.wtname + ' </a></div>');
				});	 
				$('.rank-type-won').find('span').eq(2).addClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(1).removeClass('fontcolor-red');
				$('.rank-type-won').find('span').eq(0).removeClass('fontcolor-red');
			},
			error : function(request, status, error) {
				alert("code:" + request.status
						+ "\n" + "error:" + error);	
			}
		});
	});
});