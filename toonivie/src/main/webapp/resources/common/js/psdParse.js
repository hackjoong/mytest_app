

var psd_img_mode = 'false';
function fileUploadPsdParseImg(){ //체크
	$('#upLoadPsdfileForm').ajaxForm({
		url: '/psdParse/psdUpload',
		method:"POST",
		enctype: "multipart/form-data",
		async:false,
		success: function(result){
			fileUploadOpertFlag = true;
			$("#layered_img_cropped").children().remove();
	    	objs = new Array();
	    	lastInfo = new Array();
	    	firstInfo = new Array();
	    	background_postion = new Array();
			canvas_index = new Array();
			psdParseExe(result.resultUrl);
			$('#upload_psd').data('uploaded', 'true');
			psd_img_mode = 'true';
			$("#canvas_bg").removeClass("dn");
			$(".saved-image-item").data("image","");
			$(".canvas-before").data("image","").css("background-image","url('')");
			$(".saved-image-item-image").data("image","").css("background-image","url('')");
			$(".dragDiv_BG").data("image","").css("background-image","url('')");
			$(".itemNumBG").data("image","").css("background-image","url('')"); // psd분리 bg
			$(".canvas-codeBG").data("image","").css("background-image","url('')"); // psd분리 캔퍼스 bg
			$(".use-image-canvas").data("image","").css("background-image","url('')"); // 캔버스에 딴 이미지 올린것
			$("#canvas-bg").data("image","").css("background-image","url('')");
			$(".canvas-fg").addClass("dn");	
			$(".canvas-bg").addClass("dn"); 
			
			$(".upload-img").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
			$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
			$("#canvas-before").addClass("dn");
			$(".color_box_text").removeClass("dn");
			$(".color_box_me").remove();
			

			//psh add 20180409 이미지 컷 초기화
			cropper_croped_status_wt = 'false';
			$(".re_img_upload_preview").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
			$("#wt_cut_save").text('컷 분리하기').addClass('disabled'); //이미지 분리 버튼
			$("#wt_whole_cropArea").cropper('destroy').removeAttr('src'); //크롭퍼 비활성화
			$('#wt_cut_saveName').val("");	
			$(".imgPreview_").show();
			$('#wt_cut_uploadCount').text("");
			$(".wtcut_box").children(".wt_cut_appendImg").remove();
			$("#wt_cut_save_all").addClass('disabled');
			/*$(".side_img_cut_preview_box").remove();*/ 
			
			if(result.psd_ThumbPath != ""){
				$('#upload_psd').attr('src', result.psd_ThumbPath);
				$('#upload_psd').css('display', 'block');
			}else{
				//미리보기 없을경우
			}
			$('.img_selection').data('selection', 'true');
			shrinking_ratio = 0.8;
		    $(".canvas-wrap").addClass("dn");
		    btop_start_position = $(".canvas-codeBG").offset().top;
		    videoFrameSizeSelect();
		    
			$(".clxk").draggable({ 
				disabled : false,
		    });
		},
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
            loadingOff();
        }
	});
	$("#upLoadPsdfileForm").submit();

	$(".psd_Open").addClass('disabled');
}
function psdParseExe(imgUrl){ //체크
	var data={};
	data["imgUrl"]=imgUrl;
	loadingOn();
	$.ajax({
        url : "/psdParse/psdParseExe",
        dataType : "json",
        type : "POST",
        data : data,
        success : function(data) {
        	setTimeout(function () { 
        	loadingOff();
        	makeTopLayers(data);     
        	},3000);
        },
        error : function(request, status, error) {
            alert("code:" + request.status + "\n" + "error:" + error);
            loadingOff();
        }

    });
}

