function applyTemplete(imgUrl,activeTab){
    var data = {};
    var url="/templete/applyTemplete";
    data["imgUrl"]=imgUrl;
    data["mode"]=activeTab;
    if(activeTab=="tab7"){
    	url="/templete/applyVibrate";
    }
    else if(activeTab=="tab8"){
    	$('#customTempleteForm').ajaxForm({
    		url: '/templete/uploadCustomTemplete',
    		method:"POST",
    		enctype: "multipart/form-data",
    		success: function(result){
    			return customTemplete(imgUrl,result.templete1,result.templete2);
    		}
    	});
    	$("#customTempleteForm").submit();
    }
    $.ajax({
       url : url,
       dataType : "json",
       type : "POST",
       data : data,
       success : function(data) {
    	   $("#rainVideo").html("<source src='"+data.videoUrl+"'>");
    	   $(".popup-blackbg").show();
    	   $(".heedo-popup").show();
    	   $(".video-effect-2").removeClass("dn");
    	   $(".video-effect-1").addClass("dn");
       },
       error : function(request, status, error) {
    	   alert("code:" + request.status + "\n" + "error:" + error);
       }
    });
}
function fileUploadTempleteImg(activeTab){
	$('#fileForm3').ajaxForm({
		url: '/upload/uploadImg',
		method:"POST",
		enctype: "multipart/form-data",
		success: function(result){
			applyTemplete(result.resultUrl,activeTab);
		}
	});
	$("#fileForm3").submit();
}
function customTemplete(imgUrl,templete1,templete2){
	var data = {};
	data["imgUrl"]=imgUrl;
	data["templete1"]=templete1;
	data["templete2"]=templete2;
	$.ajax({
	   url : "/templete/applyCustomTemplete",
	   dataType : "json",
	   type : "POST",
	   data : data,
	   success : function(data) {
	       $("#rainVideo").html("<source src='"+data.videoUrl+"'>");
	       $(".popup-blackbg").show();
	       $(".heedo-popup").show();
	       $(".video-effect-2").removeClass("dn");
	       $(".video-effect-1").addClass("dn");
	   },
	   error : function(request, status, error) {
	       alert("code:" + request.status + "\n" + "error:" + error);
	   }
	});
}