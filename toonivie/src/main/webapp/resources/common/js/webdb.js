/*!
 * webdb.js v0.1.0 (https://www.toonivie.com/)
 * Copyright 2017 Ideaconcert
 * Licensed under *****
 * Author : shkwak
 * Date : 2017-06-20(Tue)
 */

	var html5rocks = {};
    html5rocks.webdb = {};
    html5rocks.webdb.db = null;
  
    html5rocks.webdb.open = function() {
      var dbSize = 50 * 1024 * 1024; // 5MB -> 50MB
      html5rocks.webdb.db = openDatabase("toonivie_scene", "1.0", "Toonivie DB", dbSize);
    }
  
    html5rocks.webdb.createTable = function() {
      var db = html5rocks.webdb.db;
      db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS toonivie_scene(ID INTEGER PRIMARY KEY ASC, title TEXT, contents TEXT, updated DATETIME)", []); //add  user TEXT, pw TEXT
      });
    }
  
    html5rocks.webdb.addTodo = function(t, c) { //webdb add
      var db = html5rocks.webdb.db;
      db.transaction(function(tx){
        var d = new Date();
        tx.executeSql("INSERT INTO toonivie_scene(title, contents, updated) VALUES (?,?,?)", 
            [t, c, d],
            function() {
              html5rocks.webdb.getAllTodoItems(loadTodoItems);
            },
            html5rocks.webdb.onError);
        
       });
    }
    
    //add by shkwak, 2017-06-20
    html5rocks.webdb.updateTodo = function(c) { //webdb update
    	var db = html5rocks.webdb.db;
        db.transaction(function(tx){
          var d = new Date();
          tx.executeSql("UPDATE toonivie_scene SET contents=?, updated=?  WHERE ID=1;", 
              [c, d],
              function() {
                html5rocks.webdb.getAllTodoItems(loadTodoItems);
              },
              html5rocks.webdb.onError);          
         });    
    }
    
    //add by shkwak, 2017-06-20
    html5rocks.webdb.selectTodo = function(id) {	
    	var result = "";
        var db = html5rocks.webdb.db;
        db.transaction(function(tx) {
          tx.executeSql("SELECT contents FROM toonivie_scene WHERE ID=?;", [id], 
        	function(tx, rs) {
        	  	result = rs.rows.item(0).contents;
        	  	$('.sceneList').html(result); //scene load
            }, 
              html5rocks.webdb.onError);
        });       
    }
  
    html5rocks.webdb.onError = function(tx, e) {
      alert("There has been an error: " + e.message);
    }
  
    html5rocks.webdb.onSuccess = function(tx, r) {
	  // 모든 데이터를 다시 그림
	  //html5rocks.webdb.getAllTodoItems(tx, r);
    }
  
  
    html5rocks.webdb.getAllTodoItems = function(renderFunc) {
      var db = html5rocks.webdb.db;
      db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM toonivie_scene", [], renderFunc, 
            html5rocks.webdb.onError);
      });
    }
  
    html5rocks.webdb.deleteTodo = function(id) { //webdb delete
      var db = html5rocks.webdb.db;
      db.transaction(function(tx){
        tx.executeSql("DELETE FROM toonivie_scene WHERE ID=?", [id],      
            function() { 
              html5rocks.webdb.getAllTodoItems(loadTodoItems);
            }, 
            html5rocks.webdb.onError);
        });
    }
  
    function loadTodoItems(tx, rs) {
    }
    
    function renderTodo(row) { //modify by shkwak
      return "<li>" + row.title + ", " + row.contents + " [<a href='javascript:void(0);'  onclick='html5rocks.webdb.deleteTodo(" + row.ID +");'>Delete</a>]</li>";
    }
  
    function webdbInit() { //webdb init
      html5rocks.webdb.open();
      html5rocks.webdb.createTable();
    }
     
    function webdbAdd(title,contents) {
      html5rocks.webdb.addTodo(title,contents);	
    }

    function addUser() { //modify by shkwak
      var user = document.getElementById("user"); 
  	  var pw = document.getElementById("pw");
        html5rocks.webdb.addTodo(user.value, pw.value);
        user.value = "";
  	  pw.value = "";
      }
    
	function login() { //add by shkwak
      var user = document.getElementById("user"); 
	  var pw = document.getElementById("pw");
      html5rocks.webdb.getAllTodoItems(loadTodoItems);
      user.value = "";
	  pw.value = "";
    }