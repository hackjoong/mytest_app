$(document).ready(function() {

	var append_idx = 0;
	$('#wt_cut_save').click(function(){
		if(cropper_croped_status_wt == 'true'){
		
		var img_previw_title_num = $(".side_img_cut_preview").children().length+1;
		$(".imgPreview_").hide();
		var img_dataurl = $('#wt_whole_cropArea').cropper('getCroppedCanvas').toDataURL('image/jpeg');
		var append_str = "<div class='wt_cut_appendImg mt5' style='position: relative;' data-idx='"+append_idx+"'>" + 
						 	"<img style='width: 100%' class='"+(transImgNameFlag == true ? 'dn' : '')+"' src='"+img_dataurl+"'>" + "<span class='"+(transImgNameFlag == true ? '' : 'dn')+"'> #"+append_idx+" </span>" + 
						 	"<img style='position: absolute; right:2px; top:2px; width:20px; cursor: pointer;' src='/resources/image/v2.0/wt_cut_delete.png' onclick='wt_cut_delete(this)'>" +
						 "</div>";
		
		var append_side_previw = "<div class='side_img_cut_preview_box' data-idx='"+append_idx+"'>" + 
								 	"<div class='img_previw_title'>#"+img_previw_title_num+"</div>" +
								 	"<img class='img_previw_imgs' src='"+img_dataurl+"'>" +
								 "</div>";
		
		$('.wtcut_box').append(append_str);
		$('.side_img_cut_preview').append(append_side_previw);
		images.push(img_dataurl);
		$('#wt_cut_uploadCount').html("개수 : " +($('.wt_cut_appendImg').length+1));
		wt_cropper_status($('#wt_whole_cropArea'));
		
		$(".use-image-canvas").data("image","").css("background-image","url('')");
		$("#canvas-before").addClass("dn");
		$("#canvas-bg").data("image","").css("background-image","url('')");
		$(".canvas-fg").addClass("dn");	
		$(".canvas-bg").addClass("dn"); 
		$("#background_img").addClass("dn");
		$("#transImgName").removeClass("disabled");
		
		$('.wt_whole_img').on('cropend', '#wt_whole_cropArea', function(){
			wt_cropper_status($('#wt_whole_cropArea'));
		});
		append_idx++;
		}else if(cropper_croped_status_wt == 'false'){
			fileUploadWebtoonImg();
		}
		
	});
	$('#wt_cut_save_all').click(function(){
		createArchive(images);
	});
});

function wt_cut_delete(me){
	swal({
		  title: "선택한 컷 이미지를 삭제하시겠습니까?",
		  width: 400,
		  icon: "warning",
		  buttons: true,
		  showCancelButton: true,
		  dangerMode: true,
		})
		.then((result) => {
		if (result.value) {
			var delete_num = $(me).parent(".wt_cut_appendImg").attr('data-idx');
			$(".side_img_cut_preview_box[data-idx='"+delete_num+"']").addClass("demo");
			$(".side_img_cut_preview .demo").remove();
			var aaaaaa = $(".wt_cut_appendImg").index($(me).parent(".wt_cut_appendImg"));
			images.splice(aaaaaa, 1);
			$(me).parent().remove();
			var count = $(".wt_cut_appendImg").length;
			if(count == 0){
				$("#transImgName").addClass("disabled");
			}
			$('#wt_cut_uploadCount').html("개수 : " +count);
			wt_cropper_status($('#wt_whole_cropArea'));
			
			$('.wt_cut_appendImg').each(function(idx){		
				$(this).children("span").text("#"+(idx+1));
			});
			
			$(".img_previw_title").each(function(idx){
				var previw_title = "#" + (idx+1);
				$(this).text("");
				$(this).text(previw_title);
			});
		}else {
			return false;
		}
	});
}

var images = [];
function createArchive(images){
	  if($('#wt_cut_saveName').val() == ''){
		  swal('파일명을 입력해주세요.');
		  $('#wt_cut_saveName').focus();
		  return;
	  }
	  var zip = new JSZip(); //jszip
	  var img = zip.folder($('#wt_cut_saveName').val());
	  
	  for (var i=0; i<images.length; i++) {
		  var commaIdx = images[i].indexOf(",");
		  img.file($('#wt_cut_saveName').val()+"_"+(i+1)+".jpg", images[i].slice(commaIdx + 1), {base64: true});
	  }
      zip.generateAsync({type:"blob"}).then(function(file){
    	  saveAs(file, $('#wt_cut_saveName').val()+".zip");
      });

	}
function downloadURI(uri, name) {
	  var link = document.createElement("a");
	  link.download = name;
	  link.href = uri;
	  document.body.appendChild(link);
	  link.click();
	  document.body.removeChild(link);
	  delete link;
	}

function fileUploadWebtoonImg(){ //이미지 따기 클릭 이벤트
	$('#fileUploadWebtoonImg').ajaxForm({
		url : '/upload/uploadImg_wt',
		method : "POST",
		enctype : "multipart/form-data",
		async : false,
		beforeSend:function(){
			loadingOn();
		},
		success : function(result){		
			setTimeout(function () {
				$("#wt_cut_save").text('이미지 컷 실행');
				$(".wt_whole_img").show();
				$('.wt_whole_img').removeClass('dn');
				$(".side_img_cut_preview_box").data("idx","").attr("data-idx","");
				$('#wt_whole_cropArea').attr('src', "/resources/cvimgs/wt_img/"+result.resultUrl);
				$(".wt_whole_img").css("width", '1047px'); //우선 고정
				
				
				cropper_init_wt($('#wt_whole_cropArea'));

				imgcut_Imgdiv();

				imgcut_psdreset();
				
				leftTabSet();
				
				loadingOff();
				swal('이미지 컷할 영역을 지정해 주세요.');
			},1000);
		}
	});
	$("#fileUploadWebtoonImg").submit();
}

var cropper_croped_status_wt = 'false';
function cropper_init_wt(canvas_id){
	cropper_croped_status_wt = 'true';
	canvas_id.cropper('destroy');
	canvas_id.cropper({
		  viewMode: 3,
		  zoomable: false,
		  checkOrientation: false,
		  dragMode: 'crop',
		  ready: function () {
			  $(this).cropper('clear');
			  wt_cropper_status($(this));
		  },
	});
	
}
function wt_cropper_status(canvas_id){
	if($(".wt_cut_appendImg").length != 0){
		$('#wt_cut_save_all').addClass('btn-orange');
		$('#wt_cut_save_all').removeClass('btn-gray');
		$('#wt_cut_save_all').removeClass('disabled');
	}else{
		$('#wt_cut_save_all').removeClass('btn-orange');
		$('#wt_cut_save_all').addClass('btn-gray');
		$('#wt_cut_save_all').addClass('disabled');
	}
}

function imgcut_Imgdiv(){
	$(".canvas-before").removeAttr("data").removeAttr("style").removeAttr("width").removeAttr("height");
	$(".saved-image-item-image, .dragDiv_BG, .itemNumBG, .canvas-codeBG").removeAttr("data").removeAttr("style");
	$(".saved-image-item-image").removeAttr("data");
	$(".saved-image-item").data("image","").removeClass("ui-draggable-disabled").removeClass("ui-draggable-handle").addClass("disabled");
	$(".canvas-fg, .canvas-bg, #background_img, .canvas_width_re, .color_box_text, #canvas-before, .glyphicon-pushpin").addClass("dn");	

	$(".re_upload_img_cut").data("image","").attr("src","/resources/image/toolicon_1.png"); // 기본 이미지
	$("#img").val(''); 
	$("#img_name").text("");
	$(".wrap1 , .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8, .wrap9").remove();
	
	$(".color_box_me").remove();
	$(".imgtab_click").css("z-index","101");
	//이미지 분리 종류
	$("#cutModeBtn1").attr({"disabled":"disabled","src":"/resources/image/v2.0/cutModeBtn1.png"}).addClass("disabled"); 
	$("#cutModeBtn2").attr({"disabled":"disabled","src":"/resources/image/v2.0/cutModeBtn2.png"}).addClass("disabled");
	//배경 색상 채우기
	$("#fill_black").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_black.png"}).addClass("disabled");
	$("#fill_white").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_white.png"}).addClass("disabled");
	$("#fill_surrounding").attr({"disabled":"disabled","src":"/resources/image/v2.0/fill_surrounding.png"}).addClass("disabled");
	$("#grabcut_color_picker").attr({"disabled":"disabled","src":"/resources/image/v2.0/grabcut_color_picker.png","background-color":"#fff"}).addClass("disabled");
	//버튼 비활성화
	$("#cut_img_ori").attr("disabled","disabled").addClass("disabled"); 
	$(".cut-img-add").attr("disabled","disabled").addClass("disabled"); 
	$(".color_change_btns").attr("disabled","disabled").addClass("disabled"); 
	$("#ROI_select").attr("disabled","disabled").addClass("disabled");
	gcDataReSet();
	
}
function imgcut_psdreset(){
	$("#canvas_bg").removeAttr("data").removeAttr("style");
	$(".psd_Layer_Preview").addClass("dn"); //레이어 미리보기
	$(".psd_LPC_img").css("background","");
	$(".psd_LPC_img").css("height","0");
	$(".second_saved_img").addClass("dn");
	$("#upload_psd").attr("src","/resources/image/toolicon_1.png"); 
}