$(function() {
	commentList();
	$('#btn-comment-wonjak').click(function(){
		if($('.user_id').val()==null||$('.user_id').val()==''){
			var id = $('[name=nickname]').val();
			var pw = $('[name=pw]').val();
			var comment = $('[name=comment]').val();
			if (id == '' || id == null) {
				alert("아이디를 입력해주세요!1");
				return false;
			} else if(pw=='' || pw == null ){
				alert('비밀번호를 입력해주세요!1');
				return false;
			} else if(comment=='' || comment == null){
				alert('댓글을 입력해주세요!1');
				return false;
			} else {
				var insertData = $('[name=form1]').serialize(); // commentInsertForm의
																// 내용을가져옴
				commentInsert(insertData); // Insert 함수호출(아래)
			}
		}
		else{
			var id = $('[name=nickname]').val();
			var comment = $('[name=comment]').val();
			if (id == ''|| id == null) {
				alert("ID를 입력해주세요!");
				return false;
			} else if(comment=='' || comment == null){
				alert("댓글을 입력해주세요!");
				return false;
			} else {
				var insertData = $('[name=form1]').serialize(); // commentInsertForm의
																// 내용을가져옴
				commentInsert(insertData); // Insert 함수호출(아래)
			}
		}
	})
	$('#dialog').find('form>input').keypress(function(e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			alert("버튼을 직접클릭해주세요");
			return false;
		}
	});

});

// 댓글 등록
function commentInsert(insertData) {
	var wtno = $('[name=wtno]').val();
	$.ajax({
		url : '/comment/insert_toon',
		type : 'post',
		async : true,
		data : insertData,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				var pwnick = $('[name=pwnick]').val();
				$('[name=pw]').val(pwnick);
				var idnick = $('[name=idnick]').val();
				$('[name=comment]').val('');
				$('[name=nickname]').val(idnick);
				var total_cnt = $('#total_cnt').text();
				$('#total_cnt').text(parseInt(total_cnt) + 1);
				$('#comment_char_cnt').html($('#comment').val());
				commentList();
			}
		}
	});
}


function fncLPAD(num) {
	if (num < 10)
		return '0' + num;
	else
		return num;
}

// 댓글 시작 start
function commentList() {
	var wtno = $('[name=wtno]').val();
    $.ajax({
    	type   : 'get',
        url    : '/comment/list_toon',
        async  : true,
        data   : {
            'wtno': wtno
        },
        success: function (data) {
            var a = '';
            $.each(data, function (key, wtcomment) {
                var idnick = $('[name=idnick]').val();
                var pwnick = $('[name=pwnick]').val();
                var now = new Date(wtcomment.cdate);
                var comment_date = now.getFullYear() + '-' + fncLPAD(now.getMonth() + 1) + '-' +
                        fncLPAD(now.getDate()) + '&nbsp;' + fncLPAD(now.getHours()) + ':' +
                        fncLPAD(now.getMinutes()) + ':' + fncLPAD(now.getSeconds());

                a += '<li class="li_' + wtcomment.cno + ' commentLi" style="clear:both;">';
                a += '<strong class="name commentPw_' + wtcomment.cno + '" id="commentPw" style="displ' +
                        'ay:none;">' + wtcomment.pw + '</strong>';
                a += '<strong class="name name_'+wtcomment.cno+'">' + wtcomment.nickname + '</strong>';
                a += '<span class="comment comment_'+wtcomment.cno+'">' + wtcomment.comment + '</span>';
                a += '<span class="comment-info">';
                a += '<time>' + comment_date + ' | ';
                a += '<a class="show_' + wtcomment.cno + ' dapForm cp" onclick="show_dap(' +
                		wtcomment.cno + ');">답글</a><b style="color:#f00;">(<span class="dapcount_'+wtcomment.cno+'">'+wtcomment.wtdapCount+'</span>)</b> | ';
                a += '<a class="cp" id="comodify_'+wtcomment.nickname+'" onclick="commentModify(' + wtcomment.cno + ',' + wtcomment.wtno +
                        ',\'' + wtcomment.pw + '\')">수정</a> | ';
                a += '<a class="cp" id="codelete_'+wtcomment.nickname+'" onclick="woncomDeleted(' + wtcomment.cno + ',' + wtcomment.wtno +
                        ',\'' + wtcomment.pw + '\')">삭제</a>';
                a += '</time> ';
                a += '</span>';
                a += '<div class="dap_' + wtcomment.cno + ' dap" style="margin-top:15px; display:none; width:95%; float:right;' +
                        '">';
                a += '<form name="dapForm_' + wtcomment.cno + '">';
                a += '<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" clas' +
                        's="datinput" /> ';
                a += '<input type="hidden" name="cno" class="comment_id" value="' + wtcomment.cno + '">';
                a += '<input type="text" name="nickname" class="comment_id dap_name dapnick_'+wtcomment.cno+' m_comment_id" placeholder="ID" value="' +
                        idnick + '" autocomplete="off" style="margin-bottom:5px; width:16%; margin-righ' +
                        't:10px;">';
                a += '<input type="password" name="pwd" class="comment_id dap_pw dappwd_'+wtcomment.cno+' m_comment_pw" placeholder="Password" value="'+pwnick+'" autocomplete="off" style="margin-bottom:5px; width:16%; margin-righ' +
                't:10px;">';
                a += '<div class="mitjul">';
                a += '<textarea style="float:left;font-size:14px; width:82%; height:34px; overflow:hidden;" rows="1" class="dap_text form-control form-d' +
                        'ap daptext_'+wtcomment.cno+'" name="content" autocomplete="off" maxlength="10000" placeholder="답글">';
                a += '</textarea>';
                a += '<button type="button" style="float:right; width:14.1%; height:35px; margin-right:2px;" onclick="dapInsert(' + wtcomment.cno +
                        ');" class="btn btn-block btn-dgray btn-dap m_dap_be">등록</button>';
                a += '</div>';
                a += '<div class="dapList_' + wtcomment.cno + '" style="clear:both;width:100%;float:ri' +
                        'ght;margin-top:10px; padding-bottom:10px;">';
                a += '</form>';
                a += '</div>';
                a += '</li>';
            });
            $('.comments-list').html(a);
        },        
    });
}

// 댓글 수정 start
function commentModify(cno, wtno, pw) {
		$('#dialog').dialog({
			title : '댓글 비밀번호를 입력하세요.',
			modal : true,
			width : '350',
			height : '200',
			position:{
				my:"center",
				at:"center center"
			},
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","pwCheckComment("+cno+","+wtno+")");
			},
			close:function(event,ui){
				$('#dialog>form>input').val('');
				$('.pwsubmit').removeAttr("onclick");
			}
		});
}
//댓글수정비밀번호 검사
function pwCheckComment(cno,wtno){
	var inputPw = $('#dialog>form>input').val();
	var pwd = $('.commentPw_' + cno).text();
	$.ajax({
        url: '/comment/checkPw_toon',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'cno':cno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('.pwsubmit').removeAttr("onclick");
    		$('#dialog').dialog("close");
    		commentUpdate(cno,wtno);
       	 }
       	 else if (data == 0) {
                alert("비밀번호가 일치하지 않습니다.");
                $('#dialog>form>input').val('').focus();
                return false;
            }

        },
        error: function(data) {
        }
    })

}
// 댓글 수정 취소
function cancel() {
	var s = confirm("수정을 취소하시겠습니까?");
	if (s == true) {
		$('.pwsubmit').removeAttr("onclick");
		$('#dialog>form>input').val('');
		commentList();
	}
}

// 댓글 수정(내용출력을 input폼으로 변경)
function commentUpdate(cno,wtno) {
	var a = '';
	var name = $('.name_'+cno).text();
	var comment = $('.comment_'+cno).text();

	a += '<div class="row row-sm">';
	a += '<div class="col-xs-10" style="margin-bottom:5px; width:100%;">';
	a += '<input type="hidden" name="tno" value="'+wtno+'">'
	a += '<input type="text" name="nickname" id="id" class="comment_id m_comment_id" placeholder="ID" style="width:14%" value="'+name+'" autocomplete="off" readonly="readonly">';
	a += '</div>	<div class="col-xs-10">';
	a += '<textarea name="comment_'
			+ cno
			+ '" class="form-control" placeholder="주제와 무관한 댓글이나 스포일러, 악플은 경고 조치 없이 삭제될 수 있습니다." maxlength="200">'+comment+'</textarea>';
	a += '</div>';
	a += '<div class="col-xs-2 m_comment_dgay_re">';
	a += '<button type="button" class="btn btn-block btn-dgray m_btn-dgray_re" onclick="commentUpdateProc('
			+ cno
			+ ')" style="background-color:#5CB75C;width:50%;border-radius:0;height:55px;float:left;">수정</button>';
	a += '<button type="button" class="btn btn-block btn-dgray m_btn-dgray_re" onclick="cancel()" style="background-color:#D85450; width:50%; border-radius:0;height:55px;float:right; margin-top:0;">취소</button>';
	a += '</div>';
	a += '</div>';
	$('.li_' + cno).html(a);
}
// 댓글 수정
function commentUpdateProc(cno) {
	var updateContent = $('[name=comment_' + cno + ']').val();

	$.ajax({
		url : '/comment/update_toon',
		type : 'post',
		data : {
			'comment' : updateContent,
			'cno' : cno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				commentList(); // 댓글 수정후 목록 출력
		}
	});
}

// 댓글 삭제 start ============================================================
function woncomDeleted(cno, tno, pw) {
		$('#dialog').dialog({
			title : '댓글 비밀번호를 입력하세요.',
			modal : true,
			width : '350',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","pwCheckcoDel("+cno+","+tno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').removeAttr("onclick");
				$('#dialog>form>input').val('');
			}
		});
}
//댓글삭제 비번확인
function pwCheckcoDel(cno,tno){
	var inputPw = $('#dialog>form>input').val();	
	$.ajax({
        url: '/comment/checkPw_toon',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'cno':cno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('.pwsubmit').removeAttr("onclick");
    		$('#dialog').dialog("close");
    		commentDelete(cno, tno);
       	 }
       	 else if (data == 0) {
                alert("비밀번호가 일치하지 않습니다.");
                $('#dialog>form>input').val('').focus();
                return false;
            }

        },
        error: function(data) {
        }
    })
}
//실제 삭제되는 ajax 함수
function commentDelete(cno, tno) {
	$.ajax({
		url : '/comment/delete_toon?cno=' + cno + "&wtno=" + tno,
		type : 'post',
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				alert("삭제되었습니다.");
				var total_cnt = $('#total_cnt').text();
				$('#total_cnt').text(parseInt(total_cnt) - 1);
				commentList(); // 댓글 삭제후 목록 출력
		}
	});
}
// 댓글삭제 end ================================================================

// 답글 입력
function dapInsert(cno) {
	var dapId = $('.dapnick_'+cno).val();
	var pwd = $('.dappwd_'+cno).val();
	var content = $('.daptext_'+cno).val();
	
	if($('.user_id').val()==null || $('.user_id').val()==''){
		if (dapId == '' || dapId == null) {
			alert("아이디를 입력해주세요!");
			return false;
		} else if(pwd == '' || pwd == null ){
			alert('비밀번호를 입력해주세요!');
			return false;
		} else if(content == '' || content == null){
			alert('답글을 입력해주세요!');
			return false;
		} else {
			var dapForm = $('[name=dapForm_' + cno + ']').serialize();
			dapInput(dapForm, cno);
		}
	}
	else{
		pwd = $('.dappwd_'+cno).val($('.comment_pw').val());
		if (dapId == '' || dapId == null) {
			alert("아이디를 입력해주세요!");
			return false;
		} else if(content == '' || content == null){
			alert('답글을 입력해주세요!');
			return false;
		} else {
			var dapForm = $('[name=dapForm_' + cno + ']').serialize();
			dapInput(dapForm, cno);
		}
	}

}
//답글 입력 ajax
function dapInput(dapForm, cno) {
	$.ajax({
		url : '/dap/insert_toon?cno=' + cno,
		type : 'post',
		async : true,
		data : dapForm,
		success : function(data) {
			if (data == 1) {
				// 댓글 작성 후 댓글 목록 reload
				var idnick = $('[name=idnick]').val();
				$('[name=nickname]').val(idnick);
				$('[name=content]').val('');
				var pwnick = $('[name=pwnick]').val();
				$('[name=pwd]').val(pwnick);
				var total_cnt = $('.dapcount_'+cno).text();
				$('.dapcount_'+cno).text(parseInt(total_cnt) + 1);
			}
			dapList(cno);
		}
	});
}
// 답글 목록
function dapList(cno) {
	$.ajax({
		url : '/dap/list_toon',
		type : 'get',
		async : true,
		data : {
			'cno' : cno
		},
		success : function(data) {
			var a = '';
			$.each(data, function(key, value) {
				var now = new Date(value.wtddate);
				var dap_date = now.getFullYear() + '-'
						+ fncLPAD(now.getMonth() + 1) + '-'
						+ fncLPAD(now.getDate()) + '&nbsp;'
						+ fncLPAD(now.getHours()) + ':'
						+ fncLPAD(now.getMinutes()) + ':'
						+ fncLPAD(now.getSeconds());
				a += '<li class="dapli_' + value.wdno + ' dapli" style="width:100%;float:right; padding:10px 0;">';
				a += '<strong class="name dapPw_' + value.wdno + '" id="commentPw" style="display:none;">' + value.wtpwd + '</strong>';
				a += '<strong class="name dwriter_'+value.wdno+'">' + value.wtdwriter + '</strong>';
				a += '<span class="comment dap_comment_'+value.wdno+'">' + value.wtcontent + '</span>';
				a += '<span class="comment-info">';
				a += '<time>' + dap_date + ' | ';
				a += '<a class="cp" onclick="dapModify(' + value.wdno + ',' + value.cno +')">수정</a> | ';
				a += '<a class="cp" onclick="dapDeleteconfirm(' + value.wdno + ',' + value.cno +')">삭제</a></time> ';
				a += '</span></li>';
			});
			$(".dapList_" + cno).html(a);
		},
	});
}


//답글 수정 start
function dapModify(wdno, cno) {
		$('#dialog').dialog({
			title : '답글 비밀번호를 입력하세요.',
			modal : true,
			width : '350',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","dapSujung("+wdno+","+cno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').removeAttr("onclick");
				$('#dialog>form>input').val('');
			}
		});
}
//답글수정 비번확인
function dapSujung(wdno,cno){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/dap/checkPw_toon',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'wdno':wdno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
       		$('#dialog').dialog("close");
    		$('.pwsubmit').removeAttr("onclick");
    		$('#dialog>form>input').val('');
    		dapUpdate(wdno,cno);
    		
       	 }
       	 else if (data == 0) {
       		alert("비밀번호가 일치하지 않습니다.");
    		$('#dialog>form>input').val('').focus();
            return false;
            }

        },
        error: function(data) {
        }
    })
}

// 답글 수정 취소
function cancel_dap(cno) {
	var s = confirm("답글 수정을 취소하시겠습니까?");
	if (s == true) {
		$('.pwsubmit').removeAttr("onclick");
		dapList(cno);
	}
}

// 답글 수정(내용출력을 input폼으로 변경)
function dapUpdate(wdno,cno) {
	var a = '';
	var id=$('.dwriter_'+wdno).text();
	var dap_comment = $('.dap_comment_'+wdno).text();

	a += '<div class="row row-sm">';
	a += '<div class="col-xs-10" style="margin-bottom:5px; width:100%;">';
	a += '<input type="hidden" name="dno" value="'+wdno+'">'
	a += '<input type="text" name="nickname_'+wdno+'" id="id" class="comment_id m_comment_id" placeholder="ID" style="width:15.5%" value="'+id+'" autocomplete="off" readonly="readonly">';
	a += '</div>	<div class="col-xs-10" style="width:82.4%; padding: 0 0 0 15px;">';
	a += '<textarea name="dap_'	+ wdno + '" id="comment" class="form-control" placeholder="주제와 무관한 댓글이나 스포일러, 악플은 경고 조치 없이 삭제될 수 있습니다." maxlength="200" style="height:34px; border-radius:0; width:98%;">'+dap_comment+'</textarea>';
	a += '</div>';
	a += '<div class="col-xs-2 m_btn-dgray_re-top" style="padding:0; float:right; margin-right:16px; width:13.3%;">';
	a += '<input type="button" class="btn btn-block btn-dgray m_btn-dgray_re_er" onclick="dapUpdateProc('+ wdno +','+cno+ ')" style="background-color:#5CB75C;width:50%;border-radius:0;height:34px;float:left; font-size:13px;" value="수정">';
	a += '<button type="button" class="btn btn-block btn-dgray m_btn-dgray_re_er" onclick="cancel_dap('+cno+')" style="background-color:#D85450; width:50%; border-radius:0;height:34px;float:right; margin-top:0; font-size:13px;">취소</button>';
	a += '</div>';
	a += '</div>';
	$('.dapli_' + wdno).html(a);
}
//답글 수정
function dapUpdateProc(wdno, cno) {
	var updateContent = $('[name=dap_' + wdno + ']').val();
	var updateNick = $('[name=nickname_'+wdno+']').val();
	
	
	$.ajax({
		url : '/dap/update_toon',
		type : 'post',
		data : {
			'content' : updateContent,
			'dwriter' : updateNick,
			'dno' : wdno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				dapList(cno); // 댓글 수정후 목록 출력
		}
	});
}



//답글 삭제 start
function dapDeleteconfirm(wdno, cno) {	
		$('#dialog').dialog({
			title : '답글 비밀번호를 입력하세요.',
			modal : true,
			width : '350',
			height : '200',
			open : function(event, ui) {
				$('.pwsubmit').attr("onclick","dapDeletePw("+wdno+","+cno+")");
			},
			close : function(event, ui){
				$('.pwsubmit').attr("onclick");
				$('#dialog>form>input').val('');
			}
		});
}

//답글삭제 비번확인
function dapDeletePw(wdno,cno){
	var inputPw = $('#dialog>form>input').val();
	$.ajax({
        url: '/dap/checkPw_toon',
        type: 'post',
        data:{
        	'inputPw':inputPw,
        	'wdno':wdno
        },
        success: function(data) {
       	 
       	 if(data == 1) {
    		$('[name=pwd]').val('');
    		$('#dialog>form>input').val('');
    		$('.pwsubmit').attr("onclick");
    		dapDelete(wdno, cno);
       	 }
       	 else if (data == 0) {
       		alert("비밀번호가 일치하지 않습니다.");
    		$('#dialog>form>input').val('').focus();
            return false;
            }

        },
        error: function(data) {
        }
    })

}

//답글 삭제
function dapDelete(wdno, cno) {

	$.ajax({
		url : '/dap/delete_toon/' + wdno,
		type : 'post',
		data:{
			cno:cno
		},
		success : function(data) {
			if (data == 1)
				$('#dialog').dialog("close");
				alert("삭제되었습니다.");
				var total_cnt = $('.dapcount_'+cno).text();
				$('.dapcount_'+cno).text(parseInt(total_cnt) - 1);
				dapList(cno); // 댓글 삭제후 목록 출력
		}
	});
}

// 답글삭제 end

// 답글 리스트와 Form Show Hide 함수 시작
function show_dap(cno) {
	$(".dap_" + cno).not(".dapForm").css("display", "block");
	$(".show_" + cno).text("닫기").attr("onclick", 'hide_dap(' + cno + ')').attr(
			"class", "hide_" + cno);
	$('.li_' + cno).css("height", "auto");

	if ($(".dapList_" + cno).is(':empty')) {
		dapList(cno);
	}
	if($('.nullcheck').text()!=''){
		$('.dap_pw').css('display','none');
	}
	else if($('.nullcheck').text()==''){
		$('.dap_pw').css('display','block');
	}


	
	$(".dapList_" + cno).show();
}
function hide_dap(cno) {
	$(".dap_" + cno).css("display", "none");
	$(".hide_" + cno).text("답글").attr("onclick", 'show_dap(' + cno + ')').attr(
			"class", "show_" + cno);
	$(".dapList_" + cno).hide();
	$('.li_' + cno).css("height", "");
}
// 답글 리스트와 Form Show Hide 함수 끝

